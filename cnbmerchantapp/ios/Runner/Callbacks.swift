//
//  Callbacks.swift
//  Runner
//
//  Created by Puth Neng on 27/6/23.
//
import UIKit
import ShieldSDK

internal class Callbacks: NSObject, ShieldCallback,FlutterStreamHandler {
    
    var _eventSink: FlutterEventSink?
    private var eventCallBackName: String = ""
    
    func onListen(withArguments arguments: Any?, eventSink events: @escaping FlutterEventSink) -> FlutterError? {
        self._eventSink = events
        sendStateEvent()
        return nil
    }
    
    func onCancel(withArguments arguments: Any?) -> FlutterError? {
        _eventSink!(eventCallBackName)
      return nil
    }
  private func sendStateEvent() {
      guard let eventSink = self._eventSink else {
      return
    }
    eventSink(eventCallBackName)
 
  }
    
    func start() {
        ShieldCallbackManager.addObserver(self)
    }
    // Some callbacks are always triggered, and the return value must be checked // for whether the event is detected or not.
    func jailbreakStatus(_ jailbroken: Int32) {
        if jailbroken == 1 {
            eventCallBackName = "isJailbroken,true"
            self._eventSink?(eventCallBackName) // it is working
        }
    }
    
    func emulatorDetected() {
        eventCallBackName = "emulatorDetected,true"
        self._eventSink?(eventCallBackName) // it is working
    }
    
    func repackagingStatus(_ repackaged: Int32) {
        if repackaged == 1 {
             eventCallBackName = "isRepackaged,true"
            self._eventSink?(eventCallBackName) // it is working
        }
    }
    func screenRecordStatusChanged(_ isRecording: Bool) {
        if isRecording == true {
            eventCallBackName = "isRecording,true"
            self._eventSink?(eventCallBackName) // it is working
        }
    }
    
    func debuggerStatus(_ debugged: Int32) {
        if debugged == 1 {
            eventCallBackName = "isDebugged,true"
            self._eventSink?(eventCallBackName) // it is working
        }
    }
    // Some callbacks are only triggered when the event is detected, // and thus have no argument
    func screenshotDetected() {
        eventCallBackName = "isScreenshot,true"
        self._eventSink?(eventCallBackName) // it is working
    }
    
    func libraryInjectionPrevented() {
        eventCallBackName = "libraryInjectionPrevented,true"
        self._eventSink?(eventCallBackName) // it is working
    }
    
    func libraryInjectionDetected() {
        eventCallBackName = "libraryInjectionDetected,true"
        self._eventSink?(eventCallBackName) // it is working
    }
    
    func hookingFrameworksDetected() {
        eventCallBackName = "hookingFrameworksDetected,true"
        self._eventSink?(eventCallBackName) // it is working
    }
    
    func developerModeStatus(_ status: ShieldDeveloperModeStatus) {
        if status == .enabled {
            eventCallBackName = "DeveloperModeStatus,enabled"
            self._eventSink?(eventCallBackName) // it is working
        }else if status == .disabled {
            eventCallBackName = "DeveloperModeStatus,disabled"
            self._eventSink?(eventCallBackName) // it is working
        }else{
            eventCallBackName = "DeveloperModeStatus,notAvailable"
            self._eventSink?(eventCallBackName) // it is working
        }
    }
    
    func buildSpecificMessage(_ type: Int32, with msg: Data!) {
            
    }
}
