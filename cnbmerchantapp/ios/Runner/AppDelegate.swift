import Firebase
import Flutter
import GoogleMaps
import merchantcrypto
import UIKit


@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
    
    private let callbacks = Callbacks()
    
    override func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
    ) -> Bool {
        FirebaseApp.configure()
        let controller = (window?.rootViewController as! FlutterViewController)
        
        let eventChannel = FlutterEventChannel(name: "com.cnbmerchantapp.mobile/channel-runCheck", binaryMessenger: controller.binaryMessenger)
            eventChannel.setStreamHandler(callbacks)
        
        let cryptolibChannel = FlutterMethodChannel(name: "com.cnbmerchantapp.mobile/channel", binaryMessenger: controller.binaryMessenger)
        cryptolibChannel
            .setMethodCallHandler({
                [weak self] (call: FlutterMethodCall, result: FlutterResult) in
                guard let args = call.arguments as? [String: Any] else { return }
                let data = args["arg"] as! String
                switch call.method {
                case "encryption":
                    let encrypt = MerchantAuto.toEncryptAsync(jsonData: data)
                    return result(encrypt)
                case "decryption":
                    let decrypt = MerchantAuto.toDecryptAsync(keyData: data)
                    return result(decrypt)
                default:
                    result(FlutterMethodNotImplemented)
                }
            })
        
        GMSServices.provideAPIKey("AIzaSyBYMiuErGCEZLTfm25K-lPcFVtZTI18ZFc")
        self.frameWorkInit()
        GeneratedPluginRegistrant.register(with: self)
        
        return super.application(application, didFinishLaunchingWithOptions: launchOptions)
    }
   
    // Call this as early as possible from the app's // didFinishLaunchingWithOptions.
    public func frameWorkInit() {
        callbacks.start()
    }
}

