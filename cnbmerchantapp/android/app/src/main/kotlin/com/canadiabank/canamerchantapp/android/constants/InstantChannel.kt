
 class InstantChannel{
     companion object {
         const val cryptoChannel = "com.cnbmerchantapp.mobile/cryptoChannel"
         const val autofillChannel = "com.cnbmerchantapp.mobile/autofillChannel"
         const val startListeningUserConsent = "startListeningUserConsent"
         const val startListeningRetriever = "startListeningRetriever"
         const val stopListeningForCode = "stopListeningForCode"
     }
}