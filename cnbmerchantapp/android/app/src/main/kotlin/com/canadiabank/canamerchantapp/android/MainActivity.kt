package com.canadiabank.canamerchantapp.android
import android.app.Activity
import android.app.AlertDialog
import android.app.PendingIntent
import android.content.*
import android.os.Bundle
import android.util.Log
import com.canadiabank.canamerchantapp.android.MainActivity.Companion.events
import com.canadiabankplc.merchantappcryptolib.services.IMerchantCrypto
import com.canadiabankplc.merchantappcryptolib.services.MerchantCrypto
import com.google.android.gms.auth.api.phone.SmsRetriever
import com.google.android.gms.common.api.CommonStatusCodes
import com.google.android.gms.common.api.Status
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.EventChannel
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugins.GeneratedPluginRegistrant
import no.promon.shield.callbacks.CallbackData
import no.promon.shield.callbacks.CallbackManager
import no.promon.shield.callbacks.ExtendedObserver
import java.security.NoSuchAlgorithmException
import java.util.regex.Pattern
import javax.crypto.NoSuchPaddingException

class MainActivity : FlutterActivity() {
    private lateinit var cryptolibChannel: MethodChannel
    private lateinit var runAppCheckEventChannel: EventChannel
    private var alertDialog: AlertDialog? = null
    private val EVENT_CHANNEL = "com.cnbmerchantapp.mobile/sms_retriever"
    private val eventChannelName= "com.cnbmerchantapp.mobile/channel-runCheck"
    private lateinit var eventChannel: EventChannel

    companion object {
        private val TAG = "tMain"
        private val SMS_CONSENT_REQUEST = 100

        var events: EventChannel.EventSink? = null
    }


    override fun configureFlutterEngine(flutterEngine: FlutterEngine) {
        super.configureFlutterEngine(flutterEngine)
        var eoImpl = ExtendedObserverImplementation()
        runAppCheckEventChannel = EventChannel(flutterEngine.dartExecutor.binaryMessenger, eventChannelName)
        runAppCheckEventChannel.setStreamHandler(eoImpl)

        cryptolibChannel = MethodChannel(
            flutterEngine.dartExecutor.binaryMessenger, "com.cnbmerchantapp.mobile/channel"
        )
        cryptolibChannel.setMethodCallHandler(CryptoChannelHandler())

        eventChannel = EventChannel(flutterEngine.dartExecutor.binaryMessenger, EVENT_CHANNEL)
        eventChannel.setStreamHandler(AppStreamHandler(context = applicationContext))

        val observer: ExtendedObserver = eoImpl
        CallbackManager.addObserver(context, observer)
        GeneratedPluginRegistrant.registerWith(flutterEngine)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Check if the device is a tablet
        if (isTabletDevice()) {
            showTabletNotSupportedAlert()
            return
        }

        val intentFilter = IntentFilter(SmsRetriever.SMS_RETRIEVED_ACTION)
        registerReceiver(smsReceiver, intentFilter)
    }

    private fun isTabletDevice(): Boolean {
        return resources.configuration.smallestScreenWidthDp >= 600

    }

    private fun showTabletNotSupportedAlert() {
        alertDialog = AlertDialog.Builder(this)
            .setTitle("Tablet Not Supported")
            .setMessage("This app is not supported on tablet devices.")
            .setPositiveButton("Close") { _, _ -> finish() }
            .setCancelable(false)
            .create()

        alertDialog?.show()
    }

    override fun onDestroy() {
        try {
            unregisterReceiver(smsReceiver)
        } catch (e: IllegalArgumentException) {
            // Receiver not registered, do nothing
        }
        super.onDestroy()
    }

    private var smsReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            if (SmsRetriever.SMS_RETRIEVED_ACTION == intent?.action) {
                val extras: Bundle? = intent.extras
                val status: Status = extras?.get(SmsRetriever.EXTRA_STATUS) as Status
                when (status.statusCode) {
                    CommonStatusCodes.SUCCESS -> {
                        val consentIntent =
                            extras.getParcelable<Intent>(SmsRetriever.EXTRA_CONSENT_INTENT)
                        try {
                            val pendingIntent =
                                PendingIntent.getActivity(
                                    applicationContext,
                                    0,
                                    consentIntent,
                                    PendingIntent.FLAG_IMMUTABLE
                                )
                            startIntentSenderForResult(
                                pendingIntent.intentSender,
                                SMS_CONSENT_REQUEST,
                                null,
                                0,
                                0,
                                0,
                                null
                            )

                        } catch (e: ActivityNotFoundException) {
                            Log.e(TAG, "Exception: " + e.localizedMessage)
                        }

                    }
                    CommonStatusCodes.TIMEOUT -> {
                        Log.e(
                            TAG,
                            "${CommonStatusCodes.TIMEOUT}: " + CommonStatusCodes.getStatusCodeString(
                                CommonStatusCodes.TIMEOUT
                            )
                        )
                    }
                }
            }
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            SMS_CONSENT_REQUEST -> {
                if (resultCode == Activity.RESULT_OK && data != null) {
                    val message = data.getStringExtra(SmsRetriever.EXTRA_SMS_MESSAGE)
                    val otp = message?.let { extractOTP(it) };
                    events?.success(otp)
                }

            }
        }
    }

    private fun extractOTP(sms: String): String? {
        val pattern = Pattern.compile("(|^)\\d{6}")
        val matcher = pattern.matcher(sms)

        return if (matcher.find()) matcher.group(0) else ""
    }

}

class CryptoChannelHandler : MethodChannel.MethodCallHandler {

    private lateinit var merchantCrypto: IMerchantCrypto

    override fun onMethodCall(call: MethodCall, result: MethodChannel.Result) {
        try {
            merchantCrypto = MerchantCrypto()
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        } catch (e: NoSuchPaddingException) {
            e.printStackTrace()
        }

        val data = call.argument<String>("arg") as String
        when {
            call.method.equals("encryption") -> {
                val encryptValue = merchantCrypto.toEncryptAsync(data)
                result.success(encryptValue)
            }
            call.method.equals("decryption") -> {
                val decryptValue = merchantCrypto.toDecryptAsync(data)
                result.success(decryptValue)
            }
            else -> {
                result.notImplemented()
            }
        }
    }

}

class AppStreamHandler(private val context: Context) : EventChannel.StreamHandler {
    private var client = SmsRetriever.getClient(context)
    private var receiver: BroadcastReceiver? = null

    override fun onListen(arguments: Any?, _events: EventChannel.EventSink?) {
        if (_events == null) return

        events = _events
        client.startSmsUserConsent(null)
    }

    override fun onCancel(arguments: Any?) {
        context.unregisterReceiver(receiver)
        receiver = null
    }
}
