package com.canadiabank.canamerchantapp.android

import io.flutter.plugin.common.EventChannel
import no.promon.shield.callbacks.AdbStatusData
import no.promon.shield.callbacks.CallbackData
import no.promon.shield.callbacks.CallbackType
import no.promon.shield.callbacks.DebuggerData
import no.promon.shield.callbacks.DeveloperOptionsData
import no.promon.shield.callbacks.EmulatedInputData
import no.promon.shield.callbacks.EmulatorData
import no.promon.shield.callbacks.ExtendedObserver
import no.promon.shield.callbacks.FilesystemScanningData
import no.promon.shield.callbacks.FilesystemWatchingData
import no.promon.shield.callbacks.HookingFrameworksData
import no.promon.shield.callbacks.KeyboardData
import no.promon.shield.callbacks.NativeCodeHooksData
import no.promon.shield.callbacks.RepackagingData
import no.promon.shield.callbacks.RootingData
import no.promon.shield.callbacks.ScreenMirroringData
import no.promon.shield.callbacks.ScreenreaderData
import no.promon.shield.callbacks.TaskHijackingData
import no.promon.shield.callbacks.UntrustedSourceAppData
import no.promon.shield.callbacks.VirtualSpaceAppData

class ExtendedObserverImplementation : EventChannel.StreamHandler, ExtendedObserver {
    private lateinit var eventCallBackName: String
    companion object {
        var eoEventChannel: EventChannel.EventSink? = null
    }
    override fun onListen(args: Any?, _events: EventChannel.EventSink) {
        eoEventChannel = _events
        eoEventChannel?.success(eventCallBackName)
    }
    override fun onCancel(args: Any?) {
        eoEventChannel = null
        eventCallBackName = ""
    }

    override fun handleCallback(callData: CallbackData) {
        when (callData.callbackType) {
            CallbackType.ADB_STATUS -> {
                val callbackData = callData as AdbStatusData
                eventCallBackName = "isAdbActive,${callbackData.isAdbActive}"
            }
            CallbackType.DEBUGGER -> {
                val callbackData = callData as DebuggerData
                eventCallBackName = "isRunningUnderDebugger,${callbackData.isRunningUnderDebugger}"
            }
            CallbackType.DEVELOPER_OPTIONS -> {
                val callbackData = callData as DeveloperOptionsData
                eventCallBackName = "isDeveloperOptionsEnabled,${callbackData.isDeveloperOptionsEnabled}"
            }
            CallbackType.EMULATED_INPUT -> {
                val callbackData = callData as EmulatedInputData
                eventCallBackName = "isInputEmulated,${callbackData.isInputEmulated}"
            }
            CallbackType.EMULATOR -> {
                val callbackData = callData as EmulatorData
                eventCallBackName = "isRunningOnEmulator,${callbackData.isRunningOnEmulator}"
            }
            CallbackType.FILESYSTEM_SCANNING -> {
                val callbackData = callData as FilesystemScanningData
                eventCallBackName = "isSuDetected,${callbackData.isSuDetected},isSuidOrSgidDetected,${callbackData.isSuidOrSgidDetected},isSuspiciousFileDetected,${callbackData.isSuspiciousFileDetected},"
            }
            CallbackType.FILESYSTEM_WATCHING -> {
                val callbackData = callData as FilesystemWatchingData
                eventCallBackName = "isSuDetected,${callbackData.isSuDetected},isSuidOrSgidDetected,${callbackData.isSuidOrSgidDetected},suspiciousFileName,${callbackData.suspiciousFileName}"
            }
            CallbackType.HOOKING_FRAMEWORKS -> {
                val callbackData = callData as HookingFrameworksData
                eventCallBackName = "lastUsedDetectionMethodCode,${callbackData.lastUsedDetectionMethodCode},areHookingFrameworksPresent,${callbackData.areHookingFrameworksPresent()}"
            }
            CallbackType.KEYBOARD -> {
                val callbackData = callData as KeyboardData
                eventCallBackName = "isKeyboardUntrusted,${callbackData.isKeyboardUntrusted},keyboardSignature,${callbackData.keyboardSignature},keyboardAppLabel,${callbackData.keyboardAppLabel},keyboardPackageName,${callbackData.keyboardPackageName},keyboardSignerName,${callbackData.keyboardSignerName},keyboardVersionName,${callbackData.keyboardVersionName}"
            }
            CallbackType.NATIVE_CODE_HOOKS -> {
                val callbackData = callData as NativeCodeHooksData
                eventCallBackName = "areNativeCodeHooksPresent,${callbackData.areNativeCodeHooksPresent()}"
            }
            CallbackType.REPACKAGING -> {
                val callbackData = callData as RepackagingData
                eventCallBackName = "isRepackaged,${callbackData.isRepackaged}"
            }
            CallbackType.ROOTING -> {
                val callbackData = callData as RootingData
                eventCallBackName = "isDeviceCertainlyRooted,${callbackData.isDeviceCertainlyRooted},rootingProbability,${callbackData.rootingProbability},lastUsedDetectionMethodCode,${callbackData.lastUsedDetectionMethodCode}"
            }
            CallbackType.SCREEN_MIRRORING -> {
                val callbackData = callData as ScreenMirroringData
                eventCallBackName = "isScreenMirroringBlocked,${callbackData.isScreenMirroringBlocked},isScreenMirroringDetected,${callbackData.isScreenMirroringDetected}"
            }
            CallbackType.SCREENREADER -> {
                val callbackData = callData as ScreenreaderData
                eventCallBackName = "isUntrustedScreenreaderPresent,${callbackData.isUntrustedScreenreaderPresent},untrustedScreenreaderCount,${callbackData.untrustedScreenreaderCount}"
            }
            CallbackType.TASK_HIJACKING -> {
                val callbackData = callData as TaskHijackingData
                eventCallBackName = "intentData,${callbackData.intentData},offendingTaskAffinity,${callbackData.offendingTaskAffinity},offendingAppActivity,${callbackData.offendingAppActivity}"
            }
            CallbackType.UNTRUSTED_SOURCE_APP -> {
                val callbackData = callData as UntrustedSourceAppData
                eventCallBackName = "isUntrustedSourceAppPresent,${callbackData.isUntrustedSourceAppPresent},untrustedSourceAppCount,${callbackData.untrustedSourceAppCount}"
            }
            CallbackType.VIRTUAL_SPACE_APP -> {
                val callbackData = callData as VirtualSpaceAppData
                eventCallBackName = "isAppInVirtualSpace,${callbackData.isAppInVirtualSpace},virtualSpaceAppAppLabel,${callbackData.virtualSpaceAppAppLabel},virtualSpaceAppSignature,${callbackData.virtualSpaceAppSignature},virtualSpaceAppPackageName,${callbackData.virtualSpaceAppPackageName},virtualSpaceAppSignerName,${callbackData.virtualSpaceAppSignerName},virtualSpaceAppVersionName,${callbackData.virtualSpaceAppVersionName}"
            }
            else -> {

            }
        }
        eoEventChannel?.success(eventCallBackName)
    }
}