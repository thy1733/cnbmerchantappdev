abstract class IChannelProvider{
  Future<String> toEncryptionAsync(dynamic data);
  Future<String> toDescryptionAsync(String data);
}