import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/services.dart';

class ChannelProvider implements IChannelProvider {
  late MethodChannel _platform;

  ChannelProvider({String channelName = MethodChannelName.merchantChannelAuto}) {
    _platform = MethodChannel(channelName);
  }

  @override
  Future<String> toDescryptionAsync(String data) async{
    var result = "";
    try {
      final String response = await _platform.invokeMethod(InvokeMethodName.decryption,{'arg': data});
      result = response;
    } on PlatformException catch (e) {
      result = "Failed to Invoke: '${e.message}'.";
    }
    return result;
  }

  @override
  Future<String> toEncryptionAsync(data) async{
    var result = "";
    try {
      final String response = await _platform.invokeMethod(InvokeMethodName.encryption,{'arg': data});
      result = response;
    } on PlatformException catch (e) {
      result = "Failed to Invoke: '${e.message}'.";
    }
    return result;
  }

}