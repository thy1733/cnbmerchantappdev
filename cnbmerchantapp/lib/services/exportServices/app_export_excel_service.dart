import 'dart:convert';
import 'dart:io';

import 'package:cnbmerchantapp/core.dart';
import 'package:cnbmerchantapp/helpers/components/snackBars/xcore.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:syncfusion_flutter_xlsio/xlsio.dart' as xlsio;
import 'package:path_provider/path_provider.dart' as path_provider;
import 'package:open_file/open_file.dart' as open_file;

class AppExportExcelService {
  final DateFormat dateFormatter = DateFormat("dd-MM-yyyy");

  final xlsio.Workbook workbook = xlsio.Workbook();

  Future<List<int>> _readImageData(String name) async {
    final ByteData data = await rootBundle.load(name);
    return data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);
  }

  Future<void> _saveAndLaunchFile(List<int> bytes, String fileName) async {
    final Directory directory =
        await path_provider.getApplicationSupportDirectory();
    String path = directory.path;
    final File file = File("$path/$fileName");
    await file.writeAsBytes(bytes, flush: true);
    if (Platform.isIOS) {
      SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark);
      await open_file.OpenFile.open('$path/$fileName').then(
        (_) => SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light),
      );
    }
    if (Platform.isAndroid) {
      if (await checkPermissionAndroidAsync()) {
        SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark);
        await open_file.OpenFile.open('$path/$fileName').then(
          (_) =>
              SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light),
        );
      }
    }
  }

  Future<bool> checkPermissionAndroidAsync() async {
    var status = await Permission.manageExternalStorage.status;
    if (!status.isGranted) {
      var result = await Permission.manageExternalStorage.request();
      if (result.isDenied) {
        Get.showSnackbar(CSnackBar.appGetSnackBar(
            false, 'CantOpen'.tr, 'PermissonDenied'.tr, 3));
        return false;
      } else if (!result.isGranted) {
        await openAppSettings();
        return false;
      }
    }
    return true;
  }

  Future<void> exportTransactionReport({
    required String fileName,
    required String period,
    required DownloadReportTrxOutputModel data,
  }) async {
    final List<String> headerLabels = [
      "Date/Time",
      "Transaction ID",
      "Merchant ID",
      "Outlet",
      "Account Name",
      "Account Number",
      "Trxn. Amount",
      "Currency",
      "Customer Name",
      "Customer Bank Name",
      "Customer Account",
      "Payment Method",
      "Transaction Type",
      "Processed By",
      "Hash #",
      "Notes",
    ];

    int maxRow = 0;
    int startedRow = 2;
    String currency = "";

    // Create sheet
    final xlsio.Worksheet sheet = workbook.worksheets[0];

    // Style
    final xlsio.Style globalStyle = workbook.styles.add("globalStyle");
    globalStyle.fontSize = 12;
    globalStyle.fontColor = "#000000";
    globalStyle.hAlign = xlsio.HAlignType.left;

    // Title
    final xlsio.Style titleStyle = workbook.styles.add("Title");
    titleStyle.fontSize = 16;
    titleStyle.bold = true;
    titleStyle.hAlign = xlsio.HAlignType.center;
    titleStyle.vAlign = xlsio.VAlignType.center;
    sheet.showGridlines = true;
    sheet.getRangeByName("A1").rowHeight = 70;
    sheet.getRangeByName("A1").text =
        "CANAMERCHANT REPORT \nRun date: ${dateFormatter.format(DateTime.now())} \n $period";
    sheet.getRangeByName("A1:P1").merge();

    sheet.getRangeByName("A1").cellStyle = titleStyle;

    // CNB Logo
    final String canadiaBankImage =
        base64.encode(await _readImageData(PngAppAssets.cnbLogoFull));

    xlsio.Picture picture = sheet.pictures.addBase64(1, 1, canadiaBankImage);
    picture.width = 240;
    picture.height = 83;

    // Set Header
    for (var i = 0; i < headerLabels.length; i++) {
      String alphabet = String.fromCharCode(i + 65);
      String headerColumn = headerLabels[i];
      sheet.getRangeByName("$alphabet$startedRow").text = headerColumn;
    }

    if (data.items.isNotEmpty) {
      for (var i = 0; i <= data.items.length - 1; i++) {
        maxRow = startedRow + i + 1;
        int dataRow = i + startedRow + 1;

        var item = data.items.elementAt(i);

        sheet.getRangeByName("A$dataRow").text =
            DateFormat("MM/dd/yyyy hh:mm a").format(item.transactionDate);
        sheet.getRangeByName("B$dataRow").text = item.transactionId;
        sheet.getRangeByName("C$dataRow").text = item.merchantId;
        sheet.getRangeByName("D$dataRow").text = item.outletName;
        sheet.getRangeByName("E$dataRow").text = item.merchantAccName;
        sheet.getRangeByName("F$dataRow").text =
            item.merchantAccount.maskAccountNumber();
        sheet.getRangeByName("G$dataRow").text = item.amount.toString();
        sheet.getRangeByName("H$dataRow").text = item.currency;
        sheet.getRangeByName("I$dataRow").text = item.customerAccName;
        sheet.getRangeByName("J$dataRow").text = item.customerBankName;
        sheet.getRangeByName("K$dataRow").text =
            item.customerAccount.maskAccountNumber();
        sheet.getRangeByName("L$dataRow").text = item.paymentMethodName;
        sheet.getRangeByName("M$dataRow").text = item.transactionType;
        sheet.getRangeByName("N$dataRow").text = item.paymentReceivedBy;
        sheet.getRangeByName("O$dataRow").text = item.bakongHash;
        sheet.getRangeByName("P$dataRow").text = item.remark;
        currency = item.currency;
      }
      sheet.getRangeByName("A$startedRow:P$maxRow").autoFitColumns();
      sheet.getRangeByName("B$startedRow:P${maxRow + 3}").cellStyle =
          globalStyle;
    }

    sheet.getRangeByName("A${maxRow + 2}").text = "Total Sale";
    sheet.getRangeByName("A${maxRow + 3}").text = "Total Refund";

    sheet.getRangeByName("B${maxRow + 2}").text =
        data.totalAmountSale.toThousandSeparatorString(d2: isUSDCurrency);
    sheet.getRangeByName("B${maxRow + 3}").text =
        data.totalAmountRefund.toThousandSeparatorString(d2: isUSDCurrency);

    sheet.getRangeByName("C${maxRow + 2}").text = currency;
    sheet.getRangeByName("C${maxRow + 3}").text = currency;

    final List<int> bytes = workbook.saveAsStream();
    workbook.dispose();
    _saveAndLaunchFile(bytes, "$fileName.xlsx");
  }
}
