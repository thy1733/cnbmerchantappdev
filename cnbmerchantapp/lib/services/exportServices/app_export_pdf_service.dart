import 'dart:io';

import 'package:cnbmerchantapp/core.dart';
import 'package:cnbmerchantapp/helpers/components/snackBars/xcore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:syncfusion_flutter_pdf/pdf.dart';
import 'package:path_provider/path_provider.dart' as path_provider;
import 'package:open_file/open_file.dart' as open_file;

class AppExportPdfService {
  // final xlsio.Workbook workbook = xlsio.Workbook();
  final PdfDocument document = PdfDocument();
  final DateFormat dateFormatter = DateFormat("dd-MM-yyyy");
  final PdfColor blackColor = PdfColor(0, 0, 0);

  Future<List<int>> _readImageData(String name) async {
    final ByteData data = await rootBundle.load(name);
    return data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);
  }

  Future<void> _saveAndLaunchFile(List<int> bytes, String fileName) async {
    final Directory directory =
        await path_provider.getApplicationSupportDirectory();
    String path = directory.path;
    final File file = File("$path/$fileName");
    await file.writeAsBytes(bytes, flush: true);
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark);

    if (Platform.isIOS) {
      SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark);
      await open_file.OpenFile.open('$path/$fileName').then(
        (_) => SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light),
      );
    }
    if (Platform.isAndroid) {
      if (await checkPermissionAndroidAsync()) {
        SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark);
        await open_file.OpenFile.open('$path/$fileName').then(
          (_) =>
              SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light),
        );
      }
    }
  }

  Future<bool> checkPermissionAndroidAsync() async {
    var status = await Permission.manageExternalStorage.status;
    if (!status.isGranted) {
      var result = await Permission.manageExternalStorage.request();
      if (result.isDenied) {
        Get.showSnackbar(CSnackBar.appGetSnackBar(
            false, 'CantOpen'.tr, 'PermissonDenied'.tr, 3));
        return false;
      } else if (!result.isGranted) {
        await openAppSettings();
        return false;
      }
    }
    return true;
  }

  Future<void> exportTransactionReport({
    required String fileName,
    required String period,
    required DownloadReportTrxOutputModel data,
  }) async {
    try {
      PdfPageSettings pageSettings = document.pageSettings;
      pageSettings.setMargins(12, 50, 12, 25);
      final PdfPage page = document.pages.add();
      Size pageSize = PdfPageSize.a4;
      pageSettings.size = pageSize;

      PdfLayoutResult headerResult = await drawHeader(page, pageSize, period);

      addFooter(document);

      drawTable(page, pageSize, headerResult, data);

      final List<int> bytes = document.saveSync();
      document.dispose();
      await _saveAndLaunchFile(bytes, "$fileName.pdf");
    } catch (e) {
      e.printError();
    }
  }

  // Header
  Future<PdfLayoutResult> drawHeader(
    PdfPage page,
    Size pageSize,
    String period,
  ) async {
    final cnbImageData = await _readImageData(PngAppAssets.cnbLogoFull);
    final cnbImage = PdfBitmap(cnbImageData);
    const double cnbImageHeight = 50;
    page.graphics
        .drawImage(cnbImage, const Rect.fromLTWH(0, 0, 144, cnbImageHeight));

    final PdfFont contentFont =
        PdfStandardFont(PdfFontFamily.helvetica, 12, style: PdfFontStyle.bold);
    String textHeader =
        "CANAMERCHANT REPORT \nRun date: ${dateFormatter.format(DateTime.now())} \n$period";
    final Size contentSize = contentFont.measureString(textHeader);

    final textHeaderElement =
        PdfTextElement(text: textHeader, font: contentFont).draw(
      page: page,
      bounds: Rect.fromLTWH(
        pageSize.width - (contentSize.width + 30),
        0,
        contentSize.width + 30,
        pageSize.height - 120,
      ),
    )!;

    return textHeaderElement;
  }

  void drawTable(
    PdfPage page,
    Size pageSize,
    PdfLayoutResult result,
    DownloadReportTrxOutputModel data,
  ) {
    final PdfGrid grid = PdfGrid();

    final List<String> pdfColumns = [
      "Date/Time",
      "Transaction ID",
      "Outlet",
      "Trxn. Amount",
      "Currency",
      "Customer Name",
      "Customer Account",
      "Transaction Type",
    ];
    String currency = "";

    grid.columns.add(count: pdfColumns.length);
    final PdfGridRow headerRow = grid.headers.add(1)[0];
    // headerRow.style.backgroundBrush = PdfSolidBrush(primaryColor);
    // headerRow.style.textBrush = PdfBrushes.white;

    for (var i = 0; i < pdfColumns.length; i++) {
      headerRow.cells[i].value = pdfColumns[i];
    }
    for (var j = 0; j < data.items.length; j++) {
      final item = data.items[j];

      final PdfGridRow row = grid.rows.add();
      final PdfGridCellCollection rowCells = row.cells;
      rowCells[0].value =
          DateFormat("MM/dd/yyyy hh:mm a").format(item.transactionDate);
      rowCells[1].value = item.transactionId;
      rowCells[2].value = item.outletName;
      rowCells[3].value =
          item.amount.toThousandSeparatorString(d2: isUSDCurrency);
      rowCells[4].value = item.currency;
      rowCells[5].value = item.customerAccName;
      rowCells[6].value = item.customerAccount.maskAccountNumber();
      rowCells[7].value = item.transactionType;
      currency = item.currency;
    }
    grid.applyBuiltInStyle(PdfGridBuiltInStyle.plainTable1);
    final gridColumns = grid.columns;
    gridColumns[0].width = 60;
    gridColumns[3].width = 60;
    gridColumns[4].width = 50;
    final PdfGridCellCollection headerRowCells = headerRow.cells;
    for (int i = 0; i < headerRowCells.count; i++) {
      headerRowCells[i].style.cellPadding =
          PdfPaddings(bottom: 5, left: 5, right: 5, top: 5);
      headerRowCells[i].style.stringFormat?.lineAlignment =
          PdfVerticalAlignment.middle;
      headerRowCells[i].style.stringFormat =
          PdfStringFormat(alignment: PdfTextAlignment.center);
    }

    for (int i = 0; i < grid.rows.count; i++) {
      final PdfGridRow row = grid.rows[i];
      final PdfGridCellCollection rowCells = row.cells;
      for (int j = 0; j < rowCells.count; j++) {
        final PdfGridCell cell = rowCells[j];
        cell.stringFormat.alignment = PdfTextAlignment.center;
        cell.stringFormat.lineAlignment = PdfVerticalAlignment.middle;
        cell.style.cellPadding =
            PdfPaddings(bottom: 5, left: 5, right: 5, top: 5);
      }
    }

    drawTotal(
      page,
      pageSize,
      result,
      grid,
      pdfColumns.length,
      "$currency ${data.totalAmountSale.toThousandSeparatorString(d2: isUSDCurrency)} ",
      "$currency ${data.totalAmountRefund.toThousandSeparatorString(d2: isUSDCurrency)} ",
    );
  }

  void drawTotal(
    PdfPage page,
    Size pageSize,
    PdfLayoutResult result,
    PdfGrid grid,
    int columnLenght,
    String totalSale,
    String totalRefund,
  ) {
    Rect? totalSaleTitleCellBounds;
    grid.beginCellLayout = (Object sender, PdfGridBeginCellLayoutArgs args) {
      final PdfGrid grid = sender as PdfGrid;
      if (args.cellIndex == grid.columns.count - columnLenght) {
        totalSaleTitleCellBounds = args.bounds;
      }
    };

    PdfLayoutResult gridResult = grid.draw(
        page: page,
        bounds:
            Rect.fromLTWH(0, result.bounds.height + 30, 0, pageSize.height))!;
    final totalSaleTitleResult = PdfTextElement(
            text: "Total Sale:  ",
            font: PdfStandardFont(PdfFontFamily.helvetica, 10,
                style: PdfFontStyle.bold))
        .draw(
            page: gridResult.page,
            bounds: Rect.fromLTWH(totalSaleTitleCellBounds!.left,
                gridResult.bounds.bottom + 20, 0, 0))!;

    PdfTextElement(
            text: totalSale,
            font: PdfStandardFont(PdfFontFamily.helvetica, 9,
                style: PdfFontStyle.regular))
        .draw(
            page: gridResult.page,
            bounds: Rect.fromLTWH(totalSaleTitleResult.bounds.right + 20,
                totalSaleTitleResult.bounds.top, 0, 0))!;

    final totalRefundResult = PdfTextElement(
            text: "Total Refund:",
            font: PdfStandardFont(PdfFontFamily.helvetica, 9,
                style: PdfFontStyle.regular))
        .draw(
            page: gridResult.page,
            bounds: Rect.fromLTWH(totalSaleTitleResult.bounds.left,
                totalSaleTitleResult.bounds.bottom + 10, 0, 0))!;

    PdfTextElement(
            text: totalRefund,
            font: PdfStandardFont(PdfFontFamily.helvetica, 9,
                style: PdfFontStyle.regular))
        .draw(
            page: gridResult.page,
            bounds: Rect.fromLTWH(totalRefundResult.bounds.right + 18,
                totalRefundResult.bounds.top, 0, 0))!;
  }

  void addFooter(PdfDocument document) {
    //Create the footer with specific bounds
    PdfPageTemplateElement footerTemplate = PdfPageTemplateElement(
        Rect.fromLTWH(0, 0, document.pageSettings.size.width, 50));

    //Create the page number field
    PdfPageNumberField pageNumber = PdfPageNumberField(
        font: PdfStandardFont(PdfFontFamily.helvetica, 8),
        brush: PdfSolidBrush(blackColor));

    pageNumber.numberStyle = PdfNumberStyle.numeric;

    PdfPageCountField count = PdfPageCountField(
        font: PdfStandardFont(PdfFontFamily.helvetica, 8),
        brush: PdfSolidBrush(blackColor));

    count.numberStyle = PdfNumberStyle.numeric;

    PdfCompositeField compositeField = PdfCompositeField(
        font: PdfStandardFont(PdfFontFamily.helvetica, 8),
        brush: PdfSolidBrush(blackColor),
        text:
            "Canadia Bank | +855 23 868 222 | www.canadiabank.com.kh - Page: {0} / {1}",
        fields: <PdfAutomaticField>[
          pageNumber,
          count,
        ]);
    compositeField.bounds = footerTemplate.bounds;

    final compositeFieldYAxis =
        50 - PdfStandardFont(PdfFontFamily.helvetica, 8).height;

    compositeField.draw(
        footerTemplate.graphics, Offset(0, compositeFieldYAxis));

    document.template.bottom = footerTemplate;
  }

  Future<PdfFont> getFont(TextStyle style) async {
    //Get the external storage directory
    Directory directory = await path_provider.getApplicationSupportDirectory();
    //Create an empty file to write the font data
    File file = File('${directory.path}/${style.fontFamily}.ttf');
    if (!file.existsSync()) {
      List<FileSystemEntity> entityList = directory.listSync();
      for (FileSystemEntity entity in entityList) {
        if (entity.path.contains(style.fontFamily!)) {
          file = File(entity.path);
          break;
        }
      }
    }
    List<int>? fontBytes;

    if (file.existsSync()) {
      fontBytes = await file.readAsBytes();
    }
    if (fontBytes != null && fontBytes.isNotEmpty) {
      return PdfTrueTypeFont(fontBytes, 12);
    } else {
      return PdfStandardFont(PdfFontFamily.helvetica, 12);
    }
  }
}
