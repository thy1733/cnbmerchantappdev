import 'dart:typed_data';

abstract class IImageSaveService {
  Future<bool> imageFromStream(
      {required Uint8List image,
      String? fileName,
      String? albumName,
      bool toDcim = false,
      Map<String, String>? headers});

}
