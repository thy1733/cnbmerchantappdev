import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:io';
import 'package:gallery_saver/gallery_saver.dart';
import 'dart:typed_data';

import 'package:permission_handler/permission_handler.dart';

import '../../helpers/components/snackBars/xcore.dart';

class ImageSaveService extends IImageSaveService {
  @override
  Future<bool> imageFromStream(
      {required Uint8List image,
      String? fileName,
      String? albumName,
      bool toDcim = false,
      Map<String, String>? headers}) async {
    try {
      fileName ??= '${DateTime.now().millisecond}';

      final String dir = (await getApplicationDocumentsDirectory()).path;

      // var ishasPermission = await checkPermissionAndroidAsync();
      // check permission for save to storage
      var ishasPermission = GetPlatform.isAndroid
          ? await checkPermissionAndroidAsync()
          : GetPlatform.isIOS
              ? await checkPermissioniOSAsync()
              : false;
      if (ishasPermission == false) return false;

      final String fullPath = '$dir/$fileName.png';
      File capturedFile = File(fullPath);
      await capturedFile.writeAsBytes(image);
      var result = await GallerySaver.saveImage(capturedFile.path,
          albumName: albumName, toDcim: toDcim, headers: headers);
      if (result == null) return false;
      return result;
    } catch (e) {
      return false;
    }
  }

  Future<bool> checkPermissionAndroidAsync() async {
    var status = await Permission.storage.status;
    if (!status.isGranted) {
      var result = await Permission.storage.request();
      if (result.isDenied) {
        Get.showSnackbar(CSnackBar.appGetSnackBar(
            false, 'CantSaveImage'.tr, 'PermissonDenied'.tr, 3));
        return false;
      } else if (!result.isGranted) {
        await openAppSettings();
        return false;
      }
    }
    return true;
  }

  Future<bool> checkPermissioniOSAsync() async {
    var status = await Permission.photos.status;
    if (!status.isGranted) {
      var result = await Permission.photos.request();
      if (result.isDenied) {
        Get.showSnackbar(CSnackBar.appGetSnackBar(
            false, 'CantSaveImage'.tr, 'PermissonDenied'.tr, 3));
        return false;
      } else if (!result.isGranted) {
        await openAppSettings();
        return false;
      }
    }
    return true;
  }
}
