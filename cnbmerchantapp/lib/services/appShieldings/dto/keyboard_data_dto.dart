class KeyboardData {
  late bool isKeyboardUntrusted;
  late String getKeyboardPackageName;
  late String getKeyboardVersionName;
  late String getKeyboardAppLabel;
  late String getKeyboardSignerName;
  late String getKeyboardSignature;
}