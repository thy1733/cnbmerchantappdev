class ScreenreaderData {
  late bool isUntrustedScreenreaderPresent;
  late int getUntrustedScreenreaderCount;
  late String getUntrustedScreenreaderPackageName;
  late String getUntrustedScreenreaderVersionName;
  late String getUntrustedScreenreaderAppLabel;
  late String getUntrustedScreenreaderSignerName;
  late String getUntrustedScreenreaderSignature;
}