class RootingData {
  late bool isDeviceCertainlyRooted;
  late int getRootingProbability;
  late int getLastUsedDetectionMethodCode;
  late int getLastUsedHeuristicMethodCode;
}