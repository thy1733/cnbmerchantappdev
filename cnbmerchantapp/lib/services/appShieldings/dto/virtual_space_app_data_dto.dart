class VirtualSpaceAppData {
  late bool isAppInVirtualSpace;
  late String getVirtualSpaceAppPackageName;
  late String getVirtualSpaceAppVersionName;
  late String getVirtualSpaceAppAppLabel;
  late String getVirtualSpaceAppSignerName;
  late String getVirtualSpaceAppSignature;
}