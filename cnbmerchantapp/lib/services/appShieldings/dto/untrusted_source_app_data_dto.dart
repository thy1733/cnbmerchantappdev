class UntrustedSourceAppData {
  late bool isUntrustedSourceAppPresent;
  late int getUntrustedSourceAppCount;
  late String getUntrustedSourceAppPackageName;
  late String getUntrustedSourceAppVersionName;
  late String getUntrustedSourceAppLabel;
  late String getUntrustedSourceAppSignerName;
  late String getUntrustedSourceAppSignature;
  late String getUntrustedSourceAppInstallerPackageName;
  late String getUntrustedSourceAppInstallerSignerName;
  late String getUntrustedSourceAppInstallerSignature;
}