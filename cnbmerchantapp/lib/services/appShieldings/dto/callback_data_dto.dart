import 'package:cnbmerchantapp/core.dart';

class CallBackDataPromon {
  late AdbStatusData adbStatusData;
  late DebuggerData debuggerData;
  late DeveloperOptionsData developerOptionsData;
  late EmulatedInputData emulatedInputData;
  late EmulatorData emulatorData;
  late FilesystemScanningData filesystemScanningData;
  late FilesystemWatchingData filesystemWatchingData;
  late HookingFrameworksData hookingFrameworksData;
  late KeyboardData keyboardData;
  late NativeCodeHooksData nativeCodeHooksData;
  late RepackagingData repackagingData;
  late RootingData rootingData;
  late ScreenMirroringData screenMirroringData;
  late ScreenreaderData screenreaderData;
  late TaskHijackingData taskHijackingData;
  late UntrustedSourceAppData untrustedSourceAppData;
  late VirtualSpaceAppData virtualSpaceAppData;
} 