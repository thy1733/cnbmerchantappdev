
import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/services.dart';

class AppShieldingProvider implements IAppShieldingProvider {

  @override
  Future<void> listeningRunAppCheckingAsync(String eventName,void Function(dynamic)? onData) async {
    if(eventName.isEmpty) return;
    var runCheckEvent = EventChannel(eventName);
    runCheckEvent.receiveBroadcastStream().listen((onData));
  }
  
  @override
  Future<void> removeListener(String eventName) async{
    var runCheckEvent = EventChannel(eventName);
    runCheckEvent.receiveBroadcastStream().listen(null);
  }
}