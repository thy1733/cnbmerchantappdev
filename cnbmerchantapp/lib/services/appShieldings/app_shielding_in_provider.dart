
abstract class IAppShieldingProvider{
   Future<void> listeningRunAppCheckingAsync(String eventName,void Function(dynamic)? onData);
   Future<void> removeListener(String eventName);
}