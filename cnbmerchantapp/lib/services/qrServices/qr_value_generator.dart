import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

class QrValueGenerator {
  //#region PROPERTIES

  final _tagLenghtFormat = NumberFormat("#00", "en_US");
  final _amountFormat = NumberFormat("##0.##", "en_US");
  final _tagWithSubTag = <String>[
    '05',
    '07',
    '29',
    '30',
    '62',
    '68',
  ];

  String get deviceSerial => GetPlatform.isIOS
      ? DeviceInfoTempInstant.deviceInfo.identifier
      : DeviceInfoTempInstant.deviceInfo.identifier.replaceAll(".", "-");

  //#endregion

  /// This function is include check CRC validation
  ///
  /// if validation fails it return null
  List<KHQRTagOutputDto>? tagToModelListMapper(String qrdata) {
    if (qrdata.isEmpty || qrdata.length < 5) return null;
    if (!validateCrc(qrdata)) return null;
    return getTagDataList(qrdata);
  }

  List<KHQRTagOutputDto> getTagDataList(String qrvalue) {
    List<KHQRTagOutputDto> result = [];
    try {
      while (qrvalue.length > 4) {
        var tagData = getTagData(qrvalue);
        if (tagData.inTagLenght > tagData.tagValue.length) {
          break;
        } // to protect invalid qr

        // check sub tag
        if (tagData.tagValue.length > 4) {
          if (_tagWithSubTag.contains(tagData.tagName)) {
            var subTag = getTagDataList(tagData.tagValue);
            if (subTag.isNotEmpty) tagData.subtag.addAll(subTag);
          }
        }
        result.add(tagData);
        qrvalue = qrvalue.substring(tagData.tagData.length);
      }
    } catch (e) {
      e.printError();
    }
    return result;
  }

  KHQRTagOutputDto getTagData(String qrvalue) {
    var tagName = qrvalue.substring(0, 2);
    var tagLenghtString = qrvalue.substring(2, 4);
    var tagLenght = int.tryParse(tagLenghtString) ?? 0;
    var tagValue = qrvalue.substring(4, 4 + tagLenght);
    var tagData = tagName + tagLenghtString + tagValue;
    return KHQRTagOutputDto(
      tagName: tagName,
      inTagLenght: tagLenght,
      tagValue: tagValue,
      tagData: tagData,
    );
  }

  bool validateCrc(String data) {
    var dataCrc = data.substring(data.length - 4);
    var dataValue = data.substring(0, data.length - 4);
    var checkSumCrc = crc16(dataValue);
    return dataCrc == checkSumCrc;
  }

  String tag30({
    required String merchantId,
    required String mcc,
    required String currencyCode,
    required double amount,
    required String merchantName,
    required String merchantLocation,
    required String businessId,
    required String outletId,
    required String cashierId,
    required String txnId,
  }) {
    String result = '';

    // TAG 00
    var tag00 = QrKeyConst.payloadIndicator;

    // TAG 01
    var tag01 = amount == 0
        ? QrKeyConst.staticInitationMethod
        : QrKeyConst.dynamicInitationMethod;

    // TAG 30
    final subtag3000 =
        "00${_getTagLenght(QrKeyConst.uniqIdentifier)}${QrKeyConst.uniqIdentifier}";
    final subtag3001 = "01${_getTagLenght(merchantId)}$merchantId";
    final subtag3002 =
        "02${_getTagLenght(QrKeyConst.acquiringBankMerchant)}${QrKeyConst.acquiringBankMerchant}";

    final subtag30 = "$subtag3000$subtag3001$subtag3002";
    final tag30 = "30${_getTagLenght(subtag30)}$subtag30";

    // TAG 52
    final tag52 = "52${_getTagLenght(mcc)}$mcc";

    // TAG 53
    final tag53 = "53${_getTagLenght(currencyCode)}$currencyCode";

    // TAG 54
    final formatedAmount = _amountFormat.format(amount);
    final tag54 =
        amount == 0 ? '' : "54${_getTagLenght(formatedAmount)}$formatedAmount";

    // TAG 58
    final tag58 =
        "58${_getTagLenght(QrCountryCode.cambodia)}${QrCountryCode.cambodia}";

    // TAG 59
    final tag59 = "59${_getTagLenght(merchantName)}$merchantName";

    // TAG 60
    final tag60 = "60${_getTagLenght(merchantLocation)}$merchantLocation";

    // TAG 62 Merchant Info
    final subTag6200 =
        '00${_getTagLenght(QrKeyConst.appCode)}${QrKeyConst.appCode}';
    final subTag6201 = '01${_getTagLenght(businessId)}$businessId';
    final subTag6202 = '02${_getTagLenght(outletId)}$outletId';
    final subTag6205 = '05${_getTagLenght(cashierId)}$cashierId';
    final subTag6268Value = "$subTag6200$subTag6201$subTag6202$subTag6205";
    final subTag6268 = "68${_getTagLenght(subTag6268Value)}$subTag6268Value";
    final tag62 = "62${_getTagLenght(subTag6268)}$subTag6268";

    // tag 98
    final subtag98 = '00${_getTagLenght(deviceSerial)}${deviceSerial}01${_getTagLenght(txnId)}$txnId';
    final tag98 = '98${_getTagLenght(subtag98)}$subtag98';

    // tag 99
    final timeStamp = DateTime.now().millisecondsSinceEpoch;
    final subtag9900 = "00${_getTagLenght("$timeStamp")}$timeStamp";
    final tag99 = "99${_getTagLenght(subtag9900)}$subtag9900";

    // TAG_MAIN
    final maintag ="$tag00$tag01$tag30$tag52$tag53$tag54$tag58$tag59$tag60$tag62$tag98${tag99}6304";
    final checkSumResult = crc16(maintag);

    // FINAL_TAG_MAIN_WITH_CHECKSUM_DATA
    result = "$maintag$checkSumResult";

    return result;
  }

  String tag29({
    required String accountNumber,
    required String mcc,
    required String currencyCode,
    required double amount,
    required String merchantName,
    required String merchantLocation,
    required String businessId,
    required String outletId,
    required String cashierId,
  }) {
    String result = '';
    // TAG 00
    var tag00 = QrKeyConst.payloadIndicator;

    // TAG 01
    var tag01 = amount == 0
        ? QrKeyConst.staticInitationMethod
        : QrKeyConst.dynamicInitationMethod;

    // TAG 29
    // sub tag 29 path
    var subtag2900 =
        "00${_getTagLenght(QrKeyConst.uniqIdentifier)}${QrKeyConst.uniqIdentifier}";
    var subtag2901 = "01${_getTagLenght(accountNumber)}$accountNumber";
    var subtag2902 =
        "02${_getTagLenght(QrKeyConst.acquiringBankMerchant)}${QrKeyConst.acquiringBankMerchant}";
    var subtag29 = "$subtag2900$subtag2901$subtag2902";
    var tag29 = "29${_getTagLenght(subtag29)}$subtag29";

    // TAG 52
    var tag52 = "52${_getTagLenght(mcc)}$mcc";

    // TAG 53
    var tag53 = "53${_getTagLenght(currencyCode)}$currencyCode";

    // TAG 54
    var formatedAmount = _amountFormat.format(amount);
    var tag54 =
        amount == 0 ? '' : "54${_getTagLenght(formatedAmount)}$formatedAmount";

    // TAG 58
    var tag58 =
        "58${_getTagLenght(QrCountryCode.cambodia)}${QrCountryCode.cambodia}";

    // TAG 59
    var tag59 = "59${_getTagLenght(merchantName)}$merchantName";

    // TAG 60
    var tag60 = "60${_getTagLenght(merchantLocation)}$merchantLocation";

    // TAG 62 Merchant Info
    var subTag6200 =
        '00${_getTagLenght(QrKeyConst.appCode)}${QrKeyConst.appCode}';
    var subTag6201 = '01${_getTagLenght(businessId)}$businessId';
    var subTag6202 = '02${_getTagLenght(outletId)}$outletId';
    var subTag6205 = '05${_getTagLenght(cashierId)}$cashierId';
    var subTag6268Value = "$subTag6200$subTag6201$subTag6202$subTag6205";
    var subTag6268 = "68${_getTagLenght(subTag6268Value)}$subTag6268Value";
    var tag62 = "62${_getTagLenght(subTag6268)}$subTag6268";

    // tag 99
    var timeStamp = DateTime.now().microsecondsSinceEpoch;
    var subtag9900 = "00${_getTagLenght("$timeStamp")}$timeStamp";
    var tag99 = "99${_getTagLenght(subtag9900)}$subtag9900";

    // TAG_MAIN
    var maintag =
        "$tag00$tag01$tag29$tag52$tag53$tag54$tag58$tag59$tag60$tag62${tag99}6304";
    var checkSumResult = crc16(maintag);

    // FINAL_TAG_MAIN_WITH__CHECKSUM_DATA
    result = "$maintag$checkSumResult";
    return result;
  }

  // String _getTagLenght(String value) {
  //   var lenght = _tagLenghtFormat.format(value.length).toInt();
  //   return lenght.toRadixString(16);
  // }

  String _getTagLenght(String value) {
    return _tagLenghtFormat.format(value.length);
  }

  /// To Generate CRC16 value
  String crc16(String str) {
    var crc16 = 0xFFFF; // the CRC
    var len = str.length;

    for (var i = 0; i < len; i++) {
      var t = (crc16 >> 8) ^
          str.codeUnitAt(i); // High byte Xor Message Byte to get index
      crc16 =
          ((crc16 << 8) & 0xffff) ^ _crcLookup[t]; // Update the CRC from table
    }
    // crc16 now contains the CRC value
    return crc16.toRadixString(16).padLeft(4, "0").toUpperCase();
  }

  final _crcLookup = [
    0x0000,
    0x1021,
    0x2042,
    0x3063,
    0x4084,
    0x50A5,
    0x60C6,
    0x70E7,
    0x8108,
    0x9129,
    0xA14A,
    0xB16B,
    0xC18C,
    0xD1AD,
    0xE1CE,
    0xF1EF,
    0x1231,
    0x0210,
    0x3273,
    0x2252,
    0x52B5,
    0x4294,
    0x72F7,
    0x62D6,
    0x9339,
    0x8318,
    0xB37B,
    0xA35A,
    0xD3BD,
    0xC39C,
    0xF3FF,
    0xE3DE,
    0x2462,
    0x3443,
    0x0420,
    0x1401,
    0x64E6,
    0x74C7,
    0x44A4,
    0x5485,
    0xA56A,
    0xB54B,
    0x8528,
    0x9509,
    0xE5EE,
    0xF5CF,
    0xC5AC,
    0xD58D,
    0x3653,
    0x2672,
    0x1611,
    0x0630,
    0x76D7,
    0x66F6,
    0x5695,
    0x46B4,
    0xB75B,
    0xA77A,
    0x9719,
    0x8738,
    0xF7DF,
    0xE7FE,
    0xD79D,
    0xC7BC,
    0x48C4,
    0x58E5,
    0x6886,
    0x78A7,
    0x0840,
    0x1861,
    0x2802,
    0x3823,
    0xC9CC,
    0xD9ED,
    0xE98E,
    0xF9AF,
    0x8948,
    0x9969,
    0xA90A,
    0xB92B,
    0x5AF5,
    0x4AD4,
    0x7AB7,
    0x6A96,
    0x1A71,
    0x0A50,
    0x3A33,
    0x2A12,
    0xDBFD,
    0xCBDC,
    0xFBBF,
    0xEB9E,
    0x9B79,
    0x8B58,
    0xBB3B,
    0xAB1A,
    0x6CA6,
    0x7C87,
    0x4CE4,
    0x5CC5,
    0x2C22,
    0x3C03,
    0x0C60,
    0x1C41,
    0xEDAE,
    0xFD8F,
    0xCDEC,
    0xDDCD,
    0xAD2A,
    0xBD0B,
    0x8D68,
    0x9D49,
    0x7E97,
    0x6EB6,
    0x5ED5,
    0x4EF4,
    0x3E13,
    0x2E32,
    0x1E51,
    0x0E70,
    0xFF9F,
    0xEFBE,
    0xDFDD,
    0xCFFC,
    0xBF1B,
    0xAF3A,
    0x9F59,
    0x8F78,
    0x9188,
    0x81A9,
    0xB1CA,
    0xA1EB,
    0xD10C,
    0xC12D,
    0xF14E,
    0xE16F,
    0x1080,
    0x00A1,
    0x30C2,
    0x20E3,
    0x5004,
    0x4025,
    0x7046,
    0x6067,
    0x83B9,
    0x9398,
    0xA3FB,
    0xB3DA,
    0xC33D,
    0xD31C,
    0xE37F,
    0xF35E,
    0x02B1,
    0x1290,
    0x22F3,
    0x32D2,
    0x4235,
    0x5214,
    0x6277,
    0x7256,
    0xB5EA,
    0xA5CB,
    0x95A8,
    0x8589,
    0xF56E,
    0xE54F,
    0xD52C,
    0xC50D,
    0x34E2,
    0x24C3,
    0x14A0,
    0x0481,
    0x7466,
    0x6447,
    0x5424,
    0x4405,
    0xA7DB,
    0xB7FA,
    0x8799,
    0x97B8,
    0xE75F,
    0xF77E,
    0xC71D,
    0xD73C,
    0x26D3,
    0x36F2,
    0x0691,
    0x16B0,
    0x6657,
    0x7676,
    0x4615,
    0x5634,
    0xD94C,
    0xC96D,
    0xF90E,
    0xE92F,
    0x99C8,
    0x89E9,
    0xB98A,
    0xA9AB,
    0x5844,
    0x4865,
    0x7806,
    0x6827,
    0x18C0,
    0x08E1,
    0x3882,
    0x28A3,
    0xCB7D,
    0xDB5C,
    0xEB3F,
    0xFB1E,
    0x8BF9,
    0x9BD8,
    0xABBB,
    0xBB9A,
    0x4A75,
    0x5A54,
    0x6A37,
    0x7A16,
    0x0AF1,
    0x1AD0,
    0x2AB3,
    0x3A92,
    0xFD2E,
    0xED0F,
    0xDD6C,
    0xCD4D,
    0xBDAA,
    0xAD8B,
    0x9DE8,
    0x8DC9,
    0x7C26,
    0x6C07,
    0x5C64,
    0x4C45,
    0x3CA2,
    0x2C83,
    0x1CE0,
    0x0CC1,
    0xEF1F,
    0xFF3E,
    0xCF5D,
    0xDF7C,
    0xAF9B,
    0xBFBA,
    0x8FD9,
    0x9FF8,
    0x6E17,
    0x7E36,
    0x4E55,
    0x5E74,
    0x2E93,
    0x3EB2,
    0x0ED1,
    0x1EF0
  ];
}
