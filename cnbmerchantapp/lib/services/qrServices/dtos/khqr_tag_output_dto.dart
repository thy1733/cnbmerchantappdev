

class KHQRTagOutputDto {
  final String tagName;
  final String tagValue;
  final String tagData;
  final int inTagLenght;
  List<KHQRTagOutputDto> subtag=[];

  KHQRTagOutputDto({
    required this.tagName,
    required this.tagValue,
    required this.tagData,
    required this.inTagLenght,
    List<KHQRTagOutputDto>? subtag,
  }){
    this.subtag = subtag??[];
  }
}
