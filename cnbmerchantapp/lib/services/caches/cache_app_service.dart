import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/get.dart';
import 'package:hive/hive.dart';
import 'xcore.dart';

class AppCacheService implements IAppCacheService {
  @override
  Future<void> deleteAtAsync<T>({required String keyName, T? value, int index = 0}) async {
    try {
      debugPrint("local storage deleteAt key: $keyName");
      // final box = await Hive.openBox<T>(key);
      final secureKey = await getSecureKeyAsync(keyName: keyName);
      final box = await Hive.openBox<T>(keyName,encryptionCipher: HiveAesCipher(secureKey));
      box.deleteAt(index);
    } catch (e) {
      // exception error
      debugPrint("local exception deleteAt key: $keyName");
    }
  }

  @override
  Future<void> deletesAsync<T>({required String keyName, T? value}) async {
    try {
      debugPrint("local storage delete key: $keyName");
      // final box = await Hive.openBox<T>(key);
      final secureKey = await getSecureKeyAsync(keyName: keyName);
      final box = await Hive.openBox(keyName,encryptionCipher: HiveAesCipher(secureKey));
      box.delete(keyName);
    } catch (e) {
      // exception error
      debugPrint("local exception delets key: $keyName");
    }
  }

  @override
  Future getAtAsync<T>({required String keyName, T? value, int index = 0}) async {
    try {
      debugPrint("local storage getAt key: $keyName");
      final secureKey = await getSecureKeyAsync(keyName: keyName);
      final box = await Hive.openBox(keyName,encryptionCipher: HiveAesCipher(secureKey));
     // final box = await Hive.openBox<T>(key);
      var result = box.getAt(index);
      return result;
    } catch (e) {
      // exception error
      debugPrint("local exception getAt key: $keyName");
    }
  }

  @override
  Future getsAsync<T>({required String keyName, T? value}) async {
    try {
      debugPrint("local storage gets key: $keyName");
      final secureKey = await getSecureKeyAsync(keyName: keyName);
      final box = await Hive.openBox(keyName,encryptionCipher: HiveAesCipher(secureKey));
      var result = box.get(keyName);
      return result;
    } catch (e) {
      // exception error
      debugPrint("local exception gets key: $keyName");
      return null;
    }
  }

  @override
  Future<void> putAsync<T>({required String keyName, required T value}) async {
    try {
     debugPrint("local storage put key: $keyName");
      final secureKey = await getSecureKeyAsync(keyName: keyName);
      final box = await Hive.openBox(keyName,encryptionCipher: HiveAesCipher(secureKey));
      box.put(keyName, value);
    } catch (e) {
      // exception error
      debugPrint("local exception put key: $keyName  message: ${e.toString()}");
    }
  }

  @override
  Future<void> putAtAsync<T>(
      {required String keyName, required T value, int index = 0}) async {
    try {
      debugPrint("local storage putAt key: $keyName");
      final secureKey = await getSecureKeyAsync(keyName: keyName);
      final box = await Hive.openBox(keyName,encryptionCipher: HiveAesCipher(secureKey));
      box.putAt(index, value);
    } catch (e) {
      // exception error
      debugPrint("local exception putAt key: $keyName");
      e.printError();
    }
  }

  @override
  Future<void> updatesAllAsync<T>(
      {required String keyName, required T value, int index = 0}) async {
    try {} catch (e) {
      // exception error
    }
  }

  @override
  Future<void> updatesAtAsync<T>(
      {required String keyName, required T value, int index = 0}) async {
    try {
      debugPrint("local storage updatesAt key: $keyName");
      final secureKey = await getSecureKeyAsync(keyName: keyName);
      final box = await Hive.openBox(keyName,encryptionCipher: HiveAesCipher(secureKey));
      box.putAt(index, value);
    } catch (e) {
      // exception error
      debugPrint("local exception updatesAt key: $keyName");
    }
  }
  
  @override
  Future<Uint8List> getSecureKeyAsync({required String keyName}) async {
    const secureStorage = FlutterSecureStorage();
    // if key not exists return null
    final encryptionKey = await secureStorage.read(key: keyName);
    if (encryptionKey == null) {
      final key = Hive.generateSecureKey();
      await secureStorage.write(
        key: keyName,
        value: base64UrlEncode(key),
      );
    }
    final key = await secureStorage.read(key: keyName);
    final encryptionKeyDecode = base64Url.decode(key!);
    return encryptionKeyDecode;
  }
}