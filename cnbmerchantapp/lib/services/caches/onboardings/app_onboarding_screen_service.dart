import 'package:cnbmerchantapp/core.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:get/get.dart';

class AppOnboardingScreenService implements IAppOnboardingScreenService {
  final IAppCacheService _appCachedService = AppCacheService();

  Future<void> _initializeDeviceInfoAsync() async{
    DeviceInfoPlugin input = DeviceInfoPlugin();
    var value = AppOnboardingScreenModel.create();
    if (GetPlatform.isAndroid) {
      AndroidDeviceInfo infoOS = await input.androidInfo;
      value.identifier = infoOS.id;
      value.name = infoOS.brand;
      value.systemName = infoOS.model;
      value.systemVersion = infoOS.version.release;
      value.model = infoOS.model;
      value.isPhysicalDevice = infoOS.isPhysicalDevice;
      value.fingerprint = infoOS.fingerprint;
      value.androidId = infoOS.serialNumber;
      value.machine = infoOS.display;
      value.systemName ="Android";
      value.systemVersion = infoOS.version.release;
      value.model =  infoOS.model;
      value.nodename = infoOS.brand;
      value.isPhysicalDevice = infoOS.isPhysicalDevice;
      value.identifier = infoOS.id;
    } else if (GetPlatform.isIOS) {
      IosDeviceInfo infoOS = await input.iosInfo;
      value.identifier = infoOS.identifierForVendor!;
      value.name = infoOS.name;
      value.systemName = infoOS.systemName;
      value.systemVersion = infoOS.systemVersion;
      value.model = infoOS.model;
      value.isPhysicalDevice = infoOS.isPhysicalDevice;
      value.nodename = infoOS.utsname.nodename;
      value.machine = infoOS.utsname.machine;
    }
    DeviceInfoTempInstant.deviceInfo = value;
  }

  @override
  Future<AppOnboardingScreenModel> getAppOnboardingScreenAsync() async {
    AppOnboardingScreenModel result = AppOnboardingScreenModel.create();
    try {
      final resultCache = await _appCachedService.getsAsync(
        keyName: CacheKey.appOnboardingScreen,
      );
      if (resultCache != null){
        result = resultCache as AppOnboardingScreenModel;
        DeviceInfoTempInstant.deviceInfo = result;
      }else{
        await _initializeDeviceInfoAsync();
      }
      return result;
    } catch (e) {
      // exception error
      //print(" exception: ${e.toString()}");
      return result;
    }
  }

  @override
  Future<void> saveAppOnboardingScreenAsync(DeviceInfoPlugin input) async {
    try {
      DeviceInfoTempInstant.deviceInfo.isUserSkip = true;
      await _appCachedService.putAsync(keyName: CacheKey.appOnboardingScreen, value: DeviceInfoTempInstant.deviceInfo);
    } catch (e) {
      // exception error
      // print(" exception: ${e.toString()}");
    }
  }

  @override
  Future<void> updateUserLoginAsync(bool isLogin) async {
    try {
      var value = await getAppOnboardingScreenAsync();
      value.isUserLogin = isLogin;
      await _appCachedService.putAsync(keyName: CacheKey.appOnboardingScreen, value: value);
    } catch (e) {
      // exception error
      // print(" exception: ${e.toString()}");
    }
  }
}
