import 'package:cnbmerchantapp/core.dart';
import 'package:device_info_plus/device_info_plus.dart';

abstract class IAppOnboardingScreenService {
  
  Future<AppOnboardingScreenModel> getAppOnboardingScreenAsync();

  Future<void> saveAppOnboardingScreenAsync(DeviceInfoPlugin info);

  Future<void> updateUserLoginAsync(bool isLogin);
}