import 'package:hive/hive.dart';
import 'package:cnbmerchantapp/core.dart';
part 'app_onboarding_screen_model.g.dart';

@HiveType(typeId: CacheKeyType.appOnboardingScreen)
class AppOnboardingScreenModel {
  @HiveField(0)
  bool isUserSkip;
  @HiveField(1)
  bool isUserLogin;
  @HiveField(2)
  String identifier;
  @HiveField(3)
  String systemName;
  @HiveField(4)
  String systemVersion;
  @HiveField(5)
  String model;
  @HiveField(6)
  String createdDate;
  @HiveField(7)
  bool isPhysicalDevice;
  @HiveField(8)
  String fingerprint;
  @HiveField(9)
  String androidId;
  @HiveField(10)
  String name;
  @HiveField(11)
  String sysname;
  @HiveField(12)
  String nodename;
  @HiveField(13)
  String machine;

  AppOnboardingScreenModel(
      {this.isUserSkip = false,
      this.isUserLogin = false,
      this.identifier = "",
      this.systemName = "",
      this.systemVersion = "",
      this.model = "",
      this.createdDate = "",
      this.isPhysicalDevice = false,
      this.fingerprint = "",
      this.androidId = "",
      this.name = "",
      this.sysname = "",
      this.nodename ="",
      this.machine = ""});

  static AppOnboardingScreenModel create(
      {bool isUserSkip = false,
      bool isUserLogin = false,
      String id = "",
      String name ="",
      String systemName = "",
      String version = "",
      String model = "",
      bool isPhysicalDevice=false,
      String fingerprint="",
      String androidId="",
      String nodename="",
      String machince= "" }) {
    var result = AppOnboardingScreenModel();
    result.isUserSkip = isUserSkip;
    result.isUserLogin = isUserLogin;
    result.identifier = id;
    result.systemName = systemName;
    result.systemVersion = version;
    result.model = model;
    result.createdDate = DateTime.now().toString();
    result.isPhysicalDevice = isPhysicalDevice;
    result.fingerprint = fingerprint;
    result.androidId = androidId;
    result.nodename = nodename;
    result.machine = machince;
    return result;
  }
}
