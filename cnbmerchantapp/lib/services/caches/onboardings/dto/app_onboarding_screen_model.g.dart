// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app_onboarding_screen_model.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class AppOnboardingScreenModelAdapter
    extends TypeAdapter<AppOnboardingScreenModel> {
  @override
  final int typeId = 16;

  @override
  AppOnboardingScreenModel read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return AppOnboardingScreenModel(
      isUserSkip: fields[0] as bool,
      isUserLogin: fields[1] as bool,
      identifier: fields[2] as String,
      systemName: fields[3] as String,
      systemVersion: fields[4] as String,
      model: fields[5] as String,
      createdDate: fields[6] as String,
      isPhysicalDevice: fields[7] as bool,
      fingerprint: fields[8] as String,
      androidId: fields[9] as String,
      name: fields[10] as String,
      sysname: fields[11] as String,
      nodename: fields[12] as String,
      machine: fields[13] as String,
    );
  }

  @override
  void write(BinaryWriter writer, AppOnboardingScreenModel obj) {
    writer
      ..writeByte(14)
      ..writeByte(0)
      ..write(obj.isUserSkip)
      ..writeByte(1)
      ..write(obj.isUserLogin)
      ..writeByte(2)
      ..write(obj.identifier)
      ..writeByte(3)
      ..write(obj.systemName)
      ..writeByte(4)
      ..write(obj.systemVersion)
      ..writeByte(5)
      ..write(obj.model)
      ..writeByte(6)
      ..write(obj.createdDate)
      ..writeByte(7)
      ..write(obj.isPhysicalDevice)
      ..writeByte(8)
      ..write(obj.fingerprint)
      ..writeByte(9)
      ..write(obj.androidId)
      ..writeByte(10)
      ..write(obj.name)
      ..writeByte(11)
      ..write(obj.sysname)
      ..writeByte(12)
      ..write(obj.nodename)
      ..writeByte(13)
      ..write(obj.machine);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is AppOnboardingScreenModelAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
