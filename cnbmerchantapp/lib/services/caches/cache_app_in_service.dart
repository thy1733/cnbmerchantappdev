import 'package:flutter/foundation.dart';

abstract class IAppCacheService {
  Future<void> putAsync<T>({required String keyName,required T value});
  Future<void> putAtAsync<T>({required String keyName,required T value,int index=0});
  Future<dynamic> getsAsync<T>({required String keyName, T? value});
  Future<dynamic> getAtAsync<T>({required String keyName, T? value,int index=0});
  Future<void> updatesAtAsync<T>({required String keyName,required T value,int index=0});
  Future<void> updatesAllAsync<T>({required String keyName,required T value,int index=0});
  Future<void> deletesAsync<T>({required String keyName,T? value});
  Future<void> deleteAtAsync<T>({required String keyName,T? value,int index=0});
  Future<Uint8List> getSecureKeyAsync({required String keyName});
}