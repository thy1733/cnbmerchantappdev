// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app_setting_cache_model.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class AppSettingCacheModelAdapter extends TypeAdapter<AppSettingCacheModel> {
  @override
  final int typeId = 24;

  @override
  AppSettingCacheModel read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return AppSettingCacheModel()
      ..userSettingId = fields[0] == null ? '' : fields[0] as String
      ..inactivityTimeoutSeccond = fields[1] == null ? 30 : fields[1] as int;
  }

  @override
  void write(BinaryWriter writer, AppSettingCacheModel obj) {
    writer
      ..writeByte(2)
      ..writeByte(0)
      ..write(obj.userSettingId)
      ..writeByte(1)
      ..write(obj.inactivityTimeoutSeccond);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is AppSettingCacheModelAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
