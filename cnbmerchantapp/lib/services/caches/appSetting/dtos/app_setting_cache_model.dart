import 'package:cnbmerchantapp/core.dart';
import 'package:hive/hive.dart';

part 'app_setting_cache_model.g.dart';

@HiveType(typeId: CacheKeyType.appSetting)
class AppSettingCacheModel {
  @HiveField(0, defaultValue: "")
  String userSettingId = "";
  @HiveField(1, defaultValue: 30)
  int inactivityTimeoutSeccond = 30;

  AppSettingCacheModel();

  static AppSettingCacheModel create({
    required AppSettingModel input,
  }) {
    var result = AppSettingCacheModel();
    result.userSettingId = input.userSettingId;
    result.inactivityTimeoutSeccond = input.inactivityTimeoutSeccond;
    return result;
  }

  AppSettingModel toModel() {
    var result = AppSettingModel();
    result.userSettingId = userSettingId;
    result.inactivityTimeoutSeccond = inactivityTimeoutSeccond;
    return result;
  }
}
