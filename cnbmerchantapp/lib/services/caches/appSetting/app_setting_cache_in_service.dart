import 'package:cnbmerchantapp/core.dart';

abstract class IAppSettingCacheService{
    Future<AppSettingModel> getAppSetting(String userSettingId);
    Future<void> saveAppStting(AppSettingModel input);
    Future<void> clear();
}