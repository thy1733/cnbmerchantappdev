import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class AppSettingCacheService extends BaseAppCacheService
    implements IAppSettingCacheService {
  @override
  Future<void> clear() async {
    AppBusinessTempInstant.selectedBusiness =
        ManageBusinessItemListOutputModel();
    AppBusinessTempInstant.appCacheBusiness = ManageBusinessListOutputModel();
    await appCachedService.deletesAsync(keyName: CacheKey.appBusinessList);
    await appCachedService.deletesAsync(
        keyName: CacheKey.appSelectedBusinessId);
  }

  @override
  Future<AppSettingModel> getAppSetting(
    String userSettingId,
  ) async {
    AppSettingCacheModel result = AppSettingCacheModel();
    try {
      result.userSettingId = userSettingId;
      var valueList = <AppSettingCacheModel>[];
      final valueCache = await appCachedService
          .getAtAsync<List<AppSettingCacheModel>>(keyName: CacheKey.appSetting);
      if (valueCache != null) {
        valueList = (valueCache as List<dynamic>)
            .map((e) => e as AppSettingCacheModel)
            .toList();
        if (valueList.isNotEmpty) {
          for (var value in valueList) {
            // find is settin exist
            if (value.userSettingId == userSettingId) {
              result = value;
            }
          }
        }
      }
      AppSettingInstant.setting = result.toModel();

      return result.toModel();
    } catch (e) {
      return result.toModel();
    }
  }

  @override
  Future<void> saveAppStting(
    AppSettingModel input,
  ) async {
    try {
      var dataToSave = AppSettingCacheModel.create(input: input);
      var valueList = <AppSettingCacheModel>[];
      // get data
      final valueCache = await appCachedService
          .getAtAsync<List<AppSettingCacheModel>>(keyName: CacheKey.appSetting);

      if (valueCache != null) {
        valueList = (valueCache as List<dynamic>)
            .map((e) => e as AppSettingCacheModel)
            .toList();

        var exist = valueList.indexWhere((e) => e.userSettingId == dataToSave.userSettingId);
        if(exist > -1){
          valueList[exist] =  dataToSave;
        }else{
           valueList.add(dataToSave);
        }
        // if (valueList.isNotEmpty) {
        //   int i = 0;
        //   for (var value in valueList) {
        //     // find is settin exist
        //     if (value.userSettingId == dataToSave.userSettingId) {
        //       valueList[i] = dataToSave;
        //     } else {
        //       valueList.add(dataToSave);
        //     }
        //     i++;
        //   }
        // } else {
        //   valueList.add(dataToSave);
        // }
      } else {
        valueList.add(dataToSave);
      }

      await appCachedService.putAsync(
          keyName: CacheKey.appSetting, value: valueList);
      AppSettingInstant.setting = dataToSave.toModel();
    } catch (e) {
      e.printError();
    }
  }
}
