import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class AppUserProfileInfoService extends BaseAppCacheService implements IAppUserProfileInfoService {

  @override
  Future<AppUserProfileInfoModel> getUserProfileAsync() async{
    late AppUserProfileInfoModel result = AppUserProfileInfoModel.create();
    try {
      final resultCache = await appCachedService.getsAsync(
        keyName: CacheKey.appUserProfileInfo,
      );
      if (resultCache != null){      
        result = resultCache as AppUserProfileInfoModel;
        AppUserTempInstant.appCacheUser = result;
      }
      return result;
    } catch (e) {
      // exception error
      e.printError();
      return result;
    }
  }

  @override
  Future<void> loginAuthenticateAsync(String mCID, String number, String username,String pass) async{
    try {

      // In case API not yet ready for get Profile
      var value =  await getUserProfileAsync();
      value.mCID = mCID;
      value.accountNumber = number;
      value.userName = username;
      value.userPass = pass;
       AppUserTempInstant.appCacheUser = value;
      await appCachedService.putAsync(keyName: CacheKey.appUserProfileInfo, value: value);
    } catch (e) {
      // exception error
      e.printError();
    }
  }

  @override
  Future<void> removeUserProfileAsync()async {
    try {
      var value = AppUserProfileInfoModel();
       AppUserTempInstant.appCacheUser = value;
      await appCachedService.deletesAsync(keyName: CacheKey.appUserProfileInfo, value: value);
    } catch (e) {
      // exception error
      e.printError();
    }
  }
  
  @override
  Future<void> registerActivateAccountAsync(RegisterAccountInputModel input) async {
   try {
      var value = AppUserProfileInfoModel.create();
      value.mCID = input.cid;
      value.userType = input.inviteCode.isNotEmpty ? 2 :  input.accountNumber.isNotEmpty ? 1 : 0;
      value.firstName = input.firstName;
      value.lastName = input.lastName;
      value.displayName = "${input.firstName} ${input.lastName}";
      value.displayName = value.displayName.isEmpty ? input.phoneNumber : value.displayName;
      value.accountNumber = input.accountNumber.removeWhiteSpace();
      value.phoneNumber = input.phoneNumber.removeWhiteSpace();
      value.inviteCode = input.inviteCode.removeWhiteSpace();
      value.userPin = input.pinCode;
      value.userPass = input.password;
      value.userName = input.userName;
       AppUserTempInstant.appCacheUser = value;
      await appCachedService.putAsync(keyName: CacheKey.appUserProfileInfo, value: value);
    } catch (e) {
      // exception error
      e.printError();
    }
  }

  @override
  Future<void> updateAuthenticateAsync(AppUserProfileInfoModel value) async {
    try {
      AppUserTempInstant.appCacheUser = value;
      await appCachedService.putAsync(keyName: CacheKey.appUserProfileInfo, value: value);
    } catch (e) {
      // exception error
      e.printError();
    }
  }
  
  @override
  Future<void> logoutAsync() async{
     try {
      // remove data don't need
      var value = AppUserProfileInfoModel();
      value.userName = AppUserTempInstant.appCacheUser.userName;
      value.firstName = AppUserTempInstant.appCacheUser.firstName;
      value.lastName = AppUserTempInstant.appCacheUser.lastName;
      value.photo = AppUserTempInstant.appCacheUser.photo;
      value.displayName = AppUserTempInstant.appCacheUser.displayName;
      AppUserTempInstant.appCacheUser = value;
      await appCachedService.putAsync(keyName: CacheKey.appUserProfileInfo, value: value);
    } catch (e) {
      // exception error
      e.printError();
    }
  }

}