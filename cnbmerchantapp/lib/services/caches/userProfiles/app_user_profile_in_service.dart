
import 'package:cnbmerchantapp/core.dart';

abstract class IAppUserProfileInfoService {
  
  Future<AppUserProfileInfoModel> getUserProfileAsync();
  Future<void> loginAuthenticateAsync(String mCID, String number, String username,String pass);
  Future<void> registerActivateAccountAsync(RegisterAccountInputModel input);
  Future<void> removeUserProfileAsync();
  Future<void> updateAuthenticateAsync(AppUserProfileInfoModel userProfile);
  Future<void> logoutAsync();
}