// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app_user_profile_info_model.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class AppUserProfileInfoModelAdapter
    extends TypeAdapter<AppUserProfileInfoModel> {
  @override
  final int typeId = 19;

  @override
  AppUserProfileInfoModel read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return AppUserProfileInfoModel(
      mCID: fields[0] == null ? '' : fields[0] as String,
      userType: fields[1] == null ? 1 : fields[1] as int,
      firstName: fields[2] == null ? '' : fields[2] as String,
      lastName: fields[3] == null ? '' : fields[3] as String,
      displayName: fields[4] == null ? '' : fields[4] as String,
      phoneNumber: fields[5] == null ? '' : fields[5] as String,
      accountNumber: fields[6] == null ? '' : fields[6] as String,
      inviteCode: fields[7] == null ? '' : fields[7] as String,
      userPin: fields[8] == null ? '' : fields[8] as String,
      userPass: fields[9] == null ? '' : fields[9] as String,
      lastLoginDateTime: fields[10] == null ? '' : fields[10] as String,
      userName: fields[11] == null ? '' : fields[11] as String,
      isRefundable: fields[12] == null ? '' : fields[12] as String,
      refundLimited: fields[13] == null ? '' : fields[13] as String,
      photo: fields[14] == null ? '' : fields[14] as String,
      profileId: fields[15] == null ? '' : fields[15] as String,
      businessId: fields[16] == null ? '' : fields[16] as String,
      profilePhotoId: fields[17] == null ? '' : fields[17] as String,
      telegramToken: fields[18] == null ? '' : fields[18] as String,
    );
  }

  @override
  void write(BinaryWriter writer, AppUserProfileInfoModel obj) {
    writer
      ..writeByte(19)
      ..writeByte(0)
      ..write(obj.mCID)
      ..writeByte(1)
      ..write(obj.userType)
      ..writeByte(2)
      ..write(obj.firstName)
      ..writeByte(3)
      ..write(obj.lastName)
      ..writeByte(4)
      ..write(obj.displayName)
      ..writeByte(5)
      ..write(obj.phoneNumber)
      ..writeByte(6)
      ..write(obj.accountNumber)
      ..writeByte(7)
      ..write(obj.inviteCode)
      ..writeByte(8)
      ..write(obj.userPin)
      ..writeByte(9)
      ..write(obj.userPass)
      ..writeByte(10)
      ..write(obj.lastLoginDateTime)
      ..writeByte(11)
      ..write(obj.userName)
      ..writeByte(12)
      ..write(obj.isRefundable)
      ..writeByte(13)
      ..write(obj.refundLimited)
      ..writeByte(14)
      ..write(obj.photo)
      ..writeByte(15)
      ..write(obj.profileId)
      ..writeByte(16)
      ..write(obj.businessId)
      ..writeByte(17)
      ..write(obj.profilePhotoId)
      ..writeByte(18)
      ..write(obj.telegramToken);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is AppUserProfileInfoModelAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
