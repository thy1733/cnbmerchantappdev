import 'package:hive/hive.dart';
import 'package:cnbmerchantapp/core.dart';
part 'app_user_profile_info_model.g.dart';

@HiveType(typeId: CacheKeyType.appUserProfileInfo)
class AppUserProfileInfoModel {
  @HiveField(0, defaultValue: "")
  String mCID;
  @HiveField(1, defaultValue: 1)
  int userType; // 0-Business Owner, 1-Cashier
  @HiveField(2, defaultValue: "")
  String firstName;
  @HiveField(3, defaultValue: "")
  String lastName;
  @HiveField(4, defaultValue: "")
  String displayName;
  @HiveField(5, defaultValue: "")
  String phoneNumber;
  @HiveField(6, defaultValue: "")
  String accountNumber;
  @HiveField(7, defaultValue: "")
  String inviteCode;
  @HiveField(8, defaultValue: "")
  String userPin;
  @HiveField(9, defaultValue: "")
  String userPass;
  @HiveField(10, defaultValue: "")
  String lastLoginDateTime;
  @HiveField(11, defaultValue: "")
  String userName;
  @HiveField(12, defaultValue: "")
  String isRefundable;
  @HiveField(13, defaultValue: "")
  String refundLimited;
  @HiveField(14, defaultValue: "")
  String photo;
  @HiveField(15, defaultValue: "")
  String profileId;
  @HiveField(16, defaultValue: "")
  String businessId;
  @HiveField(17, defaultValue: "")
  String profilePhotoId;
  @HiveField(18, defaultValue: "")
  String telegramToken;

  AppUserProfileInfoModel(
      {this.mCID = "",
      this.userType = 0,
      this.firstName = "",
      this.lastName = "",
      this.displayName = "",
      this.phoneNumber = "",
      this.accountNumber = "",
      this.inviteCode = "",
      this.userPin = "",
      this.userPass = "",
      this.lastLoginDateTime = "",
      this.userName = "",
      this.isRefundable = "",
      this.refundLimited = "",
      this.photo = "",
      this.profileId = "",
      this.businessId = "",
      this.profilePhotoId = "",
      this.telegramToken = ""});

  static AppUserProfileInfoModel create(
      {String mCID = "",
      int userType = 0,
      String firstName = "",
      String lastName = "",
      String displayName = "",
      String phoneNumber = "",
      String accountNumber = "",
      String inviteCode = "",
      String userPin = "",
      String userPass = "",
      String logTime = "",
      String userName = "",
      String isRefundable = "",
      String refundLimited = "",
      String photo = "",
      String profileId = "",
      String businessId = "",
      String lastLoginDate = "",
      String profilePhotoId = "",
      String telegramToken = ""}) {
    var result = AppUserProfileInfoModel();
    result.mCID = mCID;
    result.userType = userType;
    result.firstName = firstName;
    result.lastName = lastName;
    result.displayName = displayName;
    result.phoneNumber = phoneNumber;
    result.accountNumber = accountNumber;
    result.inviteCode = inviteCode;
    result.userPin = userPin;
    result.userPass = userPass;
    result.userName = userName;
    result.isRefundable = isRefundable;
    result.refundLimited = refundLimited;
    result.photo = photo;
    result.profileId = profileId;
    result.businessId = businessId;
    result.lastLoginDateTime = lastLoginDate;
    result.profilePhotoId = profilePhotoId;
    result.telegramToken = telegramToken;
    return result;
  }

  void assignFromUserProfileModel(UserProfileOutputModel model) {
    firstName = model.firstName;
    lastName = model.lastName;
    displayName = model.displayName;
    photo = model.profileUrl;
    profilePhotoId = model.profilePhotoId;
    userType = int.tryParse(model.activateType) ?? -1;
    phoneNumber = model.phoneNumber;
    userPin = model.pinCode;
    lastLoginDateTime = model.lastLoginDateTime.toIso8601String();
    refundLimited = model.refundLimited;
    isRefundable = model.isRefundable;
    profileId = model.profileId;
    businessId = model.businessId;
    telegramToken = model.telegramToken;
  }
}
