import 'package:cnbmerchantapp/core.dart';

class AppOutletCacheService extends BaseAppCacheService
    implements IAppOutletCacheService {
  @override
  Future<OutletListOutputModel> getOutlets(String businessId) async {
    OutletListCacheModel value = OutletListCacheModel();
    try {
      final valueCache =
          await appCachedService.getsAsync(keyName: CacheKey.outletItemList);
      if (valueCache != null) {
        var valueCast = valueCache as OutletListCacheModel;
        var item = valueCast.items
            .where((element) => element.businessId == businessId)
            .toList();
        value.items = item;
      }
      AppOutletTempInstant.outletList = value.toModel();

      return value.toModel();
    } catch (e) {
      return value.toModel();
    }
  }

  @override
  Future<void> saveOutlets(OutletListOutputModel input) async {
    var value = OutletListCacheModel.create(input: input);
    await appCachedService.putAsync(
      keyName: CacheKey.outletItemList,
      value: value,
    );
    AppOutletTempInstant.outletList = input;
  }

  @override
  Future<OutletItemListOutputModel> getSelectedOutlet() async {
    OutletItemListOutputModel value = OutletItemListOutputModel();
    try {
      value = AppOutletTempInstant.outletList.items.first;

      final valueCache =
          await appCachedService.getAtAsync(keyName: CacheKey.selectedOutletId);
      if (valueCache != null) {
        var id = valueCache as String;
        if (id.isNotEmpty) {
          value = AppOutletTempInstant.outletList.items.firstWhere(
            (element) => element.id == id,
            orElse: () => AppOutletTempInstant.outletList.items.first,
          );
        }
      }
      AppOutletTempInstant.selectedOutlet = value;
      return value;
    } catch (e) {
      AppOutletTempInstant.selectedOutlet = value;
      return value;
    }
  }

  @override
  Future<void> saveSelectedOutlet(OutletItemListOutputModel input) async {
    await appCachedService.putAsync(
      keyName: CacheKey.selectedOutletId,
      value: input.id,
    );
    AppOutletTempInstant.selectedOutlet = input;
  }

  @override
  Future<void> clear() async {
    AppOutletTempInstant.selectedOutlet = OutletItemListOutputModel();
    AppOutletTempInstant.outletList = OutletListOutputModel();
    await appCachedService.deletesAsync(keyName: CacheKey.outletItemList);
    await appCachedService.deletesAsync(keyName: CacheKey.selectedOutletId);
  }
}
