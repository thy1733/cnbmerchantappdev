// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'outlet_list_cache_model.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class OutletListCacheModelAdapter extends TypeAdapter<OutletListCacheModel> {
  @override
  final int typeId = 23;

  @override
  OutletListCacheModel read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return OutletListCacheModel()
      ..items = (fields[0] as List).cast<OutletItemCacheModel>();
  }

  @override
  void write(BinaryWriter writer, OutletListCacheModel obj) {
    writer
      ..writeByte(1)
      ..writeByte(0)
      ..write(obj.items);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is OutletListCacheModelAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
