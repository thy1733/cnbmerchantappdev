// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'outlet_item_cache_model.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class OutletItemCacheModelAdapter extends TypeAdapter<OutletItemCacheModel> {
  @override
  final int typeId = 22;

  @override
  OutletItemCacheModel read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return OutletItemCacheModel()
      ..id = fields[0] == null ? '' : fields[0] as String
      ..creationDate = fields[1] == null ? '' : fields[1] as String
      ..accountNumber = fields[2] == null ? '' : fields[2] as String
      ..businessId = fields[3] == null ? '' : fields[3] as String
      ..outletName = fields[4] == null ? '' : fields[4] as String
      ..outletLogo = fields[5] == null ? '' : fields[5] as String
      ..status = fields[6] == null ? '' : fields[6] as String
      ..locationName = fields[7] == null ? '' : fields[7] as String
      ..locationAddress = fields[8] == null ? '' : fields[8] as String
      ..latitude = fields[9] == null ? 0.0 : fields[9] as double
      ..longitude = fields[10] == null ? 0.0 : fields[10] as double
      ..isPrimary = fields[11] == null ? false : fields[11] as bool
      ..telegramGroupId = fields[12] == null ? '' : fields[12] as String
      ..outletLogoId = fields[13] == null ? '' : fields[13] as String
      ..outletLogoCacheKey = fields[14] == null ? '' : fields[14] as String;
  }

  @override
  void write(BinaryWriter writer, OutletItemCacheModel obj) {
    writer
      ..writeByte(15)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.creationDate)
      ..writeByte(2)
      ..write(obj.accountNumber)
      ..writeByte(3)
      ..write(obj.businessId)
      ..writeByte(4)
      ..write(obj.outletName)
      ..writeByte(5)
      ..write(obj.outletLogo)
      ..writeByte(6)
      ..write(obj.status)
      ..writeByte(7)
      ..write(obj.locationName)
      ..writeByte(8)
      ..write(obj.locationAddress)
      ..writeByte(9)
      ..write(obj.latitude)
      ..writeByte(10)
      ..write(obj.longitude)
      ..writeByte(11)
      ..write(obj.isPrimary)
      ..writeByte(12)
      ..write(obj.telegramGroupId)
      ..writeByte(13)
      ..write(obj.outletLogoId)
      ..writeByte(14)
      ..write(obj.outletLogoCacheKey);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is OutletItemCacheModelAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
