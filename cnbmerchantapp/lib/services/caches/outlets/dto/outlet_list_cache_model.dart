import 'package:hive/hive.dart';
import 'package:cnbmerchantapp/core.dart';

part 'outlet_list_cache_model.g.dart';

@HiveType(typeId: CacheKeyType.appOutletList)
class OutletListCacheModel {
  @HiveField(0)
  List<OutletItemCacheModel> items = [];

  static OutletListCacheModel create({required OutletListOutputModel input}) {
    var result = OutletListCacheModel();
    for (var item in input.items) {
      result.items.add(OutletItemCacheModel.create(input: item));
    }
    return result;
  }

  OutletListOutputModel toModel() {
    var result = OutletListOutputModel();
    for (var item in items) {
      result.items.add(item.toModel());
    }
    return result;
  }
}
