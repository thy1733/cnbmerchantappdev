import 'package:hive/hive.dart';
import 'package:cnbmerchantapp/core.dart';
part 'outlet_item_cache_model.g.dart';

@HiveType(typeId: CacheKeyType.appOutletItem)
class OutletItemCacheModel {
  @HiveField(0, defaultValue: "")
  String id = "";
  @HiveField(1, defaultValue: "")
  String creationDate = "";
  @HiveField(2, defaultValue: "")
  String accountNumber = "";
  @HiveField(3, defaultValue: "")
  String businessId = "";
  @HiveField(4, defaultValue: "")
  String outletName = "";
  @HiveField(5, defaultValue: "")
  String outletLogo = "";
  @HiveField(6, defaultValue: "")
  String status = "";
  @HiveField(7, defaultValue: "")
  String locationName = "";
  @HiveField(8, defaultValue: "")
  String locationAddress = "";
  @HiveField(9, defaultValue:0.0)
  double latitude = 0.0;
  @HiveField(10, defaultValue: 0.0)
  double longitude = 0.0;
  @HiveField(11, defaultValue: false)
  bool isPrimary = false;
  @HiveField(12, defaultValue: "")
  String telegramGroupId = "";
  @HiveField(13, defaultValue: "")
  String outletLogoId = "";
  @HiveField(14, defaultValue: "")
  String outletLogoCacheKey = "";

  static OutletItemCacheModel create(
      {required OutletItemListOutputModel input}) {
    var result = OutletItemCacheModel();
    result.id = input.id;
    result.creationDate = input.creationDate;
    result.accountNumber = input.accountNumber;
    result.businessId = input.businessId;
    result.outletName = input.outletName;
    result.status = input.status;
    result.locationName = input.locationName;
    result.locationAddress = input.locationAddress;
    result.latitude = input.latitude;
    result.longitude = input.longitude;
    result.isPrimary = input.isPrimary;
    result.telegramGroupId = input.telegramGroupId;
    result.outletLogo = input.outletLogo;
    result.outletLogoId = input.outletLogoId;
    result.outletLogoCacheKey = input.cacheKey;
    return result;
  }

  OutletItemListOutputModel toModel() {
    var result = OutletItemListOutputModel();
    result.id = id;
    result.creationDate = creationDate;
    result.accountNumber = accountNumber;
    result.businessId = businessId;
    result.outletName = outletName;
    result.status = status;
    result.locationName = locationName;
    result.locationAddress = locationAddress;
    result.latitude = latitude;
    result.longitude = longitude;
    result.isPrimary = isPrimary;
    result.telegramGroupId = telegramGroupId;
    result.outletLogo = outletLogo;
    result.cacheKey = outletLogoCacheKey;
    result.outletLogoId = outletLogoId;
    return result;
  }
}
