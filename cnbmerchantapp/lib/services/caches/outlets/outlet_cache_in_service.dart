import 'package:cnbmerchantapp/core.dart';

abstract class IAppOutletCacheService{
    Future<OutletListOutputModel> getOutlets(String businessId);
    Future<void> saveOutlets(OutletListOutputModel input);
    Future<OutletItemListOutputModel> getSelectedOutlet();
    Future<void> saveSelectedOutlet(OutletItemListOutputModel input);
    Future<void> clear();
}