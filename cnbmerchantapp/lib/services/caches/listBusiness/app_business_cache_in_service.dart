import 'package:cnbmerchantapp/core.dart';

abstract class IAppBusinessCacheService{
    Future<ManageBusinessListOutputModel> getBusiness();
    Future<void> saveBusiness(ManageBusinessListOutputModel input);
    Future<ManageBusinessItemListOutputModel?> getSelectedBusiness();
    Future<void> saveSelectedBusiness(ManageBusinessItemListOutputModel input);
    Future<void> clear();
}