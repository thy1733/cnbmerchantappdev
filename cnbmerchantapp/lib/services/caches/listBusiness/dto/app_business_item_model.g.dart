// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app_business_item_model.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class AppBusinessItemModelAdapter extends TypeAdapter<AppBusinessItemModel> {
  @override
  final int typeId = 20;

  @override
  AppBusinessItemModel read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return AppBusinessItemModel()
      ..id = fields[0] as String
      ..creationDate = fields[1] as String
      ..accountNumber = fields[2] as String
      ..businessName = fields[3] as String
      ..businessLogo = fields[4] as String
      ..businessTypeId = fields[5] as String
      ..businessTypeName = fields[6] as String
      ..status = fields[7] as String
      ..corporateStatus = fields[8] as String
      ..locationName = fields[9] as String
      ..locationAddress = fields[10] as String
      ..latitude = fields[11] as double
      ..longitude = fields[12] as double
      ..mcc = fields[13] as String
      ..accountCurrency = fields[14] as String;
  }

  @override
  void write(BinaryWriter writer, AppBusinessItemModel obj) {
    writer
      ..writeByte(15)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.creationDate)
      ..writeByte(2)
      ..write(obj.accountNumber)
      ..writeByte(3)
      ..write(obj.businessName)
      ..writeByte(4)
      ..write(obj.businessLogo)
      ..writeByte(5)
      ..write(obj.businessTypeId)
      ..writeByte(6)
      ..write(obj.businessTypeName)
      ..writeByte(7)
      ..write(obj.status)
      ..writeByte(8)
      ..write(obj.corporateStatus)
      ..writeByte(9)
      ..write(obj.locationName)
      ..writeByte(10)
      ..write(obj.locationAddress)
      ..writeByte(11)
      ..write(obj.latitude)
      ..writeByte(12)
      ..write(obj.longitude)
      ..writeByte(13)
      ..write(obj.mcc)
      ..writeByte(14)
      ..write(obj.accountCurrency);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is AppBusinessItemModelAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
