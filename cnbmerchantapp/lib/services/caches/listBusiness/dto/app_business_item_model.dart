import 'package:hive/hive.dart';
import 'package:cnbmerchantapp/core.dart';
part 'app_business_item_model.g.dart';

@HiveType(typeId: CacheKeyType.appBusinessItem)
class AppBusinessItemModel {
  @HiveField(0)
  String id = "";
  @HiveField(1)
  String creationDate = "";
  @HiveField(2)
  String accountNumber = "";
  @HiveField(3)
  String businessName = "";
  @HiveField(4)
  String businessLogo = "";
  @HiveField(5)
  String businessTypeId = "";
  @HiveField(6)
  String businessTypeName = "";
  @HiveField(7)
  String status = "";
  @HiveField(8)
  String corporateStatus = "";
  @HiveField(9)
  String locationName = "";
  @HiveField(10)
  String locationAddress = "";
  @HiveField(11)
  double latitude = 0.0;
  @HiveField(12)
  double longitude = 0.0;
  @HiveField(13)
  String mcc = "";
  @HiveField(14)
  String accountCurrency = "";

  static AppBusinessItemModel create(
      {required ManageBusinessItemListOutputModel input}) {
    var result = AppBusinessItemModel();
    result.id = input.id;
    result.creationDate = input.creationDate;
    result.accountNumber = input.accountNumber;
    result.businessName = input.businessName;
    result.businessLogo = input.businessLogo;
    result.businessTypeId = input.businessType.id;
    result.businessTypeName = input.businessType.name;
    result.status = input.status;
    result.corporateStatus = input.corporateStatus;
    result.locationName = input.locationName;
    result.locationAddress = input.locationAddress;
    result.latitude = input.latitude;
    result.longitude = input.longitude;
    result.mcc = input.mcc;
    result.accountCurrency = input.accountCurrency;
    return result;
  }

  ManageBusinessItemListOutputModel toModel() {
    var result = ManageBusinessItemListOutputModel();
    result.id = id;
    result.creationDate = creationDate;
    result.accountNumber = accountNumber;
    result.businessName = businessName;
    result.businessLogo = businessLogo;
    result.businessType.id = businessTypeId;
    result.businessType.name = businessTypeName;
    result.status = status;
    result.corporateStatus = corporateStatus;
    result.locationName = locationName;
    result.locationAddress = locationAddress;
    result.latitude = latitude;
    result.longitude = longitude;
    result.mcc = mcc;
    result.accountCurrency = accountCurrency;
    return result;
  }
}
