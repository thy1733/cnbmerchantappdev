// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app_business_list_model.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class AppBusinessListModelAdapter extends TypeAdapter<AppBusinessListModel> {
  @override
  final int typeId = 21;

  @override
  AppBusinessListModel read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return AppBusinessListModel()
      ..items = (fields[0] as List).cast<AppBusinessItemModel>();
  }

  @override
  void write(BinaryWriter writer, AppBusinessListModel obj) {
    writer
      ..writeByte(1)
      ..writeByte(0)
      ..write(obj.items);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is AppBusinessListModelAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
