import 'package:hive/hive.dart';
import 'package:cnbmerchantapp/core.dart';

part 'app_business_list_model.g.dart';

@HiveType(typeId: CacheKeyType.appBusinessList)
class AppBusinessListModel {
  @HiveField(0)
  List<AppBusinessItemModel> items = [];

  static AppBusinessListModel create(
      {required ManageBusinessListOutputModel input}) {
    var result = AppBusinessListModel();
    for (var item in input.items) {
      result.items.add(AppBusinessItemModel.create(input: item));
    }
    return result;
  }

  ManageBusinessListOutputModel toModel() {
    var result = ManageBusinessListOutputModel();
    for (var item in items) {
      result.items.add(item.toModel());
    }
    return result;
  }
}
