import 'package:cnbmerchantapp/core.dart';

class AppBusinessCacheService extends BaseAppCacheService
    implements IAppBusinessCacheService {
  @override
  Future<ManageBusinessListOutputModel> getBusiness() async {
    AppBusinessListModel value = AppBusinessListModel();
    try {
      final valueCache =
          await appCachedService.getAtAsync(keyName: CacheKey.appBusinessList);
      if (valueCache != null) {
        value = valueCache as AppBusinessListModel;
      }
      AppBusinessTempInstant.appCacheBusiness = value.toModel();

      return value.toModel();
    } catch (e) {
      return value.toModel();
    }
  }

  @override
  Future<void> saveBusiness(ManageBusinessListOutputModel input) async {
    var value = AppBusinessListModel.create(input: input);
    await appCachedService.putAsync(
        keyName: CacheKey.appBusinessList, value: value);
    AppBusinessTempInstant.appCacheBusiness = input;
  }

  @override
  Future<ManageBusinessItemListOutputModel?> getSelectedBusiness() async {
    ManageBusinessItemListOutputModel value =
        ManageBusinessItemListOutputModel();
    try {
      value = AppBusinessTempInstant.appCacheBusiness.items.first;

      final valueCache = await appCachedService.getAtAsync(
          keyName: CacheKey.appSelectedBusinessId);
      if (valueCache != null) {
        var id = valueCache as String;
        if (id.isNotEmpty) {
          value = AppBusinessTempInstant.appCacheBusiness.items.firstWhere(
            (element) => element.id == id,
            orElse: () => AppBusinessTempInstant.appCacheBusiness.items.first,
          );
        }
      }
      AppBusinessTempInstant.selectedBusiness = value;
      return value;
    } catch (e) {
      AppBusinessTempInstant.selectedBusiness = value;
      return value;
    }
  }

  @override
  Future<void> saveSelectedBusiness(
      ManageBusinessItemListOutputModel input) async {
    await appCachedService.putAsync(
        keyName: CacheKey.appSelectedBusinessId, value: input.id);
    AppBusinessTempInstant.selectedBusiness = input;
  }

  @override
  Future<void> clear() async {
    AppBusinessTempInstant.selectedBusiness =
        ManageBusinessItemListOutputModel();
    AppBusinessTempInstant.appCacheBusiness = ManageBusinessListOutputModel();
    await appCachedService.deletesAsync(keyName: CacheKey.appBusinessList);
    await appCachedService.deletesAsync(
        keyName: CacheKey.appSelectedBusinessId);
  }
}
