// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app_localize_model.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class AppLocalizeModelAdapter extends TypeAdapter<AppLocalizeModel> {
  @override
  final int typeId = 18;

  @override
  AppLocalizeModel read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return AppLocalizeModel(
      isActivte: fields[0] as bool,
      language: fields[1] as int,
    );
  }

  @override
  void write(BinaryWriter writer, AppLocalizeModel obj) {
    writer
      ..writeByte(2)
      ..writeByte(0)
      ..write(obj.isActivte)
      ..writeByte(1)
      ..write(obj.language);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is AppLocalizeModelAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
