import 'package:cnbmerchantapp/core.dart';
import 'package:hive/hive.dart';
part 'app_localize_model.g.dart';

@HiveType(typeId: CacheKeyType.appLocalize)
class AppLocalizeModel {
  @HiveField(0)
  bool isActivte;
  @HiveField(1)
  int language;

  // default language 'English'
  AppLocalizeModel({
    this.isActivte = false,
    this.language = 1,
  });

  static AppLocalizeModel create({bool isActivte = false, int language = 1}) {
    var result = AppLocalizeModel();
    result.isActivte = isActivte;
    result.language = language;
    return result;
  }
}
