

import 'package:cnbmerchantapp/core.dart';

abstract class IAppLocalizeService {
  Future<AppLocalizeModel> getAppLocalizeAsync();
  Future<void> updateLocalizeAsync(AppLocalizeModel lan);
}