import 'package:cnbmerchantapp/core.dart';

class AppLocalizeService implements IAppLocalizeService {
  final IAppCacheService _appCachedService = AppCacheService();

  @override
  Future<AppLocalizeModel> getAppLocalizeAsync() async{
    AppLocalizeModel result = AppLocalizeModel.create(isActivte: true);
    try {
      var resultAppLocalize = await _appCachedService.getsAsync(keyName: CacheKey.applocalize);
      if (resultAppLocalize !=null) {
        var result = resultAppLocalize as AppLocalizeModel;
        return result;
      } else {
       await _appCachedService.putAsync<AppLocalizeModel>(keyName: CacheKey.applocalize, value: result);
        return result;
      }
    } catch (e) {
      return result;
    }
  }

  @override
  Future<void> updateLocalizeAsync(AppLocalizeModel lan) async{
    try {
      // update language
      await _appCachedService.putAsync(keyName: CacheKey.applocalize, value: lan);
    } catch (e) {
      // exception error
    }
  }
}