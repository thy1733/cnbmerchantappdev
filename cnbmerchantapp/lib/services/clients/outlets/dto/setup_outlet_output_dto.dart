import 'package:cnbmerchantapp/core.dart';

class SetupOutletOutputDto extends ResultStatusOutputDto {
  SetupOutletOutputDto();

  factory SetupOutletOutputDto.create(List<String> dto) {
    var result = SetupOutletOutputDto();

    result.statusCode = dto[0];
    result.isSuccess = dto[0] == "00";
    result.description = dto[0] != "00" ? dto[2] : dto[1];

    return result;
  }
}
