import 'package:cnbmerchantapp/core.dart';

class ActivateOutletOutputDto extends ResultStatusOutputDto {
  static ActivateOutletOutputDto create(List<String> dto) {
    var result = ActivateOutletOutputDto();
    result.statusCode = dto[0];
    result.isSuccess = dto[0] == "00";
    result.description = dto[1];
    return result;
  }
}
