import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class OutletItemListOutputDto extends ResultStatusOutputDto {
  OutletItemListOutputDto();

  final _totalCounts = 0.obs;
  int get totalCounts => _totalCounts.value;
  set totalCounts(int value) => _totalCounts.value = value;

  final _items = <OutletListOutputDto>[].obs;
  List<OutletListOutputDto> get items => _items;
  set items(List<OutletListOutputDto> value) => _items.value = value;

  factory OutletItemListOutputDto.create(List<String> dto) {
    var result = OutletItemListOutputDto();
    result.statusCode = dto[0];
    result.isSuccess = dto[0] == "00";
    result.totalCounts = int.tryParse(dto[3]) ?? 0;

    if (result.isSuccess) {
      result.items = OutletListOutputDto.create(dto[4].split("^"));
    } else {
      result.description = dto[4];
    }

    return result;
  }
}
