import 'package:cnbmerchantapp/services/clients/apps/xcore.dart';

class DeactivateOutletOutputDto extends ResultStatusOutputDto {
  static DeactivateOutletOutputDto create(List<String> dto) {
    var result = DeactivateOutletOutputDto();
    result.statusCode = dto[0];
    result.isSuccess = dto[0] == "00";
    result.description = dto[1];
    return result;
  }
}
