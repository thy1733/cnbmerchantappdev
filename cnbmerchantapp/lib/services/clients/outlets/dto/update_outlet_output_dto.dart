import 'package:cnbmerchantapp/services/clients/xcore.dart';

class UpdateOutletOutputDto extends ResultStatusOutputDto {
  static UpdateOutletOutputDto create(List<String> dto) {
    var result = UpdateOutletOutputDto();
    result.statusCode = dto[0];
    result.isSuccess = dto[0] == "00";
    result.description = dto[3];

    return result;
  }
}
