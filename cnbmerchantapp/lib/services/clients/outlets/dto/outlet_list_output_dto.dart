import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class OutletListOutputDto {
  final _id = "".obs;
  String get id => _id.value;
  set id(String value) => _id.value = value;

  final _creationDate = "".obs;
  String get creationDate => _creationDate.value;
  set creationDate(String value) => _creationDate.value = value;

  final _accountNumber = "".obs;
  String get accountNumber => _accountNumber.value;
  set accountNumber(String value) => _accountNumber.value = value;

  final _businessId = "".obs;
  String get businessId => _businessId.value;
  set businessId(String value) => _businessId.value = value;

  final _outletName = "".obs;
  String get outletName => _outletName.value;
  set outletName(String value) => _outletName.value = value;

  final _outletLogo = "".obs;
  String get outletLogo => _outletLogo.value;
  set outletLogo(String value) => _outletLogo.value = value;

  final _status = "".obs;
  String get status => _status.value;
  set status(String value) => _status.value = value;

  final _locationName = "".obs;
  String get locationName => _locationName.value;
  set locationName(String value) => _locationName.value = value;

  final _locationAddress = "".obs;
  String get locationAddress => _locationAddress.value;
  set locationAddress(String value) => _locationAddress.value = value;

  final _latitude = 0.0.obs;
  double get latitude => _latitude.value;
  set latitude(double value) => _latitude.value = value;

  final _longitude = 0.0.obs;
  double get longitude => _longitude.value;
  set longitude(double value) => _longitude.value = value;

  final _cashiers = <CashierItemListOutputModel>[].obs;
  List<CashierItemListOutputModel> get cashiers => _cashiers;
  set casheirs(List<CashierItemListOutputModel> value) =>
      _cashiers.value = value;

  final _isPrimary = false.obs;
  bool get isPrimary => _isPrimary.value;
  set isPrimary(bool value) => _isPrimary.value = value;

  final _telegramGroupId = "".obs;
  String get telegramGroupId => _telegramGroupId.value;
  set telegramGroupId(String value) => _telegramGroupId.value = value;

  static List<OutletListOutputDto> create(List<String> dto) {
    var result = <OutletListOutputDto>[];
    try {
      if (dto.isNotEmpty) {
        for (var i = 0; i < dto.length; i++) {
          var data = dto[i].split("!");
          var output = OutletListOutputDto();
          output.id = data[0];
          output.creationDate = data[1];
          output.accountNumber = data[2];
          output.businessId = data[3];
          output.outletName = data[4];
          output.outletLogo = data[5];
          output.status = data[6];
          output.locationName = data[7];
          output.locationAddress = data[8];
          output.latitude = double.tryParse(data[9]) ?? 0.0;
          output.longitude = double.tryParse(data[10]) ?? 0.0;
          output.casheirs = data[11].isNotEmpty
              ? data[11]
                  .split(":delimit:")
                  .map((e) => CashierItemListOutputModel.getCashierList(
                      e.split("["), output.businessId))
                  .where((element) =>
                      element.status.toLowerCase() !=
                      CashierStatus.deleted.displayTitle.toLowerCase())
                  .toList()
              : [];
          output.isPrimary = data[12].toLowerCase() == "true";
          output.telegramGroupId = data[13];
          result.add(output);
        }
      }
    } catch (e) {
      debugPrint("Error: ${e.toString()}");
    }
    return result;
  }
}
