import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class OutletDetailOutputDto extends ResultStatusOutputDto {
  final _outletId = "".obs;
  String get outletId => _outletId.value;
  set outletId(String value) => _outletId.value = value;

  final _creationDate = "".obs;
  String get creationDate => _creationDate.value;
  set creationDate(String value) => _creationDate.value = value;

  final _accountNumber = "".obs;
  String get accountNumber => _accountNumber.value;
  set accountNumber(String value) => _accountNumber.value = value;

  final _businessId = "".obs;
  String get businessId => _businessId.value;
  set businessId(String value) => _businessId.value = value;

  final _outletName = "".obs;
  String get outletName => _outletName.value;
  set outletName(String value) => _outletName.value = value;

  final _outletLogo = "".obs;
  String get outletLogo => _outletLogo.value;
  set outletLogo(String value) => _outletLogo.value = value;

  final _outletLogoId = "".obs;
  String get outletLogoId => _outletLogoId.value;
  set outletLogoId(String value) => _outletLogoId.value = value;

  final _status = "".obs;
  String get status => _status.value;
  set status(String value) => _status.value = value;

  final _locationName = "".obs;
  String get locationName => _locationName.value;
  set locationName(String value) => _locationName.value = value;

  final _locationAddress = "".obs;
  String get locationAddress => _locationAddress.value;
  set locationAddress(String value) => _locationAddress.value = value;

  final _latitude = 0.0.obs;
  double get latitude => _latitude.value;
  set latitude(double value) => _latitude.value = value;

  final _longitude = 0.0.obs;
  double get longitude => _longitude.value;
  set longitude(double value) => _longitude.value = value;

  final _cashiers = <CashierItemListOutputModel>[].obs;
  List<CashierItemListOutputModel> get cashiers => _cashiers;
  set cashiers(List<CashierItemListOutputModel> value) =>
      _cashiers.value = value;

  final _isPrimary = false.obs;
  bool get isPrimary => _isPrimary.value;
  set isPrimary(bool value) => _isPrimary.value = value;

  final _telegramToken = "".obs;
  String get telegramToken => _telegramToken.value;
  set telegramToken(String value) => _telegramToken.value = value;

  final _telegramGroupId = "".obs;
  String get telegramGroupId => _telegramGroupId.value;
  set telegramGroupId(String value) => _telegramGroupId.value = value;

  final _telegramGroupName = "".obs;
  String get telegramGroupName => _telegramGroupName.value;
  set telegramGroupName(String value) => _telegramGroupName.value = value;

  static OutletDetailOutputDto create(List<String> dto) {
    var result = OutletDetailOutputDto();
    result.statusCode = dto[0];
    result.isSuccess = dto[0] == "00";
    result.description = dto[1];

    List<String> data = dto[2].split("!");
    result.outletId = data[0];
    result.creationDate = data[1];
    result.accountNumber = data[2];
    result.businessId = data[3];
    result.outletName = data[4];
    result.outletLogo = data[5].isEmpty
        ? ""
        : data[5].downloadUrl(
            UploadProfileType.outletProfile, result.businessId, data[5]);
    result.outletLogoId = data[5];
    result.status = data[6];
    result.locationName = data[7];
    result.locationAddress = data[8];
    result.latitude = double.tryParse(data[9]) ?? 0.0;
    result.longitude = double.tryParse(data[10]) ?? 0.0;
    result.cashiers = data[11].isNotEmpty
        ? data[11]
            .split(":delimit:")
            .map((e) => CashierItemListOutputModel.getCashierList(
                e.split("["), result.businessId))
            .where((element) =>
                element.status.toLowerCase() !=
                CashierStatus.deleted.displayTitle.toLowerCase())
            .toList()
        : [];
    result.isPrimary = data[12] == "true";
    result.telegramToken = data[13];
    result.telegramGroupId = data[14];
    result.telegramGroupName = data[15];

    return result;
  }
}
