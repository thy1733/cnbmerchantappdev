import 'package:cnbmerchantapp/core.dart';
import 'package:cnbmerchantapp/services/clients/outlets/dto/outlet_detail_output_dto.dart';

class OutletService implements IOutletService {
  final AppService _appService = AppService();

  @override
  Future<ResultModel<OutletListOutputModel>> getListOutlet(
      OutletListInputModel input) async {
    var resultModel = ResultModel<OutletListOutputModel>();
    try {
      var inputModel =
          ApiInputModel.create(OutletListInputDto.toMapping(input));
      var inputDto = ApiInputDto.toJson(inputModel);
      var resultDto =
          await _appService.postsAsync(input: inputDto, tokenRequired: false);
      if (resultDto.result != null && resultDto.success) {
        return resultModel.create(
            OutletListOutputModel.create(
                OutletItemListOutputDto.create(resultDto.response)),
            true,
            null,
            false);
      } else {
        return resultModel.create(
            OutletListOutputModel.create(
                OutletItemListOutputDto.create(resultDto.response)),
            false,
            null,
            true);
      }
    } catch (e) {
      return resultModel.create(
          null, false, ApiResultError(message: e.toString()), true);
    }
  }

  @override
  Future<ResultModel<SetupOutletOutputModel>> createOutlet(
      SetupOutletInputModel input) async {
    var resultModel = ResultModel<SetupOutletOutputModel>();

    try {
      var inputModel =
          ApiInputModel.create(SetupOutletInputDto.toMapping(input));
      var inputDto = ApiInputDto.toJson(inputModel);
      var resultDto =
          await _appService.postsAsync(input: inputDto, tokenRequired: false);
      if (resultDto.result != null && resultDto.success) {
        return resultModel.create(
            SetupOutletOutputModel.create(
                SetupOutletOutputDto.create(resultDto.response)),
            true,
            null,
            false);
      } else {
        return resultModel.create(
            SetupOutletOutputModel.create(
                SetupOutletOutputDto.create(resultDto.response)),
            false,
            null,
            true);
        // return resultModel.create(null, false, resultDto.result.error, true);
      }
    } catch (e) {
      return resultModel.create(
          null, false, ApiResultError(message: e.toString()), true);
    }
  }

  @override
  Future<ResultModel<UpdateOutletOutputModel>> updateOutlet(
      UpdateOutletInputModel input) async {
    var resultModel = ResultModel<UpdateOutletOutputModel>();

    try {
      var inputModel =
          ApiInputModel.create(UpdateOutletInputDto.toMapping(input));
      var inputDto = ApiInputDto.toJson(inputModel);
      var resultDto =
          await _appService.postsAsync(input: inputDto, tokenRequired: false);
      if (resultDto.result != null && resultDto.success) {
        return resultModel.create(
            UpdateOutletOutputModel.create(
                UpdateOutletOutputDto.create(resultDto.response)),
            true,
            null,
            false);
      } else {
        return resultModel.create(
            UpdateOutletOutputModel.create(
                UpdateOutletOutputDto.create(resultDto.response)),
            false,
            null,
            true);
      }
    } catch (e) {
      return resultModel.create(
          null, false, ApiResultError(message: e.toString()), true);
    }
  }

  @override
  Future<ResultModel<ActivateOutletOutputModel>> activateOutlet(
      ActivateOutletInputModel input) async {
    var resultModel = ResultModel<ActivateOutletOutputModel>();

    try {
      var inputModel =
          ApiInputModel.create(ActivateOutletInputDto.toMapping(input));
      var inputDto = ApiInputDto.toJson(inputModel);
      var resultDto =
          await _appService.postsAsync(input: inputDto, tokenRequired: false);
      if (resultDto.result != null && resultDto.success) {
        return resultModel.create(
            ActivateOutletOutputModel.create(
                ActivateOutletOutputDto.create(resultDto.response)),
            true,
            null,
            false);
      } else {
        return resultModel.create(null, false, resultDto.result.error, true);
      }
    } catch (e) {
      return resultModel.create(
          null, false, ApiResultError(message: e.toString()), true);
    }
  }

  @override
  Future<ResultModel<DeactivateOutletOutputModel>> deactivateOutlet(
      DeactivateOutletInputModel input) async {
    var resultModel = ResultModel<DeactivateOutletOutputModel>();

    try {
      var inputModel =
          ApiInputModel.create(DeactivateOutletInputDto.toMapping(input));
      var inputDto = ApiInputDto.toJson(inputModel);
      var resultDto =
          await _appService.postsAsync(input: inputDto, tokenRequired: false);
      if (resultDto.result != null && resultDto.success) {
        return resultModel.create(
            DeactivateOutletOutputModel.create(
                DeactivateOutletOutputDto.create(resultDto.response)),
            true,
            null,
            false);
      } else {
        return resultModel.create(null, false, resultDto.result.error, true);
      }
    } catch (e) {
      return resultModel.create(
          null, false, ApiResultError(message: e.toString()), true);
    }
  }

  @override
  Future<ResultModel<OutletDetailOutputModel>> getOutletDetail(
      OutletDetailInputModel input) async {
    var resultModel = ResultModel<OutletDetailOutputModel>();

    try {
      var inputModel =
          ApiInputModel.create(OutletDetailInputDto.toMapping(input));
      var inputDto = ApiInputDto.toJson(inputModel);
      var resultDto =
          await _appService.postsAsync(input: inputDto, tokenRequired: false);
      if (resultDto.result != null && resultDto.success) {
        return resultModel.create(
            OutletDetailOutputModel.create(
                OutletDetailOutputDto.create(resultDto.response)),
            true,
            null,
            false);
      } else {
        return resultModel.create(null, false, resultDto.result.error, true);
      }
    } catch (e) {
      return resultModel.create(
          null, false, ApiResultError(message: e.toString()), true);
    }
  }
}
