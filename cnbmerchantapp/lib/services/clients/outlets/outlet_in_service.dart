import 'package:cnbmerchantapp/core.dart';

abstract class IOutletService {
  Future<ResultModel<OutletListOutputModel>> getListOutlet(
      OutletListInputModel input);

  Future<ResultModel<SetupOutletOutputModel>> createOutlet(
      SetupOutletInputModel input);

  Future<ResultModel<UpdateOutletOutputModel>> updateOutlet(
      UpdateOutletInputModel input);

  Future<ResultModel<DeactivateOutletOutputModel>> deactivateOutlet(
      DeactivateOutletInputModel input);

  Future<ResultModel<ActivateOutletOutputModel>> activateOutlet(
      ActivateOutletInputModel input);

      Future<ResultModel<OutletDetailOutputModel>> getOutletDetail(
      OutletDetailInputModel input);
}
