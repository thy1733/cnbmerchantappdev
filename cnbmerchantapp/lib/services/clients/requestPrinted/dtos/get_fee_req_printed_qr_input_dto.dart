import 'package:cnbmerchantapp/core.dart';

class GetFeeReqPrintedQrInputDto {
  String mcId = '';
  String businessId = '';
  String outletId = '';
  String selectedRecieveOption = '';
  String selectedBankOfficeLocationId = '';
  String deliveryLocationName = '';
  String deliveryLocationAddress = '';
  String deliveryLatitude = '';
  String deliveryLongitude = '';
  String standee = '';
  String stickerBillPad = '';
  String lanyard = '';
  String stickerA6 = '';
  String remark = '';

  static GetFeeReqPrintedQrInputDto create(ReqPrintedQrInputModel input) {
    var result = GetFeeReqPrintedQrInputDto();
    result.mcId = AppUserTempInstant.appCacheUser.mCID;
    result.businessId = input.selectedBusiness.id;
    result.outletId = input.selectedOutlet.id;
    result.selectedRecieveOption = input.selectedRecieveOption.name;
    result.selectedBankOfficeLocationId = input.pickUpLocation.id;
    result.deliveryLocationName = input.locationDeliver.locationName.replaceAll(",", ";");
    result.deliveryLocationAddress = input.locationDeliver.address.replaceAll(",", ";");
    result.deliveryLatitude = "${input.locationDeliver.latitude}";
    result.deliveryLongitude = "${input.locationDeliver.longitude}";
    result.standee = "${input.standee}";
    result.stickerBillPad = "${input.stickerBillPad}";
    result.lanyard = "${input.lanyard}";
    result.stickerA6 = "${input.stickerA6}";
    result.remark = input.remark;
    return result;
  }

  String toMapping() {
    var result =
        "${RoutineName.getFeeRequestPrintedQr}[[$mcId[$businessId[$outletId[$selectedRecieveOption[$selectedBankOfficeLocationId[$deliveryLocationName[$deliveryLocationAddress[$deliveryLatitude[$deliveryLongitude[$standee[$stickerBillPad[$lanyard[$stickerA6[$remark";
    return result;
  }
}
