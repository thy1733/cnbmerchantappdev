import 'package:cnbmerchantapp/core.dart';

class ReqPrintedQrInputDto {
  String mcId = '';
  String businessId = '';
  String outletId = '';
  String selectedRecieveOption = '';
  String selectedBankOfficeLocationId = '';
  String deliveryLocationName = '';
  String deliveryLocationAddress = '';
  String deliveryLatitude = '';
  String deliveryLongitude = '';
  String standee = '';
  String stickerBillPad = '';
  String lanyard = '';
  String stickerA6 = '';
  String printFee = '';
  String deliveryFee = '';
  String totalFee = '';
  String remark = '';

  static ReqPrintedQrInputDto create(ReqPrintedQrInputModel input) {
    var result = ReqPrintedQrInputDto();
    result.mcId = AppUserTempInstant.appCacheUser.mCID;
    result.businessId = input.selectedBusiness.id;
    result.outletId = input.selectedOutlet.id;
    result.selectedRecieveOption = input.selectedRecieveOption.name;
    result.selectedBankOfficeLocationId = input.pickUpLocation.id;
    result.deliveryLocationName = input.locationDeliver.locationName.replaceAll(",", ";");
    result.deliveryLocationAddress = input.locationDeliver.address.replaceAll(",", ";");
    result.deliveryLatitude = "${input.locationDeliver.latitude}";
    result.deliveryLongitude = "${input.locationDeliver.longitude}";
    result.standee = "${input.standee}";
    result.stickerBillPad = "${input.stickerBillPad}";
    result.lanyard = "${input.lanyard}";
    result.stickerA6 = "${input.stickerA6}";
    result.printFee = "${input.printFee}";
    result.deliveryFee = "${input.deliveryFee}";
    result.totalFee = "${input.totalFee}";
    result.remark = input.remark;
    return result;
  }

  String toMapping() {
    var result =
        "${RoutineName.requestPrintedQr}[[$mcId[$businessId[$outletId[$selectedRecieveOption[$selectedBankOfficeLocationId[$deliveryLocationName[$deliveryLocationAddress[$deliveryLatitude[$deliveryLongitude[$standee[$stickerBillPad[$lanyard[$stickerA6[$printFee[$deliveryFee[$totalFee[$remark";
    return result;
  }
}
