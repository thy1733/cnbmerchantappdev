class DeliveryLocationOutputDto {
  String locationName="";
  String locationAddress="";
  double latitude=0.0;
  double longitude=0.0;

  DeliveryLocationOutputDto({
    this.locationName = "",
    this.locationAddress="",
    this.latitude=0,
    this.longitude=0,
  });

  factory DeliveryLocationOutputDto.create(List<String> dtos) =>
      DeliveryLocationOutputDto(
          locationName: dtos[0],
          locationAddress: dtos[1],
          latitude: double.tryParse(dtos[2])??0.0,
          longitude:double.tryParse(dtos[3])??0.0,
          );
}
