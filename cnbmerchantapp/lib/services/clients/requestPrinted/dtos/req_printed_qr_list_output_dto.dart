import 'package:cnbmerchantapp/core.dart';

class ReqPrintedQrListOutputDto extends ResultStatusOutputDto {
  ReqPrintedQrListOutputDto();

  List<ReqPrintedQrDetailOutputDto> items = <ReqPrintedQrDetailOutputDto>[];

  static ReqPrintedQrListOutputDto create(List<String> dto) {
    var result = ReqPrintedQrListOutputDto();
    result.statusCode = dto[0];
    result.isSuccess = result.statusCode == "00";
    result.description = dto[2];
    var itemsRespone = dto[3];
    if (itemsRespone.isNotEmpty) {
      var itemsList = itemsRespone.split("^");
      for (var item in itemsList) {
        result.items.add(ReqPrintedQrDetailOutputDto.create(item.split("!")));
      }
    }
    return result;
  }
}
