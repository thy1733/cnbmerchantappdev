

import 'package:cnbmerchantapp/core.dart';

class ReqPrintedQrDetailOutputDto {
  String requestId = '';
  String requestStatus = '';
  String businessId = '';
  String businessName = '';
  String businessTypeId = '';
  String linkAccount = '';
  String outletName = '';
  String outletLogoUrl = '';
  String selectedRecieveOption = '';
  String selectedBankOfficeId = '';
  String locationName = '';
  String locationAddress = '';
  double latitude = 0.0;
  double longitude = 0.0;
  // BankOfficeOutputDto pickUpLocation = BankOfficeOutputDto();
  // DeliveryLocationOutputDto deliveryLocation = DeliveryLocationOutputDto();
  int standee = 0;
  int stickerBillPad = 0;
  int lanyard = 0;
  int stickerA6 = 0;
  double printFee = 0.0;
  double deliveryFee = 0.0;
  double totalFee = 0.0;
  String remark = '';
  int startWorkingHour = 0;
  int stopWorkingHour = 0;
  String requestDate = '0';
  String lastUpdateDate = '0';

  ReqPrintedQrDetailOutputDto();

  static ReqPrintedQrDetailOutputDto create(List<String> dto) {
    var result = ReqPrintedQrDetailOutputDto();

    if (dto.isNotEmpty) {
      result.requestId = dto[0];
      result.requestStatus = dto[1];
      result.businessName = dto[2];
      result.businessId = dto[3];
      result.businessTypeId = dto[4];
      result.linkAccount = dto[5];
      result.outletName = dto[6];
      result.outletLogoUrl = dto[7].downloadUrl(UploadProfileType.outletProfile, result.businessId, dto[7]);
      result.selectedRecieveOption = dto[8];
      result.selectedBankOfficeId = dto[9];
      result.locationName = dto[10].replaceAll(";", ",");
      result.locationAddress = dto[11].replaceAll(";", ",");
      result.latitude = _emtyResultDoubleChk(dto[12]);
      result.longitude = _emtyResultDoubleChk(dto[13]);
      
      result.standee = int.tryParse(dto[14]) ?? 0;
      result.stickerBillPad = int.tryParse(dto[15]) ?? 0;
      result.lanyard = int.tryParse(dto[16]) ?? 0;
      result.stickerA6 = int.tryParse(dto[17]) ?? 0;
      result.printFee = double.tryParse(dto[18]) ?? 0;
      result.deliveryFee = double.tryParse(dto[19]) ?? 0;
      result.totalFee = double.tryParse(dto[20]) ?? 0;
      result.remark = dto[21];
      // result.startWorkingHour = int.tryParse(dto[21]) ?? 0;
      // result.stopWorkingHour = int.tryParse(dto[26]) ?? 0;
      result.requestDate = dto[22];
      result.lastUpdateDate = dto[23];
    }
    return result;
  }

  static double _emtyResultDoubleChk(value) {
    return "$value".toUpperCase() == "NULL"
        ? 0
        : double.tryParse("$value") ?? 0;
  }
}
