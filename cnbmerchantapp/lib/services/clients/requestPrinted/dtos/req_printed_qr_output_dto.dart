import 'package:cnbmerchantapp/core.dart';

class ReqPrintedQrOutputDto extends ResultStatusOutputDto {
  ReqPrintedQrOutputDto();

  static ReqPrintedQrOutputDto create(List<String> dto) {
    var result = ReqPrintedQrOutputDto();
    result.statusCode = dto[0];
    result.isSuccess = result.statusCode == "00";
    result.description = dto[1];
    
    return result;
  }
}
