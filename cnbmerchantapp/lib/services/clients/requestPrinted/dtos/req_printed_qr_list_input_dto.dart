import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class ReqPrintedQrListInputDto {
  String mcId = "";
  ReqPrintedQrListInputDto({this.mcId = ""});

  static String toMapping(String mcId) {
    String locale = Get.locale?.languageCode ?? LanguageOption.conCodeEn;
    var result = "${RoutineName.listRequestPrintedQr}[[$mcId[$locale";
    return result;
  }
}
