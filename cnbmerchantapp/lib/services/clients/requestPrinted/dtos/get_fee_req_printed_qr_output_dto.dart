import 'package:cnbmerchantapp/core.dart';

class GetFeeReqPrintedQrOutputDto extends ResultStatusOutputDto {
  GetFeeReqPrintedQrOutputDto();

  double printFee = 0.0;
  double deliveryFee = 0.0;
  double totalFee = 0.0;

  static GetFeeReqPrintedQrOutputDto create(List<String> dto) {
    var result = GetFeeReqPrintedQrOutputDto();
    result.statusCode = dto[0];
    result.isSuccess = result.statusCode == "00";
    result.description = dto[1];
    result.printFee = double.tryParse(dto[2])??0.0;
    result.deliveryFee = double.tryParse(dto[3])??0.0;
    result.totalFee = double.tryParse(dto[4])??0.0;
    
    return result;
  }
}
