import 'package:cnbmerchantapp/core.dart';

class ReqPrintedQrService implements IReqPrintedQrService {
  final AppService _appService = AppService();
  @override
  Future<ResultModel<ReqPrintedQrListOutputModel>> getListReqPrintedQr() async {
    var resultModel = ResultModel<ReqPrintedQrListOutputModel>();
    try {
      var inputModel = ApiInputModel.create(ReqPrintedQrListInputDto.toMapping(
          AppUserTempInstant.appCacheUser.mCID));
      var inputDto = ApiInputDto.toJson(inputModel);
      var resultDto =
          await _appService.postsAsync(input: inputDto, tokenRequired: false);
      if (resultDto.result != null && resultDto.success) {
        return resultModel.create(
            ReqPrintedQrListOutputModel.create(
                ReqPrintedQrListOutputDto.create(resultDto.response)),
            true,
            null,
            false);
      } else {
        return resultModel.create(null, false, resultDto.result.error, true);
      }
    } catch (e) {
      return resultModel.create(
          null, false, ApiResultError(message: e.toString()), true);
    }
  }

  @override
  Future<ResultModel<GetFeeReqPrintedQrOutputModel>> getFeeReqPrintedQr(
    ReqPrintedQrInputModel input,
  ) async {
    var resultModel = ResultModel<GetFeeReqPrintedQrOutputModel>();
    try {
      var inputModel = ApiInputModel.create(
        GetFeeReqPrintedQrInputDto.create(input).toMapping(),
      );
      var inputDto = ApiInputDto.toJson(inputModel);
      var resultDto = await _appService.postsAsync(
        input: inputDto,
        tokenRequired: false,
      );
      if (resultDto.result != null && resultDto.success) {
        return resultModel.create(
          GetFeeReqPrintedQrOutputModel.create(
              GetFeeReqPrintedQrOutputDto.create(resultDto.response)),
          true,
          null,
          false,
        );
      } else {
        return resultModel.create(null, false, resultDto.result.error, true);
      }
    } catch (e) {
      return resultModel.create(
          null, false, ApiResultError(message: e.toString()), true);
    }
  }

  @override
  Future<ResultModel<ReqPrintedQrOutputModel>> reqPrintedQr(
      ReqPrintedQrInputModel input) async {
    var resultModel = ResultModel<ReqPrintedQrOutputModel>();
    try {
      var inputModel = ApiInputModel.create(
        ReqPrintedQrInputDto.create(input).toMapping(),
      );
      var inputDto = ApiInputDto.toJson(inputModel);
      var resultDto = await _appService.postsAsync(
        input: inputDto,
        tokenRequired: false,
      );
      if (resultDto.result != null && resultDto.success) {
        return resultModel.create(
            ReqPrintedQrOutputModel.create(
              ReqPrintedQrOutputDto.create(resultDto.response),
            ),
            true,
            null,
            false);
      } else {
        return resultModel.create(null, false, resultDto.result.error, true);
      }
    } catch (e) {
      return resultModel.create(
          null, false, null, true);
    }
  }
}
