import 'package:cnbmerchantapp/core.dart';

abstract class IReqPrintedQrService {
  Future<ResultModel<ReqPrintedQrListOutputModel>> getListReqPrintedQr();
  Future<ResultModel<GetFeeReqPrintedQrOutputModel>> getFeeReqPrintedQr(
      ReqPrintedQrInputModel input);
  Future<ResultModel<ReqPrintedQrOutputModel>> reqPrintedQr(
      ReqPrintedQrInputModel input);
}
