import 'package:cnbmerchantapp/core.dart';

abstract class IAppFileUploadService {
  Future<FileUploadOutputModel> uploadFileAsync({required UploadFileInputDto input});
  Future<void> deleteFileAsync({required UploadFileInputDto input});
}
