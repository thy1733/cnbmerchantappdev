import 'package:get/get.dart';

class FileUploadOutputDto {
  
  final _fileName = ''.obs;
  String get fileName => _fileName.value;
  set fileName(String value) => _fileName.value = value;
  
  final _filePath = ''.obs;
  String get filePath => _filePath.value;
  set filePath(String value) => _filePath.value = value;
}