import 'dart:io';
import 'package:cnbmerchantapp/core.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';

class AppFileUploadService implements IAppFileUploadService {
 
  @override
  Future<FileUploadOutputModel> uploadFileAsync(
      {required UploadFileInputDto input}) async {
    final file = File(input.filePath);
    var metaHeader =
        input.metaData.isEmpty ? "file-photo-path" : input.metaData;
// Create the file metadata
    final metadata = SettableMetadata(
        contentType: "image/jpeg",
        customMetadata: {metaHeader: input.filePath});

// Create a reference to the Firebase Storage bucket
    final storageRef = FirebaseStorage.instance.ref();

// Upload file and metadata to the path 'images/mountains.jpg'
    final uploadTask = storageRef
        .child(input.uploadType.name)
        .child(input.id)
        .child('/${input.fileName}')
        .putFile(file, metadata);

// Listen for state changes, errors, and completion of the upload.
    uploadTask.snapshotEvents.listen((TaskSnapshot taskSnapshot) {
      switch (taskSnapshot.state) {
        case TaskState.running:
          final progress =
              100.0 * (taskSnapshot.bytesTransferred / taskSnapshot.totalBytes);
          debugPrint("Upload is $progress% complete.");
          break;
        case TaskState.paused:
          debugPrint("Upload is paused.");
          break;
        case TaskState.canceled:
          debugPrint("Upload was canceled");
          break;
        case TaskState.error:
          // Handle unsuccessful uploads
          debugPrint("Upload was unsuccessful");
          break;
        case TaskState.success:
          // Handle successful uploads on complete
          debugPrint("Upload was successful");
          break;
      }
    });

    // Create a Reference to the file
    var output = FileUploadOutputModel();
    output.fileName = input.fileName;
    output.filePath = input.filePath;
    return Future.value(output);
  }

  @override
  Future<void> deleteFileAsync({required UploadFileInputDto input}) async {
    // Create a Reference to the file
    Reference ref = FirebaseStorage.instance
        .ref()
        .child(input.uploadType.name)
        .child(input.id)
        .child('/${input.fileName}');
    await ref.delete();
  }
}
