import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class NotificationItemListOutputDto extends ResultStatusOutputDto {
  final _totalCount = "".obs;
  String get totalCount => _totalCount.value;
  set totalCount(String value) => _totalCount.value = value;

  final _items = <NotificationListOutputDto>[].obs;
  List<NotificationListOutputDto> get items => _items;
  set items(List<NotificationListOutputDto> value) => _items.value = value;

  NotificationItemListOutputDto();

  factory NotificationItemListOutputDto.cteate(List<String> dto) {
    var result = NotificationItemListOutputDto();
    result.statusCode = dto[0];
    result.isSuccess = dto[0] == "00";
    result.description = dto[1];
    result.totalCount = dto[2];
    result.items = NotificationListOutputDto.cteate(dto[3].split("^"));
    return result;
  }
}
