import 'package:get/get.dart';

class NotificationListOutputDto {
  final _transactionId = "".obs;
  String get transactionId => _transactionId.value;
  set transactionId(String value) => _transactionId.value = value;

  final _accountName = "".obs;
  String get accountName => _accountName.value;
  set accountName(String value) => _accountName.value = value;

  final _dateTime = "".obs;
  String get dateTime => _dateTime.value;
  set dateTime(String value) => _dateTime.value = value;

  final _currency = "".obs;
  String get currency => _currency.value;
  set currency(String value) => _currency.value = value;

  final _amount = "".obs;
  String get amount => _amount.value;
  set amount(String value) => _amount.value = value;

  final _receiverAccountNumber = "".obs;
  String get receiverAccountNumber => _receiverAccountNumber.value;
  set receiverAccountNumber(String value) => _receiverAccountNumber.value = value;

  final _transactionType = "".obs;
  String get transactionType => _transactionType.value;
  set transactionType(String value) => _transactionType.value = value;

  final _paymentMethod = "".obs;
  String get paymentMethod => _paymentMethod.value;
  set paymentMethod(String value) => _paymentMethod.value = value;

  final _status = "".obs;
  String get status => _status.value;
  set status(String value) => _status.value = value;

  final _isRead = "".obs;
  String get isRead => _isRead.value;
  set isRead(String value) => _isRead.value = value;

  static List<NotificationListOutputDto> cteate(List<String> dto) {
    var outputs = <NotificationListOutputDto>[];
    if (dto.isNotEmpty) {
      for (var i = 0; i < dto.length; i++) {
        final data = dto[i].split("!");
        var output = NotificationListOutputDto();
        output.transactionId = data[0];
        output.accountName = data[1];
        output.dateTime = data[2];
        output.currency = data[3];
        output.amount = data[4];
        output.receiverAccountNumber = data[5];
        output.transactionType = data[6];
        output.paymentMethod = data[7];
        output.status = data[8];
        output.isRead = data[9];
        outputs.add(output);
      }
    }
    return outputs;
  }
}
