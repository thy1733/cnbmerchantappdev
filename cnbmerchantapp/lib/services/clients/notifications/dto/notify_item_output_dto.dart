
class NotifyItemOutputDto {
    NotifyItemOutputDto({
        this.acceptedVia,
        this.amount,
        this.businessId,
        this.cashierId,
        this.cashierName,
        this.clientName,
        this.currencyCode,
        this.customerAc,
        this.customerName,
        this.dateTime,
        this.discount,
        this.isAllowPrint,
        this.outletId,
        this.paidAmount,
        this.payType,
        this.paymentMethod,
        this.printCount,
        this.status,
        this.statusMessage,
        this.tip,
        this.txrId,
        this.bankref,
    });

    String? acceptedVia;
    String? amount;
    String? businessId;
    String? cashierId;
    String? cashierName;
    String? clientName;
    String? currencyCode;
    String? customerAc;
    String? customerName;
    String? dateTime;
    String? discount;
    String? isAllowPrint;
    String? outletId;
    String? paidAmount;
    String? payType;
    String? paymentMethod;
    String? printCount;
    String? status;
    String? statusMessage;
    String? tip;
    String? txrId;
    String? bankref;

    factory NotifyItemOutputDto.fromJson(Map json){
      try {
       return NotifyItemOutputDto(
        acceptedVia: json["AcceptedVia"] ?? "",
        amount: json["Amount"]?? "",
        businessId: json["BusinessId"]?? "",
        cashierId: json["CashierId"]?? "",
        cashierName: json["CashierName"]?? "",
        clientName: json["ClientName"]?? "",
        currencyCode: json["CurrencyCode"]?? "",
        customerAc: json["CustomerAc"]?? "",
        customerName: json["CustomerName"]?? "",
        dateTime: json["DateTime"]?? "",
        discount: json["Discount"]?? "",
        isAllowPrint: json["IsAllowPrint"]?? "",
        outletId: json["OutletId"]?? "",
        paidAmount: json["PaidAmount"]?? "",
        payType: json["PayType"]?? "",
        paymentMethod: json["PaymentMethod"]?? "",
        printCount: json["PrintCount"]?? "",
        status: json["Status"]?? "",
        statusMessage: json["StatusMessage"]?? "",
        tip: json["Tip"]?? "",
        txrId: json["TxrID"]?? "",
        bankref: json["bankref"]?? "",
    );
      } catch (e) {
         return NotifyItemOutputDto();
      }
    }
}
