import 'package:cnbmerchantapp/core.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_database/firebase_database.dart';

class NotifyService extends AppService implements INotifyService {
  final _appService = AppService();

  Future<ResultModel<NotificationListOutputModel>> getNotificationList(
      NotificationListInputModel input) async {
    var result = ResultModel<NotificationListOutputModel>();

    try {
      final inputDto = ApiInputDto.toJson(
          ApiInputModel.create(NotificationListInputDto.toMapping(input)));
      final resultDto =
          await _appService.postsAsync(input: inputDto, tokenRequired: false);
      if (resultDto.result != null && resultDto.success) {
        return result.create(
            NotificationListOutputModel.create(
                NotificationItemListOutputDto.cteate(resultDto.response)),
            true,
            null,
            false);
      } else {
        return result.create(null, false, resultDto.result, true);
      }
    } catch (e) {
      return result.create(
          null,
          false,
          ApiResultError(
            message: e.toString(),
          ),
          true);
    }
  }
  
  @override
  Future<ResultModel<List<NotifyItemOutputModel>>> getItemListAsync() async {
    var result = ResultModel<List<NotifyItemOutputModel>>();
    var itemList = <NotifyItemOutputModel>[];
    final db2 = FirebaseDatabase.instanceFor(
            app: Firebase.app(), databaseURL: UrlConstant.fireInstant)
        .ref(InvokeMethodName.bankRefer);

    if (AppUserTempInstant.appCacheUser.userType == 0) {
      var container = await db2.child(InvokeMethodName.bankSecret).get();
      if (container.value != null) {
        var itemMap = container.value! as Map;
        if (itemMap.isNotEmpty) {
          itemMap.forEach((key, parentData) {
            String keyName = key as String;
            if (keyName.contains(AppUserTempInstant.appCacheUser.mCID)) {
              parentData.forEach((k, childData) {
                childData.forEach((lk, lv) => itemList.add(
                    NotifyItemOutputModel.create(
                        NotifyItemOutputDto.fromJson(lv))));
              });
            }
          });
        }
      }
      }else {
        var chidldPath = "${AppUserTempInstant.appCacheUser.businessId.replaceAll(".", "-")}-${AppOutletTempInstant.selectedOutlet.id.replaceAll("${AppUserTempInstant.appCacheUser.businessId}.", "")}-${AppUserTempInstant.appCacheUser.profileId.replaceAll(".", "-")}";
        var snapshot = await db2.child("${InvokeMethodName.bankSecret}/$chidldPath").get();
        if (snapshot.value != null) {
          var itemMap = snapshot.value! as Map;
          if (itemMap.isNotEmpty) {
            itemMap.forEach((k, v) => v.forEach((ck, cv) => itemList.add(
                NotifyItemOutputModel.create(
                    NotifyItemOutputDto.fromJson(cv)))));
          }
        }
    }
    itemList.sort((a, b) => a.dateTime.compareTo(b.dateTime));
    result.result = itemList;
    result.success = true;
    return result;
  }
}
