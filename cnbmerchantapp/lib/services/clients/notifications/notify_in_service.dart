import 'package:cnbmerchantapp/core.dart';

abstract class INotifyService {
      Future<ResultModel<List<NotifyItemOutputModel>>> getItemListAsync();
}
