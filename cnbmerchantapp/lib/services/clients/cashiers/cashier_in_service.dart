import 'package:cnbmerchantapp/core.dart';

abstract class ICashierListService {
  Future<ResultModel<SetupCashierOutputModel>> createCashier(SetupCashierInputModel input);

  Future<ResultModel<CashierDeactivateOutputModel>> updateCashier(SetupCashierInputModel input);

  Future<ResultModel<CashierListOutputModel>> searchCashier(
      CashierListInputModel input);

  Future<ResultModel<CashierListOutputModel>> getListCashier(CashierListInputModel input);

  Future<ResultModel<CashierDeactivateOutputModel>> deactivateCashier(CashierDeactivateInputModel input);

  Future<ResultModel<SetupCashierOutputModel>> renewCashier(CashierDeactivateInputModel input);
}
