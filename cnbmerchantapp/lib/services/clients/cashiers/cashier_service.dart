import 'package:cnbmerchantapp/core.dart';

class CashierService implements ICashierListService {
  final AppService _appService = AppService();

  @override
  Future<ResultModel<SetupCashierOutputModel>> createCashier(
      SetupCashierInputModel input) async {
    var resultModel = ResultModel<SetupCashierOutputModel>();

    try {
      var inputModel =
          ApiInputModel.create(SetupCashierInputDto.toMapping(input));
      var inputDto = ApiInputDto.toJson(inputModel);
      var resultDto =
          await _appService.postsAsync(input: inputDto, tokenRequired: false);
      return resultModel.create(
          SetupCashierOutputModel.create(
              SetupCashierOutputDto.create(resultDto.response)),
          resultDto.success,
          null,
          false);
    } catch (e) {
      return resultModel.create(
          null, false, ApiResultError(message: e.toString()), true);
    }
  }

  @override
  Future<ResultModel<CashierListOutputModel>> getListCashier(
      CashierListInputModel input) async {
    var resultModel = ResultModel<CashierListOutputModel>();
    try {
      var inputModel =
          ApiInputModel.create(CashierListInputDto.toMapping(input));
      var inputDto = ApiInputDto.toJson(inputModel);
      var resultDto =
          await _appService.postsAsync(input: inputDto, tokenRequired: false);
      if (resultDto.result != null && resultDto.success) {
        return resultModel.create(
            CashierListOutputModel.create(
                CashierItemListOutputDto.create(resultDto.response)),
            true,
            null,
            false);
      } else {
        return resultModel.create(
            CashierListOutputModel.create(
                CashierItemListOutputDto.create(resultDto.response)),
            false,
            null,
            true);
      }
    } catch (e) {
      return resultModel.create(
          null, false, ApiResultError(message: e.toString()), true);
    }
  }

  @override
  Future<ResultModel<CashierListOutputModel>> searchCashier(
      CashierListInputModel input) {
    throw UnimplementedError();
  }

  @override
  Future<ResultModel<CashierDeactivateOutputModel>> deactivateCashier(
      CashierDeactivateInputModel input) async {
    var resultModel = ResultModel<CashierDeactivateOutputModel>();
    try {
      var inputModel =
          ApiInputModel.create(CashierDeactivateInputDto.toMapping(input));
      var inputDto = ApiInputDto.toJson(inputModel);
      var resultDto =
          await _appService.postsAsync(input: inputDto, tokenRequired: false);
      if (resultDto.result != null && resultDto.success) {
        return resultModel.create(
            CashierDeactivateOutputModel.create(
                CashierDeactivateOutputDto.create(resultDto.response)),
            true,
            null,
            false);
      } else {
        return resultModel.create(null, false, ApiResultError(message: resultDto.response.length > 2 ? resultDto.response[2].toString() : ""), true);
      }
    } catch (e) {
      return resultModel.create(
          null, false, ApiResultError(message: e.toString()), true);
    }
  }

  @override
  Future<ResultModel<SetupCashierOutputModel>> renewCashier(
      CashierDeactivateInputModel input) async {
    var resultModel = ResultModel<SetupCashierOutputModel>();
    try {
      var inputModel =
          ApiInputModel.create(CashierDeactivateInputDto.toMapping(input));
      var inputDto = ApiInputDto.toJson(inputModel);
      var resultDto =
          await _appService.postsAsync(input: inputDto, tokenRequired: false);
      if (resultDto.result != null && resultDto.success) {
        return resultModel.create(
            SetupCashierOutputModel.create(
                SetupCashierOutputDto.create(resultDto.response)),
            true,
            null,
            false);
      } else {
        return resultModel.create(null, false, ApiResultError(message: resultDto.response.length > 2 ? resultDto.response[2].toString() : ""), true);
      }
    } catch (e) {
      return resultModel.create(
          null, false, ApiResultError(message: e.toString()), true);
    }
  }

  @override
  Future<ResultModel<CashierDeactivateOutputModel>> updateCashier(
      SetupCashierInputModel input) async {
    var resultModel = ResultModel<CashierDeactivateOutputModel>();

    try {
      var inputModel =
          ApiInputModel.create(SetupCashierInputDto.toUpdateMapping(input));
      var inputDto = ApiInputDto.toJson(inputModel);
      var resultDto =
          await _appService.postsAsync(input: inputDto, tokenRequired: false);
      if (resultDto.result != null && resultDto.success) {
        return resultModel.create(
            CashierDeactivateOutputModel.create(
                CashierDeactivateOutputDto.create(resultDto.response)),
            true,
            null,
            false);
      } else {
        return resultModel.create(null, false, ApiResultError(message: resultDto.response.length > 2 ? resultDto.response[2].toString() : ""), true);
      }
    } catch (e) {
      return resultModel.create(
          null, false, ApiResultError(message: e.toString()), true);
    }
  }
}
