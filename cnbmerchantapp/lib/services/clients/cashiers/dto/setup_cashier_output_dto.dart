import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class SetupCashierOutputDto extends ResultStatusOutputDto {
  SetupCashierOutputDto();

  final _inviteCode = ''.obs;
  String get inviteCode => _inviteCode.value;
  set inviteCode(String value) => _inviteCode.value = value;

  final _inviteCodeExpiredDate = ''.obs;
  String get inviteCodeExpiredDate => _inviteCodeExpiredDate.value;
  set inviteCodeExpiredDate(String value) => _inviteCodeExpiredDate.value = value;

  factory SetupCashierOutputDto.create(List<String> dto) {
    var result = SetupCashierOutputDto();
    result.statusCode = dto[0];
    result.isSuccess = dto[0] == "00";

    if(!result.isSuccess) {
      result.description = dto[3];
      return result;
    }

    result.inviteCode = dto[3];
    result.inviteCodeExpiredDate = dto[4];
    return result;
  }
}
