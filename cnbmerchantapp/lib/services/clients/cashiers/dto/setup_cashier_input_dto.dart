import 'package:cnbmerchantapp/core.dart';

class SetupCashierInputDto {
  static String toMapping(SetupCashierInputModel input) {
    String result =
        "${RoutineName.createCashier}[[${input.businessId}[${input.outletsId}[${input.isRefundable}"
        "[${input.refundLimited}[${input.photoUrl}[0${input.phoneNumber.removeWhiteSpace()}[${input.firstName}[${input.lastName}[${input.nationalId}[${input.username}";
    return result;
  }

  static String toUpdateMapping(SetupCashierInputModel input) {
    String result =
        "${RoutineName.updateCashier}[[${input.id}[${input.isRefundable}[${input.refundLimited}[${input.photoUrl}"
        "[0${input.phoneNumber.removeWhiteSpace()}[${input.firstName}[${input.lastName}[${input.nationalId}";
    return result;
  }
}
