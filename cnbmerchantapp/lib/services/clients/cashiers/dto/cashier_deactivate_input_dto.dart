import 'package:cnbmerchantapp/core.dart';

class CashierDeactivateInputDto {
  String phoneNumber = "";
  String cashierId = "";

  static CashierDeactivateInputDto create(CashierDeactivateInputModel input) {
    var result = CashierDeactivateInputDto();
    return result;
  }

  static String toMapping(CashierDeactivateInputModel input) {
    var routeName =  "";
    switch(input.cashierStatus){
      case CashierStatus.deleted:
        routeName = RoutineName.deleteCashier;
        break;
      case CashierStatus.inactive:
        routeName = RoutineName.deactivateCashier;
        break;
      case CashierStatus.active:
        routeName = RoutineName.activateCashier;
        break;
      case CashierStatus.renew:
        routeName = RoutineName.reInviteCashier;
        break;
      case CashierStatus.pending:
        break;
    }
    var result = "$routeName[[${input.phoneNumber}[${input.cashierId}";
    return result;
  }
}
