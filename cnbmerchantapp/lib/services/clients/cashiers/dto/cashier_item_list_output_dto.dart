import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class CashierItemListOutputDto extends ResultStatusOutputDto {
  CashierItemListOutputDto();

  final _totalCount = "".obs;
  String get totalCount => _totalCount.value;
  set totalCount(String value) => _totalCount.value = value;

  final _items = <CashierListOutputDto>[].obs;
  List<CashierListOutputDto> get items => _items;
  set items(List<CashierListOutputDto> value) => _items.value = value;

  factory CashierItemListOutputDto.create(List<String> dto) {
    var result = CashierItemListOutputDto();
    result.statusCode = dto[0];
    result.isSuccess = dto[0] == "00";

    if (result.isSuccess) {
      result.description = dto[1];
      result.totalCount = dto[2];

      result.items = CashierListOutputDto.create(dto[3].split("^"));
      return result;
    } else {
      result.description = dto[2];
      return result;
    }
  }
}
