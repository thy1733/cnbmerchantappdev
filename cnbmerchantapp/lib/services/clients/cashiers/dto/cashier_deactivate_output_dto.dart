import 'package:cnbmerchantapp/core.dart';

class CashierDeactivateOutputDto extends ResultStatusOutputDto {

  static CashierDeactivateOutputDto create(List<String> dto) {
    var result = CashierDeactivateOutputDto();
    if (dto.isNotEmpty) {
      result.statusCode = dto[0];
      result.isSuccess = dto[0] == "00";
      result.description = dto[1];
    }

    return result;
  }
}
