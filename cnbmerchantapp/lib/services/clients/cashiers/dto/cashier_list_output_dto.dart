import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class CashierListOutputDto {
  final _id = "".obs;
  String get id => _id.value;
  set id(String value) => _id.value = value;

  final _creationDate = "".obs;
  String get creationDate => _creationDate.value;
  set creationDate(String value) => _creationDate.value = value;

  final _inviteCode = "".obs;
  String get inviteCode => _inviteCode.value;
  set inviteCode(String value) => _inviteCode.value = value;

  final _inviteCodeExpiredDate = "".obs;
  String get inviteCodeExpiredDate => _inviteCodeExpiredDate.value;
  set inviteCodeExpiredDate(String value) => _inviteCodeExpiredDate.value = value;

  final _status = "".obs;
  String get status => _status.value;
  set status(String value) => _status.value = value;

  final _businessId = "".obs;
  String get businessId => _businessId.value;
  set businessId(String value) => _businessId.value = value;

  final _outlets = <OutletItemListOutputModel>[].obs;
  List<OutletItemListOutputModel> get outlets => _outlets;
  set outlets(List<OutletItemListOutputModel> value) => _outlets.value = value;

  final _isRefundable = "".obs;
  String get isRefundable => _isRefundable.value;
  set isRefundable(String value) => _isRefundable.value = value;

  final _refundLimited = "".obs;
  String get refundLimited => _refundLimited.value;
  set refundLimited(String value) => _refundLimited.value = value;

  final _photoUrl = "".obs;
  String get photoUrl => _photoUrl.value;
  set photoUrl(String value) => _photoUrl.value = value;

  final _phoneNumber = "".obs;
  String get phoneNumber => _phoneNumber.value;
  set phoneNumber(String value) => _phoneNumber.value = value;

  final _firstName = "".obs;
  String get firstName => _firstName.value;
  set firstName(String value) => _firstName.value = value;

  final _lastName = "".obs;
  String get lastName => _lastName.value;
  set lastName(String value) => _lastName.value = value;

  final _nationalId = "".obs;
  String get nationalId => _nationalId.value;
  set nationalId(String value) => _nationalId.value = value;

  final _cacheImage = "".obs;
  String get cacheImage => _cacheImage.value;
  set cacheImage(String value) => _cacheImage.value = value;

  static List<CashierListOutputDto> create(List<String> dto) {
    var result = <CashierListOutputDto>[];
    if (dto.isNotEmpty) {
      for (var i = 0; i < dto.length; i++) {
        var data = dto[i].split("!");
        var input = CashierListOutputDto();
        input.id = data[0];
        input.creationDate = data[1];
        input.inviteCode = data[2];
        input.inviteCodeExpiredDate = data[3];
        input.status = data[4];
        input.businessId = data[5];
        input.outlets = data[6].split(":delimit:")
            .map((e) =>
            OutletItemListOutputModel.getShortOutlet(e.split("["),input.businessId))
            .toList();
        input.isRefundable = data[7];
        input.refundLimited = data[8];
        input.photoUrl = data[9].isNotEmpty && data[9].length < 5 ? "":data[9];
        input.phoneNumber = data[10];
        input.firstName = data[11];
        input.lastName = data[12];
        input.nationalId = data[13];
        input.cacheImage = "${data[0]} ${DateTime.now().toFormatDateString("yyyymmddhhmmsss")}";
        if(input.status.toLowerCase() != "deleted")  {
          result.add(input);
        }
      }
    }
    return result;
  }
}
