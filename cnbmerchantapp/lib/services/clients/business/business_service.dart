import 'package:cnbmerchantapp/core.dart';
import 'package:cnbmerchantapp/models/business/update_business_output_model.dart';

class BusinessService implements IBusinessService {
  final AppService _appService = AppService();

  @override
  Future<ResultModel<SetupBusinessOutputModel>> createBusinessAsync(
      SetupBusinessInputModel input) async {
    var resultModel = ResultModel<SetupBusinessOutputModel>();
    try {
      var inputModel =
          ApiInputModel.create(SetupBusinessInputDto.toMapping(input));
      var inputDto = ApiInputDto.toJson(inputModel);
      var resultDto =
          await _appService.postsAsync(input: inputDto, tokenRequired: false);

      return resultModel.create(
          SetupBusinessOutputModel.create(
              SetupBusinessOutputDto.create(resultDto.response)),
          resultDto.success,
          resultDto.error,
          resultDto.error != null);
    } catch (e) {
      return resultModel.create(
          null, false, ApiResultError(message: e.toString()), true);
    }
  }

  @override
  Future<ResultModel<BusinessTypeListOutputModel>> getListBusinessTypeAsync(
      BusinesTypeListInputModel input) async {
    var resultModel = ResultModel<BusinessTypeListOutputModel>();
    try {
      var inputModel =
          ApiInputModel.create(BusinessTypeListInputDto.toMapping(input));
      var inputDto = ApiInputDto.toJson(inputModel);
      var resultDto =
          await _appService.postsAsync(input: inputDto, tokenRequired: false);
      if (resultDto.result != null && resultDto.success) {
        return resultModel.create(
            BusinessTypeListOutputModel.create(
                BusinessTypeItemListOutputDto.create(resultDto.response)),
            true,
            null,
            false);
      } else {
        return resultModel.create(null, false, resultDto.error, true);
      }
    } catch (e) {
      return resultModel.create(
          null, false, ApiResultError(message: e.toString()), true);
    }
  }

  @override
  Future<ResultModel<DetailBusinessOutputModel>> getDetailBusiness(
      DetailBusinessInputModel input) async {
    var resultModel = ResultModel<DetailBusinessOutputModel>();

    try {
      var inputModel =
          ApiInputModel.create(DetailBusinessInputDto.toMapping(input));
      var inputDto = ApiInputDto.toJson(inputModel);
      var resultDto =
          await _appService.postsAsync(input: inputDto, tokenRequired: false);
      if (resultDto.result != null && resultDto.success) {
        return resultModel.create(
            DetailBusinessOutputModel.create(
                DetailBusinessOutputDto.create(resultDto.response)),
            true,
            null,
            false);
      } else {
        return resultModel.create(null, false, resultDto.result.error, true);
      }
    } catch (e) {
      return resultModel.create(
          null, false, ApiResultError(message: e.toString()), true);
    }
  }

  @override
  Future<ResultModel<ActivateBusinessOutputModel>> activateBusinessAsync(
      ActivateBusinessInputModel input) async {
    var resultModel = ResultModel<ActivateBusinessOutputModel>();

    try {
      var inputModel =
          ApiInputModel.create(ActivateBusinessInputModel.toMapping(input));
      var inputDto = ApiInputDto.toJson(inputModel);
      var resultDto =
          await _appService.postsAsync(input: inputDto, tokenRequired: false);
      if (resultDto.result != null && resultDto.success) {
        return resultModel.create(
            ActivateBusinessOutputModel.create(
                ActivateBusinessOutputDto.create(resultDto.response)),
            true,
            null,
            false);
      } else {
        return resultModel.create(null, false, resultDto.result.error, true);
      }
    } catch (e) {
      return resultModel.create(
          null, false, ApiResultError(message: e.toString()), true);
    }
  }

  @override
  Future<ResultModel<DeactivateBusinessOutputModel>> deactivateBusinessAsync(
      DeactivateBusinessInputModel input) async {
    var resultModel = ResultModel<DeactivateBusinessOutputModel>();

    try {
      var inputModel =
          ApiInputModel.create(DeactivateBusinessInputModel.toMapping(input));
      var inputDto = ApiInputDto.toJson(inputModel);
      var resultDto =
          await _appService.postsAsync(input: inputDto, tokenRequired: false);
      if (resultDto.result != null && resultDto.success) {
        return resultModel.create(
            DeactivateBusinessOutputModel.create(
                DeactivateBusinessOutputDto.create(resultDto.response)),
            true,
            null,
            false);
      } else {
        return resultModel.create(null, false, resultDto.result.error, true);
      }
    } catch (e) {
      return resultModel.create(
          null, false, ApiResultError(message: e.toString()), true);
    }
  }

  @override
  Future<ResultModel<ManageBusinessListOutputModel>> getListBusiness(
      ManageBusinessListInputModel input) async {
    var resultModel = ResultModel<ManageBusinessListOutputModel>();
    try {
      var inputModel =
          ApiInputModel.create(ManageBusinessListInputDto.toMapping(input));
      var inputDto = ApiInputDto.toJson(inputModel);
      var resultDto =
          await _appService.postsAsync(input: inputDto, tokenRequired: false);
      if (resultDto.result != null && resultDto.success) {
        return resultModel.create(
            ManageBusinessListOutputModel.create(
                ManageBusinessItemListOutputDto.create(resultDto.response)),
            true,
            null,
            false);
      } else {
        return resultModel.create(null, false, resultDto.error, true);
      }
    } catch (e) {
      return resultModel.create(
          null, false, ApiResultError(message: e.toString()), true);
    }
  }

  @override
  Future<ResultModel<UpdateBusinessOutputModel>> updateBusinessAsync(
      UpdateBusinessInputModel input) async {
    var resultModel = ResultModel<UpdateBusinessOutputModel>();
    try {
      var inputModel =
          ApiInputModel.create(UpdateBusinessInputDto.toMapping(input));
      var inputDto = ApiInputDto.toJson(inputModel);
      var resultDto =
          await _appService.postsAsync(input: inputDto, tokenRequired: false);
      if (resultDto.result != null && resultDto.success) {
        return resultModel.create(
            UpdateBusinessOutputModel.create(
                UpdateBusinessOutputDto.create(resultDto.response)),
            true,
            null,
            false);
      } else {
        return resultModel.create(
            UpdateBusinessOutputModel.create(
                UpdateBusinessOutputDto.create(resultDto.response)),
            false,
            null,
            true);
      }
    } catch (e) {
      return resultModel.create(
          null, false, ApiResultError(message: e.toString()), true);
    }
  }

  @override
  Future<ResultModel<UpgradeBusinessOutputModel>> upgradeBusinessAsync(
      UpgradeBusinessInputModel input) async {
    var resultModel = ResultModel<UpgradeBusinessOutputModel>();
    try {
      var inputModel =
          ApiInputModel.create(UpgradeBusinessInputDto.toMapping(input));
      var inputDto = ApiInputDto.toJson(inputModel);
      var resultDto =
          await _appService.postsAsync(input: inputDto, tokenRequired: false);
      if (resultDto.result != null && resultDto.success) {
        return resultModel.create(
            UpgradeBusinessOutputModel.create(
                UpgradeBusinessOutputDto.create(resultDto.response)),
            true,
            null,
            false);
      } else {
        return resultModel.create(null, false, resultDto.result.error, true);
      }
    } catch (e) {
      return resultModel.create(
          null, false, ApiResultError(message: e.toString()), true);
    }
  }

  @override
  Future<ResultModel<ManageBusinessListOutputModel>> getListBusinessByCashier(
      String mCID) async {
    var resultModel = ResultModel<ManageBusinessListOutputModel>();
    try {
      var inputModel = ApiInputModel.create(
          ManageBusinessListInputDto.toMappingCashier(mCID));
      var inputDto = ApiInputDto.toJson(inputModel);
      var resultDto =
          await _appService.postsAsync(input: inputDto, tokenRequired: false);
      if (resultDto.result != null && resultDto.success) {
        return resultModel.create(
            ManageBusinessListOutputModel.create(
                ManageBusinessItemListOutputDto.create(resultDto.response)),
            true,
            null,
            false);
      } else {
        return resultModel.create(null, false, resultDto.error, true);
      }
    } catch (e) {
      return resultModel.create(
          null, false, ApiResultError(message: e.toString()), true);
    }
  }
}
