import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class DeactivateBusinessInputDto {
  final _businessId = "".obs;
  String get businessId => _businessId.value;
  set businessId(String value) => _businessId.value = value;

  static DeactivateBusinessInputDto create(DeactivateBusinessInputModel input) {
    var result = DeactivateBusinessInputDto();
    result.businessId = input.businessId;
    return result;
  }
}
