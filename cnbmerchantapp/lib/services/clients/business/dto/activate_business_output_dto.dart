import 'package:cnbmerchantapp/core.dart';

class ActivateBusinessOutputDto extends ResultStatusOutputDto {
  static ActivateBusinessOutputDto create(List<String> dto) {
    var result = ActivateBusinessOutputDto();
    result.statusCode = dto[0];
    result.isSuccess = dto[0] == "00";
    result.description = dto[1];
    return result;
  }
}
