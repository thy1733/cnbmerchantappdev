import 'package:cnbmerchantapp/core.dart';

class UpdateBusinessOutputDto extends DetailBusinessOutputDto {
  static UpdateBusinessOutputDto create(List<String> dto) {
    var result = UpdateBusinessOutputDto();
    result.statusCode = dto[0];
    result.isSuccess = dto[0] == "00";
    if (result.isSuccess) {
      List<String> data = dto[3].split("!");
      result.businessId = data[0];
      result.creationDate = data[1];
      result.userId = data[2];
      result.accountNumber = data[3];
      result.accountHolderFullName = data[4];
      result.businessName = data[5];
      result.businessLogo = data[6];
      result.businessType = BusinessType.create(data[7], data[8]);
      result.status = data[9];
      result.corporateStatus = data[10];
      result.locationName = data[11];
      result.locationAddress = data[12].replaceSemiColonToComma();
      result.latitude = double.tryParse(data[13]) ?? 0.0;
      result.longitude = double.tryParse(data[14]) ?? 0.0;
      result.totalCashier = data[15];
      result.totalOutlet = data[16];
    } else {
      result.description = dto[3];
    }

    return result;
  }
}
