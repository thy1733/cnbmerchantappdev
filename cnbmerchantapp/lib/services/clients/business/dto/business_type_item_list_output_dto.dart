import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class BusinessTypeItemListOutputDto extends ResultStatusOutputDto {
  final _items = <BusinessTypeListOutputDto>[].obs;
  List<BusinessTypeListOutputDto> get items => _items;
  set items(List<BusinessTypeListOutputDto> value) => _items.value = value;

  static BusinessTypeItemListOutputDto create(List<String> dto) {
    var result = BusinessTypeItemListOutputDto();
    if (dto.isNotEmpty) {
      result.statusCode = dto[0];
      result.description = dto[1];
      result.items = BusinessTypeListOutputDto.create(dto[2].split("^"));
    }
    return result;
  }
}
