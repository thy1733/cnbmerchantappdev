import 'package:get/get.dart';

class BusinessTypeListOutputDto {
  final _id = "".obs;
  String get id => _id.value;
  set id(String value) => _id.value = value;

  final _name = "".obs;
  String get name => _name.value;
  set name(String value) => _name.value = value;

  static List<BusinessTypeListOutputDto> create(List<String> dto) {
    var result = <BusinessTypeListOutputDto>[];
    if (dto.isNotEmpty) {
      for (var i = 0; i < dto.length; i++) {
        var data = dto[i].split("!");
        if (data[0].isNotEmpty && data[1].isNotEmpty) {
          var input = BusinessTypeListOutputDto();
          input.id = data[0];
          input.name = data[1];
          result.add(input);
        }
      }
    }
    return result;
  }
}
