import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class SetupBusinessOutputDto extends ResultStatusOutputDto {
  final _id = "".obs;
  String get id => _id.value;
  set id(String value) => _id.value = value;

  final _creationDate = "".obs;
  String get creationDate => _creationDate.value;
  set creationDate(String value) => _creationDate.value = value;

  final _accountNumber = "".obs;
  String get accountNumber => _accountNumber.value;
  set accountNumber(String value) => _accountNumber.value = value;

  final _businessName = "".obs;
  String get businessName => _businessName.value;
  set businessName(String value) => _businessName.value = value;

  final _businessLogo = "".obs;
  String get businessLogo => _businessLogo.value;
  set businessLogo(String value) => _businessLogo.value = value;

  final _businessType = BusinessType().obs;
  BusinessType get businessType => _businessType.value;
  set businessType(BusinessType value) => _businessType.value = value;

  final _status = "".obs;
  String get status => _status.value;
  set status(String value) => _status.value = value;

  final _corporateStatus = "".obs;
  String get corporateStatus => _corporateStatus.value;
  set corporateStatus(String value) => _corporateStatus.value = value;

  final _locationName = "".obs;
  String get locationName => _locationName.value;
  set locationName(String value) => _locationName.value = value;

  final _locationAddress = "".obs;
  String get locationAddress => _locationAddress.value;
  set locationAddress(String value) => _locationAddress.value = value;

  final _latitude = 0.0.obs;
  double get latitude => _latitude.value;
  set latitude(double value) => _latitude.value = value;

  final _longitude = 0.0.obs;
  double get longitude => _longitude.value;
  set longitude(double value) => _longitude.value = value;

  final _staffRefID = "".obs;
  String get staffRefID => _staffRefID.value;
  set staffRefID(String value) => _staffRefID.value = value;

  final _username = "".obs;
  String get username => _username.value;
  set username(String value) => _username.value = value;

  static SetupBusinessOutputDto create(List<String> dto) {
    var result = SetupBusinessOutputDto();

    if (dto.isNotEmpty && dto[0] == "00") {
      result.statusCode = dto[0];
      result.isSuccess = dto[0] == "00";
      result.description = dto[1];

      result.id = dto[3];
      result.creationDate = dto[4];
      result.accountNumber = dto[5];
      result.businessName = dto[6];
      result.businessLogo = dto[7];
      result.businessType = BusinessType.create(dto[8], dto[9]);
      result.status = dto[10];
      result.corporateStatus = dto[11];
      result.locationName = dto[12];
      result.locationAddress = dto[13];
      result.latitude = double.tryParse(dto[14]) ?? 0.0;
      result.longitude = double.tryParse(dto[15]) ?? 0.0;
      result.staffRefID = dto[16];
      result.username = dto[17];
    } else if (dto[0] == "05") {
      result.statusCode = dto[0];
      result.isSuccess = dto[0] == "00";
      result.description = dto[2];
    }

    return result;
  }
}
