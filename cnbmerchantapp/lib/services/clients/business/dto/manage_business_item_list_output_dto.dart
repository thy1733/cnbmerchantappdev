import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class ManageBusinessItemListOutputDto extends ResultStatusOutputDto {
  final _totalCount = 0.obs;
  int get totalCount => _totalCount.value;
  set totalCount(int value) => _totalCount.value = value;

  final _items = <ManageBusinessListOutputDto>[].obs;
  List<ManageBusinessListOutputDto> get items => _items;
  set items(List<ManageBusinessListOutputDto> value) => _items.value = value;

  static ManageBusinessItemListOutputDto create(List<String> dto) {
    var result = ManageBusinessItemListOutputDto();
    if (dto.isNotEmpty) {
      result.statusCode = dto[0];
      result.description = dto[1];
      result.totalCount = int.parse(dto[2]);
      result.items = ManageBusinessListOutputDto.create(dto[3].split("^"));
    }
    return result;
  }
}
