import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class ManageBusinessListOutputDto {
  final _id = "".obs;
  String get id => _id.value;
  set id(String value) => _id.value = value;

  final _creationDate = "".obs;
  String get creationDate => _creationDate.value;
  set creationDate(String value) => _creationDate.value = value;

  final _accountNumber = "".obs;
  String get accountNumber => _accountNumber.value;
  set accountNumber(String value) => _accountNumber.value = value;

  final _businessName = "".obs;
  String get businessName => _businessName.value;
  set businessName(String value) => _businessName.value = value;

  final _businessLogo = "".obs;
  String get businessLogo => _businessLogo.value;
  set businessLogo(String value) => _businessLogo.value = value;

  final _businessLogoId = "".obs;
  String get businessLogoId => _businessLogoId.value;
  set businessLogoId(String value) => _businessLogoId.value = value;

  final _businessType = BusinessType().obs;
  BusinessType get businessType => _businessType.value;
  set businessType(BusinessType value) => _businessType.value = value;

  final _status = "".obs;
  String get status => _status.value;
  set status(String value) => _status.value = value;

  final _corporateStatus = "".obs;
  String get corporateStatus => _corporateStatus.value;
  set corporateStatus(String value) => _corporateStatus.value = value;

  final _locationName = "".obs;
  String get locationName => _locationName.value;
  set locationName(String value) => _locationName.value = value;

  final _locationAddress = "".obs;
  String get locationAddress => _locationAddress.value;
  set locationAddress(String value) => _locationAddress.value = value;

  final _latitude = 0.0.obs;
  double get latitude => _latitude.value;
  set latitude(double value) => _latitude.value = value;

  final _longitude = 0.0.obs;
  double get longitude => _longitude.value;
  set longitude(double value) => _longitude.value = value;

  final _mcc = "".obs;
  String get mcc => _mcc.value;
  set mcc(String value) => _mcc.value = value;

  final _accountCurrency = "".obs;
  String get accountCurrency => _accountCurrency.value;
  set accountCurrency(String value) => _accountCurrency.value = value;

  final _cacheImageKey = "".obs;
  String get cacheImageKey => _cacheImageKey.value;
  set cacheImageKey(String value) => _cacheImageKey.value = value;

  static List<ManageBusinessListOutputDto> create(List<String> dto) {
    var result = <ManageBusinessListOutputDto>[];
    if (dto.isNotEmpty) {
      for (var i = 0; i < dto.length; i++) {
        var data = dto[i].split("!");
        var output = ManageBusinessListOutputDto();
        output.id = data[0];
        output.creationDate = data[1];
        output.accountNumber = data[2];
        output.businessName = data[3];
        
        output.businessLogo = data[4]
            .downloadUrl(UploadProfileType.businessProfile, output.id, data[4]);
        output.cacheImageKey = data[4] + DateTime.now().toFormatDateString("yyyymmddhhmmsss");
        output.businessLogoId = data[4];
        
        output.businessType = BusinessType.create(data[5], data[6]);
        output.status = data[7];
        output.corporateStatus = data[8];
        output.locationName = data[9];
        output.locationAddress = data[10];
        output.latitude = double.tryParse(data[11]) ?? 0.0;
        output.longitude = double.tryParse(data[12]) ?? 0.0;
        output.mcc = data[13];
        output.accountCurrency = data[14];

        if (output.businessName.isNotEmpty) result.add(output);
      }
    }
    return result;
  }
}

class BusinessType {
  final _id = "".obs;
  String get id => _id.value;
  set id(String value) => _id.value = value;

  final _name = "".obs;
  String get name => _name.value;
  set name(String value) => _name.value = value;

  static BusinessType create(String id, String name) {
    var result = BusinessType();
    result.id = id;
    result.name = name;
    return result;
  }
}
