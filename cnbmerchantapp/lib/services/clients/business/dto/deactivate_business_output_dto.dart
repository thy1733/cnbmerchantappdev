import '../../../../core.dart';

class DeactivateBusinessOutputDto extends ResultStatusOutputDto {
  static DeactivateBusinessOutputDto create(List<String> dto) {
    var result = DeactivateBusinessOutputDto();
    result.statusCode = dto[0];
    result.isSuccess = dto[0] == "00";
    result.description = dto[1];
    return result;
  }
}
