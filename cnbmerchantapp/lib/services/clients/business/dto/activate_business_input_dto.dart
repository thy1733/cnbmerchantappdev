import 'package:get/get.dart';

import '../../../../core.dart';

class ActivateBusinessInputDto {
  final _businessId = "".obs;
  String get businessId => _businessId.value;
  set businessId(String value) => _businessId.value = value;

  static ActivateBusinessInputDto create(ActivateBusinessInputModel input) {
    var result = ActivateBusinessInputDto();
    result.businessId = input.businessId;
    return result;
  }
}
