import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class UpgradeBusinessOutputDto extends ResultStatusOutputDto {
  final _corporateStatus = ''.obs;
  String get corporateStatus => _corporateStatus.value;
  set corporateStatus(String value) => _corporateStatus.value = value;

  static UpgradeBusinessOutputDto create(List<String> dto) {
    var result = UpgradeBusinessOutputDto();
    result.statusCode = dto[0];
    result.isSuccess = result.statusCode == "00";
    result.description = dto[1];
    result.corporateStatus = dto[2];
    return result;
  }
}
