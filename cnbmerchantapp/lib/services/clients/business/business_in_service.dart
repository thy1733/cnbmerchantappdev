import 'package:cnbmerchantapp/core.dart';
import 'package:cnbmerchantapp/models/business/update_business_output_model.dart';

abstract class IBusinessService {
  Future<ResultModel<SetupBusinessOutputModel>> createBusinessAsync(
      SetupBusinessInputModel input);

  Future<ResultModel<BusinessTypeListOutputModel>> getListBusinessTypeAsync(
      BusinesTypeListInputModel input);

  Future<ResultModel<DetailBusinessOutputModel>> getDetailBusiness(
      DetailBusinessInputModel input);

  Future<ResultModel<DeactivateBusinessOutputModel>> deactivateBusinessAsync(
      DeactivateBusinessInputModel input);

  Future<ResultModel<ActivateBusinessOutputModel>> activateBusinessAsync(
      ActivateBusinessInputModel input);

  Future<ResultModel<ManageBusinessListOutputModel>> getListBusiness(
      ManageBusinessListInputModel input);

  Future<ResultModel<ManageBusinessListOutputModel>> getListBusinessByCashier(
      String mCID);

  Future<ResultModel<UpdateBusinessOutputModel>> updateBusinessAsync(
      UpdateBusinessInputModel input);

  Future<ResultModel<UpgradeBusinessOutputModel>> upgradeBusinessAsync(
      UpgradeBusinessInputModel input);
}
