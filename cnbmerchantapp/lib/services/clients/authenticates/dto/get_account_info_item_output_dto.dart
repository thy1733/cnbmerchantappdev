class GetAccountInfoItemOutputDto {
  String accountName = "";
  String accountNumber = "";
  String currency = "";
  String totalBalance = "";

  static List<GetAccountInfoItemOutputDto> create(List<String> dto) {
    var result = <GetAccountInfoItemOutputDto>[];
    if (dto.isNotEmpty) {
      for (var i = 0; i < dto.length; i++) {
        var data = dto[i].split("!");
        var input = GetAccountInfoItemOutputDto();
        input.accountNumber = data[0];
        input.accountName = data[1];
        input.totalBalance = data[2];
        input.currency = data[3];
        result.add(input);
      }
    }
    return result;
  }
}
