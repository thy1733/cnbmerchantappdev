import 'package:cnbmerchantapp/core.dart';

class ActivateInputDto {
  String cidOrAccountNumber = "";
  String phoneNumber = "";
  int activateType = 0;

  static ActivateInputDto create(ActivateInputModel input) {
    var result = ActivateInputDto();
    return result;
  }

  static String toMapping(ActivateInputModel input) {
    var result =
        "${RoutineName.activation}[[${input.activateType}[${input.cidOrAccountNumber}[0${input.phoneNumber}[${PlatformInfo.platform}";
    return result;
  }
}
