import 'package:cnbmerchantapp/core.dart';

class VerifyOtpOutputDto extends ResultStatusOutputDto {
  bool canRegister = false;
  String cidOrAccountNumber = "";

  static VerifyOtpOutputDto create(List<String> dto) {
    var result = VerifyOtpOutputDto();

    if (dto.isNotEmpty) {
      result.statusCode = dto[0];
      result.isSuccess = dto[0] == "00";
      result.description = dto[1];
      result.canRegister = dto[2] == "true";
      result.cidOrAccountNumber = dto[3];
    }

    return result;
  }
}
