import 'package:cnbmerchantapp/core.dart';

class GetAccountInfoListOutputDto extends ResultStatusOutputDto {
  List<GetAccountInfoItemOutputDto> items = [];

  static GetAccountInfoListOutputDto create(List<String> dto) {
    var result = GetAccountInfoListOutputDto();
    if (dto.isNotEmpty) {
      result.statusCode = dto[0];
      result.isSuccess = dto[0] == "00";

      if (result.isSuccess) {
        result.description = dto[1];
        result.items = GetAccountInfoItemOutputDto.create(dto[3].split("^"));
      } else {
        result.description = dto[3];
      }
    }
    return result;
  }
}
