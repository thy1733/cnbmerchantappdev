import 'package:cnbmerchantapp/core.dart';

class ResetPasswordInputDto {
  String accountNumberOrInviteCode = "";
  String newPassword = "";

  static ResetPasswordInputDto create(ResetPasswordInputModel input) {
    var result = ResetPasswordInputDto();
    result.accountNumberOrInviteCode = input.accountNumberOrInviteCode;
    result.newPassword = input.newPassword;
    return result;
  }

  static String toMapping(ResetPasswordInputModel input) {
    var result =
        "${RoutineName.resetPasswordUser}[[${input.accountNumberOrInviteCode.removeWhiteSpace()}[${GenHashKeyHelper.generatePassword(input.newPassword)}";
    return result;
  }
}
