import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class UserProfileOutputDto extends ResultStatusOutputDto {
  UserProfileOutputDto();

  final _firstName = ''.obs;
  String get firstName => _firstName.value;
  set firstName(String value) => _firstName.value = value;

  final _lastName = ''.obs;
  String get lastName => _lastName.value;
  set lastName(String value) => _lastName.value = value;

  final _lastLoginDateTime = ''.obs;
  String get lastLoginDateTime => _lastLoginDateTime.value;
  set lastLoginDateTime(String value) => _lastLoginDateTime.value = value;

  final _profilePhotoId = ''.obs;
  String get profilePhotoId => _profilePhotoId.value;
  set profilePhotoId(String value) => _profilePhotoId.value = value;

  final _activateType = ''.obs;
  String get activateType => _activateType.value;
  set activateType(String value) => _activateType.value = value;

  final _isRefundable = ''.obs;
  String get isRefundable => _isRefundable.value;
  set isRefundable(String value) => _isRefundable.value = value;

  final _refundLimited = ''.obs;
  String get refundLimited => _refundLimited.value;
  set refundLimited(String value) => _refundLimited.value = value;

  final _phoneNumber = ''.obs;
  String get phoneNumber => _phoneNumber.value;
  set phoneNumber(String value) => _phoneNumber.value = value;

  final _mCID = ''.obs;
  String get mCID => _mCID.value;
  set mCID(String value) => _mCID.value = value;

  final _displayName = ''.obs;
  String get displayName => _displayName.value;
  set displayName(String value) => _displayName.value = value;

  final _pinCode = ''.obs;
  String get pinCode => _pinCode.value;
  set pinCode(String value) => _pinCode.value = value;

  final _profileId = ''.obs;
  String get profileId => _profileId.value;
  set profileId(String value) => _profileId.value = value;

  final _businessId = ''.obs;
  String get businessId => _businessId.value;
  set businessId(String value) => _businessId.value = value;

  final _telegramToken = ''.obs;
  String get telegramToken => _telegramToken.value;
  set telegramToken(String value) => _telegramToken.value = value;

  static UserProfileOutputDto create(List<String> dto) {
    var result = UserProfileOutputDto();
    if (dto.isNotEmpty) {
      result.statusCode = dto[0];
      result.isSuccess = dto[1] == "true";
      result.description = dto[2];
      result.firstName = dto[3].trim();
      result.lastName = dto[4].trim();
      result.lastLoginDateTime = dto[5];
      result.profilePhotoId = dto[6];
      result.activateType = dto[7];
      result.isRefundable = dto[8];
      result.refundLimited = dto[9];
      result.phoneNumber = dto[10];
      result.mCID = dto[11];
      result.pinCode = dto[12];
      result.profileId = dto[13];
      result.businessId = dto[14];
      result.displayName = "${result.firstName} ${result.lastName}";
      result.telegramToken = dto[15];
    }
    return result;
  }
}
