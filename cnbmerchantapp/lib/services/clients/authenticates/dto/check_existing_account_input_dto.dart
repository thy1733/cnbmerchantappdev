import 'package:cnbmerchantapp/core.dart';

class CheckExistingAccountInputDto {

  String accountNumber;
  String username;

  CheckExistingAccountInputDto({
    this.accountNumber = "",
    this.username = "",
  });

  static CheckExistingAccountInputDto create(
      CheckExistingAccountInputModel input) {
    var result = CheckExistingAccountInputDto();
    result.accountNumber = input.accountNumber;
    result.username = input.username;
    return result;
  }

  static String toMapping(CheckExistingAccountInputModel input) {
    var result =
        "${RoutineName.checkExistingAccount}[[${input.accountNumber}[${input.username}";
    return result;
  }
}
