import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class AccountItemListOutputDto extends ResultStatusOutputDto {
  final _items = <AccountListOutputDto>[].obs;
  List<AccountListOutputDto> get items => _items;
  set items(List<AccountListOutputDto> value) => _items.value = value;

  static AccountItemListOutputDto create(List<String> dto) {
    var result = AccountItemListOutputDto();
    if (dto.isNotEmpty) {
      result.statusCode = dto[0];
      result.description = dto[1];
      result.items = AccountListOutputDto.create(dto[2].split("^"));
    }
    return result;
  }
}
