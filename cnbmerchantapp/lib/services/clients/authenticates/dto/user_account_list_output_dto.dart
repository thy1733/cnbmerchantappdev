import 'package:get/get.dart';

class AccountListOutputDto{

  final _acountName = ''.obs;
  String get acountName => _acountName.value;
  set acountName(String value) => _acountName.value = value;
  
  final _accountNumber = ''.obs;
  String get accountNumber => _accountNumber.value;
  set accountNumber(String value) => _accountNumber.value = value;
  
  final _currency = ''.obs;
  String get currency => _currency.value;
  set currency(String value) => _currency.value = value;
  

  static List<AccountListOutputDto> create(List<String> dto){
    var result =<AccountListOutputDto>[];
    if (dto.isNotEmpty) {
      for (var i = 0; i < dto.length; i++) {
        var data = dto[i].split("!");
        var input = AccountListOutputDto();
        input.acountName = data[0];
        input.accountNumber = data[1];
        input.currency = data[2];
        result.add(input);
      }
    }
    return result;
  }
}