import 'package:cnbmerchantapp/core.dart';

class CheckExistingAccountOutputDto extends ResultStatusOutputDto {
  bool isAlreadyExisted;

  CheckExistingAccountOutputDto({this.isAlreadyExisted = false});

  static CheckExistingAccountOutputDto create(List<String> dto) {
    var result = CheckExistingAccountOutputDto();
    if (dto.isNotEmpty) {
      result.statusCode = dto[0];
      result.isSuccess = dto[0] == "00";
      result.isAlreadyExisted = dto[2].toLowerCase() == "TRUE".toLowerCase();
      if (dto[0] != "00") {
        if (dto.asMap().containsKey(3)) {
          result.description = dto[3];
        } else {
          result.description = dto[1];
        }
      }
    }
    return result;
  }
}
