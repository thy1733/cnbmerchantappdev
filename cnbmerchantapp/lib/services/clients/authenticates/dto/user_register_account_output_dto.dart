import 'package:cnbmerchantapp/core.dart';

class RegisterAccountOuputDto extends ResultStatusOutputDto {
  bool canLogin = false;
  String mCID = "";

  static RegisterAccountOuputDto create(List<String> dto) {
    var result = RegisterAccountOuputDto();
    if (dto.isNotEmpty) {
      result.statusCode = dto[0];
      result.isSuccess = dto[0] == "00";
      result.description = dto[1];
      result.canLogin = dto[2] == "true";
      result.mCID = dto[3];
    }
    return result;
  }
}
