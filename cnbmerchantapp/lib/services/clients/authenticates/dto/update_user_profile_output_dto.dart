

import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class UpdateUserProfileOutputDto extends ResultStatusOutputDto {
  
  final _profielPhotoUrl = ''.obs;
  String get profielPhotoUrl => _profielPhotoUrl.value;
  set profielPhotoUrl(String value) => _profielPhotoUrl.value = value;
  

  static UpdateUserProfileOutputDto create(List<String> dto) {
    var result = UpdateUserProfileOutputDto();

    if (dto.isNotEmpty) {
      result.statusCode = dto[0];
      result.description = dto[1];
      result.isSuccess = dto[2] == "true";
      result.profielPhotoUrl = dto[3];
    }

    return result;
  }
}