import 'package:cnbmerchantapp/core.dart';

class CheckExistingUsernameOutputDto extends ResultStatusOutputDto {
  bool isAlreadyExisted = false;

  static CheckExistingUsernameOutputDto create(List<String> dto) {
    var result = CheckExistingUsernameOutputDto();
    if (dto.isNotEmpty) {
      result.statusCode = dto[0];
      result.isSuccess = dto[0] == "00";
      result.isAlreadyExisted = dto[2].toLowerCase() == "TRUE".toLowerCase();
    }
    return result;
  }
}
