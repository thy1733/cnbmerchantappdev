import 'package:cnbmerchantapp/core.dart';

class ActivateOutputDto extends ResultStatusOutputDto {
  String phoneNumber = "";

  static ActivateOutputDto create(List<String> dto) {
    var result = ActivateOutputDto();
    if (dto.isNotEmpty) {
      result.statusCode = dto[0];
      result.phoneNumber = dto[2];
      result.isSuccess = dto[3] == "true";
      if (dto.asMap().containsKey(4)) {
        result.description = dto[4];
      }
    }
    return result;
  }
}
