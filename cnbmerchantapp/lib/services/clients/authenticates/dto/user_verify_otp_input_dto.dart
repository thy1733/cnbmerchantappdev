import 'package:cnbmerchantapp/core.dart';

class VerifyOtpInputDto {
  String otpCode = "";
  String phoneNumber = "";
  String cidOrAccountNumber = "";
  String inviteCode = "";
  String merchantId = "";

  static VerifyOtpInputDto create(VerifyOtpInputModel input) {
    var result = VerifyOtpInputDto();
    result.otpCode = input.otpCode;
    result.phoneNumber = input.phoneNumber;
    result.cidOrAccountNumber = input.cidOrAccountNumber;
    result.inviteCode = input.inviteCode;
    result.merchantId = input.merchantId;
    return result;
  }

  static String toMapping(VerifyOtpInputModel input) {
    var result =
        "${RoutineName.verifyOtpCode}[[${input.otpCode}[${input.cidOrAccountNumber.removeWhiteSpace()}[${input.inviteCode.removeWhiteSpace()}[${input.merchantId.removeWhiteSpace()}";
    return result;
  }
}
