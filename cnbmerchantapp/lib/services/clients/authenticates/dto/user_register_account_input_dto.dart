import 'package:cnbmerchantapp/core.dart';

class RegisterAccountInputDto {
  String activateNumber = "";
  String phoneNumber = "";
  String userName = "";
  String password = "";
  String firstName = "";
  String lastName = "";
  String optCode = "";
  String pinCode = "";
  String nationalId = "";
  String inviteCode = "";

  static RegisterAccountInputDto create(RegisterAccountInputModel input) {
    var result = RegisterAccountInputDto();
    return result;
  }

  static String toMapping(RegisterAccountInputModel input) {
    var result =
        "${RoutineName.registerAccount}[[${input.accountNumber.removeWhiteSpace()}[0${input.phoneNumber.removeWhiteSpace()}[${input.userName}[${GenHashKeyHelper.generatePassword(input.password)}[${input.firstName}[${input.lastName}[${input.optCode}[${input.pinCode}[${input.inviteCode.removeWhiteSpace()}";
    return result;
  }
}
