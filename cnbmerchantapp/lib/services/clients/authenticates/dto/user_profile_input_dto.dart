import 'package:cnbmerchantapp/core.dart';

class UserProfileInputDto {

  String accountNumber;
  String username;

  UserProfileInputDto({
    this.accountNumber = "",
    this.username = "",
  });

  static UserProfileInputDto create(
      CheckExistingAccountInputModel input) {
    var result = UserProfileInputDto();
    result.accountNumber = input.accountNumber;
    result.username = input.username;
    return result;
  }

  static String toMapping(CheckExistingAccountInputModel input) {
    var result =
        "${RoutineName.getUser}[[${input.accountNumber}[${input.username}";
    return result;
  }
}
