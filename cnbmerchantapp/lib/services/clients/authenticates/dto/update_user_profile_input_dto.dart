import 'package:cnbmerchantapp/core.dart';

class UpdateUserProfileIntputDto {
  String accNumOrInviteCode = "";
  String username = "";
  String profilePhotoUrl = "";

  static UpdateUserProfileIntputDto create(
    String accNumOrInviteCode,
    String username,
    String profilePhotoUrl,
  ) {
    var result = UpdateUserProfileIntputDto();
    result.accNumOrInviteCode = accNumOrInviteCode;
    result.username = username;
    result.profilePhotoUrl = profilePhotoUrl;
    return result;
  }

  String toMapping() {
    var result = "${RoutineName.updateUser}[[$accNumOrInviteCode[$username[$profilePhotoUrl";
    return result;
  }
}
