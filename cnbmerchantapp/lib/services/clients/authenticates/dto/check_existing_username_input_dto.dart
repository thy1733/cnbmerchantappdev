import 'package:cnbmerchantapp/core.dart';

class CheckExistingUsernameInputDto {
  String username = "";

  static CheckExistingUsernameInputDto create(
      CheckExistingUsernameInputModel input) {
    var result = CheckExistingUsernameInputDto();
    result.username = input.username;
    return result;
  }

  static String toMapping(CheckExistingUsernameInputModel input) {
    var result = "${RoutineName.checkExistingUsername}[[${input.username}";
    return result;
  }
}
