import 'package:cnbmerchantapp/core.dart';

class ResetPasswordOutputDto extends ResultStatusOutputDto {
  static ResetPasswordOutputDto create(List<String> dto) {
    var result = ResetPasswordOutputDto();

    if (dto.isNotEmpty) {
      result.statusCode = dto[0];
      result.isSuccess = dto[0] == "00";
      result.description = dto[1];
    }

    return result;
  }
}
