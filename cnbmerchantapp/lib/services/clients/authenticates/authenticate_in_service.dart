import 'package:cnbmerchantapp/core.dart';

abstract class IAuthenticateService {
  Future<ResultModel<ActivateOutputModel>> activateAsync(ActivateInputModel input);
  Future<ResultModel<AccountListOutputModel>> getAccountByCidAsync(AccountListInputModel input);
  Future<ResultModel<GetAccountInfoListOutputModel>> getAccountInfoListAsync(GetAccountInfoInputModel input);
  Future<ResultModel<VerifyOtpOutputModel>> verifyOtpAsync(VerifyOtpInputModel input);
  Future<ResultModel<CheckExistingAccountOutputModel>> checkExistingAccountAsync(CheckExistingAccountInputModel input);
  Future<ResultModel<CheckExistingUsernameOutputModel>> checkExistingUsernameAsync(CheckExistingUsernameInputModel input);
  Future<ResultModel<RegisterAccountOuputModel>> registerAcountAsync(RegisterAccountInputModel input);
  Future<ResultModel<ResetPasswordOutputModel>> resetPasswordAsync(ResetPasswordInputModel input);
  Future<ResultModel<UserProfileOutputModel>> getUserProfile(CheckExistingAccountInputModel input);
  Future<ResultModel<String>> updateUserProfile(String profilePhotoUrl);
}
