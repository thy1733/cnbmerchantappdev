import 'package:cnbmerchantapp/core.dart';

class AuthenticateService implements IAuthenticateService {
  final IAppService _appService = AppService();

  final IAppUserProfileInfoService _appCacheService =
      AppUserProfileInfoService();

  @override
  Future<ResultModel<AccountListOutputModel>> getAccountByCidAsync(
      AccountListInputModel input) async {
    var resultModel = ResultModel<AccountListOutputModel>();
    try {
      var inputModel =
          ApiInputModel.create(AccountListInputDto.toMapping(input));

      var inputDto = ApiInputDto.toJson(inputModel);
      var resultDto =
          await _appService.postsAsync(input: inputDto, tokenRequired: false);
      if (resultDto.result != null && resultDto.success) {
        return resultModel.create(
            AccountListOutputModel.create(
                AccountItemListOutputDto.create(resultDto.response)),
            true,
            null,
            false);
      } else {
        return resultModel.create(null, false, resultDto.result.error, true);
      }
    } catch (e) {
      return resultModel.create(
          null, false, ApiResultError(message: e.toString()), true);
    }
  }

  @override
  Future<ResultModel<ActivateOutputModel>> activateAsync(
      ActivateInputModel input) async {
    var resultModel = ResultModel<ActivateOutputModel>();
    try {
      var inputModel = ApiInputModel.create(ActivateInputDto.toMapping(input));
      var inputDto = ApiInputDto.toJson(inputModel);
      var resultDto =
          await _appService.postsAsync(input: inputDto, tokenRequired: false);

      resultModel.result = ActivateOutputModel.create(
          ActivateOutputDto.create(resultDto.response));
      resultModel = resultModel.create(
          resultModel.result, resultDto.success, resultDto.error, false);
      return resultModel;
    } catch (e) {
      return resultModel.create(
          null, false, ApiResultError(message: e.toString()), true);
    }
  }

  @override
  Future<ResultModel<CheckExistingAccountOutputModel>>
      checkExistingAccountAsync(CheckExistingAccountInputModel input) async {
    var resultModel = ResultModel<CheckExistingAccountOutputModel>();
    try {
      var inputModel =
          ApiInputModel.create(CheckExistingAccountInputDto.toMapping(input));
      var inputDto = ApiInputDto.toJson(inputModel);
      var resultDto =
          await _appService.postsAsync(input: inputDto, tokenRequired: false);
      if (resultDto.result != null) {
        var resultOutputModel = CheckExistingAccountOutputModel.create(
            CheckExistingAccountOutputDto.create(resultDto.response));
        return resultModel.create(
            resultOutputModel, resultDto.success, null, false);
      } else {
        return resultModel.create(null, false, resultDto.error, true);
      }
    } catch (e) {
      return resultModel.create(
          null, false, ApiResultError(message: e.toString()), true);
    }
  }

  @override
  Future<ResultModel<CheckExistingUsernameOutputModel>>
      checkExistingUsernameAsync(CheckExistingUsernameInputModel input) async {
    var resultModel = ResultModel<CheckExistingUsernameOutputModel>();
    try {
      var inputModel =
          ApiInputModel.create(CheckExistingUsernameInputDto.toMapping(input));
      var inputDto = ApiInputDto.toJson(inputModel);
      var result =
          await _appService.postsAsync(input: inputDto, tokenRequired: false);
      resultModel.result = CheckExistingUsernameOutputModel.create(
          CheckExistingUsernameOutputDto.create(result.response));
      resultModel = resultModel.create(
          resultModel.result, result.success, result.error, !result.success);
      return resultModel;
    } catch (e) {
      return resultModel.create(
          null, false, ApiResultError(message: e.toString()), true);
    }
  }

  @override
  Future<ResultModel<RegisterAccountOuputModel>> registerAcountAsync(
      RegisterAccountInputModel input) async {
    var resultModel = ResultModel<RegisterAccountOuputModel>();
    try {
      var inputModel =
          ApiInputModel.create(RegisterAccountInputDto.toMapping(input));
      var inputDto = ApiInputDto.toJson(inputModel);
      var resultDto =
          await _appService.postsAsync(input: inputDto, tokenRequired: false);

      var resultOutputModel = RegisterAccountOuputModel.create(
          RegisterAccountOuputDto.create(resultDto.response));

      if (resultOutputModel.canLogin) {
        await _appCacheService.registerActivateAccountAsync(input);
      }
      return resultModel.create(
          resultOutputModel, resultDto.success, resultDto.error, false);
    } catch (e) {
      return resultModel.create(
          null, false, ApiResultError(message: e.toString()), true);
    }
  }

  @override
  Future<ResultModel<VerifyOtpOutputModel>> verifyOtpAsync(
      VerifyOtpInputModel input) async {
    var resultModel = ResultModel<VerifyOtpOutputModel>();
    try {
      var inputModel = ApiInputModel.create(VerifyOtpInputDto.toMapping(input));
      var inputDto = ApiInputDto.toJson(inputModel);
      var resultDto =
          await _appService.postsAsync(input: inputDto, tokenRequired: false);
      if (resultDto.result != null && resultDto.success) {
        var resultOutputModel = VerifyOtpOutputModel.create(
            VerifyOtpOutputDto.create(resultDto.response));
        return resultModel.create(
            resultOutputModel, resultDto.success, null, false);
      } else {
        return resultModel.create(null, false, resultDto.error, true);
      }
    } catch (e) {
      return resultModel.create(
          null, false, ApiResultError(message: e.toString()), true);
    }
  }

  @override
  Future<ResultModel<ResetPasswordOutputModel>> resetPasswordAsync(
      ResetPasswordInputModel input) async {
    var resultModel = ResultModel<ResetPasswordOutputModel>();
    try {
      var inputModel =
          ApiInputModel.create(ResetPasswordInputDto.toMapping(input));
      var inputDto = ApiInputDto.toJson(inputModel);
      var resultDto =
          await _appService.postsAsync(input: inputDto, tokenRequired: false);

      var resultOutputModel = ResetPasswordOutputModel.create(
          ResetPasswordOutputDto.create(resultDto.response));
      return resultModel.create(
          resultOutputModel, resultDto.success, resultDto.error, false);
    } catch (e) {
      return resultModel.create(
          null, false, ApiResultError(message: e.toString()), true);
    }
  }

  @override
  Future<ResultModel<GetAccountInfoListOutputModel>> getAccountInfoListAsync(
      GetAccountInfoInputModel input) async {
    var resultModel = ResultModel<GetAccountInfoListOutputModel>();
    try {
      var inputModel =
          ApiInputModel.create(GetAccountInfoInputDto.toMapping(input));
      var inputDto = ApiInputDto.toJson(inputModel);
      var resultDto =
          await _appService.postsAsync(input: inputDto, tokenRequired: false);
      if (resultDto.result != null && resultDto.success) {
        return resultModel.create(
            GetAccountInfoListOutputModel.create(
                GetAccountInfoListOutputDto.create(resultDto.response)),
            true,
            null,
            false);
      } else {
        return resultModel.create(
            GetAccountInfoListOutputModel.create(
                GetAccountInfoListOutputDto.create(resultDto.response)),
            false,
            null,
            true);
      }
    } catch (e) {
      return resultModel.create(
          null, false, ApiResultError(message: e.toString()), true);
    }
  }

  @override
  Future<ResultModel<UserProfileOutputModel>> getUserProfile(
      CheckExistingAccountInputModel input) async {
    var resultModel = ResultModel<UserProfileOutputModel>();
    try {
      var inputModel =
          ApiInputModel.create(UserProfileInputDto.toMapping(input));
      var inputDto = ApiInputDto.toJson(inputModel);
      var resultDto =
          await _appService.postsAsync(input: inputDto, tokenRequired: false);
      if (resultDto.result != null && resultDto.success) {
        return resultModel.create(
            UserProfileOutputModel.create(
                UserProfileOutputDto.create(resultDto.response)),
            true,
            null,
            false);
      } else if (resultDto.result != null && resultDto.success == false) {
        return resultModel.create(
            null,
            true,
            ApiResultError(
              code: int.tryParse(resultDto.response[0]) ?? 0,
              message: resultDto.response[1],
              details: resultDto.response[3],
            ),
            false);
      } else {
        return resultModel.create(null, false, resultDto.result.error, true);
      }
    } catch (e) {
      return resultModel.create(
          null, false, ApiResultError(message: e.toString()), true);
    }
  }
  
  @override
  Future<ResultModel<String>> updateUserProfile(String profilePhotoUrl) async{
    var resultModel = ResultModel<String>();
    try {
      var ai = AppUserTempInstant.appCacheUser.userType == 1 ? AppUserTempInstant.appCacheUser.inviteCode: AppUserTempInstant.appCacheUser.accountNumber;
      var input = UpdateUserProfileIntputDto.create(ai, AppUserTempInstant.appCacheUser.userName, profilePhotoUrl);
      var inputModel = ApiInputModel.create(input.toMapping());
      var inputDto = ApiInputDto.toJson(inputModel);

      var resultDto = await _appService.postsAsync(input: inputDto, tokenRequired: false);

      if (resultDto.result != null && resultDto.success) {
        var responDto = UpdateUserProfileOutputDto.create(resultDto.response);
        return resultModel.create(responDto.profielPhotoUrl, true, null, false);
      } else {
        return resultModel.create(null, false, resultDto.result.error, true);
      }
    } catch (e) {
      return resultModel.create(null, false, ApiResultError(message: e.toString()), true);
    }
  }
}
