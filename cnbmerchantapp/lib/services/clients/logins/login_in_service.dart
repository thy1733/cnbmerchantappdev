import 'package:cnbmerchantapp/core.dart';

abstract class ILoginService {
  Future<ResultModel<UserLoginOutputModel>> loginAsync(
      UserLoginInputModel input);
  Future<ResultModel<UserForgotPassOutputModel>> forgotPasswordAsync(
      UserForgotPassInputModel input);
  Future<ResultModel<UserResetPassOutputModel>> resetPasswordAsync(
      UserResetPassInputModel input);
  Future<ResultModel<UserChangePasswordOutputModel>> changePasswordAsync(
      UserChangePasswordInputModel input);
  Future<ResultModel<UserResetPinOutputModel>> resetPINAsync(
      UserResetPinInputModel input);
  Future<ResultModel<UserProfileOutputModel>> getUserProfile(String accNum, String username);
}
