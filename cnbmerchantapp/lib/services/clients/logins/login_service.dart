import 'package:cnbmerchantapp/core.dart';

class LoginService implements ILoginService {
  final IAppService _appService = AppService();

  final IAppOnboardingScreenService _launchService =
      AppOnboardingScreenService();
  final IAppUserProfileInfoService _appCacheUserService =
      AppUserProfileInfoService();
  final AuthenticateService _authenticateService = AuthenticateService();

  @override
  Future<ResultModel<UserLoginOutputModel>> loginAsync(
      UserLoginInputModel input) async {
    var resultModel = ResultModel<UserLoginOutputModel>();
    try {
      var inputModel = ApiInputModel.create(UserLoginInputDto.toMapping(input));
      var inputDto = ApiInputDto.toJson(inputModel);
      var resultDto =
          await _appService.postsAsync(input: inputDto, tokenRequired: false);

      resultModel.result = UserLoginOutputModel.create(
          UserLoginOutputDto.create(resultDto.response));
      resultModel = resultModel.create(
          resultModel.result, resultDto.success, resultDto.error, false);
      if (resultModel.success) {
        await _appCacheUserService.loginAuthenticateAsync(
            resultModel.result!.cid,
            resultModel.result!.accountNumber,
            input.userName,
            input.password);

        await getUserProfile(resultModel.result!.accountNumber, input.userName);

        await _launchService.updateUserLoginAsync(true);
      } else if (resultModel.result?.statusCode == "06") {
        await _appCacheUserService.loginAuthenticateAsync(
            resultModel.result!.cid,
            resultModel.result!.accountNumber,
            input.userName,
            input.password);
      }
      return resultModel;
    } catch (e) {
      return resultModel.create(
          null, false, ApiResultError(message: e.toString()), true);
    }
  }

  @override
  Future<ResultModel<UserForgotPassOutputModel>> forgotPasswordAsync(
      UserForgotPassInputModel input) async {
    var resultModel = ResultModel<UserForgotPassOutputModel>();
    try {
      var inputModel =
          ApiInputModel.create(UserForgotPassInputDto.toMapping(input));
      var inputDto = ApiInputDto.toJson(inputModel);
      var resultDto =
          await _appService.postsAsync(input: inputDto, tokenRequired: false);
      resultModel.result = UserForgotPassOutputModel.create(
          UserForgotPassOutputDto.create(resultDto.response));
      resultModel = resultModel.create(
          resultModel.result, resultDto.success, resultDto.error, false);
      return resultModel;
    } catch (e) {
      return resultModel.create(
          null, false, ApiResultError(message: e.toString()), true);
    }
  }

  @override
  Future<ResultModel<UserResetPassOutputModel>> resetPasswordAsync(
      UserResetPassInputModel input) async {
    var resultModel = ResultModel<UserResetPassOutputModel>();
    try {
      var inputModel =
          ApiInputModel.create(UserResetPassInputDto.toMapping(input));
      var inputDto = ApiInputDto.toJson(inputModel);
      var resultDto =
          await _appService.postsAsync(input: inputDto, tokenRequired: false);
      if (resultDto.result != null) {
        return resultModel.create(
            UserResetPassOutputModel.create(resultDto.response),
            true,
            null,
            false);
      } else {
        return resultModel.create(null, false, resultDto.error, true);
      }
    } catch (e) {
      return resultModel.create(
          null, false, ApiResultError(message: e.toString()), true);
    }
  }

  @override
  Future<ResultModel<UserChangePasswordOutputModel>> changePasswordAsync(
      UserChangePasswordInputModel input) async {
    var resultModel = ResultModel<UserChangePasswordOutputModel>();
    try {
      var inputModel =
          ApiInputModel.create(UserChangePasswordInputDto.toMapping(input));
      var inputDto = ApiInputDto.toJson(inputModel);
      var resultDto =
          await _appService.postsAsync(input: inputDto, tokenRequired: false);
      resultModel.result = UserChangePasswordOutputModel.create(
          UserChangePasswordOutputDto.create(resultDto.response));
      resultModel = resultModel.create(
          resultModel.result, resultDto.success, resultDto.error, true);
      return resultModel;
    } catch (e) {
      return resultModel.create(
          null, false, ApiResultError(message: e.toString()), true);
    }
  }

  @override
  Future<ResultModel<UserResetPinOutputModel>> resetPINAsync(
      UserResetPinInputModel input) async {
    var resultModel = ResultModel<UserResetPinOutputModel>();
    try {
      var inputModel =
          ApiInputModel.create(UserResetPinInputDto.toMapping(input));
      var inputDto = ApiInputDto.toJson(inputModel);
      var resultDto =
          await _appService.postsAsync(input: inputDto, tokenRequired: false);
      resultModel.result = UserResetPinOutputModel.create(
          UserResetPinOutputDto.create(resultDto.response));
      resultModel = resultModel.create(
          resultModel.result, resultDto.success, resultDto.error, true);
      if (resultModel.success) {
        var value = await _appCacheUserService.getUserProfileAsync();
        value.userPin = input.newPIN;
        _appCacheUserService.updateAuthenticateAsync(value);
        AppUserTempInstant.appCacheUser.userPin = input.newPIN;
      }
      return resultModel;
    } catch (e) {
      return resultModel.create(
          null, false, ApiResultError(message: e.toString()), true);
    }
  }

  @override
  Future<ResultModel<UserProfileOutputModel>> getUserProfile(
      String accNum, String username) async {
    var resultModel = ResultModel<UserProfileOutputModel>();

    try {
      var input = CheckExistingAccountInputModel();
      input.accountNumber = accNum.removeWhiteSpace();
      input.username = username;
      var result = await _authenticateService.getUserProfile(input);
      if (result.result != null && result.success) {
        var value = await _appCacheUserService.getUserProfileAsync();
        value.assignFromUserProfileModel(result.result!);
        AppUserTempInstant.appCacheUser = value;
        await _appCacheUserService.updateAuthenticateAsync(value);

        return resultModel.create(result.result, true, null, false);
      } else {
        return resultModel.create(
            null, false, result.error ?? ApiResultError(), false);
      }
    } catch (e) {
      return resultModel.create(null, false, ApiResultError(), false);
    }
  }
}
