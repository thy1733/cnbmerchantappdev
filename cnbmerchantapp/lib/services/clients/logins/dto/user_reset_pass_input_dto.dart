import 'package:cnbmerchantapp/core.dart';

class UserResetPassInputDto {
  String activationNumber = "";
  String newPassword = "";

  static String toMapping(UserResetPassInputModel input) {
    var result =
        "${RoutineName.resetPasswordUser}[[${input.activationNumber.removeWhiteSpace()}[${GenHashKeyHelper.generatePassword(input.newPassword)}";
    return result;
  }
}
