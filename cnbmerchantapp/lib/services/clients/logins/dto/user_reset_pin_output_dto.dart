import 'package:cnbmerchantapp/core.dart';

class UserResetPinOutputDto extends ResultStatusOutputDto {
  static UserResetPinOutputDto create(List<String> dto) {
    var result = UserResetPinOutputDto();
    if (dto.isNotEmpty) {
      result.statusCode = dto[0];
      result.isSuccess = dto[0] == "00";
      result.description = dto.length > 3 ? dto[3] : "";
    }
    return result;
  }
}
