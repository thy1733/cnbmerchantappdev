import 'package:cnbmerchantapp/core.dart';

class UserLoginInputDto {
  String userName;
  String password;
  UserLoginInputDto({this.userName = "", this.password = ""});

  static Map<String, dynamic> toJson(UserLoginInputModel input) {
    Map<String, dynamic> result = <String, dynamic>{};
    result["userNameOrEmailAddress"] = input.userName;
    result["password"] = input.password;
    result["rememberClient"] = true;
    return result;
  }

  static String toMapping(UserLoginInputModel input) {
    var result =
        "${RoutineName.authenticateTokenAuth}[[${input.userName}[${GenHashKeyHelper.generatePassword(input.password)}";
    return result;
  }
}
