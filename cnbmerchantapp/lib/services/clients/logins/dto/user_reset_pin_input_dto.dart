import 'package:cnbmerchantapp/core.dart';

class UserResetPinInputDto {
  String mCID = "";
  String currentPassword = "";
  String newPIN = "";

  static String toMapping(UserResetPinInputModel input) {
    var result =
        "${RoutineName.resetPIN}[[${AppUserTempInstant.appCacheUser.mCID.removeWhiteSpace()}[${GenHashKeyHelper.generatePassword(input.currentPassword)}[${input.newPIN}[${input.userName}";
    return result;
  }
}
