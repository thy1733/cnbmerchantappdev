import 'package:cnbmerchantapp/core.dart';

class UserChangePasswordOutputDto extends ResultStatusOutputDto {
  static UserChangePasswordOutputDto create(List<String> dto) {
    var result = UserChangePasswordOutputDto();
    if (dto.isNotEmpty) {
      result.statusCode = dto[0];
      result.isSuccess = dto[0] == "00";
      result.description = dto.length > 3
          ? dto[3]
          : dto.length > 1
              ? dto[1]
              : "";
    }
    return result;
  }
}
