class UserResetPassOutputDto {
  bool isSuccess;

  UserResetPassOutputDto({this.isSuccess = false});

  static UserResetPassOutputDto fromJson(Map<String, dynamic> json) {
    var result = UserResetPassOutputDto();
    result.isSuccess = json[0] == "00";
    return result;
  }
}
