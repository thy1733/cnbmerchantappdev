import 'package:cnbmerchantapp/core.dart';

class UserForgotPassInputDto {
  String activationNumber = "";
  String phoneNumber = "";

  static String toMapping(UserForgotPassInputModel input) {
    var result =
        "${RoutineName.forgotPasswordUser}[[${input.activationNumber.removeWhiteSpace()}[0${input.phoneNumber.removeWhiteSpace()}";
    return result;
  }
}
