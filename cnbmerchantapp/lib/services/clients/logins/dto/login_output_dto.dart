import 'package:cnbmerchantapp/core.dart';

class UserLoginOutputDto extends ResultStatusOutputModel {
  String cid;
  String accountNumber;
  UserLoginOutputDto({
    this.cid = "",
    this.accountNumber = "",
  });

  static UserLoginOutputDto create(List<String> dto) {
    var result = UserLoginOutputDto();

    if (dto.isNotEmpty) {
      result.statusCode = dto[0];
      result.isSuccess = dto[0] == "00";

      if (result.statusCode == "00") {
        result.cid = dto[3];
        result.accountNumber = dto[4];
      } else if (result.statusCode == "05") {
        result.description = dto[3];
      } else if (result.statusCode == "06") {
        result.description = dto[3];
        result.cid = dto[4];
        result.accountNumber = dto[5];
      }
    }
    return result;
  }
}
