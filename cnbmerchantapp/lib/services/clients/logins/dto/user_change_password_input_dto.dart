import 'package:cnbmerchantapp/core.dart';

class UserChangePasswordInputDto {
  String username = "";
  String currentPassword = "";
  String newPassword = "";
  String accountNumber = "";
  String inviteCode = "";

  static String toMapping(UserChangePasswordInputModel input) {
    var result =
        "${RoutineName.changePasswordUser}[[${input.username}[${input.inviteCode.isNotEmpty ? input.inviteCode : input.accountNumber}[${GenHashKeyHelper.generatePassword(input.currentPassword)}[${GenHashKeyHelper.generatePassword(input.newPassword)}";
    return result;
  }
}
