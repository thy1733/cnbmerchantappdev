import 'package:cnbmerchantapp/core.dart';

class UserForgotPassOutputDto extends ResultStatusOutputDto {
  String phoneNumber = "";

  static UserForgotPassOutputDto create(List<String> dto) {
    var result = UserForgotPassOutputDto();
    if (dto.isNotEmpty) {
      result.statusCode = dto[0];
      result.isSuccess = dto[0] == "00";
      if (result.statusCode == "05") {
        result.description = dto[3];
      }
    }
    return result;
  }
}
