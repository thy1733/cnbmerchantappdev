import 'package:cnbmerchantapp/core.dart';

abstract class ISaleAnalyticService {
  Future<ResultModel<SaleAnalyMonthlyOutputModel>> getSaleAnalytic(SaleAnalyticInputModel input);
}
