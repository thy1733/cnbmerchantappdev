import 'package:get/get.dart';

class SaleAnalyDailyOutputDto {
  final _date = DateTime.now().obs;
  DateTime get date => _date.value;
  set date(DateTime value) => _date.value = value;

  final _amount = 0.0.obs;
  double get amount => _amount.value;
  set amount(double value) => _amount.value = value;

  final _numberOfTransaction = 0.obs;
  int get numberOfTransaction => _numberOfTransaction.value;
  set numberOfTransaction(int value) => _numberOfTransaction.value = value;

  static SaleAnalyDailyOutputDto create(List<String> dto) {
    var result = SaleAnalyDailyOutputDto();
    result.date = DateTime.tryParse(dto[0]) ?? DateTime.now();
    result.amount = double.tryParse(dto[1]) ?? 0.0;
    result.numberOfTransaction = int.tryParse(dto[2])??0;
    return result;
  }
  // 20230301!TOT.AMT.SAL!TOT.TXN
}
