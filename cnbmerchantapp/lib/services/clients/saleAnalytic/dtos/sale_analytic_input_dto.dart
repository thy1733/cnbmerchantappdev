import 'package:cnbmerchantapp/core.dart';

class SaleAnalyticInputDto {
  String businessId = "";
  DateTime fromDate = DateTime.now();
  DateTime toDate = DateTime.now();

  static String toMapping(SaleAnalyticInputModel input) {
    var fromDate = DateTime(input.selectedDate.year, input.selectedDate.month, 1).toFormatDateString("yyyyMMdd");
    var toDate = DateTime(input.selectedDate.year, input.selectedDate.month+1, 0).toFormatDateString("yyyyMMdd");
    var result =
        "${RoutineName.getSaleAnalytic}[[${input.businessId}[$fromDate[$toDate";
    return result;
  }
}
