import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class SaleAnalyMonthlyOutputDto extends ResultStatusOutputDto {
  final _totalCashier = 0.obs;
  int get totalCashier => _totalCashier.value;
  set totalCashier(int value) => _totalCashier.value = value;

  final _totalOutlet = 0.obs;
  int get totalOutlet => _totalOutlet.value;
  set totalOutlet(int value) => _totalOutlet.value = value;

  final _totalAmountSale = 0.0.obs;
  double get totalAmountSale => _totalAmountSale.value;
  set totalAmountSale(double value) => _totalAmountSale.value = value;

  final _totalTransactionSale = 0.obs;
  int get totalTransactionSale => _totalTransactionSale.value;
  set totalTransactionSale(int value) => _totalTransactionSale.value = value;

  final _totalAmountRefunde = 0.0.obs;
  double get totalAmountRefund => _totalAmountRefunde.value;
  set totalAmountRefund(double value) => _totalAmountRefunde.value = value;

  final _totalTransactionRefunde = 0.obs;
  int get totalTransactionRefund => _totalTransactionRefunde.value;
  set totalTransactionRefund(int value) =>
      _totalTransactionRefunde.value = value;

  final _items = <SaleAnalyDailyOutputDto>[].obs;
  List<SaleAnalyDailyOutputDto> get items => _items;
  set items(List<SaleAnalyDailyOutputDto> value) => _items.value = value;

  static SaleAnalyMonthlyOutputDto create(List<String> dto) {
    var result = SaleAnalyMonthlyOutputDto();
    result.statusCode = dto[0];
    result.isSuccess = dto[0] == "00";
    result.description = dto[1];
    result.totalAmountSale = double.tryParse(dto[2]) ?? 0;
    result.totalTransactionSale = int.tryParse(dto[3]) ?? 0;
    result.totalOutlet = int.tryParse(dto[4]) ?? 0;
    result.totalCashier = int.tryParse(dto[5]) ?? 0;
    result.totalAmountRefund = double.tryParse(dto[6]) ?? 0;
    result.totalTransactionRefund = int.tryParse(dto[7]) ?? 0;
    if (dto[8].isNotEmpty) {
      var items = dto[8].split("^");
      for (var item in items) {
        var datas = item.split("!");
        if(datas.length>=3) {
          result.items.add(SaleAnalyDailyOutputDto.create(datas));
        }
      }
    }
    return result;
  }
}

//0 00
//1 SUCCESS
//2 TXN.AM.SALE
//3 TOT.TXN.SALE
//4 TOT.OUT
//5 TOT.CASH
//6 TOT.REF
//7 TXN.TXN.REF
//8 ^20230301!TOT.AMT.SAL!TOT.TXN^ 20230301!TOT.AMT.SAL!TOT.TXN

//0  00
//1  SUCCESS
//2  
//3  
//4  4
//5  8
//6  0
//7  0
//8  