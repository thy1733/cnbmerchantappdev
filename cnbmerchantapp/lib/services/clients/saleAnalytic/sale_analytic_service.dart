import 'package:cnbmerchantapp/core.dart';

class SaleAnalyticService implements ISaleAnalyticService {
  final AppService _appService = AppService();

  @override
  Future<ResultModel<SaleAnalyMonthlyOutputModel>> getSaleAnalytic(
    SaleAnalyticInputModel input,
  ) async {
    var resultModel = ResultModel<SaleAnalyMonthlyOutputModel>();
    try {
      var inputModel = ApiInputModel.create(
        SaleAnalyticInputDto.toMapping(input),
      );
      var inputDto = ApiInputDto.toJson(inputModel);
      var resultDto =
          await _appService.postsAsync(input: inputDto, tokenRequired: false);
      if (resultDto.result != null && resultDto.success) {
        return resultModel.create(
            SaleAnalyMonthlyOutputModel.create(
              SaleAnalyMonthlyOutputDto.create(resultDto.response),
              input,
            ),
            true,
            null,
            false);
      } else {
        return resultModel.create(null, false, resultDto.error ?? ApiResultError(message: "SomethingWentWrongPleaseTryAgainLater"), true);
      }
    } catch (e) {
      return resultModel.create(null, false,  ApiResultError(message: e.toString()), true);
    }
  }
}
