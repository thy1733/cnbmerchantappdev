class AddressComponents {
  String? longName;
  String? shortName;
  List<String>? types;

  AddressComponents({this.longName, this.shortName, this.types});

  static AddressComponents fromJson(Map<String, dynamic> json) {
    var result = AddressComponents();
    result.longName = json['long_name'];
    result.shortName = json['short_name'];
    result.types = json['types'].cast<String>();
    return result;
  }
}

class Geometry {
  LocationMap? location;
  String? locationType;
  Viewport? viewport;

  Geometry({this.location, this.locationType, this.viewport});

  static Geometry fromJson(Map<String, dynamic> json) {
    var result = Geometry();
    result.location = json['location'] != null
        ? LocationMap.fromJson(json['location'])
        : null;
    result.locationType = json['location_type'];
    result.viewport = json['viewport'] != null
        ? Viewport.fromJson(json['viewport'])
        : null;
        return result;
  }
}

class LocationMap {
  double? lat;
  double? lng;

  LocationMap({this.lat, this.lng});

  static LocationMap fromJson(Map<String, dynamic> json) {
    var result = LocationMap();
    result.lat = json['lat'];
    result.lng = json['lng'];
    return result;
  }
}

class Viewport {
  LocationMap? northeast;
  LocationMap? southwest;

  Viewport({this.northeast, this.southwest});

  static Viewport fromJson(Map<String, dynamic> json) {
    var result = Viewport();
    result.northeast = json['northeast'] != null
        ? LocationMap.fromJson(json['northeast'])
        : null;
    result.southwest = json['southwest'] != null
        ? LocationMap.fromJson(json['southwest'])
        : null;

        return result;
  }
}

class PlusCode {
  String? compoundCode;
  String? globalCode;

  PlusCode({this.compoundCode, this.globalCode});

  static PlusCode fromJson(Map<String, dynamic> json) {
    var result = PlusCode();
    result.compoundCode = json['compound_code'];
    result.globalCode = json['global_code'];
    return result;
  }
}