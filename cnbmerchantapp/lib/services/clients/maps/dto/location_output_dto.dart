import 'address_componenet_dto.dart';

class LocationOutputDto {
  List<LocationResults>? results;
  String? status;

  LocationOutputDto({this.results, this.status});

  LocationOutputDto.fromJson(Map<String, dynamic> json) {
    dynamic data = json['result'] ?? json['results'];
    if (data != null) {
      results = <LocationResults>[];
      data.forEach((v) {
        results!.add(LocationResults.fromJson(v));
      });
    }
    status = json['status'];
  }
  LocationOutputDto.create(LocationResults dto) {
    results = <LocationResults>[];
  }
}

class LocationResults {
  List<AddressComponents>? addressComponents;
  String? formattedAddress;
  Geometry? geometry;
  String? placeId;
  PlusCode? plusCode;
  List<String>? types;

  LocationResults(
      {this.addressComponents,
      this.formattedAddress,
      this.geometry,
      this.placeId,
      this.plusCode,
      this.types});

  static LocationResults fromJson(Map<String, dynamic> json) {
    var result = LocationResults();
    if (json['address_components'] != null) {
      result.addressComponents = <AddressComponents>[];
      json['address_components'].forEach((v) {
        result.addressComponents!.add(AddressComponents.fromJson(v));
      });
    }
    result.formattedAddress = json['formatted_address'];
    result.geometry =
        json['geometry'] != null ? Geometry.fromJson(json['geometry']) : null;
    result.placeId = json['place_id'];
    result.plusCode =
        json['plus_code'] != null ? PlusCode.fromJson(json['plus_code']) : null;
    result.types = json['types'].cast<String>();

    return result;
  }
}
