
import 'package:cnbmerchantapp/core.dart';

class AutoCompletedSearchBoxDto {
  List<Predictions>? predictions;
  String? status;

  AutoCompletedSearchBoxDto({this.predictions, this.status});

  AutoCompletedSearchBoxDto.fromJson(Map<String, dynamic> json) {
    if (json['predictions'] != null) {
      predictions = <Predictions>[];
      json['predictions'].forEach((v) {
        predictions!.add(Predictions.fromJson(v));
      });
    }
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (predictions != null) {
      data['predictions'] = predictions!.map((v) => v.toJson()).toList();
    }
    data['status'] = status;
    return data;
  }
}
