import 'package:cnbmerchantapp/core.dart';
import '../apps/api_constant.dart';

class MapService implements IMapService {
  final AppService clientService = AppService();

  @override
  Future<LocationAddressModel> getLocationNameByLatLgAsync(
      double lat, double lon) async {
    var url = "${ApiConstant.googleGdoLocation}$lat,$lon&sensor=true";
    var resultDto =
        await clientService.getsAsync(isOther: true,apiName: "", baseUrl: url);
    if (resultDto.result != null &&
        !resultDto.result.status.contains("ZERO_RESULTS")) {
      return resultDto.result.result.isEmpty
          ? LocationAddressModel()
          : LocationAddressModel.fromJsonResult(
              LocationResults.fromJson(resultDto.result.result[0]));
    }
    return LocationAddressModel();
  }

  @override
  Future<SearchAddressModel> searchPlaceAddressAsync(String text) async {
    var url = "${ApiConstant.searchAddress}$text";
    var resultDto =
        await clientService.getDynamicAsync(apiName: "", baseUrl: url);
    if (resultDto.result != null && resultDto.result != null) {
      var resultJson =
          AutoCompletedSearchBoxDto.fromJson(resultDto.result);
      return SearchAddressModel.fromJson(resultJson);
    }
    return SearchAddressModel();
  }

  @override
  Future<LocationAddressModel> getPlaceAddressByLatLong(String placeId) async {
    var url = "${ApiConstant.placeAddress}$placeId";
    var resultDto = await clientService.getDynamicAsync(apiName: "", baseUrl: url);
    if (resultDto.result != null && resultDto.result != null) {
      var resultJson = LocationOutputDto.fromJson(resultDto.result);
      return LocationAddressModel.fromJson(resultJson);
    }
    return LocationAddressModel();
  }
}
