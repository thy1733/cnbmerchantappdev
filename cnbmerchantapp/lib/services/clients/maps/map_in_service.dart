import '../../../models/locations/xcore.dart';

abstract class IMapService {
  Future<LocationAddressModel> getLocationNameByLatLgAsync(
      double lat, double lon);
  Future<SearchAddressModel> searchPlaceAddressAsync(String text);
  Future<LocationAddressModel> getPlaceAddressByLatLong(String placeId);
}
