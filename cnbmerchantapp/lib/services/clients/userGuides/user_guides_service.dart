import 'package:cnbmerchantapp/core.dart';

class UserGuidesService implements IUserGuidesService {
  final AppService _appService = AppService();

  @override
  Future<ResultModel<UserGuideListOutputModel>> getUserGuides(
      UserGuideItemInputModel input) async {
    var resultModel = ResultModel<UserGuideListOutputModel>();
    try {
      var inputModel =
          ApiInputModel.create(UserGuideListInputDto.toMapping(input));

      var inputDto = ApiInputDto.toJson(inputModel);
      var resultDto =
          await _appService.postsAsync(input: inputDto, tokenRequired: false);
      if (resultDto.result != null && resultDto.success) {
        return resultModel.create(
            UserGuideListOutputModel.create(
                UserGuideListOutputDto.create(resultDto.response)),
            true,
            null,
            false);
      } else {
        return resultModel.create(null, false, resultDto.error, true);
      }
    } catch (e) {
      return resultModel.create(
          null, false, ApiResultError(message: e.toString()), true);
    }
  }
}
