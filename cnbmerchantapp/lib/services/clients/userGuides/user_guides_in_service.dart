import 'package:cnbmerchantapp/core.dart';

abstract class IUserGuidesService {

  Future<ResultModel<UserGuideListOutputModel>> getUserGuides(UserGuideItemInputModel input);
}
