import 'package:cnbmerchantapp/core.dart';

class UserGuideListOutputDto extends ResultStatusOutputDto {
  List<UserGuideItemOutputDto> items = [];
  static UserGuideListOutputDto create(List<String> dto) {
    var result = UserGuideListOutputDto();
    if (dto.isNotEmpty) {
      result.statusCode = dto[0];
      result.description = dto[1];
      result.items = UserGuideItemOutputDto.create(dto[2].split("^"));
    }
    return result;
  }
}
