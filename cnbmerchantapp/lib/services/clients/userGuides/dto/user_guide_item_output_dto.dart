class UserGuideItemOutputDto {
  String title = "";
  String description = "";
  String thumbnailImageUrl = "";
  String videoLink = "";

  static List<UserGuideItemOutputDto> create(List<String> dto) {
    var result = <UserGuideItemOutputDto>[];
    if (dto.isNotEmpty) {
      for (var i = 0; i < dto.length; i++) {
        var data = dto[i].split("!");
        var input = UserGuideItemOutputDto();
        input.title = data[0];
        input.description = data[1];
        input.thumbnailImageUrl = data[2];
        input.videoLink = data[3];
        result.add(input);
      }
    }
    return result;
  }
}
