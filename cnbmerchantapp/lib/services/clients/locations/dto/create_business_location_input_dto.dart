class CreateBusinessLocationInputDto {
  final String locationName;
  final double latitude;
  final double longitude;
  final String address;
  CreateBusinessLocationInputDto({
    required this.locationName,
    required this.latitude,
    required this.longitude,
    required this.address,
  });

  Map<String, dynamic> toJson() => {
        "locationName": locationName,
        "latitude": latitude,
        "longtitude": longitude,
        "address": address
      };
}
