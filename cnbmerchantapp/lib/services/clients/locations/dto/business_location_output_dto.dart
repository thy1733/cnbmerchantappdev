class BusinessLocationOutputDto {
  String? id;
  String? creationTime;
  int? creatorUserId;
  String? lastModificationTime;
  int? lastModifierUserId;
  String? locationName;
  double? latitude;
  double? longitude;
  String? address;
  bool? isActive;

  BusinessLocationOutputDto({
    this.id,
    this.creationTime,
    this.creatorUserId,
    this.lastModificationTime,
    this.lastModifierUserId,
    this.locationName,
    this.latitude,
    this.longitude,
    this.address,
    this.isActive,
  });

  factory BusinessLocationOutputDto.fromJson(Map<String, dynamic> json) =>
      BusinessLocationOutputDto(
          id: json['id'],
          creationTime: json['creationTime'],
          creatorUserId: json['creatorUserId'],
          lastModificationTime: json['lastModificationTime'],
          lastModifierUserId: json['lastModifierUserId'],
          locationName: json['locationName'],
          latitude: json['latitude'],
          longitude: json['longtitude'],
          address: json['address'],
          isActive: json['isActive']);
}

class BusinessLocationListOutputDto {
  List<BusinessLocationOutputDto>? items;
  int? totalCount;
  BusinessLocationListOutputDto({
    this.items,
    this.totalCount,
  });

  BusinessLocationListOutputDto.fromJson(Map<String, dynamic> json) {
    if (json['items'] != null) {
      items = <BusinessLocationOutputDto>[];
      json['items'].forEach((v) {
        items!.add(BusinessLocationOutputDto.fromJson(v));
      });
    }
  }
}
