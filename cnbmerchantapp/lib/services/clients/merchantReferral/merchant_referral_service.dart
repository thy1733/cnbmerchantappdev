import 'package:cnbmerchantapp/core.dart';

class MerchantReferralService implements IMerchantReferralService {
  final AppService _appService = AppService();

  @override
  Future<ResultModel<ReqMerchantReferralOutputModel>> reqMerchantReferralAsync(
    ReqMerchantReferralInputModel input,
  ) async {
    var resultModel = ResultModel<ReqMerchantReferralOutputModel>();
    try {
      var inputModel = ApiInputModel.create(
        ReqMerchantReferralInputDto.create(input).toMapping(),
      );
      var inputDto = ApiInputDto.toJson(inputModel);
      var resultDto = await _appService.postsAsync(
        input: inputDto,
        tokenRequired: false,
      );
      if (resultDto.result != null && resultDto.success) {
        return resultModel.create(
            ReqMerchantReferralOutputModel.create(
             ReqMerchantReferralOutputDto.create(resultDto.response),
            ),
            true,
            null,
            false);
      } else {
        return resultModel.create(null, false, resultDto.result.error, true);
      }
    } catch (e) {
      return resultModel.create(null, false, null, true);
    }
  }
}
