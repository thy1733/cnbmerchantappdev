import 'package:cnbmerchantapp/core.dart';

class ReqMerchantReferralOutputDto extends ResultStatusOutputDto {
  ReqMerchantReferralOutputDto();

  factory ReqMerchantReferralOutputDto.create(List<String> dto) {
    var result = ReqMerchantReferralOutputDto();
    result.statusCode = dto[0];
    result.isSuccess = result.statusCode == "00";
    result.description = dto[1];
    
    return result;
  }
}
