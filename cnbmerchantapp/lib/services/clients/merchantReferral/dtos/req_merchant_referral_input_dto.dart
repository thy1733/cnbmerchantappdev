import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class ReqMerchantReferralInputDto {
  String mcId = '';
  String businessName = '';
  String businessContact = '';
  String businessLocationName = '';
  String businessAddress = '';
  String businessLatitude = '0.0';
  String businessLongitude = '0.0';
  String remark = '';
  ReqMerchantReferralInputDto();

  factory ReqMerchantReferralInputDto.create(ReqMerchantReferralInputModel input) {
    var result = ReqMerchantReferralInputDto();
    result.mcId = AppUserTempInstant.appCacheUser.mCID;
    result.businessName = input.businessName;
    result.businessContact = input.businessContact.removeAllWhitespace;
    result.businessLocationName = input.businessLocation.locationName.replaceAll(",", ";");
    result.businessAddress = input.businessLocation.address.replaceAll(",", ";");
    result.businessLatitude = "${input.businessLocation.latitude}";
    result.businessLongitude = "${input.businessLocation.longitude}";
    result.remark = input.remark;
    return result;
  }

  String toMapping() {
    var result =
        "${RoutineName.reqMerchantReferral}[[$mcId[$businessName[$businessContact[$businessLocationName[$businessAddress[$businessLatitude[$businessLongitude[$remark";
    return result;
  }
}
