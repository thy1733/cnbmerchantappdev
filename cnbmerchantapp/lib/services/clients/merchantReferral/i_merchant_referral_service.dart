import 'package:cnbmerchantapp/core.dart';

abstract class IMerchantReferralService {
  Future<ResultModel<ReqMerchantReferralOutputModel>> reqMerchantReferralAsync(
      ReqMerchantReferralInputModel input);
}
