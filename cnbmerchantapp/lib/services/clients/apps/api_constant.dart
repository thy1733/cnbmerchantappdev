import 'package:cnbmerchantapp/core.dart';

class ApiConstant {
  static const googleGdoLocation =
      "https://maps.googleapis.com/maps/api/geocode/json?key=${MapSecretKey.licenseKey}&latlng=";
  static const searchAddress =
      "https://maps.googleapis.com/maps/api/place/autocomplete/json?key=${MapSecretKey.licenseKey}&components=country:kh&input=";
  static const placeAddress =
      "https://maps.googleapis.com/maps/api/geocode/json?key=${MapSecretKey.licenseKey}&place_id=";

  // Account
  static const verifyOTPCode = 'api/services/app/Account/VerifyOtpCode';
  static const changePassword = 'api/services/app/User/ChangePassword';
  static const authenticate = "api/TokenAuth/Authenticate";
  static const authenticateWithTenancy =
      "api/TokenAuth/AuthenticateWithTenancy";

  // Session
  static const getCurrentLoginInformations =
      'api/services/app/Session/GetCurrentLoginInformations';
}
