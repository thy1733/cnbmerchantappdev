import 'package:dio/dio.dart';

class ApiConfigHelper{
  static String clientSecret = "";
  static String url({String apiName=''}) => "https://digital.canadiabank.com/defaultmcauat";
  static String downloadUrl = "https://firebasestorage.googleapis.com/";
}

class ApiConfigHeader{
  static const headerJson = <String, String>{
    'Content-Type': 'application/json; charset=UTF-8',
  };

  static const plainText = <String, String>{
    'Content-Type': 'text/plain; charset=UTF-8',
  };

  static const headerPDF = <String, String>{
    'Content-Type': 'application/pdf',
  };
  static const headerJPEG = <String, String>{
    'Content-Type': 'image/jpeg',
  };
  static const headerMultipart = <String, List<String>>{
    'Content-Type': ['multipart/form-data'],
  };

  static Options getOptionHeader({String token=""}) {
    var authorizationHeader = <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
      "Authorization": "Bearer $token",
    };

    Map<String, String> headers = {
      "Content-Type": "text/plain",
      "Accept": "application/json"
    };

    var noRequired = Options(
      followRedirects: true,
      validateStatus: (status) => true,
      headers: headers,
    );
    var required = Options(
      followRedirects: false,
      validateStatus: (status) => true,
      headers: authorizationHeader,
    );
    // request required with authorization token
    return token.isEmpty ? noRequired : required;
  }
}