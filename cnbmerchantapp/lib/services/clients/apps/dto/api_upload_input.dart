import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class UploadFileInputDto {
  
  final _id = ''.obs;
  String get id => _id.value;
  set id(String value) => _id.value = value;

  final _uploadType = UploadProfileType.businessProfile.obs;
  UploadProfileType get uploadType => _uploadType.value;
  set uploadType(UploadProfileType value) => _uploadType.value = value;
  
  final _fileName = ''.obs;
  String get fileName => _fileName.value;
  set fileName(String value) => _fileName.value = value;
  
  final _filePath = ''.obs;
  String get filePath => _filePath.value;
  set filePath(String value) => _filePath.value = value;
  
  final _metaData = ''.obs;
  String get metaData => _metaData.value;
  set metaData(String value) => _metaData.value = value;
  
}