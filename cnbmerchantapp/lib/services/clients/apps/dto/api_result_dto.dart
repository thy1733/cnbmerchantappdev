import 'dart:convert';

import 'package:dio/dio.dart';

class ApiResultDto {
  dynamic result;
  List<String> response =[];
  String targetUrl;
  bool success;
  String status;
  dynamic error;
  bool unAuthorizedRequest;
  bool abp;
  ApiResultDto({
    this.result,
    required this.response,
    this.targetUrl = '',
    this.success = false,
    this.error,
    this.status = '',
    this.unAuthorizedRequest = false,
    this.abp = false,
  });

  Map<String, dynamic> toMap() {
    return {
      'result': result,
      'targetUrl': targetUrl,
      'success': success,
      'error': error,
      'unAuthorizedRequest': unAuthorizedRequest,
      'abp': abp,
    };
  }

  factory ApiResultDto.fromMap(Map<String, dynamic> map) {
    return ApiResultDto(
      result: map['result'] ?? map['results'],
      response: [],
      targetUrl: map['targetUrl'] ?? '',
      success: map['success'] ?? false,
      status: map['status'] ?? '',
      error:
          map['error'] != null ? ApiResultError.fromJson(map['error']) : null,
      unAuthorizedRequest: map['unAuthorizedRequest'] ?? false,
      abp: map['_abp'] ?? false,
    );
  }

  String toJson() => json.encode(toMap());
}

class ApiResultError {
  late int code;
  late String message;
  late String details;
  late dynamic validationErrors;

  ApiResultError(
      {this.code = 0,
      this.message = '',
      this.details = '',
      this.validationErrors});

  factory ApiResultError.fromJson(Map<String, dynamic> json) {
    return ApiResultError(
        code: json['code'] ?? 0,
        message: json['message'] ?? '',
        details: json['details'] ?? '',
        validationErrors: json['validationErrors']);
  }

  static ApiResultError dioError(DioError error) {
    // var result = ApiResultError();
    // switch (error.type) {
    //   case DioErrorType.connectTimeout:
    //   case DioErrorType.sendTimeout:
    //   case DioErrorType.receiveTimeout:
    //     result.code = 400;
    //     result.message = "";
    //     result.details = "";
    //     result.validationErrors = null;
    //     break;
    //   case DioErrorType.response:
    //     switch (error.response?.statusCode) {
    //       case 400:
    //         result.code = 400;
    //         result.message = "";
    //         result.details = "";
    //         result.validationErrors = null;
    //         break;
    //       case 401:
    //         result.code = 401;
    //         result.message = "";
    //         result.details = "";
    //         result.validationErrors = null;
    //         break;
    //       case 404:
    //         result.code = 404;
    //         result.message = "";
    //         result.details = "";
    //         result.validationErrors = null;
    //         break;
    //       case 409:
    //         result.code = 409;
    //         result.message = "";
    //         result.details = "";
    //         result.validationErrors = null;
    //         break;
    //       case 500:
    //         result.code = 500;
    //         result.message = "";
    //         result.details = "";
    //         result.validationErrors = null;
    //         break;
    //     }
    //     break;
    //   case DioErrorType.cancel:
    //     result.code = 400;
    //     result.message = "";
    //     result.details = "";
    //     result.validationErrors = null;
    //     break;
    //   case DioErrorType.other:
    //     result.code = error.response!.statusCode!;
    //     result.message = "";
    //     result.details = "";
    //     result.validationErrors = null;
    //     break;
    // }
    // return result;
    return ApiResultError(
        code: error.type == DioErrorType.response ? (error.response!.statusCode == null ? 0 : error.response!.statusCode!) : error.type.index,
        message: error.type == DioErrorType.response ? error.response!.statusMessage! : error.message,
        details: "${error.stackTrace}",
        validationErrors: "${error.response}");
  }
}