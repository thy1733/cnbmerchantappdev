import 'dart:convert';

class ApiOutputOtherDto {
  dynamic result;
  String targetUrl;
  bool success;
  String status;
  dynamic error;
  bool unAuthorizedRequest;
  bool abp;
  ApiOutputOtherDto({
    this.result,
    this.targetUrl = '',
    this.success = false,
    this.error,
    this.status = '',
    this.unAuthorizedRequest = false,
    this.abp = false,
  });

  Map<String, dynamic> toMap() {
    return {
      'result': result,
      'targetUrl': targetUrl,
      'success': success,
      'error': error,
      'unAuthorizedRequest': unAuthorizedRequest,
      'abp': abp,
    };
  }

  factory ApiOutputOtherDto.fromMap(Map<String, dynamic> map) {
    return ApiOutputOtherDto(
      result: map['result'] ?? map['results'],
      targetUrl: map['targetUrl'] ?? '',
      success: map['success'] ?? false,
      status: map['status'] ?? '',
      error:
          map['error'] != null ? ApiOutputError.fromJson(map['error']) : null,
      unAuthorizedRequest: map['unAuthorizedRequest'] ?? false,
      abp: map['_abp'] ?? false,
    );
  }

  String toJson() => json.encode(toMap());

  // factory ApiOutputDto.fromJson(dynamic source) =>
  //     ApiOutputDto.fromMap(json.decode(source));
}

class ApiOutputError {
  late int code;
  late String message;
  late String details;
  late dynamic validationErrors;

  ApiOutputError(
      {this.code = 0,
      this.message = '',
      this.details = '',
      this.validationErrors});

  factory ApiOutputError.fromJson(Map<String, dynamic> json) {
    return ApiOutputError(
        code: json['code'] ?? 0,
        message: json['message'] ?? '',
        details: json['details'] ?? '',
        validationErrors: json['validationErrors']);
  }
}
