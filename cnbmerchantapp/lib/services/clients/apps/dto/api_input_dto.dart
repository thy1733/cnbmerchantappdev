import 'package:cnbmerchantapp/core.dart';

class ApiInputDto {
  late String tXNID;
  late String dEVICEINFO;
  late String mCID;
  late String sESSIONID;
  late String rEQDATA;
  ApiInputDto(
      {this.tXNID = '',
      this.dEVICEINFO = '',
      this.mCID = '',
      this.sESSIONID = '',
      this.rEQDATA = ''});

  ApiInputDto.fromJson(Map<String, dynamic> json) {
    tXNID = json['TXNID'] ?? '';
    dEVICEINFO = json['DEVICE.INFO'] ?? '';
    mCID = json['MCID'] ?? '';
    sESSIONID = json['SESSION.ID'] ?? '';
    rEQDATA = json['REQDATA'] ?? '';
  }

  static Map<String, dynamic> toJson(ApiInputModel input) {
    var result = <String, dynamic>{};
    AppUserTempInstant.transactionId = input.tXNID;
    result["TXNID"] = input.tXNID;
    result["DEVICE.INFO"] = input.dEVICEINFO;
    result["MCID"] = AppUserTempInstant.appCacheUser.mCID;//input.mCID;
    result["SESSION.ID"] = input.sESSIONID;
    result["REQDATA"] = "${ApiConfigHelper.clientSecret}[${input.rEQDATA}";
    return result;
  }
}
