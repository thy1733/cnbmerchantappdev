class ApiOutputDto {
  late String tXNID;
  late dynamic sESSIONID;
  late String rESDATA;
  late dynamic rESDATA1;

  ApiOutputDto(
      {this.tXNID='',
      this.sESSIONID='',
      this.rESDATA='',
      this.rESDATA1=''});
      
  ApiOutputDto.fromJson(Map<String, dynamic> json) {
    try {
      tXNID = json['TXNID'] ?? '';
      sESSIONID = json['SESSION.ID'];
      rESDATA = json['RES.DATA']?? '';
      rESDATA1 = json['RES.DATA1'];
    } catch (e) {
      tXNID =  '';
      sESSIONID = [];
      rESDATA = "";
      rESDATA1 = [];
    }
  }
}