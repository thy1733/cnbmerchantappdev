import 'package:get/get.dart';

class ResultStatusOutputDto {
  final _statusCode = ''.obs;
  String get statusCode => _statusCode.value;
  set statusCode(String value) => _statusCode.value = value;

  final _isSuccess = false.obs;
  bool get isSuccess => _isSuccess.value;
  set isSuccess(bool value) => _isSuccess.value = value;

  final _description = ''.obs;
  String get description => _description.value;
  set description(String value) => _description.value = value;
}
