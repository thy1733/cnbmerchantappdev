import 'package:cnbmerchantapp/core.dart';

class MapApiConstant {
  static const googleGdoLocation ="https://maps.googleapis.com/maps/api/geocode/json?key=${MapSecretKey.licenseKey}&latlng=";
  static const searchAddress ="https://maps.googleapis.com/maps/api/place/autocomplete/json?key=${MapSecretKey.licenseKey}&input=";
  static const placeAddress ="https://maps.googleapis.com/maps/api/geocode/json?key=${MapSecretKey.licenseKey}&place_id=";
}
