import 'package:cnbmerchantapp/core.dart';
import 'package:dio/dio.dart';

abstract class IAppService {
  Future<ApiResultDto> postAsync(
      {required String apiName,
      bool tokenRequired = true,
      String baseUrl = "",
      CancelToken? cancelToken});
  Future<ApiResultDto> postsAsync<I>(
      {required I input,
      bool tokenRequired = true,
      String baseUrl = "",
      CancelToken? cancelToken});
  Future<ApiResultDto> putAsync(
      {required String apiName,
      bool tokenRequired = true,
      String baseUrl = "",
      CancelToken? cancelToken});
  Future<ApiResultDto> putsAsync<I>(
      {required String apiName,
      required I input,
      bool tokenRequired = true,
      String baseUrl = "",
      CancelToken? cancelToken});
  Future<ApiResultDto> getAsync(
      {required String apiName,
      bool tokenRequired = true,
      String baseUrl = "",
      CancelToken? cancelToken});
  Future<ApiResultDto> getsAsync(
      {required String apiName,
      bool tokenRequired = true,
      String baseUrl = "",
      bool isOther=false,
      CancelToken? cancelToken});

      Future<dynamic> getDynamicAsync(
      {required String apiName,
      bool tokenRequired = true,
      String baseUrl = "",
      CancelToken? cancelToken});

  Future<ApiResultDto> deleteAsync(
      {required String apiName,
      bool tokenRequired = true,
      String baseUrl = "",
      CancelToken? cancelToken});
  Future<ApiResultDto> deletesAsync<I>(
      {required String apiName,
      required I input,
      bool tokenRequired = true,
      String baseUrl = "",
      CancelToken? cancelToken});
}