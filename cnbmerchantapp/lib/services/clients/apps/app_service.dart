import 'dart:convert';
import 'dart:io';
import 'package:cnbmerchantapp/core.dart';
import 'package:dio/adapter.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AppService implements IAppService {
  final IChannelProvider _channelProvider = ChannelProvider();
  late Dio _client;

  AppService() {
    _initClient();
  }

  _initClient() {
    BaseOptions options = BaseOptions(
        baseUrl: ApiConfigHelper.url(),
        receiveDataWhenStatusError: true,
        connectTimeout: 5 * 1000, // 5 seconds
        receiveTimeout: 60 * 1000 // 60 seconds
        );

    _client = Dio(options);
    (_client.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
        (HttpClient client) {
      client.badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
      return client;
    };
  }

  @override
  Future<ApiResultDto> deleteAsync(
      {required String apiName,
      bool tokenRequired = true,
      String baseUrl = "",
      CancelToken? cancelToken}) async {
    var result = ApiResultDto(response: []);
    try {
      baseUrl = baseUrl.isNotEmpty ? baseUrl : ApiConfigHelper.url();
      var response = await _client.deleteUri(Uri.parse(baseUrl),
          options: ApiConfigHeader.getOptionHeader(), cancelToken: cancelToken);
      // cancel the requests with "cancelled" message.
      // cancelToken?.cancel('cancelled');
      result.result = ApiOutputDto.fromJson(response.data);
      return result;
    } on DioError catch (e) {
      result.error = ApiResultError.dioError(e);
      return result;
    }
  }

  @override
  Future<ApiResultDto> deletesAsync<I>(
      {required String apiName,
      required I input,
      bool tokenRequired = true,
      String baseUrl = "",
      CancelToken? cancelToken}) async {
    var result = ApiResultDto(response: []);
    try {
      baseUrl = baseUrl.isNotEmpty ? baseUrl : ApiConfigHelper.url();
      var response = await _client.deleteUri(Uri.parse(baseUrl),
          data: await _channelProvider.toEncryptionAsync(jsonEncode(input)),
          options: ApiConfigHeader.getOptionHeader(),
          cancelToken: cancelToken);
      // cancel the requests with "cancelled" message.
      // cancelToken?.cancel('cancelled');
      result.result = ApiOutputDto.fromJson(response.data);
      return result;
    } on DioError catch (e) {
      result.error = ApiResultError(message: e.message);
      return result;
    }
  }

  @override
  Future<ApiResultDto> getAsync(
      {required String apiName,
      bool tokenRequired = true,
      String baseUrl = "",
      CancelToken? cancelToken}) async {
    var result = ApiResultDto(response: []);
    //String token = tokenRequired ? await _cacheService.userAuthAsync() : "";
    try {
      baseUrl = baseUrl.isNotEmpty ? baseUrl : ApiConfigHelper.url();
      var response = await _client.getUri(Uri.parse(baseUrl),
          options: ApiConfigHeader.getOptionHeader(), cancelToken: cancelToken);
      // cancel the requests with "cancelled" message.
      // cancelToken?.cancel('cancelled');
      result.result = ApiOutputDto.fromJson(response.data);
      return result;
    } on DioError catch (e) {
      result.error = ApiResultError(message: e.message);
      return result;
    }
  }

  @override
  Future<ApiResultDto> getsAsync(
      {required String apiName,
      bool tokenRequired = true,
      String baseUrl = "",
      bool isOther = false,
      CancelToken? cancelToken}) async {
    var result = ApiResultDto(response: []);
    //String token = tokenRequired ? await _cacheService.userAuthAsync() : "";
    try {
      baseUrl = baseUrl.isNotEmpty ? baseUrl : ApiConfigHelper.url();
      var response = await _client.getUri(Uri.parse(baseUrl),
          options: ApiConfigHeader.getOptionHeader(), cancelToken: cancelToken);
      // cancel the requests with "cancelled" message.
      // cancelToken?.cancel('cancelled');
      result.result = isOther
          ? ApiOutputOtherDto.fromMap(response.data)
          : ApiOutputDto.fromJson(response.data);
      return result;
    } on DioError catch (e) {
      result.error = ApiResultError(message: e.message);
      return result;
    }
  }

  @override
  Future<dynamic> getDynamicAsync(
      {required String apiName,
      bool tokenRequired = true,
      String baseUrl = "",
      CancelToken? cancelToken}) async {
    var result = ApiResultDto(response: []);
    try {
      baseUrl = baseUrl.isNotEmpty ? baseUrl : ApiConfigHelper.url();
      var response = await _client.getUri(Uri.parse(baseUrl),
          options: ApiConfigHeader.getOptionHeader(), cancelToken: cancelToken);
      result.result = response.data;
      return result;
    } on DioError catch (e) {
      result.error = ApiResultError(message: e.message);
      return result;
    }
  }

  @override
  Future<ApiResultDto> postAsync(
      {required String apiName,
      bool tokenRequired = true,
      String baseUrl = "",
      CancelToken? cancelToken}) async {
    var result = ApiResultDto(response: []);
    //String token = tokenRequired ? await _cacheService.userAuthAsync() : "";
    try {
      baseUrl = baseUrl.isNotEmpty ? baseUrl : ApiConfigHelper.url();
      var response = await _client.postUri(Uri.parse(baseUrl),
          data: '',
          options: ApiConfigHeader.getOptionHeader(),
          cancelToken: cancelToken);
      // cancel the requests with "cancelled" message.
      // cancelToken?.cancel('cancelled');
      result.result = ApiOutputDto.fromJson(response.data);
      return result;
    } on DioError catch (e) {
      result.error = ApiResultError(message: e.message);
      return result;
    }
  }

  @override
  Future<ApiResultDto> postsAsync<I>(
      {required I input,
      bool tokenRequired = true,
      String baseUrl = "",
      CancelToken? cancelToken}) async {
    var result = ApiResultDto(response: []);
    try {
      var toData = await _channelProvider.toEncryptionAsync(jsonEncode(input));
      baseUrl = baseUrl.isNotEmpty ? baseUrl : ApiConfigHelper.url();
      var response = await _client.post(baseUrl,
          data: toData,
          options: ApiConfigHeader.getOptionHeader(),
          cancelToken: cancelToken);
      // cancel the requests with "cancelled" message.
      // cancelToken?.cancel('cancelled');

      if (response.statusCode != null &&
          response.statusCode! >= 500 &&
          response.statusCode! <= 599) {
        result.error = ApiResultError(
          message: "${"Failure".tr}: ${response.statusCode}",
          details: "SomethingWentWrongPleaseTryAgainLater".tr,
        );
        return result;
      }

      var decryptData =
          await _channelProvider.toDescryptionAsync(response.data);
      debugPrint("Request: $input \nResponse: ${jsonDecode(decryptData)}");

      final IAppOnboardingScreenService launchService =
          AppOnboardingScreenService();
      final resultOnboard = await launchService.getAppOnboardingScreenAsync();
      if (resultOnboard.isUserSkip) {
        // final auditLog = AuditLog();
        // final auditLogService = AudioLogService();
        // auditLog.requestEn = toData;
        // auditLog.requestDe = jsonEncode(input);
        // auditLog.responseEn = response.data.toString();
        // auditLog.responseDe = decryptData;
        // auditLogService.addAuditLogData(auditLog);
      }

      var resultDecode = json.decode(decryptData);
      var outputDto = ApiOutputDto.fromJson(resultDecode);
      if (outputDto.rESDATA.isEmpty) {
        result.error = ApiResultError(
            message: "Failure".tr,
            details: "SomethingWentWrongPleaseTryAgainLater".tr);
        return result;
      }
      var splitData = outputDto.rESDATA.split("|");
      result.response = splitData;
      result.result = outputDto;
      result.success = splitData[0].toString().contains("00");
      result.status = splitData[1].toString();
      return result;
    } on DioError catch (e) {
      final errorDetailMessage = DioExceptions.fromDioError(e).toString();
      result.error =
          ApiResultError(message: "Failure".tr, details: errorDetailMessage.tr);
      return result;
    }
  }

  @override
  Future<ApiResultDto> putAsync(
      {required String apiName,
      bool tokenRequired = true,
      String baseUrl = "",
      CancelToken? cancelToken}) async {
    var result = ApiResultDto(response: [], result: null);
    //String token = tokenRequired ? await _cacheService.userAuthAsync() : "";
    try {
      baseUrl = baseUrl.isNotEmpty ? baseUrl : ApiConfigHelper.url();
      var response = await _client.putUri(Uri.parse(baseUrl),
          data: '',
          options: ApiConfigHeader.getOptionHeader(),
          cancelToken: cancelToken);
      // cancel the requests with "cancelled" message.
      // cancelToken?.cancel('cancelled');
      result.result = ApiOutputDto.fromJson(response.data);
      return result;
    } on DioError catch (e) {
      result.error = ApiResultError(message: e.message);
      return result;
    }
  }

  @override
  Future<ApiResultDto> putsAsync<I>(
      {required String apiName,
      required I input,
      bool tokenRequired = true,
      String baseUrl = "",
      CancelToken? cancelToken}) async {
    var result = ApiResultDto(response: []);
    // String token = tokenRequired ? await _cacheService.userAuthAsync() : "";
    try {
      baseUrl = baseUrl.isNotEmpty ? baseUrl : ApiConfigHelper.url();
      var response = await _client.putUri(Uri.parse(baseUrl),
          data: jsonEncode(input),
          options: ApiConfigHeader.getOptionHeader(),
          cancelToken: cancelToken);
      // cancel the requests with "cancelled" message.
      // cancelToken?.cancel('cancelled');
      result.result = ApiOutputDto.fromJson(response.data);
      return result;
    } on DioError catch (e) {
      result.error = ApiResultError(message: e.message);
      return result;
    }
  }
}
