import 'package:cnbmerchantapp/core.dart';

class CanadiaSupportsService extends ICanadiaSupportService {
  final AppService _appService = AppService();
  @override
  Future<ResultModel<FeedbackOutputModel>> sendFeedback(
      FeedbackInputModel input) async {
    var resultModel = ResultModel<FeedbackOutputModel>();
    try {
      var inputModel = ApiInputModel.create(FeedbackInputDto.toMapping(input));
      var inputDto = ApiInputDto.toJson(inputModel);
      var resultDto =
          await _appService.postsAsync(input: inputDto, tokenRequired: false);
      resultModel.result = FeedbackOutputModel.create(
          FeedbackOutputDto.create(resultDto.response));
      resultModel = resultModel.create(
          resultModel.result, resultDto.success, resultDto.error, false);
      return resultModel;
    } catch (e) {
      return resultModel.create(
          null, false, ApiResultError(message: e.toString()), true);
    }
  }
}
