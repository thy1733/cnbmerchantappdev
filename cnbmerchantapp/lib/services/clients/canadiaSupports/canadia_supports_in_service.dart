import 'package:cnbmerchantapp/core.dart';

abstract class ICanadiaSupportService {
  Future<ResultModel<FeedbackOutputModel>> sendFeedback(
      FeedbackInputModel input);
}
