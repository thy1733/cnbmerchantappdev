import 'package:cnbmerchantapp/core.dart';

class FeedbackOutputDto extends ResultStatusOutputDto {
  static FeedbackOutputDto create(List<String> dto) {
    var result = FeedbackOutputDto();
    if (dto.isEmpty) {
      result.statusCode = dto[0];
      result.isSuccess = dto[0] == "00";
      result.description = dto[1];
    }
    return result;
  }
}
