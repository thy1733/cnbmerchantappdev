import 'package:cnbmerchantapp/core.dart';

abstract class ITransactionService {
  Future<ResultModel<TransactionListOutputModel>> getListAsync(
      TransactionListFilterInputModel input);
  Future<ResultModel<TransactionItemDetailsOutputModel>> getDetailsAsync(
      TransactionItemDetailsInputModel input);
  Future<ResultModel<TransactionCreateRefundOutputModel>> refundTxnAsync(
      TransactionCreateRefundInputModel input);
  Future<ResultModel<TransactionListOutputModel>> shareTxnInvoiceAsync(
      String input);
  
  Future<ResultModel<DownloadReportTrxOutputModel>> getDownloadReportAsync(
      TransactionListFilterInputModel input);
}
