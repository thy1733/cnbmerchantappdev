import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class TransactionListOutputDto extends ResultStatusOutputDto {
  final _netSale = 0.0.obs;
  double get netSale => _netSale.value;
  set netSale(double value) => _netSale.value = value;

  final _totalTransaction = 0.0.obs;
  double get totalTransaction => _totalTransaction.value;
  set totalTransaction(double value) => _totalTransaction.value = value;

  final _items = <TransactionItemListDto>[].obs;
  List<TransactionItemListDto> get items => _items;
  set items(List<TransactionItemListDto> value) => _items.value = value;

  static TransactionListOutputDto create(List<String> dto) {
    var result = TransactionListOutputDto();

    result.statusCode = dto[0];
    result.description = dto[1];
    result.netSale = double.tryParse(dto[2]) ?? 0.0;
    if (dto[3].isNotEmpty) {
      result.items = TransactionItemListDto.create(dto[3].split("^"));
    }
    return result;
  }
}
