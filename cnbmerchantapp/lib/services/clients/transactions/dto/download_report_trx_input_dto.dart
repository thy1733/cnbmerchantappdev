import 'package:cnbmerchantapp/core.dart';

class DownloadReportTrxInputDto {
  static String toMapping(TransactionListFilterInputModel input) {
    final startDate = input.date.fromDate.toFormatDateString("yyyyMMdd");
    final endDate = input.date.toDate.toFormatDateString("yyyyMMdd");
    var result =
        "${RoutineName.downloadReportTransactions}[[${input.businessId}[${input.outletId}[${input.cashierId}[$startDate[$endDate[${input.transactionTypeValue}[${input.paymentMethodValue}[0[100";
    return result;
  }
}
