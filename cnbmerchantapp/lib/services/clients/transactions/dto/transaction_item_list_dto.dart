import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class TransactionItemListDto {
  final _transactionId = ''.obs;
  String get transactionId => _transactionId.value;
  set transactionId(String value) => _transactionId.value = value;

  final _transactionType = "".obs;
  String get transactionType => _transactionType.value;
  set transactionType(String value) => _transactionType.value = value;

  final _accountName = ''.obs;
  String get accountName => _accountName.value;
  set accountName(String value) => _accountName.value = value;

  final _transactionDateText = "".obs;
  String get transactionDateText => _transactionDateText.value;
  set transactionDateText(String value) => _transactionDateText.value = value;

  final _transactionDate = DateTime.now().obs;
  DateTime get transactionDate => _transactionDate.value;
  set transactionDate(DateTime value) => _transactionDate.value = value;

  final _currency = ''.obs;
  String get currency => _currency.value;
  set currency(String value) => _currency.value = value;

  final _amount = 0.0.obs;
  double get amount => _amount.value;
  set amount(double value) => _amount.value = value;

  final _isRefund = false.obs;
  bool get isRefund => _isRefund.value;
  set isRefund(bool value) => _isRefund.value = value;

  final _status = "".obs;
  String get status => _status.value;
  set status(String value) => _status.value = value;

  final _paymentMethod = "".obs;
  String get paymentMethod => _paymentMethod.value;
  set paymentMethod(String value) => _paymentMethod.value = value;

  static List<TransactionItemListDto> create(List<String> dto) {
    var result = <TransactionItemListDto>[];
    if (dto.isNotEmpty) {
      for (var i = 0; i < dto.length; i++) {
        var data = dto[i].split("!");
        var input = TransactionItemListDto();
        input.transactionId = data[0];
        input.accountName = data[1];
        input.transactionDate = data[2].fromUTCToDateTime();
        input.currency = data[3];
        input.amount = double.parse(data[4]);
        input.transactionType = data[5];
        input.paymentMethod = data[6];
        input.status = data[7];
        result.add(input);
      }
    }
    return result;
  }
}
