import 'package:cnbmerchantapp/core.dart';

class TransactionCreateRefundInputDto {
  static String toMapping(TransactionCreateRefundInputModel input) {
    var amountToRefund =
        isUSDCurrency ? input.amountToRefund : input.amountToRefund.toInt();
    var originalAmount =
        isUSDCurrency ? input.originalAmount : input.originalAmount.toInt();
    var result =
        "${RoutineName.refundTransaction}[[${input.transactionId}[$amountToRefund[$originalAmount[${input.remark}[${input.businessId}[${input.outletId}[${input.profileId}";
    return result;
  }
}
