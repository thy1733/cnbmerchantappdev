import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class TransactionCreateRefundOutputDto extends ResultStatusOutputDto {
  final _transactionId = ''.obs;
  String get transactionId => _transactionId.value;
  set transactionId(String value) => _transactionId.value = value;

  static TransactionCreateRefundOutputDto create(List<String> dto) {
    var result = TransactionCreateRefundOutputDto();
    if (dto.isNotEmpty) {
      result.statusCode = dto[0];
      result.description = dto[1];
      result.transactionId = dto[2];
    }
    return result;
  }
}
