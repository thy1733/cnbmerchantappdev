
import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class DownloadReportTrxOutputDto extends ResultStatusOutputDto {

  final _totalAmountSale = 0.0.obs;
  double get totalAmountSale => _totalAmountSale.value;
  set totalAmountSale(double value) => _totalAmountSale.value = value;

  final _totalTransactionSale = 0.obs;
  int get totalTransactionSale => _totalTransactionSale.value;
  set totalTransactionSale(int value) => _totalTransactionSale.value = value;

  final _totalAmountRefunde = 0.0.obs;
  double get totalAmountRefund => _totalAmountRefunde.value;
  set totalAmountRefund(double value) => _totalAmountRefunde.value = value;

  final _totalTransactionRefunde = 0.obs;
  int get totalTransactionRefund => _totalTransactionRefunde.value;
  set totalTransactionRefund(int value) =>
      _totalTransactionRefunde.value = value;

  final _items = <DownloadReportTrxItemOutputDto>[].obs;
  List<DownloadReportTrxItemOutputDto> get items => _items;
  set items(List<DownloadReportTrxItemOutputDto> value) => _items.value = value;

  static DownloadReportTrxOutputDto create(List<String> dto){
    var result = DownloadReportTrxOutputDto();
    result.statusCode = dto[0];
    result.isSuccess = dto[0] == "00";
    result.description = dto[1];
    result.totalAmountSale = _emtyResultDoubleChk(dto[3]);
    result.totalTransactionSale = _emtyResulIntChk(dto[4]);
    result.totalAmountRefund = _emtyResultDoubleChk(dto[5]);
    result.totalTransactionRefund = _emtyResulIntChk(dto[6]);
    if (dto[7].isNotEmpty) {
      var items = dto[7].split("^");
      for (var item in items) {
        var datas = item.split("!");
        if(datas.length>15) {
          result.items.add(DownloadReportTrxItemOutputDto.create(datas));
        }
      }
    }
    return result;
  }

  static double _emtyResultDoubleChk(value) {
    return "$value".toUpperCase() == "NULL"
        ? 0
        : double.tryParse("$value") ?? 0;
  }

   static int _emtyResulIntChk(value) {
    return "$value".toUpperCase() == "NULL"
        ? 0
        : int.tryParse("$value") ?? 0;
  }
}