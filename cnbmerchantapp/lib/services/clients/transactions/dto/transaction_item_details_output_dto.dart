import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class TransactionItemDetailsOutputDto extends ResultStatusOutputDto {
  final _amount = 0.0.obs;
  final _approvalCode = ''.obs;
  final _availableRefundAmount = 0.0.obs;
  final _bakongHash = ''.obs;
  final _businessName = ''.obs;
  final _currency = ''.obs;
  final _customerAccount = ''.obs;
  final _customerBankName = ''.obs;
  final _customerName = ''.obs;
  final _items = <TransactionRefundItemListOutputDto>[].obs;
  final _outletName = ''.obs;
  final _paymentMethodName = ''.obs;
  final _paymentProcessedBy = ''.obs;
  final _referalTransactionId = ''.obs;
  final _remark = ''.obs;
  final _status = "".obs;
  final _transactionDate = DateTime.now().obs;
  final _transactionDateText = ''.obs;
  final _transactionId = ''.obs;
  final _transactionType = "".obs;

  String get transactionId => _transactionId.value;

  set transactionId(String value) => _transactionId.value = value;

  String get referalTransactionId => _referalTransactionId.value;

  set referalTransactionId(String value) => _referalTransactionId.value = value;

  String get outletName => _outletName.value;

  set outletName(String value) => _outletName.value = value;

  String get transactionType => _transactionType.value;

  set transactionType(String value) => _transactionType.value = value;

  String get transactionDateText => _transactionDateText.value;

  set transactionDateText(String value) => _transactionDateText.value = value;

  DateTime get transactionDate => _transactionDate.value;

  set transactionDate(DateTime value) => _transactionDate.value = value;

  String get paymentProcessedBy => _paymentProcessedBy.value;

  set paymentProcessedBy(String value) => _paymentProcessedBy.value = value;

  String get paymentMethodName => _paymentMethodName.value;

  set paymentMethodName(String value) => _paymentMethodName.value = value;

  String get customerName => _customerName.value;

  set customerName(String value) => _customerName.value = value;

  String get customerBankName => _customerBankName.value;

  set customerBankName(String value) => _customerBankName.value = value;

  String get customerAccount => _customerAccount.value;

  set customerAccount(String value) => _customerAccount.value = value;

  String get approvalCode => _approvalCode.value;

  set approvalCode(String value) => _approvalCode.value = value;

  double get amount => _amount.value;

  set amount(double value) => _amount.value = value;

  String get status => _status.value;

  set status(String value) => _status.value = value;

  String get currency => _currency.value;

  set currency(String value) => _currency.value = value;

  String get businessName => _businessName.value;

  set businessName(String value) => _businessName.value = value;

  // ignore: invalid_use_of_protected_member
  List<TransactionRefundItemListOutputDto> get items => _items.value;

  set items(List<TransactionRefundItemListOutputDto> value) =>
      _items.value = value;

  String get remark => _remark.value;

  set remark(String value) => _remark.value = value;

  double get availableRefundAmount => _availableRefundAmount.value;

  set availableRefundAmount(double value) =>
      _availableRefundAmount.value = value;

  String get bakongHash => _bakongHash.value;

  set bakongHash(String value) => _bakongHash.value = value;

  final _allowRefundDuration = 48.obs;
  int get allowRefundDuration => _allowRefundDuration.value;
  set allowRefundDuration(int value) => _allowRefundDuration.value = value;

  static TransactionItemDetailsOutputDto create(List<String> dto) {
    var result = TransactionItemDetailsOutputDto();
    result.statusCode = dto[0];
    result.description = dto[1];
    result.transactionId = dto[2];
    result.referalTransactionId = dto[3];
    result.customerAccount = dto[4];
    result.customerBankName = dto[5];
    result.customerName = dto[6];
    result.transactionDateText = dto[7];
    result.transactionDate = dto[7].fromTxnFormatToLocalDateTime();
    result.currency = dto[8];
    result.amount = double.tryParse(dto[9]) ?? 0;
    result.transactionType = dto[10];
    result.paymentMethodName = dto[11];
    result.status = dto[12];
    result.approvalCode = _emtyResultChk(dto[13]);
    result.businessName = dto[14];
    result.outletName = dto[15];
    result.paymentProcessedBy = _emtyResultChk(dto[16]);
    result.remark = _emtyResultChk(dto[17]);
    result.availableRefundAmount = double.tryParse(dto[18]) ?? 0;
    result.bakongHash = dto.length > 20 ? _emtyResultChk(dto[19]) : 'N/A';

    if (dto.asMap().containsKey(20)) {
      final historyItems = dto[20].isNotEmpty
          ? TransactionRefundItemListOutputDto.create(dto[20].split("^"))
          : <TransactionRefundItemListOutputDto>[];
      result.items = historyItems;
    }
    if (dto.asMap().containsKey(21)) {
      debugPrint("Allowed Refund Duration: ${dto[21]}");
      result.allowRefundDuration = int.tryParse(dto[21]) ?? 48;
    }

    return result;
  }

  static String _emtyResultChk(String value) {
    return value.isEmpty ? "N/A" : value;
  }
}
