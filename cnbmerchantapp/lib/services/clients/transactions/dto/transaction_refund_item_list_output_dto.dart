import 'package:get/get.dart';

class TransactionRefundItemListOutputDto {
  final _transactionId = ''.obs;
  String get transactionId => _transactionId.value;
  set transactionId(String value) => _transactionId.value = value;

  final _referalTransactionId = ''.obs;
  String get referalTransactionId => _referalTransactionId.value;
  set referalTransactionId(String value) => _referalTransactionId.value = value;

  final _senderAccountName = ''.obs;
  String get senderAccountName => _senderAccountName.value;
  set senderAccountName(String value) => _senderAccountName.value = value;

  final _senderBankName = ''.obs;
  String get senderBankName => _senderBankName.value;
  set senderBankName(String value) => _senderBankName.value = value;

  final _senderAccountNumber = ''.obs;
  String get senderAccountNumber => _senderAccountNumber.value;
  set senderAccountNumber(String value) => _senderAccountNumber.value = value;

  final _dateTime = ''.obs;
  String get dateTime => _dateTime.value;
  set dateTime(String value) => _dateTime.value = value;

  final _currency = ''.obs;
  String get currency => _currency.value;
  set currency(String value) => _currency.value = value;

  final _amount = 0.0.obs;
  double get amount => _amount.value;
  set amount(double value) => _amount.value = value;

  final _transactionType = ''.obs;
  String get transactionType => _transactionType.value;
  set transactionType(String value) => _transactionType.value = value;

  final _paymentMethod = ''.obs;
  String get paymentMethod => _paymentMethod.value;
  set paymentMethod(String value) => _paymentMethod.value = value;

  final _status = ''.obs;
  String get status => _status.value;
  set status(String value) => _status.value = value;

  final _approvalCode = ''.obs;
  String get approvalCode => _approvalCode.value;
  set approvalCode(String value) => _approvalCode.value = value;

  final _businessName = ''.obs;
  String get businessName => _businessName.value;
  set businessName(String value) => _businessName.value = value;

  final _outletName = ''.obs;
  String get outletName => _outletName.value;
  set outletName(String value) => _outletName.value = value;

  final _paymentReceivedBy = ''.obs;
  String get paymentReceivedBy => _paymentReceivedBy.value;
  set paymentReceivedBy(String value) => _paymentReceivedBy.value = value;

  final _remark = ''.obs;
  String get remark => _remark.value;
  set remark(String value) => _remark.value = value;

  static List<TransactionRefundItemListOutputDto> create(List<String> dto) {
    var result = <TransactionRefundItemListOutputDto>[];
    if (dto.isNotEmpty) {
      for (var i = 0; i < dto.length; i++) {
        var data = dto[i].split("!");
        var input = TransactionRefundItemListOutputDto();
        input.transactionId = data[0];
        input.referalTransactionId = data[1];
        input.senderAccountName = data[2];
        input.senderBankName = data[3];
        input.senderAccountNumber = data[4];
        input.dateTime = data[5];
        input.currency = data[6];
        input.amount = double.parse(data[7]);
        input.transactionType = data[8];
        input.paymentMethod = data[9];
        input.status = data[10];
        input.approvalCode = data[11];
        input.businessName = data[12];
        input.outletName = data[13];
        input.paymentReceivedBy = data[14];
        input.remark = data.length == 16 ? data[15] : "";
        result.add(input);
      }
    }
    return result;
  }
}
