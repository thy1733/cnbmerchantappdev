import 'package:cnbmerchantapp/core.dart';

class TransactionListFilterInputDto {
  static String toMapping(TransactionListFilterInputModel input) {
    final startDate = input.date.fromDate.toFormatDateString("yyyyMMdd");
    final endDate = input.date.toDate.toFormatDateString("yyyyMMdd");
    var result =
        "${RoutineName.getListTransactions}[[${input.businessId}[${input.outletId}[${input.cashierId}[$startDate[$endDate[${input.transactionTypeValue}[${input.paymentMethodValue}[${input.skipCount}[${input.takeCount}";
    return result;
  }
}
