import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class TransactionService implements ITransactionService {
  final AppService _appService = AppService();

  @override
  Future<ResultModel<TransactionItemDetailsOutputModel>> getDetailsAsync(
      TransactionItemDetailsInputModel input) async {
    var resultModel = ResultModel<TransactionItemDetailsOutputModel>();
    try {
      var inputModel =
          ApiInputModel.create(TransactionItemDetailsInputDto.toMapping(input));
      var inputDto = ApiInputDto.toJson(inputModel);
      var resultDto =
          await _appService.postsAsync(input: inputDto, tokenRequired: false);
      if (resultDto.result != null && resultDto.success) {
        return resultModel.create(
            TransactionItemDetailsOutputModel.create(
                TransactionItemDetailsOutputDto.create(resultDto.response)),
            true,
            null,
            false);
      } else {
        return resultModel.create(null, false, resultDto.result.error, true);
      }
    } catch (e) {
      return resultModel.create(
          null, false, ApiResultError(message: e.toString()), true);
    }
  }

  @override
  Future<ResultModel<TransactionListOutputModel>> getListAsync(
      TransactionListFilterInputModel input) async {
    var resultModel = ResultModel<TransactionListOutputModel>();
    try {
      var inputModel =
          ApiInputModel.create(TransactionListFilterInputDto.toMapping(input));
      var inputDto = ApiInputDto.toJson(inputModel);
      var resultDto =
          await _appService.postsAsync(input: inputDto, tokenRequired: false);
      if (resultDto.result != null && resultDto.success) {
        return resultModel.create(
            TransactionListOutputModel.create(
                TransactionListOutputDto.create(resultDto.response)),
            true,
            null,
            false);
      } else {
        return resultModel.create(null, false, resultDto.error, true);
      }
    } catch (e) {
      return resultModel.create(
          null, false, ApiResultError(message: e.toString()), true);
    }
  }

  @override
  Future<ResultModel<TransactionCreateRefundOutputModel>> refundTxnAsync(
      TransactionCreateRefundInputModel input) async {
    var resultModel = ResultModel<TransactionCreateRefundOutputModel>();
    try {
      var inputModel = ApiInputModel.create(
          TransactionCreateRefundInputDto.toMapping(input));
      var inputDto = ApiInputDto.toJson(inputModel);
      var resultDto =
          await _appService.postsAsync(input: inputDto, tokenRequired: false);
      resultModel.result = TransactionCreateRefundOutputModel.create(
          TransactionCreateRefundOutputDto.create(resultDto.response));
      return resultModel.create(
          resultModel.result, resultDto.success, resultDto.error, false);
    } catch (e) {
      return resultModel.create(
          null, false, ApiResultError(message: e.toString()), true);
    }
  }

  @override
  Future<ResultModel<TransactionListOutputModel>> shareTxnInvoiceAsync(
      String input) {
    // Todo: implement shareTxnInvoiceAsync
    throw UnimplementedError();
  }

  @override
  Future<ResultModel<DownloadReportTrxOutputModel>> getDownloadReportAsync(
      TransactionListFilterInputModel input) async {
    var resultModel = ResultModel<DownloadReportTrxOutputModel>();
    try {
      var inputModel =
          ApiInputModel.create(DownloadReportTrxInputDto.toMapping(input));
      var inputDto = ApiInputDto.toJson(inputModel);
      var resultDto =
          await _appService.postsAsync(input: inputDto, tokenRequired: false);
      if (resultDto.result != null && resultDto.success) {
        return resultModel.create(
          DownloadReportTrxOutputModel.create(
              DownloadReportTrxOutputDto.create(resultDto.response)),
          true,
          null,
          false,
        );
      } else {
        if (resultDto.response.isNotEmpty && resultDto.response[0] == "06") {
          return resultModel.create(
            null,
            false,
            ApiResultError(
              message: "Failure".tr,
              details: "ThereAreNoTrxToExport".tr,
            ),
            true,
          );
        }
        return resultModel.create(null, false, resultDto.error, true);
      }
    } catch (e) {
      return resultModel.create(
          null, false, ApiResultError(message: e.toString()), true);
    }
  }
}
