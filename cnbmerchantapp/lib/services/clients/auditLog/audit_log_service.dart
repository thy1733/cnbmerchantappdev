import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cnbmerchantapp/core.dart';

class AudioLogService {
  final CollectionReference collection =
      FirebaseFirestore.instance.collection("audit-log");
  Stream<QuerySnapshot> getStream() {
    return collection.snapshots();
  }

  Future<DocumentReference> addAuditLogData(AuditLog auditLog) {
    return collection.add(auditLog.toJson());
  }
}
