import 'package:cnbmerchantapp/core.dart';

abstract class IBankOfficeService {
  Future<ResultModel<BankOfficeListOutputModel>> getListBankOfficeAsync(
      BankOfficeListInputModel input);
}
