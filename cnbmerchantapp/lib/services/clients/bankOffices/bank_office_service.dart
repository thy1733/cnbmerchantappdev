import 'package:cnbmerchantapp/core.dart';

class BankOfficeService implements IBankOfficeService {
  final AppService _appService = AppService();

  @override
  Future<ResultModel<BankOfficeListOutputModel>> getListBankOfficeAsync(
    BankOfficeListInputModel input,
  ) async {
    var resultModel = ResultModel<BankOfficeListOutputModel>();
    try {
      var inputModel =
          ApiInputModel.create(BankOfficeListInputDto.toMapping(input));
      var inputDto = ApiInputDto.toJson(inputModel);
      var resultDto =
          await _appService.postsAsync(input: inputDto, tokenRequired: false);
      if (resultDto.result != null && resultDto.success) {
        return resultModel.create(
            BankOfficeListOutputModel.create(
                BankOfficeListOutputDto.create(resultDto.response)),
            true,
            null,
            false);
      } else {
        return resultModel.create(null, false, resultDto.result.error, true);
      }
    } catch (e) {
      return resultModel.create(
          null, false, ApiResultError(message: e.toString()), true);
    }
  }
}
