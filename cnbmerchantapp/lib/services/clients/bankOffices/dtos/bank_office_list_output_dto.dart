import 'package:cnbmerchantapp/core.dart';

class BankOfficeListOutputDto extends ResultStatusOutputDto {
  BankOfficeListOutputDto();

  List<BankOfficeOutputDto> items = <BankOfficeOutputDto>[];

  static BankOfficeListOutputDto create(List<String> dto) {
    var result = BankOfficeListOutputDto();
    result.statusCode = dto[0];
    result.isSuccess = result.statusCode == "00";
    result.description = dto[1];
    var itemsRespone = dto[2];
    if (itemsRespone.isNotEmpty) {
      var itemsList = itemsRespone.split("^");
      for (var item in itemsList) {
        result.items.add(BankOfficeOutputDto.create(item.split("!")));
      }
    }
    return result;
  }
}
