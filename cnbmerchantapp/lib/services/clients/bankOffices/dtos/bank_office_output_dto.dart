
class BankOfficeOutputDto {
  String id = '';
  String locationName = '';
  String locationAddress = '';
  double latitude = 0.0;
  double longitude = 0.0;
  int startWorkingHour = 8;
  int stopWorkingHour = 17;

  BankOfficeOutputDto({
    this.id = "",
    this.locationName = "",
    this.locationAddress = "",
    this.latitude = 0.0,
    this.longitude = 0.0,
    this.startWorkingHour = 8,
    this.stopWorkingHour = 17,
  });

  static BankOfficeOutputDto create(List<String> dto) {
    var result = BankOfficeOutputDto();

    if (dto.isNotEmpty) {
      result.id = dto[0];
      result.locationName = dto[1].replaceAll(";", ",");
      result.longitude = double.tryParse( dto[2])??0;
      result.latitude = double.tryParse( dto[3])??0;
      result.locationAddress = dto[4].replaceAll(";", ",");
      // result.startWorkingHour = int.tryParse( dto[5])??0;
      // result.stopWorkingHour = int.tryParse(dto.elementAt(6))??0;
    }
    return result;
  }
}
