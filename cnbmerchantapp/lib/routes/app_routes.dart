abstract class Routes {
  static const init = '/';
  static const dev = '/dev';
  static const login = '/login';
  static const main = '/main';
  static const onboard = '/onboard';

  // for tab menus
  static const tabQR = '/tabQR';
  static const tabTransaction = '/tabTransaction';
  static const tabNotification = '/tabNotification';
  static const tabSetting = '/tabSetting';

  //#region Transaction
  static const transactionDetail = '/transactionDetail';
  static const filterTransaction = '/filterTransaction';
  static const refundTransaction = '/refundTransaction';
  static const refundTransactionDetail = '/refundTransactionDetail';
  //#endregion

  static const resetPassword = '/resetPassword';
  static const forgotUsernamePassword = '/forgotUsernamePassword';
  static const forgotPassword = '/forgotPassword';
  static const verifyOtp = '/verifyOtp';
  static const newPasswordScreen = '/newPasswordScreen';
  static const success = '/success';
  static const activateTermCondition = '/activateTermCondition';
  static const activateOwnerScreen = '/activateOwnerScreen';
  static const activateOwnerExistingAccount = '/activateOwnerExistingAccount';
  static const activateCashierScreen = '/activateCashierScreen';
  static const activateExistingMerchantScreen = '/activateExistingMerchantScreen';
  static const activateCreateUsernameScreen = '/activateCreateUsernameScreen';
  static const createUserName = '/createUserName';
  static const newPinScreen = '/newPinScreen';
  static const resetPin = '/resetPin';
  static const setupBusinessProfile = '/setupBusinessProfile';
  static const updateBusiness = '/updateBusiness';
  static const businessProfile = '/businessProfile';
  static const locationAddress = '/locationAddress';
  static const sharedLinked = '/sharedLinked';
  static const qrDisplay = '/qrDisplay';
  static const receivePayment = '/receivePayment';
  static const inputAmount = '/inputAmount';

  // static const transactionReportScreen = '/transactionReportScreen';
  static const changeUserName = '/changeUserName';
  static const changePassword = '/changePassword';
  static const confirmPassword = '/confirmPassword';
  static const confirmChangedPassword = '/confirmChangedPassword';
  static const confirmMessageScreen = '/confirmMessageScreen';
  static const outletListScreen = '/salePointListScreen';
  static const outletDetailScreen = '/salePointDetailScreen';
  static const cashierListScreen = '/cashierListScreen';
  static const cashierDetailScreen = '/cashierDetailScreen';
  static const kWebview = '/kWebview';
  static const manageBusiness = '/manageBusiness';
  static const requestQrMaterialListScreen = '/requestQrMaterialListScreen';
  static const requestQrMaterialScreen = '/requestPrintedQr';
  static const reviewRequestQrMaterialScreen = '/reviewRequestQrMaterialScreen';
  static const almostThere = '/almostThere';
  static const userGuides = '/userGuides';
  static const webView = "/webView";
  static const merchantPartnershipAgreement = '/merchantPartnershipAgreement';
  static const businessDetailScreen = '/businessDetailScreen';
  static const merchantPromotion = '/merchantPromotion';
  static const merchantDetail = '/merchantDetail';
  static const merchantPromotionRequest = '/merchantPromotionRequest';
  static const requestMerchantReferal = '/requestMerchantReferal';
  static const upgradeBusiness = '/upgradeBusiness';
  static const newsItemDetail = '/newsItemDetail';
  static const canadiaSupports = '/canadiaSupports';
  static const inviteFriend = '/inviteFriend';
  static const friendDetail = '/friendDetail';
  static const partnershipAgreementScreen = '/partnershipAgreementScreen';
  static const merchantTermsAndCondition = '/merchantTermsAndCondition';
  static const newsDetail = '/newsDetail';
  static const setupOutletScreen = '/setupOutletScreen';
  static const setupCashierScreen = '/setupCashierScreen';
  static const updateCashierScreen = '/updateCashierScreen';
}
