import 'package:cnbmerchantapp/core.dart';
import 'package:cnbmerchantapp/views/authenticates/new_password_screen.dart';
import 'package:cnbmerchantapp/views/authenticates/new_pin_screen.dart';
import 'package:cnbmerchantapp/views/forgotPasswords/xcore.dart';
import 'package:cnbmerchantapp/views/main_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AppPages {
  static String init = Routes.onboard;

  static final routes = [
    //#region front page of user
    GetPage(
      name: Routes.login,
      page: () => const LoginScreen(),
    ),
    GetPage(
      name: Routes.init,
      page: () => const MainScreen(),
    ),
    GetPage(
      name: Routes.onboard,
      page: () => const OnboardScreen(),
    ),
    //#endregion

    //#region tab menu
    GetPage(
      name: Routes.tabTransaction,
      page: () => const TabTransactionScreen(),
    ),
    GetPage(
      name: Routes.tabNotification,
      page: () => const TabNotificationScreen(),
    ),
    GetPage(
      name: Routes.tabSetting,
      page: () => const TabSettingScreen(),
    ),
    //#endregion

    //#region Transaction
    GetPage(
      name: Routes.transactionDetail,
      page: () => const TransactionDetailScreen(),
    ),
    GetPage(
      name: Routes.filterTransaction,
      page: () => const FilterTransactionScreen(),
    ),
    GetPage(
      name: Routes.refundTransaction,
      page: () => const RefundTransactionScreen(),
    ),
    GetPage(
      name: Routes.refundTransactionDetail,
      page: () => const RefundTransactionDetailScreen(),
    ),
    //#endregion

    //#region qr
    GetPage(
      name: Routes.qrDisplay,
      page: () => const QrDisplayScreen(),
    ),
    //#endregion

    //#region Manage Business
    GetPage(
        name: Routes.manageBusiness, page: () => const BusinessListScreen()),

    GetPage(
        name: Routes.setupBusinessProfile,
        page: () => const SetupBusinessScreen()),

    GetPage(name: Routes.locationAddress, page: () => const Scaffold()),

    GetPage(
        name: Routes.businessDetailScreen,
        page: () => const BusinessDetailScreen()),

    GetPage(
        name: Routes.setupOutletScreen, page: () => const UpsertOutletScreen()),

    GetPage(
        name: Routes.setupCashierScreen,
        page: () => const SetupCashierScreen()),

    GetPage(
        name: Routes.updateCashierScreen,
        page: () => const UpdateCashierScreen()),
    //#endregion Manage Business
    //#region setting security
    GetPage(
      name: Routes.changeUserName,
      page: () => const ChangeUsernameScreen(),
    ),

    GetPage(
      name: Routes.confirmPassword,
      page: () => const ConfirmPasswordScreen(),
    ),
    //#endregion

    //#region support
    GetPage(
      name: Routes.canadiaSupports,
      page: () => CanadiaSupportsScreen(),
    ),
    GetPage(
      name: Routes.partnershipAgreementScreen,
      page: () => PartnershipAgreementScreen(),
    ),
    GetPage(
      name: Routes.merchantTermsAndCondition,
      page: () => MerchantTermsAndConditionScreen(),
    ),
    GetPage(
      name: Routes.merchantPartnershipAgreement,
      page: () => MerchantPartnershipAgreementScreen(),
    ),
    GetPage(
      name: Routes.userGuides,
      page: () => const UserGuidesScreen(),
    ),
    GetPage(
      name: Routes.webView,
      page: () => WebViewScreen(),
    ),
    //#endregion

    // #region Setting more feature

    GetPage(
      name: Routes.merchantPromotion,
      page: () => TabMerchantPromotionListScreen(),
    ),

    GetPage(
      name: Routes.merchantPromotionRequest,
      page: () => const CreateMerchantPromotionScreen(),
    ),

    GetPage(
      name: Routes.merchantDetail,
      page: () => MerchantPromotionDetailScreen(),
    ),

    GetPage(
      name: Routes.inviteFriend,
      page: () => const InviteFriendScreen(),
    ),
    GetPage(
      name: Routes.friendDetail,
      page: () => const FriendDetailScreen(),
    ),

    //#endregion

    // #region Authenticate
    GetPage(
      name: Routes.forgotPassword,
      page: () => const ForgotPasswordScreen(),
    ),
    GetPage(
      name: Routes.newPasswordScreen,
      page: () => const NewPasswordScreen(),
    ),
    GetPage(
      name: Routes.newPinScreen,
      page: () => const NewPinScreen(),
    ),
    // #endregion

    // #region Activation
    GetPage(
      name: Routes.activateTermCondition,
      page: () => const ActivateTermConditionScreen(),
    ),
    GetPage(
      name: Routes.activateOwnerScreen,
      page: () => const ActivateOwnerScreen(),
    ),
    GetPage(
      name: Routes.activateCreateUsernameScreen,
      page: () => const ActivateNewUsernameScreen(),
    ),
    GetPage(
      name: Routes.activateCashierScreen,
      page: () => const ActivateCashierScreen(),
    ),
    // #endregion

    // #region Merchant Referral
    GetPage(
      name: Routes.requestMerchantReferal,
      page: () => const RequestMerchantReferralScreen(),
    ),
    // #endregion

    // #region Qr Material
    GetPage(
      name: Routes.requestQrMaterialListScreen,
      page: () => const RequestQrMaterialListScreen(),
    ),
    GetPage(
      name: Routes.requestQrMaterialScreen,
      page: () => const RequestQrMaterialScreen(),
    ),
    GetPage(
      name: Routes.reviewRequestQrMaterialScreen,
      page: () => const ReviewRequestQrMaterialScreen(),
    ),
    // #endregion

    // #region Upgrade Business Screen
    GetPage(
      name: Routes.upgradeBusiness,
      page: () => const UpgradeBusinessScreen(),
    ),
    // #endregion

    GetPage(
      name: Routes.newsDetail,
      page: () => const NotificationNewsDetailScreen(),
    ),
  ];
}
