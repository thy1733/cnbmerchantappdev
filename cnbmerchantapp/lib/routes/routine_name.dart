class RoutineName {
  static const String activation = "CND.MCA.ACTIVATION";
  static const String getAccountByCid = "CND.MCA.GETACC";
  static const String registerAccount = "CND.MCA.REGISTER.AC";
  static const String verifyOtpCode = "CND.MCA.VERIFYOTPCODE.AC";
  static const String getAccountInfo = "CND.MCA.GETAC.INFO";
  static const String checkExistingAccount = "CND.MCA.EXIST.ACCT";
  static const String checkExistingUsername = "CND.MCA.VALIDATE.LOGIN";
  static const String auditLogs = "CND.MCA.AUDITLOGS";
  static const String auditLogToExcel = "CND.MCA.AUDITLOGSTOEXCEL";
  static const String auditLogToExcelExporter =
      "CND.MCA.AUDITLOGLISTEXCEL.EXPORTER";

  static const String createBusiness = "CND.MCA.CREATE.BUSINESS";
  static const String deleteBusiness = "CND.MCA.DELETE.BUSINESS";
  static const String diableBusiness = "CND.MCA.DISABLE.BUSINESS";
  static const String enableBusiness = "CND.MCA.ENABLE.BUSINESS";
  static const String findBusiness = "CND.MCA.FIND.BUSINESS";
  static const String businessDetails = "CND.MCA.GETDETAIL.BUSINESS";
  static const String getListBusiness = "CND.MCA.GETLIST.BUSINESS";
  static const String updateBusiness = "CND.MCA.UPDATE.BUSINESS";

  static const String createBusinessLocation =
      "CND.MCA.CREATE.BUSINESSLOCATION";
  static const String deleteBusinessLocation =
      "CND.MCA.DELETE.BUSINESSLOCATION";
  static const String disableBusinessLocation =
      "CND.MCA.DISABLE.BUSINESSLOCATION";
  static const String enableBusinessLocation =
      "CND.MCA.ENABLE.BUSINESSLOCATION";
  static const String findBusinessLocation = "CND.MCA.FIND.BUSINESSLOCATION";
  static const String businessLocationDetails =
      "CND.MCA.GETDETAIL.BUSINESSLOCATION";
  static const String getListBusinessLocation =
      "CND.MCA.GETLIST.BUSINESSLOCATION";
  static const String updateBusinessLocation =
      "CND.MCA.UPDATE.BUSINESSLOCATION";

  static const String createBusinessType = "CND.MCA.CREATE.BUSINESSTYPE";
  static const String deleteBusinessType = "CND.MCA.DELETE.BUSINESSTYPE";
  static const String disableBusinessType = "CND.MCA.DISABLE.BUSINESSTYPE";
  static const String enableBusinessType = "CND.MCA.ENABLE.BUSINESSTYPE";
  static const String findBusinessType = "CND.MCA.FIND.BUSINESSTYPE";
  static const String businessTypeDetails = "CND.MCA.GETDETAIL.BUSINESSTYPE";
  static const String getListBusinessTypes = "CND.MCA.GETLIST.BUSINESSTYPE";
  static const String updateBusinessType = "CND.MCA.UPDATE.BUSINESSTYPE";

  static const String createCashier = "CND.MCA.CREATE.CASHIER";
  static const String activateCashier = "CND.MCA.ACTIVATE.CASHIER";
  static const String deactivateCashier = "CND.MCA.DEACTIVATE.CASHIER";
  static const String deleteCashier = "CND.MCA.DELETE.CASHIER";
  static const String findCashier = "CND.MCA.FIND.CASHIER";
  static const String cashierDetails = "CND.MCA.GETDETAIL.CASHIER";
  static const String getListCashiers = "CND.MCA.GETLIST.CASHIER";
  static const String updateCashier = "CND.MCA.UPDATE.CASHIER";
  static const String reInviteCashier = "CND.MCA.INVITECODE.CASHIER";

  static const String uploadDocument = "CND.MCA.UPLOAD.DOCUMENT";
  static const String deleteDocument = "CND.MCA.DELETE.DOCUMENT";

  static const String findFund = "CND.MCA.FIND.FUND";
  static const String fundDetails = "CND.MCA.GETDETAIL.FUND";
  static const String getListFunds = "CND.MCA.GETLIST.FUND";
  static const String refund = "CND.MCA.REFUND.FUND";
  static const String refundTransaction = "CND.MCA.REFUND.TRANSACTION";

  static const String createOnboard = "CND.MCA.CREATE.ONBOARD";
  static const String forwardOnboard = "CND.MCA.MOVEFORWARD.ONBOARD";
  static const String backForwardOnboard = "CND.MCA.MOVEBACKWARD.ONBOARD";
  static const String deleteOnboard = "CND.MCA.DELETE.ONBOARD";
  static const String disableOnboard = "CND.MCA.DISABLE.ONBOARD";
  static const String enableOnboard = "CND.MCA.ENABLE.ONBOARD";
  static const String findOnboard = "CND.MCA.FIND.ONBOARD";
  static const String onboardDetails = "CND.MCA.GETDETAIL.ONBOARD";
  static const String getListOnboards = "CND.MCA.GETLIST.ONBOARD";
  static const String updateOnboards = "CND.MCA.UPDATE.ONBOARD";

  static const String createOutlet = "CND.MCA.CREATE.OUTLET";
  static const String deleteOutlet = "CND.MCA.DELETE.OUTLET";
  static const String disableOutlet = "CND.MCA.DISABLE.OUTLET";
  static const String enableOutlet = "CND.MCA.ENABLE.OUTLET";
  static const String findOutlet = "CND.MCA.FIND.OUTLET";
  static const String outletDetails = "CND.MCA.GETDETAIL.OUTLET";
  static const String getListOutlets = "CND.MCA.GETLIST.OUTLET";
  static const String updateOutlet = "CND.MCA.UPDATE.OUTLET";

  static const String requstGenerateQR = "CND.MCA.REQUESTQRCODE";

  static const String createRole = "CND.MCA.CREATE.ROLE";
  static const String getListRole = "CND.MCA.GETROLES.ROLE";
  static const String updateRole = "CND.MCA.UPDATE.ROLE";
  static const String deleteRole = "CND.MCA.DELETE.ROLE";
  static const String getUserRole = "CND.MCA.GETROLES.USER";

  static const String getAllPermission = "CND.MCA.GETALLPERMISSIONS.ROLE";
  static const String getRoleForEdit = "CND.MCA.GETROLEFOREDIT.ROLE";
  static const String getRole = "CND.MCA.GET.ROLE";
  static const String getAllRole = "CND.MCA.GETALL.ROLE";
  static const String sendSMS = "CND.MCA.SENT.SENDSMS";

  static const String getcurrentLoginInfo = "CND.MCA.GETCURLOGININFO.SESSION";
  static const String findTermCondition = "CND.MCA.FIND.TERMCONDITION";
  static const String createTermCondition = "CND.MCA.CREATE.TERMCONDITION";
  static const String updateTermCondition = "CND.MCA.UPDATE.TERMCONDITION";
  static const String getListTermCondition = "CND.MCA.GETLIST.TERMCONDITION";
  static const String authenticateTokenAuth = "CND.MCA.AUTHENTICATE.TOKENAUTH";
  static const String getExternalAuthenticateTokenAuth =
      "CND.MCA.GETEXTERNALAUTHPROVIDER.TOKENAUTH";
  static const String externalAuthenticateTokenAuth =
      "CND.MCA.EXTERNALAUTHENTICATE.TOKENAUTH";

  static const String createTransaction = "CND.MCA.CREATE.TRANSACTION";
  static const String deleteTransaction = "CND.MCA.DELETE.TRANSACTION";
  static const String disableTransaction = "CND.MCA.DISABLE.TRANSACTION";
  static const String enableTransaction = "CND.MCA.ENABLE.TRANSACTION";
  static const String findTransaction = "CND.MCA.FIND.TRANSACTION";
  static const String transactionDetails = "CND.MCA.GETDETAIL.TRANSACTION";
  static const String getListTransactions = "CND.MCA.GETLIST.TRANSACTION";
  static const String getListNotifications = "CND.MCA.LIST.NOTIF.TXN";
  static const String updateTransactions = "CND.MCA.UPDATE.TRANSACTION";
  static const String downloadReportTransactions =
      "CND.MCA.DOWNLOAD.REPORT.TRANSACTION";

  static const String createUser = "CND.MCA.CREATE.USER";
  static const String updateUser = "CND.MCA.UPDATE.USER";
  static const String deleteUser = "CND.MCA.DELETE.USER";
  static const String activateUser = "CND.MCA.ACTIVATE.USER";
  static const String deactivateUser = "CND.MCA.DEACTIVATE.USER";
  static const String changeLanguageUser = "CND.MCA.CHANGELANGUAGE.USER";
  static const String changePasswordUser = "CND.MCA.CHANGEPASSWORD.USER";
  static const String resetPasswordUser = "CND.MCA.RESETPASSWORD.USER";
  static const String resetPIN = "CND.MCA.RESETPIN.USER";
  static const String forgotPasswordUser = "CND.MCA.FORGOTPASSWORD.USER";
  static const String getUser = "CND.MCA.GET.USER";
  static const String getAllUser = "CND.MCA.GETALL.USER";

  static const String sendFeedback = "CND.MCA.FEEDBACK.BANKSUPPORT";

  static const String getUserGuides = "CND.MCA.GETLIST.USERGUIDES";
  static const String upgradeBusiness = "CND.MCA.UPGRADE.BUSINESS";

  static const String listBankOffice = "CND.MCA.LIST.BANKOFFICE";
  static const String listRequestPrintedQr = "CND.MCA.LIST.REQPRINTEDQR";
  static const String getFeeRequestPrintedQr = "CND.MCA.FEE.REQPRINTEDQR";
  static const String requestPrintedQr = "CND.MCA.REQPRINTEDQR";

  static const String getSaleAnalytic = "CND.MCA.SALEANALYTIC";

  static const String reqMerchantReferral = "CND.MCA.MERCHANTREFERRAL";
}
