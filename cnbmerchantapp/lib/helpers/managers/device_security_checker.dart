// ignore_for_file: use_build_context_synchronously

import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_jailbreak_detection/flutter_jailbreak_detection.dart';
import 'dart:io';

import 'package:get/get.dart';

class DeviceSecurityChecker {
  static final DeviceSecurityChecker _shared = DeviceSecurityChecker();

  static DeviceSecurityChecker get shared => _shared;

  Future<bool> checkSecuredDevice(BuildContext context) async {
    bool jailbroken = await FlutterJailbreakDetection.jailbroken;
    if (jailbroken) {
      showAlertDialog(
          context: context,
          title: "Attention".tr,
          content: "JailbreakDetected".tr,
          defaultActionText: "CloseApp".tr,
          onDefaultActionTap: () {
            if (Platform.isAndroid) {
              SystemNavigator.pop();
            } else {
              exit(0);
            }
          });
    }
    return jailbroken;
  }
}
