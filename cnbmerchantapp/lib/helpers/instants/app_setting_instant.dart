import 'package:cnbmerchantapp/models/settings/xcore.dart';
import 'package:get/get.dart';

class AppSettingInstant {
  static final _setting = AppSettingModel().obs;
  static AppSettingModel get setting => _setting.value;
  static set setting(AppSettingModel value) => _setting.value = value;

}
