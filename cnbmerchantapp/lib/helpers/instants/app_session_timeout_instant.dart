import 'dart:async';

import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AppSessionTimeoutInstant {
  static final List<int> timeoutList = [ 30, 60, 120, 180, 300];
  static Timer? _timer;
  // static VoidCallback? _onSessionEnd;
  static bool _userActive = false;
  // to prevent show logout PIN when user stay in page that don't need to logout route navigate
  static bool _atLockRoute = false;
  static Future<void> startSession({
    VoidCallback? onSessionEnd,
    required String title,
    String image = "",
    String? message,
    String? description,
    required Function(RequestCallback callback) onSuccess,
  }) async {
    _atLockRoute = true;
    if (_userActive) {
      // _onSessionEnd = onSessionEnd;
      _onSessionStart();
      onSuccess(((p0, p1) => {}));
    } else {
      await Get.to(ConfirmPinScreen(
        onSuccess: (callback) {
          _userActive = true;
          // _onSessionEnd = onSessionEnd;
          _onSessionStart();
          Get.back();
          onSuccess(callback);
        },
        onBackPress: () {
          // Get.back();
        },
        title: title,
        message: message,
        description: description,
        reachManyAttemp: () {},
        image: image,
      ));
    }
  }

  /// !!Don't call this func on other screen!
  ///
  /// this func design for only call in [AppSessionTimoutWidget]. when user have interaction.
  static void handleUserInteraction([_]) {
    if ((_timer?.isActive ?? false) == true) {
      _onSessionStart();
    }
  }

  static void _onSessionStart() {
    _timer?.cancel();
    _timer = Timer(
        Duration(seconds: AppSettingInstant.setting.inactivityTimeoutSeccond),
        _onEnd);
  }

  static bool sessionStatus() {
    if (_userActive) {
      return true;
    } else if (_timer != null) {
      return _timer!.isActive;
    }
    return false;
  }

  static void pauseSession() {
    _timer?.cancel();
  }

  static void beginSession() {
    if (_timer != null) {
      _onSessionStart();
    }
  }

  /// when leave lockRoute(route that need to eneable auto lock).
  /// this function don't stop counting. this only change condition for counter
  /// when count end it's don't show pin to unlock again
  static void leaveLockRoute() {
    _atLockRoute = false;
  }

  static void stopSession() {
    _timer?.cancel();
    _timer = null;
    _userActive = false;
  }

  static void _onEnd() async {
    // clear timer
    _timer?.cancel();
    _timer = null;
    _userActive = false;

    // check current route
    var currentRoute = Get.currentRoute;
    if (_atLockRoute) {
      bool isContinues = false;
      await Get.to(
        ConfirmPinScreen(
          onSuccess: (callback) {
            _userActive = true;
            _onSessionStart();
            isContinues = true;
            Get.back();
          },
          onBackPress: () {},
          // title: title,
          // message: message,
          // description: description,
          reachManyAttemp: () {},
          // image: image,
        ),
        transition: Transition.fade,
        popGesture: false,
      );
      if (isContinues) return;
      // move to main
      navigator?.popUntil((route) => route.isFirst);
      // move main tab to home
      var mainCon = Get.find<MainScreenController>();
      if (mainCon.initialized && currentRoute != Routes.main) {
        mainCon.tabController.animateTo(0);
      }
    }
    
  }
}
