import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class AppOutletTempInstant {
  static final _selectedOutlet = OutletItemListOutputModel().obs;
  static OutletItemListOutputModel get selectedOutlet => _selectedOutlet.value;
  static set selectedOutlet(OutletItemListOutputModel value) => _selectedOutlet.value = value;
  
  static final _outletList = OutletListOutputModel().obs;
  static OutletListOutputModel get outletList =>  _outletList.value;
  static set outletList(OutletListOutputModel value) => _outletList.value = value;
  
}
