import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class AppBusinessTempInstant {
  static final _appCacheBusiness = ManageBusinessListOutputModel().obs;
  static ManageBusinessListOutputModel get appCacheBusiness =>
      _appCacheBusiness.value;
  static set appCacheBusiness(ManageBusinessListOutputModel value) =>
      _appCacheBusiness.value = value;

  static final _selectedBusiness = ManageBusinessItemListOutputModel().obs;
  static ManageBusinessItemListOutputModel get selectedBusiness =>
      _selectedBusiness.value;
  static set selectedBusiness(ManageBusinessItemListOutputModel value) =>
      _selectedBusiness.value = value;
}
