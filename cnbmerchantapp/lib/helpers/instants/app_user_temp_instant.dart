import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class AppUserTempInstant {
   static final _appCacheUser = AppUserProfileInfoModel.create().obs;
   static AppUserProfileInfoModel get appCacheUser => _appCacheUser.value;
   static set appCacheUser(AppUserProfileInfoModel value) => _appCacheUser.value = value;

   static final _transactionId = "".obs;
   static String get transactionId => _transactionId.value;
   static set transactionId(String value) => _transactionId.value = value;


  static bool get isCashier => appCacheUser.userType ==1;
}