import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class DeviceInfoTempInstant {
   static final _deviceInfo = AppOnboardingScreenModel.create().obs;
   static AppOnboardingScreenModel get deviceInfo => _deviceInfo.value;
   static set deviceInfo(AppOnboardingScreenModel value) => _deviceInfo.value = value;
}