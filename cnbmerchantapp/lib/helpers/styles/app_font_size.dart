abstract class AppFontSize {
  static double title = 20;
  static double subtitle = 16;
  static double body = 14;
  static double text = 13;
  static double label = 11;
  static double buttonLabel = 13;
}
