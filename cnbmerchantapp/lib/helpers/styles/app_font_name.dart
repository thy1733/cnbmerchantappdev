abstract class AppFontName {
  /// This font use for none localize text
  /// GoogleFonts.roboto().fontFamily
  static const String familyFont = 'Roboto';
}
