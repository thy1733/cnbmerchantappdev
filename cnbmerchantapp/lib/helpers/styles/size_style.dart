import 'package:flutter/material.dart';

// Default Design Size is iPhone 13
Size defaultDesignSize = const Size(390, 844);

EdgeInsets headerPadding = const EdgeInsets.only(left: 15, right: 15, top: 10);
EdgeInsets containerPadding =
    const EdgeInsets.only(left: 7, top: 10, right: 7, bottom: 0);

Widget getBottomPadding(BuildContext context) {
  double height = MediaQuery.of(context).viewPadding.bottom * 0.45;
  height = height > 0 ? height : 0;
  return SizedBox(
    height: MediaQuery.of(context).viewInsets.bottom > 0 ? 0 : height,
  );
}

double getBottomSafeArea(BuildContext context) =>
    MediaQuery.of(context).viewPadding.bottom;

double getTopSafeArea(BuildContext context) =>
    MediaQuery.of(context).viewPadding.top;

bool hasSafeArea(BuildContext context) =>
    MediaQuery.of(context).viewPadding.bottom > 0;
