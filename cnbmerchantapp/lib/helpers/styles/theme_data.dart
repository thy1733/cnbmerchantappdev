import 'package:flutter/material.dart';

kDatePickerThemData(BuildContext context, Widget? widget) {
  return Theme(
    data: ThemeData.light().copyWith(
      colorScheme: ColorScheme.fromSwatch(
        primarySwatch: Colors.red,
        primaryColorDark: Colors.teal,
        accentColor: Colors.teal,
      ),
      dialogBackgroundColor: Colors.white,
    ),
    child: widget!,
  );
}
