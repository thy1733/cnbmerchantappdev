// ignore_for_file: deprecated_member_use

import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class AppTextStyle {
  static TextStyle headline1 = TextStyle(
    fontSize: 96.sp,
    color: AppColors.white,
    letterSpacing: -1.5,
    fontWeight: FontWeight.w300,
  );
  static TextStyle headline2 = TextStyle(
    fontSize: 60.sp,
    color: AppColors.white,
    letterSpacing: -0.5,
    fontWeight: FontWeight.w300,
  );
  static TextStyle headline3 = TextStyle(
    fontSize: 48.sp,
    color: AppColors.white,
    letterSpacing: 0,
    fontWeight: FontWeight.w400,
  );
  static TextStyle headline4 = TextStyle(
    fontSize: 34.sp,
    color: AppColors.white,
    letterSpacing: 0.25,
    fontWeight: FontWeight.w400,
  );
  static TextStyle headline5 = TextStyle(
    fontSize: 24.sp,
    color: AppColors.white,
    letterSpacing: 0,
    fontWeight: FontWeight.w400,
  );
  static TextStyle headline6 = TextStyle(
    fontSize: 19.sp,
    color: AppColors.white,
    letterSpacing: 0.15,
    fontWeight: FontWeight.w500,
  );
  static TextStyle subtitle1 = TextStyle(
    fontSize: 16.sp,
    fontWeight: FontWeight.w400,
    letterSpacing: 0.15,
  );
  static TextStyle subtitle2 = TextStyle(
    fontSize: 14.sp,
    fontWeight: FontWeight.w500,
    letterSpacing: 0.1,
  );

  static TextStyle body1 = TextStyle(
    fontSize: 16.sp,
    fontWeight: FontWeight.w400,
    letterSpacing: 0.5,
    color: AppColors.white,
  );
  static TextStyle body2 = TextStyle(
    fontSize: 14.sp,
    fontWeight: FontWeight.w400,
    letterSpacing: 0.25,
    color: AppColors.white,
  );
  static TextStyle button = TextStyle(
    fontSize: 14.sp,
    fontWeight: FontWeight.w500,
    letterSpacing: 1.25,
    color: AppColors.white,
  );

  static TextStyle caption = TextStyle(
    fontSize: 12.sp,
    fontWeight: FontWeight.w400,
    letterSpacing: 0.4,
    color: AppColors.textPrimary,
  );

  static TextStyle overline = TextStyle(
    fontSize: 10.sp,
    fontWeight: FontWeight.w400,
    letterSpacing: 0.4,
    color: AppColors.textPrimary,
  );
  static TextStyle header = TextStyle(
      fontSize: AppFontSize.title.sp,
      fontWeight: FontWeight.w400,
      color: AppColors.white);

  static TextStyle header1 = TextStyle(
      fontSize: AppFontSize.subtitle,
      fontWeight: FontWeight.bold,
      color: AppColors.black);

  static TextStyle header2 = TextStyle(
      fontSize: AppFontSize.subtitle,
      fontWeight: FontWeight.normal,
      color: AppColors.black);

  static TextStyle header3 = TextStyle(
      fontSize: AppFontSize.subtitle,
      fontWeight: FontWeight.normal,
      color: AppColors.white);

  static TextStyle header4 = TextStyle(
      fontSize: AppFontSize.subtitle,
      fontWeight: FontWeight.w700,
      color: AppColors.black);

  static TextStyle label = TextStyle(
      fontSize: AppFontSize.label,
      fontWeight: FontWeight.normal,
      color: AppColors.grey);

  static TextStyle label1 = TextStyle(
      fontSize: AppFontSize.subtitle,
      fontWeight: FontWeight.normal,
      color: AppColors.grey);

  static TextStyle label2 = TextStyle(
      fontSize: AppFontSize.body,
      fontWeight: FontWeight.normal,
      color: AppColors.grey);

  static TextStyle value = TextStyle(
      fontSize: AppFontSize.body,
      fontWeight: FontWeight.normal,
      color: AppColors.black);

  static TextStyle primary = TextStyle(
      fontSize: AppFontSize.body,
      fontWeight: FontWeight.normal,
      color: AppColors.primary);

  static TextStyle primary1 = const TextStyle(
    fontSize: 14,
    fontWeight: FontWeight.bold,
    color: AppColors.primary,
  );

  static TextStyle primary2 = TextStyle(
      fontSize: AppFontSize.body,
      fontWeight: FontWeight.normal,
      color: AppColors.blue);

  static TextTheme defaultTextTheme = const TextTheme(
    bodyText1: TextStyle(
        color: AppColors.textPrimary,
        fontSize: 14,
        height: 1.6,
        fontWeight: FontWeight.w400),
    bodyText2: TextStyle(
      color: AppColors.textPrimary,
      fontSize: 14,
      height: 1.4,
      fontWeight: FontWeight.w400,
    ),
  );

  static TextStyle lightLable = TextStyle(
    fontSize: AppFontSize.text,
    fontWeight: FontWeight.w300,
    color: AppColors.grey,
    height: 1.3,
  );

  static TextStyle selectedTab = TextStyle(
      fontSize: AppFontSize.body,
      fontWeight: FontWeight.bold,
      color: AppColors.white);

  static TextStyle unSelectedTab = TextStyle(
      fontSize: AppFontSize.body,
      fontWeight: FontWeight.normal,
      color: AppColors.white.withOpacity(0.7));

  static TextStyle inviteCode = TextStyle(
      fontSize: AppFontSize.title + 10,
      fontWeight: FontWeight.bold,
      color: AppColors.black);

  static TextStyle buttonTextStyle = TextStyle(
    fontSize: AppFontSize.subtitle + 1,
    fontWeight: FontWeight.w600,
    color: AppColors.primary,
  );

  static TextStyle messageTextStyle = TextStyle(
      fontSize: AppFontSize.title + 7,
      color: AppColors.white,
      fontWeight: FontWeight.w700);

  static TextStyle headline7 = TextStyle(
    fontSize: 40.sp,
    color: AppColors.black,
    letterSpacing: -1.5,
    fontWeight: FontWeight.w300,
  );
}
