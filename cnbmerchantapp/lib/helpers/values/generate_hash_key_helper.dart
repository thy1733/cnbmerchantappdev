import 'dart:convert';
import 'package:cnbmerchantapp/core.dart';
import 'package:crypto/crypto.dart';
import 'package:flutter/material.dart';

class GenHashKeyHelper{

  static String generatePassword(String value){
    var data = _hmac(value);
    debugPrint("raw: $value gen: ${data.toString()}");
    return data.toString();
  }

  static Digest _hmac(String data) {
    var key = utf8.encode(InvokeMethodName.generateHashKey); //Secret key used for authentication
    var bytes = utf8.encode(data);
    var macSha256 = Hmac(sha256, key);
    Digest digest = macSha256.convert(bytes); //The final digest which will be used for verification
    return digest;
  }
}