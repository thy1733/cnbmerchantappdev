import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'dart:io' show Platform;

class SearchOutletScreenDelegate extends FullScreenSearchDelegate {
  final OutletListScreenController controller =
      Get.find<OutletListScreenController>();

  @override
  List<Widget>? buildActions(BuildContext context) => null;

  @override
  Widget? buildLeading(BuildContext context) => IconButton(
        splashRadius: 20,
        icon: Icon(
          Platform.isAndroid ? Icons.arrow_back : Icons.arrow_back_ios_new,
          size: 20,
          color: AppColors.grey,
        ),
        onPressed: () {
          close(context, null);
        },
      );

  @override
  Widget buildResults(BuildContext context) {
    List<OutletItemListOutputModel> searchResult = controller.outlets
        .where(
          (element) => element.outletName
              .toLowerCase()
              .startsWith(query.toLowerCase().trim()),
        )
        .toList();

    return outletListBuilder(searchResult);
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    return outletListBuilder(controller.outlets);
  }

  Widget outletListBuilder(List<OutletItemListOutputModel> items) {
    final double padding = 16.r;

    return items.isNotEmpty
        ? ListView.separated(
            padding: EdgeInsets.symmetric(horizontal: padding),
            physics: const AlwaysScrollableScrollPhysics(),
            itemBuilder: (context, index) {
              return listItem(items[index]);
            },
            separatorBuilder: (context, index) => const Divider(),
            itemCount: items.length)
        : emptyItem();
  }

  Widget emptyItem() {
    final double size = Get.width;
    double imageSize = 50.w;

    return SizedBox(
      height: size,
      width: size,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Image.asset(
            PngAppAssets.icEmptyBusiness,
            width: imageSize,
            height: imageSize,
          ),
          const SizedBox(
            height: 16,
          ),
          Text(
            "NoData".tr,
            style: AppTextStyle.label2,
          )
        ],
      ),
    );
  }

  Widget listItem(OutletItemListOutputModel item) {
    return InkWell(
      onTap: () {
        controller.showOutletDetail(item);
      },
      child: OutletListItem(item: item),
    );
  }
}
