import 'package:cnbmerchantapp/core.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'dart:io' show Platform;

class TabTransactionsScreenDelegate extends FullScreenSearchDelegate {
  final TabTransactionScreenController _controller =
      Get.find<TabTransactionScreenController>();

  List<TransactionItemListModel> searchResult = [];

  @override
  String? get searchFieldLabel => "Search".tr;

  @override
  List<Widget>? buildActions(BuildContext context) => null;

  @override
  Widget? buildLeading(BuildContext context) => IconButton(
        splashRadius: 20,
        icon: Icon(
          Platform.isAndroid ? Icons.arrow_back : Icons.arrow_back_ios_new,
          size: 20,
          color: AppColors.grey,
        ),
        onPressed: () => close(context, null),
      );

  @override
  Widget buildResults(BuildContext context) {
    return _performSearch();
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    return _performSearch();
  }

  Widget _buildTransactions(List<TransactionItemListModel> transactions) {
    return ListView.separated(
      padding: const EdgeInsets.fromLTRB(15, 15, 15, 0).r,
      addAutomaticKeepAlives: true,
      shrinkWrap: true,
      physics: const ScrollPhysics(),
      itemCount: transactions.length,
      itemBuilder: (context, index) {
        return listItem(transactions[index]);
      },
      separatorBuilder: (context, index) => const Divider(),
    );
  }

  Widget listItem(TransactionItemListModel item) {
    Color color = item.transactionType.toUpperCase() ==
            TransactionTypeEnum.refunded.getTitle.toUpperCase()
        ? AppColors.primary
        : item.status.toUpperCase() ==
                TransactionStatusEnum.completed.getTitle.toUpperCase()
            ? AppColors.success
            : AppColors.darkYellow;
    return AnimatedContainer(
        curve: Curves.easeOutCirc,
        padding: const EdgeInsets.all(0.0),
        decoration: BoxDecoration(
            color: AppColors.transparent,
            borderRadius: BorderRadius.circular(0.0),
            border: Border.all(
                color: Colors.transparent,
                width: 0.0,
                style: BorderStyle.none)),
        duration: const Duration(seconds: 3),
        child: InkWell(
          onTap: () async {
            var arguments = <String, dynamic>{
              ArgumentKey.item: item,
              ArgumentKey.fromPage: ArgumentValue.search
            };
            await _controller.navigationToNamedArgumentsAsync(
                Routes.transactionDetail,
                args: arguments);
          },
          child: Padding(
              padding: const EdgeInsets.only(
                top: 10,
                bottom: 10,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    decoration: BoxDecoration(
                        color: color.withOpacity(0.1), shape: BoxShape.circle),
                    width: 44.w,
                    height: 44.w,
                    child: Icon(
                      MerchantIcon.transactionItem,
                      color: color,
                      size: 20,
                    ),
                  ),
                  SizedBox(
                    width: 10.w,
                  ),
                  Expanded(
                      child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Row(
                        children: [
                          Expanded(
                            child: Text(
                              item.accountName.toUpperCase(),
                              textAlign: TextAlign.start,
                              style: AppTextStyle.body2.copyWith(
                                color: AppColors.textPrimary,
                                fontWeight: FontWeight.w800,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.symmetric(
                                horizontal: 8.w, vertical: 4.h),
                            decoration: BoxDecoration(
                              color: AppColors.textFieldBackground,
                              borderRadius: BorderRadius.circular(5),
                            ),
                            child: Text(
                              '${item.amount < 0 ? '' : '+'}${item.amount.toThousandSeparatorString(d2: isUSDCurrency)} ${item.currency}'
                                  .toUpperCase(),
                              textAlign: TextAlign.end,
                              style: AppTextStyle.body2.copyWith(color: color),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            kDateTimeFormat(item.transactionDate,
                                showTimeOnly: true),
                            textAlign: TextAlign.end,
                            style: AppTextStyle.caption.copyWith(
                              color: AppColors.textSecondary,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ))
                ],
              )),
        ));
  }

  Widget _performSearch() {
    searchResult.clear();
    searchResult = _controller.transactionList
        .where((e) =>
            e.accountName.toUpperCase().contains(query.toUpperCase()) ||
            e.transactionId.toUpperCase().contains(query.toUpperCase()))
        .toList();
    return _buildTransactions(searchResult);
  }
}
