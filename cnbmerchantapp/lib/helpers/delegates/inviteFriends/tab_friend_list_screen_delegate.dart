import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'dart:io' show Platform;

import 'package:cnbmerchantapp/core.dart';

class TabFriendListScreenDelegate extends FullScreenSearchDelegate {
  final InviteFriendScreenController _controller =
      Get.find<InviteFriendScreenController>();
  List<FriendModel> searchResult = [];

  @override
  String? get searchFieldLabel => "Search".tr;

  @override
  List<Widget>? buildActions(BuildContext context) => null;

  @override
  Widget? buildLeading(BuildContext context) => IconButton(
        splashRadius: 20,
        icon: Icon(
          Platform.isAndroid ? Icons.arrow_back : Icons.arrow_back_ios_new,
          size: 20,
          color: AppColors.grey,
        ),
        onPressed: () => close(context, null),
      );

  @override
  Widget buildResults(BuildContext context) {
    searchResult.clear();
    searchResult = _controller.friends
        .where((e) =>
            e.firstName.startsWith(query) || e.lastName.startsWith(query))
        .toList();

    return _buildInvitedFriendList(searchResult);
  }

  @override
  Widget buildSuggestions(BuildContext context) =>
      _buildInvitedFriendList(_controller.friends);

  Widget _buildInvitedFriendList(List<FriendModel> friends) =>
      ListView.separated(
          padding: const EdgeInsets.symmetric(horizontal: 15),
          physics: const AlwaysScrollableScrollPhysics(),
          itemBuilder: (context, index) {
            FriendModel friend = friends[index];
            return InkWell(
                // onTap: () async {
                //   await _controller
                //       .navigationToNamedArgumentsAsync(Routes.friendDetail);
                // },
                child: FriendItem(model: friend));
          },
          separatorBuilder: (context, index) => const Divider(),
          itemCount: friends.length);
}
