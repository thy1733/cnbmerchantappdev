import 'package:flutter/foundation.dart';

enum TransactionTypeStatus { all, receive, refund }

enum TransactionStatus { all, complete, failed, refunded }

extension TransactionTypeEnumExtension on TransactionTypeStatus {
  String get name => describeEnum(this);
  String get displayTitle {
    switch (this) {
      case TransactionTypeStatus.receive:
        return "Received";
      case TransactionTypeStatus.refund:
        return "Refund";
      default:
        return 'All';
    }
  }
}

enum TransactionTypeEnum { received, refunded }

extension TransactionTypeEnumExt on TransactionTypeEnum {
  String get name => describeEnum(this);
  String get getTitle {
    switch (this) {
      case TransactionTypeEnum.received:
        return "Received";
      case TransactionTypeEnum.refunded:
        return "Refunded";
      default:
        return "Received";
    }
  }
}

enum TransactionStatusEnum { completed, refunded }

extension TransactionStatusEnumExt on TransactionStatusEnum {
  String get name => describeEnum(this);
  String get getTitle {
    switch (this) {
      case TransactionStatusEnum.completed:
        return "Completed";
      case TransactionStatusEnum.refunded:
        return "Refunded";
    }
  }
}
