enum NotificationType { other, transaction, news }

enum NotificationTransactionType {
  receive,
  refund,
}

enum NotificationTransactionPaymentType {
  khqr,
  canadia,
}

enum NotificationTransactionStatusType {
  complete,
  failed,
  refunded,
}
