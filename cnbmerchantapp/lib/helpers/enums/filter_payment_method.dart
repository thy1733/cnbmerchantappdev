import 'package:flutter/foundation.dart';

enum FilterPaymentMethod {
  all,
  canadiaPay,
  khQR,
}

extension FilterPaymentMethodExtension on FilterPaymentMethod {
  String get name => describeEnum(this);
  String get getTitle {
    switch (this) {
      case FilterPaymentMethod.all:
        return 'All';
      case FilterPaymentMethod.canadiaPay:
        return TransactionPaymentMethodEnum.canamerchant.getTitle;
      case FilterPaymentMethod.khQR:
        return TransactionPaymentMethodEnum.khqr.getTitle;
      default:
        return 'All';
    }
  }
}

enum TransactionPaymentMethodEnum { canamerchant, khqr }

extension TransactionPaymentMethodEnumExt on TransactionPaymentMethodEnum {
  String get name => describeEnum(this);
  String get getTitle {
    switch (this) {
      case TransactionPaymentMethodEnum.canamerchant:
        return "CANAMERCHANT";
      case TransactionPaymentMethodEnum.khqr:
        return "KHQR";
      default:
        return "CANAMERCHANT";
    }
  }
}
