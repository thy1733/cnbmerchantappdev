import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/foundation.dart';

enum FilterTransactionType { all, receive, refund }

extension FilterTransactionTypeExtension on FilterTransactionType {
  String get name => describeEnum(this);
  String get getTitle {
    switch (this) {
      case FilterTransactionType.all:
        return "All";
      case FilterTransactionType.receive:
        return TransactionTypeEnum.received.getTitle;
      case FilterTransactionType.refund:
        return TransactionTypeEnum.refunded.getTitle;
      default:
        return "All";
    }
  }
}
