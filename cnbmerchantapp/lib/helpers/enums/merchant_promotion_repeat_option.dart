import 'package:flutter/foundation.dart';

enum MerchantPromotionRepeatOption { weekly, monthly, everyThreeMonths }

extension MerchantPromotionRepeatOptionExtension
    on MerchantPromotionRepeatOption {
  String get name => describeEnum(this);
  String get displayTitle {
    switch (this) {
      case MerchantPromotionRepeatOption.weekly:
        return "Weekly";
      case MerchantPromotionRepeatOption.monthly:
        return "Monthly";
      case MerchantPromotionRepeatOption.everyThreeMonths:
        return "Every3Months";
      default:
        return "Every3Months";
    }
  }
}
