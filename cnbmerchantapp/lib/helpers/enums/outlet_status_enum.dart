// ignore_for_file: constant_identifier_names

enum OutletStatus {
  Active("Active"),
  Deactivate("Deactivate"),
  Deleted("Deleted");

  const OutletStatus(this.value);
  final String value;
}

extension OutletStatusExtension on OutletStatus {
  OutletStatus setValue(String value) {
    switch (value.toLowerCase()) {
      case "active":
        return OutletStatus.Active;
      case "deactivate":
        return OutletStatus.Deactivate;
      case "deleted":
        return OutletStatus.Deleted;
      default:
        return OutletStatus.Active;
    }
  }

  String get value {
    switch (this) {
      case OutletStatus.Active:
        return "Active";
      case OutletStatus.Deactivate:
        return "Deactivate";
      case OutletStatus.Deleted:
        return "Deleted";
      default:
        return "Active";
    }
  }
}
