import 'package:flutter/foundation.dart';

enum UploadProfileType { businessProfile, outletProfile, cashierProfile, userProfile, attachment }

extension UploadTypeFormatExtension on UploadProfileType {
  String get name => describeEnum(this);
  String get displayTitle {
    switch (this) {
      case UploadProfileType.businessProfile:
        return 'business-profile-merchant';
      case UploadProfileType.outletProfile:
        return 'outlet-profile-merchant';
      case UploadProfileType.cashierProfile:
        return 'cashier-profile-merchant';
      case UploadProfileType.userProfile:
        return 'user-profile-merchant';
      case UploadProfileType.attachment:
        return 'attachment-merchant';
      default:
        return 'business-profile-merchant';
    }
  }
}
