import 'package:flutter/foundation.dart';

enum QuickDatePeriodOption {
  today,
  last7Days,
  last30Days,
  custom,
}

extension QuickDatePeriodOptionExtensions on QuickDatePeriodOption {
  String get name => describeEnum(this);
  String get displayTitle {
    switch (this) {
      case QuickDatePeriodOption.today:
        return "Today";
      case QuickDatePeriodOption.last7Days:
        return "last7Days";
      case QuickDatePeriodOption.last30Days:
        return "last30Days";
      case QuickDatePeriodOption.custom:
        return "Custom";
      default:
        return "Today";
    }
  }
}
