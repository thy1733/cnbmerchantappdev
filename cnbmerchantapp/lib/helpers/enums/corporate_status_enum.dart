// ignore_for_file: constant_identifier_names

enum CorporateStatus {
  None("None"),
  Requested("Requested"),
  Pending("Pending"),
  Corporate("Corporate");

  const CorporateStatus(this.value);
  final String value;
}

extension CorporateStatusExtension on CorporateStatus {
  CorporateStatus setValue(String value) {
    switch (value) {
      case "None":
        return CorporateStatus.None;
      case "Requested":
        return CorporateStatus.Requested;
      case "Pending":
        return CorporateStatus.Pending;
      case "Corporate":
        return CorporateStatus.Corporate;
      default:
        return CorporateStatus.None;
    }
  }

  String get value {
    switch (this) {
      case CorporateStatus.None:
        return "None";
      case CorporateStatus.Requested:
        return "Requested";
      case CorporateStatus.Pending:
        return "Pending";
      case CorporateStatus.Corporate:
        return "Corporate";
      default:
        return "None";
    }
  }
}
