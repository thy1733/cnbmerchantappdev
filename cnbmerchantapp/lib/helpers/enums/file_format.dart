import 'package:flutter/foundation.dart';

enum FileFormat { pdf, excel }

extension FileFormatExtension on FileFormat {
  String get name => describeEnum(this);
  String get displayTitle {
    switch (this) {
      case FileFormat.pdf:
        return 'PDF';
      case FileFormat.excel:
        return 'Excel';
      default:
        return 'Excel';
    }
  }
}
