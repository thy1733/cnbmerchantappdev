import 'package:flutter/foundation.dart';

enum DatePeriodOption {
  allPeriod,
  today,
  thisWeek,
  thisMonth,
  thisYear,
  lastWeek,
  lastMonth,
  lastYear,
  custom
}

extension DatePeriodOptionExtension on DatePeriodOption {
  String get name => describeEnum(this);
  String get displayTitle {
    switch (this) {
      case DatePeriodOption.allPeriod:
        return "AllPeriod";
      case DatePeriodOption.today:
        return "Today";
      case DatePeriodOption.thisWeek:
        return "ThisWeek";
      case DatePeriodOption.thisMonth:
        return "ThisMonth";
      case DatePeriodOption.thisYear:
        return "ThisYear";
      case DatePeriodOption.lastWeek:
        return "LastWeek";
      case DatePeriodOption.lastMonth:
        return "LastMonth";
      case DatePeriodOption.lastYear:
        return "LastYear";
      case DatePeriodOption.custom:
        return "Custom";
      default:
        return "AllPeriod";
    }
  }
}