import 'package:flutter/foundation.dart';

enum FriendStatus { active, inactive }

extension FriendStatusExtensions on FriendStatus {
  String get name => describeEnum(this);
  String get displayTitle {
    switch (this) {
      case FriendStatus.active:
        return "Active";
      case FriendStatus.inactive:
        return "Deactivated";
    }
  }
}
