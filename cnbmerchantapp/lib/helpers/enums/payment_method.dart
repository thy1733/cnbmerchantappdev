import 'package:flutter/foundation.dart';

enum PaymentMethod {
  unknown,
  canadia,
  sathapana,
  vattanac,
  cambodiaPostBank,
  acleda,
  prasac,
  wing,
  amk,
  prince,
  speedPay,
  ftb,
  chipmong,
  ppcb,
  bidc,
  phillip,
  aba,
  woori,
  trueMoney,
  chiefBank,
  lolc,
  hattha,
  eMoney,
  cambodianPublicBank,
  mayBank,
  lyhourPayPro,
  kookminBankCambodia,
  iPay88,
  shinHan,
  amret,
  coolCash,
  cabBank,
  asiaWeiLuy,
  bicBank,
  sbiLyhourBank,
  apdBank,
  mohanokor,
  hongLeongBank,
}

extension PaymentMethodExtension on PaymentMethod {
  String get name => describeEnum(this);
  String get displayTitle {
    switch (this) {
      case PaymentMethod.canadia:
        return "Canadia";
      case PaymentMethod.sathapana:
        return "Sathapana";
      case PaymentMethod.vattanac:
        return "Vatanac";
      case PaymentMethod.cambodiaPostBank:
        return "Cambodia Post Bank";
      case PaymentMethod.acleda:
        return "Acleda";
      case PaymentMethod.prasac:
        return "Prasac";
      case PaymentMethod.wing:
        return "Wing";
      case PaymentMethod.amk:
        return "AMK";
      case PaymentMethod.prince:
        return "Prince";
      case PaymentMethod.speedPay:
        return "Speed Pay";
      case PaymentMethod.ftb:
        return "FTB";
      case PaymentMethod.chipmong:
        return "Chip Mong";
      case PaymentMethod.ppcb:
        return "PPCB";
      case PaymentMethod.bidc:
        return "BIDC";
      case PaymentMethod.phillip:
        return "Phillip";
      case PaymentMethod.aba:
        return "ABA";
      case PaymentMethod.woori:
        return "Woori";
      case PaymentMethod.trueMoney:
        return "True Money";
      case PaymentMethod.chiefBank:
        return "Chief Bank";
      case PaymentMethod.lolc:
        return "LOLC";
      case PaymentMethod.hattha:
        return "Hattha";
      case PaymentMethod.eMoney:
        return "eMoney";
      case PaymentMethod.cambodianPublicBank:
        return "Cambodia Public Bank";
      case PaymentMethod.mayBank:
        return "May Bank";
      case PaymentMethod.lyhourPayPro:
        return "Ly Hour Pro";
      case PaymentMethod.kookminBankCambodia:
        return "Kookmin Bank Cambodia";
      case PaymentMethod.iPay88:
        return "iPay88";
      case PaymentMethod.shinHan:
        return "Shin Han";
      case PaymentMethod.amret:
        return "Amret";
      case PaymentMethod.coolCash:
        return "Cool Cash";
      case PaymentMethod.cabBank:
        return "Cab Bank";
      case PaymentMethod.asiaWeiLuy:
        return "Asia Wei Luy";
      case PaymentMethod.bicBank:
        return "BIC Bank";
      case PaymentMethod.sbiLyhourBank:
        return "SBI Ly Hour Bank";
      case PaymentMethod.apdBank:
        return "APD Bank";
      case PaymentMethod.mohanokor:
        return "Mohanokor";
      case PaymentMethod.hongLeongBank:
        return "Hong Leong Bank";
      default:
        return "unknown";
    }
  }
}
