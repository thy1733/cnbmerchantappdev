import 'package:flutter/foundation.dart';

enum BankAccountType {
  teen,
  fixedDeposit,
  savings,
  current,
  elite,
  junior,
  weddingSavings,
  installmentDeposit,
  online,
}

extension BankAccountTypeExtensions on BankAccountType {
  String get name => describeEnum(this);
  String get displayTitle {
    switch (this) {
      case BankAccountType.teen:
        return 'Teen Account';
      case BankAccountType.fixedDeposit:
        return 'Fixed Deposit Account';
      case BankAccountType.savings:
        return 'Savings Account';
      case BankAccountType.current:
        return 'Current Account';
      case BankAccountType.elite:
        return 'Elite Account';
      case BankAccountType.junior:
        return 'Junior Account';
      case BankAccountType.weddingSavings:
        return 'Wedding Savings Account';
      case BankAccountType.installmentDeposit:
        return 'Installment Deposit Account';
      case BankAccountType.online:
        return 'Online Account';
      default:
        return 'Savings Account';
    }
  }
}
