import 'package:flutter/foundation.dart';

enum MerchantPromotionDiscountOption { rate, amount }

extension MerchantPromotionDiscountOptionExtension
    on MerchantPromotionDiscountOption {
  String get name => describeEnum(this);
  String get displayTitle {
    switch (this) {
      case MerchantPromotionDiscountOption.rate:
        return "Rate";
      case MerchantPromotionDiscountOption.amount:
        return "FixedAmount";
      default:
        return "Rate %";
    }
  }
}
