// ignore_for_file: constant_identifier_names

enum BusinessStatus {
  Active("Active"),
  Deactivate("Deactivate"),
  Deleted("Deleted");

  const BusinessStatus(this.value);
  final String value;
}

extension BusinessStatusExtension on BusinessStatus {
  BusinessStatus setValue(String value) {
    switch (value.toLowerCase()) {
      case "active":
        return BusinessStatus.Active;
      case "deactivate":
        return BusinessStatus.Deactivate;
      case "deleted":
        return BusinessStatus.Deleted;
      default:
        return BusinessStatus.Active;
    }
  }

  String get value {
    switch (this) {
      case BusinessStatus.Active:
        return "Active";
      case BusinessStatus.Deactivate:
        return "Deactivate";
      case BusinessStatus.Deleted:
        return "Deleted";
      default:
        return "Active";
    }
  }
}
