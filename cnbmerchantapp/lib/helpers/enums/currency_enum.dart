import 'package:cnbmerchantapp/core.dart';

enum CurrencyEnum { usd, khr }

extension CurrencyEnumExtension on CurrencyEnum{
  String toQrCurrencyCode() {
    switch (this) {
      case CurrencyEnum.khr:
        return QrCurrencyCode.khr;
      default:
        return QrCurrencyCode.usd;
    }
  }
}
