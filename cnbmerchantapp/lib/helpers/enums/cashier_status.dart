// abstract class CashierStatus {
//   static const String idle = "Idle";
//   static const String pending = "Pending";
//   static const String active = "Active";
//   static const String deactivated = "Deactivated";
// }

import 'package:flutter/foundation.dart';

enum CashierStatus {
  active,
  inactive,
  pending,
  deleted,
  renew,
}

enum CashierUpdateStatus {
  updateRefundAmount,
  updateRefundStatus,
  updateCashierInfo
}

extension CashierExtensions on CashierStatus {
  String get name => describeEnum(this);
  String get displayTitle {
    switch (this) {
      case CashierStatus.active:
        return 'Active';
      case CashierStatus.pending:
        return 'Pending';
      case CashierStatus.inactive:
        return 'Inactive';
      case CashierStatus.deleted:
        return 'Deleted';
      case CashierStatus.renew:
        return 'Active';
    }
  }

  CashierStatus fromDto(int index) {
    if (index == CashierStatus.active.index) {
      return CashierStatus.active;
    } else if (index == CashierStatus.inactive.index) {
      return CashierStatus.inactive;
    } else if (index == CashierStatus.pending.index) {
      return CashierStatus.pending;
    } else if (index == CashierStatus.deleted.index) {
      return CashierStatus.deleted;
    } else {
      return CashierStatus.pending;
    }
  }
}
