import 'package:flutter/foundation.dart';

enum MerchantPromotionCoveredBy { bank, merchant, both }

extension MerchantPromotionCoveredByExtension on MerchantPromotionCoveredBy {
  String get name => describeEnum(this);
  String get displayTitle {
    switch (this) {
      case MerchantPromotionCoveredBy.bank:
        return "bank";
      case MerchantPromotionCoveredBy.merchant:
        return "merchant";
      case MerchantPromotionCoveredBy.both:
        return "both";
      default:
        return "both";
    }
  }
}
