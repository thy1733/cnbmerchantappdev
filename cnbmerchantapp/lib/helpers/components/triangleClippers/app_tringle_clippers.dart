import 'package:flutter/widgets.dart';

class AppTringleClippers {
  AppTringleClippers();
  static CustomClipper<Path> topRight = _TopRightTriangleClipper();
  static CustomClipper<Path> topLeft = _TopLeftTriangleClipper();
  static CustomClipper<Path> bottomLeft = _BottomLeftTriangleClipper();
}

class _TopRightTriangleClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    final path = Path();
    path.lineTo(size.width + 2, 0.0);
    path.lineTo(size.width + 2, size.height);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(_TopRightTriangleClipper oldClipper) => false;
}

class _TopLeftTriangleClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    final path = Path();
    path.lineTo(size.width, 0);
    path.lineTo(0, size.height);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(_TopLeftTriangleClipper oldClipper) => false;
}

class _BottomLeftTriangleClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    final path = Path();
    path.lineTo(0.0, size.height);
    path.lineTo(size.width, size.height);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(_BottomLeftTriangleClipper oldClipper) => false;
}
