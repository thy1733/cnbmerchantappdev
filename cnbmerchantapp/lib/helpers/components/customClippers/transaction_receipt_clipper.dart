import 'package:flutter/material.dart';

class TransactionReceiptClipper extends CustomClipper<Path> {
  final double radius;
  final double top;
  TransactionReceiptClipper(
      {required this.radius, required this.top,});
  @override
  Path getClip(Size size) {
    Path path = Path();

    path.lineTo(0.0, size.height);
    path.lineTo(size.width, size.height);
    path.lineTo(size.width, 0.0);

    path.addOval(
        Rect.fromCircle(center: Offset(0.0, top), radius: radius));
    path.addOval(
        Rect.fromCircle(center: Offset(size.width, top), radius: radius));
    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) => true;
}
