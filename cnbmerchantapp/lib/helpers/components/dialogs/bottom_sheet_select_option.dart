// ignore_for_file: must_be_immutable, no_leading_underscores_for_local_identifiers

import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import 'dart:io' show Platform;

class BottomSheetSelectOption extends StatelessWidget {
  final ScrollController scrollController = ScrollController();

  BottomSheetSelectOption({
    Key? key,
    required this.title,
    required this.items,
    required this.currentIndex,
    required this.onSelectedItem,
    this.useListViewSeparatedStyle = false,
    this.isDatePicker = false,
  }) : super(key: key);

  final String title;
  late double containerSize;
  bool useListViewSeparatedStyle = false;
  final DateFormat formatter = DateFormat.yMMMd(Get.locale?.languageCode);
  late List<OptionItemModel> items;
  final int? currentIndex;
  final Function(int selectedItemIndex, DatePeriodModel date) onSelectedItem;
  bool isDatePicker;

  final _selectedItemIndex = Rxn<int>();
  int? get selectedItemIndex => _selectedItemIndex.value;
  set selectedItemIndex(int? value) => _selectedItemIndex.value = value;

  final _selectedDate = DatePeriodModel().obs;
  DatePeriodModel get selectedDate => _selectedDate.value;
  set selectedDate(DatePeriodModel value) => _selectedDate.value = value;

  @override
  Widget build(BuildContext context) {
    containerSize = Get.height * 0.87;
    if (!hasSafeArea(context)) {
      containerSize += 16;
    }
    return Material(
      color: AppColors.white,
      child: Container(
        // height: containerSize,
        constraints: BoxConstraints(maxHeight: containerSize),
        child: Stack(
          alignment: AlignmentDirectional.topStart,
          children: [
            Obx(
              () => SingleChildScrollView(
                controller: scrollController,
                physics: const AlwaysScrollableScrollPhysics(),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    useListViewSeparatedStyle
                        ? ListView.separated(
                            physics: const NeverScrollableScrollPhysics(),
                            padding: const EdgeInsets.only(
                                    top: 60, left: 15, right: 15)
                                .r,
                            shrinkWrap: true,
                            itemBuilder: (BuildContext context, int index) {
                              return InkWell(
                                onTap: () {
                                  selectedItemIndex = index;
                                  if (selectedItemIndex != null) {
                                    if (isDatePicker) {
                                      updateDatePeriod();
                                    }
                                    onSelectedItem(
                                        selectedItemIndex!, selectedDate);
                                    Get.back(result: true);
                                  }
                                },
                                child: Padding(
                                  padding:
                                      const EdgeInsets.symmetric(vertical: 16),
                                  child: Row(
                                    children: [
                                      if (items[index].name.tr != "All".tr)
                                        CustomImage(
                                          "https://www.w3schools.com/howto/img_avatar.png",
                                          isNetwork: true,
                                          width: 40.w,
                                          height: 40.w,
                                          radius: 100,
                                          isShadow: true,
                                        ),
                                      Expanded(
                                        child: Padding(
                                          padding:
                                              const EdgeInsets.only(left: 16),
                                          child: Text(
                                            items[index].name.tr,
                                            style: AppTextStyle.body2.copyWith(
                                                color: AppColors.black),
                                          ),
                                        ),
                                      ),
                                      Visibility(
                                        visible: selectedItemIndex != null
                                            ? index == selectedItemIndex
                                            : false,
                                        child: const Icon(
                                          MerchantIcon.check,
                                          color: AppColors.primary,
                                          size: 14,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              );
                            },
                            separatorBuilder:
                                (BuildContext context, int index) {
                              return SizedBox(
                                child: Divider(
                                  height: 1,
                                  thickness: 1,
                                  color: AppColors.grey.withOpacity(0.2),
                                  indent: 0,
                                  endIndent: 0,
                                ),
                              );
                            },
                            itemCount: items.length)
                        : ListView.builder(
                            padding: const EdgeInsets.only(
                                    top: 60, left: 15, right: 15)
                                .r,
                            physics: const NeverScrollableScrollPhysics(),
                            shrinkWrap: true,
                            itemBuilder: (BuildContext context, int index) {
                              return Obx(() {
                                return ElevatedButton(
                                  style: ButtonStyle(
                                    backgroundColor:
                                        selectedItemIndex != null &&
                                                index == selectedItemIndex
                                            ? MaterialStateProperty.all(
                                                AppColors.listSeparator)
                                            : MaterialStateProperty.all(
                                                AppColors.transparent),
                                    shape: MaterialStateProperty.all(
                                      RoundedRectangleBorder(
                                        side: BorderSide.none,
                                        borderRadius: BorderRadius.circular(10),
                                      ),
                                    ),
                                    overlayColor: MaterialStateProperty.all(
                                        AppColors.primaryDark
                                            .withOpacity(0.05)),
                                    padding: MaterialStateProperty.all(
                                      const EdgeInsets.only(
                                        top: 10,
                                        bottom: 10,
                                        right: 15,
                                        left: 15,
                                      ),
                                    ),
                                    elevation:
                                        MaterialStateProperty.all<double>(0),
                                  ),
                                  onPressed: () {
                                    selectedItemIndex = index;
                                    if (selectedItemIndex != null) {
                                      if (isDatePicker) {
                                        if (DatePeriodOption
                                                .values[selectedItemIndex!] ==
                                            DatePeriodOption.custom) {
                                          selectedDate = DatePeriodModel();
                                          scrollController.animateTo(
                                              scrollController.position
                                                      .maxScrollExtent +
                                                  40,
                                              duration: const Duration(
                                                  milliseconds: 600),
                                              curve: Curves.ease);
                                        } else {
                                          updateDatePeriod();

                                          onSelectedItem(
                                              selectedItemIndex!, selectedDate);
                                          Get.back(result: true);
                                        }
                                      } else {
                                        onSelectedItem(
                                            selectedItemIndex!, selectedDate);
                                        Get.back(result: true);
                                      }
                                    }
                                  },
                                  child: Obx(
                                    () => Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          items[index].name.tr,
                                          style: AppTextStyle.body2.copyWith(
                                              color: AppColors.textPrimary),
                                        ),
                                        Visibility(
                                          visible: selectedItemIndex != null
                                              ? index == selectedItemIndex
                                              : false,
                                          child: const Icon(
                                            MerchantIcon.check,
                                            color: AppColors.primary,
                                            size: 14,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                );
                              });
                            },
                            itemCount: items.length,
                          ),
                    Visibility(
                      visible: selectedItemIndex != null && isDatePicker
                          ? DatePeriodOption.values[selectedItemIndex!] ==
                              DatePeriodOption.custom
                          : false,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.symmetric(vertical: 10),
                              child: Text(
                                "CustomPeriod".tr,
                                textAlign: TextAlign.start,
                                style: AppTextStyle.body1.copyWith(
                                  color: AppColors.black,
                                  fontWeight: FontWeight.w700,
                                  decoration: TextDecoration.none,
                                ),
                              ),
                            ),
                            fromDate(context),
                            toDate(context),
                            Padding(
                              padding: const EdgeInsets.only(top: 16),
                              child: AppButtonStyle.primaryButton(
                                label: "Done".tr,
                                onPressed: () {
                                  if (selectedItemIndex != null) {
                                    if (isDatePicker) {
                                      updateDatePeriod();
                                    }
                                    onSelectedItem(
                                        selectedItemIndex!, selectedDate);
                                    Get.back(result: true);
                                  }
                                },
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(
                      height: hasSafeArea(context)
                          ? getBottomSafeArea(context)
                          : 16,
                    ),
                  ],
                ),
              ),
            ),
            Container(
              color: AppColors.white,
              height: 60.r,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const SizedBox(
                    width: 60,
                    height: 60,
                  ),
                  Text(
                    title,
                    style: AppTextStyle.headline6.copyWith(
                      color: AppColors.black,
                      decoration: TextDecoration.none,
                    ),
                  ),
                  const CloseBottomSheetButton()
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget fromDate(BuildContext context) {
    return InkWell(
      onTap: () async {
        if (Platform.isIOS) {
          await showModalBottomSheet<DateTime>(
              context: context,
              builder: (_) {
                return IOSDatePicker(
                  initialDateTime: selectedDate.fromDate,
                  maximumDate: selectedDate.toDate,
                  onDonePress: (pickedDate) {
                    if (pickedDate != null) {
                      selectedDate.fromDate = pickedDate;
                    }
                  },
                );
              });
        } else {
          final pickedDate = await showDatePicker(
              context: context,
              initialDate: selectedDate.fromDate,
              builder: (_context, child) =>
                  kDatePickerThemData(_context, child),
              firstDate: DateTime(2000),
              lastDate: selectedDate.toDate);
          if (pickedDate != null) {
            selectedDate.fromDate = pickedDate;
          }
        }
      },
      child: Container(
        padding:
            const EdgeInsets.only(left: 15, top: 10, right: 10, bottom: 0).r,
        margin: EdgeInsets.only(bottom: 15.r),
        decoration: const BoxDecoration(
          color: AppColors.textFieldBackground,
          borderRadius: BorderRadius.all(Radius.circular(7)),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "FromDate".tr,
              style: AppTextStyle.caption.copyWith(
                color: Colors.black45,
                decoration: TextDecoration.none,
              ),
            ),
            Row(
              children: [
                const Icon(
                  MerchantIcon.calendar,
                  size: 15,
                  color: AppColors.black,
                ),
                const SizedBox(width: 10),
                Expanded(
                  child: Text(
                    formatter.format(
                      selectedDate.fromDate,
                    ),
                    style: AppTextStyle.body2.copyWith(
                      color: AppColors.textPrimary,
                      decoration: TextDecoration.none,
                    ),
                  ),
                ),
                const Icon(
                  Icons.arrow_drop_down_outlined,
                  size: 24,
                  color: Colors.black,
                ),
              ],
            ),
            const SizedBox(
              height: 10,
            )
          ],
        ),
      ),
    );
  }

  Widget toDate(BuildContext context) {
    return InkWell(
      onTap: () async {
        if (Platform.isIOS) {
          await showModalBottomSheet<DateTime>(
              context: context,
              builder: (_) {
                return IOSDatePicker(
                    initialDateTime: selectedDate.toDate,
                    minimumDate: selectedDate.fromDate,
                    maximumDate: DateTime.now(),
                    onDonePress: (pickedDate) {
                      if (pickedDate != null) {
                        selectedDate.toDate = pickedDate;
                      }
                    });
              });
        } else {
          final pickedDate = await showDatePicker(
              context: context,
              initialDate: selectedDate.toDate,
              builder: (_context, child) =>
                  kDatePickerThemData(_context, child),
              firstDate: selectedDate.fromDate,
              lastDate: DateTime.now());
          if (pickedDate != null) {
            selectedDate.toDate = pickedDate;
          }
        }
      },
      child: Container(
        padding: const EdgeInsets.only(left: 15, top: 10, right: 10, bottom: 0),
        decoration: const BoxDecoration(
          color: AppColors.textFieldBackground,
          borderRadius: BorderRadius.all(Radius.circular(7)),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "ToDate".tr,
              style: AppTextStyle.caption.copyWith(
                color: Colors.black45,
                decoration: TextDecoration.none,
              ),
            ),
            Row(
              children: [
                const Icon(
                  MerchantIcon.calendar,
                  size: 15,
                  color: AppColors.black,
                ),
                const SizedBox(width: 10),
                Expanded(
                    child: Text(
                  formatter.format(
                    selectedDate.toDate,
                  ),
                  style: AppTextStyle.body2.copyWith(
                    color: AppColors.textPrimary,
                    decoration: TextDecoration.none,
                  ),
                )),
                const Icon(
                  Icons.arrow_drop_down_outlined,
                  size: 24,
                  color: Colors.black,
                ),
              ],
            ),
            const SizedBox(
              height: 10,
            )
          ],
        ),
      ),
    );
  }

  void updateDatePeriod() {
    var now = DateTime.now();
    var today = DateTime(now.year, now.month, now.day);

    if (items[selectedItemIndex!].name ==
        DatePeriodOption.today.displayTitle.tr) {
      selectedDate
        ..fromDate = today
        ..toDate = now;
    } else if (items[selectedItemIndex!].name ==
        DatePeriodOption.thisWeek.displayTitle.tr) {
      selectedDate
        ..fromDate = today.subtract(Duration(days: now.weekday - 1))
        ..toDate = now;
    } else if (items[selectedItemIndex!].name ==
        DatePeriodOption.thisMonth.displayTitle.tr) {
      selectedDate
        ..fromDate = today.subtract(Duration(days: now.day - 1))
        ..toDate = now;
    } else if (items[selectedItemIndex!].name ==
        DatePeriodOption.thisYear.displayTitle.tr) {
      selectedDate
        ..fromDate = DateTime(now.year, 1, 1)
        ..toDate = now;
    } else if (items[selectedItemIndex!].name ==
        DatePeriodOption.lastWeek.displayTitle.tr) {
      selectedDate
        ..fromDate = today.subtract(Duration(days: now.weekday + 6))
        ..toDate = now.subtract(Duration(days: now.weekday));
    } else if (items[selectedItemIndex!].name ==
        DatePeriodOption.lastMonth.displayTitle.tr) {
      selectedDate
        ..fromDate = DateTime(today.year, today.month - 1, 1)
        ..toDate = today.subtract(Duration(days: now.day));
    } else if (items[selectedItemIndex!].name ==
        DatePeriodOption.lastYear.displayTitle.tr) {
      selectedDate
        ..fromDate = DateTime(now.year - 1, 1, 1)
        ..toDate = DateTime(now.year, 1, 0);
    } else if (items[selectedItemIndex!].name ==
        DatePeriodOption.allPeriod.displayTitle.tr) {
      final date = DatePeriodModel();
      date.fromDate = DateTime.tryParse(
              AppBusinessTempInstant.selectedBusiness.creationDate) ??
          DateTime.now();
      selectedDate = date;
    } else if (items[selectedItemIndex!].name ==
        QuickDatePeriodOption.last7Days.displayTitle.tr) {
      selectedDate
        ..fromDate = today.subtract(const Duration(days: 7))
        ..toDate = now;
    } else if (items[selectedItemIndex!].name ==
        QuickDatePeriodOption.last30Days.displayTitle.tr) {
      selectedDate
        ..fromDate = today.subtract(const Duration(days: 30))
        ..toDate = now;
    }
  }
}
