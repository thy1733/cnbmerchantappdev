import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

// ignore: must_be_immutable
class SelectionOptionTitleWidget extends StatelessWidget {
  SelectionOptionTitleWidget(
      {Key? key,
      required this.title,
      required this.isLeftShow,
      required this.onSelected,
      required this.items})
      : super(key: key);

  final String title;
  final bool isLeftShow;
  final SelectedCallback onSelected;
  late List<ItemSelectOptionModel> items = <ItemSelectOptionModel>[].obs;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Container(
            padding: const EdgeInsets.all(20),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                    width: Get.width,
                    padding: const EdgeInsets.all(15),
                    alignment: Alignment.center,
                    decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(30)),
                      color: AppColors.white,
                    ),
                    child: Column(
                      children: [
                        Text(
                          title,
                          style: const TextStyle(
                            color: AppColors.black,
                            fontSize: 17,
                            decorationColor: Colors.transparent,
                            decoration: TextDecoration.none,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        ListView.builder(
                            shrinkWrap: true,
                            padding: const EdgeInsets.all(0),
                            itemCount: items.length,
                            itemBuilder: (BuildContext context, int index) {
                              return GestureDetector(
                                onTap: () => {onSelected(items[index])},
                                child: Obx(() => SizedBox(
                                    height: 50,
                                    child: Container(
                                      padding: const EdgeInsets.only(
                                          left: 10,
                                          top: 5,
                                          right: 10,
                                          bottom: 5),
                                      decoration: BoxDecoration(
                                        color: AppColors.black.withOpacity(
                                            items[index].isSelected ? 0.07 : 0),
                                        borderRadius: const BorderRadius.all(
                                            Radius.circular(10)),
                                      ),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          if (isLeftShow)
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 7, right: 10),
                                              child: SvgPicture.asset(
                                                items[index].imageSvg,
                                                width: 30,
                                              ),
                                            ),
                                          Text(
                                            items[index].name,
                                            textAlign: TextAlign.start,
                                            style: const TextStyle(
                                              color: AppColors.black,
                                              decoration: TextDecoration.none,
                                              fontSize: 17,
                                              fontWeight: FontWeight.w400,
                                            ),
                                          ),
                                          Expanded(child: Container()),
                                          Icon(MerchantIcon.check,
                                              size: 15,
                                              color: items[index].isSelected
                                                  ? AppColors.primary
                                                  : Colors.transparent)
                                        ],
                                      ),
                                    ))),
                              );
                            })
                      ],
                    )),
              ],
            )));
  }
}
