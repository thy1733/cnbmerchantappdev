
import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
// import 'package:intl/intl.dart';
// import 'package:syncfusion_flutter_pdf/pdf.dart';
// import 'package:syncfusion_flutter_xlsio/xlsio.dart' as xlsio;
// import 'package:path_provider/path_provider.dart' as path_provider;
// import 'package:open_file/open_file.dart' as open_file;

class TransactionReportBottomSheet extends StatelessWidget {
  TransactionReportBottomSheet({
    super.key,
    required this.period,
    required this.datePeriod,
    required this.totalNetSale,
    required this.transactionListModel,
    this.onDownload,
  });

  // final _controller = Get.find<TabTransactionScreenController>();

  final String period;
  final DatePeriodModel datePeriod;
  final double totalNetSale;
  final List transactionListModel;

  // final PdfColor primaryColor = PdfColor(205, 32, 42);
  // final PdfColor primaryDarkColor = PdfColor(214, 18, 41);
  // final PdfColor blackColor = PdfColor(0, 0, 0);

  final _selectedFileFormat = FileFormat.pdf.obs;
  FileFormat get selectedFileFormat => _selectedFileFormat.value;
  set selectedFileFormat(FileFormat value) => _selectedFileFormat.value = value;

  final Function(FileFormat fileFormat)? onDownload;

  // final DateFormat dateFormatter = DateFormat("dd-MM-yyyy");

  // final List<String> pdfColumns = [
  //   "Date/Time",
  //   "Transaction ID",
  //   "Outlet",
  //   "Trxn. Amount",
  //   "Currency",
  //   "Customer Name",
  //   "Customer Account",
  //   "Transaction Type",
  // ];

  // final List<String> excelColumns = [
  //   "Date/Time",
  //   "Transaction ID",
  //   "Merchant ID",
  //   "Outlet",
  //   "Account Name",
  //   "Account Number",
  //   "Trxn. Amount",
  //   "Currency",
  //   "Customer Name",
  //   "Customer Bank Name",
  //   "Customer Account",
  //   "Payment Method",
  //   "Transaction Type",
  //   "Processed By",
  //   "Hash #",
  //   "Notes",
  // ];

  @override
  Widget build(BuildContext context) {
    return Material(
      color: AppColors.white,
      child: Stack(
        alignment: AlignmentDirectional.topEnd,
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 8).r,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20).r,
                  child: Row(
                    children: [
                      Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(right: 8).r,
                            child: SvgPicture.asset(
                              SvgAppAssets.downloadReport,
                              width: 40.w,
                              height: 40.w,
                              fit: BoxFit.contain,
                            ),
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                period.tr,
                                style: AppTextStyle.caption.copyWith(
                                  color: AppColors.textPrimary,
                                ),
                              ),
                              Text(
                                "Report".tr,
                                style: AppTextStyle.body1.copyWith(
                                  color: AppColors.textSecondary,
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Divider(
                  thickness: 1.h,
                  height: 40.h,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20).r,
                  child: Text(
                    "SelectFileFormat".tr,
                    style: AppTextStyle.body1.copyWith(
                      fontWeight: FontWeight.w600,
                      color: AppColors.textPrimary,
                    ),
                  ),
                ),
                SizedBox(
                  height: 20.h,
                ),
                Obx(() {
                  return InkWell(
                    onTap: () => selectedFileFormat = FileFormat.pdf,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20).r,
                      child: Row(
                        children: [
                          Text(
                            "PDF".tr,
                            style: AppTextStyle.body1.copyWith(
                              color: AppColors.textPrimary,
                            ),
                          ),
                          const Spacer(),
                          Radio<FileFormat>(
                            value: FileFormat.pdf,
                            groupValue: selectedFileFormat,
                            activeColor: AppColors.primary,
                            onChanged: (FileFormat? value) {
                              selectedFileFormat = value!;
                            },
                          ),
                        ],
                      ),
                    ),
                  );
                }),
                Divider(
                  height: 20.h,
                  thickness: 1.h,
                ),
                Obx(() {
                  return InkWell(
                    onTap: () => selectedFileFormat = FileFormat.excel,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20).r,
                      child: Row(
                        children: [
                          Text(
                            "Excel".tr,
                            style: AppTextStyle.body1.copyWith(
                              color: AppColors.textPrimary,
                            ),
                          ),
                          const Spacer(),
                          Radio<FileFormat>(
                            value: FileFormat.excel,
                            groupValue: selectedFileFormat,
                            activeColor: AppColors.primary,
                            onChanged: (FileFormat? value) {
                              selectedFileFormat = value!;
                            },
                          ),
                        ],
                      ),
                    ),
                  );
                }),
                Divider(
                  height: 20.h,
                  thickness: 1.h,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 35,
                    vertical: 20,
                  ).r,
                  child: AppButtonStyle.primaryButton(
                    onPressed: () async {
                      if (onDownload != null) {
                        onDownload!(selectedFileFormat);
                      }
                      // String fileName =
                      //     "CANAMERCHANT_REPORT_${dateFormatter.format(DateTime.now())}";
                      // selectedFileFormat == FileFormat.pdf
                      //     ? await generatePDF(fileName: fileName)
                      //     : await generateExcel(fileName: fileName);
                    },
                    label: "DownloadReport".tr,
                  ),
                ),
              ],
            ),
          ),
          const CloseBottomSheetButton(),
        ],
      ),
    );
  }

  //#region All Method Helpers

  // Future<void> saveAndLaunchFile(List<int> bytes, String fileName) async {
  //   final Directory directory =
  //       await path_provider.getApplicationSupportDirectory();
  //   String path = directory.path;
  //   final File file = File("$path/$fileName");
  //   await file.writeAsBytes(bytes, flush: true);
  //   SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark);
  //   open_file.OpenFile.open('$path/$fileName').then(
  //     (_) => SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light),
  //   );
  // }

  //#endregion
}

// //Generate Excel Report
// extension GenerateExcelReport on TransactionReportBottomSheet {
//   Future<List<int>> _readImageData(String name) async {
//     final ByteData data = await rootBundle.load(name);
//     return data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);
//   }

//   Future<void> generateExcel({required String fileName}) async {
//     int maxRow = 0;
//     int startedRow = 2;

//     final xlsio.Workbook workbook = xlsio.Workbook();
//     final xlsio.Worksheet sheet = workbook.worksheets[0];

//     final xlsio.Style globalStyle = workbook.styles.add("globalStyle");
//     globalStyle.fontSize = 12;
//     globalStyle.fontColor = "#000000";
//     globalStyle.hAlign = xlsio.HAlignType.left;

//     final xlsio.Style titleStyle = workbook.styles.add("Title");
//     titleStyle.fontSize = 16;
//     titleStyle.bold = true;
//     titleStyle.hAlign = xlsio.HAlignType.center;
//     titleStyle.vAlign = xlsio.VAlignType.center;
//     sheet.showGridlines = true;
//     sheet.getRangeByName("A1").rowHeight = 90;
//     sheet.getRangeByName("A1").text =
//         "CANAMERCHANT REPORT \nRun date: ${dateFormatter.format(DateTime.now())} \nFrom Date: ${dateFormatter.format(datePeriod.fromDate)} to ${dateFormatter.format(datePeriod.toDate)}";
//     sheet.getRangeByName("A1:P1").merge();

//     sheet.getRangeByName("A1").cellStyle = titleStyle;

//     final String canadiaBankImage =
//         base64.encode(await _readImageData(PngAppAssets.cnbLogoFull));

//     sheet.pictures.addBase64(1, 1, canadiaBankImage);

//     for (var i = 0; i < excelColumns.length; i++) {
//       String alphabet = String.fromCharCode(i + 65);
//       String headerColumn = excelColumns[i];
//       sheet.getRangeByName("$alphabet$startedRow").text = headerColumn;
//     }

//     if (transactionListModel.isNotEmpty) {
//       for (var i = 0; i <= transactionListModel.length - 1; i++) {
//         maxRow = startedRow + i + 1;
//         int dataRow = i + startedRow + 1;

//         TransactionItemListModel transaction = transactionListModel[i];
//         sheet.getRangeByName("A$dataRow").text =
//             DateFormat("MM/dd/yyyy hh:mm a")
//                 .format(transaction.transactionDate);
//         sheet.getRangeByName("B$dataRow").text = transaction.transactionId;
//         sheet.getRangeByName("C$dataRow").text = "${"Merchant ID"} $i";
//         sheet.getRangeByName("D$dataRow").text = "${"Outlet"} $i";
//         sheet.getRangeByName("E$dataRow").text = transaction.accountName;
//         sheet.getRangeByName("F$dataRow").text =
//             "0010020031234".maskAccountNumber();
//         sheet.getRangeByName("G$dataRow").number = transaction.amount;
//         sheet.getRangeByName("H$dataRow").text = transaction.currency;
//         sheet.getRangeByName("I$dataRow").text = "N/A";
//         sheet.getRangeByName("J$dataRow").text = "N/A";
//         sheet.getRangeByName("K$dataRow").text = "N/A";
//         sheet.getRangeByName("L$dataRow").text = transaction.paymentMethod;
//         sheet.getRangeByName("M$dataRow").text = transaction.transactionType;
//         sheet.getRangeByName("N$dataRow").text = "N/A";
//         sheet.getRangeByName("O$dataRow").text = "N/A";
//         sheet.getRangeByName("P$dataRow").text = "N/A";
//       }
//       sheet.getRangeByName("A$startedRow:P$maxRow").autoFitColumns();
//       sheet.getRangeByName("B$startedRow:P${maxRow + 3}").cellStyle =
//           globalStyle;
//     }

//     sheet.getRangeByName("A${maxRow + 2}").text = "Total Sale";
//     sheet.getRangeByName("A${maxRow + 3}").text = "Total Refund";

//     sheet.getRangeByName("B${maxRow + 2}").text =
//         "${transactionListModel[0].currency} $totalNetSale";
//     sheet.getRangeByName("B${maxRow + 3}").text = "USD 1.00";

//     sheet.getRangeByName("C${maxRow + 2}").number = 2;
//     sheet.getRangeByName("C${maxRow + 3}").number = 1;

//     final List<int> bytes = workbook.saveAsStream();
//     workbook.dispose();
//     saveAndLaunchFile(bytes, "$fileName.xlsx");
//   }
// }

// //Generate PDF Report
// extension GeneratePDFReport on TransactionReportBottomSheet {
//   Future<void> generatePDF({required String fileName}) async {
//     final PdfDocument document = PdfDocument();
//     PdfPageSettings pageSettings = document.pageSettings;
//     pageSettings.setMargins(12, 50, 12, 25);
//     final PdfPage page = document.pages.add();
//     Size pageSize = PdfPageSize.a4;
//     pageSettings.size = pageSize;

//     PdfLayoutResult headerResult = await drawHeader(page, pageSize);

//     drawTable(page, headerResult);

//     addFooter(document);

//     final List<int> bytes = document.saveSync();
//     document.dispose();
//     await saveAndLaunchFile(bytes, "$fileName.pdf");
//   }

//   Future<PdfLayoutResult> drawHeader(PdfPage page, Size pageSize) async {
//     final cnbImageData = await _readImageData(PngAppAssets.cnbLogoFull);
//     final cnbImage = PdfBitmap(cnbImageData);
//     const double cnbImageHeight = 50;
//     page.graphics
//         .drawImage(cnbImage, const Rect.fromLTWH(0, 0, 144, cnbImageHeight));

//     final PdfFont contentFont =
//         PdfStandardFont(PdfFontFamily.helvetica, 12, style: PdfFontStyle.bold);
//     String textHeader =
//         "CANAMERCHANT REPORT \nRun date: ${dateFormatter.format(DateTime.now())} \nFrom Date: ${dateFormatter.format(datePeriod.fromDate)} to ${dateFormatter.format(datePeriod.toDate)}";
//     final Size contentSize = contentFont.measureString(textHeader);

//     final textHeaderElement =
//         PdfTextElement(text: textHeader, font: contentFont).draw(
//             page: page,
//             bounds: Rect.fromLTWH(pageSize.width - (contentSize.width + 30), 0,
//                 contentSize.width + 30, pageSize.height - 120))!;

//     return textHeaderElement;
//   }

//   void drawTable(PdfPage page, PdfLayoutResult result) {
//     final PdfGrid grid = PdfGrid();
//     grid.columns.add(count: pdfColumns.length);
//     final PdfGridRow headerRow = grid.headers.add(1)[0];
//     // headerRow.style.backgroundBrush = PdfSolidBrush(primaryColor);
//     // headerRow.style.textBrush = PdfBrushes.white;

//     for (var i = 0; i < pdfColumns.length; i++) {
//       headerRow.cells[i].value = pdfColumns[i];
//     }
//     for (var j = 0; j < transactionListModel.length; j++) {
//       final transaction = transactionListModel[j] as TransactionItemListModel;

//       final PdfGridRow row = grid.rows.add();
//       final PdfGridCellCollection rowCells = row.cells;
//       rowCells[0].value =
//           DateFormat("MM/dd/yyyy hh:mm a").format(transaction.transactionDate);
//       rowCells[1].value = transaction.transactionId;
//       rowCells[2].value = "Sovannaphum Life Insurance branch ${j + 1}";
//       rowCells[3].value = transaction.amount.toString();
//       rowCells[4].value = transaction.currency;
//       rowCells[5].value = transaction.accountName;
//       rowCells[6].value = "0010010011234".maskAccountNumber();
//       rowCells[7].value = transaction.transactionType;
//     }
//     grid.applyBuiltInStyle(PdfGridBuiltInStyle.plainTable1);
//     final gridColumns = grid.columns;
//     gridColumns[0].width = 60;
//     gridColumns[3].width = 60;
//     gridColumns[4].width = 50;
//     final PdfGridCellCollection headerRowCells = headerRow.cells;
//     for (int i = 0; i < headerRowCells.count; i++) {
//       headerRowCells[i].style.cellPadding =
//           PdfPaddings(bottom: 5, left: 5, right: 5, top: 5);
//       headerRowCells[i].style.stringFormat?.lineAlignment =
//           PdfVerticalAlignment.middle;
//       headerRowCells[i].style.stringFormat =
//           PdfStringFormat(alignment: PdfTextAlignment.center);
//     }

//     for (int i = 0; i < grid.rows.count; i++) {
//       final PdfGridRow row = grid.rows[i];
//       final PdfGridCellCollection rowCells = row.cells;
//       for (int j = 0; j < rowCells.count; j++) {
//         final PdfGridCell cell = rowCells[j];
//         cell.stringFormat.alignment = PdfTextAlignment.center;
//         cell.stringFormat.lineAlignment = PdfVerticalAlignment.middle;
//         cell.style.cellPadding =
//             PdfPaddings(bottom: 5, left: 5, right: 5, top: 5);
//       }
//     }

//     drawTotal(page, result, grid);
//   }

//   void drawTotal(PdfPage page, PdfLayoutResult result, PdfGrid grid) {
//     Rect? totalSaleTitleCellBounds;
//     grid.beginCellLayout = (Object sender, PdfGridBeginCellLayoutArgs args) {
//       final PdfGrid grid = sender as PdfGrid;
//       if (args.cellIndex == grid.columns.count - pdfColumns.length) {
//         totalSaleTitleCellBounds = args.bounds;
//       }
//     };

//     PdfLayoutResult gridResult = grid.draw(
//         page: page, bounds: Rect.fromLTWH(0, result.bounds.height + 30, 0, 0))!;
//     final totalSaleTitleResult = PdfTextElement(
//             text: "Total Sale:",
//             font: PdfStandardFont(PdfFontFamily.helvetica, 10,
//                 style: PdfFontStyle.bold))
//         .draw(
//             page: gridResult.page,
//             bounds: Rect.fromLTWH(totalSaleTitleCellBounds!.left,
//                 gridResult.bounds.bottom + 20, 0, 0))!;

//     final totalSaleResult = PdfTextElement(
//             text: "${transactionListModel[0].currency} $totalNetSale",
//             font: PdfStandardFont(PdfFontFamily.helvetica, 9,
//                 style: PdfFontStyle.regular))
//         .draw(
//             page: gridResult.page,
//             bounds: Rect.fromLTWH(totalSaleTitleResult.bounds.right + 20,
//                 totalSaleTitleResult.bounds.top, 0, 0))!;

//     PdfTextElement(
//             text: "2",
//             font: PdfStandardFont(PdfFontFamily.helvetica, 9,
//                 style: PdfFontStyle.regular))
//         .draw(
//             page: gridResult.page,
//             bounds: Rect.fromLTWH(totalSaleResult.bounds.right + 20,
//                 totalSaleResult.bounds.top, 0, 0))!;

//     final totalRefundTitleResult = PdfTextElement(
//             text: "Total Refund:",
//             font: PdfStandardFont(PdfFontFamily.helvetica, 10,
//                 style: PdfFontStyle.bold))
//         .draw(
//             page: gridResult.page,
//             bounds: Rect.fromLTWH(totalSaleTitleResult.bounds.left,
//                 totalSaleTitleResult.bounds.bottom + 10, 0, 0))!;

//     final totalRefundResult = PdfTextElement(
//             text: "USD 1.00",
//             font: PdfStandardFont(PdfFontFamily.helvetica, 9,
//                 style: PdfFontStyle.regular))
//         .draw(
//             page: gridResult.page,
//             bounds: Rect.fromLTWH(totalRefundTitleResult.bounds.right + 20,
//                 totalRefundTitleResult.bounds.top, 0, 0))!;

//     PdfTextElement(
//             text: "1",
//             font: PdfStandardFont(PdfFontFamily.helvetica, 9,
//                 style: PdfFontStyle.regular))
//         .draw(
//             page: gridResult.page,
//             bounds: Rect.fromLTWH(totalRefundResult.bounds.right + 20,
//                 totalRefundResult.bounds.top, 0, 0))!;
//   }

//   void addFooter(PdfDocument document) {
//     //Create the footer with specific bounds
//     PdfPageTemplateElement footerTemplate = PdfPageTemplateElement(
//         Rect.fromLTWH(0, 0, document.pageSettings.size.width, 50));

//     //Create the page number field
//     PdfPageNumberField pageNumber = PdfPageNumberField(
//         font: PdfStandardFont(PdfFontFamily.helvetica, 8),
//         brush: PdfSolidBrush(blackColor));

//     pageNumber.numberStyle = PdfNumberStyle.numeric;

//     PdfPageCountField count = PdfPageCountField(
//         font: PdfStandardFont(PdfFontFamily.helvetica, 8),
//         brush: PdfSolidBrush(blackColor));

//     count.numberStyle = PdfNumberStyle.numeric;

//     PdfCompositeField compositeField = PdfCompositeField(
//         font: PdfStandardFont(PdfFontFamily.helvetica, 8),
//         brush: PdfSolidBrush(blackColor),
//         text:
//             "Canadia Bank | +855 23 868 222 | www.canadiabank.com.kh - Page: {0} / {1}",
//         fields: <PdfAutomaticField>[
//           pageNumber,
//           count,
//         ]);
//     compositeField.bounds = footerTemplate.bounds;

//     final compositeFieldYAxis =
//         50 - PdfStandardFont(PdfFontFamily.helvetica, 8).height;

//     compositeField.draw(
//         footerTemplate.graphics, Offset(0, compositeFieldYAxis));

//     document.template.bottom = footerTemplate;
//   }

//   Future<PdfFont> getFont(TextStyle style) async {
//     //Get the external storage directory
//     Directory directory = await path_provider.getApplicationSupportDirectory();
//     //Create an empty file to write the font data
//     File file = File('${directory.path}/${style.fontFamily}.ttf');
//     if (!file.existsSync()) {
//       List<FileSystemEntity> entityList = directory.listSync();
//       for (FileSystemEntity entity in entityList) {
//         if (entity.path.contains(style.fontFamily!)) {
//           file = File(entity.path);
//           break;
//         }
//       }
//     }
//     List<int>? fontBytes;

//     if (file.existsSync()) {
//       fontBytes = await file.readAsBytes();
//     }
//     if (fontBytes != null && fontBytes.isNotEmpty) {
//       return PdfTrueTypeFont(fontBytes, 12);
//     } else {
//       return PdfStandardFont(PdfFontFamily.helvetica, 12);
//     }
//   }

//   /*
//   Future<void> drawFooter(PdfPage page, Size pageSize) async {
//     final PdfPen linePen = PdfPen(blackColor, dashStyle: PdfDashStyle.custom);
//     linePen.dashPattern = <double>[3, 3];
//     page.graphics.drawLine(linePen, Offset(0, pageSize.height - 150),
//         Offset(pageSize.width, pageSize.height - 150));

//     const String footerContent =
//         "Canadia Bank | +855 23 868 222 | www.canadiabank.com.kh";
//     PdfFont font =
//         PdfStandardFont(PdfFontFamily.helvetica, 12, style: PdfFontStyle.bold);
//     page.graphics.drawString(
//       footerContent,
//       font,
//       format: PdfStringFormat(alignment: PdfTextAlignment.right),
//       bounds: Rect.fromLTWH(pageSize.width - 30, pageSize.height - 130, 0, 0),
//     );
//   }
//   */
// }
