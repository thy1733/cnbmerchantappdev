import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class SelectionOptionNoTitleWidget extends StatelessWidget {
  const SelectionOptionNoTitleWidget(
      {Key? key,
      required this.isLeftShow,
      required this.onSelected,
      required this.items})
      : super(key: key);
  final bool isLeftShow;
  final SelectedCallback onSelected;
  final List<ItemSelectOptionModel> items;

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        shrinkWrap: true,
        padding: const EdgeInsets.fromLTRB(15, 0, 15, 15),
        itemCount: items.length,
        itemBuilder: (BuildContext context, int index) {
          return GestureDetector(
            onTap: () => {onSelected(items[index])},
            child: SizedBox(
                height: 50,
                child: Container(
                  padding: const EdgeInsets.only(
                      left: 10, top: 5, right: 10, bottom: 5),
                  decoration: BoxDecoration(
                    color: AppColors.black
                        .withOpacity(items[index].isSelected ? 0.07 : 0),
                    borderRadius: const BorderRadius.all(Radius.circular(10)),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      if (isLeftShow)
                        Padding(
                          padding: const EdgeInsets.only(left: 7, right: 10),
                          child: SizedBox(
                            width: 20,
                            height: 20,
                            child: ClipOval(
                              child: SvgPicture.asset(
                                items[index].imageSvg,
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                        ),
                      Text(
                        items[index].name,
                        textAlign: TextAlign.start,
                        style: const TextStyle(
                          color: AppColors.black,
                          decoration: TextDecoration.none,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                      Expanded(child: Container()),
                      Icon(MerchantIcon.check,
                          size: 15,
                          color: items[index].isSelected
                              ? AppColors.primary
                              : Colors.transparent)
                    ],
                  ),
                )),
          );
        });
  }
}
