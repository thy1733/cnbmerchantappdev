
// ignore_for_file: must_be_immutable

import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';

class BottomSheetCheckOption extends StatelessWidget {
  final ScrollController scrollController = ScrollController();

  BottomSheetCheckOption({
    Key? key,
    required this.title,
    required this.items,
    required this.onSelectedItem,
  }) : super(key: key);

  final String title;
  late double containerSize;
  late List<ItemSelectOptionModel> items;
  final Function(List<ItemSelectOptionModel> selectedItems) onSelectedItem;

  final _isSelectAll = false.obs;
  bool get isSelectAll => _isSelectAll.value;
  set isSelectAll(bool value) => _isSelectAll.value = value;

  @override
  Widget build(BuildContext context) {
    final double padding = 16.r;
    isSelectAll =
        items.where((element) => element.isSelected).toList().length ==
            items.length;

    containerSize = Get.height * 0.86;
    if (!hasSafeArea(context)) {
      containerSize += 16;
    }
    return Material(
      color: AppColors.white,
      child: Container(
        constraints: BoxConstraints(maxHeight: containerSize),
        child: Stack(
          alignment: AlignmentDirectional.topStart,
          children: [
            SingleChildScrollView(
              controller: scrollController,
              physics: const AlwaysScrollableScrollPhysics(),
              child: Padding(
                padding: EdgeInsets.all(padding),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    SizedBox(
                      height: 50.r,
                    ),
                    Obx(
                      () => Align(
                        alignment: Alignment.centerRight,
                        child: TextButton(
                            onPressed: () {
                              isSelectAll = !isSelectAll;

                              items
                                  .map((e) => e.isSelected = isSelectAll)
                                  .toList();

                              onSelectedItem(items
                                  .where((element) => element.isSelected)
                                  .toList());
                            },
                            style: AppButtonStyle.kSecondaryButtonExpandStyle(
                                width: (Get.width / 4.5).r),
                            child: Text(
                              isSelectAll ? "UnSelectAll".tr : "SelectAll".tr,
                              style: AppTextStyle.primary,
                            )),
                      ),
                    ),
                    ListView.separated(
                        shrinkWrap: true,
                        physics: const NeverScrollableScrollPhysics(),
                        itemBuilder: (context, index) {
                          var item = items[index];
                          return Obx(
                            () => Row(
                              children: [
                                Expanded(child: Text(item.name)),
                                Checkbox(
                                    value: item.isSelected,
                                    activeColor: AppColors.primary,
                                    onChanged: (value) {
                                      item.isSelected = value!;

                                      isSelectAll = items
                                              .where((element) =>
                                                  element.isSelected)
                                              .toList()
                                              .length ==
                                          items.length;

                                      onSelectedItem(items
                                          .where(
                                              (element) => element.isSelected)
                                          .toList());
                                    })
                              ],
                            ),
                          );
                        },
                        separatorBuilder: (context, index) => const Divider(),
                        itemCount: items.length),
                    ButtonCircleRadius(
                      title: "LinkedOutlet".tr,
                      submit: (value) {
                        var selectedItems = items
                            .where((element) => element.isSelected)
                            .toList();

                        if (selectedItems.isNotEmpty) {
                          onSelectedItem(selectedItems);
                          Get.back();
                        } else {
                          Fluttertoast.showToast(
                              msg: "PleaseSelectOutletsMessage".tr);
                        }
                      },
                    ),
                    SizedBox(
                      height: hasSafeArea(context)
                          ? getBottomSafeArea(context)
                          : 16,
                    ),
                  ],
                ),
              ),
            ),
            Container(
              color: AppColors.white,
              height: 60.r,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const SizedBox(
                    width: 60,
                    height: 60,
                  ),
                  Text(
                    title,
                    style: AppTextStyle.headline6.copyWith(
                      color: AppColors.black,
                      decoration: TextDecoration.none,
                    ),
                  ),
                  const SizedBox(
                    width: 60,
                    height: 60,
                    child: Material(
                      color: AppColors.white,
                      child: CloseButton(
                        color: Colors.grey,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
