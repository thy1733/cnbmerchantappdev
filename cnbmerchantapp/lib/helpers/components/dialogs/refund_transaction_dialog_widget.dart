import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

typedef OnConfirmRefundAmount = void Function(double amount, String remark);

class RefundTransactionDialogWidget extends StatefulWidget {
  const RefundTransactionDialogWidget({
    super.key,
    required this.initialAmount,
    required this.currency,
    required this.maximumAmount,
    required this.onConfirm,
    required this.isDecimal,
  });

  @override
  State<RefundTransactionDialogWidget> createState() =>
      _RefundTransactionDialogWidgetState();

  final OnConfirmRefundAmount onConfirm;
  final double maximumAmount;
  final CurrencyEnum currency;
  final double initialAmount;
  final bool isDecimal;
}

class _RefundTransactionDialogWidgetState
    extends State<RefundTransactionDialogWidget> {
//#region All Override method

  @override
  void initState() {
    amount = widget.maximumAmount;
    amountString = amount.toThousandSeparatorString();
    debugPrint("Amount: $amount");
    debugPrint("Amount String: $amountString");
    currency = widget.currency;
    super.initState();
    _focusNode.addListener(() {
      isRemarkFieldFocused = _focusNode.hasFocus;
    });
  }

  @override
  void dispose() {
    _focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double bottomMargin =
        hasSafeArea(context) ? getBottomSafeArea(context) : 15;
    return Obx(
      () => InkWell(
        onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
        child: Container(
          padding: const EdgeInsets.all(5),
          decoration: const BoxDecoration(
            borderRadius: BorderRadius.vertical(top: Radius.circular(15)),
            color: AppColors.white,
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const SizedBox(width: 60),
                  Text(
                    "EnterAmount".tr,
                    style:
                        AppTextStyle.headline6.copyWith(color: AppColors.black),
                  ),
                  const CloseBottomSheetButton()
                ],
              ),
              const SizedBox(height: 40),
              Center(
                  heightFactor: 0.2,
                  widthFactor: 1,
                  child: Padding(
                    padding: const EdgeInsets.only(
                        left: 15, right: 5, top: 0, bottom: 15),
                    child: Text.rich(
                      TextSpan(
                        style: const TextStyle(
                            color: AppColors.textPrimary, fontSize: 11),
                        children: <TextSpan>[
                          TextSpan(text: "MaxAmount".tr),
                          TextSpan(
                              text:
                                  ' (${widget.maximumAmount.toThousandSeparatorString(d2: isUSDCurrency)} ${currency.name}).'
                                      .toUpperCase(),
                              style:
                                  const TextStyle(fontWeight: FontWeight.bold)),
                        ],
                      ),
                    ),
                  )),
              Text(
                "${CurrencyConverter.currencyToSymble(currency)}${thousandSeparatQR(amountString)}",
                style:
                    const TextStyle(fontSize: 40, color: AppColors.textPrimary),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 15, right: 15, top: 15),
                child: _buildRemarkTextField(),
              ),
              SizedBox(
                  height: 300.h - bottomMargin,
                  child: AnimatedOpacity(
                    duration: const Duration(milliseconds: 200),
                    opacity: isRemarkFieldFocused ? 0.0 : 1.0,
                    child: KNumpadWidget(
                      isDecimal: widget.isDecimal,
                      onPressed: isRemarkFieldFocused
                          ? (_) => FocusManager.instance.primaryFocus?.unfocus()
                          : userTapOnKeyboardAsync,
                      onBackSpacePressed: isRemarkFieldFocused
                          ? () => FocusManager.instance.primaryFocus?.unfocus()
                          : userTabOnBackSpaceAsync,
                    ),
                  )),
              AppButtonStyle.primaryButton(
                label: "Confirm".tr,
                isEnable: amountString != "0",
                margin: EdgeInsets.fromLTRB(15, 10, 15, bottomMargin),
                onPressed: () {
                  widget.onConfirm(amount, remark);
                  Get.back();
                },
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildRemarkTextField() {
    int maxLength = 250;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        TextField(
          focusNode: _focusNode,
          inputFormatters: [
            CustomLengthLimitingTextInputFormatter(maxLength),
          ],
          decoration: InputDecoration(
            filled: !isRemarkFieldFocused,
            fillColor: isRemarkFieldFocused
                ? AppColors.transparent
                : AppColors.textFieldBackground,
            labelText: "${"Remark".tr} ${"optional".tr}",
            labelStyle: const TextStyle(color: AppColors.textSecondary),
            prefixIcon: const Icon(
              MerchantIcon.remark,
              size: 15,
              color: AppColors.black,
            ),
            focusedBorder: const OutlineInputBorder(
                borderRadius: BorderRadius.all(
                  Radius.circular(7),
                ),
                borderSide: BorderSide(color: Colors.blue, width: 1)),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(7),
              borderSide: const BorderSide(color: AppColors.grey, width: 0.5),
            ),
          ),
          keyboardType: TextInputType.multiline,
          minLines: 1,
          maxLines: 3,
          onChanged: (value) {
            remark = value;
          },
        ),
        Padding(
          padding: const EdgeInsets.only(right: 4),
          child: Text(
            "${remark.length}/$maxLength",
            style:
                AppTextStyle.caption.copyWith(color: AppColors.textSecondary),
          ),
        )
      ],
    );
  }

  //#endregion

//#region All Properties

  final _focusNode = FocusNode();

  final _amount = 0.0.obs;
  double get amount => _amount.value;
  set amount(double value) => _amount.value = value;

  final _currency = CurrencyEnum.usd.obs;
  CurrencyEnum get currency => _currency.value;
  set currency(CurrencyEnum value) => _currency.value = value;

  final _amountString = '0'.obs;
  String get amountString => _amountString.value;
  set amountString(String value) => _amountString.value = value;

  final _isMaximumAmount = false.obs;
  bool get isMaximumAmount => _isMaximumAmount.value;
  set isMaximumAmount(bool value) => _isMaximumAmount.value = value;

  final _remark = ''.obs;
  String get remark => _remark.value;
  set remark(String value) => _remark.value = value;

  final _isRemarkFieldFocused = false.obs;
  bool get isRemarkFieldFocused => _isRemarkFieldFocused.value;
  set isRemarkFieldFocused(bool value) => _isRemarkFieldFocused.value = value;

  //#endregion
  Future<void> userTapOnKeyboardAsync(String value) async {
    if (value == ValueConst.point) {
      if (amountString.contains(ValueConst.point)) return;
      amountString += value;
    } else {
      // Check max length user input
      if (amountString.length >= 11) return;

      if (amountString.length == 1 && amountString == '0') {
        amountString = value;
      } else {
        if (checkDecimalValue(amountString) == 3) return;
        if (!isMaximumAmount) {
          amountString += value;
        }
      }
    }
    double inputAmount =
        double.tryParse(amountString.toSafeDoubleAsString()) ?? 0;

    amount = inputAmount;

    if (inputAmount > widget.maximumAmount) {
      amountString = widget.maximumAmount.toString();
      amount = double.tryParse(amountString.toSafeDoubleAsString()) ?? 0;
    }
  }

  int checkDecimalValue(String text) {
    if (text.contains(ValueConst.point)) {
      int startIndex = text.indexOf('.');
      var value = text.substring(startIndex);
      return value.length;
    }
    return 0;
  }

  Future<void> userTabOnBackSpaceAsync() async {
    if (amountString.isNotEmpty) {
      String result = amountString.substring(0, amountString.length - 1);
      amountString = result.isEmpty ? '0' : result;
    }
    amountString = amountString.isEmpty ? '0' : amountString;
    amount = double.parse(amountString.toSafeDoubleAsString());
  }
}
