import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

typedef OnSelectedCBottomSheet<T> = void Function(int index, T value);
typedef CBottomSheetChild<T> = Widget Function(int index, T value);

class AppBottomSheet {
  static Future<void> bottomSheetSelectDynamicOption<T>(
      {String title = "Title",
      required List<T> items,
      int? selectedItemIndex,
      T? selectedItem,
      required OnSelectedCBottomSheet<T> onSelectedItem,
      required CBottomSheetChild<T> itemBuilder,
      Widget? separator,
      bool closeOnSelect = false,
      Decoration? selectedItemDecoration,
      Color? barrierBackground,
      Color iconColor = AppColors.primary}) async {
    await showMaterialModalBottomSheet(
        context: Get.context!,
        backgroundColor: AppColors.transparent,
        barrierColor: barrierBackground ?? AppColors.barrierBackground,
        builder: (_) {
          return _CBottomSheetSelectDynamicOption<T>(
            title: title,
            items: items,
            selectedItemIndex:selectedItemIndex,
            builder: itemBuilder,
            selectedItem: selectedItem,
            onSelectedItem: onSelectedItem,
            separatorBuilder: separator,
            closeOnSelect: closeOnSelect,
            selectedItemDecoration: selectedItemDecoration,
            iconColor: iconColor,
          );
        });
  }

  static Future<void> bottomSheetBlankContent(Widget child,
      {Color? barrierBackground}) async {
    await showCupertinoModalBottomSheet(
        barrierColor: barrierBackground ?? AppColors.barrierBackground,
        topRadius: const Radius.circular(23),
        context: Get.context!,
        builder: (context) {
          return BottomSheetBusinessDetail(
            child: child,
          );
        });
  }
}

class _CBottomSheetSelectDynamicOption<T> extends StatefulWidget {
  const _CBottomSheetSelectDynamicOption({
    Key? key,
    this.selectedItemIndex,
    this.selectedItem,
    required this.title,
    required this.items,
    required this.onSelectedItem,
    required this.builder,
    this.separatorBuilder,
    this.closeOnSelect = false,
    this.selectedItemDecoration,
    this.iconColor,
  }) : super(key: key);

  final String title;
  final T? selectedItem;
  final int? selectedItemIndex;
  final List<T> items;
  final OnSelectedCBottomSheet<T> onSelectedItem;
  final CBottomSheetChild<T> builder;
  final Widget? separatorBuilder;
  final bool closeOnSelect;
  final Decoration? selectedItemDecoration;
  final Color? iconColor;

  @override
  State<_CBottomSheetSelectDynamicOption<T>> createState() =>
      _CBottomSheetSelectDynamicOptionState<T>();
}

class _CBottomSheetSelectDynamicOptionState<T>
    extends State<_CBottomSheetSelectDynamicOption<T>> {
  final _seletedItemIndex = 0.obs;
  int get seletedItemIndex => _seletedItemIndex.value;
  set seletedItemIndex(int value) => _seletedItemIndex.value = value;

  @override
  void initState() {
    super.initState();
    if (widget.selectedItem != null && widget.items.isNotEmpty) {
      var selectedItem = widget.selectedItem as T;
      seletedItemIndex = widget.items.indexOf(selectedItem);
    }else if(widget.selectedItemIndex!=null){
      seletedItemIndex = widget.selectedItemIndex??-1;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Ink(
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.vertical(top: Radius.circular(15)),
        color: AppColors.white,
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Padding(
            padding: const EdgeInsets.all(5.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const SizedBox(width: 60),
                Text(
                  widget.title,
                  style:
                      AppTextStyle.headline6.copyWith(color: AppColors.black),
                ),
                const CloseButton(color: Colors.grey),
              ],
            ),
          ),
          Container(
            color: AppColors.white,
            constraints: BoxConstraints(maxHeight: Get.height - 120),
            child: SafeArea(
              top: false,
              child: ListView.separated(
                shrinkWrap: true,
                physics: const BouncingScrollPhysics(),
                padding: const EdgeInsets.fromLTRB(15, 0, 15, 15),
                itemCount: widget.items.length,
                separatorBuilder: (context, index) =>
                    widget.separatorBuilder ?? const SizedBox(height: 10),
                itemBuilder: (context, index) => InkWell(
                  splashColor: AppColors.primary.withOpacity(.9),
                  highlightColor: AppColors.primary.withOpacity(.4),
                  overlayColor: MaterialStateProperty.all(
                      AppColors.primary.withOpacity(.9)),
                  onTap: () {
                    if (widget.closeOnSelect) Get.back();
                    seletedItemIndex = index;
                    widget.onSelectedItem(index, widget.items.elementAt(index));
                  },
                  child: Obx(
                    () => Container(
                      padding: const EdgeInsets.symmetric(horizontal: 15),
                      decoration: seletedItemIndex == index &&
                              widget.selectedItem != null
                          ? widget.selectedItemDecoration
                          : null,
                      child: Row(
                        children: [
                          Expanded(
                            child: widget.builder(
                              index,
                              widget.items.elementAt(index),
                            ),
                          ),
                          Visibility(
                            visible: seletedItemIndex == index,
                            child: Icon(
                              MerchantIcon.check,
                              color: widget.iconColor ?? AppColors.primary,
                              size: 18,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
