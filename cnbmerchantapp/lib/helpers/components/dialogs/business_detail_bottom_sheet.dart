import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class BottomSheetBusinessDetail extends StatelessWidget {
  final ScrollController scrollController = ScrollController();

  BottomSheetBusinessDetail({super.key, required this.child});

  final Widget child;

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Container(
        padding: MediaQuery.of(Get.context!).viewInsets,
        constraints: BoxConstraints(maxHeight: Get.height * 0.86),
        color: AppColors.white,
        child: Stack(
          children: [
            SingleChildScrollView(
              controller: scrollController,
              physics: const BouncingScrollPhysics(),
              child: child,
            ),
            const Positioned(
              top: 0,
              right: 0,
              child: SizedBox(
                width: 60,
                height: 60,
                child: Material(
                  color: AppColors.transparent,
                  child: CloseButton(
                    color: AppColors.grey,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
