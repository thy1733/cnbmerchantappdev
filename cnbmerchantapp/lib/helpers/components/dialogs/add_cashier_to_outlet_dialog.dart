import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

// ignore: must_be_immutable
class UpdateCashierInOutletDialog extends StatelessWidget {
  UpdateCashierInOutletDialog(
      {Key? key,
      required this.title,
      this.isShowItem = true,
      required this.onSelected,
      required this.items})
      : super(key: key);

  final String title;
  final bool isShowItem;
  final SelectedCallback onSelected;
  late List<ItemSelectOptionModel> items = <ItemSelectOptionModel>[].obs;

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: Material(
        color: Colors.transparent,
        child: Container(
            width: Get.width,
            padding: const EdgeInsets.all(15),
            margin: const EdgeInsets.all(15),
            decoration: const BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(30)),
              color: AppColors.white,
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  title,
                  style: const TextStyle(
                    color: AppColors.black,
                    fontSize: 17,
                    decorationColor: Colors.transparent,
                    decoration: TextDecoration.none,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                Container(
                  constraints: BoxConstraints(maxHeight: Get.height * 0.7),
                  child: ListView.builder(
                      shrinkWrap: true,
                      padding: const EdgeInsets.all(0),
                      itemCount: items.length,
                      itemBuilder: (BuildContext context, int index) {
                        return GestureDetector(
                          onTap: () => {onSelected(items[index])},
                          child: Obx(() => Visibility(
                                visible: isShowItem,
                                child: SizedBox(
                                    child: Container(
                                  padding: const EdgeInsets.only(
                                      left: 10, top: 5, right: 10, bottom: 5),
                                  decoration: BoxDecoration(
                                    color: AppColors.black.withOpacity(
                                        items[index].isSelected ? 0.07 : 0),
                                    borderRadius: const BorderRadius.all(
                                        Radius.circular(10)),
                                  ),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Card(
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(50)),
                                        child: AppNetworkImage(
                                            width: 44.w,
                                            height: 44.w,
                                            shape: BoxShape.circle,
                                            errorWidget: SizedBox(
                                              width: 44.w,
                                              height: 44.w,
                                              child: Icon(MerchantIcon.user,
                                                  size: 25.r,
                                                  color: AppColors.primary),
                                            ),
                                            items[index].photoUrl),
                                      ),
                                      const SizedBox(
                                        width: 8,
                                      ),
                                      Expanded(
                                        flex: 1,
                                        child: Text(
                                          items[index].name,
                                          textAlign: TextAlign.start,
                                          overflow: TextOverflow.ellipsis,
                                          style: const TextStyle(
                                              color: AppColors.black,
                                              decoration: TextDecoration.none,
                                              fontSize: 17,
                                              fontFamily:
                                                  AppFontName.familyFont,
                                              fontWeight: FontWeight.w400),
                                        ),
                                      ),
                                      Icon(MerchantIcon.check,
                                          size: 15,
                                          color: items[index].isSelected
                                              ? AppColors.primary
                                              : Colors.transparent)
                                    ],
                                  ),
                                )),
                              )),
                        );
                      }),
                )
              ],
            )),
      ),
    );
  }
}
