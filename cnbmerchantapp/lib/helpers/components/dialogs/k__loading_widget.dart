import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';

class KLoading extends StatefulWidget {
  final KLoadingStyle loadingStyle;
  final Color backgroundColor;
  final BuildContext? buildContext;
  const KLoading(
      {Key? key,
      this.loadingStyle = KLoadingStyle.circleProgress,
      this.backgroundColor = Colors.white,
      this.buildContext})
      : super(key: key);

  @override
  State<KLoading> createState() => _KLoadingState();
}

class _KLoadingState extends State<KLoading> {
  double width = double.infinity;
  double height = double.infinity;
  Widget loadingWidget = Center(
    child: CircularProgressIndicator(
        color: AppColors.primary,
        backgroundColor: AppColors.grey.withOpacity(0.3)),
  );


  @override
  void initState() {
    if (widget.buildContext != null) {
      height = MediaQuery.of(widget.buildContext!).size.height;
      width = MediaQuery.of(widget.buildContext!).size.width;
    }
    if (widget.loadingStyle != KLoadingStyle.circleProgress) {
      loadingWidget =Container();
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: widget.backgroundColor,
      child: loadingWidget,
    );
  }
}

enum KLoadingStyle {
  circleProgress,
  skeletonList,
  skeletonAvatarCircle,
  skeletonAvatarRec,
  sHomeScreen,
  sProductDetail
}
