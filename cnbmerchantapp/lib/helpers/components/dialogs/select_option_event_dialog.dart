import 'package:cnbmerchantapp/core.dart';

typedef SelectedCallback = Function(ItemSelectOptionModel item);
typedef OnChangedCallback = Function(String text);
typedef SubmitCallback = Function(dynamic value);
typedef OnEditingCompleteCallback = Function();
typedef OnBackButton = Function();
typedef OnClearCallback = Function(String text);