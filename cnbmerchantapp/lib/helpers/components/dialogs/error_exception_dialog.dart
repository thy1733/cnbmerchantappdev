import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

class AppErrorExceptionDialog {
  static void show({
    required String titile,
    required String message,
    required String imageUrl,
    String? okTitleButton,
    bool dismissable = false,
    VoidCallback? onOkButtonClicked,
  }) {
    showDialog(
        barrierColor: AppColors.barrierBackground,
        context: Get.context!,
        barrierDismissible: dismissable,
        builder: (context) => ErrorExceptionDialogBuilder(
              title: titile,
              message: message,
              imageUrl: imageUrl,
              okTitleButton: okTitleButton,
              okButtonClicked: onOkButtonClicked,
            ));
  }

  static void showOkCancel({
    required String titile,
    required String message,
    required String imageUrl,
    String? okTitleButton,
    String? cancelTitleButton,
    bool dismissable = false,
    VoidCallback? onOkButtonClicked,
    VoidCallback? onCancelButtonClicked,
  }) {
    showDialog(
        barrierColor: AppColors.barrierBackground,
        context: Get.context!,
        barrierDismissible: dismissable,
        builder: (context) => ErrorExceptionDialogBuilder(
              title: titile,
              message: message,
              imageUrl: imageUrl,
              isOkCancel: true,
              okTitleButton: okTitleButton,
              cancelTitleButton: cancelTitleButton,
              okButtonClicked: onOkButtonClicked,
              cancelButtonClicked: onCancelButtonClicked,
            ));
  }
}

class ErrorExceptionDialogBuilder extends StatelessWidget {
  const ErrorExceptionDialogBuilder({
    super.key,
    required this.title,
    required this.message,
    required this.imageUrl,
    this.isOkCancel = false,
    this.okTitleButton,
    this.cancelTitleButton,
    this.okButtonClicked,
    this.cancelButtonClicked,
  });

  final String title;
  final String message;
  final String imageUrl;
  final bool isOkCancel;

  final String? okTitleButton;
  final String? cancelTitleButton;

  final VoidCallback? okButtonClicked;
  final VoidCallback? cancelButtonClicked;

  @override
  Widget build(BuildContext context) {
    final double padding = 16.r;
    final double radius = 16.r;

    return Dialog(
      shape:
          RoundedRectangleBorder(borderRadius: BorderRadius.circular(padding)),
      backgroundColor: AppColors.white,
      insetPadding: EdgeInsets.symmetric(vertical: 100.r, horizontal: 40.r),
      child: SingleChildScrollView(
        physics: const AlwaysScrollableScrollPhysics(),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              ClipRRect(
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(radius),
                    topLeft: Radius.circular(radius)),
                child: AppNetworkImage(
                  imageUrl,
                  placeHolderShape: BoxShape.rectangle,
                ),
              ),
              Padding(
                padding: EdgeInsets.all(padding),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Text(
                      title,
                      style: AppTextStyle.header1,
                    ),
                    SizedBox(
                      height: padding - 6,
                    ),
                    Text(
                      message,
                      style: AppTextStyle.value,
                    ),
                  ],
                ),
              ),
              const Divider(),
              Padding(
                padding: EdgeInsets.only(
                    left: padding - 8, right: padding - 8, bottom: padding - 8),
                child: isOkCancel
                    ? Row(
                        children: [
                          Expanded(
                            child: TextButton(
                                style: AppButtonStyle
                                    .kSecondaryButtonExpandStyle(),
                                onPressed: () {
                                  if (cancelButtonClicked != null) {
                                    cancelButtonClicked!();
                                  }
                                  Get.back();
                                },
                                child: Text(
                                  cancelTitleButton ?? "Cancel".tr,
                                  style: AppTextStyle.caption,
                                )),
                          ),
                          Expanded(
                            child: TextButton(
                                style: AppButtonStyle
                                    .kSecondaryButtonExpandStyle(),
                                onPressed: () {
                                  if (okButtonClicked != null) {
                                    okButtonClicked!();
                                  }

                                  Get.back();
                                },
                                child: Text(
                                  okTitleButton ?? "Ok".tr,
                                  style: AppTextStyle.primary1,
                                )),
                          ),
                        ],
                      )
                    : TextButton(
                        style: AppButtonStyle.kSecondaryButtonExpandStyle(),
                        onPressed: () {
                          if (okButtonClicked != null) {
                            okButtonClicked!();
                          }
                          Get.back();
                        },
                        child: Text(
                          okTitleButton ?? "Ok".tr,
                          style: AppTextStyle.primary1,
                        )),
              ),
            ]),
      ),
    );
  }
}
