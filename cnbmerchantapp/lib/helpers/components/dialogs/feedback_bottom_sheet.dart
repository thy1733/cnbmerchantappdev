import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

typedef OnSubmitReview = void Function(FeedbackInputModel feedbackInputModel);

class FeedbackBottomSheet extends StatefulWidget {
  const FeedbackBottomSheet({super.key, required this.onSubmitReview});

  final OnSubmitReview onSubmitReview;

  @override
  State<FeedbackBottomSheet> createState() => _FeedbackBottomSheetState();
}

class _FeedbackBottomSheetState extends State<FeedbackBottomSheet> {
  //#region All Override method

  @override
  void initState() {
    super.initState();
    _focusNode.addListener(() {
      isReviewFieldFocused = _focusNode.hasFocus;
    });
  }

  @override
  void dispose() {
    _focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double containerSize = Get.height * 0.86;
    if (!hasSafeArea(context)) {
      containerSize += 16;
    }
    return Obx(() => Container(
          constraints: BoxConstraints(maxHeight: containerSize),
          child: Material(
            color: AppColors.white,
            child: GestureDetector(
              onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
              child: Stack(
                alignment: Alignment.topRight,
                children: [
                  SingleChildScrollView(
                    padding: EdgeInsets.only(
                        top: 50,
                        bottom: hasSafeArea(context)
                            ? getBottomSafeArea(context)
                            : 16),
                    scrollDirection: Axis.vertical,
                    controller: ScrollController(),
                    physics: const AlwaysScrollableScrollPhysics(),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(top: 26, bottom: 24),
                          child: SvgPicture.asset(
                            SvgAppAssets.feedbackLogo,
                            width: 92,
                            height: 84,
                          ),
                        ),
                        _buildReviewTextField(),
                        Padding(
                          padding: const EdgeInsets.only(top: 20, bottom: 25),
                          child: Text(
                            "RatingStarTitle".tr,
                            style: const TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                        ),
                        _buildRatingBar(),
                        AppButtonStyle.primaryButton(
                          margin: EdgeInsets.only(
                              left: 15,
                              right: 15,
                              top: 50,
                              bottom: MediaQuery.of(context).viewInsets.bottom),
                          label: "Submit".tr,
                          isEnable: validation(),
                          onPressed: () => onSubmitAsync(),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: 50,
                    color: AppColors.white,
                    child: const Row(
                      children: [
                        Spacer(),
                        CloseBottomSheetButton(),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ));
  }

  //#endregion

  //#region All Properties

  final _focusNode = FocusNode();

  final _reviewText = ''.obs;
  String get reviewText => _reviewText.value;
  set reviewText(value) => _reviewText.value = value;

  final _rating = 0.0.obs;
  double get rating => _rating.value;
  set rating(value) => _rating.value = value;

  final _isReviewFieldFocused = false.obs;
  bool get isReviewFieldFocused => _isReviewFieldFocused.value;
  set isReviewFieldFocused(bool value) => _isReviewFieldFocused.value = value;

  //#endregion

  //#region All Method Helpers

  Widget _buildReviewTextField() {
    int maxLength = 500;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Container(
          margin: const EdgeInsets.symmetric(horizontal: 15),
          decoration: BoxDecoration(
            color: AppColors.textFieldBackground,
            borderRadius: BorderRadius.circular(10),
          ),
          child: Padding(
            padding: const EdgeInsets.symmetric(
              vertical: 10,
              horizontal: 16,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "WriteReviewTitle".tr,
                  style: const TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w400,
                  ),
                ),
                TextFormField(
                  focusNode: _focusNode,
                  minLines: 1,
                  maxLines: 5,
                  keyboardType: TextInputType.multiline,
                  inputFormatters: [
                    CustomLengthLimitingTextInputFormatter(maxLength),
                  ],
                  decoration: InputDecoration(
                    hintText: "WriteReviewHint".tr,
                    border: InputBorder.none,
                  ),
                  style: const TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                  ),
                  initialValue: reviewText,
                  onChanged: (value) => reviewTextChange(value),
                ),
              ],
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 4, right: 23),
          child: Text(
            "${reviewText.length}/$maxLength",
            style:
                AppTextStyle.caption.copyWith(color: AppColors.textSecondary),
          ),
        ),
      ],
    );
  }

  Widget _buildRatingBar() {
    return RatingBar.builder(
      initialRating: rating,
      minRating: 1,
      direction: Axis.horizontal,
      allowHalfRating: true,
      itemCount: 5,
      itemPadding: const EdgeInsets.symmetric(horizontal: 4.0),
      itemBuilder: (context, _) => const Icon(
        Icons.star,
        size: 48,
        color: AppColors.secondary,
      ),
      onRatingUpdate: (rating) => ratingChange(rating),
    );
  }

  bool validation() {
    return reviewText.isNotEmpty || rating > 0.0;
  }

  void reviewTextChange(String value) {
    reviewText = value;
  }

  void ratingChange(double value) {
    rating = value;
  }

  //#endregion

  //#region All Command

  Future<void> onSubmitAsync() async {
    if (validation()) {
      Get.back();
      FeedbackInputModel input = FeedbackInputModel()
        ..review = reviewText
        ..rating = rating;
      widget.onSubmitReview(input);
    } else {
      debugPrint("Please provide rating or write a review");
    }
  }

  //#endregion

}
