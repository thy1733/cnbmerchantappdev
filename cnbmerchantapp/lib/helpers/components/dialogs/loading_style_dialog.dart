import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

// ignore: must_be_immutable
class InitLoadingDialog extends StatelessWidget {
  const InitLoadingDialog({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async => false,
        child: Center(
          child: Container(
            padding: const EdgeInsets.all(20),
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: AppColors.white.withOpacity(0),
            ),
            child: Lottie.asset(JsonAppAssets.cnbMerchantLoading,
                height: 110, width: 110),
          ),
        ));
  }
}
