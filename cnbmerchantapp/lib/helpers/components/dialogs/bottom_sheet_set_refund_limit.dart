import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';

class BottomSheetRefundLimit extends StatelessWidget {
  final ScrollController scrollController = ScrollController();

  BottomSheetRefundLimit({
    Key? key,
    required this.title,
    required this.onConfirmed,
    this.onCanceled,
    this.initialAmount = "0",
  }) : super(key: key);

  final String title;
  final String initialAmount;

  final Function(String value) onConfirmed;
  final Function? onCanceled;

  final _amount = 0.0.obs;
  double get amount => _amount.value;
  set amount(double value) => _amount.value = value;

  final _refundAmount = "0".obs;
  String get refundAmount => _refundAmount.value;
  set refundAmount(String value) => _refundAmount.value = value;

  final _currency = CurrencyEnum.usd.obs;
  CurrencyEnum get currency => _currency.value;
  set currency(CurrencyEnum value) => _currency.value = value;

  @override
  Widget build(BuildContext context) {
    final double padding = 16.r;

    double bottomMargin =
        hasSafeArea(context) ? getBottomSafeArea(context) : 15;

    if (initialAmount != "0") refundAmount = initialAmount;

    return Obx(
      () => Material(
        color: AppColors.white,
        child: SingleChildScrollView(
          controller: scrollController,
          physics: const AlwaysScrollableScrollPhysics(),
          child: Padding(
            padding: MediaQuery.of(Get.context!).viewInsets,
            child: Stack(
              alignment: AlignmentDirectional.topStart,
              children: [
                Padding(
                  padding: EdgeInsets.all(padding),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      SizedBox(
                        height: 50.r,
                      ),
                      Text(
                        "${CurrencyConverter.currencyToSymble(currency)}${thousandSeparatQR(refundAmount)}",
                        style: const TextStyle(
                            fontSize: 40, color: AppColors.textPrimary),
                      ),
                      SizedBox(
                        height: 300.h - bottomMargin,
                        child: KNumpadWidget(
                          onPressed: userTapOnKeyboardAsync,
                          onBackSpacePressed: userTabOnBackSpaceAsync,
                        ),
                      ),
                      SizedBox(
                        height: padding,
                      ),
                      ButtonCircleRadius(
                        title: "Confirm".tr,
                        submit: (_) {
                          if (validate()) {
                            onConfirmed(refundAmount);
                            Get.back();
                          } else {
                            Fluttertoast.showToast(msg: "SetRefundMessage".tr);
                          }
                        },
                      ),
                      SizedBox(
                        height: padding,
                      ),
                      onCanceled != null
                          ? TextButton(
                              style:
                                  AppButtonStyle.kSecondaryButtonExpandStyle(),
                              onPressed: () {
                                onCanceled!();
                                Get.back();
                              },
                              child: Text(
                                "Cancel".tr,
                                style: AppTextStyle.primary,
                              ))
                          : const SizedBox(),
                      SizedBox(
                        height: hasSafeArea(context)
                            ? getBottomSafeArea(context)
                            : 16,
                      ),
                    ],
                  ),
                ),
                Container(
                  color: AppColors.white,
                  height: 60.r,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        title,
                        style: AppTextStyle.headline6.copyWith(
                          color: AppColors.black,
                          decoration: TextDecoration.none,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  bool validate() => double.tryParse(refundAmount)! > 0;

  Future<void> userTapOnKeyboardAsync(String value) async {
    if (value == ValueConst.point) {
      if (refundAmount.contains(ValueConst.point)) return;
      refundAmount += value;
    } else {
      // Check max length user input
      if (refundAmount.length >= 11) return;

      if (refundAmount.length == 1 && refundAmount == '0') {
        refundAmount = value;
      } else {
        if (checkDecimalValue(refundAmount) == 3) return;
        refundAmount += value;
      }
    }
    double inputAmount =
        double.tryParse(refundAmount.toSafeDoubleAsString()) ?? 0;

    amount = inputAmount;
  }

  Future<void> userTabOnBackSpaceAsync() async {
    if (refundAmount.isNotEmpty) {
      String result = refundAmount.substring(0, refundAmount.length - 1);
      refundAmount = result.isEmpty ? '0' : result;
    }
    refundAmount = refundAmount.isEmpty ? '0' : refundAmount;
    amount = double.parse(refundAmount.toSafeDoubleAsString());
  }

  int checkDecimalValue(String text) {
    if (text.contains(ValueConst.point)) {
      int startIndex = text.indexOf('.');
      var value = text.substring(startIndex);
      return value.length;
    }
    return 0;
  }
}
