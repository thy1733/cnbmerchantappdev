import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ImagePickerDialog extends StatelessWidget {
  const ImagePickerDialog(
      {Key? key, required this.onTakePressed, required this.onLibraryPressed})
      : super(key: key);

  final GestureTapCallback onTakePressed;
  final GestureTapCallback onLibraryPressed;

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: Material(
        color: Colors.transparent,
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: Ink(
            width: Get.width,
            padding: const EdgeInsets.all(10),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: AppColors.white,
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                ContainerDevider.blankSpaceSm,
                Text(
                  "UploadImage".tr,
                  style: AppTextStyle.header1,
                ),
                ContainerDevider.blankSpaceSm,
                TextButton(
                  onPressed: onTakePressed,
                  child: Padding(
                    padding: const EdgeInsets.all(10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Icon(
                          MerchantIcon.cameraOutline,
                          color: AppColors.textPrimary,
                          size: AppFontSize.subtitle,
                        ),
                        ContainerDevider.blankSpaceMd,
                        Text(
                          'TakePhoto'.tr,
                          textAlign: TextAlign.start,
                          style: AppTextStyle.header2,
                        )
                      ],
                    ),
                  ),
                ),
                TextButton(
                  onPressed: onLibraryPressed,
                  child: Padding(
                    padding: const EdgeInsets.all(10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Icon(
                          MerchantIcon.image,
                          color: AppColors.textPrimary,
                          size: AppFontSize.subtitle,
                        ),
                        ContainerDevider.blankSpaceMd,
                        Text(
                          'BrowseFromPhone'.tr,
                          textAlign: TextAlign.start,
                          style: AppTextStyle.header2,
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
