import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:share_plus/share_plus.dart';
import 'package:fluttertoast/fluttertoast.dart';

class KSystemService {
  Future<void> copyLinktoClipboardAsync(String text,
      {bool showToast = true}) async {
    await Clipboard.setData(ClipboardData(text: text));
    if (showToast) {
      await Fluttertoast.showToast(
          msg: 'Copied'.tr, toastLength: Toast.LENGTH_SHORT);
    }
  }

  Future<void> shareLinkedAsync(
    String text, {
    String? subject,
    Rect? sharePositionOrigin,
  }) async {
    await Share.share(text,
        subject: subject, sharePositionOrigin: sharePositionOrigin);
  }

  static Future<void> shareFiles(
    List<String> paths, {
    List<String>? mimeTypes,
    String? subject,
    String? text,
    Rect? sharePositionOrigin,
  }) async {
    var xfiles = paths.map((e) => XFile(e)).toList();
    await Share.shareXFiles(xfiles,
        subject: subject,
        text: text,
        sharePositionOrigin: sharePositionOrigin);
  }

}
