import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:cnbmerchantapp/core.dart';

class CCheckBoxWidget extends StatelessWidget {
  const CCheckBoxWidget({
    Key? key,
    required this.isSelected,
    required this.value,
    this.text,
    this.child,
    this.fillColor,
    this.checkColor,
    this.borderRadiusSize = 5,
    this.onChange,
  }) : super(key: key);
  final String? text;
  final dynamic value;
  final bool isSelected;
  final Widget? child;
  final ValueChanged<dynamic>? onChange;
  final Color? fillColor;
  final Color? checkColor;
  final double borderRadiusSize;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.max,
      children: [
        Checkbox(
          value: isSelected,
          fillColor: MaterialStateProperty.all(fillColor ?? AppColors.black),
          checkColor: checkColor ?? AppColors.primary,
          shape: RoundedRectangleBorder(
              side: const BorderSide(
                width: 0.5,
              ),
              borderRadius:
                  BorderRadius.all(Radius.circular(borderRadiusSize))),
          onChanged: (va) => onChange!(value),
        ),
        Expanded(
            child: InkWell(
          onTap: () {
            return onChange!(value);
          },
          child: child ?? Text('${text ?? value}'),
        ))
      ],
    );
  }
}

class CRoundCheckBoxWidget<T> extends StatelessWidget {
  const CRoundCheckBoxWidget(
      {Key? key,
      required this.isSelected,
      this.text,
      this.underlineText,
      this.child,
      required this.value,
      this.onChange,
      this.underlineAction})
      : super(key: key);
  final String? text;
  final String? underlineText;
  final T value;
  final bool isSelected;
  final Widget? child;
  final Function()? underlineAction;

  final ValueChanged<T>? onChange;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.max,
      children: [
        Checkbox(
          value: isSelected,
          fillColor: MaterialStateProperty.all(AppColors.primary),
          shape: const RoundedRectangleBorder(
              side: BorderSide(
                width: 0.5,
              ),
              borderRadius: BorderRadius.all(Radius.circular(20))),
          onChanged: (va) => onChange!(value),
        ),
        Expanded(
          child: child != null
              ? InkWell(
                  onTap: () {
                    return onChange!(value);
                  },
                  child: child)
              : Text.rich(
                  TextSpan(
                    style: AppTextStyle.body2
                        .copyWith(color: AppColors.textPrimary),
                    children: [
                      TextSpan(
                          text: "$text ",
                          recognizer: TapGestureRecognizer()
                            ..onTap = () => onChange!(value)),
                      TextSpan(
                        text: underlineText,
                        style: const TextStyle(
                            color: AppColors.textPrimary,
                            fontWeight: FontWeight.bold,
                            decoration: TextDecoration.underline,
                            fontSize: 14),
                        recognizer: TapGestureRecognizer()
                          ..onTap = () => underlineAction!(),
                      ),
                    ],
                  ),
                ),
        )
      ],
    );
  }
}
