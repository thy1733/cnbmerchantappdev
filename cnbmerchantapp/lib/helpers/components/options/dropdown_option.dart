import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class DropdownOption extends StatelessWidget {
  const DropdownOption({
    Key? key,
    required this.title,
    required this.value,
    this.leadingIconData,
    this.child,
    this.trailingIconData = Icons.arrow_drop_down_outlined,
    this.color = AppColors.textFieldBackground,
    this.textColor = AppColors.textPrimary,
  }) : super(key: key);

  final String title;
  final String value;
  final Widget? child;
  final IconData? leadingIconData;
  final IconData trailingIconData;
  final Color color;
  final Color textColor;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(left: 15, top: 10, right: 10, bottom: 0),
      margin: const EdgeInsets.only(bottom: 0),
      decoration: BoxDecoration(
        color: color,
        borderRadius: const BorderRadius.all(
          Radius.circular(7),
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            title,
            style: AppTextStyle.overline.copyWith(
              color: AppColors.textSecondary,
            ),
          ),
          Row(
            children: [
              if (leadingIconData != null) ...[
                Icon(
                  leadingIconData,
                  size: 15,
                  color: AppColors.black,
                ),
                const SizedBox(
                  width: 10,
                ),
              ],
              const SizedBox(
                height: 40,
              ),
              Expanded(
                child: child ??
                    Text(
                      value,
                      style: AppTextStyle.body2.copyWith(
                        color: textColor,
                      ),
                    ),
              ),
              Icon(
                trailingIconData,
                size: 24,
                color: Colors.black,
              ),
            ],
          ),
          const SizedBox(
            height: 8,
          ),
        ],
      ),
    );
  }
}
