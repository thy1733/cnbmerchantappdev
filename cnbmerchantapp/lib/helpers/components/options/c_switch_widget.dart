import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:cnbmerchantapp/core.dart';

class CSwitchWidget extends StatelessWidget {
  const CSwitchWidget(
      {Key? key,
      required this.value,
      this.text,
      this.child,
      this.isEnable = true,
      required this.onChanged})
      : assert(child != null || text != null),
        super(key: key);
  final String? text;
  final bool value;
  final bool isEnable;
  final Widget? child;
  final ValueChanged<dynamic> onChanged;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.max,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Expanded(
            child: InkWell(
          onTap: () {
            if (!isEnable) return;
            return onChanged(value);
          },
          child: child ??
              Text(
                '$text',
                style: AppTextStyle.body2.copyWith(color: AppColors.black),
              ),
        )),
        CupertinoSwitch(
          value: value,
          onChanged: (value) {
            if (!isEnable) return;
            return onChanged(value);
          },
          activeColor: isEnable ? AppColors.primary : AppColors.greyishDisable,
          // trackColor:
          //     isEnable ? kSwitchTrackEnableColor : kSwitchTrackDisableColor,
        )
      ],
    );
  }
}
