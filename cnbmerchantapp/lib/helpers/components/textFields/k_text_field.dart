import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get_utils/src/extensions/internacionalization.dart';
import 'package:cnbmerchantapp/core.dart';

//**
// OnTextChange will return a string value as parameter
// */
typedef OnTextChange<T> = void Function(T vale);

//**
// KTextField
// */
class KTextField extends StatelessWidget {
  /// KTextField
  ///
  /// **Note [validatorMessage] == null it's will show optional
  const KTextField({
    Key? key,
    required this.name,
    this.label = "",
    this.initialValue = "",
    this.hint = "",
    this.message,
    this.obscureText = false,
    this.inputFormatters,
    this.validatorMessage,
    this.iconData = Icons.menu,
    this.needIcon = true,
    this.keyboardType = TextInputType.text,
    this.textInputAction,
    this.prefixText = "",
    this.secondChild,
    this.maxLine,
    this.valueCounter,
    this.autoFocus = false,
    this.autocorrect = true,
    this.enableSuggestions = false,
    this.focusNode,
    this.controller,
    this.onChanged,
    this.onTap,
    this.onEditingComplete,
    this.onSubmit,
    this.textCapitalization = TextCapitalization.none,
    this.maxLength,
    this.enabled = true,
    this.backgroundColor = AppColors.textFieldBackground,
    this.iconColor = AppColors.black,
    this.textColor = AppColors.textPrimary,
  }) : super(key: key);

  final String name;
  final String initialValue;
  final String label;
  final String hint;
  final String? message;
  final IconData iconData;
  final TextInputType keyboardType;
  final TextInputAction? textInputAction;
  final bool obscureText;
  final List<TextInputFormatter>? inputFormatters;
  final String? validatorMessage;
  final int? maxLine;
  final int? valueCounter;
  final bool autoFocus;
  final bool needIcon;
  final bool autocorrect;
  final bool enableSuggestions;
  final FocusNode? focusNode;
  final Widget? secondChild;
  final String prefixText;
  final TextEditingController? controller;
  final TextCapitalization textCapitalization;
  final int? maxLength;
  final bool enabled;
  final Color backgroundColor;
  final Color iconColor;
  final Color textColor;

  /// Called when the field value is changed.
  final ValueChanged<String?>? onChanged;
  final ValueChanged<String?>? onSubmit;
  final GestureTapCallback? onTap;
  final VoidCallback? onEditingComplete;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
            padding:
                const EdgeInsets.only(left: 15, top: 10, right: 10, bottom: 10),
            margin: const EdgeInsets.only(bottom: 0),
            decoration: BoxDecoration(
              color: backgroundColor,
              borderRadius: const BorderRadius.all(Radius.circular(7)),
            ),
            child: Stack(children: [
              Text(
                "${label.isEmpty ? name : label} ${validatorMessage == null ? 'optional'.tr : ''}",
                style: AppTextStyle.overline.copyWith(
                  color: AppColors.textSecondary,
                ),
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Visibility(
                      visible: needIcon,
                      child: Padding(
                          padding: const EdgeInsets.only(top: 25, right: 10),
                          child: Icon(
                            iconData,
                            size: 15,
                            color: iconColor,
                          ))),
                  if (prefixText.isNotEmpty) ...[
                    Padding(
                      padding: const EdgeInsets.only(top: 19),
                      child: Text(
                        "$prefixText ",
                        style: const TextStyle(
                          color: AppColors.grey,
                          fontSize: 16,
                        ),
                      ),
                    )
                  ],
                  Expanded(
                    child: FormBuilderTextField(
                      style: TextStyle(color: textColor),
                      enabled: enabled,
                      maxLength: maxLength,
                      textCapitalization: textCapitalization,
                      controller: controller,
                      obscureText: obscureText,
                      obscuringCharacter: '●',
                      initialValue: controller == null ? initialValue : null,
                      selectionHeightStyle:
                          BoxHeightStyle.includeLineSpacingTop,
                      keyboardType: keyboardType,
                      textInputAction: textInputAction,
                      minLines: 1,
                      maxLines: obscureText ? 1 : maxLine,
                      onChanged: onChanged,
                      autofocus: autoFocus,
                      autocorrect: autocorrect,
                      enableSuggestions: enableSuggestions,
                      onTap: onTap,
                      onEditingComplete: onEditingComplete,
                      focusNode: focusNode,
                      onSubmitted: onSubmit,
                      inputFormatters: inputFormatters ??
                          [
                            if (keyboardType == TextInputType.phone) ...[
                              MaskFormat.phoneNumber,
                              FilteringTextInputFormatter.deny(RegExp(r'^0+')),
                            ]
                          ],
                      name: name,
                      decoration: InputDecoration(
                        contentPadding: const EdgeInsets.only(top: 15),
                        counterText: maxLength != null && valueCounter != null
                            ? "$valueCounter/$maxLength"
                            : "",
                        counterStyle: (maxLength ?? 0) == valueCounter
                            ? const TextStyle(fontWeight: FontWeight.bold)
                            : null,
                        hintText: hint,
                        hintStyle: const TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                          color: AppColors.grey,
                        ),
                        border: InputBorder.none,
                      ),
                    ),
                  ),
                  Visibility(
                      visible: secondChild != null,
                      child: Padding(
                          padding: const EdgeInsets.only(top: 17),
                          child: secondChild)),
                ],
              ),
            ])),
        AnimatedCrossFade(
          firstChild: KTextFieldValidationMessageWidget(
            title: validatorMessage ?? '',
          ),
          excludeBottomFocus: false,
          secondChild: message != null
              ? Padding(
                  padding: const EdgeInsets.only(
                      left: 5, right: 5, top: 5, bottom: 15),
                  child: Text(message!,
                      textAlign: TextAlign.start, style: AppTextStyle.label))
              : const SizedBox(
                  height: 15,
                ),
          crossFadeState:
              validatorMessage != null && validatorMessage!.isNotEmpty
                  ? CrossFadeState.showFirst
                  : CrossFadeState.showSecond,
          duration: const Duration(milliseconds: 200),
        ),
      ],
    );
  }
}
