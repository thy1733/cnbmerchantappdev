import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class KTextFieldValidationMessageWidget extends StatelessWidget {
  const KTextFieldValidationMessageWidget({Key? key, required this.title})
      : super(key: key);

  final String title;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 5, right: 5, top: 5, bottom: 15),
      child: Text(
        title,
        textAlign: TextAlign.start,
        style: const TextStyle(
            color: AppColors.primary,
            fontSize: 11,
            decorationColor: Colors.transparent,
            decoration: TextDecoration.none,
            fontWeight: FontWeight.w400),
      ),
    );
  }
}
