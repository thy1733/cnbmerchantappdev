import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:cnbmerchantapp/core.dart';

class KAlertScreen {
  /// [child] is use in Column so it can't expanded
  static Future<void> success({
    required String message,
    String description = '',
    Widget? child,
    String primaryButtonTitle = 'Done',
    VoidCallback? onPrimaryButtonClick,
    String? secondaryButtonTitle,
    VoidCallback? onScondaryButtonClick,
  }) async {
    var icon = Lottie.asset(JsonAppAssets.signSuccessAlert,
        height: 135, repeat: false);
    await Get.dialog(
        _SuccessScreen(
            icon: icon,
            message: message,
            description: description,
            primaryButtonTitle: primaryButtonTitle,
            onPrimaryButtonClick: onPrimaryButtonClick,
            secondaryButtonTitle: secondaryButtonTitle,
            onSecondaryButtonClick: onScondaryButtonClick,
            child: child),
        useSafeArea: false,
        transitionDuration: const Duration(milliseconds: 100));
  }

  static Future<void> failure({
    required String message,
    String description = '',

    /// [child] is use in Column so it can be expand
    Widget? child,
    String primaryButtonTitle = 'Done',
    VoidCallback? onPrimaryButtonClick,
    String? secondaryButtonTitle,
    VoidCallback? onScondaryButtonClick,
  }) async {
    var icon =
        Lottie.asset(JsonAppAssets.signErrorAlert, height: 135, repeat: false);
    await Get.dialog(
      _SuccessScreen(
          icon: icon,
          message: message,
          description: description,
          primaryButtonTitle: primaryButtonTitle,
          onPrimaryButtonClick: onPrimaryButtonClick,
          secondaryButtonTitle: secondaryButtonTitle,
          onSecondaryButtonClick: onScondaryButtonClick,
          child: child),
      useSafeArea: false,
    );
  }

  static Future<void> other({
    required String message,
    required Widget icon,

    /// [child] is use in Column so it can be expand
    Widget? child,
    String description = '',
    String primaryButtonTitle = 'Done',
    VoidCallback? onPrimaryButtonClick,
    String? secondaryButtonTitle,
    VoidCallback? onScondaryButtonClick,
  }) async {
    await Get.dialog(
      _SuccessScreen(
          icon: icon,
          message: message,
          description: description,
          primaryButtonTitle: primaryButtonTitle,
          onPrimaryButtonClick: onPrimaryButtonClick,
          secondaryButtonTitle: secondaryButtonTitle,
          onSecondaryButtonClick: onScondaryButtonClick,
          child: child),
      useSafeArea: false,
    );
  }

  static Future<void> review({
    required String message,
    required Widget icon,

    /// [child] is use in Column so it can be expand
    Widget? child,
    String description = '',
    String primaryButtonTitle = 'Done',
    VoidCallback? onPrimaryButtonClick,
    String? secondaryButtonTitle,
    VoidCallback? onScondaryButtonClick,
  }) async {
    await Get.dialog(
      ReviewScreen(
          icon: icon,
          message: message,
          description: description,
          primaryButtonTitle: primaryButtonTitle,
          onPrimaryButtonClick: onPrimaryButtonClick,
          secondaryButtonTitle: secondaryButtonTitle,
          onSecondaryButtonClick: onScondaryButtonClick,
          child: child),
      useSafeArea: false,
    );
  }

  static Future<void> successInviteCode({
    required String message,
    required String description,
    required String formatedInviteCode,
    required String expiredIn,
    String? purpose,
    Widget? child,
    String primaryButtonTitle = 'Done',
    VoidCallback? onPrimaryButtonClick,
    String? secondaryButtonTitle,
    VoidCallback? onSecondaryButtonClick,
  }) async {
    var icon = Lottie.asset(JsonAppAssets.signSuccessAlert,
        height: 135, repeat: false);
    await Get.dialog(
        SuccessInviteCodeScreen(
          icon: icon,
          message: message,
          description: description,
          purpose: purpose,
          primaryButtonTitle: primaryButtonTitle,
          onPrimaryButtonClick: onPrimaryButtonClick,
          secondaryButtonTitle: secondaryButtonTitle,
          onSecondaryButtonClick: onSecondaryButtonClick,
          formatedInviteCode: formatedInviteCode,
          expiredIn: expiredIn,
        ),
        useSafeArea: false,
        transitionDuration: const Duration(milliseconds: 100));
  }
}

class _SuccessScreen extends StatelessWidget {
  const _SuccessScreen({
    Key? key,
    required this.icon,
    required this.message,
    this.child,
    this.description = '',
    this.primaryButtonTitle = 'Done',
    this.secondaryButtonTitle,
    this.onPrimaryButtonClick,
    this.onSecondaryButtonClick,
  }) : super(key: key);
  final Widget icon;
  final Widget? child;
  final String message;
  final String description;
  final String primaryButtonTitle;
  final String? secondaryButtonTitle;
  final VoidCallback? onPrimaryButtonClick;
  final VoidCallback? onSecondaryButtonClick;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: gradientContainer(
        begin: Alignment.topCenter,
        end: Alignment.bottomCenter,
        child: Stack(
          alignment: Alignment.topRight,
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 70, right: 20),
              child: SvgPicture.asset(SvgAppAssets.cnbBuildingBg),
            ),
            Padding(
              padding: const EdgeInsets.all(20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  const SizedBox(
                    height: 70,
                  ),
                  SizedBox(
                    height: 135,
                    width: 135,
                    child: icon,
                  ),
                  ContainerDevider.blankSpaceMd,
                  Text(
                    message,
                    textAlign: TextAlign.center,
                    style: const TextStyle(
                        color: AppColors.white,
                        fontSize: 27,
                        fontWeight: FontWeight.w700),
                  ),
                  ContainerDevider.blankSpaceMd,
                  Text(
                    description,
                    textAlign: TextAlign.center,
                    style: AppTextStyle.body2,
                  ),
                  Expanded(child: child ?? const SizedBox()),
                  AppButtonStyle.primaryButton(
                      label: primaryButtonTitle,
                      onPressed: () {
                        onPrimaryButtonClick != null
                            ? onPrimaryButtonClick!()
                            : Get.back();
                      },
                      color: AppColors.white,
                      textColor: AppColors.primary),
                  if (onSecondaryButtonClick != null) ...[
                    const SizedBox(height: 15),
                    AppButtonStyle.primaryButton(
                      label: secondaryButtonTitle ?? "Title",
                      onPressed: onSecondaryButtonClick ?? () {},
                    ),
                  ],
                  const SizedBox(height: 10)
                ],
              ),
            ),
          ],
        ),
      ),
      onWillPop: () async {
        onPrimaryButtonClick != null ? onPrimaryButtonClick!() : Get.back();
        return false;
      },
    );
  }
}

class ReviewScreen extends StatelessWidget {
  const ReviewScreen({
    super.key,
    required this.icon,
    required this.message,
    this.child,
    this.description = '',
    this.primaryButtonTitle = 'Done',
    this.secondaryButtonTitle,
    this.onPrimaryButtonClick,
    this.onSecondaryButtonClick,
  });

  final Widget icon;
  final Widget? child;
  final String message;
  final String description;
  final String primaryButtonTitle;
  final String? secondaryButtonTitle;
  final VoidCallback? onPrimaryButtonClick;
  final VoidCallback? onSecondaryButtonClick;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: gradientContainer(
        begin: Alignment.topCenter,
        end: Alignment.bottomCenter,
        child: Stack(
          alignment: Alignment.topRight,
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 70, right: 20),
              child: SvgPicture.asset(SvgAppAssets.cnbBuildingBg),
            ),
            Padding(
              padding: const EdgeInsets.all(20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  const SizedBox(
                    height: 70,
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [icon],
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Text(message,
                      textAlign: TextAlign.center, style: AppTextStyle.header),
                  const SizedBox(
                    height: 10,
                  ),
                  Text(description,
                      textAlign: TextAlign.center, style: AppTextStyle.body1),
                  Expanded(child: child ?? const SizedBox()),
                  TextButton(
                      style: AppButtonStyle.kSecondaryButtonExpandStyle(),
                      onPressed: () {
                        onPrimaryButtonClick != null
                            ? onPrimaryButtonClick!()
                            : Get.back();
                      },
                      child: Text(
                        primaryButtonTitle,
                        style: AppTextStyle.primary1,
                      )),
                  if (onSecondaryButtonClick != null) ...[
                    const SizedBox(height: 15),
                    ElevatedButton(
                      style: ButtonStyle(
                        padding: MaterialStateProperty.all(
                          const EdgeInsets.symmetric(vertical: 13),
                        ),
                        backgroundColor:
                            MaterialStateProperty.all(AppColors.transparent),
                        overlayColor: MaterialStateProperty.all(
                            AppColors.white.withOpacity(.3)),
                        shape:
                            MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(25.0),
                          ),
                        ),
                      ),
                      onPressed: onSecondaryButtonClick,
                      child: Text(
                        secondaryButtonTitle ?? "Title",
                        style: const TextStyle(
                            fontSize: 17, color: AppColors.white),
                      ),
                    ),
                  ],
                  const SizedBox(height: 10)
                ],
              ),
            ),
          ],
        ),
      ),
      onWillPop: () async {
        onPrimaryButtonClick != null ? onPrimaryButtonClick!() : Get.back();
        return false;
      },
    );
  }
}

class SuccessInviteCodeScreen extends StatelessWidget {
  const SuccessInviteCodeScreen({
    super.key,
    required this.icon,
    required this.message,
    required this.description,
    required this.formatedInviteCode,
    required this.expiredIn,
    required this.primaryButtonTitle,
    this.purpose,
    this.secondaryButtonTitle,
    this.onPrimaryButtonClick,
    this.onSecondaryButtonClick,
  });

  final Widget icon;
  final String message;
  final String description;
  final String formatedInviteCode;
  final String expiredIn;
  final String? purpose;
  final String primaryButtonTitle;
  final String? secondaryButtonTitle;
  final VoidCallback? onPrimaryButtonClick;
  final VoidCallback? onSecondaryButtonClick;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: gradientContainer(
        begin: Alignment.topCenter,
        end: Alignment.bottomCenter,
        child: Stack(
          alignment: Alignment.topRight,
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 70, right: 20),
              child: SvgPicture.asset(SvgAppAssets.cnbBuildingBg),
            ),
            Padding(
              padding: const EdgeInsets.all(20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  const SizedBox(
                    height: 70,
                  ),
                  SizedBox(
                    height: 135,
                    width: 135,
                    child: icon,
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Text(message,
                      textAlign: TextAlign.center,
                      style: AppTextStyle.messageTextStyle),
                  const SizedBox(
                    height: 10,
                  ),
                  Text.rich(
                    TextSpan(children: [
                      TextSpan(text: "Ask".tr, style: AppTextStyle.header3),
                      const TextSpan(text: " "),
                      TextSpan(text: description, style: AppTextStyle.header),
                      const TextSpan(text: " "),
                      TextSpan(
                          text: purpose ?? "toDownloadMerchantAppMsg".tr,
                          style: AppTextStyle.header3)
                    ]),
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(
                    height: 16.r,
                  ),
                  Container(
                    padding: EdgeInsets.all(20.r),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          formatedInviteCode,
                          style: AppTextStyle.inviteCode,
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Text(
                          '${'CodeExpired'.tr} $expiredIn ${'Hours'.tr}',
                          style: AppTextStyle.label1,
                        )
                      ],
                    ),
                  ),
                  Expanded(child: Container()),
                  SizedBox(
                    height: 16.r,
                  ),
                  TextButton(
                    style: AppButtonStyle.kSecondaryButtonExpandStyle(),
                    onPressed: () {
                      onPrimaryButtonClick != null
                          ? onPrimaryButtonClick!()
                          : Get.back();
                    },
                    child: SizedBox(
                      height: 40.r,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Icon(
                            MerchantIcon.share,
                            color: AppColors.primary,
                            size: 17,
                          ),
                          const SizedBox(
                            width: 10,
                          ),
                          Text("ShareInviteCode".tr,
                              style: AppTextStyle.buttonTextStyle)
                        ],
                      ),
                    ),
                  ),
                  if (onSecondaryButtonClick != null) ...[
                    const SizedBox(height: 15),
                    ButtonCircleRadius(
                        title: secondaryButtonTitle ?? "Done".tr,
                        submit: (_) {
                          onSecondaryButtonClick!();
                        }),
                  ],
                  const SizedBox(height: 10)
                ],
              ),
            ),
          ],
        ),
      ),
      onWillPop: () async {
        onPrimaryButtonClick != null ? onPrimaryButtonClick!() : Get.back();
        return false;
      },
    );
  }
}
