import 'package:package_info_plus/package_info_plus.dart';

class AppInfoHelper {
  String appName = "";
  String packageName = "";
  String version = "";
  String buildNumber = "";

  AppInfoHelper() {
    _getPackageInfo();
  }

  void _getPackageInfo() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    appName = packageInfo.appName;
    packageName = packageInfo.packageName;
    version = packageInfo.version;
    buildNumber = packageInfo.buildNumber;
  }
}
