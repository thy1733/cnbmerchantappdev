import 'package:get/get.dart';

class PlatformInfo{
  
  static int platform = GetPlatform.isAndroid ? 0 : 1;
}