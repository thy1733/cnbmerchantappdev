import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:skeletons/skeletons.dart';

class AppNetworkImage extends StatelessWidget {
  final String imageUrl;
  final double? width;
  final double? height;
  final Widget? errorWidget;
  final BoxShape placeHolderShape;
  final BoxShape shape;
  final BoxFit? fit;
  final BorderRadiusGeometry? borderRadius;
  final String? cacheKey;
  final Color? backgroundColor;

  const AppNetworkImage(this.imageUrl,
      {Key? key,
      this.width,
      this.height,
      this.errorWidget,
      this.placeHolderShape = BoxShape.rectangle,
      this.shape = BoxShape.rectangle,
      this.fit = BoxFit.cover,
      this.borderRadius,
      this.cacheKey,
      this.backgroundColor,
      })
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      decoration: BoxDecoration(
        shape: shape,
        borderRadius: borderRadius,
        color: backgroundColor
      ),
      clipBehavior: Clip.hardEdge,
      child: imageUrl.isNotEmpty
          ? CachedNetworkImage(
              imageUrl: imageUrl,
              cacheKey: cacheKey,
              fit: fit,
              errorWidget: (context, url, error) =>
                  errorWidget ??
                  Icon(
                    Icons.error,
                    size: height,
                    color: Colors.grey,
                  ),
              placeholder: (context, url) => SkeletonAvatar(
                style: SkeletonAvatarStyle(
                  shape: placeHolderShape,
                ),
              ),
            )
          : errorWidget ??
              Icon(
                Icons.error,
                size: height,
                color: Colors.grey,
              ),
    );
  }
}
