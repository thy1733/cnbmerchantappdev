import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';

abstract class PlaceholderImage {
  static Widget userName(String firstname, String lastname, {double fontSize=23}) {
    return Container(
      decoration: const BoxDecoration(
        color: AppColors.primaryLight,
        shape: BoxShape.circle,
      ),
      alignment: Alignment.center,
      child: Text(
        "${firstname.isEmpty ? "_" : firstname.characters.first}${lastname.isEmpty ? "_" : lastname.characters.first}",
        style: TextStyle(
          fontSize: fontSize,
          height: 1.15,
          color: AppColors.primary,
          fontFamily: AppFontName.familyFont,
          fontWeight: FontWeight.w700,
        ),
      ),
    );
  }
}
