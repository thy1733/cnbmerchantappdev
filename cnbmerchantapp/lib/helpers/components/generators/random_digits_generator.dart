import 'dart:math';

class RandomDigitsGenerator {
  // ignore: constant_identifier_names
  static const MAX_NUMERIC_DIGITS = 17;
  static final _random = Random();

  static int getInteger(int digitCount) {
    if (digitCount > MAX_NUMERIC_DIGITS || digitCount < 1) {
      throw RangeError.range(0, 1, MAX_NUMERIC_DIGITS, "Digit Count");
    }
    var digit = _random.nextInt(9) + 1; // first digit must not be a zero
    int n = digit;

    for (var i = 0; i < digitCount - 1; i++) {
      digit = _random.nextInt(10);
      n *= 10;
      n += digit;
    }
    return n;
  }

  static String getString(int digitCount) {
    String s = "";
    for (var i = 0; i < digitCount; i++) {
      s += _random.nextInt(10).toString();
    }
    return s;
  }
}
