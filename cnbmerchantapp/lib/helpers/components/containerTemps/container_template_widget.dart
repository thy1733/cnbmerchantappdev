import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
// ignore: must_be_immutable
class ContainerTemp extends StatefulWidget {
  ContainerTemp(
      {super.key,
      required this.header,
      required this.child,
      this.isTopSafe = true,
      this.isBottomSafe = false,
      this.isShowStatusbar =false,
      this.appShieldingText = "",
      this.resizeToAvoidBottomInset});
  final Widget header;
  final Widget child;
  late bool isTopSafe = true;
  late bool isBottomSafe = false;
  late bool isShowStatusbar = false;
  late String appShieldingText = "";
  bool? resizeToAvoidBottomInset;

  @override
  State<ContainerTemp> createState() => ContainerTempState();
}

class ContainerTempState extends State<ContainerTemp> {
  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      // Use [SystemUiOverlayStyle.light] for white status bar
      // or [SystemUiOverlayStyle.dark] for black status bar
      // https://stackoverflow.com/a/58132007/1321917
      value: SystemUiOverlayStyle.light,
      child: GestureDetector(
          onTap: () {
            FocusManager.instance.primaryFocus?.unfocus();
          },
          child: Scaffold(
            resizeToAvoidBottomInset: widget.resizeToAvoidBottomInset,
            body: Container(
                padding: EdgeInsets.zero,
                decoration: BoxDecoration(
                    color: AppColors.primary,
                    border: Border.all(color: AppColors.transparent, width: 0.0)),
                child: SafeArea(
                   top: widget.isTopSafe,
                  bottom: widget.isBottomSafe,
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        widget.header,
                        Expanded(
                            child: Container(
                          padding: EdgeInsets.zero,
                          clipBehavior: Clip.hardEdge,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: const BorderRadius.only(
                                  topLeft: Radius.circular(23),
                                  topRight: Radius.circular(23)),
                              border: Border.all(
                                  color: Colors.transparent, width: 0.0)),
                          child: widget.child,
                        ))
                      ])),
                // child: Column(
                //       crossAxisAlignment: CrossAxisAlignment.stretch,
                //       mainAxisAlignment: MainAxisAlignment.start,
                //       children: [
                //         widget.header,
                //         Expanded(
                //             child: Container(
                //           padding: EdgeInsets.zero,
                //           clipBehavior: Clip.hardEdge,
                //           decoration: BoxDecoration(
                //               color: Colors.white,
                //               borderRadius: const BorderRadius.only(
                //                   topLeft: Radius.circular(23),
                //                   topRight: Radius.circular(23)),
                //               border: Border.all(
                //                   color: Colors.transparent, width: 0.0)),
                //           child: widget.child,
                //         ))
                //       ]),
                )),
          ),
    );
  }
}
