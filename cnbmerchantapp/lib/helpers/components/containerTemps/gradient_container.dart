// ignore_for_file: prefer_const_constructors

import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';

Widget gradientContainer(
    {EdgeInsetsGeometry? padding,
    Widget? child,
    AlignmentGeometry begin = Alignment.bottomRight,
    AlignmentGeometry end = Alignment.topLeft}) {
  return Scaffold(
    body: Container(
      padding: padding,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: begin,
          end: end,
          colors: AppColors.primaryGradient,
        ),
      ),
      child: child,
    ),
  );
}

Widget gradientOnBoardContainer({Widget? child}) {
  return Container(
    decoration: BoxDecoration(
      gradient: const LinearGradient(
        begin: Alignment.topLeft,
        end: Alignment.bottomRight,
        colors: AppColors.primaryGradient,
      ),
    ),
    child: child,
  );
}
