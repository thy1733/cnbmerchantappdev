import 'package:cnbmerchantapp/helpers/colors/app_colors.dart';
import 'package:cnbmerchantapp/helpers/components/dottedLines/dotted_line.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ContainerDevider {
  static SizedBox get blankSpaceSm => SizedBox(height: 5.h, width: 5.w);
  static SizedBox get blankSpaceMd => SizedBox(height: 15.h, width: 15.w);
  static SizedBox get blankSpaceLg => SizedBox(height: 25.h, width: 25.w);

  static Widget vertical({
    Color color = const Color.fromRGBO(158, 158, 158, .1),
    double height = 1,
    EdgeInsets padding = const EdgeInsets.all(15),
  }) {
    return Padding(
      padding: padding.r,
      child: Container(
        color: color,
        height: 1.w,
      ),
    );
  }

  static Widget horizontal({
    Color color = const Color.fromRGBO(158, 158, 158, .1),
    double width = 1,
    EdgeInsets padding = const EdgeInsets.all(15),
  }) {
    return Padding(
      padding: padding.r,
      child: Container(
        color: color,
        width: 1.w,
      ),
    );
  }

   static const DottedLine dashHorizontal = DottedLine(
              direction: Axis.horizontal,
              lineThickness: 1.0,
              dashLength: 12,
              dashGapLength: 2,
              dashColor: AppColors.greyBackground,
            );

  static const dashVertical = DottedLine(
              direction: Axis.vertical,
              lineThickness: 1.0,
              dashLength: 12,
              dashGapLength: 2,
              dashColor: AppColors.greyBackground,
            );
}
