import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';

class ContainerSearchTemp extends StatefulWidget {
  const ContainerSearchTemp({super.key, required this.header, required this.child});
  final Widget header;
  final Widget child;

  @override
  State<ContainerSearchTemp> createState() => ContainerSearchTempState();
}

class ContainerSearchTempState extends State<ContainerSearchTemp> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          FocusManager.instance.primaryFocus?.unfocus();
        },
        child: Scaffold(
          body: Container(
          padding: EdgeInsets.zero,
           decoration: BoxDecoration(
            color: AppColors.white,
            border: Border.all(color: AppColors.transparent,width: 0.0)),
          child: SafeArea(
            top: true,
            bottom: false,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
            children: [
            widget.header, 
            widget.child
          ]),
        )),
    ));
  }
}
