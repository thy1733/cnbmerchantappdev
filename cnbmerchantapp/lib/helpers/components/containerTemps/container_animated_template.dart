import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ContainerAnimatedTemplate extends StatelessWidget {
  const ContainerAnimatedTemplate({
    super.key,
    required this.title,
    required this.child,
    this.floatingActionButton,
    this.appbarActions,
    this.padding,
    this.resizeToAvoidBottomInset = true,
    this.needBottomSpace = true,
    this.backButtonAction,
    this.hasBackButton = true,
  });
  final String title;

  /// should use scrollable widget
  final Widget child;

  final Widget? floatingActionButton;
  final List<Widget>? appbarActions;
  final EdgeInsets? padding;
  final bool resizeToAvoidBottomInset;
  final bool needBottomSpace;
  final OnBackButton? backButtonAction;
  final bool hasBackButton;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTapDown: (details) => FocusManager.instance.primaryFocus?.unfocus(),
      child: Material(
        child: Ink(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
                colors: [AppColors.primary, AppColors.white],
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                stops: [0.5, 0.5]),
          ),
          child: Column(
            children: [
              Expanded(
                child: NestedScrollView(
                  // physics: const BouncingScrollPhysics(),
                  headerSliverBuilder: (context, innerBoxIsScrolled) => [
                    SliverAppBar.medium(
                      leading: hasBackButton
                          ? BackButton(
                              onPressed: () => backButtonAction != null
                                  ? backButtonAction!()
                                  : Get.back(),
                            )
                          : null,
                      backgroundColor: AppColors.primary,
                      title: InkWell(
                        onTap: Get.back,
                        child: Text(
                          title,
                          style: AppTextStyle.header,
                        ),
                      ),
                      toolbarHeight: 45,
                      collapsedHeight: 45,
                      expandedHeight: 100,
                      iconTheme:
                          const IconThemeData(size: 22, color: AppColors.white),
                      actions: appbarActions,
                      excludeHeaderSemantics: true,
                      elevation: 0,
                      centerTitle: false,
                      floating: false,
                      pinned: true,
                    )
                  ],
                  body: Ink(
                    child: Container(
                      padding:
                          padding ?? const EdgeInsets.fromLTRB(15, 15, 15, 0),
                      clipBehavior: Clip.hardEdge,
                      decoration: const BoxDecoration(
                        borderRadius:
                            BorderRadius.vertical(top: Radius.circular(23)),
                        color: AppColors.white,
                      ),
                      child: child,
                    ),
                  ),
                ),
              ),
              floatingActionButton ?? const SizedBox(),
              if (needBottomSpace) ...[
                SizedBox(
                    height: MediaQuery.of(context).viewPadding.bottom > 0 ||
                            MediaQuery.of(context).viewInsets.bottom > 0
                        ? MediaQuery.of(context).padding.bottom
                        : 20),
              ],
              if (resizeToAvoidBottomInset && needBottomSpace) ...[
                SizedBox(
                  height: MediaQuery.of(context).viewInsets.bottom,
                ),
              ]
            ],
          ),
        ),
      ),
    );
  }
}
