import 'package:flutter/services.dart';

class AppTextInputRegx {
  static TextInputFormatter inputNameRegx =
      FilteringTextInputFormatter.allow(RegExp("[a-zA-Z0-9 ]"));

  static bool isUnicode(String input) {
    final unicodePattern = RegExp(r'[\u1780-\u17FF\u19E0-\u19FF]');
    return unicodePattern.hasMatch(input);
  }
}
