import 'package:flutter/material.dart';
import 'package:cnbmerchantapp/core.dart';

class KLoadingIndicator extends StatelessWidget {
  const KLoadingIndicator({Key? key, this.size}) : super(key: key);
  final Size? size;

  @override
  Widget build(BuildContext context) {
    var sizeHandle = size ?? MediaQuery.of(context).size;
    return Container(
        color: AppColors.white,
        height: sizeHandle.height * 0.5,
        width: sizeHandle.width,
        alignment: Alignment.center,
        child: const SizedBox(
          height: 40,
          width: 40,
          child: CircularProgressIndicator(
            color: AppColors.primary,
            backgroundColor: AppColors.grey,
          ),
        ));
  }
}
