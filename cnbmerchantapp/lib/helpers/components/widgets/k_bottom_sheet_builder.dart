import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

typedef OnKBottomSheetBuilderDismiss = Function();

class KBottomSheetBuilder extends StatelessWidget {
  const KBottomSheetBuilder(
      {Key? key,
      required this.child,
      this.needHeader = true,
      this.title = "",
      this.needIconDismiss = true,
      this.padding = 0,
      this.scrollPhysics = const AlwaysScrollableScrollPhysics(),
      this.onDismiss})
      : super(key: key);

  final bool needHeader;
  final bool needIconDismiss;

  final double padding;

  final String title;
  final Widget child;
  final ScrollPhysics scrollPhysics;
  final OnKBottomSheetBuilderDismiss? onDismiss;

  @override
  Widget build(BuildContext context) {
    return Material(
      color: AppColors.white,
      child: SingleChildScrollView(
        physics: scrollPhysics,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisSize: MainAxisSize.min,
          children: [
            Stack(
              children: [
                Padding(
                  padding: EdgeInsets.only(top: needHeader ? 60.0 : 0),
                  child: child,
                ),
                Positioned(
                  child: needHeader
                      ? _header(title: title, needIconDismiss: needIconDismiss)
                      : _noHeader(needIconDismiss: needIconDismiss),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget _noHeader({bool needIconDismiss = true}) {
    return Row(
      children: [
        Expanded(child: Container()),
        Visibility(
          visible: needIconDismiss,
          child: IconButton(
              padding: EdgeInsets.zero,
              onPressed: () {
                if (onDismiss != null) {
                  onDismiss!();
                } else {
                  Get.back();
                }
              },
              icon: const Icon(
                Icons.clear,
                color: Colors.grey,
              )),
        ),
      ],
    );
  }

  Widget _header({@required String? title, bool needIconDismiss = true}) {
    return Stack(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
                padding: const EdgeInsets.all(16),
                child: Text(
                  title!.tr,
                  style: AppTextStyle.header1,
                  textAlign: TextAlign.center,
                )),
          ],
        ),
        Visibility(
          visible: needIconDismiss,
          child: Positioned(
            top: 0,
            right: 0,
            child: IconButton(
                padding: EdgeInsets.zero,
                onPressed: () {
                  if (onDismiss != null) {
                    onDismiss!();
                  } else {
                    Get.back();
                  }
                },
                icon: const Icon(
                  Icons.clear,
                  color: AppColors.grey,
                )),
          ),
        )
      ],
    );
  }
}
