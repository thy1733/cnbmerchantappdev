import 'dart:async';

import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';

class AppSessionTimoutWidget extends StatefulWidget {
  /// if [onSessionEnd] null app will navigate to main page when session expired
  const AppSessionTimoutWidget(
      {super.key, required this.child, this.onSessionEnd});
  final Widget child;
  final VoidCallback? onSessionEnd;

  @override
  State<AppSessionTimoutWidget> createState() => _AppSessionTimoutWidgetState();
}

class _AppSessionTimoutWidgetState extends State<AppSessionTimoutWidget> {
  final _isSessionActive = true.obs;
  bool get isSessionActive => _isSessionActive.value;
  set isSessionActive(bool value) => _isSessionActive.value = value;

  late StreamSubscription<bool> keyboardSubscription;

  @override
  void initState() {
    super.initState();
    var keyboardVisibilityController = KeyboardVisibilityController();

    // Subscribe
    // this is use for listening when keyboard open/close
    keyboardSubscription = keyboardVisibilityController.onChange.listen((bool visible) {
      // visible mean keyboard open
      if(visible){
        AppSessionTimeoutInstant.pauseSession();
      }else{
        AppSessionTimeoutInstant.beginSession();
      }
    });
  }

  @override
  void dispose() {
    keyboardSubscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onPanDown: (details) {
        AppSessionTimeoutInstant.handleUserInteraction();
        isSessionActive = true;
      },
      child: widget.child,
    );
  }
}
