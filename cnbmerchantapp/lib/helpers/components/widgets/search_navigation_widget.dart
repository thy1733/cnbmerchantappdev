import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'dart:io' show Platform;

class SearchTopbarWidget extends StatefulWidget {
  const SearchTopbarWidget(
      {super.key,
      required this.title,
      this.onSubmited,
      this.onChanged,
      this.onEditingComplete,
      this.onClear});
  final String title;
  final SubmitCallback? onSubmited;
  final OnChangedCallback? onChanged;
  final OnEditingCompleteCallback? onEditingComplete;
  final OnClearCallback? onClear;

  @override
  State<SearchTopbarWidget> createState() => SearchTopbarWidgetState();
}

class SearchTopbarWidgetState extends State<SearchTopbarWidget> {
  final TextEditingController queryTextController = TextEditingController();
  FocusNode focusNode = FocusNode();

  void _onQueryChanged() {
    setState(() {
      // rebuild ourselves because query changed.
    });
  }

  @override
  void initState() {
    queryTextController.addListener(_onQueryChanged);
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    queryTextController.removeListener(_onQueryChanged);
    focusNode.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.only(left: 15, right: 15),
        child: Row(
          children: [
            InkWell(
                onTap: () => {Get.back()},
                child: Icon(
                    Platform.isAndroid
                        ? Icons.arrow_back
                        : Icons.arrow_back_ios_new,
                    size: 20,
                    color: AppColors.grey)),
            const SizedBox(
              width: 7,
            ),
            Expanded(
              flex: 6,
              child: Container(
                height: 40,
                margin: EdgeInsets.only(right: 15.w),
                decoration: BoxDecoration(
                  color: AppColors.grey.withOpacity(0.09),
                  borderRadius: const BorderRadius.all(
                    Radius.circular(100),
                  ),
                ),
                child: TextField(
                  controller: queryTextController,
                  focusNode: focusNode,
                  autofocus: true,
                  onSubmitted: (String _) =>
                      {if (widget.onSubmited != null) widget.onSubmited!(_)},
                  onChanged: (String _) =>
                      {if (widget.onChanged != null) widget.onChanged!(_)},
                  onEditingComplete: () {
                    widget.onEditingComplete;
                  },
                  textInputAction: TextInputAction.search,
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    isDense: true,
                    hintText: widget.title,
                    hintStyle: const TextStyle(fontSize: 14),
                    contentPadding: const EdgeInsets.only(top: 9),
                    prefixIcon: Icon(
                      Icons.search,
                      size: 24.w,
                      color: Colors.grey,
                    ),
                    suffixIcon: queryTextController.text.isEmpty
                        ? null
                        : InkWell(
                            onTap: () {
                              queryTextController.clear();
                              widget.onClear!(queryTextController.text);
                            },
                            child: const Icon(
                              Icons.cancel,
                              size: 16,
                              color: AppColors.grey,
                            ),
                          ),
                  ),
                ),
              ),
            ),
          ],
        ));
  }

  //#region Helper method

  void showResults(BuildContext context) {
    // _focusNode?.unfocus();
    // _currentBody = _SearchTransactionBody.results;
  }

  //#endregion
}
