import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';

class TabMenuComponent {
  ///
  /// !This Component create to use in horizontal list
  ///
  /// Assertion of this component
  /// You can see both of [title] and [child] are not required
  /// *** but this component need to have one of them and can use only one.
  ///
  ///
  /// [firstItem] If this param true componant will have round corner on top
  /// [lastItem] If this param true componant will have round corner on bottom
  Widget menuItem(
      {String? title,
      Color? titleColor,
      Color backgroundColor = const Color(0xFFF7F8F9),
      Widget? child,
      Widget? icon,
      IconData? iconData,
      bool permission = true,
      bool lastItem = false,
      bool firstItem = false,
      VoidCallback? onTap}) {
    // check null assertion
    if (child == null && title == null) {
      throw AssertionError('Failed assertion:  child != null || title != null');
    }
    if (child != null && title != null) {
      throw AssertionError(
          'Failed assertion:  you can\'t use child and tittle at the same time');
    }
    return !permission
        ? Container()
        : GestureDetector(
            onTap: onTap,
            child: Container(
              margin:
                  lastItem ? EdgeInsets.zero : const EdgeInsets.only(bottom: 1),
              padding: const EdgeInsets.all(15),
              decoration: BoxDecoration(
                color: backgroundColor,
                borderRadius: BorderRadius.vertical(
                    top: Radius.circular(firstItem ? 15 : 0),
                    bottom: Radius.circular(lastItem ? 15 : 0)),
              ),
              child: Row(
                children: [
                  Expanded(
                      child: child ??
                          Text(
                            '$title',
                            style: AppTextStyle.body2
                                .copyWith(color: titleColor ?? AppColors.black),
                          )),
                  icon ??
                      Icon(
                        iconData ?? Icons.navigate_next,
                        size: 20,
                        color: AppColors.primary,
                      )
                ],
              ),
            ),
          );
  }

  /// This create for default section header for menu tab
  Widget sectionTitle(String title) {
    return SizedBox(
        height: 50,
        child: Center(
          child: Text(
            title,
            textAlign: TextAlign.center,
            style: AppTextStyle.body1.copyWith(
              color: AppColors.textPrimary,
              fontWeight: FontWeight.w700,
            ),
          ),
        ));
  }
}
