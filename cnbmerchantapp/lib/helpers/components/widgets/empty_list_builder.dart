import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

class EmptyListBuilder extends StatelessWidget {
  const EmptyListBuilder(
      {super.key,
      required this.emptyImageAsset,
      required this.message,
      required this.description});

  final String emptyImageAsset;
  final String message;
  final String description;

  @override
  Widget build(BuildContext context) {
    final double size = Get.width;
    final double imageSize = 90.w;

    return SizedBox(
      height: size,
      width: size,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Image.asset(
            emptyImageAsset,
            width: imageSize,
            height: imageSize,
          ),
          SizedBox(
            height: 8.r,
          ),
          Text(
            message,
            textAlign:TextAlign.center,
            style: AppTextStyle.header1,
          ),
          Text(
            description,
            textAlign:TextAlign.center,
            style: AppTextStyle.label2,
          )
        ],
      ),
    );
  }
}
