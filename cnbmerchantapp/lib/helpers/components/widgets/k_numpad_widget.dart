import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';

typedef KNumpadTapCallback = void Function(String text);

// migrate with calculator_widget
class KNumpadWidget extends StatelessWidget {
  final KNumpadTapCallback onPressed;
  final VoidCallback? onBackSpacePressed;

  /// *** position on of [Clear button] and [Point button] is same place
  /// so if consider to use [onClearPress] then [Point button] not show
  final VoidCallback? onClearPress;
  final VoidCallback? onClearLongPress;
  final double? fontSize;
  final FontWeight? fontWeight;
  final Color? textColor;
  final bool isDecimal;

  const KNumpadWidget({
    Key? key,
    required this.onPressed,
    this.fontSize,
    this.fontWeight,
    this.textColor,
    this.onBackSpacePressed,
    this.onClearPress,
    this.onClearLongPress,
    this.isDecimal = true,
  }) : super(key: key);

  TextStyle _textFontStyle() {
    return TextStyle(
      color: textColor ?? AppColors.black,
      fontSize: fontSize ?? 30,
      fontFamily: AppFontName.familyFont,
      fontWeight: fontWeight ?? FontWeight.normal,
    );
  }

  ButtonStyle _numpadBtnStyle() {
    return ButtonStyle(
      shadowColor: MaterialStateProperty.all<Color>(Colors.transparent),
      foregroundColor: MaterialStateProperty.all<Color>(Colors.transparent),
      overlayColor:
          MaterialStateProperty.all(AppColors.primary.withOpacity(0.05)),
      backgroundColor: MaterialStateProperty.all<Color>(Colors.transparent),
      animationDuration: const Duration(milliseconds: 400),
    );
  }

  Widget _button(String value, {void Function()? onPressed}) {
    return Container(
      clipBehavior: Clip.hardEdge,
      decoration: const BoxDecoration(shape: BoxShape.circle),
      child: ElevatedButton(
          onPressed: onPressed ?? () => this.onPressed(value),
          style: _numpadBtnStyle(),
          child: Text(value,
              textAlign: TextAlign.center, style: _textFontStyle())),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Expanded(
            child: Row(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Expanded(child: _button("1")),
            Expanded(child: _button("2")),
            Expanded(child: _button("3")),
          ],
        )),
        Expanded(
            child: Row(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Expanded(child: _button("4")),
            Expanded(child: _button("5")),
            Expanded(child: _button("6")),
          ],
        )),
        Expanded(
            child: Row(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Expanded(child: _button("7")),
            Expanded(child: _button("8")),
            Expanded(child: _button("9")),
          ],
        )),
        Expanded(
            child: Row(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Expanded(
              child: onClearPress != null
                  ? _button(
                      "C",
                      onPressed: onClearPress!,
                    )
                  : isDecimal
                      ? _button(
                          ValueConst.point,
                          onPressed: () {
                            onPressed(ValueConst.point);
                          },
                        )
                      : const SizedBox(),
            ),
            Expanded(child: _button("0")),
            Expanded(
                child: Container(
                    clipBehavior: Clip.hardEdge,
                    decoration: const BoxDecoration(shape: BoxShape.circle),
                    child: ElevatedButton(
                        onPressed: onBackSpacePressed ?? () {},
                        onLongPress: onClearLongPress,
                        style: _numpadBtnStyle(),
                        child: Icon(
                          MerchantIcon.backspace2,
                          size: fontSize ?? 30,
                          color: textColor ?? AppColors.black,
                        ))))
          ],
        ))
      ],
    );
  }
}
