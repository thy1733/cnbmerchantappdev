import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';

class ErrorNetworkWidget extends StatelessWidget {
  const ErrorNetworkWidget(
      {super.key,
      this.assetName = JsonAppAssets.noInternetConnection,
      this.title = "NoInternetConnection",
      this.description = "NoInternetConnectionDetails",
      this.needRetryButton = true,
      this.onRetryPressed});

  final String assetName;
  final String title;
  final String description;
  final bool needRetryButton;
  final VoidCallback? onRetryPressed;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Lottie.asset(assetName, height: 220, width: 220),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 30),
          child: Column(
            children: [
              Visibility(
                visible: title.isNotEmpty,
                child: Text(
                  title.tr,
                  textAlign: TextAlign.center,
                  style: AppTextStyle.headline6
                      .copyWith(color: AppColors.textPrimary),
                ),
              ),
              Text(
                description.tr,
                textAlign: TextAlign.center,
                style: AppTextStyle.caption
                    .copyWith(color: AppColors.textSecondary),
              ),
            ],
          ),
        ),
        Visibility(
            visible: needRetryButton,
            child: ElevatedButton(
              onPressed: onRetryPressed,
              child: Text(
                "Retry".tr,
              ),
            )),
      ],
    );
  }
}
