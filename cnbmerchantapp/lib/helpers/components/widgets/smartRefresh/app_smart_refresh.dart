import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:get/get.dart';

class AppSmartRefresh {
  static AppSmartRefresh shared = AppSmartRefresh();
  final header = const WaterDropMaterialHeader(
    color: AppColors.white,
    distance: 30,
    offset: -10,
    backgroundColor: AppColors.primary,
  );

  Widget get footer => ClassicFooter(
        loadingText: 'Loading'.tr,
        loadingIcon: const SizedBox(
            height: 20,
            width: 20,
            child: CircularProgressIndicator(
                strokeWidth: 2,
                color: AppColors.primary,
                backgroundColor: AppColors.secondary)),
        canLoadingText: 'ReleaseToLoadMore'.tr,
        canLoadingIcon: const Icon(MerchantIcon.refresh,
            size: 15, color: AppColors.secondary),
        idleText: 'PullUpToLoadMore'.tr,
        loadStyle: LoadStyle.ShowWhenLoading,
        noDataText: 'NoData'.tr,
      );
}
