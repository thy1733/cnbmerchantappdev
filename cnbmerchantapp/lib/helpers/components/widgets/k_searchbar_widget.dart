import 'dart:ui';

import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get_utils/src/extensions/internacionalization.dart';

kSearchbar(
    {
    String? hint,
    Function(String?)? onChange,
    Function(String?)? onSubmit,
    BoxBorder? border,
    GestureTapCallback? onTap,
    VoidCallback? onEditingComplete,
    Color? color,
    required List<BoxShadow> boxShadow}) {
  return Container(
    padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
    height: 50,
    decoration: BoxDecoration(
      border: border,
      color: color ?? AppColors.grey.withOpacity(0.09),
      borderRadius: const BorderRadius.all(Radius.circular(30)),
    ),
    child: Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        const Icon(
          MerchantIcon.search,
          size: 23,
          color: AppColors.black,
        ),
        const SizedBox(
          width: 10,
          height: 0,
        ),
        Expanded(
          child: FormBuilderTextField(
              onSubmitted: onSubmit,
              onChanged: onChange,
              autofocus: false,
              onTap: onTap,
              onEditingComplete: onEditingComplete,
              selectionHeightStyle: BoxHeightStyle.includeLineSpacingBottom,
              keyboardType: TextInputType.text,
              scrollPadding: const EdgeInsets.all(0),
              name: hint ?? 'Search'.tr,
              decoration: InputDecoration(
                border: InputBorder.none,
                contentPadding: EdgeInsets.zero,
                hintText: hint ?? 'Search'.tr,
              )),
        ),
      ],
    ),
  );
}
