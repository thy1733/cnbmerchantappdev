import 'package:flutter/material.dart';
import 'package:cnbmerchantapp/core.dart';

class KRadioButtonWidget extends StatelessWidget {
  const KRadioButtonWidget({
    super.key,
    required this.radioValue,
    required this.radioGroupValue,
    this.text,
    this.child,
    required this.value,
    this.onChange,
  });

  final String? text;
  final dynamic value;
  final dynamic radioGroupValue;
  final dynamic radioValue;
  final Widget? child;
  final ValueChanged<dynamic>? onChange;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => onChange!(value),
      child: Row(
        children: [
          SizedBox(
            width: 20,
            child: Radio(
              value: radioValue,
              groupValue: radioGroupValue,
              activeColor: AppColors.primary,
              onChanged: (val) => onChange!(val),
            ),
          ),
          const SizedBox(
            width: 8,
          ),
          child ?? Text('${text ?? value}')
        ],
      ),
    );
  }
}
