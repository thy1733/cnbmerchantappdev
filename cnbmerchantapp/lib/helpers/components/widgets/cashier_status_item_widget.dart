import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CashierStatusItemWidget extends StatelessWidget {
  const CashierStatusItemWidget({super.key, required this.status});

  final CashierStatus status;

  @override
  Widget build(BuildContext context) {
    Widget child = Container();

    switch (status) {
      case CashierStatus.active:
        child = KStatusWidget(
          name: "Active".tr,
          color: AppColors.success,
        );

        break;
      case CashierStatus.inactive:
        child = KStatusWidget(
          name: "Deactivate".tr,
          color: AppColors.grey,
        );

        break;
      case CashierStatus.pending:
        child = KStatusWidget(
          name: "Pending".tr,
          color: AppColors.primary,
        );

        break;
      case CashierStatus.deleted:
        child = KStatusWidget(
          name: "Deleted".tr,
          color: AppColors.grey,
        );

        break;
      case CashierStatus.renew:
        child = KStatusWidget(
          name: "Active".tr,
          color: AppColors.success,
        );
        break;
    }
    return child;
  }
}
