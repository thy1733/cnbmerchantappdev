import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import '../../../core.dart';

class CircleImageUploadView extends StatelessWidget {
  const CircleImageUploadView(
      {super.key,
      required this.imageFile,
      required this.onImagePicked,
      required this.placeHolder,
      this.imageUrl,
      this.cacheKey});

  final Uint8List? imageFile;
  final String? imageUrl;
  final String placeHolder;
  final Function onImagePicked;
  final String? cacheKey;

  @override
  Widget build(BuildContext context) {
    final double wid = Get.width;
    final double size = wid / 4.5;
    final double iconSize = size / 3.5;
    final double radius = 90.r;

    return Container(
      decoration: BoxDecoration(
          border: Border.all(color: AppColors.greyishDisable),
          color: AppColors.white,
          borderRadius: BorderRadius.circular(radius)),
      width: size,
      height: size,
      child: InkWell(
        onTap: () {
          onImagePicked();
        },
        child: ClipRRect(
          borderRadius: BorderRadius.circular(radius),
          child: Stack(
            alignment: Alignment.center,
            children: <Widget>[
              (imageFile != null && imageFile!.isNotEmpty)
                  ? Image.memory(
                      imageFile!,
                      width: size,
                      height: size,
                      fit: BoxFit.cover,
                    )
                  : (imageUrl != null && imageUrl!.isNotEmpty)
                      ? AppNetworkImage(imageUrl!,
                          cacheKey: cacheKey ??
                              DateTime.now()
                                  .toFormatDateString("yyyymmddhhmmsss"),
                          width: size,
                          height: size,
                          errorWidget:
                              SvgPicture.asset(SvgAppAssets.pOutletDetail))
                      : SvgPicture.asset(
                          placeHolder,
                          fit: BoxFit.cover,
                          width: iconSize,
                          height: iconSize,
                        ),
              Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                    height: size / 5,
                    width: size,
                    color: AppColors.primary,
                    child: const Icon(
                      MerchantIcon.camera2,
                      color: Colors.white,
                      size: 20,
                    ),
                  ))
            ],
          ),
        ),
      ),
    );
  }
}
