import 'package:flutter/material.dart';
import 'package:get/get.dart';

enum KStatusEnum {
  none,
  pending,
  requested,
  approved,
  rejected,
  active,
  deactive,
}

class KStatusWidget extends StatelessWidget {
  const KStatusWidget({Key? key, this.status, this.name, this.color})
      : super(key: key);
  final KStatusEnum? status;
  final String? name;
  final Color? color;

  @override
  Widget build(BuildContext context) {
    return _builder();
  }

  Widget _builder() {
    Widget result = _statusWidget(name: name, color: color);
    if (status != null) {
      switch (status) {
        case KStatusEnum.pending:
          result = _statusWidget(
              name: KStatusEnum.pending.name.tr, color: Colors.red);
          break;
        case KStatusEnum.requested:
          result = _statusWidget(
              name: KStatusEnum.requested.name.tr, color: Colors.grey);
          break;
        case KStatusEnum.approved:
          result = _statusWidget(
              name: KStatusEnum.approved.name.tr,
              color: const Color(0xFF4CB97A));
          break;
        case KStatusEnum.rejected:
          result = _statusWidget(
              name: KStatusEnum.rejected.name.tr, color: Colors.black);
          break;
        case KStatusEnum.active:
          result = _statusWidget(
              name: KStatusEnum.active.name.tr, color: const Color(0xFF4CB97A));
          break;
        case KStatusEnum.deactive:
          result = _statusWidget(
              name: KStatusEnum.deactive.name.tr,
              color: const Color(0xFF9D9D9D));
          break;
        default:
      }
    }
    return result;
  }

  Widget _statusWidget({String? name, Color? color}) {
    return Container(
      decoration:
          BoxDecoration(color: color, borderRadius: BorderRadius.circular(15)),
      padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 7),
      child: Text(
        name ?? "",
        style: const TextStyle(
            color: Colors.white,
            fontSize: 11,
            height: 1,
            fontWeight: FontWeight.w400),
      ),
    );
  }
}
