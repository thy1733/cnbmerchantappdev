import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'dart:io' show Platform;

class NavigationTitle extends StatefulWidget {
  const NavigationTitle({super.key, required this.title, this.backButton});
  final String title;
  final OnBackButton? backButton;

  @override
  State<NavigationTitle> createState() => NavigationTitleState();
}

class NavigationTitleState extends State<NavigationTitle> {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        InkWell(
            onTap: () {
              widget.backButton != null ? widget.backButton!() : Get.back();
            },
            child: Padding(
                padding: const EdgeInsets.only(
                    left: 10, right: 10, bottom: 10, top: 0),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Icon(
                          Platform.isAndroid
                              ? Icons.arrow_back
                              : Icons.arrow_back_ios_new,
                          size: 20,
                          color: AppColors.white),
                      const SizedBox(
                        height: 5,
                      ),
                      Padding(
                          padding:
                              const EdgeInsets.only(left: 5, top: 5, bottom: 5),
                          child: Text(
                            widget.title,
                            textAlign: TextAlign.start,
                            style: AppTextStyle.header,
                          ))
                    ]))),
        // Expanded(child: Container())
      ],
    );
  }
}
