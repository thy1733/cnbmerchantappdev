import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';

class KWebViewWidget extends StatelessWidget {
  final String url;
  final Function()? onStart;
  final Function()? onStop;
  final Function()? onError;
  final NavigationActionPolicy Function(String url)? onUrlTapped;
  final void Function(InAppWebViewController controller)? onWebViewCreated;

  const KWebViewWidget({
    super.key,
    required this.url,
    this.onStart,
    this.onStop,
    this.onError,
    this.onUrlTapped,
    this.onWebViewCreated,
  });

  @override
  Widget build(BuildContext context) {
    return InAppWebView(
      initialUrlRequest: URLRequest(url: Uri.parse(url)),
      onWebViewCreated: onWebViewCreated,
      gestureRecognizers: {
        Factory<OneSequenceGestureRecognizer>(
          () => EagerGestureRecognizer(),
        ),
      },
      initialOptions: InAppWebViewGroupOptions(
        crossPlatform: InAppWebViewOptions(
            supportZoom: false,
            preferredContentMode: UserPreferredContentMode.MOBILE,
            disableHorizontalScroll: true,
            horizontalScrollBarEnabled: false,
            useShouldOverrideUrlLoading: true),
      ),
      onLoadStart: (controller, url) {
        if (onStart != null) onStart!();
      },
      onLoadStop: (controller, url) {
        if (onStop != null) onStop!();
      },
      onLoadError: (controller, url, code, message) {
        if (onError != null) onError!();
      },
      shouldOverrideUrlLoading: (controller, navigationAction) async {
        final url = navigationAction.request.url.toString();
        return onUrlTapped != null
            ? onUrlTapped!(url)
            : NavigationActionPolicy.ALLOW;
      },
    );
  }
}

class KLocalWebViewWidget extends StatelessWidget {
  const KLocalWebViewWidget({
    super.key,
    required this.fileUrl,
    this.onStop,
    this.onError,
  });

  final String fileUrl;
  final Function()? onStop;
  final Function()? onError;

  @override
  Widget build(BuildContext context) {
    return InAppWebView(
        initialFile: fileUrl,
        gestureRecognizers: {
          Factory<OneSequenceGestureRecognizer>(
            () => EagerGestureRecognizer(),
          ),
        },
        initialOptions: InAppWebViewGroupOptions(
          crossPlatform: InAppWebViewOptions(
              supportZoom: false,
              preferredContentMode: UserPreferredContentMode.MOBILE,
              disableHorizontalScroll: true,
              horizontalScrollBarEnabled: false,
              useShouldOverrideUrlLoading: true),
        ),
        onLoadStop: (controller, url) {
          if (onStop != null) onStop!();
        },
        onLoadError: (controller, url, code, message) {
          if (onError != null) onError!();
        });
  }
}
