import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

class IOSDatePicker extends StatelessWidget {
  const IOSDatePicker({
    super.key,
    required this.initialDateTime,
    this.minimumDate,
    this.maximumDate,
    required this.onDonePress,
  });

  final DateTime initialDateTime;
  final DateTime? maximumDate;
  final DateTime? minimumDate;
  final Function(DateTime? pickedDate) onDonePress;

  @override
  Widget build(BuildContext context) {
    DateTime? pickedDate;
    return CupertinoTheme(
      data: CupertinoThemeData(
        textTheme: CupertinoTextThemeData(
          primaryColor: AppColors.primary,
          dateTimePickerTextStyle: TextStyle(
              fontSize: 16,
              color: AppColors.textPrimary,
              fontFamily: Get.locale?.languageCode == LanguageOption.lanKhmer
                  ? GoogleFonts.notoSansKhmer().fontFamily
                  : GoogleFonts.roboto().fontFamily),
        ),
      ),
      child: SizedBox(
        height: 300,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          mainAxisSize: MainAxisSize.min,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                InkWell(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                      vertical: 15,
                      horizontal: 25,
                    ),
                    child: Text(
                      "Cancel".tr,
                      style: AppTextStyle.body2
                          .copyWith(color: AppColors.darkGrey),
                    ),
                  ),
                  onTap: () => Get.back(),
                ),
                InkWell(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                      vertical: 15,
                      horizontal: 25,
                    ),
                    child: Text(
                      "Done".tr,
                      style:
                          AppTextStyle.body2.copyWith(color: AppColors.primary),
                    ),
                  ),
                  onTap: () {
                    onDonePress(pickedDate);
                    Get.back();
                  },
                ),
              ],
            ),
            const Divider(
              height: 0,
              thickness: 1,
            ),
            Expanded(
              child: CupertinoDatePicker(
                  initialDateTime: initialDateTime,
                  maximumDate: maximumDate,
                  minimumDate: minimumDate,
                  mode: CupertinoDatePickerMode.date,
                  onDateTimeChanged: (date) {
                    SystemSound.play(SystemSoundType.alert);
                    pickedDate = date;
                  }),
            ),
          ],
        ),
      ),
    );
  }
}
