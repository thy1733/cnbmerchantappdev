import 'package:convex_bottom_bar/convex_bottom_bar.dart';
import 'package:flutter/material.dart';

class Style extends StyleHook {
  @override
  double get activeIconSize => 27;

  @override
  double get activeIconMargin => 0;

  @override
  double get iconSize => 24;
  
  @override
  TextStyle textStyle(Color color, String? fontFamily) {
     return TextStyle(color: color, fontSize: 10, fontFamily: fontFamily);
  }
  
  // @override
  // TextStyle textStyle(Color color) {
  //   return TextStyle(color: color, fontSize: 10);
  // }
 
}
