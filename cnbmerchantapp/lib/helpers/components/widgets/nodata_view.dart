import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class NoDataView extends StatelessWidget {
  final String message;
  final String description;

  /// [imageAset] only support for image typ svg file in local asset
  final String? imageAset;
  const NoDataView(
      {Key? key, required this.message, this.description = "", this.imageAset})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Column(children: [
      Expanded(child: Container()),
      SvgPicture.asset(
        imageAset ?? SvgAppAssets.noData,
        height: 120,
        fit: BoxFit.contain,
      ),
      const SizedBox(
        height: 10,
      ),
      Text(
        message,
        style: const TextStyle(
            fontSize: 14, fontWeight: FontWeight.bold, color: AppColors.black),
      ),
      const SizedBox(
        height: 5,
      ),
      Text(
        description,
        style: const TextStyle(
            fontSize: 13,
            fontWeight: FontWeight.normal,
            color: Color.fromRGBO(123, 123, 123, 1)),
        textAlign: TextAlign.center,
      ),
      Expanded(flex: 2, child: Container()),
    ]));
  }
}
