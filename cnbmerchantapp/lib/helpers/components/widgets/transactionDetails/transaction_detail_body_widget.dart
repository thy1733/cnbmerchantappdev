import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class TransactionDetailBodyWidget extends StatelessWidget {
  const TransactionDetailBodyWidget({
    super.key,
    required this.transactionId,
    required this.outletName,
    required this.paymentProcessedByTitle,
    required this.paymentProcessedBy,
    required this.paymentMethodName,
    required this.customerName,
    required this.customerBankName,
    required this.customerAccount,
    required this.bakongHash,
    required this.amount,
    required this.currency,
    required this.remark,
    required this.items,
    required this.onItemPressed,
  });

  final String transactionId;
  final String outletName;
  final String paymentProcessedByTitle;
  final String paymentProcessedBy;
  final String paymentMethodName;
  final String customerName;
  final String customerBankName;
  final String customerAccount;
  final String bakongHash;
  final double amount;
  final String currency;
  final String remark;
  final List<TransactionRefundItemListOutputModel> items;
  final Function(TransactionRefundItemListOutputModel item)? onItemPressed;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          height: 30,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                "TransactionId".tr,
                textAlign: TextAlign.start,
                style: AppTextStyle.body2.copyWith(
                  color: AppColors.textSecondary,
                ),
              ),
              Expanded(child: Container()),
              Text(
                transactionId,
                textAlign: TextAlign.start,
                style: AppTextStyle.body2.copyWith(
                  color: AppColors.textPrimary,
                ),
              ),
            ],
          ),
        ),
        SizedBox(
          height: 30,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                'BakongHash'.tr,
                textAlign: TextAlign.start,
                style: AppTextStyle.body2.copyWith(
                  color: AppColors.textSecondary,
                ),
              ),
              Expanded(child: Container()),
              Text(
                bakongHash,
                textAlign: TextAlign.start,
                style: AppTextStyle.body2.copyWith(
                  color: AppColors.textPrimary,
                ),
              )
            ],
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Outlet".tr,
              textAlign: TextAlign.start,
              style: AppTextStyle.body2.copyWith(
                color: AppColors.textSecondary,
              ),
            ),
            Flexible(
              fit: FlexFit.tight,
              child: Text(
                outletName,
                textAlign: TextAlign.end,
                overflow: TextOverflow.visible,
                style: AppTextStyle.body2.copyWith(
                  color: AppColors.textPrimary,
                ),
              ),
            )
          ],
        ),
        const Padding(
          padding: EdgeInsets.symmetric(vertical: 10),
          child: DottedLine(
            dashColor: AppColors.greyishHint,
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(paymentProcessedByTitle.tr,
                textAlign: TextAlign.start,
                style: AppTextStyle.body2.copyWith(
                  color: AppColors.textSecondary,
                )),
            Flexible(
              fit: FlexFit.tight,
              child: Text(
                paymentProcessedBy.isEmpty
                    ? "N/A"
                    : paymentProcessedBy,
                textAlign: TextAlign.end,
                overflow: TextOverflow.visible,
                style: AppTextStyle.body2.copyWith(
                  color: AppColors.textPrimary,
                ),
              ),
            ),
          ],
        ),
        SizedBox(
          height: 30,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                "PaymentMethod".tr,
                textAlign: TextAlign.start,
                style: AppTextStyle.body2.copyWith(
                  color: AppColors.textSecondary,
                ),
              ),
              Expanded(child: Container()),
              Text(
                paymentMethodName,
                textAlign: TextAlign.start,
                style: AppTextStyle.body2.copyWith(
                  color: AppColors.textPrimary,
                ),
              )
            ],
          ),
        ),
        const Padding(
          padding: EdgeInsets.symmetric(vertical: 10),
          child: DottedLine(
            dashColor: AppColors.greyishHint,
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 7),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "CustomerName".tr,
                textAlign: TextAlign.start,
                style: AppTextStyle.body2.copyWith(
                  color: AppColors.textSecondary,
                ),
              ),
              ContainerDevider.blankSpaceMd,
              Expanded(
                child: Text(
                  customerName,
                  textAlign: TextAlign.end,
                  style: AppTextStyle.body2.copyWith(
                    color: AppColors.textPrimary,
                  ),
                ),
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 4),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "CustomerBankName".tr,
                textAlign: TextAlign.start,
                style: AppTextStyle.body2.copyWith(
                  color: AppColors.textSecondary,
                ),
              ),
              // Expanded(child: Container()),
              ContainerDevider.blankSpaceMd,
              Expanded(
                child: Text(
                  customerBankName,
                  textAlign: TextAlign.end,
                  style: AppTextStyle.body2.copyWith(
                    color: AppColors.textPrimary,
                  ),
                ),
              ),
            ],
          ),
        ),
        SizedBox(
          height: 30,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                "CustomerAccount".tr,
                textAlign: TextAlign.start,
                style: AppTextStyle.body2.copyWith(
                  color: AppColors.textSecondary,
                ),
              ),
              Expanded(child: Container()),
              Text(
                 customerAccount.maskAccountNumber(),
                textAlign: TextAlign.start,
                style: AppTextStyle.body2.copyWith(
                  color: AppColors.textPrimary,
                ),
              )
            ],
          ),
        ),
        const Padding(
          padding: EdgeInsets.symmetric(vertical: 10),
          child: DottedLine(
            dashColor: AppColors.greyishHint,
          ),
        ),
        SizedBox(
          height: 30,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                'TransactionAmount'.tr,
                textAlign: TextAlign.start,
                style: AppTextStyle.body2.copyWith(
                  color: AppColors.textSecondary,
                ),
              ),
              Expanded(child: Container()),
              Text(
                "${amount.toThousandSeparatorString(d2: isUSDCurrency)} $currency"
                    .toUpperCase(),
                textAlign: TextAlign.start,
                style: AppTextStyle.body2.copyWith(
                  color: AppColors.textPrimary,
                ),
              ),
            ],
          ),
        ),
        Visibility(
          visible: remark.isNotEmpty,
          child: Column(
            children: [
              const Padding(
                padding: EdgeInsets.symmetric(vertical: 10),
                child: DottedLine(
                  dashColor: AppColors.greyishHint,
                ),
              ),
              SizedBox(
                height: 30,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text("Note".tr,
                        textAlign: TextAlign.start,
                        style: AppTextStyle.body2.copyWith(
                          color: AppColors.textSecondary,
                        )),
                    const SizedBox(
                      width: 8,
                    ),
                    Expanded(
                        child: Text(
                      remark,
                      textAlign: TextAlign.right,
                    ))
                  ],
                ),
              ),
            ],
          ),
        ),
        RefundHistoryWidget(
            items: items, currency: currency, onItemPressed: onItemPressed)
      ],
    );
  }
}
