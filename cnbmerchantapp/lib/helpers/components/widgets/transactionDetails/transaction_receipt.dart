import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

class TransactionReceipt extends StatelessWidget {
  const TransactionReceipt(
      {super.key,
      required this.transactionId,
      required this.outletName,
      required this.paymentProcessedByTitle,
      required this.paymentProcessedBy,
      required this.paymentMethodName,
      required this.customerName,
      required this.customerBankName,
      required this.customerAccount,
      required this.approvalCode,
      required this.amount,
      required this.currency,
      required this.remark,
      required this.transactionType,
      required this.transactionStatus,
      required this.transactionDate});

  final String transactionId;
  final String outletName;
  final String paymentProcessedByTitle;
  final String paymentProcessedBy;
  final String paymentMethodName;
  final String customerName;
  final String customerBankName;
  final String customerAccount;
  final String approvalCode;
  final double amount;
  final String currency;
  final String remark;
  final int transactionType;
  final int transactionStatus;
  final DateTime transactionDate;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(20)),
          color: AppColors.white),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(15, 15, 15, 0),
            child: TransactionDetailTopWidget(
                transactionType: transactionType,
                transactionStatus: transactionStatus,
                transactionDate: transactionDate),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 10),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SvgPicture.asset(
                  SvgAppAssets.cutLeft,
                  width: 10,
                  height: 10,
                  fit: BoxFit.contain,
                ),
                const Expanded(
                  child: Padding(
                    padding: EdgeInsets.only(left: 2),
                    child: DottedLine(
                      dashColor: AppColors.black,
                    ),
                  ),
                ),
                SvgPicture.asset(
                  SvgAppAssets.cutRight,
                  width: 10,
                  height: 10,
                  fit: BoxFit.contain,
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(
                  height: 30,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        "TransactionId".tr,
                        textAlign: TextAlign.start,
                        style: AppTextStyle.body2.copyWith(
                          color: AppColors.textSecondary,
                        ),
                      ),
                      Expanded(child: Container()),
                      Text(
                        transactionId,
                        textAlign: TextAlign.start,
                        style: AppTextStyle.body2.copyWith(
                          color: AppColors.textPrimary,
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 30,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Outlet".tr,
                        textAlign: TextAlign.start,
                        style: AppTextStyle.body2.copyWith(
                          color: AppColors.textSecondary,
                        ),
                      ),
                      Flexible(
                        fit: FlexFit.tight,
                        child: Text(
                          outletName,
                          textAlign: TextAlign.end,
                          overflow: TextOverflow.visible,
                          style: AppTextStyle.body2.copyWith(
                            color: AppColors.textPrimary,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 30,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(paymentProcessedByTitle.tr,
                          textAlign: TextAlign.start,
                          style: AppTextStyle.body2.copyWith(
                            color: AppColors.textSecondary,
                          )),
                      Flexible(
                        fit: FlexFit.tight,
                        child: Text(
                          paymentProcessedBy,
                          textAlign: TextAlign.end,
                          overflow: TextOverflow.visible,
                          style: AppTextStyle.body2.copyWith(
                            color: AppColors.textPrimary,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 30,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        "PaymentMethod".tr,
                        textAlign: TextAlign.start,
                        style: AppTextStyle.body2.copyWith(
                          color: AppColors.textSecondary,
                        ),
                      ),
                      Expanded(child: Container()),
                      Text(
                        paymentMethodName,
                        textAlign: TextAlign.start,
                        style: AppTextStyle.body2.copyWith(
                          color: AppColors.textPrimary,
                        ),
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: 30,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        "CustomerName".tr,
                        textAlign: TextAlign.start,
                        style: AppTextStyle.body2.copyWith(
                          color: AppColors.textSecondary,
                        ),
                      ),
                      Expanded(child: Container()),
                      Text(
                        customerName,
                        textAlign: TextAlign.start,
                        style: AppTextStyle.body2.copyWith(
                          color: AppColors.textPrimary,
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 30,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        "CustomerBankName".tr,
                        textAlign: TextAlign.start,
                        style: AppTextStyle.body2.copyWith(
                          color: AppColors.textSecondary,
                        ),
                      ),
                      Expanded(child: Container()),
                      Text(
                        customerBankName,
                        textAlign: TextAlign.start,
                        style: AppTextStyle.body2.copyWith(
                          color: AppColors.textPrimary,
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 30,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        "CustomerAccount".tr,
                        textAlign: TextAlign.start,
                        style: AppTextStyle.body2.copyWith(
                          color: AppColors.textSecondary,
                        ),
                      ),
                      Expanded(child: Container()),
                      Text(
                        customerAccount,
                        textAlign: TextAlign.start,
                        style: AppTextStyle.body2.copyWith(
                          color: AppColors.textPrimary,
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 30,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        'ApprovalCode'.tr,
                        textAlign: TextAlign.start,
                        style: AppTextStyle.body2.copyWith(
                          color: AppColors.textSecondary,
                        ),
                      ),
                      Expanded(child: Container()),
                      Text(
                        approvalCode,
                        textAlign: TextAlign.start,
                        style: AppTextStyle.body2.copyWith(
                          color: AppColors.textPrimary,
                        ),
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: 30,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        'TransactionAmount'.tr,
                        textAlign: TextAlign.start,
                        style: AppTextStyle.body2.copyWith(
                          color: AppColors.textSecondary,
                        ),
                      ),
                      Expanded(child: Container()),
                      Text(
                        "${amount.toThousandSeparatorString(d2: isUSDCurrency)} $currency"
                            .toUpperCase(),
                        textAlign: TextAlign.start,
                        style: AppTextStyle.body2.copyWith(
                          color: AppColors.textPrimary,
                        ),
                      ),
                    ],
                  ),
                ),
                Visibility(
                  visible: remark.isNotEmpty,
                  child: SizedBox(
                    height: 30,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text("Note".tr,
                            textAlign: TextAlign.start,
                            style: AppTextStyle.body2.copyWith(
                              color: AppColors.textSecondary,
                            )),
                        const SizedBox(
                          width: 8,
                        ),
                        Expanded(
                            child: Text(
                          remark,
                          textAlign: TextAlign.right,
                          style: AppTextStyle.body2
                              .copyWith(color: AppColors.textPrimary),
                        ))
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          const Padding(
            padding: EdgeInsets.only(top: 12),
            child: DottedLine(
              dashColor: AppColors.black,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(12),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Expanded(child: Container()),
                Padding(
                  padding: const EdgeInsets.only(right: 8),
                  child: SvgPicture.asset(
                    SvgAppAssets.logo,
                    width: 40,
                    height: 40,
                    fit: BoxFit.contain,
                  ),
                ),
                Text.rich(
                  TextSpan(
                    style: AppTextStyle.headline6,
                    children: const [
                      TextSpan(
                          text: "CANA ",
                          style: TextStyle(color: AppColors.yellowBrand)),
                      TextSpan(
                          text: "MERCHANT",
                          style: TextStyle(color: AppColors.primary)),
                    ],
                  ),
                ),
                Expanded(child: Container()),
              ],
            ),
          )
        ],
      ),
    );
  }
}
