import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class RefundHistoryWidget extends StatelessWidget {
  const RefundHistoryWidget({
    super.key,
    required this.items,
    required this.currency,
    this.onItemPressed,
  });

  final List<TransactionRefundItemListOutputModel> items;
  final String currency;
  final Function(TransactionRefundItemListOutputModel)? onItemPressed;

  @override
  Widget build(BuildContext context) {
    return Visibility(
      visible: items.isNotEmpty,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(
            height: 20,
          ),
          Text(
            "RefundHistory".tr,
            textAlign: TextAlign.start,
            style: AppTextStyle.body2.copyWith(
              color: AppColors.textPrimary,
              fontWeight: FontWeight.w700,
            ),
          ),
          ListView.builder(
            padding: const EdgeInsets.only(top: 10),
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            itemCount: items.length,
            itemBuilder: (context, index) {
              var item = items[index];
              return TextButton(
                onPressed: () =>
                    onItemPressed != null ? onItemPressed!(item) : () {},
                child: Column(
                  children: [
                    Row(
                      children: [
                        Text(
                          kDateTimeFormat(item.refundDate),
                          style: AppTextStyle.body2.copyWith(
                            color: AppColors.textPrimary,
                          ),
                        ),
                        Expanded(child: Container()),
                        Text(
                            '-${item.amount.toThousandSeparatorString(d2: isUSDCurrency)} $currency'
                                .toUpperCase(),
                            style: AppTextStyle.body2.copyWith(
                              color: AppColors.primaryDark,
                            )),
                        const SizedBox(
                          width: 10,
                        ),
                        const Icon(
                          Icons.navigate_next,
                          color: AppColors.primaryDark,
                          size: 20,
                        )
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 8),
                      child: Divider(
                        color: AppColors.black.withOpacity(0.2),
                      ),
                    )
                  ],
                ),
              );
            },
          ),
        ],
      ),
    );
  }
}
