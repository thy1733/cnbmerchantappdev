import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

class TransactionDetailTopWidget extends StatelessWidget {
  const TransactionDetailTopWidget({
    super.key,
    required this.transactionType,
    required this.transactionStatus,
    required this.transactionDate,
    this.statusColor = AppColors.textPrimary,
  });

  final int transactionType;
  final int transactionStatus;
  final DateTime transactionDate;
  final Color statusColor;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          decoration: BoxDecoration(
              color: statusColor.withOpacity(0.1), shape: BoxShape.circle),
          width: 44.w,
          height: 44.w,
          child: Icon(
            MerchantIcon.transactionItem,
            color: statusColor,
            size: 20,
          ),
        ),
        const SizedBox(
          width: 10,
        ),
        Expanded(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                TransactionTypeStatus.values[transactionStatus].displayTitle.tr,
                textAlign: TextAlign.start,
                style:
                    AppTextStyle.body1.copyWith(color: AppColors.textPrimary),
              ),
              Text(
                kDateTimeFormat(transactionDate),
                textAlign: TextAlign.start,
                style: AppTextStyle.body2.copyWith(
                  color: AppColors.textSecondary,
                ),
              ),
            ],
          ),
        )
      ],
    );
  }
}

class TransactionDetailTypeTopWidget extends StatelessWidget {
  const TransactionDetailTypeTopWidget({
    super.key,
    required this.transactionType,
    required this.transactionDate,
    this.statusColor = AppColors.textPrimary,
  });

  final String transactionType;
  final DateTime transactionDate;
  final Color statusColor;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          decoration: BoxDecoration(
              color: statusColor.withOpacity(0.1), shape: BoxShape.circle),
          width: 44.w,
          height: 44.w,
          child: Icon(
            MerchantIcon.transactionItem,
            color: statusColor,
            size: 20,
          ),
        ),
        const SizedBox(
          width: 10,
        ),
        Expanded(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                transactionType.tr,
                textAlign: TextAlign.start,
                style:
                    AppTextStyle.body1.copyWith(color: AppColors.textPrimary),
              ),
              Text(
                kDateTimeFormat(transactionDate),
                textAlign: TextAlign.start,
                style: AppTextStyle.body2.copyWith(
                  color: AppColors.textSecondary,
                ),
              ),
            ],
          ),
        )
      ],
    );
  }
}
