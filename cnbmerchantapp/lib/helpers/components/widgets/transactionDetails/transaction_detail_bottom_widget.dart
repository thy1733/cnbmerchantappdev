import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class TransactionDetailBottomWidget extends StatelessWidget {
  const TransactionDetailBottomWidget({
    super.key,
    required this.showRefundButton,
    required this.amountTitle,
    required this.amount,
    required this.currency,
    required this.onRefundPressed,
    required this.onSharePressed,
    this.statusColor = AppColors.textPrimary,
    required this.allowRefundDuration,
  });

  final bool showRefundButton;
  final String amountTitle;
  final double amount;
  final String currency;
  final VoidCallback? onRefundPressed;
  final VoidCallback? onSharePressed;
  final Color statusColor;
  final int allowRefundDuration;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(
        left: 15,
        top: 16,
        right: 0,
        bottom: hasSafeArea(context) ? getBottomSafeArea(context) : 16,
      ),
      decoration: BoxDecoration(
        color: AppColors.white,
        borderRadius: const BorderRadius.only(
          topLeft: Radius.circular(15),
          topRight: Radius.circular(15),
        ),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.3),
            spreadRadius: 5,
            blurRadius: 25,
            offset: const Offset(0, 3), // changes position of shadow
          ),
        ],
      ),
      child: Column(
        children: [
          Visibility(
            visible: showRefundButton,
            child: Padding(
              padding: const EdgeInsets.only(bottom: 16),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const Text(
                    "ⓘ  ",
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  Text(
                    "AllowedRefundDuration"
                        .tr
                        .replaceAll("${0}", allowRefundDuration.toString())
                        .replaceAll("${1}", "Hours".tr.toLowerCase()),
                    style: AppTextStyle.overline
                        .copyWith(color: AppColors.textSecondary),
                  ),
                ],
              ),
            ),
          ),
          Row(
            children: [
              Row(
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        amountTitle,
                        textAlign: TextAlign.end,
                        style: AppTextStyle.caption.copyWith(
                          color: AppColors.textSecondary,
                        ),
                      ),
                      Text(
                        "${amount.toThousandSeparatorString(d2: isUSDCurrency)} $currency"
                            .toUpperCase(),
                        textAlign: TextAlign.start,
                        style: AppTextStyle.headline6.copyWith(
                          color: statusColor,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                    ],
                  )
                ],
              ),
              Expanded(
                child: Container(),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  showRefundButton
                      ? RefundTransactionButton(onPressed: onRefundPressed)
                      : const SizedBox.shrink(),
                  ShareTransactionReceiptButton(
                    onPressed: onSharePressed,
                  ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}
