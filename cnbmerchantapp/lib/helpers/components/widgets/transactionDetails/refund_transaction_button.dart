import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class RefundTransactionButton extends StatelessWidget {
  const RefundTransactionButton({super.key, required this.onPressed});
  final VoidCallback? onPressed;

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all(Colors.transparent),
          overlayColor:
              MaterialStateProperty.all(AppColors.primary.withOpacity(0.1)),
          elevation: MaterialStateProperty.all(0)),
      onPressed: onPressed,
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 4),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              height: 40,
              width: 40,
              decoration: const BoxDecoration(
                color: AppColors.primaryLight,
                shape: BoxShape.circle,
              ),
              child: const Center(
                child: Icon(
                  MerchantIcon.refresh,
                  size: 15,
                  color: AppColors.primary,
                ),
              ),
            ),
            const SizedBox(
              height: 2,
            ),
            Text(
              "Refund".tr,
              textAlign: TextAlign.center,
              style: AppTextStyle.body2.copyWith(
                color: AppColors.black,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
