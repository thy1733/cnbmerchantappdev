import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'k_error_message_snack_bar.dart';
import 'k_network_connection_snack_bar.dart';

class CSnackBar {
  static GetSnackBar appGetSnackBar(
      bool isSuccess, String title, String message, int duration,
      {DismissDirection direction = DismissDirection.down,
      SnackBarBehavior behavior = SnackBarBehavior.fixed}) {
    return GetSnackBar(
        title: "Title",
        message: "message",
        backgroundColor: AppColors.white,
        margin: const EdgeInsets.all(20),
        borderRadius: 15,
        animationDuration: const Duration(milliseconds: 200),
        boxShadows: const [
          BoxShadow(
              color: Color(0x20000000), blurRadius: 10.2, spreadRadius: 2.5),
        ],
        duration: Duration(seconds: duration),
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
        icon: isSuccess
            ? const Icon(
                Icons.check_circle_outline_rounded,
                color: AppColors.success,
              )
            : const Icon(
                Icons.warning_amber_rounded,
                color: AppColors.fail,
              ),
        titleText: Text(
          title,
          style: TextStyle(
              fontWeight: FontWeight.bold,
              color: isSuccess ? AppColors.success : AppColors.fail),
        ),
        messageText: Text(
          message,
          maxLines: 10,
          overflow: TextOverflow.ellipsis,
          textAlign: TextAlign.start,
          style: const TextStyle(color: AppColors.black),
        ));
  }

  Future<GetSnackBar> networkConnectState(bool isSuccess, String title,
      String message, Color stateColor, int duration,
      {bool showProgress = false,
      DismissDirection direction = DismissDirection.down,
      SnackBarBehavior behavior = SnackBarBehavior.fixed}) async {
    return GetSnackBar(
        padding: const EdgeInsets.all(0.0),
        dismissDirection: direction,
        isDismissible: isSuccess,
        duration: Duration(seconds: duration),
        backgroundColor: AppColors.grey.withOpacity(0.2),
        snackPosition: SnackPosition.BOTTOM,
        snackStyle: SnackStyle.GROUNDED,
        userInputForm: Form(
            child: KNetworkConnectionSnackBar(
          showProgress: showProgress,
          netState: isSuccess,
          title: title,
          message: message,
          stateColor: stateColor,
        )));
  }

  Future<GetSnackBar> errorMessageState(bool isSuccess, String title,
      String message, Color stateColor, int duration,
      {bool showProgress = false,
      DismissDirection direction = DismissDirection.down,
      SnackBarBehavior behavior = SnackBarBehavior.fixed}) async {
    return GetSnackBar(
        padding: const EdgeInsets.all(0.0),
        dismissDirection: direction,
        isDismissible: isSuccess,
        duration: Duration(seconds: duration),
        backgroundColor: AppColors.black.withOpacity(0.3),
        snackPosition: SnackPosition.TOP,
        snackStyle: SnackStyle.GROUNDED,
        userInputForm: Form(
            child: KErrorMessageSnackBar(
          showProgress: showProgress,
          netState: isSuccess,
          title: title,
          message: message,
          stateColor: stateColor,
        )));
  }

  static void showSnackBarTop({required String message}) {
    Get.showSnackbar(GetSnackBar(
      snackPosition: SnackPosition.TOP,
      borderRadius: 32,
      isDismissible: true,
      margin: const EdgeInsets.symmetric(horizontal: 16),
      animationDuration: const Duration(milliseconds: 400),
      duration: const Duration(seconds: 2),
      backgroundColor: AppColors.darkGrey,
      messageText: Center(
        child: Text(
          message,
          style: AppTextStyle.body2,
        ),
      ),
    ));
  }
}
