import 'package:cnbmerchantapp/core.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

class KNetworkConnectionSnackBar extends StatelessWidget {
  final bool netState;
  final String title;
  final String message;
  final ConnectivityResult connSate;
  final Color stateColor;
  final bool showProgress;

  const KNetworkConnectionSnackBar(
      {Key? key,
      this.netState = true,
      this.title = '',
      this.message = '',
      this.connSate = ConnectivityResult.none,
      this.stateColor = Colors.amber,
      this.showProgress = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    FocusManager.instance.primaryFocus?.unfocus();

    return Scaffold(
        backgroundColor: Colors.transparent,
        body: Column(
          children: [
            Expanded(child: Container()),
            Container(
              height: 75,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: stateColor,
              ),
              padding: const EdgeInsets.all(15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  showProgress
                      ? Lottie.asset(
                          JsonAppAssets.wifiRetry,
                          width: 30,
                          height: 30,
                          repeat: true,
                        )
                      : netState
                          ? const Icon(
                              Icons.wifi,
                              color: AppColors.white,
                            )
                          : const Icon(
                              Icons.wifi_off_outlined,
                              color: AppColors.white,
                            ),
                  const SizedBox(
                    width: 10,
                  ),
                  Expanded(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          title,
                          style: const TextStyle(
                              fontWeight: FontWeight.bold,
                              color: AppColors.white),
                        ),
                        Text(
                          message,
                          maxLines: 2,
                          overflow: TextOverflow.clip,
                          textAlign: TextAlign.start,
                          style: TextStyle(
                              color: AppColors.white,
                              fontSize:
                                  MediaQuery.of(context).size.height <= 568
                                      ? 9
                                      : 10),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ));
  }
}
