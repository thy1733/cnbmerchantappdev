import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get_utils/src/extensions/internacionalization.dart';

import 'package:cnbmerchantapp/core.dart';

class AppButtonStyle {
  Widget bubble(
      {VoidCallback? onTab,
      required IconData iconData,
      double? iconSize,
      String? label,
      Color? color,
      Color? labelColor}) {
    return InkWell(
      onTap: onTab,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
              height: 45,
              width: 45,
              decoration: BoxDecoration(
                  shape: BoxShape.circle, color: color ?? AppColors.primary),
              clipBehavior: Clip.hardEdge,
              child: Icon(
                iconData,
                color: AppColors.white,
                size: iconSize ?? 18,
              )),
          const SizedBox(
            height: 10,
          ),
          Visibility(
              visible: label != null,
              child: Text(
                label ?? '',
                style: AppTextStyle.caption,
              ))
        ],
      ),
    );
  }

  static Widget primaryButton(
      {required String label,
      required VoidCallback onPressed,
      Widget? child,
      bool isLoading = false,
      bool isEnable = true,
      Color textColor = AppColors.white,
      Color color = AppColors.primary,
      EdgeInsetsGeometry margin = EdgeInsets.zero}) {
    return Padding(
      padding: margin,
      child: ElevatedButton(
        onPressed: isEnable && !isLoading ? onPressed : null,
        style: isEnable && !isLoading
            ? kPrimaryButtonStyle(color: color)
            : kDisableButtonStyle(),
        child: isLoading
            ? Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  SizedBox(
                    height: 15,
                    width: 15,
                    child: CircularProgressIndicator(
                      strokeWidth: 1.0,
                      color: AppColors.darkGrey,
                      backgroundColor: AppColors.grey.withOpacity(0.6),
                    ),
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                  Text("PleaseWait".tr,
                      style: const TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                          color: AppColors.darkGrey))
                ],
              )
            : child ??
                Text(
                  label,
                  style: TextStyle(
                      fontSize: 14.sp,
                      fontWeight: FontWeight.w400,
                      color: textColor),
                ),
      ),
    );
  }

  static Widget secondaryButton(
      {required String label,
      required VoidCallback onPressed,
      Widget? child,
      bool isLoading = false,
      bool isEnable = true,
      EdgeInsetsGeometry margin = EdgeInsets.zero}) {
    return Padding(
      padding: margin,
      child: ElevatedButton(
        onPressed: isEnable && !isLoading ? onPressed : null,
        style: isEnable && !isLoading
            ? kSecondaryButtonExpandStyle()
            : kDisableButtonStyle(),
        child: isLoading
            ? Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  SizedBox(
                    height: 15,
                    width: 15,
                    child: CircularProgressIndicator(
                      strokeWidth: 1.0,
                      color: AppColors.darkGrey,
                      backgroundColor: AppColors.grey.withOpacity(0.6),
                    ),
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                  Text("PleaseWait".tr,
                      style: const TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                          color: AppColors.darkGrey))
                ],
              )
            : child ??
                Text(label,
                    style: TextStyle(
                        fontSize: 14.sp,
                        fontWeight: FontWeight.w400,
                        color: AppColors.primary)),
      ),
    );
  }

  static Widget primaryOutlinedButton({
    required String label,
    required VoidCallback onPressed,
    bool isLoading = false,
    bool isEnable = true,
    Color textColor = AppColors.primary,
    Color borderColor = AppColors.primary,
    EdgeInsetsGeometry margin = EdgeInsets.zero,
  }) {
    return Padding(
      padding: margin,
      child: ElevatedButton(
        onPressed: isEnable && !isLoading ? onPressed : null,
        style: isEnable && !isLoading
            ? kOutlinedButtonExpandStyle(borderColor: borderColor)
            : kOutlinedButtonExpandStyle(borderColor: AppColors.greyishDisable),
        child: isLoading
            ? Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  SizedBox(
                    height: 15,
                    width: 15,
                    child: CircularProgressIndicator(
                      strokeWidth: 1.0,
                      color: AppColors.darkGrey,
                      backgroundColor: AppColors.grey.withOpacity(0.6),
                    ),
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                  Text(
                    "PleaseWait".tr,
                    style: TextStyle(
                      fontSize: 14.sp,
                      fontWeight: FontWeight.w400,
                      color: AppColors.darkGrey,
                    ),
                  )
                ],
              )
            : Text(
                label,
                style: TextStyle(
                    fontSize: 14.sp,
                    fontWeight: FontWeight.w400,
                    color: (isEnable && !isLoading)
                        ? textColor
                        : AppColors.greyishDisable),
              ),
      ),
    );
  }

  ButtonStyle btnBottomStyle = ButtonStyle(
      shadowColor: MaterialStateProperty.all<Color>(Colors.transparent),
      foregroundColor: MaterialStateProperty.all<Color>(AppColors.white),
      backgroundColor: MaterialStateProperty.all<Color>(AppColors.primary),
      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
          RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(25.0),
              side: const BorderSide(color: AppColors.primary))));

  static ButtonStyle kPrimaryButtonStyle({
    double height = 45,
    double width = double.infinity,
    Color color = AppColors.primary,
  }) {
    return ButtonStyle(
        backgroundColor: MaterialStateProperty.all(color),
        minimumSize: MaterialStateProperty.all(Size(width, height)),
        shape: MaterialStateProperty.all(RoundedRectangleBorder(
            side: BorderSide.none, borderRadius: BorderRadius.circular(25))),
        overlayColor:
            MaterialStateProperty.all(AppColors.white.withOpacity(0.2)),
        elevation: MaterialStateProperty.all(0));
  }

  static kDisableButtonStyle(
      {double height = 45, double width = double.infinity}) {
    return ButtonStyle(
        backgroundColor: MaterialStateProperty.all(const Color(0xFFE6E6E6)),
        minimumSize: MaterialStateProperty.all(Size(width, height)),
        shape: MaterialStateProperty.all(RoundedRectangleBorder(
            side: BorderSide.none, borderRadius: BorderRadius.circular(25))),
        elevation: MaterialStateProperty.all(0));
  }

  static kSecondaryButtonExpandStyle(
      {double height = 45, double width = double.infinity}) {
    return ButtonStyle(
        minimumSize: MaterialStateProperty.all(Size(width, height)),
        shape: MaterialStateProperty.all(RoundedRectangleBorder(
            side: BorderSide.none, borderRadius: BorderRadius.circular(25))),
        backgroundColor: MaterialStateProperty.all(AppColors.white),
        overlayColor:
            MaterialStateProperty.all(AppColors.primary.withOpacity(0.1)),
        elevation: MaterialStateProperty.all(0));
  }

  static ButtonStyle kOutlinedButtonExpandStyle({
    double height = 45,
    double width = double.infinity,
    Color borderColor = AppColors.primary,
  }) {
    return ButtonStyle(
      minimumSize: MaterialStateProperty.all(Size(width, height)),
      shape: MaterialStateProperty.all(
        RoundedRectangleBorder(
          side: BorderSide(
            width: 1,
            color: borderColor,
          ),
          borderRadius: BorderRadius.circular(25),
        ),
      ),
      backgroundColor: MaterialStateProperty.all(AppColors.white),
      overlayColor: MaterialStateProperty.all(
        AppColors.primary.withOpacity(0.1),
      ),
      elevation: MaterialStateProperty.all(0),
    );
  }

  static ButtonStyle kSecondaryOutlinedButtonExpandStyle({
    double height = 45,
    double width = double.infinity,
  }) {
    return ButtonStyle(
      minimumSize: MaterialStateProperty.all(Size(width, height)),
      shape: MaterialStateProperty.all(
        RoundedRectangleBorder(
          side: const BorderSide(width: 1, color: Color(0xFFE6E6E6)),
          borderRadius: BorderRadius.circular(25),
        ),
      ),
      textStyle: MaterialStateProperty.all(
        TextStyle(
          fontWeight: FontWeight.w400,
          fontSize: AppFontSize.buttonLabel,
          color: AppColors.textPrimary,
        ),
      ),
      backgroundColor: MaterialStateProperty.all(AppColors.white),
      overlayColor: MaterialStateProperty.all(
        AppColors.primary.withOpacity(0.1),
      ),
      elevation: MaterialStateProperty.all(0),
    );
  }

  static ButtonStyle kSecondaryButtonStyle(
      {BorderRadius borderRadius = BorderRadius.zero,
      Color? backgroundColor,
      Color? textColor}) {
    return ButtonStyle(
        shape: MaterialStatePropertyAll(
          RoundedRectangleBorder(
            borderRadius: borderRadius,
          ),
        ),
        backgroundColor:
            MaterialStateProperty.all(backgroundColor ?? AppColors.white),
        overlayColor:
            MaterialStateProperty.all(AppColors.primary.withOpacity(0.1)),
        elevation: MaterialStateProperty.all(0));
  }

  static kPrimaryElevateButtonTheme() {
    return ElevatedButtonThemeData(
        style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all(AppColors.primary),
            overlayColor:
                MaterialStateProperty.all(AppColors.primary.withOpacity(0.2)),
            elevation: MaterialStateProperty.all(0)));
  }
}
