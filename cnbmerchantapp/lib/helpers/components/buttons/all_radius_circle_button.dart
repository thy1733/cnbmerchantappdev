import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';

class ButtonCircleRadius extends StatelessWidget {
  const ButtonCircleRadius(
      {Key? key, required this.title, this.height = 50, required this.submit})
      : super(key: key);

  final String title;
  final double height;
  final SubmitCallback submit;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      padding: EdgeInsets.zero,
      clipBehavior: Clip.hardEdge,
      decoration: const BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(23))),
      child: ElevatedButton(
          clipBehavior: Clip.hardEdge,
          style: ButtonStyle(
            padding: MaterialStateProperty.all(EdgeInsets.zero),
            backgroundColor: MaterialStateProperty.all(AppColors.primary),
            overlayColor:
                MaterialStateProperty.all(AppColors.white.withOpacity(0.5)),
            shadowColor: MaterialStateProperty.all(AppColors.primary),
          ),
          onPressed: () => submit(""),
          child: Center(
            child: Text(
              title,
              style: const TextStyle(
                fontSize: 17,
                fontWeight: FontWeight.w600,
                color: AppColors.white,
              ),
            ),
          )),
    );
  }
}
