import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:cnbmerchantapp/core.dart';

class CloseBtn extends StatelessWidget {
  const CloseBtn({
    Key? key,
    this.backgroundColor,
    this.iconColor,
    this.iconSize,
  }) : super(key: key);
  final Color? backgroundColor;
  final Color? iconColor;
  final double? iconSize;
  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: () {
          Get.back();
        },
        child: Container(
          padding: const EdgeInsets.all(20),
          decoration: BoxDecoration(
            color: backgroundColor,
            shape: BoxShape.circle,
          ),
          child: Icon(
            MerchantIcon.x,
            size: iconSize ?? 13,
            color: iconColor ?? const Color(0xFF9D9D9D),
          ),
        ));
  }
}

class CloseBottomSheetButton extends StatelessWidget {
  const CloseBottomSheetButton({super.key, this.onPressed});

  final VoidCallback? onPressed;

  @override
  Widget build(BuildContext context) {
    return Material(
      shape: const CircleBorder(),
      clipBehavior: Clip.hardEdge,
      color: AppColors.transparent,
      child: CloseButton(
        color: Colors.grey,
        onPressed: onPressed,
      ),
    );
  }
}
