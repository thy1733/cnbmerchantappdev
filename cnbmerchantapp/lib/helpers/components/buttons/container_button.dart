import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class ContainerButton extends StatelessWidget {
  const ContainerButton({
    Key? key,
    required this.child,
    required this.submit,
    this.isEnable = true,
  }) : super(key: key);

  final Widget child;
  final SubmitCallback submit;
  final bool isEnable;
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 7.5, bottom: 7.5),
      padding: EdgeInsets.zero,
      clipBehavior: Clip.hardEdge,
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.all(
          Radius.circular(7),
        ),
      ),
      child: ElevatedButton(
          clipBehavior: Clip.hardEdge,
          style: ButtonStyle(
            padding: MaterialStateProperty.all(EdgeInsets.zero),
            backgroundColor:
                MaterialStateProperty.all(AppColors.textFieldBackground),
            overlayColor: MaterialStateProperty.all(isEnable? AppColors.listSeparator:Colors.transparent),
            shadowColor: MaterialStateProperty.all(AppColors.listSeparator),
          ),
          onPressed: () {
            if(isEnable) {
              submit("");
            }
          },
          child: child),
    );
  }
}
