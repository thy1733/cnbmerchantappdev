import 'package:cnbmerchantapp/core.dart';
 
bool get isUSDCurrency =>
    AppBusinessTempInstant.selectedBusiness.accountCurrency.toUpperCase() == "USD";
