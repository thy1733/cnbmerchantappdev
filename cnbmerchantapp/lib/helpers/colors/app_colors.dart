import 'package:flutter/material.dart';

class AppColors {
  static const Color primaryDark = Color(0xFFD61229);
  static const Color primary = Color(0xFFCD202A);
  static const Color primaryLight = Color(0xFFFFE4DB);
  static const Color secondary = Color(0xFFEBE961);
  static const Color yellowBrand = Color(0xFFFFD055);
  static const Color darkYellow = Color(0xFFF0AE00);
  static const Color success = Color(0xFF4CB97A);
  static const Color fail = Color(0xFFEF3A36);
  static const Color black = Color(0xFF000000);
  static const Color white = Color(0xFFFFFFFF);
  static const Color lightWhite = Color(0xFBFBFBFB);
  static const Color grey = Colors.grey;
  static const Color darkGrey = Color(0xFF3A3A3A);
  static const Color darkBlue = Color(0xFF142D54);
  static const Color greyishHint = Color(0xFF9D9D9D);
  static const Color greyishDisable = Color(0xFFE6E6E6);
  static const Color greyBackground = Color(0x66E6E6E6);
  static Color barrierBackground = const Color(0xFF000000).withOpacity(0.5);
  static Color listSeparator = Colors.grey.withOpacity(0.2);
  static const List<Color> primaryGradient = [
    Color(0xFFF05F49),
    Color(0xFFCA1229),
  ];
  static const List<Color> originalGradient = [
    Color(0xFFEF766C),
    Color(0xFFCA1229),
  ];
  static const Color textPrimary = Color.fromRGBO(58, 58, 58, 1);
  static const Color textSecondary = Color.fromRGBO(157, 157, 157, 1);
  static const Color textFieldBackground = Color(0xFFF7F8F9);
  static const Color transparent = Colors.transparent;
  static const Color searchBackground = Color(0xFFF2F2F2);
  static const Color blue = Colors.blue;
}
