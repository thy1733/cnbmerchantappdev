import 'package:cnbmerchantapp/helpers/extensions/string_extensions.dart';
import 'package:flutter/material.dart';
import 'package:get/get_utils/src/extensions/internacionalization.dart';
import 'package:intl/intl.dart';

//***
// use to format amount in QR screen
// */
String thousandSeparatQR(String value) {
  String result = '';
  String formatDigti = "#,##0.##";
  bool isDotEnd = false;

  if (value.contains('.')) {
    if (value.substring(value.indexOf('.')).length > 2) {
      formatDigti = "#,##0.00";
    } else if (value.substring(value.indexOf('.')).length > 1) {
      formatDigti = "#,##0.0#";
    } else {
      isDotEnd = true;
    }
  }

  NumberFormat numberFormat = NumberFormat(formatDigti, "en_US");
  value = value.toSafeDoubleAsString();
  result = numberFormat.format(double.parse(value));
  return isDotEnd ? '$result.' : result;
}

// String thousandSeparat(String value, {bool d2 = false}) {
//   String result = '';
//   String formatDigti = d2 ? "#,##0.00" : "#,##0.##";

//   NumberFormat numberFormat = NumberFormat(formatDigti, "en_US");
//   value = value.toSafeDoubleAsString();
//   result = numberFormat.format(double.parse(value));
//   return result;
// }

TextEditingValue maxValueTextInputFormatter(
    TextEditingValue oldValue, TextEditingValue newValue, double maxValue) {
  // newValue of type TextEditingValue can't override so I declare newValueText for help
  String newValueText = newValue.text;

  // #region  comma to period convert
  ///**
  /// on iOS InputType number don't have period sign it only have comma
  /// -> so I decide to use comma and use this block of code to convert it to period sign
  /// */
  if (newValueText.contains(',')) {
    newValueText = newValueText.replaceAll(',', '.');
  }
  // #endregion

  if (newValueText.indexOf('.') != newValueText.lastIndexOf('.')) {
    return oldValue;
  }

  // #region decimal number lenght limit after comma
  ///***
  /// we don't want to allow use to input decimal number over 2 digit
  /// -> we use this block of code to track it
  /// */

  // #region check for multiperiod
  ///***
  /// if user input more than one period is not allow
  /// */
  if (newValueText.contains('.')) {
    if (newValueText.substring(newValueText.indexOf('.')).length > 3) {
      return oldValue;
    }
  }
  // #endregion

  // #endregion

  //for display
  String valueString =
      thousandSeparatQR(newValueText.isEmpty ? '0' : newValueText);

  //for usage
  double newAmount = newValueText.isEmpty ? 0 : double.parse(newValueText);

  // #region check for over limit refund

  if (newAmount > maxValue) {
    double test = double.parse('$maxValue');
    valueString = thousandSeparatQR('$test');
  }
  // #endregion

  return TextEditingValue(
      text: valueString,
      selection: TextSelection.collapsed(offset: valueString.length));
}

TextEditingValue maxIntegerValueTextInputFormatter(
    TextEditingValue oldValue, TextEditingValue newValue, int maxValue) {
  // newValue of type TextEditingValue can't override so I declare newValueText for help
  String newValueText = newValue.text;

  //for display
  String valueString =
      thousandSeparatQR(newValueText.isEmpty ? '0' : newValueText);

  //for usage
  int newAmount = newValueText.isEmpty ? 0 : int.parse(newValueText);

  // #region check for over limit refund

  if (newAmount > maxValue) {
    int test = int.parse('$maxValue');
    valueString = thousandSeparatQR('$test');
  }
  // #endregion

  debugPrint("***Value String: $valueString");

  return TextEditingValue(
      text: valueString,
      selection: TextSelection.collapsed(offset: valueString.length));
}

String formatMMSS(int seconds) {
  seconds = (seconds % 3600).truncate();
  int minutes = (seconds / 60).truncate();

  String minutesStr = (minutes).toString().padLeft(minutes < 10 ? 1 : 2, '0');
  String secondsStr = (seconds % 60).toString().padLeft(2, '0');

  return "$minutesStr:$secondsStr";
}

String convertSecondToMinute(int seconds) {
  var extend = 'Minutes'.tr.toLowerCase();
  int number = 0;
  if (seconds < 60) {
    extend = 'Seconds'.tr.toLowerCase();
    number = seconds;
  } else {
    if (seconds == 60) {
      extend = 'Minute'.tr.toLowerCase();
    }
    number = seconds ~/ 60;
  }

  return '$number $extend';
}

String formatedInviteCodeString(String value) {
  var bufferString = StringBuffer();
  for (int i = 0; i < value.length; i++) {
    bufferString.write(value[i]);
    var nonZeroIndexValue = i + 1;
    if (nonZeroIndexValue % 3 == 0 && nonZeroIndexValue != value.length) {
      bufferString.write(' ');
    }
  }

  return bufferString.toString();
}
