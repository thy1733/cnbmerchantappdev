import 'package:cnbmerchantapp/helpers/enums/xcore.dart';

class CurrencyConverter {
  static String currencyToSymble(CurrencyEnum value) {
    var result = '';
    switch (value) {
      case CurrencyEnum.khr:
        result = '៛';
        break;
      case CurrencyEnum.usd:
        result = '\$';
        break;
      default:
        result = '-';
        break;
    }
    return result;
  }

  static String currencyStringToSymble(String value) {
    var result = '';
    switch (value.toLowerCase()) {
      case 'khr':
        result = '៛';
        break;
      case 'usd':
        result = '\$';
        break;
      default:
        result = '-';
        break;
    }
    return result;
  }
}
