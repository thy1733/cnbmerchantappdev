// ignore_for_file: deprecated_member_use

import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'dart:ui' as ui;

Future<Uint8List> createImageFromWidget(Widget widget, Size size) async {
  final RenderRepaintBoundary repaintBoundary = RenderRepaintBoundary();
  final RenderView renderView = RenderView(
    child: RenderPositionedBox(
        alignment: Alignment.center, child: repaintBoundary),
    configuration: ViewConfiguration(
        size: size, devicePixelRatio: ui.window.devicePixelRatio),
    view: ui.window,
  );

  final PipelineOwner pipelineOwner = PipelineOwner()..rootNode = renderView;
  renderView.prepareInitialFrame();

  final BuildOwner buildOwner = BuildOwner(focusManager: FocusManager());
  final RenderObjectToWidgetElement<RenderBox> rootElement =
      RenderObjectToWidgetAdapter<RenderBox>(
    container: repaintBoundary,
    child: Directionality(
      textDirection: TextDirection.ltr,
      child: IntrinsicHeight(child: IntrinsicWidth(child: widget)),
    ),
  ).attachToRenderTree(buildOwner);

  buildOwner
    ..buildScope(rootElement)
    ..finalizeTree();

  pipelineOwner
    ..flushLayout()
    ..flushCompositingBits()
    ..flushPaint();
  var i = await repaintBoundary.toImage(pixelRatio: ui.window.devicePixelRatio);
  var b = await i.toByteData(format: ui.ImageByteFormat.png);
  var result = b!.buffer.asUint8List();
  return result;
  // return await repaintBoundary
  //     .toImage(pixelRatio: ui.window.devicePixelRatio)
  //     .then((image) => image.toByteData(format: ui.ImageByteFormat.png))
  //     .then((byteData) => byteData!.buffer.asUint8List());
}
