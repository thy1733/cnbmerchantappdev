import 'package:get/get_utils/src/extensions/internacionalization.dart';
import 'package:get/instance_manager.dart';
import 'package:intl/intl.dart';

DateFormat userProfileFormatDate = DateFormat('dd MMM hh:mm a');
DateFormat newsFormatDate = DateFormat('dd MMM yy - hh:mm a');
DateFormat shortDateTimeFormatAMPM = DateFormat('MM/dd/yyyy hh:mm a');
DateFormat timeFormat = DateFormat('hh:mm');
DateFormat timeFormatAMPM = DateFormat('hh:mm a');

String kDateTimeFormat(DateTime value,
    {bool showDateOnly = false,
    bool showTimeOnly = false,
    bool showStandardDate = false}) {
  String result = "";
  String ampm = value.hour >= 12 ? 'PM'.tr : 'AM'.tr;
  
  if (showTimeOnly) {
    result = '${timeFormat.format(value)} $ampm';
  } else if (showStandardDate) {
    result = DateFormat.yMMMd(Get.locale?.languageCode).format(value);
  } else {
    final now = DateTime.now();
    final today = DateTime(now.year, now.month, now.day);
    final yesterday = DateTime(now.year, now.month, now.day - 1);
    final aDate = DateTime(value.year, value.month, value.day);
    if (aDate == today) {
       return showDateOnly ? 'today'.tr : '${'today'.tr} ${timeFormat.format(value)} $ampm';
    } else if (aDate == yesterday) {
      return showDateOnly ? 'yesterday'.tr : '${'yesterday'.tr} ${timeFormat.format(value)} $ampm';
    } 
    return showDateOnly ? DateFormat.yMMMd(Get.locale?.languageCode).format(value) : '${DateFormat.yMMMd(Get.locale?.languageCode).format(value)} - ${timeFormat.format(value)} $ampm';
  }
  return result.tr;
}

class AppTimeConvertor {
  static String militaryToStandard(int time) {
    String ampm = time >= 12 ? 'PM'.tr : 'AM'.tr;
    return "${time >= 12 ?time-12:time}$ampm";
  }
}
