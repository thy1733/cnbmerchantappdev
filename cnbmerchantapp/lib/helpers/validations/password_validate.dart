bool validatePassword(String value) {
  String pattern =
      r"""^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[-_!@#€$%^&*()~`\\/,.?":;{}\[\]|<>'≥÷≤∞¢£™¡¶•ªº≠‘“∂ƒ©∆˚¬…æ√∫˜µ≈çœ∑´®†¥¨ˆø–π”’+=]).{8,}$""";
  RegExp regExp = RegExp(pattern);
  return regExp.hasMatch(value);
}
