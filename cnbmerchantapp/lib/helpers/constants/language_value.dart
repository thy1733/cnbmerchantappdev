abstract class LanguageOption {
  static const String lanKhmer = 'km'; // Language code
  static const String conCodeKh = 'KH'; // Country code

  static const String lanEnglish = 'en'; // Language code
  static const String conCodeEn = 'US'; // Country code

  static const String lanChinese = 'zh'; // Language code
  static const String conCodeZh = 'Hans'; // Country code

  static const String lanName0 = 'ភាសាខ្មែរ'; // Country code
  static const String lanName1 = 'English'; // Country code
  static const String lanName2 = '中文'; // Country code
}
