
abstract class QrKeyConst {
  static const String payloadIndicator = '000201';
  static const String staticInitationMethod = '010211';
  static const String dynamicInitationMethod = '010212';
  static const String uniqIdentifier = 'cadikhppxxx@cadi';
  static const String acquiringBankMerchant = 'cadi';
  static const String appCode = 'CMA@CNB';
}

abstract class QrCurrencyCode {
  static const String khr = '116';
  static const String usd = '840';
}

abstract class QrCountryCode {
  static const String cambodia = 'KH';
}
