abstract class CacheKey {
  static const String appOnboardingScreen = 'app-onboarding-screen-running';
  static const String applocalize = 'app-localization-running';
  static const String appUserProfileInfo = 'app-profile-running';
  static const String appBusinessItem = 'app-business-item-running';
  static const String appBusinessList = 'app-business-list-running';
  static const String appSelectedBusinessId = 'app-selected-business-id';

  static const String outletItemList = 'app-outlet-list-running';
  static const String selectedOutletId = 'app-selected-outlet-id-running';
  static const String appSetting = 'app-setting-running';
}

abstract class CacheKeyType {
  static const int appOnboardingScreen = 16;
  static const int appLocalize = 18;
  static const int appUserProfileInfo = 19;
  static const int appBusinessItem = 20;
  static const int appBusinessList = 21;
  static const int appOutletItem = 22;
  static const int appOutletList = 23;
  static const int appSetting = 24;
  static const int appUserSetting = 25;
}
