abstract class ValueConst {
  static const double padding = 16;
  static const int appPinLength = 6;
  static const int businessNameLength =
      14; // Notice: Required by KHQR while it used Business Name and Outlet Name limit charactors.
  static const int requestQrMaterialItemMax = 5;
  static const String clear = 'clear';
  static const String backspace = 'backspace';
  static const String point = '.';
  static const String appTenancy = "merchant";
  static const String canadiaPhoneNumber = "023868222";
  static const String canadiaMessengerId = "478294965597629";
  static const String inviteFriendLink =
      "https://invite.canamerchant.com.kh/xEk2efep7cnJVc4D8";
  static const String sampleImageUrl =
      "https://s3-ap-southeast-1.amazonaws.com/yp-s3-dev/uploads/kh/logo_images/original/1408466.jfif";
  static const String appName = "Cana Merchant";
  static const String baseWebUrl = "https://digital.canadiabank.com/cmbuat";
  static const String termAndConditionURL = "$baseWebUrl/tc.php?tc=TCA&lg=";
  static const String partnershipAgreementURL = "$baseWebUrl/tc.php?tc=MPA&lg=";
  static const String notificationNewsURL = "$baseWebUrl/img.php?lg=";
  static const String userGuidesURL = "$baseWebUrl/vdo.php?lg=";
  static const String feedbackURL = "$baseWebUrl/rating.php?lg=";
  static const String activateTermConditonUrl = "$baseWebUrl/tc.php?tc=ACT&lg=";
}

class ArgumentValue {
  static const String search = "Search";
}
