abstract class ParameterKey {
  static const String routes = 'routes';
  static const String icon = 'icon';
  static const String color = 'color';
  static const String title = 'title';
  static const String action = 'action';
  static const String message = 'message';
  static const String btnTitle = 'btnTitle';
  static const String tabSelected = 'tabSelected';

  static const String value = 'value';
}

class ArgumentKey {
  static const String id = "Id";
  static const String item = "Item";
  static const String detail = "Detail";
  static const String fromPage = "fromPage";
  static const String pageTitle = "pageTitle";
  static const String input = "input";
  static const String imgCacheKey = "imgCacheKey";
}


class ArgumentValues {
  static const String notifyDetail = "notification";
}

abstract class TabMenu {
  static const int qrGenerate = 0;
  static const int transaction = 1;
  static const int notification = 2;
  static const int setting = 3;
}

abstract class ImageConfigure {
  static const String partOf = "v0/b/cnbmerchantapp.appspot.com/o/";
}

abstract class AppUserType {
  static const int businessOwner = 0;
  static const int cashier = 0;
}
