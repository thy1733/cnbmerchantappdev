// Place fonts/cnb_merchant.ttf in your fonts/ directory and
// add the following to your pubspec.yaml
// flutter:
//   fonts:
//    - family: cnb_merchant
//      fonts:
//       - asset: fonts/cnb_merchant.ttf
import 'package:flutter/widgets.dart';

class MerchantIcon {
  MerchantIcon._();

  static const String _fontFamily = 'cnb_merchant_icon_font';

  static const IconData merchantId = IconData(0xe959, fontFamily: _fontFamily);
  static const IconData merchantExisting = IconData(0xe957, fontFamily: _fontFamily);
  static const IconData merchantNew = IconData(0xe958, fontFamily: _fontFamily);
  static const IconData transactionItem = IconData(0xe956, fontFamily: _fontFamily);
  static const IconData flip = IconData(0xe955, fontFamily: _fontFamily);
  static const IconData home = IconData(0xe954, fontFamily: _fontFamily);
  static const IconData cameraOutlineBold =
      IconData(0xe950, fontFamily: _fontFamily);
  static const IconData downloadOutlineBold =
      IconData(0xe951, fontFamily: _fontFamily);
  static const IconData khqr = IconData(0xe952, fontFamily: _fontFamily);
  static const IconData shareOutlineBold =
      IconData(0xe953, fontFamily: _fontFamily);
  static const IconData inviteCode = IconData(0xe94f, fontFamily: _fontFamily);
  static const IconData link = IconData(0xe94d, fontFamily: _fontFamily);
  static const IconData signInfo = IconData(0xe94e, fontFamily: _fontFamily);
  static const IconData terms = IconData(0xe93b, fontFamily: _fontFamily);
  static const IconData transactionType =
      IconData(0xe93c, fontFamily: _fontFamily);
  static const IconData amount = IconData(0xe93d, fontFamily: _fontFamily);
  static const IconData chat = IconData(0xe93e, fontFamily: _fontFamily);
  static const IconData cloudDownloadFill =
      IconData(0xe93f, fontFamily: _fontFamily);
  static const IconData dollaSign2 = IconData(0xe940, fontFamily: _fontFamily);
  static const IconData filter = IconData(0xe941, fontFamily: _fontFamily);
  static const IconData rate = IconData(0xe942, fontFamily: _fontFamily);
  static const IconData guide = IconData(0xe943, fontFamily: _fontFamily);
  static const IconData nextStep = IconData(0xe944, fontFamily: _fontFamily);
  static const IconData note = IconData(0xe945, fontFamily: _fontFamily);
  static const IconData plusFill = IconData(0xe946, fontFamily: _fontFamily);
  static const IconData qrType = IconData(0xe947, fontFamily: _fontFamily);
  static const IconData rate1 = IconData(0xe948, fontFamily: _fontFamily);
  static const IconData refund = IconData(0xe949, fontFamily: _fontFamily);
  static const IconData repeat = IconData(0xe94a, fontFamily: _fontFamily);
  static const IconData user3Fill = IconData(0xe94b, fontFamily: _fontFamily);
  static const IconData user3FrameFill =
      IconData(0xe94c, fontFamily: _fontFamily);
  static const IconData minus = IconData(0xe939, fontFamily: _fontFamily);
  static const IconData plus = IconData(0xe93a, fontFamily: _fontFamily);
  static const IconData currencyKhr = IconData(0xe937, fontFamily: _fontFamily);
  static const IconData currencyUsd = IconData(0xe938, fontFamily: _fontFamily);
  static const IconData telephone1 = IconData(0xe926, fontFamily: _fontFamily);
  static const IconData shop = IconData(0xe923, fontFamily: _fontFamily);
  static const IconData chevronDown = IconData(0xe934, fontFamily: _fontFamily);
  static const IconData chevronUp = IconData(0xe935, fontFamily: _fontFamily);
  static const IconData docText = IconData(0xe936, fontFamily: _fontFamily);
  static const IconData bulkAction = IconData(0xe932, fontFamily: _fontFamily);
  static const IconData businessType =
      IconData(0xe933, fontFamily: _fontFamily);
  static const IconData calendar = IconData(0xe92f, fontFamily: _fontFamily);
  static const IconData docExcel = IconData(0xe930, fontFamily: _fontFamily);
  static const IconData docPDF = IconData(0xe931, fontFamily: _fontFamily);
  static const IconData cameraOutline =
      IconData(0xe900, fontFamily: _fontFamily);
  static const IconData alert = IconData(0xe901, fontFamily: _fontFamily);
  static const IconData backspace2 = IconData(0xe902, fontFamily: _fontFamily);
  static const IconData bank = IconData(0xe903, fontFamily: _fontFamily);
  static const IconData camera2 = IconData(0xe904, fontFamily: _fontFamily);
  static const IconData check = IconData(0xe905, fontFamily: _fontFamily);
  static const IconData checkOut = IconData(0xe906, fontFamily: _fontFamily);
  static const IconData chevronLeft = IconData(0xe907, fontFamily: _fontFamily);
  static const IconData chevronRight =
      IconData(0xe908, fontFamily: _fontFamily);
  static const IconData clear = IconData(0xe909, fontFamily: _fontFamily);
  static const IconData copy = IconData(0xe90a, fontFamily: _fontFamily);
  static const IconData documentText =
      IconData(0xe90b, fontFamily: _fontFamily);
  static const IconData dollarSign = IconData(0xe90c, fontFamily: _fontFamily);
  static const IconData download = IconData(0xe90d, fontFamily: _fontFamily);
  static const IconData edit = IconData(0xe90e, fontFamily: _fontFamily);
  static const IconData gps = IconData(0xe90f, fontFamily: _fontFamily);
  static const IconData image = IconData(0xe910, fontFamily: _fontFamily);
  static const IconData key = IconData(0xe911, fontFamily: _fontFamily);
  static const IconData mapLocation = IconData(0xe912, fontFamily: _fontFamily);
  static const IconData user2 = IconData(0xe913, fontFamily: _fontFamily);
  static const IconData menu = IconData(0xe914, fontFamily: _fontFamily);
  static const IconData messanger = IconData(0xe915, fontFamily: _fontFamily);
  static const IconData nationalId = IconData(0xe916, fontFamily: _fontFamily);
  static const IconData notification =
      IconData(0xe917, fontFamily: _fontFamily);
  static const IconData pin = IconData(0xe918, fontFamily: _fontFamily);
  static const IconData pin2 = IconData(0xe919, fontFamily: _fontFamily);
  static const IconData power = IconData(0xe91a, fontFamily: _fontFamily);
  static const IconData qrCode = IconData(0xe91b, fontFamily: _fontFamily);
  static const IconData refresh = IconData(0xe91c, fontFamily: _fontFamily);
  static const IconData remark = IconData(0xe91d, fontFamily: _fontFamily);
  static const IconData report = IconData(0xe91e, fontFamily: _fontFamily);
  static const IconData review = IconData(0xe91f, fontFamily: _fontFamily);
  static const IconData search = IconData(0xe920, fontFamily: _fontFamily);
  static const IconData share = IconData(0xe921, fontFamily: _fontFamily);
  static const IconData shop2 = IconData(0xe922, fontFamily: _fontFamily);
  static const IconData shopPlus2 = IconData(0xe924, fontFamily: _fontFamily);
  static const IconData telephone = IconData(0xe925, fontFamily: _fontFamily);
  static const IconData time = IconData(0xe927, fontFamily: _fontFamily);
  static const IconData transaction = IconData(0xe928, fontFamily: _fontFamily);
  static const IconData trash = IconData(0xe929, fontFamily: _fontFamily);
  static const IconData user = IconData(0xe92a, fontFamily: _fontFamily);
  static const IconData userCheck = IconData(0xe92b, fontFamily: _fontFamily);
  static const IconData userPlus = IconData(0xe92c, fontFamily: _fontFamily);
  static const IconData userX = IconData(0xe92d, fontFamily: _fontFamily);
  static const IconData x = IconData(0xe92e, fontFamily: _fontFamily);
}
