/* For PNG Assets Only */

abstract class PngAppAssets {
  static const String cnbLogoFull = 'assets/images/cnb_logo_full.jpg';
  static const String trxCnbLogo = 'assets/images/trx_cnb_logo.png';
  static const String trxKhQrLogo = 'assets/images/trx_khqr_logo.png';
  static const String icLauncher = 'assets/images/ic_launcher.png';
  static const String icEmptyBusiness = "assets/images/ic_empty_business.png";
  static const String icSearchNotFound =
      'assets/images/ic_search_not_found.png';

  static const String onboard1 = "assets/images/onboarding1.png";
  static const String onboard2 = "assets/images/onboarding2.png";
  static const String onboard3 = "assets/images/onboarding3.png";
  static const String appIcon = "assets/images/app-icon.png";
}
