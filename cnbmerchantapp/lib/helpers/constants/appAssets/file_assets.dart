abstract class FileAssets {
  static const String activateTermCondition =
      "assets/files/activate_term_condition.html";
  static const String merchantPartnershipAgreement =
      "assets/files/merchant_partnership_agreement.html";
  static const String termAndCondition = "assets/files/term_and_condition.html";
}
