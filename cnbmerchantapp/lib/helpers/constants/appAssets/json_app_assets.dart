/* For JSON Assets Only */

abstract class JsonAppAssets {
  static const String canaLogoAnimate = "assets/json/cana_logo_animate.json";
  static const String signSuccessAlert =
      "assets/json/sign-for-success-alert.json";
  static const String signErrorAlert = "assets/json/sign-for-error-alert.json";
  static const String downloadAnimate = "assets/json/download-animate.json";
  static const String wifiRetry = "assets/json/wifi_retry_animate.json";
  static const String cnbMerchantLoading =
      "assets/json/cnb_merchant_loading.json";
  static const String arrowUp = 'assets/json/arrow.json';
  static const String qrScanner = 'assets/json/qr-scanner.json';
  static const String noInternetConnection =
      "assets/json/no-internet-connection-empty-state.json";
  static const String notFound = "assets/json/404_notfound.json";
  static const String underMaintenance = "assets/json/under_maintenance.json";
}
