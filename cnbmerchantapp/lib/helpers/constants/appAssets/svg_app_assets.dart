/* For SVG Assets Only */

abstract class SvgAppAssets {
  static const String boarding1 = 'assets/svgs/samples/boarding1.svg';
  static const String boarding2 = 'assets/svgs/samples/boarding2.svg';
  static const String boarding3 = 'assets/svgs/samples/boarding3.svg';

  static const String logo = 'assets/svgs/logo.svg';
  static const String cnbLogoYellow = 'assets/svgs/cnb_logo.svg';
  static const String setupIcon = 'assets/svgs/setup-icon.svg';
  static const String shop = 'assets/svgs/shop.svg';
  static const String shopCircle = 'assets/svgs/shop_circle.svg';
  static const String userCircle = 'assets/svgs/user_circle.svg';
  // static const String qrFrame = 'assets/svgs/qr_frame.svg';
  static const String keyIcon = 'assets/svgs/key-icon.svg';
  static const String messageIcon = 'assets/svgs/message-icon.svg';
  static const String transaction = 'assets/svgs/transaction.svg';
  static const String wallet = 'assets/svgs/wallet.svg';
  // static const String qrArrow = 'assets/svgs/qr_arrow.svg';

  // static const String icCamera = 'assets/svgs/ic_camera.svg';
  // static const String icGallery = 'assets/svgs/ic_gallery.svg';

  static const String khLogo = 'assets/svgs/kh.svg';
  static const String enLogo = 'assets/svgs/en.svg';
  static const String chLogo = 'assets/svgs/ch.svg';

  static const String cashier = 'assets/svgs/cashier.svg';
  static const String noData = 'assets/svgs/no_data.svg';

  static const String flagKhmer = 'assets/svgs/khmer_flag.svg';
  static const String flagEnglish = 'assets/svgs/uk_flag.svg';
  static const String flagChinese = 'assets/svgs/chines_flag.svg';
  // static const String cashierError = 'assets/svgs/casheir_error_image.svg';
  // static const String salepointError = 'assets/svgs/salepoint_error_image.svg';
  static const String khqrLogo = 'assets/svgs/khqr_logo.svg';
  static const String locationOn = 'assets/svgs/location_on.svg';
  static const String locationOnSelected =
      'assets/svgs/location_on_selected.svg';

  static const String icStar = 'assets/svgs/ic_star.svg';
  static const String icAddBusiness = 'assets/svgs/ic_add_business.svg';
  static const String icDefaultShop = 'assets/svgs/ic_default_shop.svg';
  static const String icAddCasheir = 'assets/svgs/ic_add_cashier.svg';
  static const String icShopRed = 'assets/svgs/ic_shop_red.svg';
  static const String icEditor = 'assets/svgs/ic_edit.svg';
  // static const String icBusinessType = 'assets/svgs/ic_business_type.svg';
  static const String icLinkAccountName = 'assets/svgs/ic_link_acc_name.svg';
  static const String icLinkTelegramGroup = 'assets/svgs/ic_link_telegram.svg';

  static const String changeUsernameLogo =
      'assets/svgs/change_username_screen_logo.svg';

  static const String businessOwnerActivationLogo =
      'assets/svgs/business_owner_activation_screen_logo.svg';
  static const String cashierActivationLogo =
      'assets/svgs/cashier_activation_screen_logo.svg';
  static const String verifyOTPLogo = 'assets/svgs/verify_otp_screen_logo.svg';
  static const String usernameLogo = 'assets/svgs/username_screen_logo.svg';
  static const String createNewPasswordLogo =
      'assets/svgs/create_new_password_screen_logo.svg';
  static const String feedbackLogo = 'assets/svgs/feedback_screen_logo.svg';
  static const String icQrStand = 'assets/svgs/ic-qr stand.svg';
  static const String activateByCIDLogo =
      'assets/svgs/activate_by_cid_screen_logo.svg';
  static const String userCircleGray = 'assets/svgs/user_circle_gray.svg';
  static const String forgotUsernamePasswordLogo =
      'assets/svgs/forgot_username_password_screen_logo.svg';
  static const String downloadReport = 'assets/svgs/download_report.svg';
  static const String findCIDLogo = 'assets/svgs/find_cid_logo.svg';
  static const String checkList = 'assets/svgs/checklist.svg';
  static const String merchantProgram = 'assets/svgs/program.svg';
  static const String friendInvitation = 'assets/svgs/invite_friend.svg';
  static const String icRequestMerchantReferal =
      'assets/svgs/ic_request_merchant_referal.svg';
  static const String icUpgrade = 'assets/svgs/ic_upgrade.svg';
  static const String icNote = 'assets/svgs/ic_note.svg';
  static const String icDelete = 'assets/svgs/ic_delete.svg';
  static const String icPDF = 'assets/svgs/ic_pdf.svg';
  // static const String flip = 'assets/svgs/flip.svg';
  static const String cnbBuildingBg = 'assets/svgs/cnb_building_bg.svg';
  static const String icAccount = 'assets/svgs/ic_account.svg';
  static const String icFeedback = 'assets/svgs/ic_feedback.svg';
  static const String icPhone = 'assets/svgs/ic_phone.svg';
  static const String icOnlineChat = 'assets/svgs/ic_online_chat.svg';

  static const String qrStickerBg = 'assets/svgs/qr_sticker_bg.svg';
  static const String cutLeft = 'assets/svgs/ic_cut_left.svg';
  static const String cutRight = 'assets/svgs/ic_cut_right.svg';

  static const String confirmRefund = 'assets/svgs/confirm_refund.svg';

  // placeholder image
  static const String pBusinessDetail = 'assets/svgs/placeholders/business_detail.svg';
  static const String pBusinessList = 'assets/svgs/placeholders/business_list.svg';
  static const String pOutletDetail = 'assets/svgs/placeholders/outlet_detail.svg';
  static const String pOutletList = 'assets/svgs/placeholders/outlet_list.svg';
  static const String pOutletCreate = 'assets/svgs/placeholders/outlet_create.svg';

}
