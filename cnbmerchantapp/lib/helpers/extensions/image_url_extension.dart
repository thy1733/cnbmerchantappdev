import 'package:cnbmerchantapp/core.dart';

extension GetImageUrl on String {
  String downloadUrl(UploadProfileType uploadType, String refId,String fileName){
    var path = fileName.isNotEmpty ? "${ApiConfigHelper.downloadUrl}${ImageConfigure.partOf}${uploadType.name}%2F$refId%2F$fileName?alt=media" : "${ApiConfigHelper.downloadUrl}${ImageConfigure.partOf}default%2F${uploadType.name}.png?alt=media";
    return path;
  }
}
