import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

extension StringExtensions on String {
  int toInt() {
    return int.parse(this);
  }

  String removeWhiteSpace() {
    return replaceAll(' ', '');
  }

  String removeFirstZero() {
    return replaceFirst(RegExp(r'^0+'), '');
  }

  String removeNoGmail() {
    return replaceAll('@nogmail.com', '');
  }

  String removePercentage() => replaceAll('%', '');

  String removeDash() {
    return replaceAll('-', '');
  }

  String replaceSemiColonToComma() => replaceAll(",", ";");

  String replaceToComma() => replaceAll(";", ",");

  String replaceFromZeroToCountryCode() {
    return replaceFirst(RegExp('^0*'), '855');
  }

  String replaceFromCountryCodeToZero() {
    return replaceFirst(RegExp('^855*'), '0');
  }

  String maskPhoneNumber() {
    return MaskFormat.phoneNumberString(this);
  }

  String maskAccountNumber() {
    var result = MaskFormat.maskAccountNumber(this);
    return result.isNotEmpty? result: this;
  }

  removeTrailingZeros() => replaceAll(RegExp(r"([.]*0+)(?!.*\d)"), "");

  String toThousandSeparatorString({bool d2 = false}) {
    try {
      String formatDigti = d2 ? "#,##0.00" : "#,##0.##";
      NumberFormat numberFormat = NumberFormat(formatDigti, "en_US");
      return numberFormat.format(double.parse(this));
    } catch (e) {
      return "E";
    }
  }

  /// This extension will remove all charactor excpted RegExp('[^0-9.]') to represent a double value as string
  String toSafeDoubleAsString() {
    String result = this;
    return result.replaceAll(RegExp('[^0-9.]'), '');
  }

  String addMultipleLanguagesSupport() {
    String locale = Get.locale?.countryCode ?? LanguageOption.conCodeEn;
    if (locale == LanguageOption.conCodeZh) {
      locale = 'CN';
    } else if (locale == LanguageOption.conCodeKh) {
      locale = 'KH';
    } else {
      locale = 'EN';
    }
    return this + locale;
  }
}
