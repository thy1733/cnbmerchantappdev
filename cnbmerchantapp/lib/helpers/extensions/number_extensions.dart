import 'package:intl/intl.dart';

extension IntExtensions on int {
  String toThousandSeparatorString() {
    try {
      String formatDigti = "#,##0";
      NumberFormat numberFormat = NumberFormat(formatDigti, "en_US");
      return numberFormat.format(this);
    } catch (e) {
      return "E";
    }
  }
}

extension DoubleExtensions on double {
  String toThousandSeparatorString({bool d2 = false}) {
    try {
      String formatDigti = d2 ? "#,##0.00" : "#,##0.##";
      NumberFormat numberFormat = NumberFormat(formatDigti, "en_US");
      return numberFormat.format(this);
    } catch (e) {
      return "E";
    }
  }
}

extension KAccountMask on int {
  String fromGenricToString(String pattern) {
    try {
      // return DateFormat(pattern).format(this);
      NumberFormat numberFormat = NumberFormat(pattern, "en_US");
      return numberFormat.format(this);
    } catch (e) {
      return "E";
    }
  }
}

extension KNumberMask on double {
  String fromGenricToString(String pattern) {
    try {
      // return DateFormat(pattern).format(this);
      NumberFormat numberFormat = NumberFormat(pattern, "en_US");
      return numberFormat.format(this);
    } catch (e) {
      return "E";
    }
  }
}
