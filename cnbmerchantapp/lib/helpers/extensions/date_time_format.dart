import 'package:intl/intl.dart';

extension KDateTimeFormat on DateTime {
  String toFormatString(DateFormat pattern) {
    try {
      return pattern.format(this);
    } catch (e) {
      return e.toString();
    }
  }

  bool isToday() {
    Duration differnce = DateTime.now().difference(this);
    if (differnce.inDays == 0) {
      return true;
    } else {
      return false;
    }
  }

  DateTime dateOnly() {
    return DateTime(year, month, day);
  }

  String toFormatDateString(String pattern) {
    try {
      return DateFormat(pattern).format(this);
    } catch (e) {
      return "";
    }
  }
}
