import 'package:get/get.dart';
import 'package:intl/intl.dart';

extension DateTimeFormatExtensions on String {
  DateTime fromUTCToLocalDateTime() {
    return DateTime.parse(
        DateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(this).toString());
  }

  DateTime fromTxnFormatToLocalDateTime() {
    var dt = DateTime.now();
    try {
      String date = this;
      String dateWithT = '${date.substring(0, 8)}T${date.substring(8)}';
      dt = DateTime.parse(dateWithT);
    } catch (e) {
      e.printError();
    }
    return dt;
  }

  DateTime fromUTCToDateTime() {
    String formattedDate = this;
    if (formattedDate.endsWith(':')) {
      formattedDate += '00';
    }
    if (formattedDate.length == 16) {
      formattedDate += ':00';
    }
    if (formattedDate.length == 19) {
      formattedDate += '.000Z';
    }
    return DateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
        .parse(formattedDate)
        .toLocal();
  }

  DateTime fromStringToDateTime(String dateString,
      {String inputFormat = "yyyy-MM-dd'T'HH:mm",
      String outputFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"}) {
    final inputDate = DateFormat(inputFormat).parse(dateString);
    if (inputFormat == "yyyy-MM-dd'T'HH:mm" && !dateString.endsWith(":00")) {
      dateString += ":00";
      inputDate.add(const Duration(seconds: 0));
    }
    final outputString = DateFormat(outputFormat).format(inputDate);
    return DateTime.parse(outputString);
  }

  String timeAgoSinceDate(DateTime dt, {bool numericDates = true}) {
    DateTime date = dt.isUtc ? dt.toLocal() : dt;
    final date2 = DateTime.now().toLocal();
    final difference = date2.difference(date);

    if (difference.inSeconds < 5) return 'Just now';
    if (difference.inSeconds < 60) return '${difference.inSeconds} seconds ago';
    if (difference.inMinutes <= 1) {
      return (numericDates) ? '1 minute ago' : 'A minute ago';
    }
    if (difference.inMinutes < 60) return '${difference.inMinutes} minutes ago';
    if (difference.inHours <= 1) {
      return (numericDates) ? '1 hour ago' : 'An hour ago';
    }
    if (difference.inHours < 24) return '${difference.inHours} hours ago';
    if (difference.inDays <= 1) {
      return (numericDates) ? '1 day ago' : 'Yesterday';
    }
    if (difference.inDays < 6) return '${difference.inDays} days ago';
    if ((difference.inDays / 7).ceil() <= 1) {
      return (numericDates) ? '1 week ago' : 'Last week';
    }
    if ((difference.inDays / 7).ceil() < 4) {
      return '${(difference.inDays / 7).ceil()} weeks ago';
    }
    if ((difference.inDays / 30).ceil() <= 1) {
      return (numericDates) ? '1 month ago' : 'Last month';
    }
    if ((difference.inDays / 30).ceil() < 30) {
      return '${(difference.inDays / 30).ceil()} months ago';
    }
    if ((difference.inDays / 365).ceil() <= 1) {
      return (numericDates) ? '1 year ago' : 'Last year';
    }
    return '${(difference.inDays / 365).floor()} years ago';
  }
}

extension DateFormatPattern on DateTime {
  DateTime toLocalDateTime(String pattern) {
    var date = DateFormat(pattern).format(this).toString();
    return DateTime.parse(date);
  }

  String toTimeAgoDate(DateTime dt) {
    DateTime date = dt.isUtc ? dt.toLocal() : dt;
    final now = DateTime.now();
    final today = DateTime(now.year, now.month, now.day);
    final yesterday = DateTime(now.year, now.month, now.day - 1);
    final aDate = DateTime(date.year, date.month, date.day);
    if (aDate == today) {
       return 'today'.tr;
    } else if (aDate == yesterday) {
      return 'yesterday'.tr;
    } 
    return DateFormat.yMMMd(Get.locale?.languageCode).format(dt);
  }
}
