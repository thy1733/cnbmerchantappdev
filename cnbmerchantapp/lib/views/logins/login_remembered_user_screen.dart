import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LoginRememberedUserScreen extends StatefulWidget {
  const LoginRememberedUserScreen({Key? key}) : super(key: key);

  @override
  State<LoginRememberedUserScreen> createState() =>
      _LoginRememberedUserScreenState();
}

class _LoginRememberedUserScreenState extends State<LoginRememberedUserScreen> {
  final LoginScreenController _controller = Get.find<LoginScreenController>();

  final passwordFocusNode2 = FocusNode();

  void onClickLogin(String? value) {
    _controller.usernameValidate;
    _controller.passwordValidate;

    if (!_controller.passwordValidate) {
      passwordFocusNode2.requestFocus();
    } else {
      _controller.loginAsync();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Obx(() => Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "WelcomeBack".tr.replaceAll(
                  "{0}",
                  AppUserTempInstant
                      .appCacheUser.displayName), //* Nedd to update *//
              style: const TextStyle(
                color: AppColors.textPrimary,
                fontSize: 18,
                fontWeight: FontWeight.w700,
              ),
            ),
            const SizedBox(height: 20),
            Center(
              child: SizedBox(
                width: 125,
                height: 125,
                child: ClipOval(
                  child: AppNetworkImage(
                    AppUserTempInstant.appCacheUser.photo,
                    errorWidget: PlaceholderImage.userName(
                        AppUserTempInstant.appCacheUser.firstName.toUpperCase(),
                        AppUserTempInstant.appCacheUser.lastName.toUpperCase(),
                        fontSize: 30),
                  ),
                ),
              ),
            ),
            const SizedBox(height: 20),
            KTextField(
              controller: _controller.passwordTextFieldController,
              name: "Password",
              label: "Password".tr,
              hint: "Password".tr,
              obscureText: _controller.isHidePassword,
              secondChild: _controller.loginInput.password.isNotEmpty
                  ? InkWell(
                      onTap: () {
                        _controller.isHidePassword =
                            !_controller.isHidePassword;
                      },
                      child: Padding(
                        padding: const EdgeInsets.all(5),
                        child: Icon(
                          _controller.isHidePassword
                              ? Icons.visibility
                              : Icons.visibility_off,
                          size: 20,
                        ),
                      ),
                    )
                  : null,
              initialValue: '',
              onSubmit: onClickLogin,
              autoFocus: true,
              focusNode: passwordFocusNode2,
              validatorMessage:
                  _controller.loginInput.passwordValidateMessage.tr,
              iconData: MerchantIcon.key,
              keyboardType: TextInputType.visiblePassword,
              onChanged: (value) => _controller.passwordEventChanging(value),
            ),
            Align(
              alignment: Alignment.centerRight,
              child: TextButton(
                onPressed: _controller.forgotPassword,
                child: Text(
                  "ForgotPassword".tr,
                  textAlign: TextAlign.right,
                  style: const TextStyle(
                      color: AppColors.primary, fontWeight: FontWeight.w300),
                ),
              ),
            ),
            AppButtonStyle.primaryButton(
              margin: const EdgeInsets.only(bottom: 10),
              label: "Login".tr,
              onPressed: () => onClickLogin(""),
            ),
            AppButtonStyle.primaryOutlinedButton(
              label: "LoginDifferentAccount".tr,
              textColor: AppColors.textPrimary,
              borderColor: AppColors.greyishDisable,
              onPressed: _controller.onSwitchLoginMode,
            ),
          ],
        ));
  }
}
