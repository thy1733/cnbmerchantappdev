import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen>
    with SingleTickerProviderStateMixin {
  final LoginScreenController _controller = Get.put(LoginScreenController());

  // final _passwordTextFieldController = TextEditingController();
  FocusNode userNameFocusNode = FocusNode();
  FocusNode passwordFocusNode = FocusNode();

  @override
  void initState() {
    super.initState();
    // DeviceSecurityManager.sharedInstance.checkSecuredDevice();
  }

  void onUserNameSubmit(String? value) {
    if (_controller.usernameValidate) {
      passwordFocusNode.requestFocus();
    }
  }

  void onPasswordSubmit(String? value) {
    if (_controller.passwordValidate) {
      onClickLogin();
    }
  }

  void onClickLogin() {
    _controller.usernameValidate;
    _controller.passwordValidate;

    if (!_controller.usernameValidate) {
      userNameFocusNode.requestFocus();
    } else if (!_controller.passwordValidate) {
      passwordFocusNode.requestFocus();
    } else {
      // controller.onLoginAsnync();
      _controller.loginAsync();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => ContainerTemp(
        header: Padding(
          padding: const EdgeInsets.all(15),
          child: _header(),
        ),
        child: SingleChildScrollView(
          padding: const EdgeInsets.all(15),
          child: _checkRememberUser(),
        ),
      ),
    );
  }

  Widget _checkRememberUser() {
    return AnimatedCrossFade(
      firstChild: const LoginRememberedUserScreen(),
      secondChild: _loginUserPassword(),
      alignment: Alignment.topCenter,
      firstCurve: Curves.ease,
      crossFadeState: _controller.isShowRememberedUser
          ? CrossFadeState.showFirst
          : CrossFadeState.showSecond,
      duration: const Duration(milliseconds: 250),
    );
  }

  Widget _header() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        SvgPicture.asset(
          SvgAppAssets.logo,
          height: 40.h,
          fit: BoxFit.fitWidth,
        ),
        ContainerDevider.blankSpaceSm,
        Text.rich(
          TextSpan(
            style: TextStyle(
              color: AppColors.white,
              fontSize: 17.sp,
              height: 1.3.h,
              fontWeight: FontWeight.w700,
            ),
            children: [
              TextSpan(
                text: "${"Welcome".tr}\n",
                style: AppTextStyle.caption.copyWith(color: AppColors.white),
              ),
              TextSpan(
                text: ValueConst.appName.toUpperCase(),
                style: TextStyle(fontFamily: GoogleFonts.roboto().fontFamily),
              ),
            ],
          ),
        ),
        const Expanded(child: SizedBox()),
        _languageChangeButton(),
      ],
    );
  }

  Widget _languageChangeButton() {
    return InkWell(
      onTap: () async {
        await _controller.updateLanguagesAsync();
      },
      child: Container(
        padding: const EdgeInsets.all(5).r,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(30).r,
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(
              height: 25.h,
              width: 25.w,
              child: ClipOval(
                child: SvgPicture.asset(
                  _controller.languageSelected.imageSvg,
                  fit: BoxFit.cover,
                ),
              ),
            ),
            ContainerDevider.blankSpaceSm,
            Text(
              _controller.languageSelected.name,
              style: AppTextStyle.body2.copyWith(color: AppColors.textPrimary),
            ),
            ContainerDevider.blankSpaceSm,
          ],
        ),
      ),
    );
  }

  Widget _loginUserPassword() => Column(children: [
        Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Align(
              alignment: Alignment.centerLeft,
              child: Text(
                "Login".tr,
                textAlign: TextAlign.start,
                style: AppTextStyle.header.copyWith(
                    fontSize: 18.sp,
                    height: 1.8,
                    color: AppColors.textPrimary,
                    fontWeight: FontWeight.w700),
              ),
            ),
            const SizedBox(height: 15),
            KTextField(
              controller: _controller.usernameTextFieldController,
              name: "Username",
              label: "Username".tr,
              hint: "Username".tr,
              initialValue: '',
              validatorMessage: _controller.loginInput.userValidateMessage.tr,
              iconData: MerchantIcon.user,
              keyboardType: TextInputType.name,
              autocorrect: false,
              enableSuggestions: false,
              textInputAction: TextInputAction.next,
              onSubmit: onUserNameSubmit,
              focusNode: userNameFocusNode,
              onEditingComplete: () {
                debugPrint("onEditingComplete");
              },
              onChanged: (value) => _controller.userNameEventChanging(value),
            ),
            KTextField(
              controller: _controller.passwordTextFieldController,
              name: "Password",
              label: "Password".tr,
              hint: "Password".tr,
              initialValue: '',
              onSubmit: onPasswordSubmit,
              focusNode: passwordFocusNode,
              obscureText: _controller.isHidePassword,
              secondChild: _controller.loginInput.password.isNotEmpty
                  ? InkWell(
                      onTap: () {
                        _controller.isHidePassword =
                            !_controller.isHidePassword;
                      },
                      child: Padding(
                        padding: const EdgeInsets.all(5),
                        child: Icon(
                          _controller.isHidePassword
                              ? Icons.visibility
                              : Icons.visibility_off,
                          size: 20,
                        ),
                      ),
                    )
                  : null,
              validatorMessage:
                  _controller.loginInput.passwordValidateMessage.tr,
              iconData: MerchantIcon.key,
              keyboardType: TextInputType.visiblePassword,
              onChanged: (value) => _controller.passwordEventChanging(value),
            ),
            Align(
              alignment: Alignment.centerRight,
              child: ElevatedButton(
                style: ButtonStyle(
                    shadowColor:
                        MaterialStateProperty.all<Color>(Colors.transparent),
                    foregroundColor:
                        MaterialStateProperty.all<Color>(AppColors.primary),
                    backgroundColor: MaterialStateProperty.all<Color>(
                        AppColors.white.withOpacity(0.0))),
                onPressed: _controller.forgotPassword,
                child: Text(
                  "${"ForgotPassword".tr}?",
                  style: const TextStyle(
                      color: AppColors.primary, fontWeight: FontWeight.w300),
                ),
              ),
            ),
            _buttonPart(),
            const SizedBox(
              height: 20,
            ),
          ],
        ),
      ]);

  Widget _buttonPart() => Column(
        children: [
          AppButtonStyle.primaryButton(
            label: "Login".tr,
            onPressed: () => onClickLogin(),
          ),
          ContainerDevider.blankSpaceMd,
          Visibility(
            visible: _controller.isRememberedUser,
            child: Padding(
              padding: const EdgeInsets.only(bottom: 15).r,
              child: AppButtonStyle.primaryOutlinedButton(
                label: "Back".tr,
                textColor: AppColors.textPrimary,
                borderColor: AppColors.greyishDisable,
                onPressed: _controller.onSwitchLoginMode,
              ),
            ),
          ),
          SizedBox(
            child: Text(
              "Or".tr.toUpperCase(),
              textAlign: TextAlign.right,
              style: AppTextStyle.label1,
            ),
          ),
          ContainerDevider.blankSpaceMd,
          AppButtonStyle.primaryOutlinedButton(
            label: "ActivateAsBusinessOwner".tr,
            textColor: AppColors.textPrimary,
            borderColor: AppColors.greyishDisable,
            onPressed: _controller.activateOwner,
          ),
          ContainerDevider.blankSpaceMd,
          AppButtonStyle.primaryOutlinedButton(
            label: "ActivateAsCashier".tr,
            textColor: AppColors.textPrimary,
            borderColor: AppColors.greyishDisable,
            onPressed: _controller.activateCashier,
          ),
          // ContainerDevider.blankSpaceMd,
          // AppButtonStyle.primaryOutlinedButton(
          //   label: "ActivateExistingMerchant".tr,
          //   textColor: AppColors.textPrimary,
          //   borderColor: AppColors.greyishDisable,
          //   onPressed: _controller.activateExistingMerchant,
          // ),
          const SizedBox(
            height: 15,
          ),
        ],
      );
}
