import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';

class UpsertOutletScreen extends StatefulWidget {
  const UpsertOutletScreen({super.key});

  @override
  State<UpsertOutletScreen> createState() => _UpsertOutletScreenState();
}

class _UpsertOutletScreenState extends State<UpsertOutletScreen> {
  final UpsertOutletScreenController controller =
      Get.put(UpsertOutletScreenController());

  @override
  Widget build(BuildContext context) {
    final double padding = 16.r;

    return Obx(
      () => ContainerAnimatedTemplate(
        title: controller.title,
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisSize: MainAxisSize.min,
            children: [
              Expanded(
                  child: SingleChildScrollView(
                physics: const ScrollPhysics(),
                child: Column(
                  children: [
                    imageSection(),
                    SizedBox(
                      height: padding,
                    ),
                    outletNameSection(),
                    outletLocationNameSection(),
                    outletAddressSection(),
                    addCashiersSection(),
                    SizedBox(
                      height: padding,
                    ),
                    primaryOutletSection(),
                    // linkTelegramSection(),
                  ],
                ),
              )),
              ButtonCircleRadius(
                title: controller.titleButton,
                submit: (value) {
                  controller.upsertOutletAsync();
                },
              )
            ]),
      ),
    );
  }

  Widget imageSection() {
    return CircleImageUploadView(
        placeHolder: SvgAppAssets.pOutletCreate,
        imageFile: controller.photoFile,
        imageUrl: controller.outletInfo.outletLogo,
        cacheKey: controller.cacheKey,
        onImagePicked: controller.isOutletPatentRegister
            ? () {
                Fluttertoast.showToast(
                    msg: "OutletPrimaryMessage".tr,
                    toastLength: Toast.LENGTH_LONG);
              }
            : () {
                controller.uploadOutletLogo();
              });
  }

  Widget outletNameSection() {
    return KTextField(
      enabled: !controller.isOutletPatentRegister,
      focusNode: controller.outletNameFocus,
      controller: controller.outletNameController,
      prefixText: controller.businessName,
      textInputAction: TextInputAction.next,
      maxLine: 1,
      name: "OutletName".tr,
      iconData: MerchantIcon.shop,
      keyboardType: TextInputType.text,
      textCapitalization: TextCapitalization.words,
      validatorMessage: controller.outletNameValidateMsg,
      inputFormatters: [
        AppTextInputRegx.inputNameRegx,
      ],
      onChanged: (value) {
        controller.onOutletNameChanged(value!);
      },
    );
  }

  Widget outletLocationNameSection() {
    return InkWell(
      onTap: () {
        controller.browseLocation();
      },
      child: KTextField(
        controller: controller.locationNameConroller,
        enabled: false,
        name: "OutletLocation".tr,
        hint: "OutletLocation".tr,
        validatorMessage: controller.outletLocationNameValidateMsg,
        needIcon: false,
        secondChild: const Icon(
          MerchantIcon.mapLocation,
          size: 16,
        ),
      ),
    );
  }

  Widget outletAddressSection() {
    return KTextField(
      controller: controller.addressInputConroller,
      textInputAction: TextInputAction.done,
      name: "Address".tr,
      hint: "Address".tr,
      iconData: MerchantIcon.shop,
      keyboardType: TextInputType.text,
      textCapitalization: TextCapitalization.words,
      validatorMessage: controller.outletLocationAddressValidateMsg,
      onChanged: (value) {
        controller.onOutletLocationAddressChanged(value!);
      },
    );
  }

  Widget addCashiersSection() {
    final double hei = 100.r;
    bool visibility = (controller.selectedCashiers.isEmpty ||
            controller.cashiers.length != controller.selectedCashiers.length) &&
        controller.cashiers.isNotEmpty;

    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          padding: const EdgeInsets.all(10),
          decoration: BoxDecoration(
              border: Border.all(color: AppColors.grey),
              borderRadius: BorderRadius.circular(10)),
          height: hei,
          child: Row(children: [
            Expanded(
              flex: 12,
              child: AbsorbPointer(
                absorbing: !visibility,
                child: Opacity(
                  opacity: visibility ? 1 : .5,
                  child: Stack(
                    alignment: AlignmentDirectional.center,
                    children: [
                      InkWell(
                        onTap: () {
                          controller.addCashier();
                        },
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Expanded(
                              flex: 1,
                              child: Container(
                                  width: 44.w,
                                  height: 44.w,
                                  decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      border: Border.all(
                                          color: AppColors.primary, width: 1)),
                                  child: Icon(MerchantIcon.userPlus,
                                      size: 25.r, color: AppColors.primary)),
                            ),
                            Text("AddCashier".tr,
                                textAlign: TextAlign.center,
                                style: AppTextStyle.primary),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            const SizedBox(
              width: 8,
            ),
            Container(
              height: hei - 16,
              width: 1,
              color: AppColors.grey,
            ),
            const SizedBox(
              width: 16,
            ),
            Expanded(
                flex: 25,
                child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    padding: EdgeInsets.zero,
                    itemCount: controller.selectedCashiers.length,
                    shrinkWrap: true,
                    itemBuilder: (context, index) {
                      var cashier = controller.selectedCashiers[index];
                      return cashierItem(cashier);
                    }))
          ]),
        ),
      ],
    );
  }

  Widget cashierItem(ItemSelectOptionModel cashier) {
    var originItem = cashier.data as CashierItemListOutputModel;

    return Padding(
      padding: const EdgeInsets.only(right: 8),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            flex: 1,
            child: Stack(alignment: AlignmentDirectional.topEnd, children: [
              Card(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(50)),
                child: AppNetworkImage(
                  width: 44.w,
                  height: 44.w,
                  shape: BoxShape.circle,
                  originItem.photoUrl,
                  errorWidget: SizedBox(
                    width: 44.w,
                    height: 44.w,
                    child: Icon(MerchantIcon.user,
                        size: 25.r, color: AppColors.primary),
                  ),
                ),
              ),
              InkWell(
                onTap: () {
                  controller.removeCashier(cashier);
                },
                child: Transform.rotate(
                  angle: 180,
                  child: const Icon(
                    MerchantIcon.plusFill,
                    color: AppColors.primary,
                  ),
                ),
              ),
            ]),
          ),
          Text(
            cashier.name,
            textAlign: TextAlign.center,
            style: AppTextStyle.value,
          ),
        ],
      ),
    );
  }

  Widget primaryOutletSection() {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Expanded(
          child: Text(
            "PrimaryOutlet".tr,
            style: AppTextStyle.value,
          ),
        ),
        CupertinoSwitch(
          activeColor: AppColors.primary,
          value: controller.isPrimary,
          onChanged: (value) {
            controller.onPrimaryChanged(value);
          },
        ),
      ],
    );
  }

  Widget linkTelegramSection() {
    return SizedBox(
      child: TextButton.icon(
        style: AppButtonStyle.kSecondaryButtonExpandStyle(),
        onPressed: () {
          controller.showLinkedTelegramGroup();
        },
        icon: const Icon(
          Icons.link,
          color: AppColors.primary,
        ),
        label: Text(
          controller.telegramGroupID.isNotEmpty
              ? "RemoveTelegramGroupID".tr
              : "LinkTelegramGroup".tr,
          textAlign: TextAlign.center,
          style: AppTextStyle.primary,
        ),
      ),
    );
  }
}
