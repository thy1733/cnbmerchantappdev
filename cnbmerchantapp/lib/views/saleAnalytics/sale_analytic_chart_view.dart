import 'dart:math';

import 'package:cnbmerchantapp/core.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SaleAnalyticChartView extends StatefulWidget {
  const SaleAnalyticChartView({
    super.key,
    required this.datas,
  });
  final List<SaleAnalyMonthlyOutputModel> datas;

  @override
  State<SaleAnalyticChartView> createState() => _SaleAnalyticChartViewState();
}

class _SaleAnalyticChartViewState extends State<SaleAnalyticChartView>
    with SingleTickerProviderStateMixin {
  late TabController chartTabController;
  final controller = Get.find<TabHomeScreenController>();

  final _currentIndex = 0.obs;
  int get currentIndex => _currentIndex.value;
  set currentIndex(int value) => _currentIndex.value = value;

  @override
  void initState() {
    if (widget.datas.isNotEmpty) {
      chartTabController = TabController(
        vsync: this,
        length: widget.datas.length,
        initialIndex: widget.datas.length - 1,
      );

      // total data
      WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
        currentIndex = widget.datas.length - 1;
      });

      // event listener for tap
      chartTabController.addListener(() async {
        if (chartTabController.indexIsChanging == false) {
          controller.showLoading();
          currentIndex = chartTabController.index;
          controller.currentSaleAnalyDatasChartIndex = currentIndex;
          await controller.fetchSaleAnalyticDataAsync(
            chartTabController.index,
            widget.datas.elementAt(currentIndex).selectDate,
          );
          controller.dismiss();
        }
      });
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 260,
      width: Get.width,
      child: _graphBuilder(),
    );
  }

  Widget _graphBuilder() {
    return DefaultTabController(
      length: widget.datas.length,
      initialIndex: widget.datas.length - 1,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          TabBar(
            isScrollable: true,
            controller: chartTabController,
            labelColor: AppColors.textPrimary,
            unselectedLabelColor: AppColors.greyishDisable,
            // onTap: (index) => controller.fetchSaleAnalyticDataAsync(
            //     index, widget.datas.elementAt(index).selectDate),
            indicatorColor: Colors.transparent,
            tabs: List.generate(
              widget.datas.length,
              (index) => Padding(
                padding: const EdgeInsets.only(bottom: 5),
                child: Text(
                  widget.datas
                      .elementAt(index)
                      .selectDate
                      .toFormatDateString("MMMM")
                      .toUpperCase()
                      .tr,
                  style: AppTextStyle.subtitle2,
                ),
              ),
            ),
          ),
          const SizedBox(height: 5),
          Expanded(
            child: TabBarView(
              controller: chartTabController,
              children: List.generate(
                widget.datas.length,
                (index) => controller.canShowChat &&
                        widget.datas.elementAt(index).items.isNotEmpty
                    ? _chartItemBuilder(data: widget.datas.elementAt(index))
                    : _emptyChartItemBuilder(),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _chartItemBuilder({required SaleAnalyMonthlyOutputModel data}) {
    double? maxGraphValue;
    double? minGraphValue;
    var maxItemsAmount = data.items.isEmpty
        ? 0
        : data.items
            .reduce(
              (v, e) => v.amount > e.amount ? v : e,
            )
            .amount;
    var maxLenght = maxItemsAmount.toInt().toString().length;
    var maxRound = maxItemsAmount / pow(10, maxLenght - 1);
    var maxCiel = maxRound.ceil();

    maxGraphValue = maxCiel * pow(10, maxLenght - 1).toDouble();

    double minItemsAmount = data.items.isEmpty
        ? 0
        : data.items
            .reduce(
              (v, e) => v.amount < e.amount ? v : e,
            )
            .amount;

    var minLenght = minItemsAmount.toInt().toString().length;
    var minRound = minItemsAmount / pow(10, minLenght - 2);
    var minCiel = minRound.floor();

    minGraphValue = minCiel * pow(10, minLenght - 2).toDouble();


    // if (maxItemsAmount > 10) {
    //   var b = 1;
    //   var a = 10.0;

    //   if (maxItemsAmount >= 100) {
    //     b = 10;
    //   } else if (maxItemsAmount >= 1000) {
    //     b = 100;
    //   } else if (maxItemsAmount >= 10000) {
    //     b = 1000;
    //   }

    //   if (maxItemsAmount >= 100) {
    //     a = (maxItemsAmount ~/ b) % 10;
    //     maxGraphValue = ((maxItemsAmount ~/ b) + ((b - a))).toDouble() * b;
    //   } else {
    //     a = (maxItemsAmount % 10);
    //     maxGraphValue = maxItemsAmount + ((10 - a));
    //   }

    //   intervalPrice = maxGraphValue / 100 * 25;
    // }

    return Container(
      padding: const EdgeInsets.fromLTRB(15, 10, 20, 15),
      margin: const EdgeInsets.symmetric(horizontal: 20),
      decoration: BoxDecoration(
          border: Border.all(color: AppColors.greyishDisable),
          borderRadius: BorderRadius.circular(10)),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          // Text(data.title),
          const SizedBox(height: 25),
          Expanded(
            child: LineChart(
              LineChartData(
                lineBarsData: [
                  LineChartBarData(
                      spots: data.itemsToFlSpot(),
                      belowBarData: BarAreaData(
                        show: true,
                        color: AppColors.textFieldBackground.withOpacity(0.8),
                      ),
                      barWidth: .3,
                      color: AppColors.primary,
                      dotData: FlDotData(
                        show: true,
                        getDotPainter: (p0, p1, p2, p3) => FlDotCirclePainter(
                          radius: 3,
                          strokeWidth: 0,
                          color: AppColors.primary,
                        ),
                      )),
                ],
                lineTouchData: LineTouchData(
                  enabled: true,
                  touchTooltipData: LineTouchTooltipData(
                    tooltipBgColor: AppColors.primaryLight,
                    showOnTopOfTheChartBoxArea: true,
                    tooltipMargin: 0,
                    tooltipPadding:
                        const EdgeInsets.symmetric(horizontal: 5, vertical: 2),
                    getTooltipItems: (touchedSpots) {
                      return touchedSpots.map((barSpot) {
                        var date = data.items.elementAt(barSpot.spotIndex).date;
                        return LineTooltipItem(
                            "${CurrencyConverter.currencyStringToSymble(AppBusinessTempInstant.selectedBusiness.accountCurrency)}${data.itemsToFlSpot().elementAt(barSpot.spotIndex).y.toThousandSeparatorString()}\n",
                            const TextStyle(
                              color: AppColors.primary,
                              fontWeight: FontWeight.bold,
                              fontSize: 10,
                            ),
                            children: [
                              TextSpan(
                                text:
                                    "${date.toFormatDateString("dd")} ${date.toFormatDateString("MMMM").toUpperCase().tr}",
                                style: const TextStyle(
                                    fontWeight: FontWeight.normal, fontSize: 8),
                              ),
                            ]);
                      }).toList();
                    },
                  ),
                ),
                // clipData: FlClipData(
                //     bottom: false, top: false, left: true, right: true),
                minY: minGraphValue > 0 ? 0 : minGraphValue>-50?-50:minGraphValue,
                minX: 1,
                maxY: data.items.isEmpty ? 30 : maxGraphValue,
                maxX: data.items.length < 5 ? 30 : null,
                titlesData: FlTitlesData(
                  leftTitles: AxisTitles(
                    sideTitles: SideTitles(
                      showTitles: true,
                      reservedSize:
                          (maxGraphValue.toThousandSeparatorString().length *10) + 15,
                      // interval: intervalPrice,
                      getTitlesWidget: (value, meta) {
                        return Text(
                            "${value < 0 ? '-' : ''}${CurrencyConverter.currencyStringToSymble(AppBusinessTempInstant.selectedBusiness.accountCurrency)}${value.toInt().abs().toThousandSeparatorString()}");
                      },
                    ),
                  ),
                  bottomTitles: AxisTitles(
                    sideTitles: SideTitles(
                      showTitles: true,
                      getTitlesWidget: (value, meta) => value == meta.max - 1
                          ? const SizedBox()
                          : Text("${value.toInt()}"),
                    ),
                  ),
                  rightTitles:
                      AxisTitles(sideTitles: SideTitles(showTitles: false)),
                  topTitles:
                      AxisTitles(sideTitles: SideTitles(showTitles: false)),
                ),
                borderData: FlBorderData(show: false),
                gridData: FlGridData(show: false),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _emptyChartItemBuilder() {
    return Container(
      padding: const EdgeInsets.fromLTRB(15, 10, 20, 15),
      margin: const EdgeInsets.symmetric(horizontal: 20),
      decoration: BoxDecoration(
          border: Border.all(color: AppColors.greyishDisable),
          borderRadius: BorderRadius.circular(10)),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          // Text(data.title),
          const SizedBox(height: 25),
          Expanded(
            child: LineChart(
              LineChartData(
                lineBarsData: [
                  LineChartBarData(
                      spots: [],
                      belowBarData: BarAreaData(
                        show: true,
                        color: AppColors.textFieldBackground.withOpacity(0.8),
                      ),
                      barWidth: .3,
                      color: AppColors.primary,
                      dotData: FlDotData(
                        show: true,
                        getDotPainter: (p0, p1, p2, p3) => FlDotCirclePainter(
                          radius: 3,
                          strokeWidth: 0,
                          color: AppColors.primary,
                        ),
                      )),
                ],
                lineTouchData: LineTouchData(
                  enabled: true,
                ),
                minY: 0,
                minX: 1,
                maxY: 3,
                maxX: 30,
                titlesData: FlTitlesData(
                  leftTitles: AxisTitles(
                    sideTitles: SideTitles(
                      showTitles: true,
                      reservedSize: 45,
                      getTitlesWidget: (value, meta) {
                        return Text(
                            "${CurrencyConverter.currencyStringToSymble(AppBusinessTempInstant.selectedBusiness.accountCurrency)}${value.toInt()}");
                      },
                    ),
                  ),
                  bottomTitles: AxisTitles(
                    sideTitles: SideTitles(
                      showTitles: true,
                      getTitlesWidget: (value, meta) => value == meta.max - 1
                          ? const SizedBox()
                          : Text("${value.toInt()}"),
                    ),
                  ),
                  rightTitles:
                      AxisTitles(sideTitles: SideTitles(showTitles: false)),
                  topTitles:
                      AxisTitles(sideTitles: SideTitles(showTitles: false)),
                ),
                borderData: FlBorderData(show: false),
                // gridData: FlGridData(show: false),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class EmptySaleAnalyticChartView extends StatelessWidget {
  EmptySaleAnalyticChartView({super.key});

  final controller = Get.find<TabHomeScreenController>();
  @override
  Widget build(BuildContext context) {
    return SizedBox(height: 260, width: Get.width, child: _emptyGraphBuilder());
  }

  Widget _emptyGraphBuilder() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Padding(
          padding: const EdgeInsets.only(bottom: 5, right: 20),
          child: Text(
            DateTime.now().toFormatDateString("MMMM").toUpperCase().tr,
            style: AppTextStyle.subtitle2,
          ),
        ),
        Expanded(child: _emptyChartItemBuilder())
      ],
    );
  }

  Widget _emptyChartItemBuilder() {
    return Container(
      padding: const EdgeInsets.fromLTRB(15, 10, 20, 15),
      margin: const EdgeInsets.symmetric(horizontal: 20),
      decoration: BoxDecoration(
          border: Border.all(color: AppColors.greyishDisable),
          borderRadius: BorderRadius.circular(10)),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          // Text(data.title),
          const SizedBox(height: 25),
          Expanded(
            child: LineChart(
              LineChartData(
                lineBarsData: [
                  LineChartBarData(
                      spots: [],
                      belowBarData: BarAreaData(
                        show: true,
                        color: AppColors.textFieldBackground.withOpacity(0.8),
                      ),
                      barWidth: .3,
                      color: AppColors.primary,
                      dotData: FlDotData(
                        show: true,
                        getDotPainter: (p0, p1, p2, p3) => FlDotCirclePainter(
                          radius: 3,
                          strokeWidth: 0,
                          color: AppColors.primary,
                        ),
                      )),
                ],
                lineTouchData: LineTouchData(
                  enabled: true,
                ),
                minY: 0,
                minX: 1,
                maxY: 4,
                maxX: 30,
                titlesData: FlTitlesData(
                  leftTitles: AxisTitles(
                    sideTitles: SideTitles(
                      showTitles: true,
                      reservedSize: 45,
                      getTitlesWidget: (value, meta) {
                        return Text(
                            "${CurrencyConverter.currencyStringToSymble(AppBusinessTempInstant.selectedBusiness.accountCurrency)}${value.toInt()}");
                      },
                    ),
                  ),
                  bottomTitles: AxisTitles(
                    sideTitles: SideTitles(
                      showTitles: true,
                      getTitlesWidget: (value, meta) => value == meta.max - 1
                          ? const SizedBox()
                          : Text("${value.toInt()}"),
                    ),
                  ),
                  rightTitles:
                      AxisTitles(sideTitles: SideTitles(showTitles: false)),
                  topTitles:
                      AxisTitles(sideTitles: SideTitles(showTitles: false)),
                ),
                borderData: FlBorderData(show: false),
                // gridData: FlGridData(show: false),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
