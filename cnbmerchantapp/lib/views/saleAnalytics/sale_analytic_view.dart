import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class SaleAnalyticView extends StatelessWidget {
  SaleAnalyticView({super.key});

  final controller = Get.find<TabHomeScreenController>();

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => SmartRefresher(
        controller: controller.homeRefreshController,
        onRefresh: () async {
          await controller.onSelectedYearFilterAsync(
              controller.availableYearsFilter.first,
              checkSelectedYear: false);
          controller.homeRefreshController.refreshCompleted();
        },
        child: SingleChildScrollView(
          physics: const BouncingScrollPhysics(),
          padding: const EdgeInsets.all(0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              ContainerDevider.blankSpaceMd,
              // InkWell(child: _filterdAmount(), onTap: controller.fetchSaleAnalyticDataAsync,),
              _filterdAmount(),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  _filterdTransaction(),
                  _filterYears(),
                ],
              ),
              ContainerDevider.blankSpaceMd,
              controller.canShowChat && controller.saleAnalyDatas.isNotEmpty
                  ? SaleAnalyticChartView(
                      datas: controller.saleAnalyDatas.toList(),
                    )
                  : EmptySaleAnalyticChartView(),
              ContainerDevider.blankSpaceMd,
              _totalSumaryWidget(
                  title: "NetSale".tr,
                  amount: controller.selectedMonthSale.totalAmountSale,
                  children: [
                    _samaryItemWidget(
                        title: "Outlets".tr,
                        value: "${controller.selectedMonthSale.totalOutlet} "),
                    _samaryItemWidget(
                        title: "Cashiers".tr,
                        value: "${controller.selectedMonthSale.totalCashier} "),
                    _samaryItemWidget(
                      title: "Transactions".tr,
                      value:
                          "${controller.selectedMonthSale.totalTransactionSale.toThousandSeparatorString()} ",
                    ),
                  ]),
              ContainerDevider.blankSpaceMd,
              _totalSumaryWidget(
                  title: "Refunds".tr,
                  amount: controller.selectedMonthSale.totalAmountRefund.abs(),
                  isNagativeValue:
                      controller.selectedMonthSale.totalAmountRefund <= 0,
                  children: [
                    _samaryItemWidget(
                      title: "Transactions".tr,
                      value:
                          "${controller.selectedMonthSale.totalTransactionRefund.toThousandSeparatorString()} ",
                    ),
                  ]),
              ContainerDevider.blankSpaceMd,
              _totalSumaryWidget(
                  title: "TodaySale".tr,
                  amount: controller.todaySaleAmount.abs(),
                  isNagativeValue: controller.todaySaleAmount < 0,
                  children: [
                    // _samaryItemWidget(
                    //     title: "Outlets".tr,
                    //     value: "${controller.latestMonthSale.totalOutlet} "),
                    // _samaryItemWidget(
                    //     title: "Cashiers".tr,
                    //     value: "${controller.latestMonthSale.totalCashier}  "),
                    _samaryItemWidget(
                      title: "Transactions".tr,
                      value:
                          "${controller.todaySaleTransaction.toThousandSeparatorString()} ",
                    ),
                  ]),
              ContainerDevider.blankSpaceMd,
            ],
          ),
        ),
      ),
    );
  }

  Widget _filterYears() {
    return InkWell(
      onTap: controller.selectYearFilter,
      child: Container(
        padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
        margin: const EdgeInsets.symmetric(horizontal: 20),
        decoration: BoxDecoration(
            color: AppColors.greyBackground,
            borderRadius: BorderRadius.circular(6)),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              controller.selectedYearFilter,
              style: AppTextStyle.caption.copyWith(
                  color: AppColors.textPrimary, height: 1, fontSize: 17),
            ),
            const SizedBox(width: 15),
            const Icon(
              MerchantIcon.chevronDown,
              size: 12,
              color: AppColors.textPrimary,
            )
          ],
        ),
      ),
    );
  }

  Widget _filterdAmount() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: RichText(
          text: TextSpan(
        // crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          TextSpan(
            text: CurrencyConverter.currencyStringToSymble(
                AppBusinessTempInstant.selectedBusiness.accountCurrency),
            style: TextStyle(
              fontFamily: GoogleFonts.roboto().fontFamily,
              color: AppColors.textSecondary,
              fontSize: 28,
              height: 1.1,
            ),
          ),
          // const SizedBox(width: 3),
          TextSpan(
            text: controller.selectedMonthSale.totalAmountSale
                .toThousandSeparatorString(d2: isUSDCurrency),
            style: const TextStyle(
              color: AppColors.textPrimary,
              fontSize: 26,
              fontWeight: FontWeight.w500,
            ),
          ),
        ],
      )),
    );
  }

  Widget _filterdTransaction() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Text.rich(
        TextSpan(
          style: AppTextStyle.body2.copyWith(color: AppColors.textPrimary),
          children: [
            TextSpan(
              text: "${controller.selectedMonthSale.totalTransactionSale} ",
              style: const TextStyle(fontWeight: FontWeight.w700),
            ),
            TextSpan(
              text: "Transactions".tr,
            ),
          ],
        ),
      ),
    );
  }

  Widget _totalSumaryWidget({
    required String title,
    double amount = 0,
    List<Widget>? children,
    bool isNagativeValue = false,
  }) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 15),
      margin: const EdgeInsets.symmetric(horizontal: 20),
      decoration: BoxDecoration(
          color: isNagativeValue && amount != 0
              ? AppColors.primary.withOpacity(0.05)
              : AppColors.textFieldBackground,
          borderRadius: BorderRadius.circular(15)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  title,
                  style: AppTextStyle.body2.copyWith(
                      color: AppColors.textPrimary,
                      fontWeight: FontWeight.w500),
                ),
                const SizedBox(height: 30),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    // Text(
                    //   CurrencyConverter.currencyStringToSymble(
                    //     AppBusinessTempInstant.selectedBusiness.accountCurrency,
                    //   ),
                    //   style:  TextStyle(
                    //     color: isNagativeValue ?AppColors.primary: AppColors.success,
                    //     fontSize: 25,
                    //     height: 0.9,
                    //     fontFamily: GoogleFonts.roboto().fontFamily,
                    //     fontWeight:
                    //         isUSDCurrency ? FontWeight.normal : FontWeight.w400,
                    //   ),
                    // ),
                    Icon(
                      isUSDCurrency
                          ? MerchantIcon.currencyUsd
                          : MerchantIcon.currencyKhr,
                      color: isNagativeValue
                          ? AppColors.primary
                          : AppColors.success,
                      size: 18,
                      shadows: [
                        Shadow(
                          offset: const Offset(0.5, 0.5),
                          blurRadius: 0,
                          color: isNagativeValue
                              ? AppColors.primary
                              : AppColors.success,
                        )
                      ],
                    ),
                    Expanded(
                      child: Text(
                        amount.toThousandSeparatorString(d2: isUSDCurrency),
                        style: TextStyle(
                          color: isNagativeValue
                              ? AppColors.primary
                              : AppColors.success,
                          fontSize: 25,
                          fontWeight: FontWeight.w500,
                          height: 0.9,
                        ),
                      ),
                    ),
                  ],
                ),
                // RichText(
                //   // overflow: TextOverflow.ellipsis,
                //   text: TextSpan(
                //       style:  TextStyle(
                //         color: isNagativeValue ?AppColors.primary: AppColors.success,
                //         fontSize: 25,
                //         fontWeight: FontWeight.w500,
                //         height: 0.9,
                //       ),
                //       children: [
                //         // if (isNagativeValue) ...[
                //         //   const TextSpan(text: '-'),
                //         // ],
                //         TextSpan(
                //           text: CurrencyConverter.currencyStringToSymble(
                //             AppBusinessTempInstant
                //                 .selectedBusiness.accountCurrency,
                //           ),
                //           style: TextStyle(
                //               fontFamily: GoogleFonts.roboto().fontFamily,
                //               fontWeight: isUSDCurrency
                //                   ? FontWeight.normal
                //                   : FontWeight.w400,
                //               // letterSpacing: -2
                //               ),
                //         ),
                //         TextSpan(text:"1992314141000098102993"+ amount.toThousandSeparatorString(d2: isUSDCurrency)),
                //       ]),
                // ),
                // Text(
                //   "${isNagativeValue ? '-' : ''}${CurrencyConverter.currencyStringToSymble(AppBusinessTempInstant.selectedBusiness.accountCurrency)}${amount.toThousandSeparatorString()}",
                //   style: const TextStyle(
                //     color: AppColors.textPrimary,
                //     fontSize: 25,
                //   ),
                // ),
              ],
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisAlignment: MainAxisAlignment.end,
            children: children ?? [],
          )
        ],
      ),
    );
  }

  Text _samaryItemWidget({String value = '', String title = ''}) {
    return Text.rich(TextSpan(style: AppTextStyle.caption, children: [
      TextSpan(
          text: value, style: const TextStyle(fontWeight: FontWeight.w700)),
      TextSpan(text: title)
    ]));
  }
}
