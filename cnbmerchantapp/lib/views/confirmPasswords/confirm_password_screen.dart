import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

class ConfirmPasswordScreen extends StatefulWidget {
  const ConfirmPasswordScreen({Key? key}) : super(key: key);

  @override
  State<ConfirmPasswordScreen> createState() => _ConfirmPasswordScreenState();
}

class _ConfirmPasswordScreenState extends State<ConfirmPasswordScreen> {
  final ConfirmPasswordScreenController _controller =
      Get.put(ConfirmPasswordScreenController());

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => ContainerAnimatedTemplate(
        title: _controller.title.tr,
        floatingActionButton: AppButtonStyle.primaryButton(
            label: "Next".tr,
            margin: const EdgeInsets.symmetric(horizontal: 15).r,
            isEnable: (_controller.password.isNotEmpty),
            onPressed: () => _controller.onSubmit()),
        child: _body(),
      ),
    );
  }

  Widget _body() {
    return SingleChildScrollView(
      physics: const AlwaysScrollableScrollPhysics(),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          SvgPicture.asset(
            SvgAppAssets.createNewPasswordLogo,
            height: 90.h,
          ),
          const SizedBox(
            height: 20,
          ),
          Text(
            _controller.subTitle.tr,
            textAlign: TextAlign.center,
            style: AppTextStyle.body1.copyWith(
                color: AppColors.textPrimary, fontWeight: FontWeight.w700),
          ),
          const SizedBox(
            height: 10,
          ),
          Text(
            "ConfirmPasswordLabel".tr,
            textAlign: TextAlign.center,
            style: AppTextStyle.body2.copyWith(color: AppColors.textSecondary),
          ),
          const SizedBox(
            height: 30,
          ),
          KTextField(
            name: "Password",
            label: "Password".tr,
            hint: "Password".tr,
            initialValue: '',
            validatorMessage: '',
            iconData: MerchantIcon.key,
            keyboardType: TextInputType.visiblePassword,
            onSubmit: (value) => _controller.onSubmit(),
            obscureText: !_controller.isPasswordVisible,
            controller: _controller.passwordTextController,
            onChanged: (value) => {_controller.password = value!},
            secondChild: _controller.password.isNotEmpty
                ? InkWell(
                    onTap: () => _controller.isPasswordVisible =
                        !_controller.isPasswordVisible,
                    child: Padding(
                      padding: const EdgeInsets.all(2.0),
                      child: Icon(
                        _controller.isPasswordVisible
                            ? Icons.visibility_off
                            : Icons.visibility,
                        color: AppColors.textPrimary,
                        size: 20,
                      ),
                    ),
                  )
                : null,
          ),
        ],
      ),
    );
  }
}
