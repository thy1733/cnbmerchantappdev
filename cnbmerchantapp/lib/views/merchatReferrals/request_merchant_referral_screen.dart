import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

class RequestMerchantReferralScreen extends StatefulWidget {
  const RequestMerchantReferralScreen({Key? key}) : super(key: key);

  @override
  State<RequestMerchantReferralScreen> createState() =>
      _RequestMerchantReferralScreenState();
}

class _RequestMerchantReferralScreenState
    extends State<RequestMerchantReferralScreen> {
  final controller = Get.put(RequestMerchantReferralScreenController());
  SizedBox get _seperator => const SizedBox(height: 15);
  double iconHeight = Get.width * 0.25;
  @override
  void initState() {
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Obx(
      () => ContainerAnimatedTemplate(
        title: 'MerchantReferral'.tr,
        floatingActionButton: AppButtonStyle.primaryButton(
          label: "Submit".tr,
          isEnable: controller.isEnable,
          onPressed: controller.onNextBtnPress,
          margin: const EdgeInsets.symmetric(horizontal: 15),
        ),
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            // #region ICON DESCRIPTION
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 20),
              child: SvgPicture.asset(SvgAppAssets.icRequestMerchantReferal,
                  height: iconHeight > 132 ? 132 : iconHeight),
            ),
            _seperator,
            Text(
              "SuggestUs".tr,
              textAlign: TextAlign.center,
              style: AppTextStyle.body1.copyWith(
                  color: AppColors.textPrimary, fontWeight: FontWeight.w700),
            ),
            _seperator,
            Text(
              "RequestMerchantReferralMsg".tr,
              style:
                  AppTextStyle.body2.copyWith(color: AppColors.textSecondary),
              textAlign: TextAlign.center,
            ),
            _seperator,
            // #endregion

            // #region INPUT
            KTextField(
              name: "BusinessName".tr,
              hint: "BusinessName".tr,
              iconData: MerchantIcon.user,
              textInputAction: TextInputAction.next,
              validatorMessage: controller.businessNameValidateMessage.tr,
              onChanged: (value) => {
                controller.reqInput.businessName = value ?? "",
                controller.businessNameValidator(),
              },
            ),
            KTextField(
              name: "ContactNumber".tr,
              hint: "XX XXX XXX",
              initialValue: "",
              prefixText: '+855 (0)',
              validatorMessage: controller.phoneValidateMessage.tr,
              iconData: MerchantIcon.telephone,
              keyboardType: TextInputType.phone,
              onChanged: (value) => {
                controller.reqInput.businessContact = value ?? "",
                controller.phoneValidator(),
              },
            ),
            businessLocationSection(),
            // #endregion
          ],
        ),
      ),
    );
  }

  Widget businessLocationSection() {
    return InkWell(
      onTap: () {
        controller.browseLocation();
      },
      child: KTextField(
        controller: controller.locationNameConroller,
        enabled: false,
        name: "Location".tr,
        hint: "SelectLocation".tr,
        validatorMessage: "",
        iconData: MerchantIcon.mapLocation,
        needIcon: false,
        secondChild: const Padding(
          padding: EdgeInsets.only(left: 10),
          child: Icon(
            MerchantIcon.mapLocation,
            size: 16,
          ),
        ),
      ),
    );
  }
}
