import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:cnbmerchantapp/core.dart';

class RequestQrMaterialScreen extends StatefulWidget {
  const RequestQrMaterialScreen({Key? key}) : super(key: key);

  @override
  State<RequestQrMaterialScreen> createState() =>
      _RequestQrMaterialScreenState();
}

class _RequestQrMaterialScreenState extends State<RequestQrMaterialScreen> {
  final RequestQRMaterialScreenController controller =
      Get.put(RequestQRMaterialScreenController());
  SizedBox get _seperator => const SizedBox(height: 10);
  final qrTypeDatas = QRMaterialTypeDataConst();

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => ContainerAnimatedTemplate(
        title: "RequestPrintedQr".tr,
        floatingActionButton: AppButtonStyle.primaryButton(
            label: "Next".tr,
            isEnable: controller.isEnable,
            onPressed: controller.reviewRequest,
            margin: const EdgeInsets.symmetric(horizontal: 15)),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("SelectQrType".tr, style: AppTextStyle.caption),
              _seperator,
              _qrMaterialTypeList(),
              _seperator,
              // BUSINESS
              ContainerButton(
                submit: (value) {
                  controller.selectBusiness();
                },
                child: DropdownOption(
                  title: "Business Profile".tr,
                  value: controller.selectedBusiness.id.isEmpty
                      ? "PleaseSelect".tr
                      : controller.selectedBusiness.businessName,
                  textColor: controller.selectedBusiness.id.isEmpty
                      ? AppColors.greyishHint
                      : AppColors.textPrimary,
                  // leadingIconData: MerchantIcon.businessType,
                ),
              ),
              if (controller.selectedBusiness.id.isNotEmpty) ...[
                ContainerButton(
                  submit: (value) {
                    controller.onSelectOutlet();
                  },
                  child: DropdownOption(
                    title: "Outlet".tr,
                    value: controller.selectedOutlet.id.isEmpty
                        ? "PleaseSelect".tr
                        : controller.selectedOutlet.outletName,
                    textColor: controller.selectedOutlet.id.isEmpty
                        ? AppColors.greyishHint
                        : AppColors.textPrimary,
                  ),
                ),
              ],
              ContainerButton(
                submit: (value) {
                  controller.selectPickUpOPtion();
                },
                child: DropdownOption(
                  title: "ReceiveOption".tr,
                  value: controller.recieverOptoin ==
                          QrMaterialRecieverOptionEnum.none
                      ? "PleaseSelect".tr
                      : controller.recieverOptoin.name.tr,
                  leadingIconData: MerchantIcon.pin,
                  textColor: controller.recieverOptoin ==
                          QrMaterialRecieverOptionEnum.none
                      ? AppColors.greyishHint
                      : AppColors.textPrimary,
                ),
              ),
              _seperator,
              controller.recieverOptoin == QrMaterialRecieverOptionEnum.Delivery
                  ? deliveryLocationSection()
                  : controller.recieverOptoin ==
                          QrMaterialRecieverOptionEnum.SelfPickUp
                      ? browseBankBranchSection()
                      : const SizedBox(),
              KTextField(
                name: "Note".tr,
                hint: "Note".tr,
                iconData: MerchantIcon.note,
                keyboardType: TextInputType.text,
                validatorMessage: "",
                onChanged: (t) {
                  controller.requestQrMaterialInput.remark = t ?? '';
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget deliveryLocationSection() {
    return InkWell(
      onTap: () {
        controller.browseDeliveryLocation();
      },
      child: KTextField(
        controller: controller.locationNameConroller,
        enabled: false,
        name: "Location".tr,
        hint: "SelectLocation".tr,
        validatorMessage: "",
        iconData: MerchantIcon.mapLocation,
        needIcon: false,
        secondChild: const Padding(
          padding: EdgeInsets.only(left: 10),
          child: Icon(
            MerchantIcon.mapLocation,
            size: 16,
          ),
        ),
      ),
    );
  }

  Widget browseBankBranchSection() {
    return InkWell(
      onTap: () {
        // controller.getBankOfficeLocationAsync();
        controller.browseBankBranchLocation();
      },
      child: KTextField(
        controller: controller.bankBranchConroller,
        enabled: false,
        name: "PickUpLocation".tr,
        hint: "PickUpLocation".tr,
        initialValue: controller.requestQrMaterialInput.pickUpLocation.locationName,
        validatorMessage: "",
        needIcon: false,
        secondChild: const Padding(
          padding: EdgeInsets.only(left: 10),
          child: Icon(
            MerchantIcon.mapLocation,
            size: 16,
          ),
        ),
      ),
    );
  }

  Widget _qrMaterialTypeList() {
    return ListView(
      padding: EdgeInsets.zero,
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      children: [
        _qrTypeItem(
          qrTypeDatas.standee,
          qty: controller.requestQrMaterialInput.standee,
          onDecreese: () {
            controller.requestQrMaterialInput.standee--;
            controller.checkIsEnable();
          },
          onInCreese: () {
            controller.requestQrMaterialInput.standee++;
            controller.checkIsEnable();
          },
        ),
        _seperator,
        _qrTypeItem(
          qrTypeDatas.stickerBillPad,
          qty: controller.requestQrMaterialInput.stickerBillPad,
          onDecreese: () {
            controller.requestQrMaterialInput.stickerBillPad--;
            controller.checkIsEnable();
          },
          onInCreese: () {
            controller.requestQrMaterialInput.stickerBillPad++;
            controller.checkIsEnable();
          },
        ),
        _seperator,
        _qrTypeItem(
          qrTypeDatas.lanyard,
          qty: controller.requestQrMaterialInput.lanyard,
          onDecreese: () {
            controller.requestQrMaterialInput.lanyard--;
            controller.checkIsEnable();
          },
          onInCreese: () {
            controller.requestQrMaterialInput.lanyard++;
            controller.checkIsEnable();
          },
        ),
      ],
    );
  }

  Widget _qrTypeItem(
    QRMaterialTypeModel data, {
    required VoidCallback onDecreese,
    required VoidCallback onInCreese,
    int qty = 0,
  }) {
    return Row(
      mainAxisSize: MainAxisSize.max,
      children: [
        Container(
          height: 40,
          width: 40,
          clipBehavior: Clip.hardEdge,
          decoration: const BoxDecoration(
              color: AppColors.primaryLight, shape: BoxShape.circle),
          child: AppNetworkImage(data.imageUrl),
        ),
        const SizedBox(width: 15),
        Expanded(
          child: Text(
            data.name,
            style: const TextStyle(
                color: AppColors.darkGrey,
                fontSize: 14,
                fontWeight: FontWeight.w700),
          ),
        ),
        qtyBtn(
            ids: "${data.name}qtyminus",
            iconData: MerchantIcon.minus,
            onTap: onDecreese,
            isEnable: qty > 0),
        const SizedBox(width: 15),
        Text(
          "$qty",
          style: const TextStyle(
              color: AppColors.darkGrey,
              fontSize: 14,
              fontWeight: FontWeight.w700),
        ),
        const SizedBox(width: 15),
        qtyBtn(
            ids: "${data.name}qtyplus",
            iconData: MerchantIcon.plus,
            onTap: onInCreese,
            isEnable: qty < ValueConst.requestQrMaterialItemMax),
      ],
    );
  }

  Widget qtyBtn(
      {required dynamic ids,
      required IconData iconData,
      required VoidCallback onTap,
      bool isEnable = false}) {
    return FloatingActionButton.small(
      backgroundColor: AppColors.white,
      onPressed: isEnable ? onTap : null,
      elevation: 0,
      heroTag: ids,
      child: Icon(
        iconData,
        color: isEnable ? AppColors.primary : AppColors.grey,
        size: 13,
      ),
    );
  }
}
