import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class RequestQrMaterialListScreen extends StatefulWidget {
  const RequestQrMaterialListScreen({super.key});

  @override
  State<RequestQrMaterialListScreen> createState() =>
      _RequestQrMaterialListScreenState();
}

class _RequestQrMaterialListScreenState
    extends State<RequestQrMaterialListScreen> {
  final controller = Get.put(RequestPrintedQrListScreenController());
  final qrTypeDatas = QRMaterialTypeDataConst();

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => ContainerAnimatedTemplate(
        title: 'RequestPrintedQr'.tr,
        needBottomSpace: false,
        appbarActions: [
          if (AppBusinessTempInstant.appCacheBusiness.items.isNotEmpty) ...[
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 7),
              child: ElevatedButton(
                onPressed: () => controller.onRequestQrMaterial(),
                style: AppButtonStyle.kSecondaryButtonStyle(
                  borderRadius: BorderRadius.circular(10),
                  backgroundColor: AppColors.white.withOpacity(0.2),
                ),
                child: Text("RequestQR".tr),
              ),
            )
          ]
        ],
        child: SmartRefresher(
            controller: controller.refreshController,
            onRefresh: () => controller.onRefresh(),
            child: controller.requestPrintedQrDatas.isEmpty &&
                    controller.isLoading == false
                ? _noDataView()
                : _requestPrintedQrListBuilder()),
      ),
    );
  }

  Widget _requestPrintedQrListBuilder() {
    return ListView.builder(
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      padding: EdgeInsets.zero,
      itemCount: controller.requestPrintedQrDatas.length,
      itemBuilder: (context, index) {
        return _requestPrintedQrItemBuilder(
          controller.requestPrintedQrDatas.elementAt(index),
          isLastItem: controller.requestPrintedQrDatas.length - 1 == index,
        );
      },
    );
  }

  Widget _requestPrintedQrItemBuilder(ReqPrintedQrDetailOutputModel data,
      {bool isLastItem = false}) {
    return InkWell(
      onTap: () {
        controller.onReviewRequestMaterial(data);
      },
      child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          children: [
            ClipOval(
              child: AppNetworkImage(
                data.outletLogoUrl,
                width: 50,
                height: 50,
                errorWidget: Container(
                  color: AppColors.primaryLight,
                  padding: const EdgeInsets.all(15.0),
                  child: SvgPicture.asset(
                    SvgAppAssets.icDefaultShop,
                    color: AppColors.primary,
                  ),
                ),
              ),
            ),
            const SizedBox(width: 10),
            Expanded(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Text(
                    data.requestDisplayName,
                    style: AppTextStyle.subtitle1,
                  ),
                  const SizedBox(height: 2),
                  Row(
                    children: [
                      KStatusWidget(
                        status: data.requestStatus,
                      ),
                      const SizedBox(width: 5),
                      Expanded(
                        child: _qrTypePath(data),
                      )
                    ],
                  ),
                  const SizedBox(height: 2),
                  Text(
                    kDateTimeFormat(data.requestDateTime),
                    style: AppTextStyle.caption
                        .copyWith(color: AppColors.textSecondary),
                  ),
                  const SizedBox(
                    height: 5,
                  ),
                  Visibility(
                      visible: !isLastItem, // replace with last index
                      child: Divider(
                        thickness: 1,
                        indent: 1,
                        color: AppColors.darkGrey.withOpacity(.1),
                      )),
                  const SizedBox(
                    height: 5,
                  ),
                ],
              ),
            )
          ]),
    );
  }

  // SELECTED_QR_TYPE
  Widget _qrTypePath(ReqPrintedQrDetailOutputModel data) {
    var typeStringDatas = <String>[];
    var typeDatas = <Text>[];
    if (data.standee > 0) {
      typeStringDatas.add("${qrTypeDatas.standee.name} x${data.standee}");
    }
    if (data.stickerBillPad > 0) {
      typeStringDatas
          .add("${qrTypeDatas.stickerBillPad.name} x${data.stickerBillPad}");
    }
    if (data.lanyard > 0) {
      typeStringDatas.add("${qrTypeDatas.lanyard.name} x${data.lanyard}");
    }
    for (int i = 0; i < typeStringDatas.length; i++) {
      String seperator = i == typeStringDatas.length - 1 ? "" : ", ";
      typeDatas.add(Text("${typeStringDatas.elementAt(i)}$seperator"));
    }

    return Wrap(
      runAlignment: WrapAlignment.start,
      alignment: WrapAlignment.start,
      spacing: 3,
      children: typeDatas,
    );
  }

  Widget _noDataView() => SizedBox(
        height: 500,
        child: NoDataView(
          message: "NoResult".tr,
          description: "YouDontHaveAnyRequestYet".tr,
          imageAset: SvgAppAssets.icQrStand,
        ),
      );
}
