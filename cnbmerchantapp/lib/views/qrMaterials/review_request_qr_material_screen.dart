import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:cnbmerchantapp/core.dart';

class ReviewRequestQrMaterialScreen extends StatefulWidget {
  const ReviewRequestQrMaterialScreen({Key? key}) : super(key: key);

  @override
  State<ReviewRequestQrMaterialScreen> createState() =>
      _ReviewRequestQrMaterialScreenState();
}

class _ReviewRequestQrMaterialScreenState
    extends State<ReviewRequestQrMaterialScreen> {
  final controller = Get.put(ReviewRequestQRMaterialScreenController());
  SizedBox get separater => const SizedBox(height: 15);
  final qrTypeDatas = QRMaterialTypeDataConst();

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => ContainerAnimatedTemplate(
        title: 'Review'.tr,
        floatingActionButton:
            controller.fromPage == Routes.requestQrMaterialScreen
                ? AppButtonStyle.primaryButton(
                    label: "Confirm".tr,
                    margin: const EdgeInsets.symmetric(horizontal: 15),
                    onPressed: controller.onNextBtnPress,
                  )
                : null,
        child: _body(),
      ),
    );
  }

  Widget _body() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        businessData(),
        separater,
        _qrTypePath(controller.requestQrMaterialOutput),
        separater,
        if (controller.requestQrMaterialOutput.selectedRecieveOption ==
            QrMaterialRecieverOptionEnum.Delivery) ...[
          deliveryPath()
        ] else ...[
          _pickUpPath()
        ],
        separater,
        map(),
        separater,
        notePath(),
        separater,
        Container(height: .3, color: AppColors.greyishHint),
        separater,
        costPath(),
        separater,
        separater,
        infoPath()
      ],
    );
  }

  Widget infoPath() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        const Icon(
          MerchantIcon.signInfo,
          size: 15,
          color: AppColors.darkGrey,
        ),
        const SizedBox(width: 5),
        Text(
          "RequestPritedQrProceesingDayMessage".tr,
          style: AppTextStyle.caption,
        )
      ],
    );
  }

  // COST_PATH
  Widget costPath() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      mainAxisSize: MainAxisSize.min,
      children: [
        Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            costTextLabel("${"PrintedQr".tr}:"),
            costTextLabel(
                "${controller.requestQrMaterialOutput.printFee.fromGenricToString("#,##0.00")} USD"),
          ],
        ),
        const SizedBox(height: 5),
        Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            costTextLabel("${"DeliveryFee".tr}:"),
            costTextLabel(
                "${controller.requestQrMaterialOutput.deliveryFee.fromGenricToString("#,##0.00")} USD"),
          ],
        ),
        const SizedBox(height: 5),
        Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            costTextValue("${"Total".tr}:"),
            costTextValue(
                "${controller.requestQrMaterialOutput.totalFee.fromGenricToString("#,##0.00")} USD"),
          ],
        ),
      ],
    );
  }

  // NOTE
  Widget notePath() {
    return Visibility(
      visible: controller.requestQrMaterialOutput.remark.isNotEmpty,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          sessionTitle("Note".tr),
          const SizedBox(height: 3),
          Text(
            controller.requestQrMaterialOutput.remark,
            style: const TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.w400,
                color: Color.fromRGBO(14, 22, 44, 0.6)),
          ),
        ],
      ),
    );
  }

  // DELIVERY
  Widget deliveryPath() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        sessionTitle("DeliveryLocation".tr),
        const SizedBox(height: 3),
        Text(
          controller.requestQrMaterialOutput.locationDeliver.address,
          style: const TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.w400,
              color: Color.fromRGBO(14, 22, 44, 0.6)),
        ),
      ],
    );
  }

  // SELF_PICKUP
  Widget _pickUpPath() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        sessionTitle("PickUpLocation".tr),
        const SizedBox(height: 3),
        Text(
          "${controller.requestQrMaterialOutput.pickUpLocation.locationName} - ${controller.requestQrMaterialOutput.pickUpLocation.locationAddress}",
          style: AppTextStyle.subtitle2,
        ),
      ],
    );
  }

  Widget map() {
    return InkWell(
      onTap: () => controller.openMapAsync(),
      child: SizedBox(
        height: 130,
        child: controller.isLoading
            ? const KLoadingIndicator()
            : GoogleMap(
                myLocationButtonEnabled: false,
                myLocationEnabled: false,
                scrollGesturesEnabled: true,
                zoomGesturesEnabled: true,
                markers: controller.markers,
                onMapCreated: (GoogleMapController controllerMap) {
                  if (!controller.mapController.isCompleted) {
                    controller.mapController.complete(controllerMap);
                  }
                },
                onTap: (latLng) => controller.openMapAsync(),
                initialCameraPosition: const CameraPosition(
                    target: LatLng(11.312, 12.13231), zoom: 13)),
      ),
    );
  }

  // SELECTED_QR_TYPE
  Widget _qrTypePath(ReqPrintedQrDetailOutputModel data) {
    var typeStringDatas = <String>[];
    var typeDatas = <Text>[];
    if (data.standee > 0) {
      typeStringDatas.add("${qrTypeDatas.standee.name} x${data.standee}");
    }
    if (data.stickerBillPad > 0) {
      typeStringDatas
          .add("${qrTypeDatas.stickerBillPad.name} x${data.stickerBillPad}");
    }
    if (data.lanyard > 0) {
      typeStringDatas.add("${qrTypeDatas.lanyard.name} x${data.lanyard}");
    }
    for (int i = 0; i < typeStringDatas.length; i++) {
      String seperator = i == typeStringDatas.length - 1 ? "" : ", ";
      typeDatas.add(Text(
        "${typeStringDatas.elementAt(i)}$seperator",
        style: AppTextStyle.subtitle2,
      ));
    }
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        sessionTitle("Orders".tr),
        Wrap(
          runAlignment: WrapAlignment.start,
          alignment: WrapAlignment.start,
          spacing: 3,
          children: typeDatas,
        ),
      ],
    );
  }

  // BUSINESS_DATA
  Widget businessData() {
    return Padding(
      padding: const EdgeInsets.all(5.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          ClipOval(
            child: AppNetworkImage(
              controller.requestQrMaterialOutput.outletLogoUrl,
              height: 70,
              width: 70,
              errorWidget: Container(
                color: AppColors.primaryLight,
                padding: const EdgeInsets.all(20.0),
                child: SvgPicture.asset(
                  SvgAppAssets.icDefaultShop,
                  color: AppColors.primary,
                ),
              ),
            ),
          ),
          const SizedBox(width: 15),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                controller.requestQrMaterialOutput.requestDisplayName,
                style: AppTextStyle.header1,
              ),
              Text(
                "${"LinkedToAccount".tr}: ${controller.requestQrMaterialOutput.linkAccount.maskAccountNumber()}",
                style: AppTextStyle.caption,
              ),
              Text(
                controller.requestQrMaterialOutput.businessTypeName,
                style: AppTextStyle.caption,
              ),
            ],
          ),
        ],
      ),
    );
  }

  Text costTextLabel(String text) => Text(
        text,
        style: AppTextStyle.subtitle2,
      );

  Text costTextValue(String text) => Text(
        text,
        style: AppTextStyle.subtitle2.copyWith(fontWeight: FontWeight.w700),
      );

  Widget sessionTitle(String text) => Text(
        text,
        style: AppTextStyle.subtitle2.copyWith(fontWeight: FontWeight.w700),
      );
}
