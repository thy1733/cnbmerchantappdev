import 'package:cnbmerchantapp/controllers/qrScanner/qr_scanner_screen_controller.dart';
import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';

class QrScannerScreen extends StatefulWidget {
  const QrScannerScreen({super.key});

  @override
  State<QrScannerScreen> createState() => _QrScannerScreenState();
}

class _QrScannerScreenState extends State<QrScannerScreen> {
  final controller = Get.put(QrScannerScreenController());
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Scaffold(
        backgroundColor: AppColors.black,
        body: controller.isPermissionAllowed == false && controller.isAppActive
            ? _noPermissionBuilder()
            : _qrScanBuilder(),
      ),
    );
  }

  Widget _qrScanBuilder() => Stack(
        alignment: Alignment.topCenter,
        children: <Widget>[
          if (controller.canStartScanner) ...[
            // MobileScanner(
            //     allowDuplicates: true,
            //     controller: controller.mcontroller,
            //     onDetect: (barcode, args) {
            //       if(!controller.isScanner){
            //          controller.qrCodeScanningResult(barcode.rawValue ?? '');
            //       }
            //     }),
          ],
          SafeArea(
            child: Padding(
              padding: const EdgeInsets.all(15.0),
              child: Column(
                children: [
                  const Align(
                    alignment: Alignment.topLeft,
                    child: ContainerBlur(
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                      ),
                      child: CloseBtn(
                        iconColor: AppColors.white,
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: Align(
                      alignment: Alignment.bottomCenter,
                      child: ContainerBlur(
                        constraints: BoxConstraints(maxWidth: scanArea),
                        padding: const EdgeInsets.fromLTRB(15, 5, 15, 7),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color: AppColors.black.withOpacity(0.2),
                        ),
                        child: Text(
                          'ScanToReceivePayment'.tr,
                          textAlign: TextAlign.center,
                          style: AppTextStyle.caption
                              .copyWith(color: AppColors.white),
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(height: 15),
                  Center(
                    child: Lottie.asset(
                      JsonAppAssets.qrScanner,
                      animate: controller.isAppActive,
                      height: 250.h,
                      width: 250.h,
                    ),
                  ),
                  Expanded(
                    flex: 3,
                    child: controller.qrValue.isNotEmpty
                        ? Center(
                            child: ContainerBlur(
                              padding: const EdgeInsets.symmetric(
                                horizontal: 15,
                                vertical: 5,
                              ),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                color: AppColors.black.withOpacity(0.2),
                              ),
                              child: Text(
                                "Result: ${controller.qrValue}",
                                style: AppTextStyle.caption
                                    .copyWith(color: AppColors.white),
                              ),
                            ),
                          )
                        : const SizedBox(),
                  ),
                  controller.isAllowFlash
                      ? InkWell(
                          onTap: () async {
                            // if (controller.mcontroller != null) {
                            //   await controller.mcontroller!.toggleTorch();
                            //   controller.isFlashOn =
                            //       controller.mcontroller!.torchState.value ==
                            //               TorchState.on
                            //           ? true
                            //           : false;
                            // }
                          },
                          child: ContainerBlur(
                            decoration: const BoxDecoration(
                              shape: BoxShape.circle,
                            ),
                            child: Container(
                              padding: const EdgeInsets.all(20),
                              color: controller.isFlashOn
                                  ? AppColors.white.withOpacity(.5)
                                  : AppColors.transparent,
                              child: Icon(
                                Icons.flashlight_on,
                                color: controller.isFlashOn
                                    ? AppColors.black
                                    : AppColors.white,
                                size: 23,
                              ),
                            ),
                          ),
                        )
                      : const SizedBox(
                          height: 60,
                        ),
                ],
              ),
            ),
          ),
        ],
      );

  Widget _noPermissionBuilder() {
    return Stack(children: [
      // fake background
      ContainerTemp(
          header: const SizedBox(height: 90), child: const SizedBox()),
      // bluring background
      ContainerBlur(
        decoration: BoxDecoration(
          color: Colors.black.withOpacity(0.5),
        ),
        padding: const EdgeInsets.all(20),
        child: SafeArea(
          child: Column(
            children: [
              Align(
                alignment: Alignment.topLeft,
                child: ContainerBlur(
                  padding: EdgeInsets.zero,
                  decoration: BoxDecoration(
                      border: Border.all(),
                      shape: BoxShape.circle,
                      color: AppColors.white),
                  child: const CloseBtn(
                    iconColor: AppColors.white,
                  ),
                ),
              ),
              const SizedBox(height: 50),
              Lottie.asset(
                JsonAppAssets.signErrorAlert,
                repeat: false,
                height: 140.h,
                width: 140.h,
              ),
              ContainerDevider.blankSpaceMd,
              Text(
                "CantAccessCamera".tr,
                style: AppTextStyle.body1.copyWith(
                    color: AppColors.white, fontWeight: FontWeight.bold),
              ),
              ContainerDevider.blankSpaceSm,
              Text(
                "CameraPermissionDenied".tr,
                style: AppTextStyle.caption.copyWith(color: AppColors.white),
              ),
              const Expanded(child: SizedBox()),
              AppButtonStyle.secondaryButton(
                  label: "GoToSetting".tr,
                  onPressed: () {
                    controller.openAppSettingAsync();
                  })
            ],
          ),
        ),
      )
    ]);
  }

  var scanArea = (MediaQuery.of(Get.context!).size.width < 400 ||
          MediaQuery.of(Get.context!).size.height < 400)
      ? 150.0
      : 250.0;

  cameraButonStyle() {
    return ButtonStyle(
        backgroundColor:
            MaterialStateProperty.all(AppColors.black.withOpacity(0.2)),
        elevation: MaterialStateProperty.all(0),
        shape: MaterialStateProperty.all(
            const CircleBorder(side: BorderSide.none)),
        padding: MaterialStateProperty.all(const EdgeInsets.all(20)));
  }
}
