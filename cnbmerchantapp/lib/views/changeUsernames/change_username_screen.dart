import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

class ChangeUsernameScreen extends StatefulWidget {
  const ChangeUsernameScreen({Key? key}) : super(key: key);

  @override
  State<ChangeUsernameScreen> createState() => _ChangeUsernameScreenState();
}

class _ChangeUsernameScreenState extends State<ChangeUsernameScreen> {
  final ChangeUsernameScreenController _controller =
      Get.put(ChangeUsernameScreenController());

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => ContainerAnimatedTemplate(
        title: "ChangeUsername".tr,
        floatingActionButton: _nextBtn(),
        padding: EdgeInsets.zero,
        child: _body(),
      ),
    );
  }

  Widget _body() {
    return SingleChildScrollView(
      physics: const AlwaysScrollableScrollPhysics(),
      padding: const EdgeInsets.fromLTRB(15, 15, 15, 0).r,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          SvgPicture.asset(
            SvgAppAssets.changeUsernameLogo,
            height: 90.h,
          ),
          Padding(
            padding: const EdgeInsets.only(top: 20, bottom: 10).r,
            child: Text("UpdateLoginCredential".tr,
                textAlign: TextAlign.center,
                style: AppTextStyle.body1.copyWith(
                    color: AppColors.textPrimary, fontWeight: FontWeight.w700)),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 30).r,
            child: Text(
              "UpdateLoginCredentialLabel".tr,
              textAlign: TextAlign.center,
              style: AppTextStyle.body2.copyWith(
                color: AppColors.textSecondary,
              ),
            ),
          ),
          KTextField(
            name: "NewUsername",
            label: "NewUsername".tr,
            hint: "NewUsername".tr,
            inputFormatters: [
              FilteringTextInputFormatter.deny(RegExp(r" ")),
              CustomLengthLimitingTextInputFormatter(32)
            ],
            validatorMessage: _controller.userNameValidate,
            focusNode: _controller.usernameFocusNode,
            textInputAction: TextInputAction.next,
            iconData: MerchantIcon.user,
            keyboardType: TextInputType.name,
            autocorrect: false,
            onChanged: (value) {
              _controller.userName = value ?? "";
              _controller.onUserNameChange();
            },
            onSubmit: (value) {
              _controller.confirmUsernameFocusNode.requestFocus();
            },
          ),
          KTextField(
            name: "ConfirmNewUsername",
            label: "ConfirmNewUsername".tr,
            hint: "ConfirmNewUsername".tr,
            inputFormatters: [
              FilteringTextInputFormatter.deny(RegExp(r" ")),
              CustomLengthLimitingTextInputFormatter(32)
            ],
            validatorMessage: _controller.confirmUserNameValidate,
            focusNode: _controller.confirmUsernameFocusNode,
            keyboardType: TextInputType.name,
            autocorrect: false,
            iconData: MerchantIcon.user,
            onChanged: (value) {
              _controller.confirmedUserName = value ?? "";
              _controller.onConfirmsUserNameChange();
            },
            onSubmit: (value) {
              _controller.onSubmit();
            },
          ),
          SizedBox(
            height: 20.r,
          ),
        ],
      ),
    );
  }

  Widget _nextBtn() {
    return AppButtonStyle.primaryButton(
      label: "Next".tr,
      margin: const EdgeInsets.symmetric(horizontal: 15).r,
      isEnable: _controller.isNextButtonEnabled,
      onPressed: () => _controller.onSubmit(),
    );
  }
}
