import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ActivateTermConditionScreen extends StatefulWidget {
  const ActivateTermConditionScreen({Key? key}) : super(key: key);

  @override
  ActivateTermConditionScreenState createState() =>
      ActivateTermConditionScreenState();
}

class ActivateTermConditionScreenState
    extends State<ActivateTermConditionScreen> {
  final controller = Get.put(ActivateTermConditionScreenController());

  @override
  Widget build(BuildContext context) {
    return ContainerTemp(
      header: NavigationTitle(title: 'TermAndCondition'.tr),
      isTopSafe: true,
      resizeToAvoidBottomInset: true,
      child: Obx(
        () => Column(
          children: [
            Expanded(
              child: controller.isOnline && !controller.loadWebViewError
                  ? KWebViewWidget(
                      url: controller.url,
                      onStart: () => controller.showLoading(),
                      onStop: () => controller.dismiss(),
                      onError: () async {
                        controller.dismiss();
                        controller.loadWebViewError = true;
                      },
                    )
                  : KLocalWebViewWidget(
                      fileUrl: FileAssets.activateTermCondition,
                      onStop: () => controller.dismiss(),
                      onError: () => controller.dismiss(),
                    ),
            ),
            _agreementButton(),
          ],
        ),
      ),
    );
  }

  Widget _agreementButton() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        ContainerDevider.vertical(
            padding: EdgeInsets.zero,
            color: AppColors.textSecondary.withOpacity(0.4)),
        ContainerDevider.blankSpaceSm,
        CRoundCheckBoxWidget(
          isSelected: controller.termsAggree.elementAt(0),
          onChange: (value) => controller.checkChange(0),
          value: 'accept_terms',
          text: "TermReadAndAccept".tr,
          underlineText: "Canadia Merchant App General Term & Conditions. ",
          underlineAction: () => controller.onConditionPress(),
        ),
        ContainerDevider.blankSpaceSm,
        CRoundCheckBoxWidget(
          isSelected: controller.termsAggree.elementAt(1),
          onChange: (value) => controller.checkChange(1),
          value: 'accept_agreement_terms',
          text: "TermReadAndAccept".tr,
          underlineText: 'Canadia Merchant App Agreements. ',
          underlineAction: () => controller.onAgreementPress(),
        ),
        ContainerDevider.blankSpaceMd,
        ContainerDevider.vertical(
            padding: const EdgeInsets.symmetric(horizontal: 15),
            color: AppColors.textSecondary.withOpacity(0.4)),
        ContainerDevider.blankSpaceSm,
        CRoundCheckBoxWidget(
          isSelected: controller.isEnable,
          onChange: (value) => controller.checkChange(2),
          value: 'accept_all',
          text: 'TermAgree'.tr,
        ),
        ContainerDevider.blankSpaceMd,
        AppButtonStyle.primaryButton(
            isEnable: controller.isEnable,
            label: "Next".tr,
            margin: const EdgeInsets.symmetric(horizontal: 15),
            onPressed: () {
              controller.onNextBtnPress();
            }),
        const SizedBox(height: 20)
      ],
    );
  }
}
