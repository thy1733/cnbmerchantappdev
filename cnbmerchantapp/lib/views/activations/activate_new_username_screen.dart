import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

class ActivateNewUsernameScreen extends StatefulWidget {
  const ActivateNewUsernameScreen({super.key});

  @override
  State<ActivateNewUsernameScreen> createState() =>
      _ActivateNewUsernameScreenState();
}

class _ActivateNewUsernameScreenState extends State<ActivateNewUsernameScreen> {
  final ActivateCreateUsernameScreenController controller =
      Get.put(ActivateCreateUsernameScreenController());
  SizedBox get _seperator => const SizedBox(height: 15);
  double iconHeight = Get.width * 0.25;

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => ContainerAnimatedTemplate(
        title: controller.title.tr,
        floatingActionButton: AppButtonStyle.primaryButton(
          label: "Next".tr,
          isEnable: controller.isEnable,
          onPressed: controller.onNextBtnPress,
          margin: const EdgeInsets.symmetric(horizontal: 15),
        ),
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            // ICON DESCRIPTION
            ContainerDevider.blankSpaceLg,
            SvgPicture.asset(
              SvgAppAssets.usernameLogo,
              height: iconHeight.h,
            ),
            ContainerDevider.blankSpaceLg,
            Text(
              "CreateNewUsername".tr,
              style: AppTextStyle.subtitle1,
              textAlign: TextAlign.center,
            ),
            _seperator,
            Text(
              controller.captionMessage.tr,
              style: AppTextStyle.caption,
              textAlign: TextAlign.center,
            ),
            _seperator,
            // INPUT
            Visibility(
              visible: controller.isNeedSelectBankAccount,
              child: Opacity(
                opacity: controller.enableSelectBankAccount ?1:0.5,
                child: ContainerButton(
                  submit: (value) {
                    controller.onSelectBankAcountAsync();
                  },
                  isEnable: controller.enableSelectBankAccount,
                  child:DropdownOption(
                      title: "SelectAccount".tr,
                      value: "SelectAccount".tr,
                      leadingIconData: MerchantIcon.user,
                      color: Colors.transparent,
                      child: controller.bankAccountList.items.isEmpty
                          ? null
                          : Text.rich(
                              TextSpan(
                                style: AppTextStyle.value,
                                children: [
                                  TextSpan(
                                    text:
                                        "${controller.bankAccountList.items.elementAt(controller.selectedBankAccountIndex).accountName.tr} ",
                                    style: const TextStyle(
                                      fontWeight: FontWeight.w300,
                                    ),
                                  ),
                                  TextSpan(
                                    text: controller.bankAccountList.items
                                        .elementAt(
                                            controller.selectedBankAccountIndex)
                                        .accountNumber
                                        .maskAccountNumber(),
                                    style: const TextStyle(
                                      fontWeight: FontWeight.w700,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                    ),
                ),
              ),
            ),
            _seperator,
            KTextField(
              name: "Username",
              label: "Username".tr,
              hint: "Username".tr,
              validatorMessage: controller.usernameValidateMessage.tr,
              iconData: MerchantIcon.user,
              keyboardType: TextInputType.name,
              maxLength: 32,
              maxLine: 1,
              autocorrect: false,
              enableSuggestions: false,
              inputFormatters: [
                FilteringTextInputFormatter.allow(RegExp('[a-z A-Z 0-9]'))
              ],
              onChanged: (value) => {
                controller.registerAccountInput.userName = value ?? "",
                controller.usernameValidator(),
              },
            ),
            _usernameValidateHint
          ],
        ),
      ),
    );
  }

  Widget get _usernameValidateHint => Padding(
        padding: const EdgeInsets.only(left: 8),
        child: Text.rich(
          TextSpan(
            style: const TextStyle(
              fontSize: 14,
              color: Color(0xFF9D9D9D),
              height: 1.3,
              fontWeight: FontWeight.w400,
            ),
            children: [
              TextSpan(text: 'UsernameMustBe'.tr),
              TextSpan(
                text: '\n    •  ${'Between5and32Characters'.tr}',
              ),
              TextSpan(
                text: '\n    •  ${'NoSpace'.tr}',
              ),
            ],
          ),
          textAlign: TextAlign.start,
        ),
      );
}
