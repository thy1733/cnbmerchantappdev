// ignore_for_file: deprecated_member_use

import 'dart:io';

import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';

class ActivateOwnerScreen extends StatefulWidget {
  const ActivateOwnerScreen({Key? key}) : super(key: key);

  @override
  State<ActivateOwnerScreen> createState() => _ActivateOwnerScreenState();
}

class _ActivateOwnerScreenState extends State<ActivateOwnerScreen> {
  final ActivateOwnerScreenController controller =
      Get.put(ActivateOwnerScreenController());
  double iconHeight = Get.width * 0.25;

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => ContainerAnimatedTemplate(
        title: "ActivateAsBusinessOwner".tr,
        needBottomSpace: true,
        resizeToAvoidBottomInset: false,
        floatingActionButton: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            if (controller.activateType == ActivateType.businessOwner) ...[
              _openAccount()
            ],
            const SizedBox(height: 25),
            AppButtonStyle.primaryButton(
              label: "Next".tr,
              isEnable: controller.isEnable,
              onPressed: controller.onNextPress,
              margin: const EdgeInsets.symmetric(horizontal: 15),
            ),
          ],
        ),
        child: Column(
          children: [
            // ICON DESCRIPTION
            ContainerDevider.blankSpaceLg,
            // SvgPicture.asset(SvgAppAssets.activateByCIDLogo,
            //     width: iconHeight.h),
            // ContainerDevider.blankSpaceMd,
            // Text(
            //   "ActivationUsingCID".tr,
            //   style: AppTextStyle.subtitle1,
            // ),
            _activateType(),
            ContainerDevider.blankSpaceMd,
            AnimatedCrossFade(
              firstChild: _newMerchantForm(),
              secondChild: _existingMerchantForm(),
              crossFadeState:
                  controller.activateType == ActivateType.existingMerchant
                      ? CrossFadeState.showSecond
                      : CrossFadeState.showFirst,
              duration: const Duration(milliseconds: 100),
            ),
            _phoneNumberInput(),
            const Expanded(child: SizedBox()),
          ],
        ),
      ),
    );
  }

  Widget _newMerchantForm() {
    return Column(
      children: [
        Text(
          "ActivationUsingCIDLabel".tr,
          style: AppTextStyle.caption,
          textAlign: TextAlign.center,
        ),
        ContainerDevider.blankSpaceMd,
        // INPUT
        KTextField(
          name: "CIDORAccountNumber".tr,
          iconData: MerchantIcon.user,
          hint: "CIDORAccountNumber".tr,
          keyboardType: TextInputType.number,
          maxLength: 16,
          // autoFocus: true,
          textInputAction: TextInputAction.next,
          inputFormatters: [
            MaskFormat.accountNumber,
          ],
          validatorMessage: controller.cidValidateMessage.tr,
          onChanged: (value) => {
            controller.cidOrAccountNumberInput = value ?? "",
            controller.cidOrAccountNumberValidator(),
          },
          secondChild: InkWell(
              onTap: onFindCIDQuestiionClick,
              child: const Padding(
                padding: EdgeInsets.fromLTRB(20, 3, 5, 3),
                child: Icon(
                  MerchantIcon.signInfo,
                  color: AppColors.grey,
                  size: 18,
                ),
              )),
        ),
      ],
    );
  }

  Widget _existingMerchantForm() {
    return Column(
      children: [
        Text(
          "ActivationExistingMerchantLabel".tr,
          style: AppTextStyle.caption,
          textAlign: TextAlign.center,
        ),
        ContainerDevider.blankSpaceMd,
        // INPUT
        KTextField(
          name: "MerchantId".tr,
          hint: "MerchantId".tr,
          iconData: MerchantIcon.merchantId,
          keyboardType: TextInputType.number,
          textInputAction: TextInputAction.next,
          inputFormatters: [
            MaskFormat.merchantId,
          ],
          validatorMessage: controller.merchantIdValidateMessage.tr,
          onChanged: (value) => {
            controller.merchantId = value ?? "",
            controller.merchantIdValidator(),
          },
        ),
      ],
    );
  }

  Widget _phoneNumberInput() {
    return KTextField(
      name: "PhoneNumber",
      label: "PhoneNumber".tr,
      prefixText: '+855 (0)',
      validatorMessage: controller.phoneValidateMessage.tr,
      iconData: MerchantIcon.telephone,
      keyboardType: TextInputType.phone,
      onChanged: (value) => {
        controller.phoneInput = value ?? "",
        controller.phoneValidator(),
      },
    );
  }

  Widget _activateType() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        _activateTypeOption(
          title: "NewMerchant".tr,
          icon: MerchantIcon.merchantNew,
          isSelected: controller.activateType == ActivateType.businessOwner,
          onSelected: () {
            if (controller.activateType != ActivateType.businessOwner) {
              controller.activateType = ActivateType.businessOwner;
              controller.checkEnable();
            }
          },
        ),
        ContainerDevider.blankSpaceMd,
        _activateTypeOption(
          title: "ExistingMerchant".tr,
          icon: MerchantIcon.merchantExisting,
          isSelected: controller.activateType == ActivateType.existingMerchant,
          onSelected: () {
            if (controller.activateType != ActivateType.existingMerchant) {
              controller.activateType = ActivateType.existingMerchant;
              controller.checkEnable();
            }
          },
        )
      ],
    );
  }

  Widget _activateTypeOption({
    required String title,
    required IconData icon,
    bool isSelected = false,
    void Function()? onSelected,
  }) {
    final double size = Get.width * 0.4;
    return Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          color: AppColors.white,
          border: Border.all(
            color: isSelected ? AppColors.primary : AppColors.greyishDisable,
            width: 1,
            strokeAlign: BorderSide.strokeAlignInside,
          )),
      constraints: const BoxConstraints(maxHeight: 150, maxWidth: 150),
      height: size,
      width: size,
      clipBehavior: Clip.hardEdge,
      child: RawMaterialButton(
        onPressed: onSelected,
        splashColor: AppColors.primary.withOpacity(0.1),
        // highlightColor:  AppColors.primary,
        child: Column(
          children: [
            Expanded(
              flex: 4,
              child: Align(
                alignment: Alignment.topRight,
                child: Padding(
                  padding: const EdgeInsets.all(5),
                  child: Icon(Icons.check_circle,
                      size: 24,
                      color: isSelected
                          ? AppColors.primary
                          : AppColors.transparent),
                ),
              ),
            ),
            Icon(icon, size: 50, color: AppColors.primary),
            ContainerDevider.blankSpaceSm,
            Expanded(
              flex: 5,
              child: Align(
                alignment: Alignment.topCenter,
                child: Text(
                  title,
                  style: TextStyle(
                      fontWeight:
                          isSelected ? FontWeight.w700 : FontWeight.w400),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _openAccount() => GestureDetector(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              child: Text(
                "DonHaveAccount".tr,
                textAlign: TextAlign.right,
                style: const TextStyle(
                    color: AppColors.textSecondary,
                    fontSize: 12,
                    fontWeight: FontWeight.w400),
              ),
            ),
            const SizedBox(
              width: 7,
            ),
            Text(
              "OpenNow".tr,
              textAlign: TextAlign.center,
              style: const TextStyle(
                  color: AppColors.primary,
                  fontSize: 12,
                  fontWeight: FontWeight.w400),
            ),
          ],
        ),
        onTap: () async {
          launchMB();
        },
      );

  void launchMB() async {
    const scheme = 'canadia://';
    const packageName = 'com.canadiabank.mobileapp.android';
    const appStoreLink =
        'https://apps.apple.com/kh/app/canadia-banks/id1554821237';
    bool launched = false;
    try {
      // Attempt to launch the scheme
      launched = await launch(scheme);
    } catch (_) {}

    if (!launched) {
      // If launching the scheme failed or returned false, fall back to the app store link
      final appLink = Platform.isAndroid
          ? 'https://play.google.com/store/apps/details?id=$packageName'
          : appStoreLink;

      await launch(appLink);
    }
  }

  void onFindCIDQuestiionClick() {
    AppBottomSheet.bottomSheetBlankContent(
      Padding(
        padding: const EdgeInsets.all(15.0),
        child: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                'FindCIDQuestion'.tr,
                style: AppTextStyle.header4.copyWith(color: AppColors.black),
              ),
              ContainerDevider.blankSpaceLg,
              Text(
                '1. ${'AccountNoteCard'.tr}',
                style: const TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w700,
                ),
              ),
              ContainerDevider.blankSpaceSm,
              Text(
                'AccountNoteCardDescribe'.tr,
                textAlign: TextAlign.center,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 16),
                child: SvgPicture.asset(
                  SvgAppAssets.findCIDLogo,
                  fit: BoxFit.contain,
                ),
              ),
              ContainerDevider.blankSpaceSm,
              Text(
                '2. ${'CanadiaBankApp'.tr}',
                style:
                    const TextStyle(fontSize: 14, fontWeight: FontWeight.w700),
              ),
              ContainerDevider.blankSpaceSm,
              Text(
                'CanadiaBankAppDescribe'.tr,
                textAlign: TextAlign.center,
              ),
              ContainerDevider.blankSpaceLg
            ],
          ),
        ),
      ),
    );
  }
}
