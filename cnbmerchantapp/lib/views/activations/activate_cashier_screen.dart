import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

class ActivateCashierScreen extends StatefulWidget {
  const ActivateCashierScreen({Key? key}) : super(key: key);

  @override
  State<ActivateCashierScreen> createState() => _ActivateCashierScreenState();
}

class _ActivateCashierScreenState extends State<ActivateCashierScreen> {
  final controller = Get.put(ActivateCashierScreenController());
  SizedBox get _seperator => const SizedBox(height: 15);
  double iconHeight = Get.width * 0.25;

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => ContainerAnimatedTemplate(
        title: "ActivateAsCashier".tr,
        floatingActionButton: AppButtonStyle.primaryButton(
          label: "Next".tr,
          isEnable: controller.isEnable,
          onPressed: controller.onNextPress,
          margin: const EdgeInsets.symmetric(horizontal: 15),
        ),
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            // #region ICON DESCRIPTION
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 20),
              child: SvgPicture.asset(SvgAppAssets.cashierActivationLogo,
                  height: iconHeight > 90 ? 90 : iconHeight),
            ),
            _seperator,
            Text(
              "ActivateByInviteCode".tr,
              textAlign: TextAlign.center,
              style: AppTextStyle.subtitle1,
            ),
            _seperator,
            Text(
              "InvitationLabel".tr,
              style: AppTextStyle.caption,
              textAlign: TextAlign.center,
            ),
            _seperator,
            // #endregion

            // #region INPUT
            KTextField(
              name: "InvitationCode".tr,
              hint: "InvitationCode".tr,
              iconData: MerchantIcon.inviteCode,
              keyboardType: TextInputType.number,
              textInputAction: TextInputAction.next,
              inputFormatters: [MaskFormat.cid],
              validatorMessage: controller.cidValidateMessage.tr,
              onChanged: (value) => {
                controller.inviteCodeInput = value ?? "",
                controller.cidValidator(),
              },
            ),
            KTextField(
              name: "PhoneNumber",
              label: "PhoneNumber".tr,
              initialValue: "",
              prefixText: '+855 (0)',
              validatorMessage: controller.phoneValidateMessage.tr,
              iconData: MerchantIcon.telephone,
              keyboardType: TextInputType.phone,
              onChanged: (value) => {
                controller.phoneInput = value ?? "",
                controller.phoneValidator(),
              },
            ),
            // #endregion
          ],
        ),
      ),
    );
  }
}
