import 'package:cached_network_image/cached_network_image.dart';
import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

class TabSettingScreen extends StatefulWidget {
  const TabSettingScreen({Key? key}) : super(key: key);

  @override
  State<TabSettingScreen> createState() => TabSettingScreenState();
}

class TabSettingScreenState extends State<TabSettingScreen>
    with AutomaticKeepAliveClientMixin {
  final _controller = Get.put(TabSettingScreenController());
  final TabMenuComponent _tabMenuComponent = TabMenuComponent();
  final ScrollController _scrollController = ScrollController();

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    CachedNetworkImage.evictFromCache(AppUserTempInstant.appCacheUser.photo,
        cacheKey: AppUserTempInstant.appCacheUser.profilePhotoId);
    _controller.getProfile();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Obx(() => ContainerTemp(
        header: _sumaryProfile(),
        child: SingleChildScrollView(
          controller: _scrollController,
          child: Padding(
            padding: const EdgeInsets.only(left: 15, right: 15),
            child: _userProfileContainer(),
          ),
        )));
  }

  //#region User profile summary

  _sumaryProfile() {
    return Padding(
      padding: const EdgeInsets.all(15),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          InkWell(
            onTap: () {
              _controller.updateProfileImageAsync();
            },
            child: Container(
              decoration: const BoxDecoration(
                color: AppColors.primaryLight,
                shape: BoxShape.circle,
              ),
              clipBehavior: Clip.hardEdge,
              width: 70,
              height: 70,
              child: Stack(
                fit: StackFit.expand,
                children: <Widget>[
                  _controller.photoFile.isNotEmpty
                      ? Image.memory(
                          _controller.photoFile,
                          fit: BoxFit.cover,
                        )
                      : AppNetworkImage(
                          AppUserTempInstant.appCacheUser.photo,
                          fit: BoxFit.cover,
                          cacheKey:
                              AppUserTempInstant.appCacheUser.userName,
                          errorWidget: PlaceholderImage.userName(
                            AppUserTempInstant.appCacheUser.firstName
                                .toUpperCase(),
                            AppUserTempInstant.appCacheUser.lastName
                                .toUpperCase(),
                          ),
                        ),
                  _controller.isUploadingProfileImage
                      ? const CircularProgressIndicator(
                          color: AppColors.yellowBrand,
                          strokeWidth: 5,
                          backgroundColor: AppColors.primary,
                        )
                      : Align(
                          alignment: Alignment.bottomCenter,
                          child: Container(
                            decoration: BoxDecoration(
                                color: AppColors.black.withOpacity(.1)),
                            // blurRadius: 2,
                            child: const SizedBox(
                              height: 20,
                              width: 100,
                              child: Icon(
                                Icons.camera_alt,
                                color: AppColors.white,
                                size: 14,
                              ),
                            ),
                          ),
                        ),
                ],
              ),
            ),
          ),
          const SizedBox(width: 10),
          Expanded(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                _controller.userProfile.displayName,
                textAlign: TextAlign.start,
                style: const TextStyle(
                    color: AppColors.white,
                    fontSize: 25,
                    fontFamily: AppFontName.familyFont,
                    fontWeight: FontWeight.w700),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text.rich(TextSpan(children: [
                    TextSpan(
                      text: "${"PhoneNumber".tr} ",
                      style: const TextStyle(
                        color: AppColors.white,
                        fontSize: 11,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                    TextSpan(
                      text: _controller.userProfile.phoneNumber,
                      style: const TextStyle(
                        color: AppColors.white,
                        fontSize: 11,
                        fontFamily: AppFontName.familyFont,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ])),
                  Text(
                    "${"LastTimeSigned".tr} ${kDateTimeFormat(_controller.userProfile.lastLogin)}",
                    textAlign: TextAlign.start,
                    style: const TextStyle(
                        color: AppColors.white,
                        fontSize: 11,
                        fontWeight: FontWeight.w400),
                  )
                ],
              ),
            ],
          ))
        ],
      ),
    );
  }

  //#endregion

  //#region User profile container
  Widget _userProfileContainer() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        //const DashboardView(),

        //#region More feature

        if (AppUserTempInstant.appCacheUser.userType ==
            AppUserType.businessOwner) ...[
          _tabMenuComponent.sectionTitle(
            "MoreFeature".tr,
          ),
          _tabMenuComponent.menuItem(
              title: "ManageBusiness".tr,
              firstItem: true,
              onTap: () => _controller.sessionTimeoutCheck(
                    route: Routes.manageBusiness,
                  )),
          _tabMenuComponent.menuItem(
            title: "RequestPrintedQr".tr,
            onTap: () => _controller.sessionTimeoutCheck(
                route: Routes.requestQrMaterialListScreen),
          ),
          _tabMenuComponent.menuItem(
            title: "MerchantReferral".tr,
            onTap: () => _controller.sessionTimeoutCheck(
                route: Routes.requestMerchantReferal),
          ),
          // _tabMenuComponent.menuItem(
          //   title: "MerchantPromotion".tr,
          //   onTap: () => _controller.sessionTimeoutCheck(
          //       route: Routes.merchantPromotion),
          // ),
          _tabMenuComponent.menuItem(
            title: "InviteFriend".tr,
            lastItem: true,
            onTap: () =>
                _controller.sessionTimeoutCheck(route: Routes.inviteFriend),
          ),
        ],

        //#endregion

        //#region Security setting
        _tabMenuComponent.sectionTitle(
          "SecuritySetting".tr,
        ),
        _tabMenuComponent.menuItem(
          firstItem: true,
          title: "AutoLogout".tr,
          onTap: () => _controller.timeOutScreenOptionSettingAsync(),
          icon: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                convertSecondToMinute(
                    AppSettingInstant.setting.inactivityTimeoutSeccond),
                style: AppTextStyle.body2.copyWith(
                  color: AppColors.textPrimary,
                ),
              ),
              const SizedBox(
                width: 5,
                height: 1,
              ),
              Icon(
                MerchantIcon.time,
                color: AppColors.primaryDark,
                size: 17.w,
              )
            ],
          ),
        ),
        // _tabMenuComponent.menuItem(
        //   title: "ChangeUsername".tr,
        //   onTap: () => _controller
        //       .navigationToNamedArgumentsAsync(Routes.changeUserName),
        // ),
        _tabMenuComponent.menuItem(
            title: "ChangePassword".tr,
            onTap: () {
              var arguments = {ArgumentKey.fromPage: Routes.changePassword};
              _controller.navigationToNamedArgumentsAsync(
                  Routes.newPasswordScreen,
                  args: arguments);
            }),
        _tabMenuComponent.menuItem(
          lastItem: true,
          title: "ResetPIN".tr,
          onTap: () => _controller.resetPinAsync(),
        ),
        //#endregion

        //#region App Setting
        _tabMenuComponent.sectionTitle(
          "AppSetting".tr,
        ),
        _tabMenuComponent.menuItem(
          child: SizedBox(
            height: 30,
            child: CSwitchWidget(
              value: _controller.isPushNotification,
              text: "PushNotification".tr,
              onChanged: (value) {
                _controller.updateAppConfig();
              },
            ),
          ),
          firstItem: true,
          icon: Container(),
          onTap: () {
            _controller.updateAppConfig();
          },
        ),
        _tabMenuComponent.menuItem(
            title: "Language".tr,
            lastItem: true,
            onTap: () => _controller.updateLanguagesAsync(),
            icon: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.only(right: 10),
                  child: SizedBox(
                    width: 20,
                    height: 20,
                    child: ClipOval(
                      child: SvgPicture.asset(
                        _controller.currentSelectedLanguage.imageSvg,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),
                Text(
                  "${_controller.currentSelectedLanguage.name} ",
                  textAlign: TextAlign.start,
                  style: AppTextStyle.body2.copyWith(
                    color: AppColors.textPrimary,
                  ),
                ),
              ],
            )),
        //#endregion

        //#region App Supporting
        _tabMenuComponent.sectionTitle(
          "Support".tr,
        ),
        _tabMenuComponent.menuItem(
          firstItem: true,
          title: "CanadiaSupport".tr,
          iconData: Icons.support_agent,
          onTap: () => _controller
              .navigationToNamedArgumentsAsync(Routes.canadiaSupports),
        ),
        _tabMenuComponent.menuItem(
          title: "TermsAndConditions".tr,
          iconData: Icons.grading,
          onTap: () => _controller.navigationToNamedArgumentsAsync(
              Routes.merchantPartnershipAgreement),
        ),
        _tabMenuComponent.menuItem(
            lastItem: true,
            title: "UserGuides".tr,
            iconData: Icons.slow_motion_video,
            onTap: () {
              final args = {ArgumentKey.detail: ValueConst.userGuidesURL};
              _controller.navigationToNamedArgumentsAsync(Routes.userGuides,
                  args: args);
            }),
        //#endregion

        // //#region App Version & Logout
        const SizedBox(height: 10),
        _tabMenuComponent.menuItem(
            lastItem: true,
            firstItem: true,
            title: 'Version'.tr,
            icon: Text(
                "${_controller.appInfoHelper.version} (${_controller.appInfoHelper.buildNumber})")),
        const SizedBox(height: 10),
        _tabMenuComponent.menuItem(
            lastItem: true,
            firstItem: true,
            titleColor: AppColors.primary,
            title: "${'LogOut'.tr} ",
            onTap: () => _controller.logout(),
            iconData: Icons.power_settings_new),
        const SizedBox(height: 20),

        // _tabMenuComponent.menuItem(
        //     title: 'Version'.tr,
        //     backgroundColor: Colors.transparent,
        //     icon: const Text('1.0.0')),
        // const SizedBox(height: 10),
        // // dev
        // Visibility(
        //   visible: kDebugMode,
        //   child: Padding(
        //     padding: const EdgeInsets.only(bottom: 20),
        //     child: _tabMenuComponent.menuItem(
        //         lastItem: true,
        //         firstItem: true,
        //         title: 'dev'.tr,
        //         onTap: () {
        //           // _controller.requestToConfirmPIN();
        //           _controller.kAlertScreen.failure(
        //               message: "faul",
        //               child: const Text("data"),
        //               description:
        //                   'Setup PIN pin blah blah so is karatesorava du maā sû it ka li pa tu ru sa manda karu');
        //         },
        //         iconData: Icons.developer_mode),
        //   ),
        // ),
        // Visibility(
        //     visible: kDebugMode,
        //     child: Padding(
        //       padding: const EdgeInsets.only(bottom: 20),
        //       child: _tabMenuComponent.menuItem(
        //           lastItem: true,
        //           firstItem: true,
        //           title: 'Clear Cach'.tr,
        //           onTap: () => _controller.clearCacheAsync(),
        //           iconData: Icons.developer_mode),
        //     ))
        //#endregion
      ],
    );
  }

  //#endregion
}
