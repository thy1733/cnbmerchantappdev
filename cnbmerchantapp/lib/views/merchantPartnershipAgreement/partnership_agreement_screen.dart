import 'dart:io';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:cnbmerchantapp/core.dart';

class PartnershipAgreementScreen extends StatelessWidget {
  PartnershipAgreementScreen({Key? key}) : super(key: key);

  final PartnershipAgreementScreenController _controller =
      Get.put(PartnershipAgreementScreenController());
  @override
  Widget build(BuildContext context) {
    return ContainerTemp(
      header: InkWell(
        onTap: () {
          _controller.backAsync();
        },
        child: Align(
          alignment: Alignment.centerLeft,
          child: Padding(
            padding: const EdgeInsets.only(top: 15, left: 20, bottom: 20),
            child: Row(children: [
              Icon(
                  Platform.isAndroid
                      ? Icons.arrow_back
                      : Icons.arrow_back_ios_new,
                  size: 20,
                  color: AppColors.white),
              const SizedBox(width: 15),
              Text(
                "${"MerchantPartnershipAgreement".tr} ",
                style: AppTextStyle.header,
              )
            ]),
          ),
        ),
      ),
      child: _body(),
    );
  }

  _body() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.all(12.0),
          child: Text(
            "MerchantPartnershipAgreement".tr,
            textAlign: TextAlign.start,
            style:
                AppTextStyle.headline5.copyWith(color: AppColors.textPrimary),
          ),
        ),
        Expanded(
          child: Obx(
            () => _controller.isOnline && !_controller.loadWebViewError
                ? KWebViewWidget(
                    url: ValueConst.partnershipAgreementURL
                        .addMultipleLanguagesSupport(),
                    onStart: () {
                      _controller.showLoading();
                    },
                    onStop: () {
                      _controller.dismiss();
                    },
                    onError: () {
                      _controller.dismiss();
                      _controller.loadWebViewError = true;
                    },
                  )
                : KLocalWebViewWidget(
                    fileUrl: FileAssets.merchantPartnershipAgreement,
                    onStop: () => _controller.dismiss(),
                    onError: () => _controller.dismiss(),
                  ),
          ),
        ),
      ],
    );
  }
}
