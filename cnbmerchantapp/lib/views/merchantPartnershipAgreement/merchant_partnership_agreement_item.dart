import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MerchantPartnershipAgreementItem extends StatelessWidget {
  const MerchantPartnershipAgreementItem({
    Key? key,
    required this.title,
  }) : super(key: key);

  final String title;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 40,
      child: Row(
        children: [
          Expanded(
            child: Text(
              title.tr,
              style: AppTextStyle.body2.copyWith(
                  color: AppColors.textPrimary, fontWeight: FontWeight.w700),
            ),
          ),
          const Icon(
            Icons.navigate_next,
            size: 20,
            color: AppColors.primaryDark,
          ),
        ],
      ),
    );
  }
}
