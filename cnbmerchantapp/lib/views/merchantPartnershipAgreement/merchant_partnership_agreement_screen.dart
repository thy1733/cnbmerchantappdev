import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:cnbmerchantapp/core.dart';

class MerchantPartnershipAgreementScreen extends StatelessWidget {
  MerchantPartnershipAgreementScreen({Key? key}) : super(key: key);

  final MerchantPartnershipAgreementScreenController _controller =
      Get.put(MerchantPartnershipAgreementScreenController());

  @override
  Widget build(BuildContext context) {
    return ContainerAnimatedTemplate(
      needBottomSpace: false,
      padding: EdgeInsets.zero,
      title: "TermsAndConditions".tr,
      child: _body(),
    );
  }

  Widget _body() {
    final List<String> items = [
      "TermsAndConditions",
      // "MerchantPartnershipAgreement"
    ];
    return ListView.separated(
      padding: const EdgeInsets.fromLTRB(15, 15, 15, 0).r,
      itemBuilder: (context, index) {
        return InkWell(
          onTap: () {
            if (index == 0) {
              _controller.navigationToNamedArgumentsAsync(
                  Routes.merchantTermsAndCondition);
            } else if (index == 1) {
              _controller.navigationToNamedArgumentsAsync(
                  Routes.partnershipAgreementScreen);
            }
          },
          child: MerchantPartnershipAgreementItem(
            title: items[index],
          ),
        );
      },
      separatorBuilder: (context, index) => const Divider(),
      itemCount: items.length,
    );
  }
}
