import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class WebViewScreen extends StatelessWidget {
  WebViewScreen({super.key});

  final WebViewScreenController _controller =
      Get.put(WebViewScreenController());

  @override
  Widget build(BuildContext context) {
    return ContainerTemp(
      header: NavigationTitle(title: _controller.title.tr),
      child: Obx(() {
        if (!_controller.isOnline) {
          return ErrorNetworkWidget(
            onRetryPressed: _controller.retryAsync,
          );
        } else if (_controller.loadWebViewError) {
          return ErrorNetworkWidget(
            assetName: JsonAppAssets.underMaintenance,
            title: "",
            description: "SomethingWentWrongPleaseTryAgainLater",
            onRetryPressed: () {
              _controller.loadWebViewError = false;
            },
          );
        } else {
          return KWebViewWidget(
            url: _controller.url,
            onWebViewCreated: (con) {
              _controller.showLoading();
            },
            onError: () {
              _controller.dismiss();
              _controller.loadWebViewError = true;
            },
            onStop: () {
              _controller.dismiss();
            },
          );
        }
      }),
    );
  }
}
