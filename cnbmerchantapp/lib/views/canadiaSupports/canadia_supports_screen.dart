// ignore_for_file: prefer_const_constructors, must_be_immutable, prefer_const_literals_to_create_immutables

import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

class CanadiaSupportsScreen extends StatelessWidget {
  CanadiaSupportsScreen({Key? key}) : super(key: key);

  final CanadiaSupportsScreenController _controller =
      Get.put(CanadiaSupportsScreenController());

  @override
  Widget build(BuildContext context) {
    return ContainerAnimatedTemplate(
      needBottomSpace: false,
      padding: EdgeInsets.zero,
      title: "CanadiaSupport".tr,
      child: _body(context),
    );
  }

  Widget _body(BuildContext context) {
    return ListView.separated(
      padding: const EdgeInsets.fromLTRB(15, 15, 15, 0).r,
      scrollDirection: Axis.vertical,
      physics: AlwaysScrollableScrollPhysics(),
      shrinkWrap: true,
      itemCount: _controller.supports.length,
      itemBuilder: (BuildContext context, int index) {
        CanadiaSupportModel support = _controller.supports[index];
        return InkWell(
          onTap: () => _controller.onTapCanadiaSupportsAsync(context, index),
          child: CanadiaSupportsItem(support: support),
        );
      },
      separatorBuilder: (context, index) => Divider(),
    );
  }
}
