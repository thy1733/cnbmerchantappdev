import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

class CanadiaSupportsItem extends StatelessWidget {
  const CanadiaSupportsItem({
    Key? key,
    required this.support,
  }) : super(key: key);

  final CanadiaSupportModel support;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 8),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SvgPicture.asset(
            support.icon,
            width: 20.w,
            height: 20.w,
          ),
          const SizedBox(
            width: 16,
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  support.title.tr,
                  style: AppTextStyle.body2.copyWith(
                    color: AppColors.textPrimary,
                  ),
                ),
                Text(
                  support.subtitle.tr,
                  style: AppTextStyle.caption.copyWith(
                    color: AppColors.textSecondary,
                  ),
                ),
              ],
            ),
          ),
          const Icon(
            Icons.navigate_next,
            size: 20,
            color: AppColors.primaryDark,
          )
        ],
      ),
    );
  }
}
