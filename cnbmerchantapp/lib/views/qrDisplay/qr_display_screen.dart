import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:screen_brightness/screen_brightness.dart';

class QrDisplayScreen extends StatefulWidget {
  const QrDisplayScreen({super.key});

  @override
  State<QrDisplayScreen> createState() => _QrDisplayScreenState();
}

class _QrDisplayScreenState extends State<QrDisplayScreen> {
  final controller = Get.put(QrDisplayScreenController());

  /// the standard of KHQR design is ratio 20:29(w:h) is equal to 1.45 as ratio size
  final double sizeRatio = 1.45;

  @override
  void dispose() {
    super.dispose();
    ScreenBrightness().resetScreenBrightness();
  }

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Material(
        child: Stack(children: [
          // fake background
          ContainerTemp(
              header: const SizedBox(height: 90), child: const SizedBox()),
          // bluring background
          ContainerBlur(
            decoration: BoxDecoration(
              color: Colors.black.withOpacity(0.5),
            ),
            child: InkWell(
              // onTap: controller.onBackgroundClick,
              child: _child(),
            ),
          )
        ]),
      ),
    );
  }

  Widget _child() {
    return SafeArea(
      maintainBottomViewPadding: true,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 30.sp),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            ContainerDevider.blankSpaceLg,
            _selecteOutlet(),
            ContainerDevider.blankSpaceLg,
            // Flexible(
            //   child: _qrPart(borderRadios: 15),
            // ),
            _qrCard(borderRadios: 15),
            ContainerDevider.blankSpaceLg,
            ElevatedButton(
              onPressed: controller.onSetAmountAsync,
              style: AppButtonStyle.kPrimaryButtonStyle(height: 50).copyWith(
                backgroundColor: MaterialStatePropertyAll(
                  AppColors.white.withOpacity(.15),
                ),
              ),
              child: Text("SetAmount".tr),
            ),
            ContainerDevider.blankSpaceLg,
            _shareButtons(),
            ContainerDevider.blankSpaceSm,
            CloseBtn(iconColor: AppColors.white.withOpacity(1)),
          ],
        ),
      ),
    );
  }

  Widget _selecteOutlet() {
    return InkWell(
      onTap: () {
        controller.onSelectOutletAsync();
      },
      child: Container(
        decoration: BoxDecoration(
            color: AppColors.white.withOpacity(.15),
            borderRadius: BorderRadius.circular(15)),
        padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 7),
        child: Row(
          children: [
            AppNetworkImage(
              AppOutletTempInstant.selectedOutlet.outletLogo,
              height: 35,
              width: 35,
              shape: BoxShape.circle,
              backgroundColor: AppColors.greyBackground,
              cacheKey: AppOutletTempInstant.selectedOutlet.cacheKey,
              errorWidget: SvgPicture.asset(SvgAppAssets.pOutletList),
            ),
            const SizedBox(width: 5),
            Expanded(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "SelectedOutlet".tr,
                    style:
                        AppTextStyle.overline.copyWith(color: AppColors.white),
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Expanded(
                        child: Text(
                          AppOutletTempInstant.selectedOutlet.outletName,
                          overflow: TextOverflow.ellipsis,
                          maxLines: 1,
                          style: AppTextStyle.subtitle1.copyWith(
                            color: AppColors.white,
                            fontWeight: FontWeight.w600,
                            fontFamily: AppFontName.familyFont,
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            const Icon(
              MerchantIcon.chevronDown,
              size: 14,
              color: AppColors.white,
            )
          ],
        ),
      ),
    );
  }

  QrImageView _qrImage({double? size}) => QrImageView(
        data: controller.qrValue,
        size: size,
        padding: EdgeInsets.zero,
      );

  Widget _qrCard({double borderRadios = 0}) {
    var cardHeigh = Get.height * 0.5;
    if (cardHeigh > 450) {
      cardHeigh = 450;
    }
    return InkWell(
      onTap: () {
        // var a = QrResultOutputModel();
        // controller.toInvoiceDetail(a);
      },
      child: Container(
        width: cardHeigh / sizeRatio,
        height: cardHeigh,
        padding: EdgeInsets.zero,
        clipBehavior: Clip.antiAlias,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(borderRadios),
          color: AppColors.white,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              decoration: BoxDecoration(
                color: AppColors.primary,
                borderRadius:
                    BorderRadius.vertical(top: Radius.circular(borderRadios)),
                // border use to avoid Unexpected space between Container
                border: Border.all(
                    color: AppColors.primary,
                    strokeAlign: BorderSide.strokeAlignInside,
                    width: 1),
              ),
              alignment: Alignment.center,
              height: cardHeigh * 0.12,
              child: SvgPicture.asset(
                SvgAppAssets.khqrLogo,
                height: cardHeigh * 0.04,
              ),
            ),
            Expanded(
              child: Container(
                // color: Colors.yellow,
                decoration: const BoxDecoration(),
                alignment: Alignment.centerRight,
                child: ClipPath(
                  clipper: AppTringleClippers.topRight,
                  child: Container(
                    decoration: const BoxDecoration(
                      color: AppColors.primary,
                    ),
                    width: 15.sp,
                  ),
                ),
              ),
            ),
            // amount
            InkWell(
              onTap: () => controller.onSetAmountAsync(),
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: cardHeigh * 0.05),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    // outlet
                    Text(
                      controller.merchantName,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: AppColors.darkGrey,
                        fontSize: cardHeigh * 0.03,
                        fontFamily: GoogleFonts.nunitoSans().fontFamily,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                    // amount and currency
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Text(
                          "${CurrencyConverter.currencyToSymble(controller.currency)} ${controller.amountToPay.toThousandSeparatorString(d2: isUSDCurrency && controller.amountToPay > 0)} ",
                          style: TextStyle(
                              color: AppColors.darkGrey,
                              fontSize: cardHeigh * 0.065,
                              height: 1.2,
                              fontWeight: FontWeight.w700,
                              fontFamily: GoogleFonts.nunitoSans().fontFamily),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            const Expanded(child: SizedBox()),
            const DottedLine(
              direction: Axis.horizontal,
              lineThickness: 1.0,
              dashLength: 12,
              dashGapLength: 2,
              dashColor: AppColors.greyishHint,
            ),
            Padding(
                padding: EdgeInsets.all(
                  cardHeigh * 0.08,
                ),
                child: Stack(
                  fit: StackFit.loose,
                  alignment: Alignment.center,
                  children: [
                    _qrImage(),
                    Container(
                      alignment: Alignment.center,
                      height: cardHeigh * 0.12,
                      decoration: BoxDecoration(
                        border: Border.all(
                            color: AppColors.white,
                            width: 4.sp,
                            strokeAlign: BorderSide.strokeAlignInside),
                        shape: BoxShape.circle,
                        color: AppColors.black,
                      ),
                      child: Icon(
                        isUSDCurrency? MerchantIcon.currencyUsd: MerchantIcon.currencyKhr,
                        color: AppColors.white,
                        size: cardHeigh * 0.06,
                      ),
                    ),
                  ],
                )),
          ],
        ),
      ),
    );
  }

  Widget _shareButtons() {
    return Container(
        height: 50,
        decoration: BoxDecoration(
            color: AppColors.white.withOpacity(.15),
            borderRadius: BorderRadius.circular(25)),
        padding: const EdgeInsets.all(10),
        child: Row(
          children: [
            Expanded(
              child: _shareButtonsWidget(
                  icon: MerchantIcon.cameraOutlineBold,
                  title: "Screenshot".tr,
                  onTap: () => controller.onScreenshotAsync(_qrCard())),
            ),

            if (controller.amountToPay < 1) ...[
              Container(width: 1, color: AppColors.white),
              Expanded(
                child: _shareButtonsWidget(
                    icon: MerchantIcon.downloadOutlineBold,
                    title: "Save".tr,
                    onTap: () =>
                        controller.onSaveImageAsync(_qrStickerScreenToSave())),
              ),
              // Container(width: 1, color: AppColors.white)
            ],
            // Expanded(
            //   child: _shareButtonsWidget(
            //     icon: MerchantIcon.shareOutlineBold,
            //     title: "ShareLink".tr,
            //     onTap: () => controller.onShare(),
            //   ),
            // ),
          ],
        ));
  }

  Widget _shareButtonsWidget({
    IconData icon = MerchantIcon.cameraOutlineBold,
    String title = "",
    VoidCallback? onTap,
  }) {
    return InkWell(
      onTap: onTap,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Icon(icon, size: 16, color: AppColors.white),
          Text(
            title,
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 8.sp, color: AppColors.white, height: 1),
          ),
        ],
      ),
    );
  }

  Widget _qrStickerScreenToSave() => SizedBox(
        height: 550,
        width: 390,
        child: Stack(
          children: [
            SvgPicture.asset(SvgAppAssets.qrStickerBg),
            Center(
              child: _qrImage(size: 180),
            ),
            Positioned(
              left: 0,
              right: 0,
              bottom: 130,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    controller.merchantName,
                    style: const TextStyle(color: AppColors.textPrimary),
                  ),
                  const SizedBox(height: 10),
                  Text(
                    AppBusinessTempInstant.selectedBusiness.accountNumber,
                    style: AppTextStyle.caption
                        .copyWith(fontWeight: FontWeight.w700),
                  )
                ],
              ),
            )
          ],
        ),
      );
}
