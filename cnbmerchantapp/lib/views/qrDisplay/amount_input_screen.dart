import 'package:auto_size_text/auto_size_text.dart';
import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

class AmountInputScreen extends StatefulWidget {
  const AmountInputScreen({super.key});

  @override
  State<AmountInputScreen> createState() => _AmountInputScreenState();
}

class _AmountInputScreenState extends State<AmountInputScreen> {
  final _controller = Get.put(AmountInputScreenController());

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.all(15),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: [
            const SizedBox(
              height: 10,
            ),
            Text(
              "AmountToPay".tr,
              textAlign: TextAlign.center,
              style: TextStyle(
                  color: AppColors.darkGrey,
                  fontSize: 11.sp,
                  fontFamily: AppFontName.familyFont,
                  fontWeight: FontWeight.w400),
            ),
            Expanded(
              child: _amountPath(),
            ),
            Container(
              constraints: BoxConstraints(maxHeight: Get.height * 0.40),
              // child: CalculatorKeyBoard(
              //   onPressed: controller.userTapOnKeyboardAsync,
              //   onBackPress: controller.userTabOnBackSpaceAsync,
              //   onClear: controller.resetAsync,
              //   fontSize: 30,
              //   fontWeight: FontWeight.normal,
              // ),
              child: KNumpadWidget(
                onPressed: _controller.userTapOnKeyboardAsync,
                onBackSpacePressed: _controller.userTabOnBackSpaceAsync,
              ),
            ),
            const SizedBox(
              height: 5,
            ),
            _bottomButton()
          ],
        ));
  }

  // Widget _header() => Padding(
  //       padding: const EdgeInsets.only(left: 15, right: 15, top: 0, bottom: 10),
  //       child: Row(
  //         crossAxisAlignment: CrossAxisAlignment.center,
  //         mainAxisAlignment: MainAxisAlignment.spaceBetween,
  //         children: [
  //           SvgPicture.asset(
  //             SvgAppAssets.logo,
  //             height: 40,
  //             width: 40,
  //             fit: BoxFit.cover,
  //           ),
  //           const SizedBox(
  //             width: 10,
  //           ),
  //           _selecterBizOutlet(),
  //         ],
  //       ),
  //     );

  // Widget _selecterBizOutlet() {
  //   return InkWell(
  //     child: Container(
  //       decoration: BoxDecoration(
  //           color: AppColors.white, borderRadius: BorderRadius.circular(25)),
  //       padding: const EdgeInsets.all(2),
  //       child: Row(
  //         mainAxisSize: MainAxisSize.min,
  //         children: [
  //           const SizedBox(
  //             width: 35,
  //             height: 35,
  //             child: ClipOval(
  //               child: AppNetworkImage(
  //                 "https://i.pinimg.com/originals/ce/fb/2b/cefb2ba2bcaf3a770fc5b9a16e347a9e.jpg",
  //               ),
  //             ),
  //           ),
  //           const SizedBox(width: 5),
  //           Column(
  //             mainAxisSize: MainAxisSize.min,
  //             crossAxisAlignment: CrossAxisAlignment.start,
  //             mainAxisAlignment: MainAxisAlignment.center,
  //             children: const [
  //               Text(
  //                 "Business",
  //                 style: TextStyle(
  //                   fontSize: 14,
  //                   height: 1,
  //                   color: AppColors.darkGrey,
  //                   fontWeight: FontWeight.w400,
  //                 ),
  //               ),
  //               Text(
  //                 "Outlet",
  //                 style: TextStyle(
  //                   fontSize: 11,
  //                   height: 1,
  //                   color: AppColors.grey,
  //                 ),
  //               ),
  //             ],
  //           ),
  //           const SizedBox(width: 10),
  //           const Icon(
  //             MerchantIcon.chevronDown,
  //             size: 15,
  //             color: AppColors.grey,
  //             shadows: [BoxShadow(color: AppColors.grey, spreadRadius: 0.5)],
  //           ),
  //           const SizedBox(width: 10),
  //         ],
  //       ),
  //     ),
  //   );
  // }

  Widget _amountPath() {
    return Stack(
      alignment: Alignment.center,
      children: [
        _amountInput(),
        Align(
          alignment: Alignment.bottomCenter,
          child: _maxAmount(),
        )
      ],
    );
  }

  Widget _amountInput() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.max,
      children: [
        Obx(() => Flexible(
              child: AutoSizeText(
                "${CurrencyConverter.currencyToSymble(_controller.currency)}${thousandSeparatQR(_controller.inputAmount)}",
                maxLines: 1,
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: (_controller.isMaximumAmount ||
                            double.parse(_controller.inputAmount) <= 0)
                        ? AppColors.darkGrey.withOpacity(0.5)
                        : AppColors.darkGrey,
                    fontSize: 30,
                    fontFamily: AppFontName.familyFont,
                    fontWeight: FontWeight.w400),
              ),
            )),
        const SizedBox(
          width: 20,
        )
      ],
    );
  }

  Widget _maxAmount() => Obx(() => AnimatedContainer(
      height: _controller.isShowMaxAmount ? 30 : 0,
      alignment: Alignment.center,
      duration: const Duration(milliseconds: 200),
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
        decoration: BoxDecoration(
          color: AppColors.darkYellow,
          borderRadius: BorderRadius.circular(5),
        ),
        child: AutoSizeText(
          'IsMaxAmountMessage'
              .tr
              .replaceAll('0', _controller.isMaximumAmountMessage),
          textAlign: TextAlign.center,
          style: const TextStyle(
              color: AppColors.white,
              fontSize: 13,
              height: 1.5,
              fontFamily: AppFontName.familyFont,
              fontWeight: FontWeight.w400),
        ),
      )));

  ButtonStyle _shareBtnStyle(
          {BorderRadiusGeometry borderRadius = BorderRadius.zero,
          bool isEnable = true}) =>
      ButtonStyle(
        backgroundColor: MaterialStateProperty.all(isEnable
            ? AppColors.primary.withOpacity(.08)
            : AppColors.greyishDisable),
        shape: MaterialStateProperty.all(RoundedRectangleBorder(
            side: BorderSide.none, borderRadius: borderRadius)),
        overlayColor: MaterialStateProperty.all(
          AppColors.primary.withOpacity(0.07),
        ),
      );

  Widget _bottomButton() {
    return SizedBox(
        height: 50,
        width: Get.width - 40,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Obx(() => ElevatedButton(
                  style: _shareBtnStyle(
                      isEnable: !_controller.isMaximumAmount,
                      borderRadius: const BorderRadius.horizontal(
                          left: Radius.circular(25))),
                  onPressed: !_controller.isMaximumAmount
                      ? () async {
                          _controller.shareNote = '';
                          await AppBottomSheet.bottomSheetBlankContent(
                            _sharingNoteWidget(),
                            barrierBackground: AppColors.barrierBackground,
                          );
                        }
                      : null,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      const SizedBox(
                        width: 5,
                      ),
                      Text(
                        'Share'.tr,
                        textAlign: TextAlign.start,
                        style: TextStyle(
                            fontSize: 14,
                            color: !_controller.isMaximumAmount
                                ? AppColors.primary
                                : AppColors.grey,
                            fontWeight: FontWeight.w400),
                      )
                    ],
                  ),
                )),
            const SizedBox(
              width: 3,
            ),
            Expanded(
              child: Obx(() => ElevatedButton(
                    style: _shareBtnStyle(
                        isEnable: !_controller.isMaximumAmount,
                        borderRadius: const BorderRadius.horizontal(
                            right: Radius.circular(25))),
                    onPressed: !_controller.isMaximumAmount
                        ? () => _controller.generateOrConfirmQRAsync()
                        : null,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Flexible(
                            child: AutoSizeText(
                          _controller.valueAmount == 0
                              ? 'GenerateQR'.tr
                              : '${'Confirm'.tr} ${_controller.confirmAmount}',
                          style: TextStyle(
                              fontSize: 14,
                              color: !_controller.isMaximumAmount
                                  ? AppColors.primary
                                  : AppColors.grey,
                              fontWeight: FontWeight.w400),
                        )),
                      ],
                    ),
                  )),
            ),
          ],
        ));
  }

  Widget _sharingNoteWidget() {
    return Obx(
      () => InkWell(
        onTap: () {
          FocusManager.instance.primaryFocus?.unfocus();
        },
        child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                "PaymentLink".tr,
                style: AppTextStyle.headline6.copyWith(color: AppColors.black),
              ),
              ContainerDevider.blankSpaceLg,
              KTextField(
                name: "Note",
                label: "Note".tr,
                keyboardType: TextInputType.multiline,
                iconData: MerchantIcon.note,
                maxLine: 5,
                maxLength: 250,
                valueCounter: _controller.shareNote.length,
                needIcon: false,
                hint: "ShortDescription".tr,
                inputFormatters: [
                  CustomLengthLimitingTextInputFormatter(250),
                ],
                onChanged: (value) => _controller.shareNote = value ?? '',
              ),
              ContainerDevider.blankSpaceSm,
              AppButtonStyle.secondaryButton(
                  label: "Copy".tr,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Icon(
                        MerchantIcon.copy,
                        size: 15,
                        color: AppColors.textPrimary,
                      ),
                      ContainerDevider.blankSpaceSm,
                      Text(
                        "Copy".tr,
                        style: AppTextStyle.body2
                            .copyWith(color: AppColors.textPrimary),
                      )
                    ],
                  ),
                  onPressed: () async {
                    _controller.backAsync();
                    await _controller.copyLinkedAsync();
                  }),
              ContainerDevider.blankSpaceSm,
              AppButtonStyle.primaryButton(
                label: "ShareLink",
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Icon(MerchantIcon.share, size: 15),
                    ContainerDevider.blankSpaceSm,
                    Text(
                      " ${"ShareLink".tr} ",
                      style: AppTextStyle.body2,
                    )
                  ],
                ),
                onPressed: () async {
                  _controller.backAsync();
                  await _controller.shareLinkedAsync();
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
