import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class TabTransactionScreen extends StatefulWidget {
  const TabTransactionScreen({Key? key}) : super(key: key);

  @override
  State<TabTransactionScreen> createState() => TabTransactionScreenState();
}

class TabTransactionScreenState extends State<TabTransactionScreen>
    with AutomaticKeepAliveClientMixin {
  final TabTransactionScreenController _controller =
      Get.put<TabTransactionScreenController>(TabTransactionScreenController());

  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Obx(() => ContainerTemp(
          header: Padding(
              padding: const EdgeInsets.only(left: 15, right: 15, bottom: 10),
              child: _header()),
          child: SmartRefresher(
              header: AppSmartRefresh.shared.header,
              footer: AppSmartRefresh.shared.footer,
              controller: _controller.refreshController,
              onLoading: () async {
                await Future.delayed(const Duration(seconds: 2));
                _controller.refreshController.loadComplete();
              },
              onRefresh: () async {
                await _controller.getTransactionListAsync(
                    checkListEmpty: false);
              },
              enablePullDown: true,
              enablePullUp: true,
              physics: const ClampingScrollPhysics(),
              child: _controller.isOnline
                  ? _controller.isNoData
                      ? EmptyListBuilder(
                          emptyImageAsset: PngAppAssets.icEmptyBusiness,
                          message: "NoTransactionHistory".tr,
                          description: "NoTransactionHistoryDescription".tr,
                        )
                      : SingleChildScrollView(
                          physics: const NeverScrollableScrollPhysics(),
                          child: Padding(
                              padding: const EdgeInsets.all(15),
                              child: Column(children: [
                                totalTrx(),
                                Divider(
                                  color: AppColors.greyishHint,
                                  height: 32.h,
                                ),
                                _groupedListBuilder()
                              ])))
                  : ErrorNetworkWidget(
                      onRetryPressed: _controller.retryAsync,
                    )),
        ));
  }

//#region ==================== Components Header =======================

  Widget _header() {
    return Obx(
      () => Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: [
              Expanded(
                  child: Text(
                "Transactions".tr,
                style: AppTextStyle.headline6,
              )),
              Row(
                children: [
                  searchButton(context),
                  if (AppBusinessTempInstant
                      .selectedBusiness.id.isNotEmpty) ...[
                    filterButton(),
                  ]
                ],
              ),
            ],
          ),
          downloadReportPart()
        ],
      ),
    );
  }

  Widget searchButton(BuildContext context) {
    return InkWell(
      onTap: () async {
        // _controller.toSearchTxnAsync();
        await showFullScreenSearch(
            context: context, delegate: TabTransactionsScreenDelegate());
      },
      child: Container(
        margin: const EdgeInsets.all(0),
        padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 7),
        child: Icon(
          Icons.search,
          size: 22.w,
          color: AppColors.white,
        ),
      ),
    );
  }

  Widget filterButton() {
    return InkWell(
      onTap: () => {_controller.toFilterTxnAsync()},
      child: Container(
        margin: const EdgeInsets.all(0),
        padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 7),
        decoration: const BoxDecoration(
          color: AppColors.transparent,
        ),
        child: Obx(
          () => Icon(
            _controller.filterIconData,
            size: 22.w,
            color: AppColors.white,
          ),
        ),
      ),
    );
  }

  Widget downloadReportPart() {
    return SizedBox(
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Visibility(
              visible: _controller.transactionFilter.hasFilter,
              child: Padding(
                padding: const EdgeInsets.only(right: 8),
                child: InkWell(
                  onTap: () {
                    _controller.clearAllFilters();
                  },
                  child: Container(
                    alignment: Alignment.center,
                    margin: const EdgeInsets.all(0),
                    padding:
                        EdgeInsets.symmetric(vertical: 4.w, horizontal: 10.w),
                    decoration: BoxDecoration(
                      color: AppColors.white.withOpacity(0.2),
                      borderRadius: BorderRadius.all(
                        Radius.circular(10.r),
                      ),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Padding(
                          padding: EdgeInsets.only(right: 4.w),
                          child: Icon(
                            Icons.cancel_outlined,
                            size: 18.r,
                            color: AppColors.white,
                          ),
                        ),
                        Text(
                          "ClearAllFilters".tr,
                          textAlign: TextAlign.center,
                          style: AppTextStyle.body2.copyWith(
                            color: AppColors.white,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            InkWell(
              onTap: () async {
                _controller.onDownloadButtonTapAsync(context);
              },
              child: Container(
                alignment: Alignment.center,
                margin: const EdgeInsets.all(0),
                padding: EdgeInsets.symmetric(vertical: 4.w, horizontal: 10.w),
                decoration: BoxDecoration(
                  color: AppColors.white.withOpacity(0.2),
                  borderRadius: BorderRadius.all(
                    Radius.circular(10.r),
                  ),
                ),
                child: Text(
                  _controller.downloadButtonTitle.tr,
                  textAlign: TextAlign.center,
                  style: AppTextStyle.body2.copyWith(
                    color: AppColors.white,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

//#endregion

//#region ==================== Components Summary of Transaction =======================
  Widget totalTrx() {
    return Container(
      decoration: BoxDecoration(
          color: AppColors.white,
          border: Border.all(color: AppColors.transparent, width: 0.0)),
      child: Row(
        children: [
          Expanded(
              child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SvgPicture.asset(
                SvgAppAssets.wallet,
                width: 30.w,
              ),
              SizedBox(
                width: 10.w,
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "NetSale".tr,
                      style: AppTextStyle.caption.copyWith(
                        color: AppColors.black.withOpacity(0.8),
                      ),
                    ),
                    Obx(() => Text(
                          "${CurrencyConverter.currencyStringToSymble(AppBusinessTempInstant.selectedBusiness.accountCurrency)}${_controller.totalNetSale.toThousandSeparatorString(d2: isUSDCurrency)}",
                          textAlign: TextAlign.start,
                          style: AppTextStyle.body1.copyWith(
                            fontWeight: FontWeight.w700,
                            color: AppColors.textPrimary,
                          ),
                        ))
                  ],
                ),
              )
            ],
          )),
          Expanded(
            child: Row(
              children: [
                SvgPicture.asset(
                  SvgAppAssets.transaction,
                  height: 30.w,
                ),
                SizedBox(
                  width: 10.w,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "TotalTransaction".tr,
                      textAlign: TextAlign.start,
                      style: AppTextStyle.caption.copyWith(
                        color: AppColors.black.withOpacity(0.8),
                      ),
                    ),
                    Obx(
                      () => Text(
                        _controller.transactionList.length
                            .toThousandSeparatorString(),
                        textAlign: TextAlign.start,
                        style: AppTextStyle.body1.copyWith(
                          fontWeight: FontWeight.w700,
                          color: AppColors.textPrimary,
                        ),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
//#endregion

//#region ==================== Components group item list =======================
  Widget _groupedListBuilder() {
    return GroupedListView<TransactionItemListModel, DateTime>(
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      padding: EdgeInsets.zero,
      sort: false,
      elements: _controller.transactionList,
      groupBy: (value) {
        return value.transactionDate.toLocalDateTime("yyyy-MM-dd");
      },
      order: GroupedListOrder.DESC,
      groupHeaderBuilder: (value) {
        return Obx(() => headerItem(value));
      },
      itemBuilder: (context, transaction) {
        return Obx(() => listItem(transaction));
      },
      separator: Padding(
        padding: const EdgeInsets.only(left: 10, right: 5),
        child: SizedBox(
            child: Divider(
          height: 1.h,
          thickness: 1.h,
          color: AppColors.listSeparator.withOpacity(0.15),
          indent: 0,
          endIndent: 0,
        )),
      ),
    );
  }

  Widget headerItem(TransactionItemListModel item) {
    return Text(item.transactionDate.toTimeAgoDate(item.transactionDate),
        style: AppTextStyle.body1.copyWith(
          color: AppColors.textPrimary,
          fontWeight: FontWeight.w700,
        ));
  }

  Widget listItem(TransactionItemListModel item) {
    Color color = item.transactionType.toUpperCase() ==
            TransactionTypeEnum.refunded.getTitle.toUpperCase()
        ? AppColors.primary
        : item.status.toUpperCase() ==
                TransactionStatusEnum.completed.getTitle.toUpperCase()
            ? AppColors.success
            : AppColors.darkYellow;
    return AnimatedContainer(
        curve: Curves.easeOutCirc,
        padding: const EdgeInsets.all(0.0),
        decoration: BoxDecoration(
            color: AppColors.transparent,
            borderRadius: BorderRadius.circular(0.0),
            border: Border.all(
                color: Colors.transparent,
                width: 0.0,
                style: BorderStyle.none)),
        duration: const Duration(seconds: 3),
        child: InkWell(
          onTap: () => _controller.toTxnDetailAsync(item),
          child: Padding(
              padding: const EdgeInsets.only(
                top: 10,
                bottom: 10,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  // CustomImage(
                  //   item.transactionType == FilterPaymentMethod.canadiaPay.name
                  //       ? PngAppAssets.trxCnbLogo
                  //       : PngAppAssets.trxKhQrLogo,
                  //   width: 44.w,
                  //   height: 44.w,
                  //   radius: 100,
                  //   bgColor: AppColors.greyishHint,
                  //   isNetwork: false,
                  //   isShadow: true,
                  // ),
                  Container(
                    decoration: BoxDecoration(
                        color: color.withOpacity(0.1), shape: BoxShape.circle),
                    width: 44.w,
                    height: 44.w,
                    child: Icon(
                      MerchantIcon.transactionItem,
                      color: color,
                      size: 20,
                    ),
                  ),
                  SizedBox(
                    width: 10.w,
                  ),
                  Expanded(
                      child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Row(
                        children: [
                          Expanded(
                            child: Text(
                              item.accountName.toUpperCase(),
                              textAlign: TextAlign.start,
                              style: AppTextStyle.body2.copyWith(
                                color: AppColors.textPrimary,
                                fontWeight: FontWeight.w800,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.symmetric(
                                horizontal: 8.w, vertical: 4.h),
                            decoration: BoxDecoration(
                              color: AppColors.textFieldBackground,
                              borderRadius: BorderRadius.circular(5),
                            ),
                            child: Text(
                              '${item.amount < 0 ? '' : '+'}${item.amount.toThousandSeparatorString(d2: isUSDCurrency)} ${item.currency}'
                                  .toUpperCase(),
                              textAlign: TextAlign.end,
                              style: AppTextStyle.body2.copyWith(color: color),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            kDateTimeFormat(item.transactionDate,
                                showTimeOnly: true),
                            textAlign: TextAlign.end,
                            style: AppTextStyle.caption.copyWith(
                              color: AppColors.textSecondary,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ))
                ],
              )),
        ));
  }
//#endregion
}
