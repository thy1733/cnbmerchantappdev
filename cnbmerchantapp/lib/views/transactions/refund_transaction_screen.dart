import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

class RefundTransactionScreen extends StatefulWidget {
  const RefundTransactionScreen({Key? key}) : super(key: key);

  @override
  State<RefundTransactionScreen> createState() =>
      _RefundTransactionScreenState();
}

class _RefundTransactionScreenState extends State<RefundTransactionScreen>
    with SingleTickerProviderStateMixin {
  final RefundTransactionController _controller =
      Get.put(RefundTransactionController());

  @override
  void initState() {
    _controller.getTransactionAsync();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ContainerTemp(
      header: NavigationTitle(title: 'Refund'.tr),
      child: Padding(
        padding: const EdgeInsets.all(15.0),
        child: _body(context),
      ),
    );
  }

  Widget _body(BuildContext context) {
    return Column(
      children: [
        Expanded(
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                KTextField(
                  initialValue: '0',
                  name: 'RefundAmount',
                  hint: 'RefundAmount'.tr,
                  iconData: MerchantIcon.dollarSign,
                  validatorMessage: '',
                  inputFormatters: [
                    TextInputFormatter.withFunction((oldValue, newValue) {
                      return maxValueTextInputFormatter(
                          oldValue, newValue, _controller.transaction.amount);
                    })
                  ],
                  keyboardType: const TextInputType.numberWithOptions(
                      decimal: true, signed: false),
                  label: 'RefundAmount'.tr,
                  onChanged: (value) {
                    _controller.amountToRefund = double.parse(value!);
                  },
                ),
                Center(
                    heightFactor: 0.2,
                    widthFactor: 1,
                    child: Padding(
                      padding: const EdgeInsets.only(
                        left: 15,
                        right: 5,
                        top: 0,
                        bottom: 15,
                      ),
                      child: Text.rich(
                        TextSpan(
                          style: const TextStyle(
                              color: AppColors.textPrimary, fontSize: 11),
                          children: <TextSpan>[
                            TextSpan(text: 'MaxAmount'.tr),
                            TextSpan(
                                text:
                                    ' (${_controller.transaction.amount} ${_controller.transaction.currency}).'
                                        .toUpperCase(),
                                style: const TextStyle(
                                    fontWeight: FontWeight.bold)),
                          ],
                        ),
                      ),
                    )),
                const SizedBox(
                  height: 10,
                ),
                KTextField(
                  name: 'Remark',
                  hint: 'Remark'.tr,
                  iconData: MerchantIcon.remark,
                  keyboardType: TextInputType.text,
                  label: 'Remark'.tr,
                  onChanged: (value) {
                    if (value != null) {
                      _controller.remark = value;
                    }
                  },
                ),
              ],
            ),
          ),
        ),
        Obx(
          () => AppButtonStyle.primaryButton(
            label: "Next".tr,
            isEnable: _controller.amountToRefund > 0,
            onPressed: () => _controller.confirmRefundTransactionAsync(),
          ),
        )
      ],
    );
  }
}
