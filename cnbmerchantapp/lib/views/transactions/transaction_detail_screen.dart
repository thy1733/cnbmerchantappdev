import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

class TransactionDetailScreen extends StatefulWidget {
  const TransactionDetailScreen({Key? key}) : super(key: key);

  @override
  State<TransactionDetailScreen> createState() =>
      _TransactionDetailScreenState();
}

class _TransactionDetailScreenState extends State<TransactionDetailScreen> {
  final TransactionDetailScreenController _controller =
      Get.put(TransactionDetailScreenController());

  @override
  void dispose() {
    _controller.lightThemMode();
    super.dispose();
  }

  String getAmountTitle(TransactionItemDetailsOutputModel itemDetail) {
    if (itemDetail.transactionType.toUpperCase() ==
        TransactionTypeEnum.received.getTitle.toUpperCase()) {
      if (itemDetail.status.toUpperCase() ==
              TransactionStatusEnum.refunded.getTitle.toUpperCase() &&
          !itemDetail.isAvailableRefundAmount) {
        return "Refunded";
      } else {
        return "TotalAmount";
      }
    } else {
      return "RefundAmount";
    }
  }

  String getPaymentProcessedByTitle(
      TransactionItemDetailsOutputModel itemDetail) {
    if (itemDetail.transactionType.toUpperCase() ==
        TransactionTypeEnum.received.getTitle.toUpperCase()) {
      return "PaymentReceivedBy";
    } else {
      return "PaymentRefundedBy";
    }
  }

  bool _refundButtonCheck(TransactionItemDetailsOutputModel itemDetail) {
    Duration difference = DateTime.now().difference(itemDetail.transactionDate);
    bool isPast48Hours = difference.inHours >= itemDetail.allowRefundDuration;
    if (isPast48Hours) {
      return false;
    } else {
      if (AppUserTempInstant.appCacheUser.isRefundable == "false") {
        return false;
      } else {
        if (itemDetail.transactionType.toUpperCase() ==
            TransactionTypeEnum.refunded.getTitle.toUpperCase()) {
          // Red
          return false;
        } else {
          // Yellow, Green
          return itemDetail.paymentMethodName.toUpperCase() ==
                  TransactionPaymentMethodEnum.canamerchant.getTitle
                      .toUpperCase() &&
              itemDetail.isAvailableRefundAmount;
        }
      }
    }
  }

  Color _statusColorChk(TransactionItemDetailsOutputModel itemDetail) {
    var color = AppColors.textPrimary;
    if (itemDetail.transactionType.toUpperCase() ==
        TransactionTypeEnum.refunded.getTitle.toUpperCase()) {
      color = AppColors.primary;
    } else {
      if (itemDetail.status.toUpperCase() ==
          TransactionStatusEnum.completed.getTitle.toUpperCase()) {
        color = AppColors.success;
      } else {
        color = AppColors.darkYellow;
      }
    }

    return color;
  }

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      final itemDetail = _controller.itemDetail;
      Color statusColor = _statusColorChk(itemDetail);
      return ContainerAnimatedTemplate(
        padding: EdgeInsets.zero,
        needBottomSpace: false,
        floatingActionButton: _controller.isLoading
            ? const SizedBox.shrink()
            : TransactionDetailBottomWidget(
                showRefundButton: _refundButtonCheck(itemDetail),
                amountTitle: getAmountTitle(itemDetail).tr,
                amount: itemDetail.amount,
                currency: itemDetail.currency,
                statusColor: statusColor,
                allowRefundDuration: itemDetail.allowRefundDuration,
                onRefundPressed: () async {
                  _controller.toRefundTransactionAsync();
                },
                onSharePressed: () async => _controller.shareTransactionAsync(
                    widget: _invoiceTosaveWidget()),
              ),
        resizeToAvoidBottomInset: false,
        title: "TransactionDetail".tr,
        child: _controller.isLoading
            ? const SizedBox.shrink()
            : SingleChildScrollView(
                physics: const NeverScrollableScrollPhysics(),
                padding: const EdgeInsets.fromLTRB(15, 15, 15, 0).r,
                child: Column(
                  children: [
                    AppNetworkImage(
                      _controller.outletDetail.outletLogo,
                      height: 0,
                      width: 0,
                    ),
                    TransactionDetailTypeTopWidget(
                      transactionType: itemDetail.transactionType,
                      statusColor: statusColor,
                      transactionDate: itemDetail.transactionDate,
                    ),
                    // Why user smarter refresh becaseu when click on item list in search page it a have a problem with UI constrant.
                    const SizedBox(
                      height: 15,
                    ),
                    TransactionDetailBodyWidget(
                      transactionId: itemDetail.transactionId,
                      outletName: itemDetail.outletName,
                      paymentProcessedByTitle:
                          getPaymentProcessedByTitle(itemDetail),
                      paymentProcessedBy: itemDetail.paymentProcessedBy,
                      paymentMethodName: itemDetail.paymentMethodName,
                      customerName: itemDetail.customerName,
                      customerBankName: itemDetail.customerBankName,
                      customerAccount: itemDetail.customerAccount,
                      bakongHash: itemDetail.bakongHash,
                      amount: itemDetail.amount,
                      currency: itemDetail.currency,
                      remark: itemDetail.remark,
                      items: itemDetail.items,
                      onItemPressed: (item) async =>
                          _controller.toRefundTransactionDetailAsync(
                              item.referalTransactionId),
                    ),
                  ],
                ),
              ),
      );
    });
  }

  Widget _invoiceTosaveWidget() {
    return Container(
      width: double.infinity,
      color: AppColors.white,
      constraints: const BoxConstraints(maxWidth: 390),
      padding: const EdgeInsets.all(20),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          _outletInfo(),
          ContainerDevider.blankSpaceLg,
          ContainerDevider.dashHorizontal,
          ContainerDevider.blankSpaceMd,
          ..._invoiceItemList(),
          ContainerDevider.blankSpaceMd,
          ContainerDevider.dashHorizontal,
          ContainerDevider.blankSpaceLg,
          _totalAmount(),
          ContainerDevider.blankSpaceMd,
        ],
      ),
    );
  }

  Widget _outletInfo() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          decoration: const BoxDecoration(
            color: AppColors.greyBackground,
            shape: BoxShape.circle,
          ),
          child: AppNetworkImage(
            _controller.outletDetail.outletLogo,
            height: 65,
            width: 65,
            shape: BoxShape.circle,
            errorWidget: SvgPicture.asset(SvgAppAssets.pOutletList),
          ),
        ),
        ContainerDevider.blankSpaceSm,
        Text(
          _controller.outletDetail.outletName,
          style: AppTextStyle.header1.copyWith(),
        ),
        // ContainerDevider.blankSpaceSm,
        Text(
          _controller.outletDetail.locationAddress,
          textAlign: TextAlign.center,
          style: AppTextStyle.overline,
        )
      ],
    );
  }

  List<Widget> _invoiceItemList() {
    return [
      _invoiceItem("Status".tr, _controller.itemDetail.status.tr,
          color: _statusColorChk(_controller.itemDetail)),
      _invoiceItem("Type".tr, _controller.itemDetail.transactionType.tr,
          color: _statusColorChk(_controller.itemDetail)),
      _invoiceItem(
          "Date".tr,
          _controller.itemDetail.transactionDate
              .toFormatDateString("dd/MM/yyyy hh:mm a")),
      _invoiceItem("TransactionId".tr, _controller.itemDetail.transactionId),
      _invoiceItem("BakongHash".tr, _controller.itemDetail.bakongHash),
      _invoiceItem("CustomerName".tr, _controller.itemDetail.customerName),
      _invoiceItem(
          "CustomerAccount".tr, _controller.itemDetail.customerAccount),
      _invoiceItem(
          "PaymentMethod".tr, _controller.itemDetail.paymentMethodName),
      // _invoiceItem("AcceptedVia".tr, _controller.itemDetail.a),
      _invoiceItem(getPaymentProcessedByTitle(_controller.itemDetail).tr,
          _controller.itemDetail.paymentProcessedBy),
    ];
  }

  Widget _invoiceItem(String label, String value, {Color? color}) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 7),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            label,
            style: AppTextStyle.body2.copyWith(
              color: AppColors.textPrimary,
              fontWeight: FontWeight.w300,
              fontSize: 13.sp,
            ),
          ),
          Text(
            value,
            style: AppTextStyle.body2.copyWith(
              color: color ?? AppColors.textPrimary,
              fontWeight: FontWeight.w700,
              fontSize: 13.sp,
            ),
          )
        ],
      ),
    );
  }

  Row _totalAmount() {
    return Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Image.asset(
            PngAppAssets.appIcon,
            height: 50,
            width: 50,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Text(
                "${_controller.itemDetail.amount.toThousandSeparatorString(d2: isUSDCurrency)} ${_controller.itemDetail.currency}",
                style: TextStyle(
                    fontSize: 16.sp,
                    color: _statusColorChk(_controller.itemDetail),
                    fontWeight: FontWeight.w700),
              ),
              Text(
                "TotalAmount".tr,
                style: AppTextStyle.body2.copyWith(
                  color: AppColors.textPrimary,
                  fontWeight: FontWeight.w300,
                  fontSize: 13.sp,
                ),
              ),
            ],
          )
        ]);
  }
}
