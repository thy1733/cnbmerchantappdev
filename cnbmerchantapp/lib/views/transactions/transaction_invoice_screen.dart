import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:screen_brightness/screen_brightness.dart';

class TransactionInvoiceScreen extends StatefulWidget {
  const TransactionInvoiceScreen({super.key});

  @override
  State<TransactionInvoiceScreen> createState() =>
      _TransactionInvoiceScreenState();
}

class _TransactionInvoiceScreenState extends State<TransactionInvoiceScreen> {
  final _controller = Get.put(TransactionInvoiceScreenController());
  SizedBox get _separater => SizedBox(height: 30.h);

  final invoiceKey = GlobalKey();

  @override
  void dispose() {
    super.dispose();
    ScreenBrightness().resetScreenBrightness();
  }

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Material(
        child: Stack(children: [
          // fake background
          ContainerTemp(
              header: const SizedBox(height: 90), child: const SizedBox()),
          // bluring background
          ContainerBlur(
              decoration: BoxDecoration(
                color: Colors.black.withOpacity(0.5),
              ),
              child: AnimatedCrossFade(
                firstChild: Container(
                  height: Get.height,
                  alignment: Alignment.center,
                  child: Lottie.asset(JsonAppAssets.cnbMerchantLoading,
                      height: 120),
                ),
                secondChild: InkWell(
                  onTap: _controller.onBackgroundClick,
                  child: _child(),
                ),
                crossFadeState: _controller.isLoading
                    ? CrossFadeState.showFirst
                    : CrossFadeState.showSecond,
                duration: const Duration(milliseconds: 400),
              )),
        ]),
      ),
    );
  }

  Widget _child() {
    return SafeArea(
      maintainBottomViewPadding: true,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 20.sp),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            _invoiceWidget(),
            _separater,
            _shareButtons(),
          ],
        ),
      ),
    );
  }

  Widget _invoiceWidget() {
    return Container(
      key: invoiceKey,
      decoration: BoxDecoration(
          color: AppColors.white, borderRadius: BorderRadius.circular(10.r)),
      width: double.infinity,
      constraints: const BoxConstraints(maxWidth: 390),
      padding: const EdgeInsets.all(20),
      child: Column(
        children: [
          _outletInfo(),
          ContainerDevider.blankSpaceLg,
          ContainerDevider.dashHorizontal,
          ContainerDevider.blankSpaceMd,
          ..._invoiceItemList(),
          ContainerDevider.blankSpaceMd,
          ContainerDevider.dashHorizontal,
          ContainerDevider.blankSpaceLg,
          _totalAmount(),
          ContainerDevider.blankSpaceMd,
        ],
      ),
    );
  }

  Widget _invoiceTosaveWidget() {
    return Container(
      width: double.infinity,
      color: AppColors.white,
      constraints: const BoxConstraints(maxWidth: 390),
      padding: const EdgeInsets.all(20),
      child: Column(
        children: [
          _outletInfo(),
          ContainerDevider.blankSpaceLg,
          ContainerDevider.dashHorizontal,
          ContainerDevider.blankSpaceMd,
          ..._invoiceItemList(),
          ContainerDevider.blankSpaceMd,
          ContainerDevider.dashHorizontal,
          ContainerDevider.blankSpaceLg,
          _totalAmount(),
          ContainerDevider.blankSpaceMd,
        ],
      ),
    );
  }

  Widget _outletInfo() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          decoration: const BoxDecoration(
            color: AppColors.greyBackground,
            shape: BoxShape.circle,
          ),
          child: AppNetworkImage(
            AppOutletTempInstant.selectedOutlet.outletLogo,
            height: 65,
            width: 65,
            shape: BoxShape.circle,
            errorWidget: SvgPicture.asset(SvgAppAssets.pOutletList),
          ),
        ),
        ContainerDevider.blankSpaceSm,
        Text(
          _controller.merchantName,
          style: AppTextStyle.header1.copyWith(),
        ),
        // ContainerDevider.blankSpaceSm,
        Text(
          AppOutletTempInstant.selectedOutlet.locationAddress,
          textAlign: TextAlign.center,
          style: AppTextStyle.overline,
        )
      ],
    );
  }

  List<Widget> _invoiceItemList() {
    return [
      _invoiceItem("Status".tr, _controller.invoiceDetail.status.tr,
          color: _controller.invoiceDetail.isSuccess
              ? AppColors.success
              : AppColors.primaryDark),
      _invoiceItem(
          "Date".tr,
          _controller.invoiceDetail.dateTime
              .toFormatDateString("dd/MM/yyyy hh:mm a")),
      _invoiceItem("TransactionId".tr, _controller.invoiceDetail.bankref),
      _invoiceItem("BakongHash".tr, _controller.invoiceDetail.bakongHash),
      _invoiceItem("CustomerName".tr,
          _controller.invoiceDetail.customerName),
         _invoiceItem("CustomerAccount".tr,
          _controller.invoiceDetail.customerAccount),    
      _invoiceItem("PaymentMethod".tr, _controller.invoiceDetail.paymentMethod),
      _invoiceItem("AcceptedVia".tr, _controller.invoiceDetail.acceptedVia),
      _invoiceItem(
          "PaymentReceiveBy".tr,
          _controller.invoiceDetail.cashierName.isNotEmpty &&
                  _controller.invoiceDetail.cashierName != " "
              ? _controller.invoiceDetail.cashierName
              : AppUserTempInstant.appCacheUser.displayName),
    ];
  }

  Widget _invoiceItem(String label, String value, {Color? color}) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 7),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            label,
            style: AppTextStyle.body2.copyWith(
              color: AppColors.textPrimary,
              fontWeight: FontWeight.w300,
              fontSize: 13.sp,
            ),
          ),
          Text(
            value,
            style: AppTextStyle.body2.copyWith(
              color: color ?? AppColors.textPrimary,
              fontWeight: FontWeight.w700,
              fontSize: 13.sp,
            ),
          )
        ],
      ),
    );
  }

  Row _totalAmount() {
    return Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Image.asset(
            PngAppAssets.appIcon,
            height: 50,
            width: 50,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Text(
                "${_controller.invoiceDetail.amount.toThousandSeparatorString(d2: isUSDCurrency)} ${_controller.invoiceDetail.currencyCode}",
                style: TextStyle(
                    fontSize: 16.sp,
                    color: AppColors.success,
                    fontWeight: FontWeight.w700),
              ),
              Text(
                "TotalAmount".tr,
                style: AppTextStyle.body2.copyWith(
                  color: AppColors.textPrimary,
                  fontWeight: FontWeight.w300,
                  fontSize: 13.sp,
                ),
              ),
            ],
          )
        ]);
  }

  Widget _shareButtons() {
    return Container(
        height: 50,
        decoration: BoxDecoration(
            color: AppColors.white.withOpacity(.15),
            borderRadius: BorderRadius.circular(25)),
        padding: const EdgeInsets.all(10),
        child: Row(
          children: [
            Expanded(
              child: _shareButtonsWidget(
                  icon: MerchantIcon.cameraOutlineBold,
                  title: "Screenshot".tr,
                  onTap: () => _controller.onScreenshotAsync(
                      _invoiceTosaveWidget(),
                      invoiceKey.currentContext?.size ?? const Size(0, 0))),
            ),
            Container(width: 1, color: AppColors.white),
            Expanded(
              child: _shareButtonsWidget(
                icon: MerchantIcon.downloadOutlineBold,
                title: "Save".tr,
                onTap: () => _controller.onSaveImageAsync(
                  _invoiceTosaveWidget(),
                  invoiceKey.currentContext?.size ?? const Size(0, 0),
                ),
              ),
            ),
            Container(width: 1, color: AppColors.white),
            Expanded(
              child: _shareButtonsWidget(
                icon: MerchantIcon.transaction,
                title: "Detail".tr,
                onTap: () => _controller.toTransactionDetail(),
              ),
            ),
            // Container(width: 1, color: AppColors.white),
            // Expanded(
            //   child: _shareButtonsWidget(
            //     icon: MerchantIcon.shareOutlineBold,
            //     title: "ShareLink".tr,
            //     // onTap: () => _controller.onShare(),
            //   ),
            // ),
          ],
        ));
  }

  Widget _shareButtonsWidget({
    IconData icon = MerchantIcon.cameraOutlineBold,
    String title = "",
    VoidCallback? onTap,
  }) {
    return InkWell(
      onTap: onTap,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Icon(icon, size: 16, color: AppColors.white),
          Text(
            title,
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 8.sp, color: AppColors.white, height: 1),
          ),
        ],
      ),
    );
  }
}
