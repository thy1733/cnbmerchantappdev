import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

class RefundTransactionDetailScreen extends StatefulWidget {
  const RefundTransactionDetailScreen({Key? key}) : super(key: key);

  @override
  State<RefundTransactionDetailScreen> createState() =>
      _RefundTransactionDetailScreenState();
}

class _RefundTransactionDetailScreenState
    extends State<RefundTransactionDetailScreen> {
  final RefundTransactionDetailScreenController _controller =
      Get.put(RefundTransactionDetailScreenController());

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _controller.onWillPop(),
      child: Obx(
        () {
          final itemDetail = _controller.itemDetail;
          return ContainerAnimatedTemplate(
            title: "TransactionDetail".tr,
            needBottomSpace: false,
            backButtonAction: _controller.onBackPressed,
            padding: EdgeInsets.zero,
            floatingActionButton: _controller.isLoading
                ? const SizedBox.shrink()
                : TransactionDetailBottomWidget(
                    showRefundButton: false,
                    amountTitle: "RefundAmount".tr,
                    amount: itemDetail.amount,
                    currency: itemDetail.currency,
                    allowRefundDuration: itemDetail.allowRefundDuration,
                    statusColor: AppColors.primary,
                    onRefundPressed: null,
                    onSharePressed: () async => _controller
                        .shareTransactionAsync(widget: _invoiceTosaveWidget()),
                  ),
            child: _controller.isLoading
                ? const SizedBox.shrink()
                : SingleChildScrollView(
                    padding: const EdgeInsets.fromLTRB(15, 15, 15, 0).r,
                    physics: const NeverScrollableScrollPhysics(),
                    child: Column(
                      children: [
                        TransactionDetailTypeTopWidget(
                          transactionType: "Refunded",
                          transactionDate: itemDetail.transactionDate,
                          statusColor: AppColors.primary,
                        ),
                        const SizedBox(
                          height: 15,
                        ),
                        TransactionDetailBodyWidget(
                          transactionId: itemDetail.transactionId,
                          outletName: itemDetail.outletName,
                          paymentProcessedByTitle: "PaymentRefundedBy".tr,
                          paymentProcessedBy: itemDetail.paymentProcessedBy,
                          paymentMethodName: itemDetail.paymentMethodName,
                          customerName: itemDetail.customerName,
                          customerBankName: itemDetail.customerBankName,
                          customerAccount: itemDetail.customerAccount,
                          bakongHash: itemDetail.bakongHash,
                          amount: itemDetail.amount,
                          currency: itemDetail.currency,
                          remark: itemDetail.remark,
                          items: const [],
                          onItemPressed: null,
                        ),
                      ],
                    ),
                  ),
          );
        },
      ),
    );
  }

  Widget _outletInfo() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          decoration: const BoxDecoration(
            color: AppColors.greyBackground,
            shape: BoxShape.circle,
          ),
          child: AppNetworkImage(
            _controller.outletDetail.outletLogo,
            height: 65,
            width: 65,
            shape: BoxShape.circle,
            errorWidget: SvgPicture.asset(SvgAppAssets.pOutletList),
          ),
        ),
        ContainerDevider.blankSpaceSm,
        Text(
          _controller.outletDetail.outletName,
          style: AppTextStyle.header1.copyWith(),
        ),
        // ContainerDevider.blankSpaceSm,
        Text(
          _controller.outletDetail.locationAddress,
          textAlign: TextAlign.center,
          style: AppTextStyle.overline,
        )
      ],
    );
  }

  Widget _invoiceTosaveWidget() {
    return Container(
      width: double.infinity,
      color: AppColors.white,
      constraints: const BoxConstraints(maxWidth: 390),
      padding: const EdgeInsets.all(20),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          _outletInfo(),
          ContainerDevider.blankSpaceLg,
          ContainerDevider.dashHorizontal,
          ContainerDevider.blankSpaceMd,
          ..._invoiceItemList(),
          ContainerDevider.blankSpaceMd,
          ContainerDevider.dashHorizontal,
          ContainerDevider.blankSpaceLg,
          _totalAmount(),
          ContainerDevider.blankSpaceMd,
        ],
      ),
    );
  }

  List<Widget> _invoiceItemList() {
    return [
      _invoiceItem("Status".tr, "Completed".tr, color: AppColors.primary),
      _invoiceItem("Type".tr, "Refunded".tr, color: AppColors.primary),
      _invoiceItem(
          "Date".tr,
          _controller.itemDetail.transactionDate
              .toFormatDateString("dd/MM/yyyy hh:mm a")),
      _invoiceItem("TransactionId".tr, _controller.itemDetail.transactionId),
      _invoiceItem("BakongHash".tr, _controller.itemDetail.bakongHash),
      _invoiceItem("CustomerName".tr, _controller.itemDetail.customerName),
      _invoiceItem(
          "CustomerAccount".tr, _controller.itemDetail.customerAccount),
      _invoiceItem(
          "PaymentMethod".tr, _controller.itemDetail.paymentMethodName),
      // _invoiceItem("AcceptedVia".tr, _controller.itemDetail.a),
      _invoiceItem(
          "PaymentRefundedBy".tr, _controller.itemDetail.paymentProcessedBy),
    ];
  }

  Widget _invoiceItem(String label, String value, {Color? color}) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 7),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            label,
            style: AppTextStyle.body2.copyWith(
              color: AppColors.textPrimary,
              fontWeight: FontWeight.w300,
              fontSize: 13.sp,
            ),
          ),
          Text(
            value,
            style: AppTextStyle.body2.copyWith(
              color: color ?? AppColors.textPrimary,
              fontWeight: FontWeight.w700,
              fontSize: 13.sp,
            ),
          )
        ],
      ),
    );
  }

  Row _totalAmount() {
    return Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Image.asset(
            PngAppAssets.appIcon,
            height: 50,
            width: 50,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Text(
                "${_controller.itemDetail.amount.toThousandSeparatorString(d2: isUSDCurrency)} ${_controller.itemDetail.currency}",
                style: TextStyle(
                    fontSize: 16.sp,
                    color: AppColors.primary,
                    fontWeight: FontWeight.w700),
              ),
              Text(
                "TotalAmount".tr,
                style: AppTextStyle.body2.copyWith(
                  color: AppColors.textPrimary,
                  fontWeight: FontWeight.w300,
                  fontSize: 13.sp,
                ),
              ),
            ],
          )
        ]);
  }
}
