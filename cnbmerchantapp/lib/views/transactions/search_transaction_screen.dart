import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

class SearchTransactionScreen extends StatefulWidget {
  const SearchTransactionScreen({Key? key}) : super(key: key);

  @override
  State<SearchTransactionScreen> createState() =>
      _SearchTransactionScreenState();
}

class _SearchTransactionScreenState extends State<SearchTransactionScreen> {
  final SearchTransactionScreenController controller =
      Get.put(SearchTransactionScreenController());

  @override
  void initState() {
    controller.darkThemMode();
    super.initState();
  }

  @override
  void dispose() {
    controller.lightThemMode();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ContainerSearchTemp(
      header: SearchTopbarWidget(
          title: 'Search'.tr,
          onChanged: controller.onUserChange,
          onSubmited: controller.onUserSubmit,
          onClear: controller.onUserClear),
      child: Expanded(
          child: Obx(() => controller.transactionList.isEmpty
              ? EmptyListBuilder(
                  emptyImageAsset: PngAppAssets.icEmptyBusiness,
                  message: "",
                  description: "NoData".tr)
              : ListView.separated(
                  padding: const EdgeInsets.fromLTRB(15, 15, 15, 0).r,
                  addAutomaticKeepAlives: true,
                  shrinkWrap: true,
                  physics: const ScrollPhysics(),
                  itemCount: controller.transactionList.length,
                  itemBuilder: (context, index) {
                    return listItem(controller.transactionList[index]);
                  },
                  separatorBuilder: (context, index) => const Divider(),
                ))),
    );
  }

  //#region Item template
  Widget listItem(TransactionItemListModel item) {
    return AnimatedContainer(
        curve: Curves.easeOutCirc,
        padding: const EdgeInsets.all(0.0),
        decoration: BoxDecoration(
            color: AppColors.transparent,
            borderRadius: BorderRadius.circular(0.0),
            border: Border.all(
                color: Colors.transparent,
                width: 0.0,
                style: BorderStyle.none)),
        duration: const Duration(seconds: 3),
        child: InkWell(
          onTap: () => controller.toTxnDetailAsync(item),
          child: Padding(
              padding: const EdgeInsets.only(
                top: 10,
                bottom: 10,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  CustomImage(
                    item.transactionType == FilterPaymentMethod.canadiaPay.name
                        ? PngAppAssets.trxCnbLogo
                        : PngAppAssets.trxKhQrLogo,
                    width: 44.w,
                    height: 44.w,
                    radius: 100,
                    bgColor: AppColors.greyishHint,
                    isNetwork: false,
                    isShadow: true,
                  ),
                  SizedBox(
                    width: 10.w,
                  ),
                  Expanded(
                      child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Row(
                        children: [
                          Text(
                            item.accountName.toUpperCase(),
                            textAlign: TextAlign.start,
                            style: AppTextStyle.body2.copyWith(
                              color: AppColors.textPrimary,
                              fontWeight: FontWeight.w800,
                            ),
                          ),
                          Expanded(
                            child: Container(),
                          ),
                          Container(
                            padding: EdgeInsets.symmetric(
                                horizontal: 8.w, vertical: 4.h),
                            decoration: BoxDecoration(
                              color: AppColors.textFieldBackground,
                              borderRadius: BorderRadius.circular(5),
                            ),
                            child: Text(
                              '${item.transactionType.toUpperCase() == TransactionTypeEnum.refunded.getTitle.toUpperCase() ? '-' : '+'}${item.amount} ${item.currency}'
                                  .toUpperCase(),
                              textAlign: TextAlign.end,
                              style: AppTextStyle.body2.copyWith(
                                color: item.transactionType.toUpperCase() ==
                                        TransactionTypeEnum.refunded.getTitle
                                            .toUpperCase()
                                    ? AppColors.primary
                                    : item.status.toUpperCase() ==
                                            TransactionStatusEnum
                                                .completed.getTitle
                                                .toUpperCase()
                                        ? AppColors.success
                                        : AppColors.darkYellow,
                              ),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            kDateTimeFormat(item.transactionDate,
                                showTimeOnly: true),
                            textAlign: TextAlign.end,
                            style: AppTextStyle.caption.copyWith(
                              color: AppColors.textSecondary,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ))
                ],
              )),
        ));
  }
  //#endregion
}
