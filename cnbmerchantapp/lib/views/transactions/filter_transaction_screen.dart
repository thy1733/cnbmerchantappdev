import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

class FilterTransactionScreen extends StatefulWidget {
  const FilterTransactionScreen({Key? key}) : super(key: key);

  @override
  State<FilterTransactionScreen> createState() =>
      _FilterTransactionScreenState();
}

class _FilterTransactionScreenState extends State<FilterTransactionScreen> {
  final FilterTransactionScreenController controller =
      Get.put(FilterTransactionScreenController());

  @override
  Widget build(BuildContext context) {
    return Obx(() => ContainerAnimatedTemplate(
          title: "Filter".tr,
          floatingActionButton: AppButtonStyle.primaryButton(
              label: "Apply".tr,
              onPressed: controller.applyFilterAsync,
              margin: const EdgeInsets.symmetric(horizontal: 15).r),
          child: _body(),
        ));
  }

  //#region Components container

  Widget _body() {
    return Column(
      children: [
        ContainerButton(
            submit: (value) {
              controller.filterOptionsAsync("TransactionPeriod".tr);
            },
            child: DropdownOption(
                title: "TransactionPeriod".tr,
                value: controller.selectedDateForDisplay,
                leadingIconData: MerchantIcon.calendar)),
        ContainerButton(
            submit: (value) => controller.filterOptionsAsync("Outlet".tr),
            child: DropdownOption(
                title: "Outlet".tr,
                value: controller.selectedOutletForDisplay,
                leadingIconData: MerchantIcon.shop)),
        ContainerButton(
            submit: (value) => controller.filterOptionsAsync("Cashier".tr),
            child: DropdownOption(
                title: "Cashier".tr,
                value: controller.selectedCashierForDisplay,
                leadingIconData: MerchantIcon.user)),
        ContainerButton(
            submit: (value) =>
                controller.filterOptionsAsync("PaymentMethod".tr),
            child: DropdownOption(
                title: "PaymentMethod".tr,
                value: controller.selectedPaymentMethodForDisplay,
                leadingIconData: MerchantIcon.qrType)),
        ContainerButton(
            submit: (value) =>
                controller.filterOptionsAsync("TransactionType".tr),
            child: DropdownOption(
                title: "TransactionType".tr,
                value: controller.selectedTransactionTypeForDisplay,
                leadingIconData: MerchantIcon.transactionType))
      ],
    );
  }

  //#endregion
}
