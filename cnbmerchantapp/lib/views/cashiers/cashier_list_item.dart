import 'package:cnbmerchantapp/core.dart';
import 'package:cnbmerchantapp/helpers/components/widgets/cashier_status_item_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get_state_manager/get_state_manager.dart';

class CashierListItem extends StatelessWidget {
  const CashierListItem({
    super.key,
    required this.item,
  });

  final CashierItemListOutputModel item;

  @override
  Widget build(BuildContext context) {
    final double padding = 10.w;
    final double imageSize = 44.w;

    return Obx(
      () => Row(
        children: [
          Card(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(50),
                  side: BorderSide(color: AppColors.grey.withOpacity(.5))),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(50),
                child: AppNetworkImage(
                  item.photoUrl,
                  cacheKey: item.cacheImage,
                  width: imageSize,
                  height: imageSize,
                  errorWidget: PlaceholderImage.userName(
                      item.firstName, item.lastName,
                      fontSize: 20),
                ),
              )),
          SizedBox(
            width: padding,
          ),
          Expanded(
              flex: 1,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("${item.firstName} ${item.lastName}",
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      style: AppTextStyle.header1),
                  Text(
                    item.outlets[0].outletName,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    style: AppTextStyle.label2,
                  ),
                ],
              )),
          SizedBox(
            width: padding,
          ),
          CashierStatusItemWidget(
            status: item.cashierStatus,
          )
        ],
      ),
    );
  }
}
