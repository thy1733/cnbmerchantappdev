import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class CashierListScreen extends StatefulWidget {
  const CashierListScreen({super.key});

  @override
  State<CashierListScreen> createState() => _CashierListScreenState();
}

class _CashierListScreenState extends State<CashierListScreen> {
  final CashierListScreenController controller =
      Get.put(CashierListScreenController());

  @override
  Widget build(BuildContext context) {
    final double iconSize = 24.r;
    final double padding = 16.r;

    RefreshController refreshController =
        RefreshController(initialRefresh: false);

    return Obx(
      () => Scaffold(
        floatingActionButton: FloatingActionButton(
            heroTag: "btnCashier",
            backgroundColor: AppColors.white,
            shape: const StadiumBorder(
                side: BorderSide(color: AppColors.primary, width: 1)),
            child: SvgPicture.asset(
              SvgAppAssets.icAddCasheir,
              width: iconSize,
              height: iconSize,
            ),
            onPressed: () {
              controller.toSetupCashierScreenAsync();
            }),
        body: Padding(
          padding: EdgeInsets.all(padding),
          child: Column(
            children: [
              searchBar(
                onTap: () {
                  controller.onSearchCashierAsync();
                },
                boxShadow: const [
                  BoxShadow(blurRadius: 3, color: AppColors.grey),
                ],
              ),
              SizedBox(
                height: padding,
              ),
              Expanded(
                child: SmartRefresher(
                  controller: refreshController,
                  header: AppSmartRefresh.shared.header,
                  footer: AppSmartRefresh.shared.footer,
                  onLoading: () async {
                    await Future.delayed(const Duration(seconds: 2));
                    refreshController.loadComplete();
                  },
                  onRefresh: () async {
                    controller.getListCashier();
                    refreshController.refreshCompleted();
                  },
                  enablePullDown: true,
                  enablePullUp: true,
                  physics: const ClampingScrollPhysics(),
                  child: SingleChildScrollView(
                    padding: EdgeInsets.only(bottom: padding * 2),
                    physics: const NeverScrollableScrollPhysics(),
                    child: controller.isEmptyData
                        ? EmptyListBuilder(
                            emptyImageAsset: PngAppAssets.icEmptyBusiness,
                            message: "NoCashierYet".tr,
                            description: 'TapButtonBelowToAddNewCashier'.tr,
                          )
                        : listBuilder(),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget searchBar(
      {BoxBorder? border,
      GestureTapCallback? onTap,
      Color? color,
      required List<BoxShadow> boxShadow}) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        height: 40,
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
        decoration: BoxDecoration(
          border: border,
          color: color ?? AppColors.grey.withOpacity(0.09),
          borderRadius: const BorderRadius.all(Radius.circular(30)),
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Icon(
              Icons.search,
              size: 24.w,
              color: AppColors.grey,
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(left: 10, bottom: 2),
                child: Text(
                  "Search".tr,
                  style: const TextStyle(fontSize: 14, color: AppColors.grey),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget listBuilder() {
    return ListView.separated(
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      itemCount: controller.cashierList.length,
      separatorBuilder: (BuildContext context, int index) => const Divider(
        color: AppColors.grey,
      ),
      itemBuilder: (BuildContext context, int index) {
        return listItem(controller.cashierList[index]);
      },
    );
  }

  Widget listItem(CashierItemListOutputModel item) {
    return InkWell(
      onTap: () {
        controller.item = item;
        controller.showCashierDetail();
      },
      child: CashierListItem(
        item: item,
      ),
    );
  }
}
