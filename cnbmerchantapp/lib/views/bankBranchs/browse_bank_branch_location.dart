import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:cnbmerchantapp/core.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class BrowseBankBranchLocation extends StatefulWidget {
  const BrowseBankBranchLocation(
      {Key? key, required this.onConfirm, this.initialValue})
      : super(key: key);

  final BrowseBankBranchLocationCallBack onConfirm;
  final BankOfficeOutputModel? initialValue;

  @override
  State<BrowseBankBranchLocation> createState() =>
      _BrowseBankBranchLocationState();
}

class _BrowseBankBranchLocationState extends State<BrowseBankBranchLocation> {
  final BrowseBankBranchLocationController controller =
      Get.find<BrowseBankBranchLocationController>();
  final animateDuration = const Duration(milliseconds: 200);

  @override
  void initState() {
    controller.initialData(widget.initialValue);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      // Use [SystemUiOverlayStyle.light] for white status bar
      // or [SystemUiOverlayStyle.dark] for black status bar
      // https://stackoverflow.com/a/58132007/1321917
      value: SystemUiOverlayStyle.dark,
      child: Obx(
        () => Scaffold(
          resizeToAvoidBottomInset: false,
          body: Stack(children: [
            /// because [controller.markers] can't use with obs to notify view right now
            /// we use [controller.isLoading] for temporary solution to refresh map maker
            controller.isLoading
                ? SizedBox(
                    width: Get.width,
                    height: Get.height,
                    child: const KLoadingIndicator(),
                  )
                : GoogleMap(
                    mapType: MapType.normal,
                    myLocationEnabled: true,
                    zoomControlsEnabled: false,
                    myLocationButtonEnabled: false,
                    initialCameraPosition: controller.initialCameraPosition,
                    markers: controller.markers,
                    gestureRecognizers: {
                      Factory<OneSequenceGestureRecognizer>(
                        () => EagerGestureRecognizer(),
                      ),
                    },
                    onMapCreated: (GoogleMapController controllerMap) {
                      if (!controller.mapController.isCompleted) {
                        controller.mapController.complete(controllerMap);
                      }
                    }),

            _infoPath(),
            _searchList(),
            _searchBar(),
            _btnBack(),
          ]),
        ),
      ),
    );
  }

  // #region SHARE_COMPONENT

  ///
  /// this iclude all bottom path such as GPS and selected branch
  ///
  Widget _infoPath() => Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Expanded(child: Container()),
          Align(
            alignment: Alignment.centerRight,
            child: _gps(),
          ),
          _selectedBranch(),
          Visibility(
            visible: controller.isMapLoading || controller.isLoading,
            child: Align(
              alignment: Alignment.bottomLeft,
              child: LinearProgressIndicator(
                backgroundColor: AppColors.primary.withOpacity(0.2),
                color: AppColors.primary,
              ),
            ),
          ),
        ],
      );

  ///
  /// botton container show which brand selected as destail and button confirm
  ///
  /// !! this will fisplay/hide base on [controller.selectedBranch]
  ///
  Widget _selectedBranch() {
    return AnimatedCrossFade(
      duration: animateDuration,
      crossFadeState: controller.selectedBranch.locationName.isEmpty
          ? CrossFadeState.showFirst
          : CrossFadeState.showSecond,
      firstChild: const SizedBox(),
      secondChild: Container(
        padding: const EdgeInsets.fromLTRB(20, 20, 20, 15),
        margin: const EdgeInsets.only(top: 5),
        decoration: BoxDecoration(
          color: AppColors.white,
          boxShadow: [
            BoxShadow(blurRadius: 3, color: Colors.black.withOpacity(0.1)),
          ],
          borderRadius: const BorderRadius.only(
            topLeft: Radius.circular(30),
            topRight: Radius.circular(30),
          ),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisSize: MainAxisSize.min,
          children: [
            _branchInfo(controller.selectedBranch),

            const SizedBox(height: 30),
            // submit btn

            AppButtonStyle.primaryButton(
              label: 'ConfirmLocation'.tr,
              onPressed: () {
                widget.onConfirm(controller.selectedBranch);
              },
            ),
            getBottomPadding(Get.context!),
          ],
        ),
      ),
    );
  }

  ///
  /// show detail of branch: name description ...
  ///
  Widget _branchInfo(BankOfficeOutputModel item) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          item.locationName,
          style: const TextStyle(
            fontSize: 15,
            color: AppColors.darkGrey,
            fontWeight: FontWeight.bold,
          ),
        ),
        ContainerDevider.blankSpaceSm,
        Text.rich(
          TextSpan(
            style: const TextStyle(
                color: AppColors.textPrimary,
                fontSize: 14,
                fontWeight: FontWeight.w300),
            children: [
              item.isOpen
                  ? TextSpan(
                      text: "Open".tr,
                      style: const TextStyle(
                          color: AppColors.success,
                          fontSize: 14,
                          fontWeight: FontWeight.w700),
                    )
                  : TextSpan(text: "Close".tr),
              TextSpan(
                text:
                    " | ${AppTimeConvertor.militaryToStandard(item.startWorkingHour)} - ${AppTimeConvertor.militaryToStandard(item.stopWorkingHour)}",
              ),
            ],
          ),
        ),
        Text(item.locationAddress),
      ],
    );
  }
  // #endregion

  // #region GPS
  Widget _gps() => InkWell(
        onTap: () => controller.currentLocationAsync(),
        child: Container(
          height: 50,
          width: 50,
          margin: const EdgeInsets.all(15),
          decoration: BoxDecoration(boxShadow: [
            BoxShadow(blurRadius: 3, color: Colors.black.withOpacity(0.1)),
          ], color: AppColors.white, shape: BoxShape.circle),
          child: const Icon(
            MerchantIcon.gps,
            color: AppColors.darkGrey,
          ),
        ),
      );
  // #endregion GPS

  // #region LIST_&_SEARCH

  Widget _searchList() {
    return AnimatedCrossFade(
      duration: animateDuration,
      alignment: Alignment.bottomCenter,
      crossFadeState: controller.isOnSearching
          ? CrossFadeState.showSecond
          : CrossFadeState.showFirst,
      firstChild: SizedBox(
        height: Get.height,
        width: Get.width,
      ),
      secondChild: Container(
        height: Get.height,
        width: Get.width,
        color: AppColors.white,
        padding: EdgeInsets.only(
          top: MediaQuery.of(context).viewPadding.top + 50,
        ), // Safe area
        child: _searchListBuilder(),
      ),
    );
  }

  Widget _searchListBuilder() {
    var refCon = RefreshController();
    return InkWell(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: SmartRefresher(
        controller: refCon,
        physics: const BouncingScrollPhysics(),
        onRefresh: () async {
          await Future.delayed(const Duration(seconds: 1));
          refCon.refreshCompleted();
        },
        child: ListView.separated(
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            padding: const EdgeInsets.symmetric(vertical: 20),
            itemBuilder: (context, index) => _searchListBuilderItem(
                controller.bankBranchItems.elementAt(index)),
            separatorBuilder: (context, index) => ContainerDevider.vertical(
                  padding: EdgeInsets.zero,
                ),
            itemCount: controller.bankBranchItems.length),
      ),
    );
  }

  Widget _searchListBuilderItem(BankOfficeOutputModel item) {
    return InkWell(
      onTap: () {
        controller.onSelectSearchListItem(item);
      },
      child: Container(
        color: item == controller.selectedBranch
            ? AppColors.greyBackground
            : Colors.transparent,
        padding: const EdgeInsets.all(20),
        child: Row(
          children: [
            Expanded(child: _branchInfo(item)),
            Visibility(
                visible: item == controller.selectedBranch,
                child: const Icon(
                  MerchantIcon.check,
                  color: AppColors.primary,
                ))
          ],
        ),
      ),
    );
  }

  Widget _searchBar() {
    return Obx(
      () => AnimatedContainer(
        duration: animateDuration,
        padding: EdgeInsets.fromLTRB(
          15,
          MediaQuery.of(context).viewPadding.top + 5,
          controller.isOnSearching ? 15 : 70,
          15,
        ),
        child: kSearchbar(
          color: controller.isOnSearching
              ? AppColors.searchBackground
              : AppColors.white,
          onChange: (value) async {
            controller.onSearchBranchList(filter: value ?? '');
          },
          onSubmit: (text) {},
          onTap: () {
            controller.isOnSearching = true;
          },
          boxShadow: const [
            BoxShadow(blurRadius: 3, color: AppColors.greyBackground),
          ],
        ),
      ),
    );
  }

  Widget _btnBack() => Align(
        alignment: Alignment.topRight,
        child: Padding(
          padding: EdgeInsets.only(
            top: MediaQuery.of(context).viewPadding.top + 5,
            right: 15,
          ),
          child: InkWell(
              onTap: () {
                controller.cancelSearchOrGoBack();
                //Get.back();
              },
              child: Container(
                  height: 50,
                  width: 50,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: controller.isOnSearching
                        ? AppColors.searchBackground
                        : AppColors.white,
                  ),
                  child: const Icon(
                    MerchantIcon.x,
                    size: 15,
                    color: AppColors.black,
                  ))),
        ),
      );

  // #endregion LIST_&_SEARCH
}
