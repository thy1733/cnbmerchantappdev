import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';

class FriendDetailScreen extends StatelessWidget {
  const FriendDetailScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return ContainerTemp(
      header: const NavigationTitle(title: "Friend Detail"),
      child: Center(
        child: Text(
          "Under Development",
          style: AppTextStyle.headline5.copyWith(color: AppColors.black),
        ),
      ),
    );
  }
}
