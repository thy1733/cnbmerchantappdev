import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

typedef LocationCallBack = Function(LocationAddressModel result);

class LocationScreen extends StatefulWidget {
  const LocationScreen(
      {Key? key, required this.initLocation, required this.locationCallBack})
      : super(key: key);
  final LocationAddressModel? initLocation;
  final LocationCallBack? locationCallBack;

  @override
  State<LocationScreen> createState() => _LocationScreenState();
}

class _LocationScreenState extends State<LocationScreen> {
  final LocationScreenController controller =
      Get.put(LocationScreenController());

  @override
  void initState() {
    controller.locationModel = widget.initLocation!;
    controller.locationOriginalModel = widget.initLocation!;

    WidgetsBinding.instance.addPostFrameCallback((_) async {
      controller.isMapAvalable = true;
      if (controller.locationModel.latitude == 0.0 &&
          controller.locationModel.longitude == 0.0) {
        controller.getCurrentLocationAsync();
      } else {
        await controller.initailizeMapCamera();
      }
    });
    super.initState();
  }

  Widget _gps() => Container(
      height: 50,
      width: 50,
      margin: const EdgeInsets.all(15),
      child: ElevatedButton(
        style: ButtonStyle(
          padding: MaterialStateProperty.all(EdgeInsets.zero),
          backgroundColor: MaterialStateProperty.all(AppColors.white),
          overlayColor:
              MaterialStateProperty.all(AppColors.primary.withOpacity(0.06)),
          shape: MaterialStateProperty.all(RoundedRectangleBorder(
              side: BorderSide.none, borderRadius: BorderRadius.circular(25))),
        ),
        onPressed: () {
          controller.getCurrentLocationAsync();
        },
        child: const Icon(
          MerchantIcon.gps,
          color: AppColors.darkGrey,
        ),
      ));

  Widget _infoPath() => Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Align(
            alignment: Alignment.topRight,
            child: _gps(),
          ),
          Obx(() => AnimatedContainer(
                duration: controller.animateDuration,
                height: controller.locationModel.address.isEmpty ? 0 : null,
                padding: const EdgeInsets.all(20),
                decoration: const BoxDecoration(
                    color: AppColors.white,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(30),
                        topRight: Radius.circular(30))),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(
                      controller.locationModel.locationName,
                      style: const TextStyle(
                          color: AppColors.black,
                          fontSize: 17,
                          fontWeight: FontWeight.w400),
                    ),
                    ContainerDevider.blankSpaceSm,
                    Text(
                      controller.locationModel.address,
                      style: const TextStyle(
                        color: AppColors.grey,
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                    ContainerDevider.blankSpaceLg,

                    // submit btn
                    AppButtonStyle.primaryButton(
                        label: "ConfirmLocation".tr,
                        onPressed: () {
                          if (controller.isMapLoading) return;
                          controller.createBusinessLocation((location) {
                            widget.locationCallBack!(location);
                            Get.back();
                          });
                        }),
                  ],
                ),
              ))
        ],
      );

  Widget _btnBack() => InkWell(
      onTap: () {
        widget.locationCallBack!(controller.locationOriginalModel);
        controller.cancelSearchOrGoBack();
        //Get.back();
      },
      child: ClipOval(
          child: Container(
              height: 50,
              width: 50,
              color: controller.isMapAvalable
                  ? AppColors.white
                  : AppColors.searchBackground,
              child: const Icon(
                MerchantIcon.x,
                size: 15,
                color: AppColors.black,
              ))));

  Widget _listBuilder() {
    var refCon = RefreshController();
    return InkWell(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: SmartRefresher(
        controller: refCon,
        physics: const BouncingScrollPhysics(),
        onRefresh: () async {
          await Future.delayed(const Duration(seconds: 1));
          refCon.refreshCompleted();
        },
        child: ListView.separated(
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            padding: const EdgeInsets.all(15),
            separatorBuilder: (context, index) => SizedBox(
                    child: Divider(
                  height: 1,
                  thickness: 1,
                  color: AppColors.grey.withOpacity(0.2),
                  indent: 0,
                  endIndent: 0,
                )),
            itemCount: controller.searchAddressList.listAddress.length,
            itemBuilder: (BuildContext context, int index) {
              var item = controller.searchAddressList.listAddress[index];

              return InkWell(
                onTap: () => {controller.getGeoLocationFromAddress(item)},
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 10),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(23),
                            color: AppColors.white,
                          ),
                          height: 40,
                          width: 40,
                          child: const Icon(
                            MerchantIcon.pin,
                            size: 25,
                            color: AppColors.primary,
                          ),
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          child: Text(
                            item.description,
                            textAlign: TextAlign.start,
                            maxLines: 2,
                            style: AppTextStyle.subtitle2
                                .copyWith(color: AppColors.textPrimary),
                          ),
                        )
                      ]),
                ),
              );
            }),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      // Use [SystemUiOverlayStyle.light] for white status bar
      // or [SystemUiOverlayStyle.dark] for black status bar
      // https://stackoverflow.com/a/58132007/1321917
      value: SystemUiOverlayStyle.dark,
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        body: Stack(
          children: [
            GoogleMap(
                mapType: MapType.normal,
                zoomControlsEnabled: false,
                myLocationButtonEnabled: false,
                initialCameraPosition: controller.initialCameraPosition,
                onCameraMove: controller.onCameraMove,
                onCameraIdle: controller.onCamaraIdle,
                gestureRecognizers: {
                  Factory<OneSequenceGestureRecognizer>(
                    () => EagerGestureRecognizer(),
                  ),
                },
                onMapCreated: (GoogleMapController controllerMap) {
                  if (!controller.mapController.isCompleted) {
                    controller.mapController.complete(controllerMap);
                  }
                }),
            Center(
                child: Padding(
                    padding: const EdgeInsets.only(bottom: 30),
                    child: SvgPicture.asset(
                      SvgAppAssets.locationOn,
                      height: 48,
                    ))),
            // bottom info: when select on map this path will show info of locatopj
            _infoPath(),

            // #region this path is for search
            Obx(
              () => AnimatedCrossFade(
                duration: controller.animateDuration,
                crossFadeState: controller.isMapAvalable
                    ? CrossFadeState.showFirst
                    : CrossFadeState.showSecond,
                firstChild: SizedBox(
                  height: Get.height,
                  width: Get.width,
                ),
                secondChild: Container(
                  height: Get.height,
                  width: Get.width,
                  color: AppColors.white,
                  padding: EdgeInsets.only(
                    top: MediaQuery.of(context).viewPadding.top + 65,
                  ), // Safe area
                  child: _listBuilder(),
                ),
              ),
            ),
            // #endregion

            // #region this header button // search input and close btn
            Obx(() => AnimatedContainer(
                  duration: controller.animateDuration,
                  padding: controller.isMapAvalable
                      ? EdgeInsets.fromLTRB(
                          15, controller.safeArea.top + 5, 70, 20)
                      : EdgeInsets.fromLTRB(
                          15, controller.safeArea.top + 5, 15, 20),
                  child: kSearchbar(
                    color: !controller.isMapAvalable
                        ? AppColors.searchBackground
                        : AppColors.white,
                    onChange: (value) async {
                      controller.searchAddress = value!;
                      controller.searchAddressesFromText(value);
                      if (controller.isSearchingPlace || value.isEmpty) {
                        return;
                      }
                    },
                    onSubmit: (text) {},
                    onTap: () {
                      controller.isMapAvalable = false;
                    },
                    boxShadow: const [
                      BoxShadow(blurRadius: 3, color: AppColors.greyBackground),
                    ],
                  ),
                )),
            Padding(
              padding: EdgeInsets.only(
                top: MediaQuery.of(context).viewPadding.top + 5,
                right: 15,
              ),
              child: Align(
                alignment: Alignment.topRight,
                child: Obx(() => _btnBack()),
              ),
            ),

            Obx(() => Visibility(
                visible: controller.isMapLoading || controller.isLoading,
                child: Align(
                    alignment: Alignment.bottomLeft,
                    child: LinearProgressIndicator(
                      backgroundColor: AppColors.primary.withOpacity(0.2),
                      color: AppColors.primary,
                    )))),
            // safe area
            SizedBox(height: controller.safeArea.bottom)
            // #endregion
          ],
        ),
      ),
    );
  }
}
