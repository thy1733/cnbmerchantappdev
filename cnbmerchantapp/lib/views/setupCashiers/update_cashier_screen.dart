import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

class UpdateCashierScreen extends StatefulWidget {
  const UpdateCashierScreen({super.key});

  @override
  State<UpdateCashierScreen> createState() => _UpdateCashierScreenState();
}

class _UpdateCashierScreenState extends State<UpdateCashierScreen> {
  final SetupCashierScreenController controller =
  Get.put(SetupCashierScreenController());

  @override
  void initState() {
    controller.setupInitValue();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final double padding = 16.r;

    return Obx(
          () => ContainerAnimatedTemplate(
        title: "EditCashier".tr,
        child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisSize: MainAxisSize.min,
            children: [
              Expanded(
                  child: SingleChildScrollView(
                    physics: const ScrollPhysics(),
                    child: Column(
                      children: [
                        CircleImageUploadView(
                            placeHolder: SvgAppAssets.icAddCasheir,
                            imageFile: controller.photoFile,
                            imageUrl: controller.cashierOldItem.photoUrl,
                            onImagePicked: () {
                              controller.uploadCashierProfile();
                            }),
                        SizedBox(
                          height: padding,
                        ),
                        phoneNumberSection(),
                        firstNameSection(),
                        lastNameSection(),
                        nationalIDSection(),
                      ],
                    ),
                  )),
              ButtonCircleRadius(
                title: "UpdateCashier".tr,
                submit: (value) {
                  controller.updateCashierAsync(mode: CashierUpdateStatus.updateCashierInfo);
                },
              ),
            ]),
      ),
    );
  }

  Widget phoneNumberSection() {
    var phone = controller.cashierOldItem.phoneNumber;
    if (phone.startsWith('0')) {
      phone = phone.substring(1); // Remove the first character
    }
    return KTextField(
      focusNode: controller.phoneNumberFocus,
      textInputAction: TextInputAction.next,
      prefixText: "+855 (0)",
      name: "PhoneNumber".tr,
      hint: "XX XXX XXX",
      iconData: MerchantIcon.telephone,
      keyboardType: TextInputType.phone,
      initialValue: MaskFormat.phoneNumberString(phone),
      validatorMessage: controller.phoneNumberValidateMsg,
      onChanged: (value) {
        controller.onPhoneNumberChanged(value!);
      },
    );
  }

  Widget firstNameSection() {
    return KTextField(
      focusNode: controller.firstNameFocus,
      textInputAction: TextInputAction.next,
      name: "FirstName".tr,
      hint: "FirstName".tr,
      maxLine: 1,
      iconData: MerchantIcon.user,
      keyboardType: TextInputType.text,
      initialValue: controller.cashierOldItem.firstName,
      textCapitalization: TextCapitalization.words,
      validatorMessage: controller.firstNameValidateMsg,
      onChanged: (value) {
        controller.onFirstNameChanged(value!);
      },
    );
  }

  Widget lastNameSection() {
    return KTextField(
      focusNode: controller.lastNameFocus,
      textInputAction: TextInputAction.done,
      name: "LastName".tr,
      hint: "LastName".tr,
      maxLine: 1,
      iconData: MerchantIcon.user,
      keyboardType: TextInputType.text,
      textCapitalization: TextCapitalization.words,
      initialValue: controller.cashierOldItem.lastName,
      validatorMessage: controller.lastNameValidateMsg,
      onChanged: (value) {
        controller.onLastNameChanged(value!);
      },
    );
  }

  Widget nationalIDSection() {
    return KTextField(
      textInputAction: TextInputAction.done,
      name: "NationalID".tr,
      hint: "NationalID".tr,
      iconData: MerchantIcon.nationalId,
      keyboardType: TextInputType.number,
      inputFormatters: [MaskFormat.maskNationalID],
      initialValue: controller.cashierOldItem.nationalId,
      validatorMessage: controller.nationalIdValidateMsg,
      onChanged: (value) {
        controller.onNationalIdChanged(value!);
      },
    );
  }
}
