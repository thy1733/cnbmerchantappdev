import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

class SetupCashierScreen extends StatefulWidget {
  const SetupCashierScreen({super.key});

  @override
  State<SetupCashierScreen> createState() => _SetupCashierScreenState();
}

class _SetupCashierScreenState extends State<SetupCashierScreen> {
  final SetupCashierScreenController controller =
      Get.put(SetupCashierScreenController());

  @override
  Widget build(BuildContext context) {
    final double padding = 16.r;

    return Obx(
      () => ContainerAnimatedTemplate(
        title: "NewCashier".tr,
        child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisSize: MainAxisSize.min,
            children: [
              Expanded(
                  child: SingleChildScrollView(
                physics: const ScrollPhysics(),
                child: Column(
                  children: [
                    CircleImageUploadView(
                        placeHolder: SvgAppAssets.icAddCasheir,
                        imageFile: controller.photoFile,
                        cacheKey: controller.cashierOldItem.cacheImage,
                        onImagePicked: () {
                          controller.uploadCashierProfile();
                        }),
                    SizedBox(
                      height: padding,
                    ),
                    phoneNumberSection(),
                    firstNameSection(),
                    lastNameSection(),
                    nationalIDSection(),
                    refundSection(),
                    SizedBox(
                      height: 10.r,
                    ),
                    linkedOutletsSection(),
                    SizedBox(
                      height: padding,
                    ),
                  ],
                ),
              )),
              ButtonCircleRadius(
                title: "CreateCashier".tr,
                submit: (value) {
                  controller.createCashierAsync();
                },
              ),
            ]),
      ),
    );
  }

  Widget phoneNumberSection() {
    return KTextField(
      focusNode: controller.phoneNumberFocus,
      textInputAction: TextInputAction.next,
      prefixText: "+855 (0)",
      name: "PhoneNumber".tr,
      hint: "XX XXX XXX",
      iconData: MerchantIcon.telephone,
      keyboardType: TextInputType.phone,
      initialValue: MaskFormat.phoneNumberString(""),
      validatorMessage: controller.phoneNumberValidateMsg,
      onChanged: (value) {
        controller.onPhoneNumberChanged(value!);
      },
    );
  }

  Widget firstNameSection() {
    return KTextField(
      focusNode: controller.firstNameFocus,
      textInputAction: TextInputAction.next,
      name: "FirstName".tr,
      hint: "FirstName".tr,
      maxLine: 1,
      iconData: MerchantIcon.user,
      keyboardType: TextInputType.text,
      textCapitalization: TextCapitalization.words,
      validatorMessage: controller.firstNameValidateMsg,
      onChanged: (value) {
        controller.onFirstNameChanged(value!);
      },
    );
  }

  Widget lastNameSection() {
    return KTextField(
      focusNode: controller.lastNameFocus,
      textInputAction: TextInputAction.done,
      name: "LastName".tr,
      hint: "LastName".tr,
      maxLine: 1,
      iconData: MerchantIcon.user,
      keyboardType: TextInputType.text,
      textCapitalization: TextCapitalization.words,
      validatorMessage: controller.lastNameValidateMsg,
      onChanged: (value) {
        controller.onLastNameChanged(value!);
      },
    );
  }

  Widget nationalIDSection() {
    return KTextField(
      textInputAction: TextInputAction.done,
      name: "NationalID".tr,
      hint: "NationalID".tr,
      iconData: MerchantIcon.nationalId,
      keyboardType: TextInputType.number,
      inputFormatters: [MaskFormat.maskNationalID],
      validatorMessage: controller.nationalIdValidateMsg,
      onChanged: (value) {
        controller.onNationalIdChanged(value!);
      },
    );
  }

  Widget refundSection() {
    return Row(
      children: [
        Expanded(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Refund".tr,
                style: AppTextStyle.value,
              )
            ],
          ),
        ),
        CupertinoSwitch(
          activeColor: AppColors.primary,
          value: controller.isRefund,
          onChanged: (value) {
            controller.onRefundChanged(value);
          },
        ),
      ],
    );
  }

  Widget linkedOutletsSection() {
    final double padding = 16.r;
    final double iconSize = 30.r;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Card(
          elevation: 8,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          child: Padding(
              padding: EdgeInsets.all(padding),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Text(
                    "LinkedOutlet".tr,
                  ),
                  Text("LinkedSalePointLabel".tr),
                  SizedBox(
                    height: padding,
                  ),
                  Visibility(
                    visible: controller.selectedOutlets.isNotEmpty,
                    child: Column(
                      children: [
                        Container(
                          width: Get.width,
                          height: 1,
                          color: AppColors.greyBackground,
                        ),
                        SizedBox(
                          height: padding,
                        ),
                        ListView.separated(
                            padding: EdgeInsets.zero,
                            shrinkWrap: true,
                            physics: const NeverScrollableScrollPhysics(),
                            itemBuilder: (context, index) {
                              ItemSelectOptionModel item =
                                  controller.selectedOutlets[index];

                              return Row(
                                children: [
                                  Expanded(
                                      child: Text(
                                    item.name,
                                    style: AppTextStyle.value,
                                  )),
                                  SizedBox(
                                    width: iconSize,
                                    height: iconSize,
                                    child: IconButton(
                                        iconSize: 18.r,
                                        padding: EdgeInsets.zero,
                                        onPressed: () {
                                          controller.removeOutlets(item);
                                        },
                                        icon: const Icon(
                                          Icons.clear,
                                          color: AppColors.grey,
                                        )),
                                  )
                                ],
                              );
                            },
                            separatorBuilder: (context, index) =>
                                const Divider(),
                            itemCount: controller.selectedOutlets.length),
                        SizedBox(
                          height: padding,
                        ),
                        Container(
                          width: Get.width,
                          height: 1,
                          color: AppColors.greyBackground,
                        ),
                      ],
                    ),
                  ),
                  Visibility(
                    visible: controller.onShowLinkedOutlets(),
                    child: TextButton.icon(
                        style: AppButtonStyle.kSecondaryButtonExpandStyle(),
                        onPressed: () {
                          controller.showSelectedOutlets();
                        },
                        icon: Icon(
                          MerchantIcon.shopPlus2,
                          color: AppColors.primary,
                          size: 18.r,
                        ),
                        label: Text(
                          "LinkedOutlet".tr,
                          style: AppTextStyle.primary,
                        )),
                  )
                ],
              )),
        ),
        KTextFieldValidationMessageWidget(
            title: controller.linkedOutletValidateMsg)
      ],
    );
  }
}
