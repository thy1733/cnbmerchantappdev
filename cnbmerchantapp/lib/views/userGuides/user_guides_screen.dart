import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:get/get.dart';

class UserGuidesScreen extends StatefulWidget {
  const UserGuidesScreen({Key? key}) : super(key: key);

  @override
  State<UserGuidesScreen> createState() => _UserGuidesScreenState();
}

class _UserGuidesScreenState extends State<UserGuidesScreen> {
  final UserGuidesScreenController _controller =
      Get.put(UserGuidesScreenController());

  @override
  Widget build(BuildContext context) {
    // RefreshController refreshController =
    //     RefreshController(initialRefresh: false);
    return ContainerTemp(
        header: NavigationTitle(
          title: "UserGuides".tr,
        ),
        child: Obx(() {
          if (!_controller.isOnline) {
            return ErrorNetworkWidget(
              onRetryPressed: _controller.retryAsync,
            );
          } else if (_controller.loadWebViewError) {
            return ErrorNetworkWidget(
              assetName: JsonAppAssets.underMaintenance,
              title: "",
              description: "SomethingWentWrongPleaseTryAgainLater",
              onRetryPressed: () {
                _controller.loadWebViewError = false;
              },
            );
          } else {
            return KWebViewWidget(
              url: ValueConst.userGuidesURL.addMultipleLanguagesSupport(),
              onWebViewCreated: (_) => _controller.showLoading(),
              onUrlTapped: (url) {
                if (url.contains(ValueConst.userGuidesURL)) {
                  return NavigationActionPolicy.ALLOW;
                } else {
                  var arg = {
                    ArgumentKey.detail: url,
                    ArgumentKey.pageTitle: "Detail",
                  };
                  _controller.navigationToNamedArgumentsAsync(Routes.webView,
                      args: arg);
                  return NavigationActionPolicy.CANCEL;
                }
              },
              onError: () {
                _controller.dismiss();
                _controller.loadWebViewError = true;
              },
              onStop: () => _controller.dismiss(),
            );
          }
        }));
  }
}
