import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:cnbmerchantapp/core.dart';

class UserGuideItem extends StatelessWidget {
  UserGuideItem({
    Key? key,
    required this.userGuide,
  }) : super(key: key);

  final UserGuideItemOutputModel userGuide;
  final double imageHeight = (Get.width) / 1.7 - 40;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: Get.width,
      margin: const EdgeInsets.symmetric(vertical: 8),
      decoration: BoxDecoration(
        color: AppColors.white,
        borderRadius: BorderRadius.circular(10),
        boxShadow: [
          BoxShadow(
            color: Colors.black87.withOpacity(0.05),
            spreadRadius: 1,
            blurRadius: 1,
            offset: const Offset(0, 0.5), // changes position of shadow
          ),
        ],
      ),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisSize: MainAxisSize.min,
          children: [
            Stack(
              alignment: Alignment.center,
              children: [
                CustomImage(
                  userGuide.imageUrl,
                  isNetwork: true,
                  isShadow: false,
                  height: imageHeight,
                  width: Get.width,
                  radius: 0,
                  placeholderShape: BoxShape.rectangle,
                ),
                Container(
                  height: imageHeight,
                  width: Get.width,
                  color: Colors.black.withOpacity(0.1),
                ),
                const Icon(
                  Icons.play_circle_outline_rounded,
                  size: 55,
                  color: Colors.white,
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    userGuide.title,
                    style: AppTextStyle.body2
                        .copyWith(color: AppColors.textPrimary),
                  ),
                  SizedBox(
                    height: 4.h,
                  ),
                  Text(
                    userGuide.description,
                    style: AppTextStyle.caption.copyWith(
                      color: AppColors.textSecondary,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
