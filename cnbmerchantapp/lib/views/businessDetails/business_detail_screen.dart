import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class BusinessDetailScreen extends StatefulWidget {
  const BusinessDetailScreen({super.key});

  @override
  State<BusinessDetailScreen> createState() => _BusinessDetailScreenState();
}

class _BusinessDetailScreenState extends State<BusinessDetailScreen>
    with TickerProviderStateMixin {
  final BusinessDetailScreenController controller =
      Get.put(BusinessDetailScreenController());

  TabController? tabController;

  @override
  void initState() {
    super.initState();

    tabController = TabController(length: 2, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    final double wid = Get.width;
    final double radius = wid / 5;

    List<Tab> tabs = [
      Tab(
        text: "Outlets".tr,
      ),
      Tab(
        text: "Cashiers".tr,
      )
    ];
    List<Widget> children = [
      const OutletListScreen(),
      const CashierListScreen()
    ];

    return Obx(
      () => Scaffold(
        body: ContainerTemp(
            header: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(
                  flex: 1,
                  child: NavigationTitle(
                    title: controller.businessDetail.businessName,
                    backButton: () => Get.back(),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 16, top: 16),
                  child: OutlinedButton.icon(
                      onPressed: () {
                        controller.onShowDetailBusiness();
                      },
                      icon: const Icon(
                        Icons.info_outline,
                        color: AppColors.white,
                      ),
                      label: Text(
                        "Detail".tr,
                        style: AppTextStyle.header3,
                      ),
                      style: OutlinedButton.styleFrom(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(radius)),
                          side: const BorderSide(
                              width: 1, color: AppColors.white))),
                ),
              ],
            ),
            child: Scaffold(
              backgroundColor: AppColors.white,
              appBar: PreferredSize(
                  preferredSize: const Size.fromHeight(kToolbarHeight),
                  child: TabBar(
                    labelColor: AppColors.primary,
                    unselectedLabelColor: AppColors.grey,
                    labelStyle: const TextStyle(fontWeight: FontWeight.bold),
                    unselectedLabelStyle:
                        const TextStyle(fontWeight: FontWeight.bold),
                    indicatorColor: AppColors.primary,
                    controller: tabController,
                    tabs: tabs,
                  )),
              body: TabBarView(controller: tabController, children: children),
            )),
      ),
    );
  }
}
