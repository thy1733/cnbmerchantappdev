import 'package:cnbmerchantapp/core.dart';
import 'package:cnbmerchantapp/views/saleAnalytics/sale_analytic_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:skeletons/skeletons.dart';

class TabHomeScreen extends StatefulWidget {
  const TabHomeScreen({super.key});

  @override
  State<TabHomeScreen> createState() => _TabHomeScreenState();
}

class _TabHomeScreenState extends State<TabHomeScreen>
    with AutomaticKeepAliveClientMixin {
  final controller = Get.find<TabHomeScreenController>();

  @override
  bool get wantKeepAlive => true;


  @override
  Widget build(BuildContext context) {
    super.build(context);
    return  Obx(() => ContainerTemp(
      appShieldingText:' _controller.appShieldingInfo',
      header: header(),
      child: SaleAnalyticView(),
      // child: const AmountInputScreen(),
    ));
  }

  Widget header() {
    return Row(
      mainAxisSize: MainAxisSize.max,
      children: [
        InkWell(
          onTap: () {
            if (controller.isLoading) return;
            controller.onSelectBusinessAsync();
          },
          child: Row(
            children: [
              const SizedBox(width: 15),
              AppNetworkImage(
                AppBusinessTempInstant.selectedBusiness.businessLogo,
                height: 35,
                width: 35,
                shape: BoxShape.circle,
                errorWidget:
                    AppBusinessTempInstant.selectedBusiness.businessName.isEmpty
                        ? Container(
                            padding:const EdgeInsets.all(2),
                            color: AppColors.primary,
                            child: SvgPicture.asset(SvgAppAssets.logo),
                          )
                        : SvgPicture.asset(SvgAppAssets.pBusinessList),
                backgroundColor: AppColors.greyBackground,
              ),
              const SizedBox(width: 5),
              Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Welcome".tr,
                    style:
                        AppTextStyle.overline.copyWith(color: AppColors.white),
                  ),
                  controller.isLoading
                      ? const Opacity(
                          opacity: .3,
                          child: SkeletonAvatar(
                            style: SkeletonAvatarStyle(
                              padding: EdgeInsets.only(top: 3),
                              height: 16,
                              width: 85,
                            ),
                          ),
                        )
                      : Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              AppBusinessTempInstant
                                      .selectedBusiness.businessName.isEmpty
                                  ? "CANA Merchant"
                                  : AppBusinessTempInstant
                                      .selectedBusiness.businessName,
                              style: AppTextStyle.subtitle1.copyWith(
                                  color: AppColors.white,
                                  fontWeight: FontWeight.w700,
                                  height: 1.2),
                            ),
                            const SizedBox(width: 5),
                            if (AppUserTempInstant.appCacheUser.userType ==
                                AppUserType.businessOwner) ...[
                              const Icon(
                                MerchantIcon.chevronDown,
                                size: 14,
                                color: AppColors.white,
                              )
                            ]
                          ],
                        ),
                  ContainerDevider.blankSpaceSm
                ],
              ),
            ],
          ),
        ),
        const Expanded(
            child: SizedBox(
          height: 76,
        )),
        // InkWell(
        //   // onTap: () => CChoiceDialog.ok(
        //   //     title: "QR Scanner",
        //   //     message: "To implement QR Scanner for offline payment."),
        //   onTap: () => controller.showQrScanner(),
        //   child: const Icon(
        //     MerchantIcon.qrCode,
        //     size: 26,
        //     color: AppColors.white,
        //   ),
        // ),
        if (controller.isShowQRDisplay) ...[
          InkWell(
            onTap: () => controller.showQrDisplay(),
            child: const Padding(
                padding: EdgeInsets.all(25),
                child: Icon(
                  MerchantIcon.khqr,
                  size: 26,
                  color: AppColors.white,
                )),
          ),
        ]
        // _selecteOutlet(),
      ],
    );
  }

  // Widget _selecteOutlet() {
  //   return InkWell(
  //     onTap: () {
  //       if (AppOutletTempInstant.selectedOutlet.id.isNotEmpty) {
  //         controller.onSelectOutletAsync();
  //       }
  //     },
  //     child: Container(
  //       decoration: BoxDecoration(
  //           color: AppColors.white.withOpacity(.15),
  //           borderRadius: BorderRadius.circular(25)),
  //       padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 5),
  //       margin: const EdgeInsets.only(right: 15, bottom: 10, top: 10),
  //       child: Row(
  //         children: [
  //           if (AppOutletTempInstant.selectedOutlet.outletName.isNotEmpty) ...[
  //             const SizedBox(width: 5),
  //             const Icon(
  //               MerchantIcon.chevronDown,
  //               size: 10,
  //               color: AppColors.white,
  //             ),
  //             const SizedBox(width: 5),
  //             Text(
  //               AppOutletTempInstant.selectedOutlet.outletName,
  //               overflow: TextOverflow.ellipsis,
  //               maxLines: 1,
  //               style: AppTextStyle.subtitle1.copyWith(
  //                 color: AppColors.white,
  //                 fontSize: 11,
  //                 fontWeight: FontWeight.w600,
  //                 fontFamily: AppFontName.familyFont,
  //               ),
  //             ),
  //             const SizedBox(width: 5),
  //           ],
  //           AppNetworkImage(
  //             AppOutletTempInstant.selectedOutlet.outletLogo,
  //             height: 25,
  //             width: 25,
  //             shape: BoxShape.circle,
  //             backgroundColor: AppColors.greyBackground,
  //             cacheKey: AppOutletTempInstant.selectedOutlet.cacheKey,
  //             errorWidget: SvgPicture.asset(SvgAppAssets.pOutletList),
  //           ),
  //         ],
  //       ),
  //     ),
  //   );
  // }
}
