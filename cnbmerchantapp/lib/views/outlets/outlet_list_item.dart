import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

class OutletListItem extends StatelessWidget {
  const OutletListItem({super.key, required this.item});

  final OutletItemListOutputModel item;

  @override
  Widget build(BuildContext context) {
    final double imageSize = 44.w;

    List<CashierItemListOutputModel> cashiersToDisplay =
        item.cashiers.take(6).toList();
    int restOfCashier = item.cashiers.length - cashiersToDisplay.length;

    return Row(
      children: [
        Card(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(50),
                side: BorderSide(color: AppColors.grey.withOpacity(.5))),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(50),
              child: AppNetworkImage(
                item.outletLogo,
                cacheKey: item.cacheKey,
                width: imageSize,
                height: imageSize,
                errorWidget: SvgPicture.asset(SvgAppAssets.pOutletList),
              ),
            )),
        const SizedBox(
          width: 16,
        ),
        Expanded(
            flex: 5,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(item.outletName,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    style: AppTextStyle.header1),
                Visibility(
                  visible: item.cashiers.isNotEmpty,
                  child: Row(
                    children: [
                      Row(
                        children: List.generate(
                            cashiersToDisplay.length,
                            (index) =>
                                cashiersInOuletItem(item.cashiers[index])),
                      ),
                      Visibility(
                        visible:
                            item.cashiers.length > cashiersToDisplay.length,
                        child: Text(
                          "+$restOfCashier ${"Others".tr}",
                          style: AppTextStyle.label,
                        ),
                      )
                    ],
                  ),
                ),
                Visibility(
                    visible: item.isPrimary,
                    child: KStatusWidget(
                      name: "Primary".tr,
                      color: AppColors.primary,
                    )),
              ],
            )),
      ],
    );
  }

  Widget cashiersInOuletItem(CashierItemListOutputModel cashier) {
    final double imageSize = 25.w;
    final double padding = 7.w;

    return Card(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(50),
            side: BorderSide(color: AppColors.grey.withOpacity(.5))),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(50),
          child: AppNetworkImage(
            cashier.photoUrl,
            cacheKey: cashier.cacheImage,
            width: imageSize,
            height: imageSize,
            errorWidget: Container(
                padding: EdgeInsets.all(padding),
                width: imageSize,
                height: imageSize,
                child: SvgPicture.asset(SvgAppAssets.icAccount)),
          ),
        ));
  }
}
