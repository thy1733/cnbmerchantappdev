import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class OutletListScreen extends StatefulWidget {
  const OutletListScreen({super.key});

  @override
  State<OutletListScreen> createState() => _OutletListScreenState();
}

class _OutletListScreenState extends State<OutletListScreen> {
  final OutletListScreenController controller =
      Get.put(OutletListScreenController());

  @override
  Widget build(BuildContext context) {
    final double iconSize = 30.r;
    final double padding = 16.r;

    RefreshController refreshController =
        RefreshController(initialRefresh: false);

    return Obx(
      () => Scaffold(
        floatingActionButton: FloatingActionButton(
            heroTag: "btnOutlet",
            backgroundColor: AppColors.white,
            shape: const StadiumBorder(
                side: BorderSide(color: AppColors.primary, width: 1)),
            child: SvgPicture.asset(
              SvgAppAssets.icAddBusiness,
              width: iconSize,
              height: iconSize,
            ),
            onPressed: () {
              controller.toCreateOutletScreenAsync();
            }),
        body: Padding(
          padding: EdgeInsets.all(padding),
          child: Column(children: [
            searchBar(
              onTap: () {
                controller.onSearchOutletAsync();
              },
              boxShadow: const [
                BoxShadow(blurRadius: 3, color: AppColors.grey),
              ],
            ),
            SizedBox(
              height: padding,
            ),
            Expanded(
              child: SmartRefresher(
                controller: refreshController,
                header: AppSmartRefresh.shared.header,
                footer: AppSmartRefresh.shared.footer,
                onLoading: () async {
                  refreshController.loadComplete();
                },
                onRefresh: () async {
                  controller.retrieveOutletList();
                  refreshController.refreshCompleted();
                },
                enablePullDown: true,
                enablePullUp: true,
                physics: const ClampingScrollPhysics(),
                child: SingleChildScrollView(
                  padding: EdgeInsets.only(bottom: padding * 2),
                  physics: const NeverScrollableScrollPhysics(),
                  child: controller.isEmptyData
                      ? EmptyListBuilder(
                          emptyImageAsset: PngAppAssets.icEmptyBusiness,
                          message: "NoOutletYet".tr,
                          description: "TapButtonBelowToAddNewOutlet".tr)
                      : listBuilder(),
                ),
              ),
            ),
          ]),
        ),
      ),
    );
  }

  Widget searchBar(
      {BoxBorder? border,
      GestureTapCallback? onTap,
      Color? color,
      required List<BoxShadow> boxShadow}) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        height: 40,
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
        decoration: BoxDecoration(
          border: border,
          color: color ?? AppColors.grey.withOpacity(0.09),
          borderRadius: const BorderRadius.all(Radius.circular(30)),
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Icon(
              Icons.search,
              size: 24.w,
              color: AppColors.grey,
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(left: 10, bottom: 2),
                child: Text(
                  "Search".tr,
                  style: const TextStyle(fontSize: 14, color: AppColors.grey),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget listBuilder() {
    return ListView.separated(
      padding: EdgeInsets.zero,
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      itemCount: controller.outlets.length,
      separatorBuilder: (BuildContext context, int index) => const Divider(
        color: AppColors.grey,
      ),
      itemBuilder: (BuildContext context, int index) {
        var item = controller.outlets[index];
        // if (item.isPrimary) {
        //   item.outletLogo = controller.detailBusiness.businessLogo;
        //   item.cacheKey = controller.detailBusiness.cacheImageKey;
        // }

        return listItem(item);
      },
    );
  }

  Widget listItem(OutletItemListOutputModel item) {
    final double imageSize = 44.w;

    List<CashierItemListOutputModel> cashiersToDisplay =
        item.cashiers.take(6).toList();
    int restOfCashier = item.cashiers.length - cashiersToDisplay.length;

    return InkWell(
        onTap: () {
          controller.showOutletDetail(item);
        },
        child: Row(
          children: [
            Card(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(50),
                    side: BorderSide(color: AppColors.grey.withOpacity(.5))),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(50),
                  child: AppNetworkImage(
                    item.outletLogo,
                    cacheKey: item.cacheKey,
                    width: imageSize,
                    height: imageSize,
                    errorWidget: SvgPicture.asset(SvgAppAssets.pOutletList),
                  ),
                )),
            const SizedBox(
              width: 16,
            ),
            Expanded(
                flex: 5,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(item.outletName,
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        style: AppTextStyle.header1),
                    Visibility(
                      visible: item.cashiers.isNotEmpty,
                      child: Row(
                        children: [
                          Row(
                            children: List.generate(
                                cashiersToDisplay.length,
                                (index) =>
                                    cashiersInOuletItem(item.cashiers[index])),
                          ),
                          Visibility(
                            visible:
                                item.cashiers.length > cashiersToDisplay.length,
                            child: Text(
                              "+$restOfCashier ${"Others".tr}",
                              style: AppTextStyle.label,
                            ),
                          )
                        ],
                      ),
                    ),
                    Visibility(
                        visible: item.isPrimary,
                        child: KStatusWidget(
                          name: "Primary".tr,
                          color: AppColors.primary,
                        )),
                  ],
                )),
          ],
        ));
  }

  Widget cashiersInOuletItem(CashierItemListOutputModel cashier) {
    final double imageSize = 25.w;
    final double padding = 7.w;

    return Card(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(50),
            side: BorderSide(color: AppColors.grey.withOpacity(.5))),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(50),
          child: AppNetworkImage(
            cashier.photoUrl,
            cacheKey: cashier.cacheImage,
            width: imageSize,
            height: imageSize,
            errorWidget: Container(
                padding: EdgeInsets.all(padding),
                width: imageSize,
                height: imageSize,
                child: SvgPicture.asset(SvgAppAssets.icAccount)),
          ),
        ));
  }
}
