
import 'package:cnbmerchantapp/core.dart';
import 'package:convex_bottom_bar/convex_bottom_bar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MainScreen extends StatefulWidget {
  const MainScreen({Key? key}) : super(key: key);

  @override
  State<MainScreen> createState() => MainScreenState();
}

class MainScreenState extends State<MainScreen>
    with SingleTickerProviderStateMixin {
  final MainScreenController controller =
      Get.put<MainScreenController>(MainScreenController());

  @override
  void initState() {
    super.initState();
    controller.tabController =
        TabController(initialIndex: 0, length: 4, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    double iconSize = 19;
    // controller.isShowStatusbar = true;
    return Scaffold(
      body: Container(
          padding: EdgeInsets.zero,
          decoration: BoxDecoration(
              color: AppColors.primary,
              border: Border.all(color: AppColors.transparent, width: 0.0)),
          child: Column(
            children: [
              // AppShieldingTemp.isShowStatusbar
              // // true
              //     ? Container(
              //         height: 50,
              //         color: Colors.green,
              //         child: Align(
              //           alignment: Alignment.bottomCenter,
              //           child: AnimatedTextKit(
              //             pause: const Duration(milliseconds: 10),
              //             totalRepeatCount: 5,
              //             animatedTexts: [
              //               TypewriterAnimatedText(
              //                   'App Protected on ${AppShieldingTemp.info}',
              //                   textStyle:
              //                       const TextStyle(color: Colors.white)),
              //             ],
              //             onTap: () => {},
              //             onFinished: () {
              //               AppShieldingTemp.isShowStatusbar = false;
              //             },
              //           ),
              //         ))
              //     : Container(),
              Expanded(
                  child: TabBarView(
                physics: const NeverScrollableScrollPhysics(),
                controller: controller.tabController,
                children: const [
                  // TabQRScreen(),
                  TabHomeScreen(),
                  TabTransactionScreen(),
                  TabNotificationScreen(),
                  TabSettingScreen()
                ],
              ))
            ],
          )),
      bottomNavigationBar: StyleProvider(
          style: Style(),
          child: ConvexAppBar.badge(
              {
                2: Container(
                  padding: EdgeInsets.zero,
                  width: 20,
                  alignment: Alignment.center,
                  decoration: const BoxDecoration(
                      shape: BoxShape.circle, color: AppColors.primaryDark),
                  child: const Text(
                    '3',
                    style: TextStyle(
                      fontSize: 11,
                      color: AppColors.white,
                    ),
                  ),
                ),
              },
              badgeMargin: const EdgeInsets.only(left: 20, top: 0, bottom: 20),
              backgroundColor: Colors.white,
              color: AppColors.textPrimary,
              items: [
                TabItem(
                  icon: Icon(
                    MerchantIcon.home,
                    color: AppColors.textPrimary,
                    size: iconSize + 2,
                  ),
                  activeIcon: Icon(
                    MerchantIcon.home,
                    color: AppColors.primary,
                    size: iconSize + 2,
                  ),
                ),
                TabItem(
                  icon: Icon(
                    MerchantIcon.transaction,
                    color: AppColors.textPrimary,
                    size: iconSize + 2,
                  ),
                  activeIcon: Icon(
                    MerchantIcon.transaction,
                    color: AppColors.primary,
                    size: iconSize + 2,
                  ),
                ),
                TabItem(
                  icon: Icon(
                    MerchantIcon.notification,
                    color: AppColors.textPrimary,
                    size: iconSize + 2,
                  ),
                  activeIcon: Icon(
                    MerchantIcon.notification,
                    color: AppColors.primary,
                    size: iconSize + 2,
                  ),
                ),
                TabItem(
                  icon: Icon(
                    MerchantIcon.menu,
                    color: AppColors.textPrimary,
                    size: iconSize,
                  ),
                  activeIcon: Icon(
                    MerchantIcon.menu,
                    color: AppColors.primary,
                    size: iconSize,
                  ),
                ),
              ],
              activeColor: AppColors.primary,
              style: TabStyle.react,
              height: 50,
              top: 0,
              elevation: GetPlatform.isAndroid ? 0.5 : 1,
              shadowColor: Colors.black38,
              controller: controller.tabController,
              initialActiveIndex:
                  controller.tabController.index, //optional, default as 0
              onTap: (tab) {
            if (controller.tabHomeCon.isOnline == false) {
              if (controller.tabController.index != 0) {
                controller.tabController.animateTo(0);
                controller.tabController.index = 0;
              }
              controller.tabHomeCon.onReady();
            } else {
              controller.tabController.index = tab;
            }
          })),
    );
  }
}
