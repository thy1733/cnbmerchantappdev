import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:cnbmerchantapp/core.dart';

class TabShareInviteScreen extends StatelessWidget {
   TabShareInviteScreen({super.key});

  final InviteFriendScreenController _controller =
      Get.put(InviteFriendScreenController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.white,
      body: _body(),
    );
  }

  Widget _body() {
    final double width = Get.width;

    return Column(
      children: [
        Expanded(
          flex: 1,
          child: SizedBox(
            height: width,
            width: width,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const SizedBox(
                  height: 50,
                ),
                SvgPicture.asset(SvgAppAssets.friendInvitation),
                const SizedBox(
                  height: 20,
                ),
                Text(
                  "InviteDes".tr,
                  textAlign: TextAlign.center,
                  style: AppTextStyle.label2,
                ),
              ],
            ),
          ),
        ),
        AppButtonStyle.primaryButton(
          label: "InviteFriend".tr,
          onPressed: () async {
            _controller.inviteFriendAsync();
          },
          margin: const EdgeInsets.fromLTRB(15, 0, 15, 15),
        )
      ],
    );
  }
}
