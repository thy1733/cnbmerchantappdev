import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:cnbmerchantapp/core.dart';

class InviteFriendScreen extends StatefulWidget {
  const InviteFriendScreen({Key? key}) : super(key: key);

  @override
  State<InviteFriendScreen> createState() => _InviteFriendScreenState();
}

class _InviteFriendScreenState extends State<InviteFriendScreen>
    with TickerProviderStateMixin {
  // TabController? _tabController;

  @override
  void initState() {
    super.initState();
    // _tabController = TabController(length: 2, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return ContainerAnimatedTemplate(
      title: "InviteFriend".tr,
      child: _body(),
    );
  }

  Widget _body() {
    // List<Tab> tabs = [
    //   Tab(
    //     text: "ShareInvitation".tr,
    //   ),
    //   Tab(
    //     text: "InviteList".tr,
    //   )
    // ];
    // List<Widget> children = [
    //   TabShareInviteScreen(),
    //   TabFriendListScreen(),
    // ];

    return Scaffold(
      backgroundColor: AppColors.white,
      body: TabShareInviteScreen(),
      // appBar: PreferredSize(
      //     preferredSize: const Size.fromHeight(kToolbarHeight),
      //     child: TabBar(
      //       labelColor: AppColors.primary,
      //       unselectedLabelColor: AppColors.grey,
      //       labelStyle: const TextStyle(fontWeight: FontWeight.bold),
      //       unselectedLabelStyle: const TextStyle(fontWeight: FontWeight.bold),
      //       indicatorColor: AppColors.primary,
      //       controller: _tabController,
      //       splashBorderRadius: BorderRadius.circular(6),
      //       tabs: tabs,
      //     )),
      // body: TabBarView(controller: _tabController, children: children),
    );
  }
}
