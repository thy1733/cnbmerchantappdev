import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

class FriendItem extends StatelessWidget {
  const FriendItem({
    Key? key,
    required this.model,
  }) : super(key: key);

  final FriendModel model;

  @override
  Widget build(BuildContext context) {
    const double padding = 16;

    return Row(
      children: [
        CustomImage(
          model.photoUrl,
          isNetwork: true,
          isShadow: true,
          width: 44.w,
          height: 44.w,
          radius: 100,
        ),
        const SizedBox(
          width: padding,
        ),
        Expanded(
            flex: 1,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "${model.firstName} ${model.lastName}",
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  style:
                      AppTextStyle.body2.copyWith(color: AppColors.textPrimary),
                ),
                Text(
                  model.account,
                  overflow: TextOverflow.ellipsis,
                  style: AppTextStyle.caption
                      .copyWith(color: AppColors.textSecondary),
                ),
              ],
            )),
        const SizedBox(
          width: padding,
        ),
        Container(
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(100),
              color: model.status == FriendStatus.active
                  ? AppColors.success
                  : AppColors.grey),
          child: Text(
            model.status.displayTitle.tr,
            style: const TextStyle(
              color: Colors.white,
              fontSize: 11,
              height: 1,
              fontWeight: FontWeight.w400,
            ),
          ),
        )
      ],
    );
  }
}
