import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:cnbmerchantapp/core.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class TabFriendListScreen extends StatelessWidget {
  TabFriendListScreen({super.key});
  final InviteFriendScreenController _controller =
      Get.put(InviteFriendScreenController());

  @override
  Widget build(BuildContext context) {
    RefreshController refreshController =
        RefreshController(initialRefresh: false);
    return SmartRefresher(
      header: AppSmartRefresh.shared.header,
      footer: AppSmartRefresh.shared.footer,
      controller: refreshController,
      onLoading: () async {
        await Future.delayed(const Duration(seconds: 2));
        refreshController.loadComplete();
      },
      onRefresh: () async {
        refreshController.refreshCompleted();
      },
      enablePullDown: true,
      enablePullUp: true,
      physics: const ClampingScrollPhysics(),
      child: Scaffold(
        backgroundColor: AppColors.white,
        body: Column(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 16),
              child: _buildSearchBar(
                  onTap: () async {
                    await showFullScreenSearch(
                      context: context,
                      delegate: TabFriendListScreenDelegate(),
                    );
                  },
                  boxShadow: [
                    const BoxShadow(blurRadius: 3, color: AppColors.grey),
                  ]),
            ),
            Expanded(
              flex: 1,
              child: _controller.friends.isEmpty
                  ? _emptyBody()
                  : _buildInvitedFriendList(_controller.friends),
            ),
            Visibility(
              visible: _controller.friends.isEmpty,
              child: AppButtonStyle.primaryButton(
                  label: "InviteFriend".tr,
                  onPressed: () async {
                    _controller.inviteFriendAsync();
                  },
                  margin: const EdgeInsets.fromLTRB(15, 0, 15, 15)),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildInvitedFriendList(List<FriendModel> friends) {
    return ListView.separated(
        padding: EdgeInsets.zero,
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        itemBuilder: (context, index) {
          FriendModel friend = friends[index];
          return InkWell(
              // We don't have friend detail UI in Figma yet
              // onTap: () => _controller.navigationToNamedArgumentsAsync(Routes.friendDetail),
              child: FriendItem(model: friend));
        },
        separatorBuilder: (context, index) => const Divider(),
        itemCount: friends.length);
  }

  Widget _emptyBody() {
    final double width = Get.width;

    return SizedBox(
      height: width,
      width: width,
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const SizedBox(
              height: 20,
            ),
            Image.asset(
              PngAppAssets.icEmptyBusiness,
              width: width / 3,
              height: width / 3,
            ),
            const SizedBox(
              height: 16,
            ),
            Text(
              "NoReferral".tr,
              style: AppTextStyle.label1,
            ),
            Text(
              "NoReferralDes".tr,
              style: AppTextStyle.label2,
            )
          ],
        ),
      ),
    );
  }

  Widget _buildSearchBar(
      {BoxBorder? border,
      GestureTapCallback? onTap,
      Color? color,
      required List<BoxShadow> boxShadow}) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        height: 40,
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
        decoration: BoxDecoration(
          border: border,
          color: color ?? AppColors.grey.withOpacity(0.09),
          borderRadius: const BorderRadius.all(Radius.circular(30)),
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Icon(
              Icons.search,
              size: 24.w,
              color: AppColors.grey,
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(left: 10, bottom: 2),
                child: Text(
                  "Search".tr,
                  style: const TextStyle(fontSize: 14, color: AppColors.grey),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
