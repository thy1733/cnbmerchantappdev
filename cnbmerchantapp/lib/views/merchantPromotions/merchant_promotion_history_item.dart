import 'package:cnbmerchantapp/core.dart';
import 'package:cnbmerchantapp/models/merchantPromotions/create_merchant_promotion_output_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MerchantPromotionHistoryItem extends StatelessWidget {
  const MerchantPromotionHistoryItem({Key? key, required this.item})
      : super(key: key);

  final CreateMerchantPromotionOutputModel item;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                item.repeatOption.tr,
                style: AppTextStyle.body2.copyWith(
                    color: AppColors.textPrimary, fontWeight: FontWeight.w800),
              ),
              Text(
                "${kDateTimeFormat(item.date.fromDate, showStandardDate: true)} - ${kDateTimeFormat(item.date.toDate, showStandardDate: true)}",
                style: AppTextStyle.caption
                    .copyWith(color: AppColors.textSecondary),
              ),
            ],
          ),
        ),
        Container(
            margin: const EdgeInsets.symmetric(vertical: 12),
            decoration: BoxDecoration(
                color: AppColors.grey,
                borderRadius: BorderRadius.circular(100)),
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
              child: Text(
                "Submitted".tr,
                style: const TextStyle(
                  color: Colors.white,
                  fontSize: 11,
                  height: 1,
                  fontWeight: FontWeight.w400,
                ),
              ),
            ))
      ],
    );
  }
}
