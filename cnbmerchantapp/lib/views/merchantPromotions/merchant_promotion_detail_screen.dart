import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:cnbmerchantapp/core.dart';

class MerchantPromotionDetailScreen extends StatelessWidget {
  MerchantPromotionDetailScreen({Key? key}) : super(key: key);

  final MerchantPromotionDetailScreenController _controller =
      Get.put(MerchantPromotionDetailScreenController());

  @override
  Widget build(BuildContext context) {
    return ContainerAnimatedTemplate(
      title: "MerchantPromotion".tr,
      padding: EdgeInsets.zero,
      floatingActionButton: AppButtonStyle.primaryOutlinedButton(
        margin: const EdgeInsets.symmetric(horizontal: 15),
        label: "Cancel".tr,
        onPressed: _controller.cancelPromotionRequest,
      ),
      child: _body(),
    );
  }

  Widget _body() {
    return SingleChildScrollView(
      padding: const EdgeInsets.fromLTRB(15, 15, 15, 0).r,
      physics: const ClampingScrollPhysics(),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          SvgPicture.asset(SvgAppAssets.merchantProgram),
          const SizedBox(
            height: 20,
          ),
          Text(
            "AgreementStatus".tr,
            textAlign: TextAlign.center,
            style: AppTextStyle.body2.copyWith(
                color: AppColors.textPrimary, fontWeight: FontWeight.w600),
          ),
          const SizedBox(height: 20),
          Text(
            "PromotionPeriod".tr,
            style: AppTextStyle.body2.copyWith(
                color: AppColors.textPrimary, fontWeight: FontWeight.w700),
          ),
          const SizedBox(height: 20),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                  child: _buildSection(
                      title: "StartDate",
                      subtitle: kDateTimeFormat(
                          _controller.promotionItem.date.fromDate,
                          showStandardDate: true))),
              Expanded(
                  child: _buildSection(
                      title: "EndDate",
                      subtitle: kDateTimeFormat(
                          _controller.promotionItem.date.toDate,
                          showStandardDate: true))),
            ],
          ),
          const SizedBox(height: 20),
          Text(
            "DiscountOption".tr,
            style: AppTextStyle.body2.copyWith(
                color: AppColors.textPrimary, fontWeight: FontWeight.w700),
          ),
          const SizedBox(height: 20),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                  child: _buildSection(
                      title: "Rate", subtitle: _controller.promotionItem.rate)),
              Expanded(
                  child: _buildSection(
                      title: "CoverBy",
                      subtitle: _controller.promotionItem.coveredBy ==
                              MerchantPromotionCoveredBy.both
                          ? "${"Bank".tr.toUpperCase()} - ${_controller.promotionItem.coveredByBankValue}\n${"Merchant".tr.toUpperCase()} - ${_controller.promotionItem.coveredByMerchantValue}"
                          : _controller.promotionItem.coveredBy!.displayTitle.tr
                              .toUpperCase())),
            ],
          ),
          const SizedBox(height: 20),
          _buildSection(
              title: "Repeat",
              subtitle: _controller.promotionItem.repeatOption),
          const SizedBox(
            height: 20,
          ),
        ],
      ),
    );
  }

  Widget _buildSection({required String title, required String subtitle}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        Text(
          title.tr,
          style: AppTextStyle.caption.copyWith(
            color: AppColors.textPrimary,
          ),
        ),
        Text(
          subtitle.tr,
          style: AppTextStyle.caption.copyWith(
              color: AppColors.textPrimary, fontWeight: FontWeight.w700),
        ),
      ],
    );
  }
}
