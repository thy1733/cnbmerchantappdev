import 'package:cnbmerchantapp/models/merchantPromotions/create_merchant_promotion_output_model.dart';
import 'package:flutter/material.dart';
import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class TabMerchantPromotionHistoryScreen extends StatelessWidget {
  TabMerchantPromotionHistoryScreen({
    Key? key,
  }) : super(key: key);

  final MerchantPromotionScreenController _controller =
      Get.find<MerchantPromotionScreenController>();

  @override
  Widget build(BuildContext context) {
    RefreshController refreshController =
        RefreshController(initialRefresh: false);
    return SmartRefresher(
      header: AppSmartRefresh.shared.header,
      footer: AppSmartRefresh.shared.footer,
      controller: refreshController,
      onLoading: () async {
        await Future.delayed(const Duration(seconds: 2));
        refreshController.loadComplete();
      },
      onRefresh: () async {
        refreshController.refreshCompleted();
      },
      enablePullDown: true,
      enablePullUp: true,
      physics: const ClampingScrollPhysics(),
      child: _buildMerchantPromotionList(),
    );
  }

  Widget _buildMerchantPromotionList() {
    final historyItems = CreateMerchantPromotionOutputModel.sampleData();
    return ListView.separated(
      padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
      physics: const NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      itemBuilder: (BuildContext context, int index) {
        final historyItem = historyItems[index];
        return InkWell(
          onTap: () async {
            _controller.toMerchantPromotionDetailScreen(historyItem);
          },
          child: MerchantPromotionHistoryItem(item: historyItem),
        );
      },
      separatorBuilder: (BuildContext context, int index) {
        return const Divider();
      },
      itemCount: historyItems.length,
    );
  }
}
