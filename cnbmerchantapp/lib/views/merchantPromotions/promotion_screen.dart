import 'package:flutter/material.dart';
import 'package:cnbmerchantapp/core.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

class PromotionScreen extends StatelessWidget {
  PromotionScreen({
    Key? key,
    required this.promotion,
  }) : super(key: key);

  final MerchantPromotionModel promotion;
  final double imageHeight = (Get.width) / 1.7 - 40;
  final MerchantPromotionScreenController _controller =
      Get.find<MerchantPromotionScreenController>();

  @override
  Widget build(BuildContext context) {
    return Container(
      width: Get.width,
      margin: const EdgeInsets.symmetric(vertical: 8),
      decoration: BoxDecoration(
        color: AppColors.white,
        borderRadius: BorderRadius.circular(10),
        boxShadow: [
          BoxShadow(
            color: Colors.black87.withOpacity(0.05),
            spreadRadius: 1,
            blurRadius: 1,
            offset: const Offset(0, 0.5), // changes position of shadow
          ),
        ],
      ),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisSize: MainAxisSize.min,
          children: [
            Stack(
              alignment: Alignment.center,
              children: [
                CustomImage(
                  promotion.imageUrl,
                  isNetwork: true,
                  isShadow: false,
                  height: imageHeight,
                  width: Get.width,
                  radius: 0,
                  placeholderShape: BoxShape.rectangle,
                ),
                Container(
                  height: imageHeight,
                  width: Get.width,
                  color: Colors.black.withOpacity(0.1),
                ),
                InkWell(
                  onTap: _controller.shareLink,
                  child: Container(
                    padding: const EdgeInsets.all(8),
                    decoration: BoxDecoration(
                        color: AppColors.primary.withOpacity(0.5),
                        borderRadius: BorderRadius.circular(5)),
                    child: Column(
                      children: [
                        const Icon(
                          Icons.ios_share,
                          size: 24,
                          color: Colors.white,
                        ),
                        SizedBox(height: 4.r),
                        Text(
                          "Share".tr,
                          style: AppTextStyle.body2
                              .copyWith(color: AppColors.white),
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    promotion.title,
                    style: AppTextStyle.body2.copyWith(
                        color: AppColors.textPrimary,
                        fontWeight: FontWeight.w700),
                  ),
                  const SizedBox(
                    height: 4,
                  ),
                  Text(
                    promotion.description,
                    style: AppTextStyle.caption
                        .copyWith(color: AppColors.textSecondary),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
