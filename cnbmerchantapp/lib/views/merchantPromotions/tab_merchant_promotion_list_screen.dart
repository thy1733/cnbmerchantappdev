import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:cnbmerchantapp/core.dart';

class TabMerchantPromotionListScreen extends StatelessWidget {
  final MerchantPromotionScreenController _controller =
      Get.put(MerchantPromotionScreenController());

  TabMerchantPromotionListScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(() => ContainerAnimatedTemplate(
          needBottomSpace: false,
          padding: EdgeInsets.zero,
          title: "MerchantPromotion".tr,
          child: _body(),
        ));
  }

  Widget _body() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        _promotionStatus(),
        _controller.isAll ? _listPromotion() : _listPromotionHistory(),
      ],
    );
  }

  Widget _promotionStatus() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        TextButton(
          onPressed: () {
            _controller.changeOptionStatus(0);
          },
          style: ButtonStyle(
            padding: MaterialStateProperty.all(
              const EdgeInsets.symmetric(horizontal: 20, vertical: 2),
            ),
          ),
          child: Text(
            "All".tr.toUpperCase(),
            textAlign: TextAlign.center,
            style: TextStyle(
                color: _controller.isAll ? AppColors.primary : AppColors.grey,
                fontSize: 13,
                fontWeight: FontWeight.w400),
          ),
        ),
        TextButton(
          onPressed: () {
            _controller.changeOptionStatus(1);
          },
          style: ButtonStyle(
              padding: MaterialStateProperty.all(
                  const EdgeInsets.symmetric(horizontal: 20, vertical: 2))),
          child: Text(
            "History".tr.toUpperCase(),
            textAlign: TextAlign.center,
            style: TextStyle(
              color: _controller.isHistory ? AppColors.primary : AppColors.grey,
              fontSize: 13,
              fontWeight: FontWeight.w400,
            ),
          ),
        )
      ],
    );
  }

  Widget _listPromotion() {
    final double size = Get.width;
    return _controller.merchantPromotion.isEmpty
        ? _emptyBody()
        : Expanded(
            child: SizedBox(
              height: size,
              width: size,
              child: ListView.separated(
                padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                shrinkWrap: true,
                itemCount: _controller.merchantPromotion.length,
                physics: const NeverScrollableScrollPhysics(),
                itemBuilder: (BuildContext context, int index) {
                  var item = _controller.merchantPromotion[index];
                  return PromotionScreen(promotion: item);
                },
                separatorBuilder: (BuildContext context, int index) =>
                    Container(
                  height: 1,
                  color: const Color.fromRGBO(245, 245, 245, 1),
                ),
              ),
            ),
          );
  }

  Widget _listPromotionHistory() {
    return Expanded(
      child: TabMerchantPromotionHistoryScreen(),
    );
  }

  Widget _emptyBody() {
    final double size = Get.width;

    return SizedBox(
      height: size,
      width: size,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Image.asset(
            PngAppAssets.icEmptyBusiness,
            width: size / 3,
            height: size / 3,
          ),
          const SizedBox(
            height: 16,
          ),
          Text(
            "NoPromotionYet".tr,
            style: AppTextStyle.label2,
          ),
          const SizedBox(
            height: 30,
          ),
          AppButtonStyle.primaryButton(
              margin: const EdgeInsets.symmetric(horizontal: 15),
              label: "CreateMerchantPromotion".tr,
              onPressed: () async {
                _controller.toCreateMerchantPromotionScreen();
              })
        ],
      ),
    );
  }
}
