import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:cnbmerchantapp/core.dart';

class CreateMerchantPromotionScreen extends StatefulWidget {
  const CreateMerchantPromotionScreen({Key? key}) : super(key: key);

  @override
  State<CreateMerchantPromotionScreen> createState() =>
      _CreateMerchantPromotionScreenState();
}

class _CreateMerchantPromotionScreenState
    extends State<CreateMerchantPromotionScreen> {
  final CreateMerchantPromotionScreenController _controller =
      Get.put(CreateMerchantPromotionScreenController());

  final DateFormat formatter = DateFormat.yMMMd(Get.locale?.languageCode);

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Obx(() => ContainerAnimatedTemplate(
          padding: EdgeInsets.zero,
          title: "MerchantPromotion".tr,
          floatingActionButton: Container(
            color: AppColors.white,
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 8, bottom: 12),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Checkbox(
                        value: _controller.createPromotion.isAgreed,
                        activeColor: AppColors.primary,
                        checkColor: AppColors.white,
                        shape: const RoundedRectangleBorder(
                          side:
                              BorderSide(width: 0.5, color: AppColors.darkGrey),
                          borderRadius: BorderRadius.all(
                            Radius.circular(2),
                          ),
                        ),
                        onChanged: (_) =>
                            _controller.onTermConditionAgreementChanged(),
                      ),
                      Expanded(
                          child: Text.rich(
                        TextSpan(
                          children: [
                            TextSpan(text: "MerchantAgree".tr),
                            TextSpan(
                                text: "MerchantPartnership".tr,
                                style: const TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: AppColors.primary),
                                recognizer: TapGestureRecognizer()
                                  ..onTap = () {
                                    _controller.navigationToNamedArgumentsAsync(
                                        Routes.partnershipAgreementScreen);
                                  }),
                          ],
                        ),
                      ))
                    ],
                  ),
                ),
                AppButtonStyle.primaryButton(
                    label: "Submit".tr,
                    isEnable: _controller.createPromotion.isValid(),
                    margin: const EdgeInsets.symmetric(horizontal: 15).r,
                    onPressed: () {
                      _controller.onSubmit();
                    }),
              ],
            ),
          ),
          child: _body(context),
        ));
  }

  Widget _body(BuildContext context) {
    return SingleChildScrollView(
      controller: _controller.scrollController,
      padding: const EdgeInsets.fromLTRB(15, 15, 15, 0).r,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SvgPicture.asset(SvgAppAssets.checkList),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 20),
                child: Text(
                  "CorporatePartner".tr,
                  textAlign: TextAlign.center,
                  style: AppTextStyle.body1.copyWith(
                      color: AppColors.textPrimary,
                      fontWeight: FontWeight.w700),
                ),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "PromotionPeriod".tr,
                    style: AppTextStyle.body2.copyWith(
                        color: AppColors.textPrimary,
                        fontWeight: FontWeight.w700),
                  ),
                  _buildStartDate(),
                  _buildEndDate(),
                  _buildRepeat(),
                ],
              ),
              const SizedBox(
                height: 8,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "DiscountOption".tr,
                    style: AppTextStyle.body2.copyWith(
                        color: AppColors.textPrimary,
                        fontWeight: FontWeight.w700),
                  ),
                  _buildDiscountOption(context),
                  Visibility(
                    visible:
                        _controller.createPromotion.isRatePercentageSelected,
                    child: _buildRatePercentage(),
                  ),
                  Visibility(
                      visible:
                          _controller.createPromotion.isFixedAmountSelected,
                      child: _buildAmountDiscountOption()),
                ],
              ),
              Visibility(
                visible: _controller.createPromotion.isRatePercentageSelected ||
                    _controller.createPromotion.isFixedAmountSelected,
                child: Padding(
                  padding: const EdgeInsets.only(top: 8, left: 16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "CoverBy".tr,
                        style: AppTextStyle.caption.copyWith(
                          color: AppColors.textSecondary,
                        ),
                      ),
                      _buildCoveredBy(),
                      _buildCoveredByBoth(),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 20.r,
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildDiscountOption(BuildContext context) {
    return ContainerButton(
      submit: (value) {
        _controller.onBottomSheetTapped(
            context, MerchantPromotionAction.discount);
      },
      child: DropdownOption(
        title: "DiscountOption".tr,
        value: "",
        leadingIconData: MerchantIcon.rate,
        child: Text(
          _controller.selectedDiscountOptionForDisplay,
          style: _controller.selectedDiscountOptionTextStyle,
        ),
      ),
    );
  }

  Widget _buildRepeat() {
    return ContainerButton(
      submit: (value) {
        _controller.onBottomSheetTapped(
            context, MerchantPromotionAction.repeat);
      },
      child: DropdownOption(
          title: "Repeat".tr,
          value: "",
          leadingIconData: MerchantIcon.repeat,
          child: Text(
            _controller.selectedRepeatOptionForDisplay,
            style: _controller.selectedRepeatOptionTextStyle,
          )),
    );
  }

  Widget _buildEndDate() {
    return ContainerButton(
      submit: (value) {
        _controller.onBottomSheetTapped(
            context, MerchantPromotionAction.endDate);
      },
      child: DropdownOption(
          title: "EndDate".tr,
          value: formatter.format(
            _controller.createPromotion.date.toDate,
          ),
          leadingIconData: MerchantIcon.calendar),
    );
  }

  Widget _buildStartDate() {
    return ContainerButton(
      submit: (value) {
        _controller.onBottomSheetTapped(
            context, MerchantPromotionAction.startDate);
      },
      child: DropdownOption(
          title: "StartDate".tr,
          value: formatter.format(
            _controller.createPromotion.date.fromDate,
          ),
          leadingIconData: MerchantIcon.calendar),
    );
  }

  Widget _buildRatePercentage() {
    return ContainerButton(
      submit: (value) {
        _controller.onBottomSheetTapped(context, MerchantPromotionAction.rate);
      },
      child: DropdownOption(
        title: "Rate".tr,
        value: _controller.selectedRateOptionForDisplay,
        leadingIconData: MerchantIcon.rate1,
      ),
    );
  }

  Widget _buildAmountDiscountOption() {
    return GestureDetector(
      onTap: () => {},
      child: KTextField(
        prefixText: "USD",
        name: "EnterAmount".tr,
        iconData: MerchantIcon.amount,
        controller: _controller.fixedAmountTextController,
        keyboardType:
            const TextInputType.numberWithOptions(decimal: true, signed: false),
        initialValue: _controller.createPromotion.fixedAmountValue.toString(),
        validatorMessage:
            _controller.createPromotion.fixedAmountValidatorMessage,
        onChanged: (value) {
          _controller.onFixedAmountEditingChanged(value);
        },
      ),
    );
  }

  Widget _buildCoveredBy() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        KRadioButtonWidget(
          radioValue: MerchantPromotionCoveredBy.bank,
          radioGroupValue: _controller.createPromotion.coveredBy,
          onChange: (value) =>
              _controller.onCoveredBySelectedRadioButtonChanged(
                  MerchantPromotionCoveredBy.bank),
          value: MerchantPromotionCoveredBy.bank.displayTitle.tr.toUpperCase(),
        ),
        Expanded(child: Container()),
        KRadioButtonWidget(
          radioValue: MerchantPromotionCoveredBy.merchant,
          radioGroupValue: _controller.createPromotion.coveredBy,
          onChange: (value) => {
            _controller.onCoveredBySelectedRadioButtonChanged(
                MerchantPromotionCoveredBy.merchant)
          },
          value:
              MerchantPromotionCoveredBy.merchant.displayTitle.tr.toUpperCase(),
        ),
        Expanded(child: Container()),
        KRadioButtonWidget(
          radioValue: MerchantPromotionCoveredBy.both,
          radioGroupValue: _controller.createPromotion.coveredBy,
          onChange: (value) {
            _controller.onCoveredBySelectedRadioButtonChanged(
                MerchantPromotionCoveredBy.both);
          },
          value: MerchantPromotionCoveredBy.both.displayTitle.tr.toUpperCase(),
        ),
        Expanded(child: Container()),
      ],
    );
  }

  Widget _buildCoveredByBoth() {
    return Visibility(
      visible: _controller.createPromotion.coveredBy ==
          MerchantPromotionCoveredBy.both,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: KTextField(
              inputFormatters: [
                TextInputFormatter.withFunction((oldValue, newValue) {
                  return _controller.createPromotion.isRatePercentageSelected
                      ? maxIntegerValueTextInputFormatter(
                          oldValue,
                          newValue,
                          _controller.ratePercentageList[
                                  _controller.createPromotion.rateOptionIndex]
                              .toInt())
                      : maxValueTextInputFormatter(
                          oldValue,
                          newValue,
                          _controller.createPromotion.fixedAmountValue
                              .toInt()
                              .toDouble());
                })
              ],
              prefixText: _controller.createPromotion.isFixedAmountSelected
                  ? "\$"
                  : "%",
              name: "Bank".tr,
              iconData: MerchantIcon.bank,
              keyboardType: _controller.createPromotion.isRatePercentageSelected
                  ? TextInputType.number
                  : const TextInputType.numberWithOptions(
                      decimal: true, signed: false),
              validatorMessage:
                  _controller.createPromotion.converedByBothValidatorMessage,
              controller: _controller.coveredByBankTextController,
              onTap: _controller.onCoveredByBankFieldFocused,
              onChanged: (value) => _controller.onCoveredByBankChanged(value),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
            child: InkWell(
              onTap: _controller.isSwapButtonEnabled
                  ? _controller.onSwapTapped
                  : null,
              child: Icon(
                MerchantIcon.flip,
                color: _controller.isSwapButtonEnabled
                    ? AppColors.primary
                    : AppColors.greyishDisable,
              ),
            ),
          ),
          Expanded(
            child: KTextField(
              // backgroundColor: AppColors.transparent,
              enabled: false,
              prefixText: _controller.createPromotion.isFixedAmountSelected
                  ? "\$"
                  : "%",
              name: "Merchant".tr,
              iconData: MerchantIcon.shop,
              iconColor: AppColors.grey,
              textColor: AppColors.grey,
              keyboardType: _controller.createPromotion.isRatePercentageSelected
                  ? TextInputType.number
                  : const TextInputType.numberWithOptions(
                      decimal: true, signed: false),
              validatorMessage:
                  _controller.createPromotion.converedByBothValidatorMessage,
              controller: _controller.coveredByMerchantTextController,
              onTap: _controller.onCoveredByMerchantFieldFocused,
              onChanged: (value) =>
                  _controller.onCoveredByMerchantChanged(value),
            ),
          ),
        ],
      ),
    );
  }
}
