import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class BusinessListScreen extends StatefulWidget {
  const BusinessListScreen({super.key});

  @override
  State<BusinessListScreen> createState() => _BusinessListScreenState();
}

class _BusinessListScreenState extends State<BusinessListScreen> {
  final BusinessListScreenController controller =
      Get.put(BusinessListScreenController());
  final RefreshController refreshController =
      RefreshController(initialRefresh: false);

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => ContainerTemp(
        header: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Expanded(
              flex: 1,
              child: NavigationTitle(
                title: 'ManageBusinesses'.tr,
                backButton: () => Get.back(),
              ),
            ),
            IconButton(
                padding: const EdgeInsets.only(top: 16, right: 16),
                onPressed: () {
                  controller.toSetupBusinessAsync();
                },
                icon: ClipRRect(
                    borderRadius: BorderRadius.circular(20),
                    child: Container(
                        color: Colors.white,
                        child: const Icon(
                          Icons.add_outlined,
                          color: AppColors.primary,
                        )))),
          ],
        ),
        child: Padding(
          padding: const EdgeInsets.all(15),
          child: SmartRefresher(
            controller: refreshController,
            header: AppSmartRefresh.shared.header,
            footer: AppSmartRefresh.shared.footer,
            onLoading: () {
              refreshController.loadComplete();
            },
            onRefresh: () {
              controller.retrieveListBusinesses();
              refreshController.refreshCompleted();
            },
            enablePullDown: true,
            enablePullUp: true,
            physics: const ClampingScrollPhysics(),
            child: SingleChildScrollView(
              physics: const NeverScrollableScrollPhysics(),
              child: controller.isEmptyData
                  ? EmptyListBuilder(
                      emptyImageAsset: PngAppAssets.icEmptyBusiness,
                      message: "NoBusinessMessage".tr,
                      description: "NoBusinessDescription".tr,
                    )
                  : listBuilder(),
            ),
          ),
        ),
      ),
    );
  }

  Widget listBuilder() {
    return ListView.separated(
      padding: EdgeInsets.zero,
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      itemCount: controller.businesses.length,
      separatorBuilder: (BuildContext context, int index) => const Divider(
        color: AppColors.greyishDisable,
      ),
      itemBuilder: (BuildContext context, int index) {
        return listItem(controller.businesses[index]);
      },
    );
  }

  Widget listItem(ManageBusinessItemListOutputModel item) {
    final double imageSize = 44.w;
    final double padding = 16.w;

    return InkWell(
      onTap: () {
        refreshController.refreshCompleted();
        controller.toBusinessDetailAsync(item);
      },
      child: Padding(
        padding: EdgeInsets.all(2.w),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: [
            // IMAGE
            Container(
              width: imageSize,
              height: imageSize,
              padding: const EdgeInsets.all(1),
              decoration: const BoxDecoration(
                  shape: BoxShape.circle, color: AppColors.grey),
              child: ClipOval(
                  clipBehavior: Clip.hardEdge,
                  child: AppNetworkImage(
                    item.businessLogo,
                    cacheKey: item.cacheImageKey,
                    errorWidget: SvgPicture.asset(SvgAppAssets.pBusinessList),
                  )),
            ),
            SizedBox(width: padding),

            // TEXT
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    item.businessName,
                    style: AppTextStyle.subtitle2,
                  ),
                ],
              ),
            ),

            const Icon(
              Icons.arrow_forward_ios,
              size: 16,
            )
          ],
        ),
      ),
    );
  }
}
