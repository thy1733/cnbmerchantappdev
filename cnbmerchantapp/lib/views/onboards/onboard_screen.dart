import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

class OnboardScreen extends StatefulWidget {
  const OnboardScreen({Key? key}) : super(key: key);
  @override
  State<OnboardScreen> createState() => _OnboardScreenState();
}

class _OnboardScreenState extends State<OnboardScreen> {
  final OnboardScreenController controller = Get.put(OnboardScreenController());

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Scaffold(
        backgroundColor: AppColors.white,
        body: SafeArea(
          child: Column(
            children: [
              Align(
                alignment: Alignment.topRight,
                child: _languageChangeButton(),
              ),
              SizedBox(
                height: Get.height * 0.60,
                child: PageView.builder(
                  itemCount: controller.data.length,
                  controller: controller.pageController,
                  onPageChanged: (index) {
                    controller.currentIndex = index;
                  },
                  itemBuilder: (BuildContext context, index) {
                    return _onboardView(data: controller.data.elementAt(index));
                  },
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: List.generate(
                  controller.data.length,
                  (i) => Padding(
                    padding: const EdgeInsets.fromLTRB(4, 5, 4, 16),
                    child: Container(
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: controller.currentIndex == i
                            ? AppColors.darkYellow
                            : AppColors.grey,
                      ),
                      width: 8,
                      height: 8,
                    ),
                  ),
                ),
              ),
              const Expanded(child: SizedBox()),
              AnimatedOpacity(
                opacity: controller.currentIndex != controller.data.length - 1
                    ? 1
                    : 0,
                duration: const Duration(milliseconds: 100),
                child: AppButtonStyle.secondaryButton(
                  onPressed: () => controller.skipAction(),
                  label: "Skip".tr,
                  margin: const EdgeInsets.symmetric(
                    horizontal: 15,
                    vertical: 5,
                  ),
                ),
              ),
              AppButtonStyle.primaryButton(
                margin: const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                label: controller.currentIndex != controller.data.length - 1
                    ? "Continue".tr
                    : "GetStarted".tr,
                onPressed: () {
                  controller.currentIndex++;
                  controller.nextPageAction();
                },
              ),
              const SizedBox(height: 5),
            ],
          ),
        ),
      ),
    );
  }

  Widget _onboardView({
    required OnboardModel data,
  }) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: SvgPicture.asset(
              data.imageUrl,
            ),
          ),
          const SizedBox(
            height: 30,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15),
            child: Text(
              data.title.tr,
              style: const TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              textAlign: TextAlign.center,
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          SizedBox(
            height: 70,
            child: Text(
              data.description.tr,
              textAlign: TextAlign.center,
              maxLines: 4,
              overflow: TextOverflow.ellipsis,
            ),
          ),
        ],
      ),
    );
  }

  Widget _languageChangeButton() {
    return InkWell(
      onTap: () async {
        await controller.updateLanguagesAsync();
      },
      splashColor: Colors.transparent,
      highlightColor: Colors.transparent,
      child: Container(
        padding: const EdgeInsets.all(5),
        margin: const EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: AppColors.white,
          borderRadius: BorderRadius.circular(30),
          border: Border.all(width: 1, color: AppColors.listSeparator),
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 22,
              width: 22,
              child: ClipOval(
                child: SvgPicture.asset(
                  controller.languageSelected.imageSvg,
                  fit: BoxFit.cover,
                ),
              ),
            ),
            const SizedBox(
              width: 5,
            ),
            Text(
              " ${controller.languageSelected.name} ",
              style: AppTextStyle.label1
                  .copyWith(color: AppColors.textPrimary, fontSize: 13),
            ),
            const SizedBox(
              width: 5,
            ),
          ],
        ),
      ),
    );
  }
}
