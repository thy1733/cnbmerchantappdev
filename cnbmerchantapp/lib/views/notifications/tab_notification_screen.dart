import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class TabNotificationScreen extends StatefulWidget {
  const TabNotificationScreen({Key? key}) : super(key: key);

  @override
  State<TabNotificationScreen> createState() => TabNotificationScreenState();
}

class TabNotificationScreenState extends State<TabNotificationScreen>
    with AutomaticKeepAliveClientMixin {
  final TabNotificationScreenController _controller =
      Get.find<TabNotificationScreenController>();
  final double imageHeight = (Get.width) / 1.7;
  // final Duration _animateDuration = const Duration(milliseconds: 300);
  List<RefreshController> refreshController = [
    RefreshController(initialRefresh: false),
    RefreshController(initialRefresh: false)
  ];

  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Obx(() => ContainerTemp(
        header: setupHeader(),
        child: setupPageView()));
  }

  Widget setupHeader() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 15, right: 15, top: 15),
          child: Text(
            "Notification".tr,
            style: AppTextStyle.header,
          ),
        ),
        Row(
          children: [
            Expanded(
              child: InkWell(
                onTap: () async {
                  await _controller.onSwape(0);
                },
                child: Padding(
                  padding: const EdgeInsets.all(15),
                  child: Text(
                    "Transactions".tr,
                    style: _controller.currentIndex == 0
                        ? AppTextStyle.selectedTab
                        : AppTextStyle.unSelectedTab,
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
            ),
            Expanded(
              child: InkWell(
                onTap: () async {
                  await _controller.onSwape(1);
                },
                child: Padding(
                  padding: const EdgeInsets.all(15),
                  child: Text(
                    "News".tr,
                    style: _controller.currentIndex == 1
                        ? AppTextStyle.selectedTab
                        : AppTextStyle.unSelectedTab,
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
            )
          ],
        ),
      ],
    );
  }

  Widget setupPageView() {
    return PageView.builder(
      itemCount: 2,
      controller: _controller.pageController,
      physics: const NeverScrollableScrollPhysics(),
      onPageChanged: (index) {
        _controller.currentIndex = index;
      },
      itemBuilder: (BuildContext context, index) {
        return index == 0 ? _transactionContainer() : _newsContiner();
      },
    );
  }
  //#region Container layout item

  Widget _newsContiner() {
    return Obx(() {
      if (!_controller.isOnline) {
        return ErrorNetworkWidget(
          onRetryPressed: _controller.retryAsync,
        );
      } else if (_controller.loadWebViewError) {
        return ErrorNetworkWidget(
          assetName: JsonAppAssets.underMaintenance,
          title: "",
          description: "SomethingWentWrongPleaseTryAgainLater",
          onRetryPressed: () {
            _controller.loadWebViewError = false;
          },
        );
      } else {
        return KWebViewWidget(
          url: ValueConst.notificationNewsURL.addMultipleLanguagesSupport(),
          onWebViewCreated: (con) {
            _controller.showLoading();
            _controller.newsWebViewController = con;
          },
          onError: () {
            _controller.dismiss();
            _controller.loadWebViewError = true;
          },
          onUrlTapped: (url) {
            return _controller.onTap(url);
          },
          onStop: () {
            _controller.dismiss();
          },
        );
      }
    });
  }

  // Widget _itemListContainer(TransactionItemModel item) {
  //   return InkWell(
  //       onTap: () {
  //         _controller.toNewsDetailAsync(item);
  //       },
  //       child: Container(
  //         width: Get.width,
  //         decoration: BoxDecoration(
  //           boxShadow: [
  //             BoxShadow(
  //                 blurRadius: 1.0,
  //                 spreadRadius: 1.0,
  //                 color: AppColors.black.withOpacity(0.04))
  //           ],
  //           color: AppColors.white,
  //           borderRadius: BorderRadius.circular(10),
  //         ),
  //         child: ClipRRect(
  //           borderRadius: BorderRadius.circular(10),
  //           child: Column(
  //             crossAxisAlignment: CrossAxisAlignment.stretch,
  //             mainAxisSize: MainAxisSize.min,
  //             children: [
  //               Obx(
  //                 () => AnimatedContainer(
  //                   duration: _animateDuration,
  //                   height: item.isShowLess ? 100 : imageHeight,
  //                   child: AppNetworkImage(
  //                     item.imageUrl,
  //                     height: imageHeight,
  //                     width: Get.width,
  //                     placeHolderShape: BoxShape.rectangle,
  //                     shape: BoxShape.rectangle,
  //                   ),
  //                 ),
  //               ),
  //               const SizedBox(height: 10),
  //               Padding(
  //                 padding: const EdgeInsets.symmetric(horizontal: 10),
  //                 child: Text(
  //                   item.title,
  //                   style: AppTextStyle.header4,
  //                   maxLines: 2,
  //                 ),
  //               ),
  //               Obx(() => AnimatedSize(
  //                     duration: _animateDuration,
  //                     child: Padding(
  //                         padding: const EdgeInsets.symmetric(horizontal: 10),
  //                         child: Text(
  //                           "${item.description} ${item.description} ${item.description}",
  //                           maxLines: item.isShowLess ? 2 : null,
  //                           style: AppTextStyle.lightLable,
  //                         )),
  //                   )),
  //               InkWell(
  //                 onTap: () {
  //                   item.isShowLess = !item.isShowLess;
  //                 },
  //                 child: Container(
  //                   padding: const EdgeInsets.symmetric(
  //                       horizontal: 10, vertical: 10),
  //                   child: Obx(() => Row(
  //                         mainAxisAlignment: MainAxisAlignment.spaceBetween,
  //                         crossAxisAlignment: CrossAxisAlignment.center,
  //                         children: [
  //                           Visibility(
  //                             visible:
  //                                 item.newLink.isNotEmpty && !item.isShowLess,
  //                             child: TextButton(
  //                                 onPressed: () {},
  //                                 child: Text("LearnMore".tr)),
  //                           ),
  //                           Icon(
  //                             item.isShowLess
  //                                 ? MerchantIcon.chevronDown
  //                                 : MerchantIcon.chevronUp,
  //                             size: 18,
  //                           )
  //                         ],
  //                       )),
  //                 ),
  //               ),
  //             ],
  //           ),
  //         ),
  //       ));
  // }
  //#endregion

  //#region Container transaction item

  Widget _transactionContainer() {
    return Obx(() => SmartRefresher(
        header: AppSmartRefresh.shared.header,
        footer: AppSmartRefresh.shared.footer,
        controller: _controller.refreshController,
        onLoading: () async {
          _controller.loadCompleteAync();
        },
        onRefresh: () async {
          _controller.refreshCompletedAync();
        },
        enablePullDown: true,
        enablePullUp: false,
        physics: const ClampingScrollPhysics(),
        child: _controller.isLoading
            ? const Center(
                child: KLoadingIndicator(),
              )
            : _controller.isNoData
                ? EmptyListBuilder(
                    emptyImageAsset: PngAppAssets.icEmptyBusiness,
                    message: "NoNotificationHistory".tr,
                    description:
                        "YouHaveNoNotificationHereDoAnyTransactionToSeeNotification"
                            .tr,
                  )
                : _groupedListBuilder()));
  }

  Widget _groupedListBuilder() {
    return GroupedListView<NotifyItemOutputModel, DateTime>(
      addAutomaticKeepAlives: true,
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      sort: true,
      elements: _controller.notifyItemList,
      groupBy: (value) {
        return value.txnDateTime.toLocalDateTime("yyyy-MM-dd");
      },
      order: GroupedListOrder.DESC,
      groupHeaderBuilder: (value) {
        return headerItem(value);
      },
      itemBuilder: (context, transaction) {
        return Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15),
            child: listItem(transaction));
      },
      separator: ContainerDevider.vertical(
          padding: const EdgeInsets.symmetric(horizontal: 15)),
    );
  }

  Widget headerItem(NotifyItemOutputModel item) {
    return Padding(
        padding: const EdgeInsets.only(left: 15, right: 15, top: 15),
        child: Text(item.txnDateTime.toTimeAgoDate(item.txnDateTime),
            style: AppTextStyle.subtitle2.copyWith(
              color: AppColors.textPrimary,
              fontWeight: FontWeight.w700,
            )));
  }

  Widget listItem(NotifyItemOutputModel item) {
    Color color =
        double.parse(item.amount) < 0 ? AppColors.primary : AppColors.success;
    return AnimatedContainer(
        curve: Curves.easeOutCirc,
        padding: const EdgeInsets.all(0.0),
        decoration: BoxDecoration(
            color: AppColors.transparent,
            borderRadius: BorderRadius.circular(0.0),
            border: Border.all(
                color: Colors.transparent,
                width: 0.0,
                style: BorderStyle.none)),
        duration: const Duration(seconds: 3),
        child: InkWell(
          onTap: () => _controller.toTxnDetailAsync(item),
          child: Padding(
              padding: const EdgeInsets.only(
                top: 10,
                bottom: 10,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    decoration: BoxDecoration(
                        color: color.withOpacity(0.1), shape: BoxShape.circle),
                    width: 44.w,
                    height: 44.w,
                    child: Icon(
                      MerchantIcon.transactionItem,
                      color: color,
                      size: 20,
                    ),
                  ),
                  SizedBox(
                    width: 10.w,
                  ),
                  Expanded(
                      child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Row(
                        children: [
                          Expanded(
                            child: RichText(
                              text: TextSpan(
                                text: double.parse(item.amount) > 0
                                    ? "${"YouReceived".tr} "
                                    : "${"YouRefunded".tr} ",
                                style: AppTextStyle.subtitle2.copyWith(
                                  color: AppColors.textPrimary,
                                  fontWeight: FontWeight.w400,
                                  overflow: TextOverflow.ellipsis,
                                ),
                                children: <TextSpan>[
                                  TextSpan(
                                      text:
                                          "${item.paidAmount.toThousandSeparatorString(d2: isUSDCurrency)} ${item.currencyCode.tr}",
                                      style: AppTextStyle.subtitle2.copyWith(
                                        color: AppColors.textPrimary,
                                        fontWeight: FontWeight.w600,
                                      )),
                                  TextSpan(
                                      text:
                                          " ${double.parse(item.amount) > 0 ? "FromSource".tr : "ToSource".tr} "),
                                  TextSpan(
                                      text: item.customerName.capitalize,
                                      style: AppTextStyle.subtitle2.copyWith(
                                        color: AppColors.textPrimary,
                                        fontWeight: FontWeight.w600,
                                        overflow: TextOverflow.ellipsis,
                                      )),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            kDateTimeFormat(item.txnDateTime,
                                showTimeOnly: true),
                            textAlign: TextAlign.end,
                            style: AppTextStyle.caption.copyWith(
                              color: AppColors.textSecondary,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ))
                ],
              )),
        ));
  }

  // Widget listItem(NotifyItemOutputModel item) {
  //   return InkWell(
  //     onTap: () => _controller.toTxnDetailAsync(0),
  //     child: Container(
  //       padding: const EdgeInsets.all(15),
  //       decoration: BoxDecoration(
  //         // color: item.isUnread
  //         //     ? AppColors.darkBlue.withOpacity(0.05)
  //         //     : AppColors.white,
  //         color: AppColors.white
  //       ),
  //       child: Padding(
  //           padding: const EdgeInsets.only(left: 10, right: 10),
  //           child: Row(
  //             mainAxisAlignment: MainAxisAlignment.start,
  //             crossAxisAlignment: CrossAxisAlignment.center,
  //             children: [
  //               ClipOval(
  //                 child: CustomImage(
  //                   // item.type == NotificationType.transaction
  //                   //     ? PngAppAssets.trxCnbLogo
  //                   //     : PngAppAssets.trxKhQrLogo,
  //                   PngAppAssets.icEmptyBusiness,
  //                   width: 44.w,
  //                   height: 44.w,
  //                   isNetwork: false,
  //                   isShadow: false,
  //                 ),
  //               ),
  //               SizedBox(
  //                 width: 10.w,
  //               ),
  //               Expanded(
  //                   child: Column(
  //                 mainAxisAlignment: MainAxisAlignment.center,
  //                 crossAxisAlignment: CrossAxisAlignment.start,
  //                 children: [
  //                   Text(
  //                     item.txrId,
  //                     style: AppTextStyle.value,
  //                     maxLines: 2,
  //                   ),
  //                   const SizedBox(
  //                     height: 7,
  //                   ),
  //                   Text(
  //                       kDateTimeFormat(item.txnDateTime,
  //                           showTimeOnly:true),
  //                       textAlign: TextAlign.start,
  //                       style: AppTextStyle.label)
  //                 ],
  //               ))
  //             ],
  //           )),
  //     ),
  //   );
  // }

  //#endregion
}
