import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'dart:io' show Platform;

class NotificationNewsDetailScreen extends StatefulWidget {
  const NotificationNewsDetailScreen({super.key});
  @override
  State<NotificationNewsDetailScreen> createState() =>
      _NotificationNewsDetailScreenState();
}

class _NotificationNewsDetailScreenState
    extends State<NotificationNewsDetailScreen> {
  final _controller = Get.put(NotificationNewsDetailScreenController());

  @override
  Widget build(BuildContext context) {
    return _webView();
  }

  Widget _webView() {
    return ContainerTemp(
      header: InkWell(
        onTap: () {
          Get.back(result: _controller.isOnline);
        },
        child: Align(
          alignment: Alignment.centerLeft,
          child: Padding(
            padding: const EdgeInsets.only(top: 15, left: 20, bottom: 20),
            child: Row(children: [
              Icon(
                  Platform.isAndroid
                      ? Icons.arrow_back
                      : Icons.arrow_back_ios_new,
                  size: 20,
                  color: AppColors.white),
              const SizedBox(width: 15),
              Text(
                "${"Detail".tr} ",
                style: AppTextStyle.header,
              )
            ]),
          ),
        ),
      ),

      /// [InAppWebView] show black screen on start when turn on dark mode
      /// using [Opacity] to hide it until load complete
      child: Obx(() {
        if (!_controller.isOnline) {
          return ErrorNetworkWidget(
            onRetryPressed: _controller.retryAsync,
          );
        } else if (_controller.loadWebViewError) {
          return ErrorNetworkWidget(
            assetName: JsonAppAssets.underMaintenance,
            title: "",
            description: "SomethingWentWrongPleaseTryAgainLater",
            onRetryPressed: () {
              _controller.loadWebViewError = false;
            },
          );
        } else {
          return KWebViewWidget(
            url: _controller.detailUrl,
            onWebViewCreated: (con) {
              _controller.showLoading();
            },
            onError: () {
              _controller.dismiss();
              _controller.loadWebViewError = true;
            },
            onStop: () {
              _controller.dismiss();
            },
          );
        }
      }),
    );
  }
}
