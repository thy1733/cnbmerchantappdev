import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

class ForgotPasswordScreen extends StatefulWidget {
  const ForgotPasswordScreen({super.key});

  @override
  State<ForgotPasswordScreen> createState() => _ForgotPasswordScreenState();
}

class _ForgotPasswordScreenState extends State<ForgotPasswordScreen> {
  final ForgotPasswordScreenController controller =
      Get.put(ForgotPasswordScreenController());
  SizedBox get _seperator => const SizedBox(height: 15);

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => ContainerAnimatedTemplate(
        title: "ForgotPassword".tr,
        floatingActionButton: AppButtonStyle.primaryButton(
            label: "Next".tr,
            isEnable: controller.isEnable,
            onPressed: () {
              controller.onNextPress();
            },
            margin: const EdgeInsets.symmetric(horizontal: 15)),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              // ICON DESCRIPTION
              SvgPicture.asset(SvgAppAssets.forgotUsernamePasswordLogo),
              _seperator,

              Text(
                "AccountCredential".tr,
                style: AppTextStyle.subtitle1,
              ),
              _seperator,
              Text(
                "ForgotPasswordScreenDescription".tr,
                style: AppTextStyle.caption,
              ),
              _seperator,

              // INPUT
              KTextField(
                name: "AccountNumberORInvitationCode".tr,
                iconData: MerchantIcon.user,
                hint: "AccountNumberORInvitationCode".tr,
                keyboardType: TextInputType.number,
                textInputAction: TextInputAction.next,
                inputFormatters: [MaskFormat.accountNumber],
                validatorMessage:
                    controller.accountNumberOrInviteCodeValidateMessage.tr,
                onChanged: (value) => {
                  controller.accountNumberOrInviteCode = value ?? "",
                  controller.accountValidator(),
                },
              ),
              KTextField(
                name: "PhoneNumber",
                label: "PhoneNumber".tr,
                initialValue: "",
                prefixText: '+855 (0)',
                validatorMessage: controller.phoneNumberValidateMessage.tr,
                iconData: MerchantIcon.telephone,
                keyboardType: TextInputType.phone,
                onChanged: (value) => {
                  controller.phoneNumber = value ?? "",
                  controller.phoneValidator(),
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
