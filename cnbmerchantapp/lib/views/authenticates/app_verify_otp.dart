import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

class AppVerifyOTP {
  Future<void> requestVerifyOTP({
    required String phoneNumber,
    required String cidOrAccountNumber,
    required OnVerifyOTPSuccess onSuccess,
    required String merchantId,
    ActivateType activateType = ActivateType.businessOwner,
    String title = "Title",
    String fromPage = "",
  }) async {
    await Get.to(_VerifyOtpScreen(
        cidOrAccountNumber: cidOrAccountNumber,
        phoneNumber: phoneNumber,
        merchantId: merchantId,
        activateType: activateType,
        title: title,
        fromPage: fromPage,
        onSuccess: onSuccess));
  }
}

// typedef OnVerifyOTPSuccess = void Function(String otpCode);

class _VerifyOtpScreen extends StatefulWidget {
  const _VerifyOtpScreen({
    Key? key,
    required this.cidOrAccountNumber,
    required this.phoneNumber,
    required this.activateType,
    required this.merchantId,
    required this.title,
    required this.onSuccess,
    required this.fromPage,
  }) : super(key: key);
  final String cidOrAccountNumber;
  final String merchantId;
  final String phoneNumber;
  final String title;
  final OnVerifyOTPSuccess onSuccess;
  final String fromPage;
  final ActivateType activateType;

  @override
  // ignore: library_private_types_in_public_api
  _VerifyOtpScreenState createState() => _VerifyOtpScreenState();
}

class _VerifyOtpScreenState extends State<_VerifyOtpScreen>
    with SingleTickerProviderStateMixin {
  final VerifyOtpScreenController controller =
      Get.put(VerifyOtpScreenController());

  @override
  void initState() {
    controller.onSuccess = widget.onSuccess;
    controller.fromPage = widget.fromPage;
    controller.verifyOtpInputModel.phoneNumber = widget.phoneNumber;
    controller.verifyOtpInputModel.merchantId = widget.merchantId;
    controller.verifyOtpInputModel.cidOrAccountNumber =
        widget.cidOrAccountNumber;
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      setState(() {
        pinWidth = 50 * pinLenght < Get.width - 20 ? 50.0 : null;
      });
    });

    super.initState();
  }


  double? pinWidth;
  double pinLenght = 6;
  @override
  Widget build(BuildContext context) {
    return ContainerAnimatedTemplate(title: widget.title, child: _body());
  }

  Widget _body() => SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: [
            const SizedBox(
              height: 30,
            ),
            SvgPicture.asset(
              SvgAppAssets.verifyOTPLogo,
              height: 65,
              fit: BoxFit.contain,
            ),
            const SizedBox(
              height: 20,
            ),
            Text(
              "VerifyOtp".tr,
              textAlign: TextAlign.center,
              style: AppTextStyle.subtitle1,
            ),
            const SizedBox(
              height: 10,
            ),
            Text.rich(
              TextSpan(
                style: AppTextStyle.caption,
                children: [
                  TextSpan(
                    text: 'SentSMStoPhone'.tr.replaceAll(
                        "{0}", "0${widget.phoneNumber}".maskPhoneNumber()),
                    style: const TextStyle(
                        color: AppColors.textPrimary, fontSize: 15),
                  ),
                  const TextSpan(
                    text: "",
                    style: TextStyle(
                      color: AppColors.textPrimary,
                      fontWeight: FontWeight.w400,
                      fontSize: 14,
                    ),
                  ),
                ],
              ),
              textAlign: TextAlign.center,
            ),
            const SizedBox(
              height: 50,
            ),
            Obx(() => _otpPart()),
            Obx(() =>
                Text(controller.isOtpIncorrect ? 'OtpIncorrectMsg'.tr : '')),
            const SizedBox(
              height: 20,
            ),
            SizedBox(
                height: 35,
                child: Obx(() => controller.counter > 0
                    ? Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "PleaseWait".tr,
                            textAlign: TextAlign.right,
                            style: const TextStyle(
                                color: AppColors.textSecondary,
                                fontSize: 15,
                                height: 1,
                                fontWeight: FontWeight.w400),
                          ),
                          const SizedBox(
                            width: 5,
                          ),
                          Text(
                            controller.timeDisplay,
                            textAlign: TextAlign.right,
                            style: const TextStyle(
                                color: AppColors.primary,
                                height: 1,
                                fontSize: 15,
                                fontWeight: FontWeight.w400),
                          ),
                        ],
                      )
                    : Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "DidReceivedSMS".tr,
                            textAlign: TextAlign.right,
                            style: const TextStyle(
                                color: AppColors.textSecondary,
                                height: 1,
                                fontSize: 15,
                                fontWeight: FontWeight.w400),
                          ),
                          const SizedBox(
                            width: 5,
                          ),
                          InkWell(
                            child: Text(
                              "Resend".tr,
                              textAlign: TextAlign.right,
                              style: const TextStyle(
                                  color: AppColors.primary,
                                  height: 1,
                                  fontSize: 15,
                                  fontWeight: FontWeight.w400),
                            ),
                            onTap: () {
                              controller.resendAsync(
                                  activateNumber:widget.activateType == ActivateType.existingMerchant?widget.merchantId:widget.cidOrAccountNumber,
                                  phoneNumber: widget.phoneNumber,
                                  activateType: widget.activateType);
                            },
                          ),
                        ],
                      ))),
          ],
        ),
      );

  Widget _otpPart() {
    return ShakeWidget(
      key: controller.shakeKey,
      shakeCount: 3,
      shakeOffset: 10,
      shakeDuration: const Duration(milliseconds: 500),
      child: PinCodeTextField(
        appContext: context,
        length: 6,
        obscureText: false,
        scrollPadding: const EdgeInsets.all(10),
        cursorColor: AppColors.textSecondary,
        autoFocus: true,
        animationType: AnimationType.scale,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        pinTheme: PinTheme(
            shape: PinCodeFieldShape.circle,
            borderWidth: 0,
            fieldHeight: pinWidth,
            fieldWidth: pinWidth,
            activeFillColor: Colors.transparent,
            selectedFillColor: AppColors.textFieldBackground,
            inactiveFillColor: AppColors.textFieldBackground,
            selectedColor: Colors.transparent,
            inactiveColor: controller.isOtpIncorrect
                ? AppColors.primaryDark
                : Colors.transparent,
            activeColor: controller.isOtpIncorrect
                ? AppColors.primaryDark
                : Colors.transparent),
        keyboardType: TextInputType.number,
        animationDuration: const Duration(milliseconds: 300),
        backgroundColor: Colors.transparent,
        inputFormatters: [FilteringTextInputFormatter.allow(RegExp(r'[0-9]'))],
        enableActiveFill: true,
        boxShadows: const [BoxShadow(color: Colors.transparent)],
        // errorAnimationController: errorController,
        controller: controller.textEditingController,
        onCompleted: (v) async {
          controller.verifyOtpInputModel.otpCode = v;
          controller.verifyOtpInputModel.cidOrAccountNumber =
              widget.cidOrAccountNumber;
          if(widget.activateType == ActivateType.cashier ||
              (controller.verifyOtpInputModel.cidOrAccountNumber.length==7)){
            controller.verifyOtpInputModel.inviteCode = widget.cidOrAccountNumber;
            controller.verifyOtpInputModel.cidOrAccountNumber = "";
          }

          await controller.verifyOTPAsync(v);
        },
        onChanged: (value) {
          controller.onOtpChanging(value);
        },
        beforeTextPaste: (text) {
          // print("Allowing to paste $text");
          //if you return true then it will show the paste confirmation dialog. Otherwise if false, then nothing will happen.
          //but you can show anything you want here, like your pop up saying wrong paste format or etc
          return true;
        },
      ),
    );
  }
}
