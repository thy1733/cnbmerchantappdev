import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

class NewPasswordScreen extends StatefulWidget {
  const NewPasswordScreen({super.key});

  @override
  State<NewPasswordScreen> createState() => _NewPasswordScreenState();
}

class _NewPasswordScreenState extends State<NewPasswordScreen> {
  final NewPasswordScreenController _controller =
      Get.put(NewPasswordScreenController());

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => ContainerAnimatedTemplate(
        title: _controller.title,
        floatingActionButton: _nextBtn(),
        padding: EdgeInsets.zero,
        child: _body(),
      ),
    );
  }

  Widget _body() {
    return SingleChildScrollView(
      padding: const EdgeInsets.fromLTRB(15, 15, 15, 0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          // ICON DESCRIPTION

          ContainerDevider.blankSpaceLg,
          SvgPicture.asset(
            SvgAppAssets.createNewPasswordLogo,
            width: (Get.width * 0.25).h,
          ),

          Padding(
            padding: const EdgeInsets.only(top: 20, bottom: 10).r,
            child: Text(
              "CreateNewPassword".tr,
              textAlign: TextAlign.center,
              style: AppTextStyle.body1.copyWith(
                  color: AppColors.textPrimary, fontWeight: FontWeight.w700),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 30).r,
            child: Text(
              "PasswordValidationLabel".tr,
              textAlign: TextAlign.center,
              style: AppTextStyle.body2.copyWith(
                color: AppColors.textSecondary,
              ),
            ),
          ),

          // INPUT
          KTextField(
            iconData: MerchantIcon.key,
            name: "NewPassword".tr,
            hint: "NewPassword".tr,
            keyboardType: TextInputType.visiblePassword,
            textInputAction: TextInputAction.next,
            validatorMessage: _controller.newPasswordValidateMessage.tr,
            obscureText: !_controller.isPasswordVisible,
            onChanged: (value) => {
              _controller.newPasswordInput = value ?? "",
              _controller.newPasswordValidator(),
            },
            secondChild: _controller.newPasswordInput.isNotEmpty
                ? InkWell(
                    onTap: () => _controller.isPasswordVisible =
                        !_controller.isPasswordVisible,
                    child: Padding(
                      padding: const EdgeInsets.all(2.0),
                      child: Icon(
                        _controller.isPasswordVisible
                            ? Icons.visibility_off
                            : Icons.visibility,
                        color: AppColors.textPrimary,
                        size: 20,
                      ),
                    ),
                  )
                : null,
          ),
          KTextField(
            name: "ConfirmPassword".tr,
            hint: "ConfirmPassword".tr,
            initialValue: "",
            validatorMessage: _controller.confirmPasswordValidateMessage.tr,
            iconData: MerchantIcon.key,
            keyboardType: TextInputType.visiblePassword,
            obscureText: !_controller.isConfirmedPasswordVisible,
            onSubmit: (value) => _controller.onNextBtnPress(),
            onChanged: (value) => {
              _controller.confirmPasswordInput = value ?? "",
              _controller.confirmPasswordValidator(),
            },
            secondChild: _controller.confirmPasswordInput.isNotEmpty
                ? InkWell(
                    onTap: () => _controller.isConfirmedPasswordVisible =
                        !_controller.isConfirmedPasswordVisible,
                    child: Padding(
                      padding: const EdgeInsets.all(2.0),
                      child: Icon(
                        _controller.isConfirmedPasswordVisible
                            ? Icons.visibility_off
                            : Icons.visibility,
                        color: AppColors.textPrimary,
                        size: 20,
                      ),
                    ),
                  )
                : null,
          ),
          SizedBox(
            height: 20.r,
          ),
        ],
      ),
    );
  }

  Widget _nextBtn() {
    return AppButtonStyle.primaryButton(
      label: "Next".tr,
      isEnable: _controller.isEnable,
      onPressed: _controller.onNextBtnPress,
      margin: const EdgeInsets.symmetric(horizontal: 15).r,
    );
  }
}
