import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

class NewPinScreen extends StatefulWidget {
  const NewPinScreen({Key? key}) : super(key: key);

  @override
  State<NewPinScreen> createState() => _NewPinScreenState();
}

class _NewPinScreenState extends State<NewPinScreen> {
  final NewPinScreenController controller = Get.put(NewPinScreenController());
  double iconHeight = Get.width * 0.25;
  final double headerSized = Get.height - 400;

  @override
  void dispose() {
    super.dispose();
    controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => ContainerTemp(
        header: NavigationTitle(
          title: controller.title,
          backButton: () => controller.onCancelBtnPressed(),
        ),
        child: _body(),
      ),
    );
  }

  Widget _body() {
    return Padding(
      padding: const EdgeInsets.all(15.0).r,
      child: Column(
        children: [
          ContainerDevider.blankSpaceLg,
          SvgPicture.asset(
            SvgAppAssets.userCircleGray,
            height: iconHeight,
            fit: BoxFit.contain,
          ),
          ContainerDevider.blankSpaceSm,
          Text(
            controller.subTitle,
            textAlign: TextAlign.center,
            style: AppTextStyle.subtitle1,
          ),
          ContainerDevider.blankSpaceSm,
          SizedBox(
            height: 60,
            child: Text(
              controller.pinConfirmValue.isNotEmpty
                  ? 'PleaseReEnterYour0digitPin'
                      .tr
                      .replaceAll('{0}', '${ValueConst.appPinLength}')
                  : "SetUpPinLabel".tr,
              textAlign: TextAlign.center,
              style: AppTextStyle.caption,
            ),
          ),
          _pinCodeField(context),
          Text(
            'IncorrectConfirmPIN'.tr,
            style: TextStyle(
              fontSize: 13,
              color: controller.isConfirmPinIncorrect
                  ? AppColors.primaryDark
                  : Colors.transparent,
            ),
          ),
          // const Expanded(flex: 1, child: SizedBox()),
          Expanded(
              flex: 30,
              child: KNumpadWidget(
                fontSize: 26.sp,
                textColor: AppColors.textPrimary,
                onBackSpacePressed: controller.onBackSpacePressed,
                onClearPress: controller.onClearPress,
                onPressed: controller.onNumPressed,
              )),
          ContainerDevider.blankSpaceMd,
        ],
      ),
    );
  }

  Widget _pinCodeField(BuildContext context) {
    return ShakeWidget(
      // 4. pass the GlobalKey as an argument
      key: controller.shakeKey,
      // 5. configure the animation parameters
      shakeCount: 2,
      shakeOffset: 5,
      shakeDuration: const Duration(milliseconds: 300),
      // 6. Add the child widget that will be animated
      child: PinCodeTextField(
        appContext: context,
        length: ValueConst.appPinLength,
        obscureText: true,
        obscuringWidget: AnimatedContainer(
          duration: const Duration(milliseconds: 200),
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: controller.isConfirmPinIncorrect
                ? const Color.fromARGB(255, 255, 108, 108)
                : AppColors.primaryDark,
          ),
        ),
        animationType: AnimationType.fade,
        pinTheme: PinTheme(
          shape: PinCodeFieldShape.circle,
          activeFillColor: AppColors.primary,
          inactiveFillColor: Colors.black,
          activeColor: AppColors.primary,
          inactiveColor: AppColors.barrierBackground.withOpacity(.2),
          borderWidth: 10,
          fieldOuterPadding: const EdgeInsets.all(5),
          fieldHeight: 15,
          fieldWidth: 15,
        ),
        mainAxisAlignment: MainAxisAlignment.center,
        animationDuration: const Duration(milliseconds: 0),
        keyboardType: TextInputType.none,
        controller: controller.pinCodeFieldController,
        onChanged: (value) {},
        beforeTextPaste: (text) {
          return true;
        },
      ),
    );
  }
}
