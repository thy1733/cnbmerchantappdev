import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:cnbmerchantapp/core.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

class ConfirmPinScreen extends StatefulWidget {
  const ConfirmPinScreen(
      {Key? key,
      required this.onSuccess,
      required this.onBackPress,
      this.title,
      this.isPermanentRequired = false,
      this.message,
      this.description,
      this.reachManyAttemp,
      this.image = ""})
      : super(key: key);
  final String? title;
  final String? message;
  final String? description;
  final Function(RequestCallback callback) onSuccess;
  final VoidCallback? reachManyAttemp;
  final VoidCallback onBackPress;
  final bool isPermanentRequired;
  final String image;

  @override
  State<ConfirmPinScreen> createState() => _ConfirmPinScreenState();
}

class _ConfirmPinScreenState extends State<ConfirmPinScreen> {
  final ConfirmPinScreenController _controller =
      Get.put(ConfirmPinScreenController());

  TextEditingController pinCodeFieldController = TextEditingController();
  late String message;

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
      if (widget.isPermanentRequired == false) {
        await _controller.checkPinSession();
      }
    });
    _controller.pinCodeFieldController = pinCodeFieldController;
    _controller.clearPinValue();
    _controller.onSuccess = widget.onSuccess;
    super.initState();
  }

  @override
  void dispose() {
    widget.onBackPress();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ContainerTemp(
      header: NavigationTitle(title: widget.title ?? 'ConfirmPin'.tr),
      child: Padding(
        padding: const EdgeInsets.only(top: 40).r,
        child: _body(context),
      ),
    );
  }

  Widget _body(BuildContext context) {
    final double imageSize = 80.w;
    const double headerSized = 135;
    return SizedBox(
      height: Get.height - headerSized,
      width: Get.width,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            flex: 2,
            child: Column(
              children: [
                SvgPicture.asset(
                  widget.image.isNotEmpty ? widget.image : SvgAppAssets.keyIcon,
                  width: imageSize,
                  height: imageSize,
                ),
                SizedBox(height: 5.r),
                Text(
                  widget.message ?? 'ConfirmPin'.tr,
                  style: AppTextStyle.body1.copyWith(
                      color: AppColors.textPrimary,
                      fontWeight: FontWeight.w700),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 16).r,
                  child: Text(
                    widget.description ?? 'ConfirmPinLabel'.tr,
                    textAlign: TextAlign.center,
                    style: AppTextStyle.body2
                        .copyWith(color: AppColors.textSecondary),
                  ),
                ),
                IgnorePointer(
                  child: ShakeWidget(
                      key: _controller.shakeKey,
                      shakeCount: 3,
                      shakeOffset: 10,
                      shakeDuration: const Duration(milliseconds: 500),
                      child: _pinCodeField(context)),
                ),
                Obx(
                  (() => Opacity(
                        opacity: _controller.isConfirmPinIncorrect ? 1 : 0,
                        child: Text(
                          'IncorrectPIN'.tr,
                          style: const TextStyle(
                              fontSize: 13,
                              height: 1.5,
                              color: AppColors.primary),
                        ),
                      )),
                ),
              ],
            ),
          ),
          Expanded(
            flex: 3,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: KNumpadWidget(
                fontSize: 26,
                textColor: AppColors.darkGrey,
                onBackSpacePressed: _controller.onBackSpacePressed,
                onClearPress: _controller.onClearPress,
                onPressed: _controller.onNumPressed,
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _pinCodeField(BuildContext context) {
    return PinCodeTextField(
      appContext: context,
      length: ValueConst.appPinLength,
      obscureText: true,
      obscuringWidget: Obx(
        () => AnimatedContainer(
          duration: const Duration(milliseconds: 200),
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: _controller.isConfirmPinIncorrect
                ? const Color.fromARGB(255, 255, 108, 108)
                : AppColors.primary,
          ),
        ),
      ),
      animationType: AnimationType.fade,
      pinTheme: PinTheme(
        shape: PinCodeFieldShape.circle,
        activeFillColor: AppColors.primary,
        inactiveFillColor: Colors.black,
        activeColor: AppColors.primary,
        inactiveColor: AppColors.darkGrey,
        borderWidth: 1,
        fieldOuterPadding: const EdgeInsets.all(5),
        fieldHeight: 15,
        fieldWidth: 15,
      ),
      mainAxisAlignment: MainAxisAlignment.center,
      animationDuration: const Duration(milliseconds: 0),
      keyboardType: TextInputType.none,
      controller: pinCodeFieldController,
      onChanged: (value) {},
      beforeTextPaste: (text) {
        return true;
      },
    );
  }
}
