import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SetupBusinessScreen extends StatefulWidget {
  const SetupBusinessScreen({super.key});

  @override
  State<SetupBusinessScreen> createState() => _SetupBusinessScreenState();
}

class _SetupBusinessScreenState extends State<SetupBusinessScreen> {
  final SetupBusinessScreenController controller =
      Get.put(SetupBusinessScreenController());

  @override
  Widget build(BuildContext context) {
    const double padding = 16;

    return Obx(
      () => ContainerAnimatedTemplate(
          title: "SetupBusinessProfile".tr,
          hasBackButton: !(controller.fromPage == Routes.activateOwnerScreen ||
              controller.fromPage == Routes.login),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisSize: MainAxisSize.min,
              children: [
                Expanded(
                    flex: 1,
                    child: SingleChildScrollView(
                        physics: const ScrollPhysics(),
                        child: Column(
                          children: [
                            CircleImageUploadView(
                              imageFile: controller.photoFile,
                              placeHolder: SvgAppAssets.icDefaultShop,
                              onImagePicked: () {
                                controller.uploadBusinessProfile();
                              },
                            ),
                            const SizedBox(
                              height: padding * 2,
                            ),
                            businessNameSection(),
                            businessTypeSection(),
                            const SizedBox(
                              height: padding,
                            ),
                            businessLocationNameSection(),
                            businessLocationAddressSection(),
                            selectAccountSection(),
                            const SizedBox(
                              height: padding,
                            ),
                            staffReferralSection(),
                          ],
                        ))),
                AppButtonStyle.primaryButton(
                    label: "CreateBusiness".tr,
                    onPressed: controller.createBusinessAsync),
              ])),
    );
  }

  Widget businessNameSection() {
    return KTextField(
      textInputAction: TextInputAction.next,
      focusNode: controller.businessNameFocus,
      name: "BusinessName".tr,
      hint: "BusinessName".tr,
      iconData: MerchantIcon.shop,
      keyboardType: TextInputType.text,
      textCapitalization: TextCapitalization.words,
      maxLine: 1,
      inputFormatters: [
        CustomLengthLimitingTextInputFormatter(
            ValueConst.businessNameLength + 1),
        AppTextInputRegx.inputNameRegx,
      ],
      validatorMessage: controller.businessNameValidateMsg,
      onChanged: (value) {
        controller.onBusinessNameChanged(value!);
      },
    );
  }

  Widget businessTypeSection() {
    return ContainerButton(
        submit: (value) {
          controller.onBusinessTypesSelected(value);
        },
        child: DropdownOption(
            title: "BusinessType".tr,
            value: controller.businessTypes.items.isEmpty
                ? ""
                : controller.businessTypes.items
                    .elementAt(controller.selectedBusinessTypeId)
                    .name,
            leadingIconData: MerchantIcon.businessType));
  }

  Widget businessLocationNameSection() {
    return InkWell(
      onTap: () {
        controller.onBrowseLocation();
      },
      child: KTextField(
        controller: controller.locationNameConroller,
        enabled: false,
        name: "Location".tr,
        hint: "SelectLocation".tr,
        validatorMessage: controller.businessLocationNameValidateMsg,
        iconData: MerchantIcon.mapLocation,
        needIcon: false,
        secondChild: const Icon(
          MerchantIcon.mapLocation,
          size: 16,
        ),
      ),
    );
  }

  Widget businessLocationAddressSection() {
    return KTextField(
      controller: controller.addressInputConroller,
      textInputAction: TextInputAction.done,
      maxLine: 5,
      name: "Address".tr,
      hint: "Address".tr,
      iconData: MerchantIcon.shop,
      keyboardType: TextInputType.multiline,
      textCapitalization: TextCapitalization.words,
      validatorMessage: controller.businessLocationAddressValidateMsg,
      onChanged: (value) {
        controller.onBusinessLocationAddressChanged(value!);
      },
    );
  }

  Widget selectAccountSection() {
    return Visibility(
        visible: !(controller.fromPage == Routes.activateOwnerScreen ||
            controller.fromPage == Routes.login),
        child: ContainerButton(
          submit: (value) {
            controller.onSelectBankAcountAsync();
          },
          child: DropdownOption(
            title: "SelectAccount".tr,
            value: "SelectAccount".tr,
            leadingIconData: MerchantIcon.user,
            child: controller.accounts.items.isEmpty
                ? null
                : Text.rich(
                    TextSpan(
                      style: AppTextStyle.value,
                      children: [
                        TextSpan(
                          text:
                              "${controller.accounts.items.elementAt(controller.selectedAccountId).accountName.tr} ",
                          style: const TextStyle(
                            fontWeight: FontWeight.w100,
                          ),
                        ),
                        TextSpan(
                          text: controller.accounts.items
                              .elementAt(controller.selectedAccountId)
                              .accountNumber,
                          style: const TextStyle(
                            fontWeight: FontWeight.w900,
                          ),
                        ),
                      ],
                    ),
                  ),
          ),
        ));
  }

  Widget staffReferralSection() {
    return KTextField(
      textInputAction: TextInputAction.done,
      name: "StaffReferralID".tr,
      hint: "StaffReferralID".tr,
      iconData: MerchantIcon.shop,
      needIcon: false,
      keyboardType: TextInputType.number,
      textCapitalization: TextCapitalization.words,
      onChanged: (value) {
        controller.onStaffRefChanged(value!);
      },
    );
  }
}
