import 'dart:io';

import 'package:cnbmerchantapp/core.dart';
import 'package:fdottedline_nullsafety/fdottedline__nullsafety.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:image_cropper/image_cropper.dart';

class UpgradeBusinessScreen extends StatefulWidget {
  const UpgradeBusinessScreen({super.key});

  @override
  State<UpgradeBusinessScreen> createState() => _UpgradeBusinessScreenState();
}

class _UpgradeBusinessScreenState extends State<UpgradeBusinessScreen> {
  final UpgradeScreenController controller = Get.put(UpgradeScreenController());

  @override
  Widget build(BuildContext context) {
    final double padding = 16.r;

    return Obx(
      () => ContainerTemp(
        header: NavigationTitle(
          title: "CorporateMerchant".tr,
          backButton: () => Get.back(),
        ),
        child: Padding(
          padding: EdgeInsets.all(padding),
          child: Column(
            children: [
              Expanded(
                child: SingleChildScrollView(
                  physics: const AlwaysScrollableScrollPhysics(),
                  child: Column(
                    children: [
                      photoSection(),
                      SizedBox(
                        height: padding,
                      ),
                      businessNameSection(),
                      SizedBox(
                        height: padding - 6,
                      ),
                      descriptionSection(),
                      SizedBox(
                        height: padding,
                      ),
                      attachDocumentsAsGridSection(),
                      SizedBox(
                        height: padding,
                      ),
                      uploadPhotoSection(),
                      SizedBox(
                        height: padding,
                      ),
                      noteSection(),
                    ],
                  ),
                ),
              ),
              ButtonCircleRadius(
                  title: "UpgradeNow".tr,
                  submit: (_) {
                    controller.onUpgradeBusiness();
                  })
            ],
          ),
        ),
      ),
    );
  }

  Widget photoSection() {
    return ClipRRect(
      borderRadius: BorderRadius.circular(60.r),
      child: AppNetworkImage(
        controller.businessInfo.businessLogo,
        width: 70.r,
        height: 70.r,
      ),
    );
  }

  Widget businessNameSection() => Text(
        controller.businessInfo.businessName,
        style: AppTextStyle.header1,
      );

  Widget descriptionSection() => Text(
        "SupportDocsDescription".tr,
        style: AppTextStyle.header2,
      );

  Widget attachDocumentsAsGridSection() {
    return Visibility(
      visible: controller.documents.isNotEmpty,
      child: GridView.builder(
          padding: EdgeInsets.zero,
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
          gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
              childAspectRatio: 1 / 1,
              crossAxisSpacing: 8,
              mainAxisSpacing: 8,
              crossAxisCount: 2),
          itemCount: controller.documents.length,
          itemBuilder: (context, index) {
            CroppedFile item = controller.documents[index];
            return itemGridDocumentBuilder(item, index);
          }),
    );
  }

  Widget itemGridDocumentBuilder(CroppedFile file, int index) {
    final double wid = Get.width;
    final double imageSize = wid;

    return Stack(
      children: [
        Card(
          elevation: 6,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(16)),
          child: ClipRRect(
              borderRadius: BorderRadius.circular(16),
              child: Image(
                image: FileImage(File(file.path)),
                fit: BoxFit.cover,
                width: imageSize,
                height: imageSize,
              )),
        ),
        Positioned(
            top: 0,
            right: 0,
            child: IconButton(
                onPressed: () {
                  controller.removeDocument(index);
                },
                icon: const Icon(
                  Icons.cancel_rounded,
                  color: AppColors.grey,
                )))
      ],
    );
  }

  Widget uploadPhotoSection() {
    final double padding = 16.r;

    return Visibility(
      visible: controller.isUploadDocsReachLimit,
      child: InkWell(
        onTap: () {
          controller.chooseDocument();
        },
        child: FDottedLine(
          corner: FDottedLineCorner.all(10),
          color: AppColors.primary,
          width: 2,
          child: Container(
              padding:
                  EdgeInsets.symmetric(vertical: padding, horizontal: padding),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const Icon(
                    Icons.add,
                    color: AppColors.primary,
                  ),
                  const SizedBox(
                    width: 5,
                  ),
                  Text(
                    "UploadPhotoMessage".tr,
                    style: AppTextStyle.header2,
                  )
                ],
              )),
        ),
      ),
    );
  }

  Widget noteSection() {
    return KTextField(
      name: "Note".tr,
      iconData: MerchantIcon.note,
      hint: "Note".tr,
      onChanged: (value) {
        controller.onNoteChanged(value!);
      },
    );
  }
}
