import 'package:cnbmerchantapp/core.dart';

class CreateMerchantPromotionOutputModel {
  DatePeriodModel date = DatePeriodModel();
  String rate = "";
  MerchantPromotionCoveredBy? coveredBy;
  String coveredByBankValue = "";
  String coveredByMerchantValue = "";
  String repeatOption = "";

  CreateMerchantPromotionOutputModel();

  CreateMerchantPromotionOutputModel.create({
    required this.date,
    required this.rate,
    required this.coveredBy,
    required this.coveredByBankValue,
    required this.coveredByMerchantValue,
    required this.repeatOption,
  });

  static List<CreateMerchantPromotionOutputModel> sampleData() {
    return [
      CreateMerchantPromotionOutputModel.create(
          date: DatePeriodModel(),
          rate: "20%",
          coveredBy: MerchantPromotionCoveredBy.bank,
          coveredByBankValue: "",
          coveredByMerchantValue: "",
          repeatOption: "Weekly"),
      CreateMerchantPromotionOutputModel.create(
          date: DatePeriodModel(),
          rate: "30%",
          coveredBy: MerchantPromotionCoveredBy.merchant,
          coveredByBankValue: "",
          coveredByMerchantValue: "",
          repeatOption: "Monthly"),
      CreateMerchantPromotionOutputModel.create(
          date: DatePeriodModel(),
          rate: "50%",
          coveredBy: MerchantPromotionCoveredBy.both,
          coveredByBankValue: "20%",
          coveredByMerchantValue: "30%",
          repeatOption: "Every3Months")
    ];
  }
}
