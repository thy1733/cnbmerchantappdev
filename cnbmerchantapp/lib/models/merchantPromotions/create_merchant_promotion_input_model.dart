import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class CreateMerchantPromotionInputModel {
  CreateMerchantPromotionInputModel();

  final _date = DatePeriodModel().obs;
  DatePeriodModel get date => _date.value;
  set date(DatePeriodModel value) => _date.value = value;

  final _repeatOptionIndex = Rxn<int>();
  int? get repeatOptionIndex => _repeatOptionIndex.value;
  set repeatOptionIndex(int? value) => _repeatOptionIndex.value = value;

  final _discountOptionIndex = Rxn<int>();
  int? get discountOptionIndex => _discountOptionIndex.value;
  set discountOptionIndex(int? value) => _discountOptionIndex.value = value;

  bool get isRatePercentageSelected =>
      discountOptionIndex != null && discountOptionIndex == 0;

  bool get isFixedAmountSelected =>
      discountOptionIndex != null && discountOptionIndex == 1;

  final _rateOptionIndex = 0.obs;
  int get rateOptionIndex => _rateOptionIndex.value;
  set rateOptionIndex(int value) => _rateOptionIndex.value = value;

  final _fixedAmountValue = 0.0.obs;
  double get fixedAmountValue => _fixedAmountValue.value;
  set fixedAmountValue(double value) => _fixedAmountValue.value = value;

  final _ratePercentageValue = 0.obs;
  int get ratePercentageValue => _ratePercentageValue.value;
  set ratePercentageValue(int value) => _ratePercentageValue.value = value;

  final _fixedAmountValidatorMessage = ''.obs;
  String get fixedAmountValidatorMessage => _fixedAmountValidatorMessage.value;
  set fixedAmountValidatorMessage(String value) =>
      _fixedAmountValidatorMessage.value = value;

  final _coveredBy = MerchantPromotionCoveredBy.bank.obs;
  MerchantPromotionCoveredBy get coveredBy => _coveredBy.value;
  set coveredBy(MerchantPromotionCoveredBy value) => _coveredBy.value = value;

  final _coveredByBankValue = ''.obs;
  String get coveredByBankValue => _coveredByBankValue.value;
  set coveredByBankValue(String value) => _coveredByBankValue.value = value;

  final _coveredByMerchantValue = ''.obs;
  String get coveredByMerchantValue => _coveredByMerchantValue.value;
  set coveredByMerchantValue(String value) =>
      _coveredByMerchantValue.value = value;

  final _converedByBothValidatorMessage = ''.obs;
  String get converedByBothValidatorMessage =>
      _converedByBothValidatorMessage.value;
  set converedByBothValidatorMessage(String value) =>
      _converedByBothValidatorMessage.value = value;

  final _isAgreed = false.obs;
  bool get isAgreed => _isAgreed.value;
  set isAgreed(bool value) => _isAgreed.value = value;

  bool isValid() {
    var valid = repeatOptionIndex != null && isAgreed;

    // check discount option is fixed amount && amount field is not empty
    if (isFixedAmountSelected && fixedAmountValue != 0.0) {
      valid = valid && true;
    } else if (isRatePercentageSelected) {
      // check if discount option is Rate %
      valid = valid && true;
    } else {
      valid = valid && false;
    }

    // check covered by both merchant and bank && covered by bank value and covered by merchant value is not empty
    if (coveredBy == MerchantPromotionCoveredBy.both) {
      valid = valid &&
          coveredByBankValue.isNotEmpty &&
          coveredByMerchantValue.isNotEmpty &&
          converedByBothValidatorMessage.isEmpty;
    }
    return valid;
  }
}
