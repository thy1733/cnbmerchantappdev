import 'package:get/get.dart';

class MerchantPromotionModel {
  final _id = 0.obs;
  int get id => _id.value;
  set id(int value) => _id.value = value;

  final _imageUrl = ''.obs;
  String get imageUrl => _imageUrl.value;
  set imageUrl(String value) => _imageUrl.value = value;

  final _title = ''.obs;
  String get title => _title.value;
  set title(String value) => _title.value = value;

  final _description = ''.obs;
  String get description => _description.value;
  set description(String value) => _description.value = value;

  MerchantPromotionModel();

  MerchantPromotionModel.create({
    required int id,
    required String imageUrl,
    required String title,
    required String description,
  }) {
    this.id = id;
    this.imageUrl = imageUrl;
    this.title = title;
    this.description = description;
  }
}


