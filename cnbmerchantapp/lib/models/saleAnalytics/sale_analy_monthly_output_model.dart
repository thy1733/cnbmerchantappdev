import 'package:cnbmerchantapp/core.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:get/get.dart';

class SaleAnalyMonthlyOutputModel extends IndexModel {
  final _year = 0.obs;
  int get year => _year.value;
  set year(int value) => _year.value = value;

  final _selectDate = DateTime.now().obs;
  DateTime get selectDate => _selectDate.value;
  set selectDate(DateTime value) => _selectDate.value = value;

  final _totalCashier = 0.obs;
  int get totalCashier => _totalCashier.value;
  set totalCashier(int value) => _totalCashier.value = value;

  final _totalOutlet = 0.obs;
  int get totalOutlet => _totalOutlet.value;
  set totalOutlet(int value) => _totalOutlet.value = value;

  final _totalAmountSale = 0.0.obs;
  double get totalAmountSale => _totalAmountSale.value;
  set totalAmountSale(double value) => _totalAmountSale.value = value;

  final _totalTransactionSale = 0.obs;
  int get totalTransactionSale => _totalTransactionSale.value;
  set totalTransactionSale(int value) => _totalTransactionSale.value = value;

  final _totalAmountRefunde = 0.0.obs;
  double get totalAmountRefund => _totalAmountRefunde.value;
  set totalAmountRefund(double value) => _totalAmountRefunde.value = value;

  final _totalTransactionRefunde = 0.obs;
  int get totalTransactionRefund => _totalTransactionRefunde.value;
  set totalTransactionRefund(int value) =>
      _totalTransactionRefunde.value = value;

  final _items = <SaleAnalyDailyOutputModel>[].obs;
  List<SaleAnalyDailyOutputModel> get items => _items;
  set items(List<SaleAnalyDailyOutputModel> value) => _items.value = value;

  // List<FlSpot> get itemsToFlSpot => items.isEmpty
  //     ? []
  //     : items.map((e) => FlSpot(e.date.day.toDouble(), e.amount)).toList();

  static SaleAnalyMonthlyOutputModel create(
      SaleAnalyMonthlyOutputDto dto, SaleAnalyticInputModel input) {
    var result = SaleAnalyMonthlyOutputModel();
    result.year = input.selectedDate.year;
    // result.month = input.selectedDate.toFormatDateString("MMMM");
    result.selectDate = input.selectedDate;
    result.totalAmountSale = dto.totalAmountSale;
    result.totalTransactionSale = dto.totalTransactionSale;
    result.totalOutlet = dto.totalOutlet;
    result.totalCashier = dto.totalCashier;
    result.totalAmountRefund = dto.totalAmountRefund;
    result.totalTransactionRefund = dto.totalTransactionRefund;
    result.items =
        dto.items.map((e) => SaleAnalyDailyOutputModel.create(e)).toList();
    return result;
  }

  List<FlSpot> itemsToFlSpot() {
    try {
      // var datas = <FlSpot>[];
      // var numOfDay = DateTime(selectDate.year, selectDate.month + 1, 0).day;
      // for (int i = 1; i <= numOfDay; i++) {
      //   var data = items.firstWhereOrNull((e) => e.date.day == i);
      //   if (data != null) {
      //     datas.add(FlSpot(i.toDouble(), data.amount));
      //   }else {
      //     datas.add(FlSpot(i.toDouble(), 0.0));
      //   }
      // }

      // return datas;

       return items.isEmpty
          ? []
          : items.map((e) => FlSpot(e.date.day.toDouble(), e.amount)).toList();
    } catch (e) {
      e.printError();
      return [];
    }
  }
}
