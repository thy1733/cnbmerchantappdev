import 'package:get/get.dart';

class SaleAnalyticInputModel{
  final _businessId = ''.obs;
  get businessId => _businessId.value;
  set businessId(value) => _businessId.value = value;

  
  final _selectedDate = DateTime.now().obs;
  DateTime get selectedDate => _selectedDate.value;
  set selectedDate(DateTime value) => _selectedDate.value = value;
  

  SaleAnalyticInputModel({
    String businessId = "",
    DateTime? selectedDate,
  }) {
    this.businessId = businessId;
    this.selectedDate = selectedDate ?? this.selectedDate;
  }
}
