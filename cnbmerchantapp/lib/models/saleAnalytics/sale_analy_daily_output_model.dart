import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class SaleAnalyDailyOutputModel extends IndexModel {
  // SaleAnalyDailyOutputModel({
  //   DateTime? date,
  //   double amount = 0,
  //   String currency = 'usd',
  // }) {
  //   this.date = date ?? this.date;
  //   this.amount = amount;
  //   this.currency = currency;
  // }
  final _date = DateTime.now().obs;
  DateTime get date => _date.value;
  set date(DateTime value) => _date.value = value;

  final _amount = 0.0.obs;
  double get amount => _amount.value;
  set amount(double value) => _amount.value = value;

  final _numberOfTransaction = 0.obs;
  int get numberOfTransaction => _numberOfTransaction.value;
  set numberOfTransaction(int value) => _numberOfTransaction.value = value;

  static SaleAnalyDailyOutputModel create(SaleAnalyDailyOutputDto dto) {
    var result = SaleAnalyDailyOutputModel();
    result.date = dto.date;
    result.amount = dto.amount;
    result.numberOfTransaction = dto.numberOfTransaction;
    return result;
  }

  // static List<SaleAnalyDailyOutputModel> sample1() {
  //   return [
  //     // SaleAnalyInDayOutputModel(date: DateTime(2022, 11, 10), amount: 10),
  //     // SaleAnalyInDayOutputModel(date: DateTime(2022, 11, 9), amount: 14),
  //     // SaleAnalyInDayOutputModel(date: DateTime(2022, 11, 8), amount: 15),
  //     // SaleAnalyInDayOutputModel(date: DateTime(2022, 11, 7), amount: 35),
  //     // SaleAnalyInDayOutputModel(date: DateTime(2022, 11, 6), amount: 31),
  //     // SaleAnalyInDayOutputModel(date: DateTime(2022, 11, 5), amount: 12),
  //     // SaleAnalyInDayOutputModel(date: DateTime(2022, 11, 4), amount: 35),
  //     // SaleAnalyInDayOutputModel(date: DateTime(2022, 11, 3), amount: 15),
  //     SaleAnalyDailyOutputModel(date: DateTime(2022, 11, 2), amount: 05),
  //     SaleAnalyDailyOutputModel(date: DateTime(2022, 11, 1), amount: 2),
  //   ];
  // }

  // static List<SaleAnalyDailyOutputModel> sample2() {
  //   return [
  //     SaleAnalyDailyOutputModel(date: DateTime(2022, 11, 30), amount: 10),
  //     SaleAnalyDailyOutputModel(date: DateTime(2022, 11, 29), amount: 14),
  //     SaleAnalyDailyOutputModel(date: DateTime(2022, 11, 28), amount: 15),
  //     SaleAnalyDailyOutputModel(date: DateTime(2022, 11, 27), amount: 35),
  //     SaleAnalyDailyOutputModel(date: DateTime(2022, 11, 26), amount: 31),
  //     SaleAnalyDailyOutputModel(date: DateTime(2022, 11, 25), amount: 12),
  //     SaleAnalyDailyOutputModel(date: DateTime(2022, 11, 24), amount: 35),
  //     SaleAnalyDailyOutputModel(date: DateTime(2022, 11, 23), amount: 15),
  //     SaleAnalyDailyOutputModel(date: DateTime(2022, 11, 22), amount: 055),
  //     SaleAnalyDailyOutputModel(date: DateTime(2022, 11, 21), amount: 20),
  //     SaleAnalyDailyOutputModel(date: DateTime(2022, 11, 20), amount: 10),
  //     SaleAnalyDailyOutputModel(date: DateTime(2022, 11, 19), amount: 14),
  //     SaleAnalyDailyOutputModel(date: DateTime(2022, 11, 18), amount: 15),
  //     SaleAnalyDailyOutputModel(date: DateTime(2022, 11, 17), amount: 35),
  //     SaleAnalyDailyOutputModel(date: DateTime(2022, 11, 16), amount: 31),
  //     SaleAnalyDailyOutputModel(date: DateTime(2022, 11, 15), amount: 12),
  //     SaleAnalyDailyOutputModel(date: DateTime(2022, 11, 14), amount: 35),
  //     SaleAnalyDailyOutputModel(date: DateTime(2022, 11, 13), amount: 15),
  //     SaleAnalyDailyOutputModel(date: DateTime(2022, 11, 12), amount: 05),
  //     SaleAnalyDailyOutputModel(date: DateTime(2022, 11, 11), amount: 5),
  //     SaleAnalyDailyOutputModel(date: DateTime(2022, 11, 10), amount: 10),
  //     SaleAnalyDailyOutputModel(date: DateTime(2022, 11, 9), amount: 14),
  //     SaleAnalyDailyOutputModel(date: DateTime(2022, 11, 8), amount: 15),
  //     SaleAnalyDailyOutputModel(date: DateTime(2022, 11, 7), amount: 35),
  //     SaleAnalyDailyOutputModel(date: DateTime(2022, 11, 6), amount: 31),
  //     SaleAnalyDailyOutputModel(date: DateTime(2022, 11, 5), amount: 12),
  //     SaleAnalyDailyOutputModel(date: DateTime(2022, 11, 4), amount: 35),
  //     SaleAnalyDailyOutputModel(date: DateTime(2022, 11, 3), amount: 15),
  //     SaleAnalyDailyOutputModel(date: DateTime(2022, 11, 2), amount: 05),
  //     SaleAnalyDailyOutputModel(date: DateTime(2022, 11, 1), amount: 2),
  //   ];
  // }

  // static List<SaleAnalyDailyOutputModel> sample3() {
  //   return [
  //     SaleAnalyDailyOutputModel(date: DateTime(2022, 11, 30), amount: 10),
  //     SaleAnalyDailyOutputModel(date: DateTime(2022, 11, 29), amount: 14),
  //     SaleAnalyDailyOutputModel(date: DateTime(2022, 11, 28), amount: 15),
  //     SaleAnalyDailyOutputModel(date: DateTime(2022, 11, 27), amount: 35),
  //     SaleAnalyDailyOutputModel(date: DateTime(2022, 11, 26), amount: 31),
  //     SaleAnalyDailyOutputModel(date: DateTime(2022, 11, 25), amount: 12),
  //     SaleAnalyDailyOutputModel(date: DateTime(2022, 11, 24), amount: 35),
  //     SaleAnalyDailyOutputModel(date: DateTime(2022, 11, 23), amount: 15),
  //     SaleAnalyDailyOutputModel(date: DateTime(2022, 11, 22), amount: 055),
  //     SaleAnalyDailyOutputModel(date: DateTime(2022, 11, 21), amount: 20),
  //     SaleAnalyDailyOutputModel(date: DateTime(2022, 11, 20), amount: 10),
  //     SaleAnalyDailyOutputModel(date: DateTime(2022, 11, 19), amount: 14),
  //     SaleAnalyDailyOutputModel(date: DateTime(2022, 11, 18), amount: 15),
  //     SaleAnalyDailyOutputModel(date: DateTime(2022, 11, 17), amount: 35),
  //     SaleAnalyDailyOutputModel(date: DateTime(2022, 11, 16), amount: 31),
  //     SaleAnalyDailyOutputModel(date: DateTime(2022, 11, 15), amount: 12),
  //     SaleAnalyDailyOutputModel(date: DateTime(2022, 11, 14), amount: 35),
  //     SaleAnalyDailyOutputModel(date: DateTime(2022, 11, 13), amount: 101),
  //     SaleAnalyDailyOutputModel(date: DateTime(2022, 11, 12), amount: 051),
  //     SaleAnalyDailyOutputModel(date: DateTime(2022, 11, 11), amount: 52),
  //     SaleAnalyDailyOutputModel(date: DateTime(2022, 11, 10), amount: 50),
  //     SaleAnalyDailyOutputModel(date: DateTime(2022, 11, 9), amount: 44),
  //     SaleAnalyDailyOutputModel(date: DateTime(2022, 11, 8), amount: 11),
  //     SaleAnalyDailyOutputModel(date: DateTime(2022, 11, 7), amount: 15),
  //     SaleAnalyDailyOutputModel(date: DateTime(2022, 11, 6), amount: 111),
  //     SaleAnalyDailyOutputModel(date: DateTime(2022, 11, 5), amount: 122),
  //     SaleAnalyDailyOutputModel(date: DateTime(2022, 11, 4), amount: 35),
  //     SaleAnalyDailyOutputModel(date: DateTime(2022, 11, 3), amount: 114),
  //     SaleAnalyDailyOutputModel(date: DateTime(2022, 11, 2), amount: 045),
  //     SaleAnalyDailyOutputModel(date: DateTime(2022, 11, 1), amount: 31),
  //   ];
  // }
}
