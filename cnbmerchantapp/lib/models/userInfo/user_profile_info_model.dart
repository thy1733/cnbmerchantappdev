import 'package:get/get.dart';

class UserProfileInfoModel {
  final _displayName = ''.obs;
  String get displayName => _displayName.value;
  set displayName(String value) => _displayName.value = value;

  final _firstName = ''.obs;
  String get firstName => _firstName.value;
  set firstName(String value) => _firstName.value = value;

  final _lastName = ''.obs;
  String get lastName => _lastName.value;
  set lastName(String value) => _lastName.value = value;

  final _phoneNumber = ''.obs;
  String get phoneNumber => _phoneNumber.value;
  set phoneNumber(String value) => _phoneNumber.value = value;

  final _lastLogin = DateTime.now().obs;
  DateTime get lastLogin => _lastLogin.value;
  set lastLogin(DateTime value) => _lastLogin.value = value;

  final _userProfileUrl = ''.obs;
  String get userProfileUrl => _userProfileUrl.value;
  set userProfileUrl(String value) => _userProfileUrl.value = value;


  final _activateType = 0.obs;
  int get activateType => _activateType.value;
  set activateType(int value) => _activateType.value = value;

  final _isRefundable = ''.obs;
  String get isRefundable => _isRefundable.value;
  set isRefundable(String value) => _isRefundable.value = value;

  final _refundLimited = ''.obs;
  String get refundLimited => _refundLimited.value;
  set refundLimited(String value) => _refundLimited.value = value;

  final _pinCode = ''.obs;
  String get pinCode => _pinCode.value;
  set pinCode(String value) => _pinCode.value = value;

  final _profileId = ''.obs;
  String get profileId => _profileId.value;
  set profileId(String value) => _profileId.value = value;
}
