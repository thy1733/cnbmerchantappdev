import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';

class ApiInputModel {
  String tXNID = "";
  String dEVICEINFO = "";
  String mCID = "";
  String sESSIONID = "";
  String rEQDATA = "";

  static ApiInputModel create(String request) {
    var info = DeviceInfoTempInstant.deviceInfo;
    var deviceInfo = "${info.machine}!${info.systemName}|${info.systemVersion}|${info.model}!${info.nodename}!${info.isPhysicalDevice}|${info.identifier}";
    var result = ApiInputModel();
    var txnId = DateTime.now().toFormatDateString("yyyymmddhhmmsss");
    debugPrint("txnId:---->$txnId");
    result.tXNID = txnId;
    result.dEVICEINFO = deviceInfo.replaceAll(",", ".");
    result.sESSIONID = "YEMSJD<JE<JFHEMHHHDHDJ";
    result.rEQDATA = request;
    return result;
  }
}
