
import 'package:get/get.dart';

class AppSettingModel{
  
  final _inactivityTimeoutSeconds = 30.obs;
  int get inactivityTimeoutSeccond => _inactivityTimeoutSeconds.value;
  set inactivityTimeoutSeccond(int value) => _inactivityTimeoutSeconds.value = value;
  

  String userSettingId = "";
}

class AppShieldingTemp{
  
  static final _isShowStatusbar = false.obs;
  static bool get isShowStatusbar => _isShowStatusbar.value;
  static set isShowStatusbar(bool value) => _isShowStatusbar.value = value;
  
  
  static final _info = ''.obs;
  static String get info => _info.value;
  static set info(String value) => _info.value = value;
  
}