import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ItemSelectOptionModel {
  final _id = ''.obs;
  String get id => _id.value;
  set id(String value) => _id.value = value;

  final _index = 0.obs;
  int get index => _index.value;
  set index(int value) => _index.value = value;

  final _imageSvg = SvgAppAssets.setupIcon.obs;
  String get imageSvg => _imageSvg.value;
  set imageSvg(String value) => _imageSvg.value = value;

  final _name = ''.obs;
  String get name => _name.value;
  set name(String value) => _name.value = value;

  final _photoUrl = ''.obs;
  String get photoUrl => _photoUrl.value;
  set photoUrl(String value) => _photoUrl.value = value;

  final _isSelected = false.obs;
  bool get isSelected => _isSelected.value;
  set isSelected(bool value) => _isSelected.value = value;

  Widget? leftWidget;
  dynamic data;

  final _timeCount = 0.obs;
  int get timeCount => _timeCount.value;
  set timeCount(int value) => _timeCount.value = value;

  static ItemSelectOptionModel create(int index, String name, bool selected,
      {int time = 0,
      dynamic data,
      String imageSvg = SvgAppAssets.setupIcon,
      Widget? leftWidget}) {
    var result = ItemSelectOptionModel();
    result.index = index;
    result.name = name;
    result.data = data;
    result.isSelected = selected;
    result.leftWidget = leftWidget;
    result.imageSvg = imageSvg;
    result.timeCount = time;
    return result;
  }

  static ItemSelectOptionModel createItem(
      String id, String name, bool selected) {
    var result = ItemSelectOptionModel();
    result.id = id;
    result.name = name;
    result.isSelected = selected;
    return result;
  }

  static ItemSelectOptionModel createItemData(CashierItemListOutputModel item,bool isSelected) {
    var result = ItemSelectOptionModel();
    result.id = item.id;
    result.photoUrl = item.photoUrl;
    result.name = "${item.firstName} ${item.lastName}";
    result.data = item;
    result.isSelected = isSelected;
    return result;
  }
}
