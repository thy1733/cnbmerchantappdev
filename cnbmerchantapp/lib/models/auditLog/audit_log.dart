import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cnbmerchantapp/core.dart';

class AuditLog {
  String mCID = "";
  String businessId = "";
  Timestamp createdOn = Timestamp.now();
  String responseEn = "";
  String responseDe = "";
  String requestEn = "";
  String requestDe = "";
  String transactionId = "";
  String cashierId = "";

  AuditLog();

  factory AuditLog.fromJson(Map<String, dynamic> json) {
    final auditLog = AuditLog();
    auditLog.mCID = json["mCID"] as String;
    auditLog.businessId = json["businessId"] as String;
    auditLog.createdOn = json["createdOn"] as Timestamp;
    auditLog.responseEn = json["encryptedResponse"] as String;
    auditLog.responseDe = json["decryptedResponse"] as String;
    return auditLog;
  }

  Map<String, dynamic> toJson() => _auditLogToJson(this);

  Map<String, dynamic> _auditLogToJson(AuditLog instance) => <String, dynamic>{
        "mCID": AppUserTempInstant.appCacheUser.mCID,
        "businessId": AppUserTempInstant.appCacheUser.accountNumber,
        "transactionId": AppUserTempInstant.transactionId,
        "cashierId": AppUserTempInstant.appCacheUser.inviteCode,
        "createdOn": instance.createdOn,
        "responseEn": instance.responseEn,
        "responseDe": instance.responseDe,
        "requestEn": instance.requestEn,
        "requestDe": instance.requestDe,
      };
}
