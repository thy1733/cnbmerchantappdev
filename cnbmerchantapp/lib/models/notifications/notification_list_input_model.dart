import 'package:get/get.dart';

class NotificationListInputModel {
  final _mCID = "".obs;
  String get mCID => _mCID.value;
  set mCID(String value) => _mCID.value = value;

  final _lastItemCount = 0.obs;
  int get lastItemCount => _lastItemCount.value;
  set lastItemCount(int value) => _lastItemCount.value = value;

  final _limit = 0.obs;
  int get limit => _limit.value;
  set limit(int value) => _limit.value = value;
}
