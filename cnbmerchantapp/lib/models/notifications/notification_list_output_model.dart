import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class NotificationListOutputModel {
  final _totalCount = "".obs;
  String get totalCount => _totalCount.value;
  set totalCount(String value) => _totalCount.value = value;

  final _items = <NotificationItemOutputModel>[].obs;
  List<NotificationItemOutputModel> get items => _items;
  set items(List<NotificationItemOutputModel> values) => _items.value = values;

  NotificationListOutputModel();

  factory NotificationListOutputModel.create(
      NotificationItemListOutputDto dto) {
    var result = NotificationListOutputModel();
    result.totalCount = dto.totalCount;
    result.items =
        dto.items.map((e) => NotificationItemOutputModel.create(e)).toList();
    return result;
  }
}
