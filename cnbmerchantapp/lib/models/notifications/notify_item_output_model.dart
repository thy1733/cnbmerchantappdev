import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class NotifyItemOutputModel {
  
  final _acceptedVia = ''.obs;
  String get acceptedVia => _acceptedVia.value;
  set acceptedVia(String value) => _acceptedVia.value = value;
  
  final _amount = ''.obs;
  String get amount => _amount.value;
  set amount(String value) => _amount.value = value;
  
  final _businessId = ''.obs;
  String get businessId => _businessId.value;
  set businessId(String value) => _businessId.value = value;
  
  final _cashierId = ''.obs;
  String get cashierId => _cashierId.value;
  set cashierId(String value) => _cashierId.value = value;
  
  final _cashierName = ''.obs;
  String get cashierName => _cashierName.value;
  set cashierName(String value) => _cashierName.value = value;
  
  final _clientName = ''.obs;
  String get clientName => _clientName.value;
  set clientName(String value) => _clientName.value = value;
  
  final _currencyCode = ''.obs;
  String get currencyCode => _currencyCode.value;
  set currencyCode(String value) => _currencyCode.value = value;
  
  final _customerAc = ''.obs;
  String get customerAc => _customerAc.value;
  set customerAc(String value) => _customerAc.value = value;
  
  final _customerName = ''.obs;
  String get customerName => _customerName.value;
  set customerName(String value) => _customerName.value = value;
  
  final _dateTime = ''.obs;
  String get dateTime => _dateTime.value;
  set dateTime(String value) => _dateTime.value = value;
  
  final _txnDateTime = DateTime.now().obs;
  DateTime get txnDateTime => _txnDateTime.value;
  set txnDateTime(DateTime value) => _txnDateTime.value = value;

  final _discount = ''.obs;
  String get discount => _discount.value;
  set discount(String value) => _discount.value = value;
  
  final _isAllowPrint = ''.obs;
  String get isAllowPrint => _isAllowPrint.value;
  set isAllowPrint(String value) => _isAllowPrint.value = value;
  
  final _outletId = ''.obs;
  String get outletId => _outletId.value;
  set outletId(String value) => _outletId.value = value;
  
  final _paidAmount = ''.obs;
  String get paidAmount => _paidAmount.value;
  set paidAmount(String value) => _paidAmount.value = value;
  
  final _payType = ''.obs;
  String get payType => _payType.value;
  set payType(String value) => _payType.value = value;
  
  final _paymentMethod = ''.obs;
  String get paymentMethod => _paymentMethod.value;
  set paymentMethod(String value) => _paymentMethod.value = value;
  
  final _printCount = ''.obs;
  String get printCount => _printCount.value;
  set printCount(String value) => _printCount.value = value;
  
  final _status = ''.obs;
  String get status => _status.value;
  set status(String value) => _status.value = value;
  
  final _statusMessage = ''.obs;
  String get statusMessage => _statusMessage.value;
  set statusMessage(String value) => _statusMessage.value = value;
  
  final _tip = ''.obs;
  String get tip => _tip.value;
  set tip(String value) => _tip.value = value;
  
  final _txrId = ''.obs;
  String get txrId => _txrId.value;
  set txrId(String value) => _txrId.value = value;
  
  final _bankref = ''.obs;
  String get bankref => _bankref.value;
  set bankref(String value) => _bankref.value = value;

  static NotifyItemOutputModel create(NotifyItemOutputDto dto){
    var result = NotifyItemOutputModel();
    result.acceptedVia = dto.acceptedVia!;
    result.amount = dto.amount!;
    result.businessId = dto.businessId!;
    result.cashierId = dto.cashierId!;
    result.cashierName = dto.cashierName!;
    result.clientName = dto.clientName!;
    result.currencyCode = dto.currencyCode!;
    result.customerAc = dto.customerAc!;
    result.customerName = dto.customerName!;
    result.dateTime = dto.dateTime!;
    result.txnDateTime = dto.dateTime!.fromTxnFormatToLocalDateTime();
    result.discount = dto.discount!;
    result.isAllowPrint = dto.isAllowPrint!;
    result.outletId = dto.outletId!;
    result.paidAmount = dto.paidAmount!;
    result.payType = dto.payType!;
    result.paymentMethod = dto.paymentMethod!;
    result.status = dto.status!;
    result.statusMessage = dto.statusMessage!;
    result.tip = dto.tip!;
    result.txrId = dto.txrId!;
    result.bankref = dto.bankref!;
    return result;
  }
}
