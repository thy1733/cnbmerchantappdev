import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/foundation.dart';
import 'package:get/get.dart';
/*enum _TransactionType{
    receive,
    refund
}*/

class NotificationItemOutputModel {
  final _transactionId = "".obs;
  String get transactionId => _transactionId.value;
  set transactionId(String value) => _transactionId.value = value;

  final _accountName = "".obs;
  String get accountName => _accountName.value;
  set accountName(String value) => _accountName.value = value;

  final _dateTime = "".obs;
  String get dateTime => _dateTime.value;
  set dateTime(String value) => _dateTime.value = value;

  final _currency = "".obs;
  String get currency => _currency.value;
  set currency(String value) => _currency.value = value;

  final _amount = 0.0.obs;
  double get amount => _amount.value;
  set amount(double value) => _amount.value = value;

  final _receiverAccountNumber = "".obs;
  String get receiverAccountNumber => _receiverAccountNumber.value;
  set receiverAccountNumber(String value) => _receiverAccountNumber.value = value;

  final _transactionType =
      NotificationTransactionType.receive.obs;
  NotificationTransactionType get transactionType => _transactionType.value;
  set transactionType(NotificationTransactionType value) =>
      _transactionType.value = value;

  final _paymentMethod =
      NotificationTransactionPaymentType.khqr.obs;
  NotificationTransactionPaymentType get paymentMethod => _paymentMethod.value;
  set paymentMethod(NotificationTransactionPaymentType value) =>
      _paymentMethod.value = value;

  final _status =
      NotificationTransactionStatusType.failed.obs;
  NotificationTransactionStatusType get status => _status.value;
  set status(NotificationTransactionStatusType value) => _status.value = value;

  final _isRead = "".obs;
  String get isRead => _isRead.value;
  set isRead(String value) => _isRead.value = value;

  NotificationItemOutputModel();

  factory NotificationItemOutputModel.create(NotificationListOutputDto dto) {
    var data = NotificationItemOutputModel();
    data.transactionId = dto.transactionId;
    data.accountName = dto.accountName;
    data.dateTime = dto.dateTime;
    data.currency = dto.currency;
    data.receiverAccountNumber = dto.receiverAccountNumber;

    final transactionTypes = NotificationTransactionType.values
        .where((element) => describeEnum(element) == dto.transactionType);
    data.transactionType = transactionTypes.isEmpty
        ? NotificationTransactionType.receive
        : transactionTypes.first;

    final paymentType = NotificationTransactionPaymentType.values
        .where((element) => describeEnum(element) == dto.paymentMethod);
    data.paymentMethod = paymentType.isEmpty
        ? NotificationTransactionPaymentType.khqr
        : paymentType.first;

    final statuses = NotificationTransactionStatusType.values
        .where((element) => describeEnum(element) == dto.status);
    data.status = statuses.isEmpty
        ? NotificationTransactionStatusType.failed
        : statuses.first;

    data.isRead = dto.isRead;
    return data;
  }
}
