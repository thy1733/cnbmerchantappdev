class UserChangePasswordInputModel {
  String username = "";
  String currentPassword = "";
  String newPassword = "";
  String accountNumber = "";
  String inviteCode = "";
  UserChangePasswordInputModel({
    required this.username,
    required this.currentPassword,
    required this.newPassword,
    this.accountNumber = "",
    this.inviteCode = "",
  });
}
