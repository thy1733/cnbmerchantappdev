import 'package:cnbmerchantapp/core.dart';

class UserLoginOutputModel extends ResultStatusOutputModel {
  String cid = "";
  String accountNumber = "";

  UserLoginOutputModel();

  static UserLoginOutputModel create(UserLoginOutputDto dto) {
    var result = UserLoginOutputModel();
    result.statusCode = dto.statusCode;
    result.isSuccess = dto.isSuccess;
    result.cid = dto.cid;
    result.accountNumber = dto.accountNumber;
    result.description = dto.description;
    return result;
  }
}
