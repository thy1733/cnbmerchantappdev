import 'package:get/get.dart';

class UserResetPassOutputModel {
  final _statusCode = ''.obs;
  String get statusCode => _statusCode.value;
  set statusCode(String value) => _statusCode.value = value;

  final _description = ''.obs;
  String get description => _description.value;
  set description(String value) => _description.value = value;

  static UserResetPassOutputModel create(List<String> dto) {
    var result = UserResetPassOutputModel();
    result.description = dto[1];
    result.statusCode = dto[0];
    return result;
  }
}