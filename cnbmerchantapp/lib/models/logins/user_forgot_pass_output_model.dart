import 'package:cnbmerchantapp/core.dart';

class UserForgotPassOutputModel extends ResultStatusOutputModel {
  String phoneNumber = "";

  static UserForgotPassOutputModel create(UserForgotPassOutputDto dto) {
    var result = UserForgotPassOutputModel();
    result.phoneNumber = dto.phoneNumber;
    result.statusCode = dto.statusCode;
    result.description = dto.description;
    return result;
  }
}
