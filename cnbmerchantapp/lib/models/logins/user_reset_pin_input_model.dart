class UserResetPinInputModel {
  String mCID = "";
  String currentPassword = "";
  String newPIN = "";
  String userName = "";

  UserResetPinInputModel({
    required this.mCID,
    required this.currentPassword,
    required this.newPIN,
    required this.userName,
  });
}
