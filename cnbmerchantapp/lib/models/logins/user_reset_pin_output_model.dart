import 'package:cnbmerchantapp/core.dart';

class UserResetPinOutputModel extends ResultStatusOutputModel {
  static UserResetPinOutputModel create(UserResetPinOutputDto dto) {
    var result = UserResetPinOutputModel();
    result.statusCode = dto.statusCode;
    result.isSuccess = dto.isSuccess;
    result.description = dto.description;
    return result;
  }
}
