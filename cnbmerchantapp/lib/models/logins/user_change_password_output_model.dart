import 'package:cnbmerchantapp/core.dart';

class UserChangePasswordOutputModel extends ResultStatusOutputModel {
  UserChangePasswordOutputModel();
  static UserChangePasswordOutputModel create(UserChangePasswordOutputDto dto) {
    var result = UserChangePasswordOutputModel();
    result.statusCode = dto.statusCode;
    result.isSuccess = dto.isSuccess;
    result.description = dto.description;
    return result;
  }
}
