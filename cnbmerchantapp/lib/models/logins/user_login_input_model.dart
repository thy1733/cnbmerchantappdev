import 'package:get/get.dart';

class UserLoginInputModel {
  final _userName = ''.obs;
  String get userName => _userName.value;
  set userName(String value) => _userName.value = value;

  final _password = ''.obs;
  String get password => _password.value;
  set password(String value) => _password.value = value;

  final _userValidateMessage = ''.obs;
  String get userValidateMessage => _userValidateMessage.value;
  set userValidateMessage(String value) => _userValidateMessage.value = value;

  final _passwordValidateMessage = ''.obs;
  String get passwordValidateMessage => _passwordValidateMessage.value;
  set passwordValidateMessage(String value) =>
      _passwordValidateMessage.value = value;

  static UserLoginInputModel create(String user, String pass) {
    var result = UserLoginInputModel();
    result.userName = user;
    result.password = pass;
    return result;
  }
}
