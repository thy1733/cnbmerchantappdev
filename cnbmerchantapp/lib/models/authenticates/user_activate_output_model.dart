import 'package:cnbmerchantapp/core.dart';

class ActivateOutputModel extends ResultStatusOutputModel {
  String phoneNumber = "";

  ActivateOutputModel();

  static ActivateOutputModel create(ActivateOutputDto dto) {
    var result = ActivateOutputModel();
    result.statusCode = dto.statusCode;
    result.isSuccess = dto.isSuccess;
    result.description = dto.description;
    return result;
  }
}
