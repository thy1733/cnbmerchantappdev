import 'package:cnbmerchantapp/core.dart';

class GetAccountInfoListOutputModel extends ResultStatusOutputModel {
  List<GetAccountInfoItemOutputModel> items = [];

  static GetAccountInfoListOutputModel create(GetAccountInfoListOutputDto dto) {
    var result = GetAccountInfoListOutputModel();
    result.statusCode = dto.statusCode;
    result.isSuccess = dto.isSuccess;
    result.description = dto.description;

    result.items =
        dto.items.map((e) => GetAccountInfoItemOutputModel.create(e)).toList();
    return result;
  }
}
