import 'package:cnbmerchantapp/core.dart';

class CheckExistingAccountOutputModel extends ResultStatusOutputModel {
  bool isAlreadyExisted = false;

  static CheckExistingAccountOutputModel create(
      CheckExistingAccountOutputDto dto) {
    var result = CheckExistingAccountOutputModel();
    result.isAlreadyExisted = dto.isAlreadyExisted;
    result.statusCode = dto.statusCode;
    result.description = dto.description;
    return result;
  }
}
