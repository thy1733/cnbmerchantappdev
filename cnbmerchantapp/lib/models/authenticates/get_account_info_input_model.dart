
import 'package:get/get.dart';

class GetAccountInfoInputModel {
  String cidOrAccountNumber = "";
  
  final _merchantId = ''.obs;
  String get merchantId => _merchantId.value;
  set merchantId(String value) => _merchantId.value = value;
  
}
