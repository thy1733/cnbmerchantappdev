import 'package:cnbmerchantapp/core.dart';
class ResetPasswordOutputModel extends ResultStatusOutputModel {

  static ResetPasswordOutputModel create(ResetPasswordOutputDto dto) {
    var result = ResetPasswordOutputModel();
    result.statusCode = dto.statusCode;
    result.description = dto.description;
    return result;
  }
}
