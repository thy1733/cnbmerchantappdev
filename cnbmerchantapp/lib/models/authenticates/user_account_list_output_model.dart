import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class AccountListOutputModel{
  
  final _items = <AccountItemListOutputModel>[].obs;
  List<AccountItemListOutputModel> get items => _items;
  set items(List<AccountItemListOutputModel> value) => _items.value = value;
   
  static AccountListOutputModel create(AccountItemListOutputDto dto){
    var result = AccountListOutputModel();
    result.items = dto.items.map((e) => AccountItemListOutputModel.create(e)).toList();
    return result;
  }
  
}