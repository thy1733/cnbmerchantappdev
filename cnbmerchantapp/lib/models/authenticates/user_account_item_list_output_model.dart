import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class AccountItemListOutputModel {
  final _accountName = ''.obs;
  String get accountName => _accountName.value;
  set accountName(String value) => _accountName.value = value;

  final _accountNumber = ''.obs;
  String get accountNumber => _accountNumber.value;
  set accountNumber(String value) => _accountNumber.value = value;

  final _currency = ''.obs;
  String get currency => _currency.value;
  set currency(String value) => _currency.value = value;

  AccountItemListOutputModel(
      {String accountName = "",
      String accountNumber = "",
      String currency = ""}) {
    this.accountName = accountName;
    this.accountNumber = accountNumber;
    this.currency = currency;
  }

  static AccountItemListOutputModel create(AccountListOutputDto dto) {
    var result = AccountItemListOutputModel();
    result.accountName = dto.acountName;
    result.accountNumber = dto.accountNumber;
    result.currency = dto.currency;
    return result;
  }

  static List<AccountItemListOutputModel> generate() {
    return List.generate(
        5,
        (index) => AccountItemListOutputModel(
            accountName: "Sample Acc - $index",
            accountNumber: "001000506178$index",
            currency: "USD"));
  }
}
