import 'package:get/get.dart';

class AccountListInputModel{
  final _activateNumber = ''.obs;
  String get activateNumber => _activateNumber.value;
  set activateNumber(String value) => _activateNumber.value = value;
  
  final _phoneNumber = ''.obs;
  String get phoneNumber => _phoneNumber.value;
  set phoneNumber(String value) => _phoneNumber.value = value;
  
  final _activateType = 0.obs;
  int get activateType => _activateType.value;
  set activateType(int value) => _activateType.value = value;
}