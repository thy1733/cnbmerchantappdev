import 'package:cnbmerchantapp/core.dart';

class GetAccountInfoItemOutputModel {
  String accountName = "";
  String accountNumber = "";
  String currency = "";
  String totalBalance = "";

  static GetAccountInfoItemOutputModel create(GetAccountInfoItemOutputDto dto) {
    var result = GetAccountInfoItemOutputModel();
    result.accountName = dto.accountName;
    result.accountNumber = dto.accountNumber;
    result.currency = dto.currency;
    result.totalBalance = dto.totalBalance;
    return result;
  }
}
