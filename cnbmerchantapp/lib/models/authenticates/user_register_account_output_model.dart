import 'package:cnbmerchantapp/core.dart';

class RegisterAccountOuputModel extends ResultStatusOutputModel {
  bool canLogin = false;
  String mCID = "";

  static RegisterAccountOuputModel create(RegisterAccountOuputDto dto) {
    var result = RegisterAccountOuputModel();
    result.canLogin = dto.canLogin;
    result.statusCode = dto.statusCode;
    result.description = dto.description;
    result.mCID = dto.mCID;
    return result;
  }
}
