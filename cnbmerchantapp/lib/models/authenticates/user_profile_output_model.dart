import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class UserProfileOutputModel extends ResultStatusOutputModel {
  final _firstName = ''.obs;
  String get firstName => _firstName.value;
  set firstName(String value) => _firstName.value = value;

  final _lastName = ''.obs;
  String get lastName => _lastName.value;
  set lastName(String value) => _lastName.value = value;

  final _lastLoginDateTime = DateTime.now().obs;
  DateTime get lastLoginDateTime => _lastLoginDateTime.value;
  set lastLoginDateTime(DateTime value) => _lastLoginDateTime.value = value;

  final _profilePhotoId = ''.obs;
  String get profilePhotoId => _profilePhotoId.value;
  set profilePhotoId(String value) => _profilePhotoId.value = value;

  final _profileUrl = ''.obs;
  String get profileUrl => _profileUrl.value;
  set profileUrl(String value) => _profileUrl.value = value;

  final _activateType = ''.obs;
  String get activateType => _activateType.value;
  set activateType(String value) => _activateType.value = value;

  final _isRefundable = ''.obs;
  String get isRefundable => _isRefundable.value;
  set isRefundable(String value) => _isRefundable.value = value;

  final _refundLimited = ''.obs;
  String get refundLimited => _refundLimited.value;
  set refundLimited(String value) => _refundLimited.value = value;

  final _phoneNumber = ''.obs;
  String get phoneNumber => _phoneNumber.value;
  set phoneNumber(String value) => _phoneNumber.value = value;

  final _mCID = ''.obs;
  String get mCID => _mCID.value;
  set mCID(String value) => _mCID.value = value;

  final _displayName = ''.obs;
  String get displayName => _displayName.value;
  set displayName(String value) => _displayName.value = value;

  final _pinCode = ''.obs;
  String get pinCode => _pinCode.value;
  set pinCode(String value) => _pinCode.value = value;

  final _profileId = ''.obs;
  String get profileId => _profileId.value;
  set profileId(String value) => _profileId.value = value;

  final _businessId = ''.obs;
  String get businessId => _businessId.value;
  set businessId(String value) => _businessId.value = value;

  final _telegramToken = ''.obs;
  String get telegramToken => _telegramToken.value;
  set telegramToken(String value) => _telegramToken.value = value;

  static UserProfileOutputModel create(UserProfileOutputDto dto) {
    var result = UserProfileOutputModel();
    result.statusCode = dto.statusCode;
    result.description = dto.description;
    result.firstName = dto.firstName;
    result.lastName = dto.lastName;
    result.lastLoginDateTime =
        DateTime.tryParse(dto.lastLoginDateTime) ?? result.lastLoginDateTime;
    result.profilePhotoId = dto.profilePhotoId;
    result.profileUrl = dto.profilePhotoId.downloadUrl(
        UploadProfileType.userProfile,
        dto.activateType == "0" ? dto.mCID : dto.profileId,
        dto.profilePhotoId);
    result.activateType = dto.activateType;
    result.isRefundable = dto.isRefundable;
    result.refundLimited = dto.refundLimited;
    result.phoneNumber = dto.phoneNumber;
    result.mCID = dto.mCID;
    result.displayName = dto.displayName;
    result.pinCode = dto.pinCode;
    result.profileId = dto.profileId;
    result.businessId = dto.businessId;
    result.telegramToken = dto.telegramToken;
    return result;
  }
}
