import 'package:cnbmerchantapp/core.dart';

class CheckExistingUsernameOutputModel extends ResultStatusOutputModel {
  bool isAlreadyExisted = false;

  static CheckExistingUsernameOutputModel create(
      CheckExistingUsernameOutputDto dto) {
    var result = CheckExistingUsernameOutputModel();
    result.isAlreadyExisted = dto.isAlreadyExisted;
    result.statusCode = dto.statusCode;
    result.description = dto.description;
    return result;
  }
}
