import 'package:cnbmerchantapp/core.dart';

class VerifyOtpOutputModel extends ResultStatusOutputModel {
  bool canRegister = false;
  String cidOrAccountNumber= "";
  static VerifyOtpOutputModel create(VerifyOtpOutputDto dto) {
    var result = VerifyOtpOutputModel();
    result.canRegister = dto.canRegister;
    result.statusCode = dto.statusCode;
    result.description = dto.description;
    result.cidOrAccountNumber = dto.cidOrAccountNumber;
    return result;
  }
}
