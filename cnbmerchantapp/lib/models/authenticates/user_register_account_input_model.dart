class RegisterAccountInputModel {
  String cid = "";
  String accountNumber = "";
  String phoneNumber = "";
  String userName = "";
  String password = "";
  String firstName = "";
  String lastName = "";
  String optCode = "";
  String pinCode = "";
  String nationalId = "";
  String inviteCode = "";
  String merchantId = ""; // for existng merchant flow use this id to get account under this merchant only
}
