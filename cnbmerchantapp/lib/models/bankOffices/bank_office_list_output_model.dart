import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class BankOfficeListOutputModel {
  BankOfficeListOutputModel();

  final _items = <BankOfficeOutputModel>[].obs;
  List<BankOfficeOutputModel> get items => _items;
  set items(List<BankOfficeOutputModel> value) => _items.value = value;

  static BankOfficeListOutputModel create(BankOfficeListOutputDto dto) {
    var result = BankOfficeListOutputModel();
    result.items =
        dto.items.map((e) => BankOfficeOutputModel.create(e)).toList();
    return result;
  }
}
