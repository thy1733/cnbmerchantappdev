import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class BankOfficeOutputModel {
  final _id = ''.obs;
  String get id => _id.value;
  set id(String value) => _id.value = value;

  final _locationName = ''.obs;
  String get locationName => _locationName.value;
  set locationName(String value) => _locationName.value = value;

  final _locationAddress = ''.obs;
  String get locationAddress => _locationAddress.value;
  set locationAddress(String value) => _locationAddress.value = value;

  final _latitude = 0.0.obs;
  double get latitude => _latitude.value;
  set latitude(double value) => _latitude.value = value;

  final _longitude = 0.0.obs;
  double get longitude => _longitude.value;
  set longitude(double value) => _longitude.value = value;

  final _startWorkingHour = 0.obs;
  int get startWorkingHour => _startWorkingHour.value;
  set startWorkingHour(int value) => _startWorkingHour.value = value;

  final _stopWorkingHour = 0.obs;
  int get stopWorkingHour => _stopWorkingHour.value;
  set stopWorkingHour(int value) => _stopWorkingHour.value = value;

  bool get isOpen =>
      DateTime.now().hour > startWorkingHour &&
      DateTime.now().hour < stopWorkingHour;

  BankOfficeOutputModel({
    String id = '',
    String locationName = '',
    String locationAddress = '',
    double latitude = 0.0,
    double longitude = 0.0,
    int startWorkingHour = 9,
    int stopWorkingHour = 18,
  }) {
    this.id = id;
    this.locationName = locationName;
    this.locationAddress = locationAddress;
    this.latitude = latitude;
    this.longitude = longitude;
    this.startWorkingHour = startWorkingHour;
    this.stopWorkingHour = stopWorkingHour;
  }

  static BankOfficeOutputModel create(BankOfficeOutputDto dto) {
    var result = BankOfficeOutputModel(
      id: dto.id,
      locationName: dto.locationName,
      locationAddress: dto.locationAddress,
      latitude: dto.latitude,
      longitude: dto.longitude,
      startWorkingHour: dto.startWorkingHour,
      stopWorkingHour: dto.stopWorkingHour,
    );
    return result;
  }

  ///////////////////
  static List<BankOfficeOutputModel> sampleList() => <BankOfficeOutputModel>[
        BankOfficeOutputModel(
          id: "1",
          locationName: "Head Office",
          locationAddress: "phnom Penh 1",
          latitude: 11.549164,
          longitude: 104.911742,
        ),
        BankOfficeOutputModel(
          id: "2",
          locationName: "Tuol Kok",
          locationAddress: "phnom Penh 2",
          latitude: 11.039164,
          longitude: 105.311742,
        ),
        BankOfficeOutputModel(
          id: "1",
          locationName: "Sen Sok",
          locationAddress: "Sen Sok",
          latitude: 11.749164,
          longitude: 103.911742,
        ),
      ];
}
