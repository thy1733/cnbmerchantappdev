import 'package:get/get.dart';

class BankOfficeListInputModel {
  final _locationName = ''.obs;
  String get locationName => _locationName.value;
  set locationName(String value) => _locationName.value = value;

  BankOfficeListInputModel({String locationName=""}) {
    this.locationName = locationName;
  }
}
