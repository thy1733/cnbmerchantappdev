import 'bankOffices/bank_office_output_model.dart';

typedef OnVerifyOTPSuccess = void Function(String otpCode, String cidOrAccountNumber);
typedef BrowseBankBranchLocationCallBack = Function(
    BankOfficeOutputModel result);
