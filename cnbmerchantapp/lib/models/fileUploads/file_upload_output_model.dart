import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class FileUploadOutputModel {
  
  final _fileName = ''.obs;
  String get fileName => _fileName.value;
  set fileName(String value) => _fileName.value = value;
  
  final _filePath = ''.obs;
  String get filePath => _filePath.value;
  set filePath(String value) => _filePath.value = value;
  
  static FileUploadOutputModel create(FileUploadOutputDto dto){
    var result = FileUploadOutputModel();
    result.fileName = dto.fileName;
    result.filePath = dto.filePath;
    return result;
  }
}