class CanadiaSupportModel {
  final String icon;
  final String title;
  final String subtitle;
  CanadiaSupportModel({
    required this.icon,
    required this.title,
    required this.subtitle,
  });
}
