import 'package:cnbmerchantapp/core.dart';

class FeedbackOutputModel extends IndexModel {
  FeedbackOutputModel();

  static FeedbackOutputModel create(FeedbackOutputDto dto) {
    var result = FeedbackOutputModel();
    result.statusCode = dto.statusCode;
    result.isSuccess = dto.isSuccess;
    return result;
  }
}
