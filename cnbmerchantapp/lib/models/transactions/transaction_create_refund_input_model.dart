import 'package:get/get.dart';

class TransactionCreateRefundInputModel {
  final _transactionId = ''.obs;
  String get transactionId => _transactionId.value;
  set transactionId(String value) => _transactionId.value = value;

  final _amountToRefund = 0.0.obs;
  double get amountToRefund => _amountToRefund.value;
  set amountToRefund(double value) => _amountToRefund.value = value;

  final _originalAmount = 0.0.obs;
  double get originalAmount => _originalAmount.value;
  set originalAmount(double value) => _originalAmount.value = value;

  final _remark = ''.obs;
  String get remark => _remark.value;
  set remark(String value) => _remark.value = value;

  final _businessId = ''.obs;
  String get businessId => _businessId.value;
  set businessId(String value) => _businessId.value = value;

  final _outletId = ''.obs;
  String get outletId => _outletId.value;
  set outletId(String value) => _outletId.value = value;

  final _profileId = ''.obs;
  String get profileId => _profileId.value;
  set profileId(String value) => _profileId.value = value;
}
