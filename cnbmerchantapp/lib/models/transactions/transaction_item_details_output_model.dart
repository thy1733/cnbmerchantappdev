import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class TransactionItemDetailsOutputModel extends IndexModel {
  final _transactionId = ''.obs;
  String get transactionId => _transactionId.value;
  set transactionId(String value) => _transactionId.value = value;

  final _referalTransactionId = ''.obs;
  String get referalTransactionId => _referalTransactionId.value;
  set referalTransactionId(String value) => _referalTransactionId.value = value;

  final _outletName = ''.obs;
  String get outletName => _outletName.value;
  set outletName(String value) => _outletName.value = value;

  final _transactionDateText = ''.obs;
  String get transactionDateText => _transactionDateText.value;
  set transactionDateText(String value) => _transactionDateText.value = value;

  final _transactionDate = DateTime.now().obs;
  DateTime get transactionDate => _transactionDate.value;
  set transactionDate(DateTime value) => _transactionDate.value = value;

  final _paymentProcessedBy = ''.obs;
  String get paymentProcessedBy => _paymentProcessedBy.value;
  set paymentProcessedBy(String value) => _paymentProcessedBy.value = value;

  final _paymentMethodName = ''.obs;
  String get paymentMethodName => _paymentMethodName.value;
  set paymentMethodName(String value) => _paymentMethodName.value = value;

  final _customerName = ''.obs;
  String get customerName => _customerName.value;
  set customerName(String value) => _customerName.value = value;

  final _customerBankName = ''.obs;
  String get customerBankName => _customerBankName.value;
  set customerBankName(String value) => _customerBankName.value = value;

  final _customerAccount = ''.obs;
  String get customerAccount => _customerAccount.value;
  set customerAccount(String value) => _customerAccount.value = value;

  final _currency = ''.obs;
  String get currency => _currency.value;
  set currency(String value) => _currency.value = value;

  final _amount = 0.0.obs;
  double get amount => _amount.value;
  set amount(double value) => _amount.value = value;

  final _transactionType = "".obs;
  String get transactionType => _transactionType.value;
  set transactionType(String value) => _transactionType.value = value;

  final _status = "".obs;
  String get status => _status.value;
  set status(String value) => _status.value = value;

  final _businessName = ''.obs;
  String get businessName => _businessName.value;
  set businessName(String value) => _businessName.value = value;

  final _remark = ''.obs;
  String get remark => _remark.value;
  set remark(String value) => _remark.value = value;

  final _items = <TransactionRefundItemListOutputModel>[].obs;
  // ignore: invalid_use_of_protected_member
  List<TransactionRefundItemListOutputModel> get items => _items.value;
  set items(List<TransactionRefundItemListOutputModel> value) =>
      _items.value = value;

  final _bakongHash = ''.obs;
  String get bakongHash => _bakongHash.value;
  set bakongHash(String value) => _bakongHash.value = value;

  final _availableRefundAmount = 0.0.obs;
  double get availableRefundAmount => _availableRefundAmount.value;
  set availableRefundAmount(double value) =>
      _availableRefundAmount.value = value;

  final _isAvailableRefundAmount = false.obs;
  bool get isAvailableRefundAmount => _isAvailableRefundAmount.value;
  set isAvailableRefundAmount(bool value) =>
      _isAvailableRefundAmount.value = value;

  final _allowRefundDuration = 48.obs;
  int get allowRefundDuration => _allowRefundDuration.value;
  set allowRefundDuration(int value) => _allowRefundDuration.value = value;

  static TransactionItemDetailsOutputModel create(
      TransactionItemDetailsOutputDto dto) {
    var result = TransactionItemDetailsOutputModel();
    result.statusCode = dto.statusCode;
    result.transactionId = dto.transactionId;
    result.referalTransactionId = dto.referalTransactionId;
    result.customerAccount = dto.customerAccount;
    result.customerBankName = dto.customerBankName;
    result.customerName = dto.customerName;
    result.transactionDate = dto.transactionDate;
    result.transactionDateText = dto.transactionDateText;
    result.currency = dto.currency;
    result.amount = dto.amount;
    result.transactionType = dto.transactionType;
    result.paymentMethodName = dto.paymentMethodName;
    result.status = dto.status;
    result.businessName = dto.businessName;
    result.outletName = dto.outletName;
    result.paymentProcessedBy = dto.paymentProcessedBy;
    result.remark = dto.remark;
    result.items = dto.items.reversed
        .map((e) => TransactionRefundItemListOutputModel.create(e))
        .toList();
    result.bakongHash = dto.bakongHash;
    result.availableRefundAmount = dto.availableRefundAmount;
    result.isAvailableRefundAmount = dto.availableRefundAmount > 0.0;
    result.allowRefundDuration = dto.allowRefundDuration;
    return result;
  }
}
