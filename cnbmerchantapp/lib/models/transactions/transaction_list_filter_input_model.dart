import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class TransactionListFilterInputModel {
  TransactionListFilterInputModel._();

  factory TransactionListFilterInputModel.initial() {
    final date = DatePeriodModel();

    date.fromDate = DateTime.tryParse(
            AppBusinessTempInstant.selectedBusiness.creationDate) ??
        DateTime.now();
    final filterTransaction = TransactionListFilterInputModel._()
      ..businessId = AppBusinessTempInstant.selectedBusiness.id
      ..outletId = ""
      ..cashierId = ""
      ..dateId = 0
      ..date = date
      ..transactionTypeId = 0
      ..transactionTypeValue = ""
      ..paymentMethodId = 0
      ..paymentMethodValue = ""
      ..skipCount = "1"
      ..takeCount = "10"
      ..hasFilter = false;
    return filterTransaction;
  }

  final _filter = ''.obs;
  String get filter => _filter.value;
  set filter(String value) => _filter.value = value;

  String get dateForDisplay => dateId == 0
      ? "AllPeriod".tr
      : DatePeriodOption.values[dateId] != DatePeriodOption.custom
          ? DatePeriodOption.values[dateId].name
          : "${kDateTimeFormat(date.fromDate, showStandardDate: true)} - ${kDateTimeFormat(date.toDate, showStandardDate: true)}";

  final _date = DatePeriodModel().obs;
  DatePeriodModel get date => _date.value;
  set date(DatePeriodModel value) => _date.value = value;

  final _dateId = 0.obs;
  int get dateId => _dateId.value;
  set dateId(int value) => _dateId.value = value;

  final _outletId = "".obs;
  String get outletId => _outletId.value;
  set outletId(String value) => _outletId.value = value;

  final _cashierId = "".obs;
  String get cashierId => _cashierId.value;
  set cashierId(String value) => _cashierId.value = value;

  final _paymentMethodId = 0.obs;
  int get paymentMethodId => _paymentMethodId.value;
  set paymentMethodId(int value) => _paymentMethodId.value = value;

  final _transactionTypeId = 0.obs;
  int get transactionTypeId => _transactionTypeId.value;
  set transactionTypeId(int value) => _transactionTypeId.value = value;

  final _businessId = "".obs;
  String get businessId => _businessId.value;
  set businessId(String value) => _businessId.value = value;

  final _paymentMethodValue = "".obs;
  String get paymentMethodValue => _paymentMethodValue.value;
  set paymentMethodValue(String value) => _paymentMethodValue.value = value;

  final _transactionTypeValue = "".obs;
  String get transactionTypeValue => _transactionTypeValue.value;
  set transactionTypeValue(String value) => _transactionTypeValue.value = value;

  final _skipCount = "".obs;
  String get skipCount => _skipCount.value;
  set skipCount(String value) => _skipCount.value = value;

  final _takeCount = "".obs;
  String get takeCount => _takeCount.value;
  set takeCount(String value) => _takeCount.value = value;

  final _hasFilter = false.obs;
  bool get hasFilter => _hasFilter.value;
  set hasFilter(bool value) => _hasFilter.value = value;
}
