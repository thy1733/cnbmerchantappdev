
import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class DownloadReportTrxItemOutputModel {
  final _merchantId = ''.obs;
  String get merchantId => _merchantId.value;
  set merchantId(String value) => _merchantId.value = value;

  final _transactionDate = DateTime.now().obs;
  DateTime get transactionDate => _transactionDate.value;
  set transactionDate(DateTime value) => _transactionDate.value = value;

  final _transactionDateText = ''.obs;
  String get transactionDateText => _transactionDateText.value;
  set transactionDateText(String value) => _transactionDateText.value = value;

  final _transactionId = ''.obs;
  String get transactionId => _transactionId.value;
  set transactionId(String value) => _transactionId.value = value;

  final _outletName = ''.obs;
  String get outletName => _outletName.value;
  set outletName(String value) => _outletName.value = value;

  final _merchantAccName = ''.obs;
  String get merchantAccName => _merchantAccName.value;
  set merchantAccName(String value) => _merchantAccName.value = value;

  final _merchantAccount = ''.obs;
  String get merchantAccount => _merchantAccount.value;
  set merchantAccount(String value) => _merchantAccount.value = value;

  final _amount = 0.0.obs;
  double get amount => _amount.value;
  set amount(double value) => _amount.value = value;

  final _currency = ''.obs;
  String get currency => _currency.value;
  set currency(String value) => _currency.value = value;

  final _customerBankName = ''.obs;
  String get customerBankName => _customerBankName.value;
  set customerBankName(String value) => _customerBankName.value = value;

  final _customerName = ''.obs;
  String get customerAccName => _customerName.value;
  set customerAccName(String value) => _customerName.value = value;

  final _customerAccount = ''.obs;
  String get customerAccount => _customerAccount.value;
  set customerAccount(String value) => _customerAccount.value = value;

  final _paymentMethodName = ''.obs;
  String get paymentMethodName => _paymentMethodName.value;
  set paymentMethodName(String value) => _paymentMethodName.value = value;

  final _transactionType = "".obs;
  String get transactionType => _transactionType.value;
  set transactionType(String value) => _transactionType.value = value;

  final _paymentReceivedBy = ''.obs;
  String get paymentReceivedBy => _paymentReceivedBy.value;
  set paymentReceivedBy(String value) => _paymentReceivedBy.value = value;

  final _bakongHash = ''.obs;
  String get bakongHash => _bakongHash.value;
  set bakongHash(String value) => _bakongHash.value = value;

  final _remark = ''.obs;
  String get remark => _remark.value;
  set remark(String value) => _remark.value = value;

  static DownloadReportTrxItemOutputModel create(DownloadReportTrxItemOutputDto dto) {
    var result = DownloadReportTrxItemOutputModel();
    result.merchantId = dto.merchantId;
    result.transactionDateText = dto.transactionDateText;
    result.transactionDate =dto.transactionDate;
    result.transactionId = dto.transactionId;
    result.outletName = dto.outletName;
    result.merchantAccount = dto.merchantAccount;
    result.merchantAccName = dto.merchantAccName;
    result.amount = dto.amount;
    result.currency = dto.currency;
    result.customerAccName = dto.customerAccName;
    result.customerBankName = dto.customerBankName;
    result.customerAccount = dto.customerAccount;
    result.paymentMethodName = dto.paymentMethodName;
    result.transactionType = dto.transactionType;
    result.paymentReceivedBy = dto.paymentReceivedBy;
    result.bakongHash = dto.bakongHash;
    result.remark = dto.remark;
    return result;
  }

}
