import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class TransactionCreateRefundOutputModel extends ResultStatusOutputModel {
  final _transactionId = ''.obs;
  String get transactionId => _transactionId.value;
  set transactionId(String value) => _transactionId.value = value;

  static TransactionCreateRefundOutputModel create(
      TransactionCreateRefundOutputDto dto) {
    var result = TransactionCreateRefundOutputModel();
    result.statusCode = dto.statusCode;
    result.description = dto.description;
    result.transactionId = dto.transactionId;
    return result;
  }
}
