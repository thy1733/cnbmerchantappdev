import 'package:get/get.dart';

class TransactionItemDetailsInputModel{
  
  final _transactionId = ''.obs;
  String get transactionId => _transactionId.value;
  set transactionId(String value) => _transactionId.value = value;
}