import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class DownloadReportTrxOutputModel extends ResultStatusOutputModel {
  final _totalAmountSale = 0.0.obs;
  double get totalAmountSale => _totalAmountSale.value;
  set totalAmountSale(double value) => _totalAmountSale.value = value;

  final _totalTransactionSale = 0.obs;
  int get totalTransactionSale => _totalTransactionSale.value;
  set totalTransactionSale(int value) => _totalTransactionSale.value = value;

  final _totalAmountRefunde = 0.0.obs;
  double get totalAmountRefund => _totalAmountRefunde.value;
  set totalAmountRefund(double value) => _totalAmountRefunde.value = value;

  final _totalTransactionRefunde = 0.obs;
  int get totalTransactionRefund => _totalTransactionRefunde.value;
  set totalTransactionRefund(int value) =>
      _totalTransactionRefunde.value = value;

  final _items = <DownloadReportTrxItemOutputModel>[].obs;
  List<DownloadReportTrxItemOutputModel> get items => _items;
  set items(List<DownloadReportTrxItemOutputModel> value) =>
      _items.value = value;

  static DownloadReportTrxOutputModel create(DownloadReportTrxOutputDto dto) {
    var result = DownloadReportTrxOutputModel();
    result.statusCode = dto.statusCode;
    result.isSuccess = dto.isSuccess;
    result.description = dto.description;
    result.totalAmountSale = dto.totalAmountSale;
    result.totalTransactionSale = dto.totalTransactionSale;
    result.totalAmountRefund = dto.totalAmountRefund;
    result.totalTransactionRefund = dto.totalTransactionRefund;
    for (var element in dto.items) {
      result.items.add(DownloadReportTrxItemOutputModel.create(element));
    }
    return result;
  }
}
