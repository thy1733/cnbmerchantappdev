import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class TransactionRefundItemListOutputModel {
  final _transactionId = ''.obs;
  String get transactionId => _transactionId.value;
  set transactionId(String value) => _transactionId.value = value;

  final _referalTransactionId = ''.obs;
  String get referalTransactionId => _referalTransactionId.value;
  set referalTransactionId(String value) => _referalTransactionId.value = value;

  final _senderAccountName = ''.obs;
  String get senderAccountName => _senderAccountName.value;
  set senderAccountName(String value) => _senderAccountName.value = value;

  final _senderBankName = ''.obs;
  String get senderBankName => _senderBankName.value;
  set senderBankName(String value) => _senderBankName.value = value;

  final _senderAccountNumber = ''.obs;
  String get senderAccountNumber => _senderAccountNumber.value;
  set senderAccountNumber(String value) => _senderAccountNumber.value = value;

  final _refundDate = DateTime.now().obs;
  DateTime get refundDate => _refundDate.value;
  set refundDate(DateTime value) => _refundDate.value = value;

  final _currency = ''.obs;
  String get currency => _currency.value;
  set currency(String value) => _currency.value = value;

  final _amount = 0.0.obs;
  double get amount => _amount.value;
  set amount(double value) => _amount.value = value;

  final _transactionType = ''.obs;
  String get transactionType => _transactionType.value;
  set transactionType(String value) => _transactionType.value = value;

  final _paymentMethod = ''.obs;
  String get paymentMethod => _paymentMethod.value;
  set paymentMethod(String value) => _paymentMethod.value = value;

  final _status = ''.obs;
  String get status => _status.value;
  set status(String value) => _status.value = value;

  final _approvalCode = ''.obs;
  String get approvalCode => _approvalCode.value;
  set approvalCode(String value) => _approvalCode.value = value;

  final _businessName = ''.obs;
  String get businessName => _businessName.value;
  set businessName(String value) => _businessName.value = value;

  final _outletName = ''.obs;
  String get outletName => _outletName.value;
  set outletName(String value) => _outletName.value = value;

  final _paymentReceivedBy = ''.obs;
  String get paymentReceivedBy => _paymentReceivedBy.value;
  set paymentReceivedBy(String value) => _paymentReceivedBy.value = value;

  final _remark = ''.obs;
  String get remark => _remark.value;
  set remark(String value) => _remark.value = value;

  static TransactionRefundItemListOutputModel create(
      TransactionRefundItemListOutputDto dto) {
    var result = TransactionRefundItemListOutputModel();
    result.transactionId = dto.transactionId;
    result.referalTransactionId = dto.referalTransactionId;
    result.senderAccountName = dto.senderAccountName;
    result.senderBankName = dto.senderBankName;
    result.senderAccountNumber = dto.senderAccountNumber;
    result.refundDate = dto.dateTime.fromTxnFormatToLocalDateTime();
    result.currency = dto.currency;
    result.amount = dto.amount;
    result.transactionType = dto.transactionType;
    result.paymentMethod = dto.paymentMethod;
    result.status = dto.status;
    result.approvalCode = dto.approvalCode;
    result.businessName = dto.businessName;
    result.outletName = dto.outletName;
    result.paymentReceivedBy = dto.paymentReceivedBy;
    result.remark = dto.remark;
    return result;
  }
}
