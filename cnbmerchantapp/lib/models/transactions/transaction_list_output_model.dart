import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class TransactionListOutputModel {
  final _netSale = 0.0.obs;
  double get netSale => _netSale.value;
  set netSale(double value) => _netSale.value = value;

  final _totalTransaction = 0.0.obs;
  double get totalTransaction => _totalTransaction.value;
  set totalTransaction(double value) => _totalTransaction.value = value;

  final _items = <TransactionItemListModel>[].obs;
  List<TransactionItemListModel> get items => _items;
  set items(List<TransactionItemListModel> value) => _items.value = value;

  static TransactionListOutputModel create(TransactionListOutputDto dto) {
    var result = TransactionListOutputModel();
    result.netSale = dto.netSale;
    // var itemGroups = groupBy(dto.items,(item) => (item as TransactionItemListDto).transactionDateText.substring(0,10));
    // itemGroups.forEach((key, value) {
    //    result.items.add(TransactionItemListOutputModel.create(key, value));
    //  });
    result.items =
        dto.items.map((e) => TransactionItemListModel.create(e)).toList();
    result.totalTransaction = dto.totalTransaction;
    return result;
  }
}
