import 'package:get/get.dart';

class TransactionPayStatusModel{
  
  final _clientName = ''.obs;
  String get clientName => _clientName.value;
  set clientName(String value) => _clientName.value = value;
  
  final _status = ''.obs;
  String get status => _status.value;
  set status(String value) => _status.value = value;
  
  final _statusMessage = ''.obs;
  String get statusMessage => _statusMessage.value;
  set statusMessage(String value) => _statusMessage.value = value;
  
  final _txnID = ''.obs;
  String get txnID => _txnID.value;
  set txnID(String value) => _txnID.value = value;
  
  final _dateTime = ''.obs;
  String get dateTime => _dateTime.value;
  set dateTime(String value) => _dateTime.value = value;
  
  final _cardLast4digits = ''.obs;
  String get cardLast4digits => _cardLast4digits.value;
  set cardLast4digits(String value) => _cardLast4digits.value = value;
  
  final _cardScheme = ''.obs;
  String get cardScheme => _cardScheme.value;
  set cardScheme(String value) => _cardScheme.value = value;
  
  final _cardHolderName = ''.obs;
  String get cardHolderName => _cardHolderName.value;
  set cardHolderName(String value) => _cardHolderName.value = value;
  
  final _tip = ''.obs;
  String get tip => _tip.value;
  set tip(String value) => _tip.value = value;
  
  final _amount = ''.obs;
  String get amount => _amount.value;
  set amount(String value) => _amount.value = value;
  
  final _approvalCode = ''.obs;
  String get approvalCode => _approvalCode.value;
  set approvalCode(String value) => _approvalCode.value = value;
  
  final _referenceNo = ''.obs;
  String get referenceNo => _referenceNo.value;
  set referenceNo(String value) => _referenceNo.value = value;
  
  final _currencyCode = ''.obs;
  String get currencyCode => _currencyCode.value;
  set currencyCode(String value) => _currencyCode.value = value;
  
  final _discount = ''.obs;
  String get discount => _discount.value;
  set discount(String value) => _discount.value = value;
  
  final _paidAmount = ''.obs;
  String get paidAmount => _paidAmount.value;
  set paidAmount(String value) => _paidAmount.value = value;

  Map<String, dynamic> toJson() => {
        "Amount": amount,
        "ApprovalCode": approvalCode,
        "CardHolderName": cardHolderName,
        "CardLast4digits": cardLast4digits,
        "CardScheme": cardScheme,
        "ClientName": clientName,
        "CurrencyCode": currencyCode,
        "DateTime": dateTime,
        "Discount": discount,
        "PaidAmount": paidAmount,
        "PayType": "Account",
        "Status": status,
        "StatusMessage": statusMessage,
        "Tip": tip,
        "TxrID": txnID,
        "bankref": referenceNo,
      };
  
}