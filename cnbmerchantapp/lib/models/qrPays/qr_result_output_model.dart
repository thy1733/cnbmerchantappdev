import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class QrResultOutputModel extends IndexModel {

  final _amount = 0.0.obs;
  double get amount => _amount.value;
  set amount(double value) => _amount.value = value;

  final _businessId = ''.obs;
  String get businessId => _businessId.value;
  set businessId(String value) => _businessId.value = value;

  final _cashierId = ''.obs;
  String get cashierId => _cashierId.value;
  set cashierId(String value) => _cashierId.value = value;

  final _cashierName = ''.obs;
  String get cashierName => _cashierName.value;
  set cashierName(String value) => _cashierName.value = value;

  // customer bank name
  final _clientName = ''.obs;
  String get clientName => _clientName.value;
  set clientName(String value) => _clientName.value = value;

  final _currencyCode = ''.obs;
  String get currencyCode => _currencyCode.value;
  set currencyCode(String value) => _currencyCode.value = value;

  final _dateTime = DateTime.now().obs;
  DateTime get dateTime => _dateTime.value;
  set dateTime(DateTime value) => _dateTime.value = value;

  final _discount = .0.obs;
  double get discount => _discount.value;
  set discount(double value) => _discount.value = value;

  final _isAllowPrint = false.obs;
  bool get isAllowPrint => _isAllowPrint.value;
  set isAllowPrint(bool value) => _isAllowPrint.value = value;

  final _outletId = ''.obs;
  String get outletId => _outletId.value;
  set outletId(String value) => _outletId.value = value;

  final _paidAmount = 0.0.obs;
  double get paidAmount => _paidAmount.value;
  set paidAmount(double value) => _paidAmount.value = value;

  final _payType = ''.obs;
  String get payType => _payType.value;
  set payType(String value) => _payType.value = value;

  final _printCount = ''.obs;
  String get printCount => _printCount.value;
  set printCount(String value) => _printCount.value = value;

  final _status = ''.obs;
  String get status => _status.value;
  set status(String value) => _status.value = value;

  final _statusMessage = ''.obs;
  String get statusMessage => _statusMessage.value;
  set statusMessage(String value) => _statusMessage.value = value;

  final _tip = 0.0.obs;
  double get tip => _tip.value;
  set tip(double value) => _tip.value = value;

  final _txrID = ''.obs;
  String get txrID => _txrID.value;
  set txrID(String value) => _txrID.value = value;

  final _bankref = ''.obs;
  String get bankref => _bankref.value;
  set bankref(String value) => _bankref.value = value;

  //
  final _customerName = ''.obs;
  String get customerName => _customerName.value;
  set customerName(String value) => _customerName.value = value;
  //
  final _customerAccount = ''.obs;
  String get customerAccount => _customerAccount.value;
  set customerAccount(String value) => _customerAccount.value = value;
  // payment method
  final _paymentMethod = ''.obs;
  String get paymentMethod => _paymentMethod.value;
  set paymentMethod(String value) => _paymentMethod.value = value;

  final _acceptedVia = ''.obs;
  String get acceptedVia => _acceptedVia.value;
  set acceptedVia(String value) => _acceptedVia.value = value;
  
  final _bakongHash = ''.obs;
 String get bakongHash => _bakongHash.value;
  set bakongHash(String value) => _bakongHash.value = value;
  

  static QrResultOutputModel fromJson(Map map) {
    var result = QrResultOutputModel();
    result.acceptedVia = _getStringNullChk(map['AcceptedVia']);
    result.amount = _getValueNullChk(map['Amount']);
    result.businessId = map['BusinessId'];
    result.cashierId = map['CashierId'];
    result.cashierName = _getStringNullChk(map['CashierName']);
    result.clientName = _getStringNullChk(map['ClientName']);
    result.currencyCode =_getStringNullChk(map['CurrencyCode']);
    result.customerAccount = _getStringNullChk(map['CustomerAc']);
    result.customerName = _getStringNullChk(map['CustomerName']);
    result.dateTime = map['DateTime'].toString().fromTxnFormatToLocalDateTime();
    result.discount = _getValueNullChk(map['Discount']);
    result.isAllowPrint = bool.fromEnvironment(map['IsAllowPrint']);
    result.outletId = map['OutletId'];
    result.paidAmount = _getValueNullChk(map['PaidAmount']);
    result.payType = _getStringNullChk(map['PayType']).tr;
    result.paymentMethod = _getStringNullChk(map['PaymentMethod']).tr;
    result.printCount = _getStringNullChk(map['PrintCount']);
    result.status = _getStringNullChk(map['Status']);
    result.isSuccess = result.status.toUpperCase() == "SUCCESS";
    result.statusMessage = map['StatusMessage'];
    result.tip = _getValueNullChk(map['Tip']);
    result.txrID = map['TxrID'];
    result.bankref = map['bankref'];
    result.bakongHash = _getStringNullChk(map['BakongHash']);
    return result;
  }

  static double _getValueNullChk(value) {
    return "$value".toUpperCase() == "NULL"
        ? 0
        : double.tryParse("$value") ?? 0;
  }

  static String _getStringNullChk(value) {
    return "$value".toUpperCase() == "NULL" || "$value".isEmpty
        ? "N/A"
        : "$value";
  }
}
