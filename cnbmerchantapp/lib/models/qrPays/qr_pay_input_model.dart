import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class QrPayInputModel extends IndexModel {
  final _currency = CurrencyEnum.usd.obs;
  CurrencyEnum get currency => _currency.value;
  set currency(CurrencyEnum value) => _currency.value = value;

  final _amountToPay = 0.0.obs;
  double get amountToPay => _amountToPay.value;
  set amountToPay(double value) => _amountToPay.value = value;

  final _merchantName = ''.obs;
  String get merchantName => _merchantName.value;
  set merchantName(String value) => _merchantName.value = value;
}
