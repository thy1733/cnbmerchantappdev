import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class GetFeeReqPrintedQrOutputModel extends ResultStatusOutputModel {
  final _printFee = 0.0.obs;
  double get printFee => _printFee.value;
  set printFee(double value) => _printFee.value = value;

  final _deliveryFee = 0.0.obs;
  double get deliveryFee => _deliveryFee.value;
  set deliveryFee(double value) => _deliveryFee.value = value;

  final _totalFee = 0.0.obs;
  double get totalFee => _totalFee.value;
  set totalFee(double value) => _totalFee.value = value;

  static GetFeeReqPrintedQrOutputModel create(GetFeeReqPrintedQrOutputDto dto) {
    var result = GetFeeReqPrintedQrOutputModel();
    result.statusCode = dto.statusCode;
    result.isSuccess = result.statusCode == "00";
    result.description = dto.description;
    result.printFee = dto.printFee;
    result.deliveryFee = dto.deliveryFee;
    result.totalFee = dto.totalFee;

    return result;
  }
}
