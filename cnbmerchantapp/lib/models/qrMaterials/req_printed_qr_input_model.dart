import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class ReqPrintedQrInputModel {
  final _selectedBusiness = ManageBusinessItemListOutputModel().obs;
  ManageBusinessItemListOutputModel get selectedBusiness =>
      _selectedBusiness.value;
  set selectedBusiness(ManageBusinessItemListOutputModel value) =>
      _selectedBusiness.value = value;

  final _selectedOutlet = OutletItemListOutputModel().obs;
  OutletItemListOutputModel get selectedOutlet => _selectedOutlet.value;
  set selectedOutlet(OutletItemListOutputModel value) =>
      _selectedOutlet.value = value;

  final _selectedRecieveOption = QrMaterialRecieverOptionEnum.none.obs;
  QrMaterialRecieverOptionEnum get selectedRecieveOption =>
      _selectedRecieveOption.value;
  set selectedRecieveOption(QrMaterialRecieverOptionEnum value) =>
      _selectedRecieveOption.value = value;

  // DELIVERY LOCATIOLN
  final _locationDeliver = LocationAddressModel().obs;
  LocationAddressModel get locationDeliver => _locationDeliver.value;
  set locationDeliver(LocationAddressModel value) =>
      _locationDeliver.value = value;

  // PICK_UP_LOCATION
  final _pickUpLocation = BankOfficeOutputModel().obs;
  BankOfficeOutputModel get pickUpLocation => _pickUpLocation.value;
  set pickUpLocation(BankOfficeOutputModel value) =>
      _pickUpLocation.value = value;

  final _standee = 0.obs;
  int get standee => _standee.value;
  set standee(int value) => _standee.value = value;

  final _stickerBillPad = 0.obs;
  int get stickerBillPad => _stickerBillPad.value;
  set stickerBillPad(int value) => _stickerBillPad.value = value;

  final _stickerA6 = 0.obs;
  int get stickerA6 => _stickerA6.value;
  set stickerA6(int value) => _stickerA6.value = value;

  final _lanyard = 0.obs;
  int get lanyard => _lanyard.value;
  set lanyard(int value) => _lanyard.value = value;

  final _printFee = .0.obs;
  double get printFee => _printFee.value;
  set printFee(double value) => _printFee.value = value;

  final _deliveryFee = .0.obs;
  double get deliveryFee => _deliveryFee.value;
  set deliveryFee(double value) => _deliveryFee.value = value;

  final _totalFee = .0.obs;
  double get totalFee => _totalFee.value;
  set totalFee(double value) => _totalFee.value = value;

  final _remark = ''.obs;
  String get remark => _remark.value;
  set remark(String value) => _remark.value = value;

  ReqPrintedQrDetailOutputModel toDetailOutput() {
    var result = ReqPrintedQrDetailOutputModel();
    result.requestStatus = KStatusEnum.pending;
    result.businessName = selectedBusiness.businessName;
    result.businessTypeName = selectedBusiness.businessType.name;
    result.linkAccount = selectedBusiness.accountNumber;
    result.outletName = selectedOutlet.outletName;
    result.outletLogoUrl = selectedOutlet.logoUrl;
    result.selectedRecieveOption = selectedRecieveOption;
    result.locationDeliver = locationDeliver;
    result.pickUpLocation = pickUpLocation;
    result.standee = standee;
    result.stickerBillPad = stickerBillPad;
    result.stickerA6 = stickerA6;
    result.lanyard = lanyard;
    result.printFee = printFee;
    result.deliveryFee = deliveryFee;
    result.totalFee = totalFee;
    result.remark = remark;
    return result;
  }
}
