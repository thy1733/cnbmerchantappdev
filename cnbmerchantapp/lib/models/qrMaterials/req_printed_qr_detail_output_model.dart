import 'package:get/get.dart';
import 'package:cnbmerchantapp/core.dart';

class ReqPrintedQrDetailOutputModel {
  final _requestId = "".obs;
  String get requestId => _requestId.value;
  set requestId(String value) => _requestId.value = value;

  get requestDisplayName {
    var value = _businessName == _outletName
        ? _businessName.value
        : "$_businessName - $_outletName";
    return value;
  }

  final _requestStatus = KStatusEnum.none.obs;
  KStatusEnum get requestStatus => _requestStatus.value;
  set requestStatus(KStatusEnum value) => _requestStatus.value = value;

  final _businessName = ''.obs;
  String get businessName => _businessName.value;
  set businessName(String value) => _businessName.value = value;

  final _businessTypeName = ''.obs;
  String get businessTypeName => _businessTypeName.value;
  set businessTypeName(String value) => _businessTypeName.value = value;
  
  final _businessTypeId = ''.obs;
  String get businessTypeId => _businessTypeId.value;
  set businessTypeId(String value) => _businessTypeId.value = value;
  
  final _linkAccount = ''.obs;
  String get linkAccount => _linkAccount.value;
  set linkAccount(String value) => _linkAccount.value = value;

  final _outletName = ''.obs;
  String get outletName => _outletName.value;
  set outletName(String value) => _outletName.value = value;

  final _outletLogoUrl = ''.obs;
  String get outletLogoUrl => _outletLogoUrl.value;
  set outletLogoUrl(String value) => _outletLogoUrl.value = value;

  final _selectedRecieveOption = QrMaterialRecieverOptionEnum.none.obs;
  QrMaterialRecieverOptionEnum get selectedRecieveOption =>
      _selectedRecieveOption.value;
  set selectedRecieveOption(QrMaterialRecieverOptionEnum value) =>
      _selectedRecieveOption.value = value;

  // DELIVERY LOCATIOLN
  final _locationDeliver = LocationAddressModel().obs;
  LocationAddressModel get locationDeliver => _locationDeliver.value;
  set locationDeliver(LocationAddressModel value) =>
      _locationDeliver.value = value;

  // PICK_UP_LOCATION
  final _pickUpLocation = BankOfficeOutputModel().obs;
  BankOfficeOutputModel get pickUpLocation => _pickUpLocation.value;
  set pickUpLocation(BankOfficeOutputModel value) =>
      _pickUpLocation.value = value;

  final _standee = 0.obs;
  int get standee => _standee.value;
  set standee(int value) => _standee.value = value;

  final _stickerBillPad = 0.obs;
  int get stickerBillPad => _stickerBillPad.value;
  set stickerBillPad(int value) => _stickerBillPad.value = value;

  final _stickerA6 = 0.obs;
  int get stickerA6 => _stickerA6.value;
  set stickerA6(int value) => _stickerA6.value = value;

  final _lanyard = 0.obs;
  int get lanyard => _lanyard.value;
  set lanyard(int value) => _lanyard.value = value;

  final _printFee = .0.obs;
  double get printFee => _printFee.value;
  set printFee(double value) => _printFee.value = value;

  final _deliveryFee = .0.obs;
  double get deliveryFee => _deliveryFee.value;
  set deliveryFee(double value) => _deliveryFee.value = value;

  final _totalFee = .0.obs;
  double get totalFee => _totalFee.value;
  set totalFee(double value) => _totalFee.value = value;

  final _remark = ''.obs;
  String get remark => _remark.value;
  set remark(String value) => _remark.value = value;
  // String id = "";

  final _requestDateTime = DateTime.now().obs;
  DateTime get requestDateTime => _requestDateTime.value;
  set requestDateTime(DateTime value) => _requestDateTime.value = value;

  final _lastUpdateDate = DateTime.now().obs;
  DateTime get lastUpdateDate => _lastUpdateDate.value;
  set lastUpdateDate(DateTime value) => _lastUpdateDate.value = value;

  static ReqPrintedQrDetailOutputModel create(ReqPrintedQrDetailOutputDto dto) {
    var result = ReqPrintedQrDetailOutputModel();
    result.requestId = dto.requestId;
    result.requestStatus = KStatusEnum.values.firstWhereOrNull(
            (e) => e.name.toUpperCase() == dto.requestStatus.toUpperCase()) ??
        KStatusEnum.pending;
    result.businessName = dto.businessName;
    result.businessTypeId = dto.businessTypeId;
    result.linkAccount = dto.linkAccount;
    result.outletName = dto.outletName;
    result.outletLogoUrl = dto.outletLogoUrl;
    result.selectedRecieveOption = QrMaterialRecieverOptionEnum.values
            .firstWhereOrNull((e) =>
                e.name.toUpperCase() ==
                dto.selectedRecieveOption.toUpperCase()) ??
        QrMaterialRecieverOptionEnum.SelfPickUp;

    result.locationDeliver.id = dto.selectedBankOfficeId;
    result.locationDeliver.locationName = dto.locationName;
    result.locationDeliver.address = dto.locationAddress;
    result.locationDeliver.latitude = dto.latitude;
    result.locationDeliver.longitude = dto.longitude;

    result.pickUpLocation.locationName = dto.locationName;
    result.pickUpLocation.locationAddress = dto.locationAddress;
    result.pickUpLocation.latitude = dto.latitude;
    result.pickUpLocation.longitude = dto.longitude;
    // result.locationDeliver = LocationAddressModel.create(dto.deliveryLocation);
    // result.pickUpLocation = BankOfficeOutputModel.create(dto.pickUpLocation);
    result.standee = dto.standee;
    result.stickerBillPad = dto.stickerBillPad;
    result.lanyard = dto.lanyard;
    result.stickerA6 = dto.standee;
    result.printFee = dto.printFee;
    result.deliveryFee = dto.deliveryFee;
    result.totalFee = dto.totalFee;
    result.remark = dto.remark;
    result.requestDateTime =
        DateTime.tryParse(dto.requestDate) ?? DateTime(2000);
    result.lastUpdateDate =
        DateTime.tryParse(dto.lastUpdateDate) ?? DateTime(2000);
    return result;
  }
}
