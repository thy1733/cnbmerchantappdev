import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class ReqPrintedQrListOutputModel extends ResultStatusOutputModel {
  final _items = <ReqPrintedQrDetailOutputModel>[].obs;
  List<ReqPrintedQrDetailOutputModel> get items => _items;
  set items(List<ReqPrintedQrDetailOutputModel> value) => _items.value = value;

  static ReqPrintedQrListOutputModel create(ReqPrintedQrListOutputDto dto) {
    var result = ReqPrintedQrListOutputModel();
    result.statusCode = dto.statusCode;
    result.isSuccess = result.statusCode == "00";
    result.description = dto.description;
    result.items =
        dto.items.map((e) => ReqPrintedQrDetailOutputModel.create(e)).toList();

    return result;
  }
}
