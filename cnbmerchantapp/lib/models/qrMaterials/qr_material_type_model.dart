class QRMaterialTypeModel {
  final String imageUrl;
  final String name;

  QRMaterialTypeModel({
    required this.imageUrl,
    required this.name,
  });
}

class QRMaterialTypeDataConst {
  final standee = QRMaterialTypeModel(
    imageUrl: "https://digital.canadiabank.com/qrmcpro/image/qr_stand.png",
    name: "Standee KHQR",
  );
  final stickerBillPad = QRMaterialTypeModel(
    imageUrl: "https://digital.canadiabank.com/qrmcpro/image/qr_sticker.png",
    name: "Sticker KHQR",
  );
  final lanyard = QRMaterialTypeModel(
    imageUrl: "https://digital.canadiabank.com/qrmcpro/image/qr_card.png",
    name: "Card KHQR",
  );
}
