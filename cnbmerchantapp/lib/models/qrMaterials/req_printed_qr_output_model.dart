import 'package:cnbmerchantapp/core.dart';

class ReqPrintedQrOutputModel extends ResultStatusOutputModel {
  static ReqPrintedQrOutputModel create(ReqPrintedQrOutputDto dto) {
    var result = ReqPrintedQrOutputModel();
    result.statusCode = dto.statusCode;
    result.isSuccess = result.statusCode == "00";
    result.description = dto.description;

    return result;
  }
}
