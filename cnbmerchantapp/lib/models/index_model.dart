import 'package:get/get.dart';

class IndexModel {
  final _index = 0.obs;
  int get index => _index.value;
  set index(int value) => _index.value = value;

  final _statusCode = ''.obs;
  String get statusCode => _statusCode.value;
  set statusCode(String value) => _statusCode.value = value;

  final _isSuccess = false.obs;
  bool get isSuccess => _isSuccess.value;
  set isSuccess(bool value) => _isSuccess.value = value;
}