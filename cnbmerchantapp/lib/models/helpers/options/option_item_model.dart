import 'package:get/get.dart';

class OptionItemModel{
  final _id = ''.obs;
  String get id => _id.value;
  set id(String value) => _id.value = value;

  final _name = ''.obs;
  String get name => _name.value;
  set name(String value) => _name.value = value;
  
  final _data =  <String>[].obs;
  List<String> get data => _data;
  set data(List<String> value) => _data.value = value;
  
  static OptionItemModel create(String title,List<String> item){
    var result = OptionItemModel();
    result.name = title;
    result.data = item;
    return result;
  }

  static OptionItemModel createList(String id, String title,List<String> item){
    var result = OptionItemModel();
    result.id = id;
    result.name = title;
    result.data = item;
    return result;
  }
}