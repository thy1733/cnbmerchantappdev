import 'package:get/get.dart';

class DatePeriodModel {
  final _fromDate = DateTime.now().obs;
  DateTime get fromDate => _fromDate.value;
  set fromDate(DateTime value) => _fromDate.value = value;

  final _toDate = DateTime.now().obs;
  DateTime get toDate => _toDate.value;
  set toDate(DateTime value) => _toDate.value = value;

  DatePeriodModel();

  DatePeriodModel create(DateTime fromDate, DateTime todate) {
    DatePeriodModel result = DatePeriodModel();
    result.fromDate = fromDate;
    result.toDate = todate;
    return result;
  }
}
