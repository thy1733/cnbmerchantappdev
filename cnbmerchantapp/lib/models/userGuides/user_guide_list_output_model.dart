// ignore_for_file: invalid_use_of_protected_member

import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class UserGuideListOutputModel {
  final _items = <UserGuideItemOutputModel>[].obs;
  List<UserGuideItemOutputModel> get items => _items.value;
  set items(value) => _items.value = value;

  static UserGuideListOutputModel create(UserGuideListOutputDto dto) {
    var result = UserGuideListOutputModel();
    result.items =
        dto.items.map((e) => UserGuideItemOutputModel.create(e)).toList();
    return result;
  }
}
