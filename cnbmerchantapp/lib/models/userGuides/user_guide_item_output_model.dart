import 'package:cnbmerchantapp/core.dart';

class UserGuideItemOutputModel {
  String imageUrl = "";
  String title = "";
  String description = "";
  String videoUrl = "";

  UserGuideItemOutputModel({
    required this.imageUrl,
    required this.videoUrl,
    required this.title,
    required this.description,
  });

  static UserGuideItemOutputModel create(UserGuideItemOutputDto dto) {
    var result = UserGuideItemOutputModel(
        imageUrl: dto.thumbnailImageUrl,
        videoUrl: dto.videoLink,
        title: dto.title,
        description: dto.description);
    return result;
  }
}
