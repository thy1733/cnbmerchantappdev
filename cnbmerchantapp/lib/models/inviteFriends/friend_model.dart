import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class FriendModel {
  final _id = 0.obs;
  int get id => _id.value;
  set id(int value) => _id.value = value;

  final _firstName = "".obs;
  String get firstName => _firstName.value;
  set firstName(String value) => _firstName.value = value;

  final _lastName = "".obs;
  String get lastName => _lastName.value;
  set lastName(String value) => _lastName.value = value;

  final _account = "".obs;
  String get account => _account.value;
  set account(String value) => _account.value = value;

  final _status = FriendStatus.active.obs;
  FriendStatus get status => _status.value;
  set status(FriendStatus value) => _status.value = value;

  final _photoUrl = "https://www.w3schools.com/howto/img_avatar.png".obs;

  String get photoUrl => _photoUrl.value;

  set photoUrl(String value) => _photoUrl.value = value;

  FriendModel.create(
      {required int id,
      required String firstName,
      required String lastName,
      required FriendStatus status,
      String photoUrl = "",
      String account = ""}) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.status = status;
    this.photoUrl = photoUrl;
    this.account = account;
  }

  FriendModel();
}
