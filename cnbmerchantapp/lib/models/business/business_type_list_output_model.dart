import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class BusinessTypeListOutputModel {
  final _items = <BusinessTypeItemListOutputModel>[].obs;
  List<BusinessTypeItemListOutputModel> get items => _items;
  set items(List<BusinessTypeItemListOutputModel> value) =>
      _items.value = value;

  static BusinessTypeListOutputModel create(BusinessTypeItemListOutputDto dto) {
    var result = BusinessTypeListOutputModel();
    result.items =
        dto.items.map((e) => BusinessTypeItemListOutputModel.create(e)).toList();
    return result;
  }
}
