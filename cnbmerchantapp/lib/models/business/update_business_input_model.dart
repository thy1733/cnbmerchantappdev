import 'package:get/get.dart';

class UpdateBusinessInputModel {
  final _businessId = "".obs;
  String get businessId => _businessId.value;
  set businessId(String value) => _businessId.value = value;

  final _businessLogo = "".obs;
  String get businessLogo => _businessLogo.value;
  set businessLogo(String value) => _businessLogo.value = value;

  final _businessTypeId = "".obs;
  String get businessTypeId => _businessTypeId.value;
  set businessTypeId(String value) => _businessTypeId.value = value;
}
