import 'package:get/get.dart';

import '../../core.dart';

class DetailBusinessOutputModel extends ResultStatusOutputModel {
  final _businessId = "".obs;
  String get businessId => _businessId.value;
  set businessId(String value) => _businessId.value = value;

  final _creationDate = "".obs;
  String get creationDate => _creationDate.value;
  set creationDate(String value) => _creationDate.value = value;

  final _userId = "".obs;
  String get userId => _userId.value;
  set userId(String value) => _userId.value = value;

  final _accountNumber = "".obs;
  String get accountNumber => _accountNumber.value;
  set accountNumber(String value) => _accountNumber.value = value;

  final _accountHolderFullName = "".obs;
  String get accountHolderFullName => _accountHolderFullName.value;
  set accountHolderFullName(String value) =>
      _accountHolderFullName.value = value;

  final _businessName = "".obs;
  String get businessName => _businessName.value;
  set businessName(String value) => _businessName.value = value;

  final _businessLogo = "".obs;
  String get businessLogo => _businessLogo.value;
  set businessLogo(String value) => _businessLogo.value = value;

  final _businessType = BusinessType().obs;
  BusinessType get businessType => _businessType.value;
  set businessType(BusinessType value) => _businessType.value = value;

  final _status = "".obs;
  String get status => _status.value;
  set status(String value) => _status.value = value;

  final _corporateStatus = "".obs;
  String get corporateStatus => _corporateStatus.value;
  set corporateStatus(String value) => _corporateStatus.value = value;

  final _locationName = "".obs;
  String get locationName => _locationName.value;
  set locationName(String value) => _locationName.value = value;

  final _locationAddress = "".obs;
  String get locationAddress => _locationAddress.value;
  set locationAddress(String value) => _locationAddress.value = value;

  final _latitude = 0.0.obs;
  double get latitude => _latitude.value;
  set latitude(double value) => _latitude.value = value;

  final _longitude = 0.0.obs;
  double get longitude => _longitude.value;
  set longitude(double value) => _longitude.value = value;

  final _totalCashier = "".obs;
  String get totalCashier => _totalCashier.value;
  set totalCashier(String value) => _totalCashier.value = value;

  final _totalOutlet = "".obs;
  String get totalOutlet => _totalOutlet.value;
  set totalOutlet(String value) => _totalOutlet.value = value;

  final _cacheImageKey = "".obs;
  String get cacheImageKey => _cacheImageKey.value;
  set cacheImageKey(String value) => _cacheImageKey.value = value;

  static DetailBusinessOutputModel create(DetailBusinessOutputDto dto) {
    var result = DetailBusinessOutputModel();

    result.statusCode = dto.statusCode;
    result.isSuccess = dto.isSuccess;
    result.description = dto.description;

    result.businessId = dto.businessId;
    result.creationDate = dto.creationDate;
    result.userId = dto.userId;
    result.accountNumber = dto.accountNumber;
    result.accountHolderFullName = dto.accountHolderFullName;
    result.businessName = dto.businessName;
    result.cacheImageKey = dto.cacheImageKey;
    result.businessLogo = dto.businessLogo;
    result.businessType.id = dto.businessType.id;
    result.businessType.name = dto.businessType.name;
    result.status = dto.status;
    result.corporateStatus = dto.corporateStatus;
    result.locationName = dto.locationName;
    result.locationAddress = dto.locationAddress;
    result.latitude = dto.latitude;
    result.longitude = dto.longitude;
    result.totalCashier = dto.totalCashier;
    result.totalOutlet = dto.totalOutlet;

    return result;
  }
}
