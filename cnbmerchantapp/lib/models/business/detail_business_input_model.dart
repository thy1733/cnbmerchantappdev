import 'package:get/get.dart';

class DetailBusinessInputModel {
  final _businessId = "".obs;
  String get businessId => _businessId.value;
  set businessId(String value) => _businessId.value = value;
}
