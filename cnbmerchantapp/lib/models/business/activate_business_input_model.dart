import 'package:get/get.dart';

import '../../core.dart';

class ActivateBusinessInputModel {
  final _businessId = "".obs;
  String get businessId => _businessId.value;
  set businessId(String value) => _businessId.value = value;

  static String toMapping(ActivateBusinessInputModel input) {
    var result = "${RoutineName.enableBusiness}[[${input.businessId}";
    return result;
  }
}
