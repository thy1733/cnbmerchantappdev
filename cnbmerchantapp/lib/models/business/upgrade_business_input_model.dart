import 'package:get/get.dart';

class UpgradeBusinessInputModel {
  final _businessId = "".obs;
  String get businessId => _businessId.value;
  set businessId(String value) => _businessId.value = value;

  final _patentPhotos = "".obs;
  String get patentPhotos => _patentPhotos.value;
  set patentPhotos(String value) => _patentPhotos.value = value;

  final _note = "".obs;
  String get note => _note.value;
  set note(String value) => _note.value = value;
}
