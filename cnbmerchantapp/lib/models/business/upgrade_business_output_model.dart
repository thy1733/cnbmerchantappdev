import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class UpgradeBusinessOutputModel {
  final _corporateStatus = CorporateStatus.None.obs;
  CorporateStatus get corporateStatus => _corporateStatus.value;
  set corporateStatus(CorporateStatus value) => _corporateStatus.value = value;

  static UpgradeBusinessOutputModel create(UpgradeBusinessOutputDto dto) {
    var result = UpgradeBusinessOutputModel();
    result.corporateStatus = CorporateStatus.values.firstWhereOrNull(
            (e) => e.name.toUpperCase() == dto.corporateStatus.toUpperCase()) ??
        CorporateStatus.None;
    return result;
  }
}
