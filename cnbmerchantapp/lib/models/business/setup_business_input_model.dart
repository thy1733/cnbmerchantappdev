import 'package:get/get.dart';

class SetupBusinessInputModel {
  final _businessLogo = "".obs;
  String get businessLogo => _businessLogo.value;
  set businessLogo(String value) => _businessLogo.value = value;

  final _businessName = "".obs;
  String get businessName => _businessName.value;
  set businessName(String value) => _businessName.value = value;

  final _businessTypeId = "".obs;
  String get businessTypeId => _businessTypeId.value;
  set businessTypeId(String value) => _businessTypeId.value = value;

  final _locationName = ''.obs;
  String get locationName => _locationName.value;
  set locationName(String value) => _locationName.value = value;

  final _locationAddress = ''.obs;
  String get locationAddress => _locationAddress.value;
  set locationAddress(String value) => _locationAddress.value = value;

  final _latitude = ''.obs;
  String get latitude => _latitude.value;
  set latitude(String value) => _latitude.value = value;

  final _longitude = ''.obs;
  String get longitude => _longitude.value;
  set longitude(String value) => _longitude.value = value;

  final _accountNumber = "".obs;
  String get accountNumber => _accountNumber.value;
  set accountNumber(String value) => _accountNumber.value = value;

  final _staffRefId = "".obs;
  String get staffRefId => _staffRefId.value;
  set staffRefId(String value) => _staffRefId.value = value;

  final _username = "".obs;
  String get username => _username.value;
  set username(String value) => _username.value = value;
}
