import 'package:get/get.dart';

import '../../core.dart';

class DeactivateBusinessInputModel {
  final _businessId = "".obs;
  String get businessId => _businessId.value;
  set businessId(String value) => _businessId.value = value;

  static String toMapping(DeactivateBusinessInputModel input) {
    var result = "${RoutineName.diableBusiness}[[${input.businessId}";
    return result;
  }
}
