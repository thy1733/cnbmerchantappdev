import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class ManageBusinessItemListOutputModel {
  final _id = "".obs;
  String get id => _id.value;
  set id(String value) => _id.value = value;

  final _creationDate = "".obs;
  String get creationDate => _creationDate.value;
  set creationDate(String value) => _creationDate.value = value;

  final _accountNumber = "".obs;
  String get accountNumber => _accountNumber.value;
  set accountNumber(String value) => _accountNumber.value = value;

  final _businessName = "".obs;
  String get businessName => _businessName.value;
  set businessName(String value) => _businessName.value = value;

  final _businessLogo = "".obs;
  String get businessLogo => _businessLogo.value;
  set businessLogo(String value) => _businessLogo.value = value;

  final _businessLogoId = "".obs;
  String get businessLogoId => _businessLogoId.value;
  set businessLogoId(String value) => _businessLogoId.value = value;

  final _businessType = BusinessType().obs;
  BusinessType get businessType => _businessType.value;
  set businessType(BusinessType value) => _businessType.value = value;

  final _status = "".obs;
  String get status => _status.value;
  set status(String value) => _status.value = value;

  final _corporateStatus = "".obs;
  String get corporateStatus => _corporateStatus.value;
  set corporateStatus(String value) => _corporateStatus.value = value;

  final _locationName = "".obs;
  String get locationName => _locationName.value;
  set locationName(String value) => _locationName.value = value;

  final _locationAddress = "".obs;
  String get locationAddress => _locationAddress.value;
  set locationAddress(String value) => _locationAddress.value = value;

  final _latitude = 0.0.obs;
  double get latitude => _latitude.value;
  set latitude(double value) => _latitude.value = value;

  final _longitude = 0.0.obs;
  double get longitude => _longitude.value;
  set longitude(double value) => _longitude.value = value;

  final _mcc = "".obs;
  String get mcc => _mcc.value;
  set mcc(String value) => _mcc.value = value;

  final _accountCurrency = "".obs;
  String get accountCurrency => _accountCurrency.value;
  set accountCurrency(String value) => _accountCurrency.value = value;

  final _cacheImageKey = "".obs;
  String get cacheImageKey => _cacheImageKey.value;
  set cacheImageKey(String value) => _cacheImageKey.value = value;

  static ManageBusinessItemListOutputModel create(
      ManageBusinessListOutputDto dto) {
    var result = ManageBusinessItemListOutputModel();
    result.id = dto.id;
    result.creationDate = dto.creationDate;
    result.accountNumber = dto.accountNumber;
    result.businessName = dto.businessName;
    result.businessLogo = dto.businessLogo;
    result.businessLogoId = dto.businessLogoId;
    result.businessType = dto.businessType;
    result.status = dto.status;
    result.corporateStatus = dto.corporateStatus;
    result.locationName = dto.locationName;
    result.locationAddress = dto.locationAddress;
    result.latitude = dto.latitude;
    result.longitude = dto.longitude;
    result.mcc = dto.mcc;
    result.accountCurrency = dto.accountCurrency;
    result.cacheImageKey = dto.cacheImageKey;
    return result;
  }
}
