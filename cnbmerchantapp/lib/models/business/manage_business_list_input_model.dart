import 'package:get/get.dart';

class ManageBusinessListInputModel {
  final _mCID = ''.obs;
  String get mCID => _mCID.value;
  set mCID(String value) => _mCID.value = value;
  
  final _filterStatus = ''.obs;
  String get filterStatus => _filterStatus.value;
  set filterStatus(String value) => _filterStatus.value = value;
  
}
