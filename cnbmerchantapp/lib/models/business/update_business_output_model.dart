import 'package:cnbmerchantapp/core.dart';

class UpdateBusinessOutputModel extends DetailBusinessOutputModel {
  static UpdateBusinessOutputModel create(UpdateBusinessOutputDto dto) {
    var result = UpdateBusinessOutputModel();
    result.statusCode = dto.statusCode;
    result.isSuccess = dto.isSuccess;
    result.description = dto.description;
    
    result.businessId = dto.businessId;
    result.creationDate = dto.creationDate;
    result.userId = dto.userId;
    result.accountNumber = dto.accountNumber;
    result.accountHolderFullName = dto.accountHolderFullName;
    result.businessName = dto.businessName;
    result.businessLogo = dto.businessLogo;
    result.businessType = dto.businessType;
    result.status = dto.status;
    result.corporateStatus = dto.corporateStatus;
    result.locationName = dto.locationName;
    result.locationAddress = dto.locationAddress;
    result.latitude = dto.latitude;
    result.longitude = dto.longitude;
    result.totalCashier = dto.totalCashier;
    result.totalOutlet = dto.totalOutlet;

    return result;
  }
}
