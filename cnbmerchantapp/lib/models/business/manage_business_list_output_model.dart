import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class ManageBusinessListOutputModel {
  
  final _items = <ManageBusinessItemListOutputModel>[].obs;
  List<ManageBusinessItemListOutputModel> get items => _items;
  set items(List<ManageBusinessItemListOutputModel> value) =>
      _items.value = value;

  static ManageBusinessListOutputModel create(
      ManageBusinessItemListOutputDto dto) {
    var result = ManageBusinessListOutputModel();
    result.items = dto.items
        .map((e) => ManageBusinessItemListOutputModel.create(e))
        .toList();
    return result;
  }
}
