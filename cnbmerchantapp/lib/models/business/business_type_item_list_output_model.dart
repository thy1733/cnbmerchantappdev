import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class BusinessTypeItemListOutputModel {
  final _id = "".obs;
  String get id => _id.value;
  set id(String value) => _id.value = value;

  final _name = "".obs;
  String get name => _name.value;
  set name(String value) => _name.value = value;

  BusinessTypeItemListOutputModel({String name = ""}) {
    this.name = name;
  }

  static BusinessTypeItemListOutputModel create(BusinessTypeListOutputDto dto) {
    var result = BusinessTypeItemListOutputModel();
    result.id = dto.id;
    result.name = dto.name;
    return result;
  }
}
