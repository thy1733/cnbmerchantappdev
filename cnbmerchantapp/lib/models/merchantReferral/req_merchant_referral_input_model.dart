import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class ReqMerchantReferralInputModel {
 
 
 final _businessName = ''.obs;
 String get businessName => _businessName.value;
 set businessName(String value) => _businessName.value = value;
 
 final _businessContact = ''.obs;
 String get businessContact => _businessContact.value;
 set businessContact(String value) => _businessContact.value = value;
 
  // DELIVERY LOCATIOLN
  final _businessLocation = LocationAddressModel().obs;
  LocationAddressModel get businessLocation => _businessLocation.value;
  set businessLocation(LocationAddressModel value) =>
      _businessLocation.value = value;


  final _remark = ''.obs;
  String get remark => _remark.value;
  set remark(String value) => _remark.value = value;

}
