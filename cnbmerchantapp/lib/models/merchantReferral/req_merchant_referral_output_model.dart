import 'package:cnbmerchantapp/core.dart';

class ReqMerchantReferralOutputModel extends ResultStatusOutputModel {
  static ReqMerchantReferralOutputModel create(ReqMerchantReferralOutputDto dto) {
    var result = ReqMerchantReferralOutputModel();
    result.statusCode = dto.statusCode;
    result.isSuccess = result.statusCode == "00";
    result.description = dto.description;
    return result;
  }
}
