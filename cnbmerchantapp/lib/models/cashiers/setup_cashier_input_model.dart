import 'package:cnbmerchantapp/models/cashiers/cashier_item_list_output_model.dart';
import 'package:get/get.dart';

import '../../helpers/instants/app_user_temp_instant.dart';

class SetupCashierInputModel {

  final _id = "".obs;
  String get id => _id.value;
  set id(String value) => _id.value = value;

  final _businessId = "".obs;
  String get businessId => _businessId.value;
  set businessId(String value) => _businessId.value = value;

  final _outletsId = "".obs;
  String get outletsId => _outletsId.value;
  set outletsId(String value) => _outletsId.value = value;

  final _isRefundable = "".obs;
  String get isRefundable => _isRefundable.value;
  set isRefundable(String value) => _isRefundable.value = value;

  final _refundLimited = "".obs;
  String get refundLimited => _refundLimited.value;
  set refundLimited(String value) => _refundLimited.value = value;

  final _photoUrl = "".obs;
  String get photoUrl => _photoUrl.value;
  set photoUrl(String value) => _photoUrl.value = value;

  final _phoneNumber = "".obs;
  String get phoneNumber => _phoneNumber.value;
  set phoneNumber(String value) => _phoneNumber.value = value;

  final _firstName = "".obs;
  String get firstName => _firstName.value;
  set firstName(String value) => _firstName.value = value;

  final _lastName = "".obs;
  String get lastName => _lastName.value;
  set lastName(String value) => _lastName.value = value;

  final _nationalId = "".obs;
  String get nationalId => _nationalId.value;
  set nationalId(String value) => _nationalId.value = value;

  final _username = "".obs;
  String get username => _username.value;
  set username(String value) => _username.value = value;

  SetupCashierInputModel();

  SetupCashierInputModel.getCurrentItem(CashierItemListOutputModel cashierOldItem) {
    id = cashierOldItem.id;
    isRefundable = cashierOldItem.isRefundable;
    refundLimited = cashierOldItem.refundLimited;
    photoUrl = cashierOldItem.photoUrl;
    phoneNumber = cashierOldItem.phoneNumber;
    firstName = cashierOldItem.firstName;
    lastName = cashierOldItem.lastName;
    nationalId = cashierOldItem.nationalId;
    username = AppUserTempInstant.appCacheUser.userName;
  }

}
