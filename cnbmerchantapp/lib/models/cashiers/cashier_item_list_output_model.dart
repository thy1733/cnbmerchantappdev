import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class CashierItemListOutputModel {
  CashierItemListOutputModel();

  final _id = "".obs;
  String get id => _id.value;
  set id(String value) => _id.value = value;

  final _creationDate = "".obs;
  String get creationDate => _creationDate.value;
  set creationDate(String value) => _creationDate.value = value;

  final _inviteCode = "".obs;
  String get inviteCode => _inviteCode.value;
  set inviteCode(String value) => _inviteCode.value = value;

  final _inviteCodeExpiredDate = "".obs;
  String get inviteCodeExpiredDate => _inviteCodeExpiredDate.value;
  set inviteCodeExpiredDate(String value) =>
      _inviteCodeExpiredDate.value = value;

  final _status = "".obs;
  String get status => _status.value;
  set status(String value) => _status.value = value;

  final _businessId = "".obs;
  String get businessId => _businessId.value;
  set businessId(String value) => _businessId.value = value;

  final _outlets = <OutletItemListOutputModel>[].obs;
  List<OutletItemListOutputModel> get outlets => _outlets;
  set outlets(List<OutletItemListOutputModel> value) => _outlets.value = value;

  final _isRefundable = "".obs;
  String get isRefundable => _isRefundable.value;
  set isRefundable(String value) => _isRefundable.value = value;

  final _refundLimited = "".obs;
  String get refundLimited => _refundLimited.value;
  set refundLimited(String value) => _refundLimited.value = value;

  final _photoUrl = "".obs;
  String get photoUrl => _photoUrl.value;
  set photoUrl(String value) => _photoUrl.value = value;

  final _photoUrlId = "".obs;
  String get photoUrlId => _photoUrlId.value;
  set photoUrlId(String value) => _photoUrlId.value = value;

  final _phoneNumber = "".obs;
  String get phoneNumber => _phoneNumber.value;
  set phoneNumber(String value) => _phoneNumber.value = value;

  final _firstName = "".obs;
  String get firstName => _firstName.value;
  set firstName(String value) => _firstName.value = value;

  final _lastName = "".obs;
  String get lastName => _lastName.value;
  set lastName(String value) => _lastName.value = value;

  final _nationalId = "".obs;
  String get nationalId => _nationalId.value;
  set nationalId(String value) => _nationalId.value = value;

  final _cacheImage = "".obs;
  String get cacheImage => _cacheImage.value;
  set cacheImage(String value) => _cacheImage.value = value;

  final _cashierStatus = CashierStatus.active.obs;
  CashierStatus get cashierStatus => _cashierStatus.value;
  set cashierStatus(CashierStatus value) => _cashierStatus.value = value;

  String get displayName =>"$firstName $lastName";

  factory CashierItemListOutputModel.create(CashierListOutputDto dto) {
    var result = CashierItemListOutputModel();
    result.id = dto.id;
    result.creationDate = dto.creationDate;
    result.inviteCode = dto.inviteCode;
    result.inviteCodeExpiredDate = dto.inviteCodeExpiredDate;
    result.status = dto.status;
    result.businessId = dto.businessId;
    result.outlets = dto.outlets;
    result.isRefundable = dto.isRefundable;
    result.refundLimited = dto.refundLimited;
    result.photoUrl = dto.photoUrl.downloadUrl(
        UploadProfileType.cashierProfile, dto.businessId, dto.photoUrl);
    result.photoUrlId = dto.photoUrl;
    result.phoneNumber = dto.phoneNumber;
    result.firstName = dto.firstName;
    result.lastName = dto.lastName;
    result.nationalId = dto.nationalId;
    result.cacheImage = dto.cacheImage;
    result.setCashierStatus(dto, result);
    return result;
  }

  void setCashierStatus(CashierListOutputDto dto, CashierItemListOutputModel result) {
    switch (dto.status.toLowerCase()) {
      case 'pending':
        result.cashierStatus = CashierStatus.pending;
        break;
      case 'active':
        result.cashierStatus = CashierStatus.active;
        break;
      case 'deactivate':
        result.cashierStatus = CashierStatus.inactive;
        break;
      case 'deleted':
        result.cashierStatus = CashierStatus.deleted;
        break;
    }
  }

  static CashierItemListOutputModel getCashierList(List<String> items, String businessId) {
    var result = CashierItemListOutputModel();
    result.id = items[0];
    result.photoUrl = items[1]
        .downloadUrl(UploadProfileType.cashierProfile, businessId, items[1]);
    result.cacheImage =
        items[1] + DateTime.now().toFormatDateString("yyyymmddhhmmsss");
    result.photoUrlId = items[1];
    result.phoneNumber = items[2];
    result.firstName = items[3];
    result.lastName = items[4];
    result.status = items[5];
    return result;
  }
}
