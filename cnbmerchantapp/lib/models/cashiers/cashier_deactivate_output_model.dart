import 'package:cnbmerchantapp/core.dart';

class CashierDeactivateOutputModel extends ResultStatusOutputModel{

  static CashierDeactivateOutputModel create(CashierDeactivateOutputDto dto) {
    var result = CashierDeactivateOutputModel();
    result.statusCode = dto.statusCode;
    result.description = dto.description;
    return result;
  }
}
