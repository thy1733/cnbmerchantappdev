import 'package:get/get.dart';
import 'package:cnbmerchantapp/core.dart';

class CashierDeactivateInputModel {

  final _cashierId = "".obs;
  String get cashierId => _cashierId.value;
  set cashierId(String value) => _cashierId.value = value;

  final _phoneNumber = "".obs;
  String get phoneNumber => _phoneNumber.value;
  set phoneNumber(String value) => _phoneNumber.value = value;

  final _cashierStatus = CashierStatus.active.obs;
  CashierStatus get cashierStatus => _cashierStatus.value;
  set cashierStatus(CashierStatus value) => _cashierStatus.value = value;

}


