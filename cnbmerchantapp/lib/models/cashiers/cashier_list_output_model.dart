import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class CashierListOutputModel {
  CashierListOutputModel();

  final _totalCount = "".obs;
  String get totalCount => _totalCount.value;
  set totalCount(String value) => _totalCount.value = value;

  final _items = <CashierItemListOutputModel>[].obs;
  List<CashierItemListOutputModel> get items => _items;
  set items(List<CashierItemListOutputModel> value) => _items.value = value;

  factory CashierListOutputModel.create(CashierItemListOutputDto dto) {
    var result = CashierListOutputModel();
    result.totalCount = dto.totalCount;
    result.items =
        dto.items.map((e) => CashierItemListOutputModel.create(e)).toList();
    return result;
  }
}
