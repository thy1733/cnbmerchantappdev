import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class  SetupCashierOutputModel extends ResultStatusOutputModel{
  SetupCashierOutputModel();

  final _inviteCode = ''.obs;
  String get inviteCode => _inviteCode.value;
  set inviteCode(String value) => _inviteCode.value = value;

  final _inviteCodeExpiredDate = ''.obs;
  String get inviteCodeExpiredDate => _inviteCodeExpiredDate.value;
  set inviteCodeExpiredDate(String value) => _inviteCodeExpiredDate.value = value;

  static SetupCashierOutputModel create(SetupCashierOutputDto dto){
    var result = SetupCashierOutputModel();
    result.inviteCode = dto.inviteCode;
    result.inviteCodeExpiredDate = dto.inviteCodeExpiredDate;
    result.description = dto.description;
    return result;
  }

}
