import 'package:get/get.dart';

class CashierListInputModel {

  final _businessId = "".obs;
  String get businessId => _businessId.value;
  set businessId(String value) => _businessId.value = value;

  final _outletId = "".obs;
  String get outletId => _outletId.value;
  set outletId(String value) => _outletId.value = value;

  final _filter = "".obs;
  String get filter => _filter.value;
  set filter(String value) => _filter.value = value;

  final _totalPage = "".obs;
  String get totalPage => _totalPage.value;
  set totalPage(String value) => _totalPage.value = value;

  final _perPage = "".obs;
  String get perPage => _perPage.value;
  set perPage(String value) => _perPage.value = value;

}
