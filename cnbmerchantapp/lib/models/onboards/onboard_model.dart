import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class OnboardModel extends IndexModel {
  final _id = 0.obs;
  int get id => _id.value;
  set id(int value) => _id.value = value;

  final _title = ''.obs;
  String get title => _title.value;
  set title(String value) => _title.value = value;

  final _description = ''.obs;
  String get description => _description.value;
  set description(String value) => _description.value = value;

  final _imageUrl = ''.obs;
  String get imageUrl => _imageUrl.value;
  set imageUrl(String value) => _imageUrl.value = value;

  final _isActive = false.obs;
  bool get isActive => _isActive.value;
  set isActive(bool value) => _isActive.value = value;

  OnboardModel();

  OnboardModel.create(
      {int id = 0,
      String title = "",
      String description = "",
      String imageUrl = ""}) {
    index = 0;
    this.id = id;
    this.title = title;
    this.description = description;
    this.imageUrl = imageUrl;
  }
}
