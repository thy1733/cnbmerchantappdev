import 'package:cnbmerchantapp/core.dart';

class DeactivateOutletOutputModel extends ResultStatusOutputModel {
  static DeactivateOutletOutputModel create(DeactivateOutletOutputDto dto) {
    var result = DeactivateOutletOutputModel();
    result.statusCode = dto.statusCode;
    result.isSuccess = dto.isSuccess;
    result.description = dto.description;
    return result;
  }
}
