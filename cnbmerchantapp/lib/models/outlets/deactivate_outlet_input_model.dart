import 'package:get/get.dart';

class DeactivateOutletInputModel {
  final _outletId = "".obs;
  String get outletId => _outletId.value;
  set outletId(String value) => _outletId.value = value;
}
