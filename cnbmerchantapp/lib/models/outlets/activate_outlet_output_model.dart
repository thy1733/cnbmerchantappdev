import '../../core.dart';

class ActivateOutletOutputModel extends ResultStatusOutputModel {
  static ActivateOutletOutputModel create(ActivateOutletOutputDto dto) {
    var result = ActivateOutletOutputModel();
    result.statusCode = dto.statusCode;
    result.isSuccess = dto.isSuccess;
    result.description = dto.description;
    return result;
  }
}
