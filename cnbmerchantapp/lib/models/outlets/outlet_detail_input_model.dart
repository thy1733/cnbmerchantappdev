import 'package:get/get.dart';

class OutletDetailInputModel {
  final _outletId = "".obs;
  String get outletId => _outletId.value;
  set outletId(String value) => _outletId.value = value;
}
