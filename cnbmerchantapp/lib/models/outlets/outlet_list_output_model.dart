import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class OutletListOutputModel extends ResultStatusOutputModel {
  OutletListOutputModel();

  final _items = <OutletItemListOutputModel>[].obs;
  List<OutletItemListOutputModel> get items => _items;
  set items(List<OutletItemListOutputModel> value) => _items.value = value;

  factory OutletListOutputModel.create(OutletItemListOutputDto dto) {
    var result = OutletListOutputModel();
    result.statusCode = dto.statusCode;
    result.isSuccess = dto.isSuccess;
    result.description = dto.description;
    
    result.items =
        dto.items.map((e) => OutletItemListOutputModel.create(e)).toList();
    return result;
  }
}
