import 'package:cnbmerchantapp/core.dart';

class SetupOutletOutputModel extends ResultStatusOutputModel {
  
  static SetupOutletOutputModel create(SetupOutletOutputDto dto) {
    var result = SetupOutletOutputModel();
    result.statusCode = dto.statusCode;
    result.isSuccess = dto.isSuccess;
    result.description = dto.description;
    return result;
  }
}
