import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class OutletItemListOutputModel {
  OutletItemListOutputModel();

  final _id = "".obs;
  String get id => _id.value;
  set id(String value) => _id.value = value;

  final _creationDate = "".obs;
  String get creationDate => _creationDate.value;
  set creationDate(String value) => _creationDate.value = value;

  final _accountNumber = "".obs;
  String get accountNumber => _accountNumber.value;
  set accountNumber(String value) => _accountNumber.value = value;

  final _businessId = "".obs;
  String get businessId => _businessId.value;
  set businessId(String value) => _businessId.value = value;

  final _outletName = "".obs;
  String get outletName => _outletName.value;
  set outletName(String value) => _outletName.value = value;

  final _outletLogo = "".obs;
  String get outletLogo => _outletLogo.value;
  set outletLogo(String value) => _outletLogo.value = value;

  final _outletLogoId = "".obs;
  String get outletLogoId => _outletLogoId.value;
  set outletLogoId(String value) => _outletLogoId.value = value;

  final _status = "".obs;
  String get status => _status.value;
  set status(String value) => _status.value = value;

  final _locationName = "".obs;
  String get locationName => _locationName.value;
  set locationName(String value) => _locationName.value = value;

  final _locationAddress = "".obs;
  String get locationAddress => _locationAddress.value;
  set locationAddress(String value) => _locationAddress.value = value;

  final _latitude = 0.0.obs;
  double get latitude => _latitude.value;
  set latitude(double value) => _latitude.value = value;

  final _longitude = 0.0.obs;
  double get longitude => _longitude.value;
  set longitude(double value) => _longitude.value = value;

  final _cashiers = <CashierItemListOutputModel>[].obs;
  List<CashierItemListOutputModel> get cashiers => _cashiers;
  set cashiers(List<CashierItemListOutputModel> value) =>
      _cashiers.value = value;

  final _isPrimary = false.obs;
  bool get isPrimary => _isPrimary.value;
  set isPrimary(bool value) => _isPrimary.value = value;

  final _telegramGroupId = "".obs;
  String get telegramGroupId => _telegramGroupId.value;
  set telegramGroupId(String value) => _telegramGroupId.value = value;

  final _logoUrl = "".obs;
  String get logoUrl => _logoUrl.value;
  set logoUrl(String value) => _logoUrl.value = value;

  final _cacheKey = "".obs;
  String get cacheKey => _cacheKey.value;
  set cacheKey(String value) => _cacheKey.value = value;

  factory OutletItemListOutputModel.create(OutletListOutputDto dto) {
    var result = OutletItemListOutputModel();
    result.id = dto.id;
    result.creationDate = dto.creationDate;
    result.accountNumber = dto.accountNumber;
    result.businessId = dto.businessId;
    result.outletName = dto.outletName;

    result.outletLogo = dto.outletLogo.downloadUrl(
        UploadProfileType.outletProfile, dto.businessId, dto.outletLogo);
    result.cacheKey =
        dto.outletLogo + DateTime.now().toFormatDateString("yyyymmddhhmmsss");
    result.outletLogoId = dto.outletLogo;

    result.status = dto.status;
    result.locationName = dto.locationName;
    result.locationAddress = dto.locationAddress.replaceAll(";", ",");
    result.latitude = dto.latitude;
    result.longitude = dto.longitude;
    result.cashiers = dto.cashiers;
    result.isPrimary = dto.isPrimary;
    result.telegramGroupId = dto.telegramGroupId;

    return result;
  }

  static OutletItemListOutputModel getShortOutlet(
      List<String> items, String referId) {
    var logo = items[2].isNotEmpty && items[2].length < 5 ? "" : items[2];
    var result = OutletItemListOutputModel();
    result.id = items[0];
    result.outletName = items[1];
    result.logoUrl =
        logo.downloadUrl(UploadProfileType.outletProfile, referId, logo);
    return result;
  }
}
