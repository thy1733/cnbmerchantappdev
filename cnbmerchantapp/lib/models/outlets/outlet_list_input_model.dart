import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class OutletListInputModel {
  final _businessId = "".obs;
  String get businessId => _businessId.value;
  set businessId(String value) => _businessId.value = value;


  final _outletId = "".obs;
  String get outletId => _outletId.value;
  set outletId(String value) => _outletId.value = value;


  // Used for search as the Keyword
  final _filter = "".obs;
  String get filter => _filter.value;
  set filter(String value) => _filter.value = value;

  final _maxResultCount = 0.obs;
  int get maxResultCount => _maxResultCount.value;
  set maxResultCount(int value) => _maxResultCount.value = value;

  final _skipCount = 0.obs;
  int get skipCount => _skipCount.value;
  set skipCount(int value) => _skipCount.value = value;

  static String toMapping(OutletListInputModel input) {
    String result =
        "${RoutineName.getListOutlets}[[${input.businessId}[${input.outletId}[${input.filter}[${input.maxResultCount}[${input.skipCount}";
    return result;
  }

}
