import 'package:get/get.dart';

class SetupOutletInputModel {
  final _businessId = "".obs;
  String get businessId => _businessId.value;
  set businessId(String value) => _businessId.value = value;

  final _accountNumber = "".obs;
  String get accountNumber => _accountNumber.value;
  set accountNumber(String value) => _accountNumber.value = value;

  final _outletName = "".obs;
  String get outletName => _outletName.value;
  set outletName(String value) => _outletName.value = value;

  final _outletLogo = "".obs;
  String get outletLogo => _outletLogo.value;
  set outletLogo(String value) => _outletLogo.value = value;

  final _locationName = "".obs;
  String get locationName => _locationName.value;
  set locationName(String value) => _locationName.value = value;

  final _locationAddress = "".obs;
  String get locationAddress => _locationAddress.value;
  set locationAddress(String value) => _locationAddress.value = value;

  final _latitude = 0.0.obs;
  double get latitude => _latitude.value;
  set latitude(double value) => _latitude.value = value;

  final _longitude = 0.0.obs;
  double get longitude => _longitude.value;
  set longitude(double value) => _longitude.value = value;

  final _cashiers = "".obs;
  String get cashiers => _cashiers.value;
  set cashiers(String value) => _cashiers.value = value;

  final _isPrimary = false.obs;
  bool get isPrimary => _isPrimary.value;
  set isPrimary(bool value) => _isPrimary.value = value;

  final _telegramGroupId = "".obs;
  String get telegramGroupId => _telegramGroupId.value;
  set telegramGroupId(String value) => _telegramGroupId.value = value;
}
