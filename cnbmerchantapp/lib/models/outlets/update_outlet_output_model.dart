import 'package:cnbmerchantapp/core.dart';

class UpdateOutletOutputModel extends ResultStatusOutputModel {
  static UpdateOutletOutputModel create(UpdateOutletOutputDto dto) {
    var result = UpdateOutletOutputModel();
    result.statusCode = dto.statusCode;
    result.isSuccess = dto.isSuccess;
    result.description = dto.description;
    return result;
  }
}
