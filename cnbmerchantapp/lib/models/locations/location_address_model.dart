import 'package:cnbmerchantapp/core.dart';

class LocationAddressModel {
  late String id;
  late String address;
  late String locationName;
  late double latitude;
  late double longitude;

  LocationAddressModel(
      {this.id = "",
      this.address = "",
      this.locationName = "",
      this.latitude = 0.0,
      this.longitude = 0.0});
      
  static LocationAddressModel create(DeliveryLocationOutputDto dtos) {
    var result = LocationAddressModel();
    result.locationName = dtos.locationName;
    result.address = dtos.locationAddress;
    result.latitude =dtos.longitude;
    result.longitude =dtos.longitude;
    return result;
  }

  factory LocationAddressModel.fromDto(BusinessLocationOutputDto outputDto) {
    var locationAddressModel = LocationAddressModel();
    locationAddressModel.id = outputDto.id ?? '';
    locationAddressModel.address = outputDto.address ?? '';
    locationAddressModel.locationName = outputDto.locationName ?? '';
    locationAddressModel.latitude = outputDto.latitude ?? 0.0;
    locationAddressModel.longitude = outputDto.longitude ?? 0.0;
    return locationAddressModel;
  }

  static LocationAddressModel fromJson(LocationOutputDto json) {
    var result = LocationAddressModel();
    if (json.results!.isNotEmpty) {
      result.address = (json.results == null || json.results!.isEmpty
          ? ""
          : json.results![0].formattedAddress)!;
      if (json.results![0].addressComponents!.length > 2) {
        result.locationName = (json.results == null || json.results!.isEmpty
            ? ""
            : json.results![0].addressComponents![2].longName)!;
      } else {
        result.locationName = (json.results == null || json.results!.isEmpty
            ? ""
            : json.results![0].addressComponents![0].longName)!;
      }

      if (json.results![0].geometry != null &&
          json.results![0].geometry!.location != null) {
        result.latitude = json.results![0].geometry!.location!.lat!;
        result.longitude = json.results![0].geometry!.location!.lng!;
      }
    }

    return result;
  }

  static LocationAddressModel fromJsonResult(LocationResults json) {
    var result = LocationAddressModel();
    result.address = json.formattedAddress!;
    if (json.addressComponents!.length > 2) {
      result.locationName = json.addressComponents![2].longName!;
    } else {
      result.locationName = json.addressComponents![0].longName!;
    }

    if (json.geometry != null && json.geometry!.location != null) {
      result.latitude = json.geometry!.location!.lat!;
      result.longitude = json.geometry!.location!.lng!;
    }
    return result;
  }
}
