import 'package:cnbmerchantapp/core.dart';

class SearchAddressModel {
  List<AddressListModel> listAddress = [];

  static SearchAddressModel fromJson(AutoCompletedSearchBoxDto json) {
    var result = SearchAddressModel();
    if (json.predictions == null) {
      result.listAddress = [];
    } else {
      result.listAddress =
          json.predictions!.map((e) => AddressListModel.fromJson(e)).toList();
    }
    return result;
  }
}

class AddressListModel {
  String description;
  String placeId;

  AddressListModel({this.description = "", this.placeId = ""});

  static AddressListModel fromJson(Predictions json) {
    var result = AddressListModel();
    result.description = json.description!;
    result.placeId = json.placeId!;
    return result;
  }
}
