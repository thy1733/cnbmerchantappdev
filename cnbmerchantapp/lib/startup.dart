import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

class MerchantStartup extends StatelessWidget {
  const MerchantStartup({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    bool isKhmerLanguage = Get.locale?.languageCode == LanguageOption.lanKhmer;
    return ScreenUtilInit(
      designSize: defaultDesignSize,
      builder: (context, _) {
        return GetMaterialApp(
          localizationsDelegates: const [
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate
          ],
          supportedLocales: LocalizeApp.languagesLocale,
          translationsKeys: LocalizeApp.languagesCode,
          locale: const Locale('en', 'US'),
          fallbackLocale: const Locale('en', 'US'),
          title: "CanaMerchant",
          debugShowCheckedModeBanner: false,
          defaultTransition: Transition.cupertino,
          opaqueRoute: Get.isOpaqueRouteDefault,
          popGesture: Get.isPopGestureEnable,
          transitionDuration: const Duration(milliseconds: 230),
          theme: ThemeData(
            textSelectionTheme: const TextSelectionThemeData(
              cursorColor: Colors.blue,
            ),
            primaryColor: AppColors.primary,
            fontFamily: isKhmerLanguage
                ? GoogleFonts.notoSansKhmer().fontFamily
                : GoogleFonts.roboto().fontFamily,
            textTheme: AppTextStyle.defaultTextTheme,
            elevatedButtonTheme: AppButtonStyle.kPrimaryElevateButtonTheme(),
            textButtonTheme: TextButtonThemeData(
                style: ButtonStyle(
                    overlayColor: MaterialStateProperty.all(
                        AppColors.primary.withOpacity(0.05)))),
            visualDensity: VisualDensity.adaptivePlatformDensity,
          ),
          initialRoute: AppPages.init,
          initialBinding: AppBinding(),
          getPages: AppPages.routes,
          builder: (context, child) {
            return MediaQuery(
              data: MediaQuery.of(context).copyWith(
                textScaleFactor: 1.0,
              ),
              child: Material(
                child: AppSessionTimoutWidget(
                    child: Stack(
                  children: [
                    child ?? const Placeholder(),
                    Obx(() => AppShieldingTemp.isShowStatusbar
                        ? Container(
                            height: 50,
                            color: Colors.green,
                            child: Align(
                              alignment: Alignment.bottomCenter,
                              child: AnimatedTextKit(
                                pause: const Duration(milliseconds: 10),
                                totalRepeatCount: 5,
                                animatedTexts: [
                                  FadeAnimatedText(
                                      'App Protected on ${AppShieldingTemp.info}',
                                      textStyle:
                                          const TextStyle(color: Colors.white)),
                                ],
                                onTap: () => {},
                                onFinished: () {
                                  AppShieldingTemp.isShowStatusbar = false;
                                },
                              ),
                            ))
                        : const SizedBox()),
                  ],
                )),
              ),
            );
          },
        );
      },
    );
  }
}
