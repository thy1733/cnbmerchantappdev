import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:path_provider/path_provider.dart';
import 'package:firebase_core/firebase_core.dart';

Future<void> initializeFirebase() async {
  await Firebase.initializeApp(options: DefaultFirebaseOptions.currentPlatform);
}

Future<void> initializeHive() async {
  final appDocumentDirectory = await getApplicationDocumentsDirectory();
  await Hive.initFlutter(appDocumentDirectory.path);
  Hive.init(appDocumentDirectory.path);
  Hive.registerAdapter(AppLocalizeModelAdapter());
  Hive.registerAdapter(AppOnboardingScreenModelAdapter());
  Hive.registerAdapter(AppUserProfileInfoModelAdapter());
  Hive.registerAdapter(AppBusinessListModelAdapter());
  Hive.registerAdapter(AppBusinessItemModelAdapter());
  Hive.registerAdapter(OutletListCacheModelAdapter());
  Hive.registerAdapter(OutletItemCacheModelAdapter());
  Hive.registerAdapter(AppSettingCacheModelAdapter());
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await initializeFirebase();
  await initializeHive();


  //#region init route page at the app startup
  final IAppOnboardingScreenService launchService =
      AppOnboardingScreenService();
  final result = await launchService.getAppOnboardingScreenAsync();
  if (result.isUserLogin) {
    AppPages.init = Routes.main;
  } else if (result.isUserSkip && !result.isUserLogin) {
    AppPages.init = Routes.login;
  }
  // AppPages.init = Routes.main;
  //#endregion

  // Setup StatusBar Color
  SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
    systemNavigationBarColor: Colors.transparent, // navigation bar color
    statusBarColor: Colors.transparent, // status bar color
    systemNavigationBarContrastEnforced: true,
    statusBarBrightness: Brightness.dark,
  ));

  runApp(const MerchantStartup());
}
