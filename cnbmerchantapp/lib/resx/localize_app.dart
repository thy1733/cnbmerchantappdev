import 'package:flutter/material.dart';

import 'localize_en.dart';
import 'localize_kh.dart';
import 'localize_ch.dart';

abstract class LocalizeApp {
  static Map<String, Map<String, String>> languagesCode = {
    "en_US": english,
    "km": khmer,
    "zh": chinese,
  };

  static List<Locale> languagesLocale = [
    const Locale("en", "US"),
    const Locale("km", "KH"),
    const Locale("zh", "CN"),
  ];
}
