import 'dart:async';
import 'dart:io';
import 'package:cnbmerchantapp/core.dart';
import 'package:cnbmerchantapp/helpers/components/dialogs/set_amount_dialog_widget.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:path_provider/path_provider.dart' as path_provider;
import 'package:screen_brightness/screen_brightness.dart';

class QrDisplayScreenController extends AppController {
  late StreamSubscription<DatabaseEvent> _listeningTransaction;
  final systemService = KSystemService();
  final _qrValueGenerator = QrValueGenerator();
  final _screenBrightness = ScreenBrightness();
  final _tabHomeController = Get.find<TabHomeScreenController>();
  final IAppOutletCacheService _appOutletCacheService = AppOutletCacheService();
  final _tabTrxController = Get.find<TabTransactionScreenController>();

  //#region All Override method

  @override
  void onReady() async {
      if (Get.arguments != null && Get.arguments[ArgumentKey.input] != null) {
      var input = Get.arguments[ArgumentKey.input] as QrPayInputModel;
      currency = input.currency;
      amountToPay = input.amountToPay;
    }
    selectCurrency();
    setMaxAmount();
    increaseBrightness();
    setQrMerchantName();
    var txansactionId = DateTime.now().toFormatDateString("yyyymmddhhmmsss");
    onGenerateQrValue(txansactionId);
    super.onReady();
  }

  @override
  void onClose() {
    _listeningTransaction.cancel();
    super.onClose();
  }


  //#endregion

  //#region All Properties

  final _currency = CurrencyEnum.usd.obs;
  CurrencyEnum get currency => _currency.value;
  set currency(CurrencyEnum value) => _currency.value = value;

  final _amountToPay = 0.0.obs;
  double get amountToPay => _amountToPay.value;
  set amountToPay(double value) => _amountToPay.value = value;

  final _merchantName = ''.obs;
  String get merchantName => _merchantName.value;
  set merchantName(String value) => _merchantName.value = value;

  final _qrValue = ''.obs;

  /// final value for use to generate qr image
  String get qrValue => _qrValue.value;

  /// final value for use to generate qr image
  set qrValue(String value) => _qrValue.value = value;

  final _shareNote = ''.obs;
  String get shareNote => _shareNote.value;
  set shareNote(String value) => _shareNote.value = value;

  bool isImageSaved = false;

  double maximumAmount = 10000;

  //#endregion

  // Future<void> _shareLinkedAsync() async {
  //   try {
  //     showLoading();
  //     isLoading = true;
  //     var note = '';
  //     if (shareNote.isNotEmpty) {
  //       note = '&&$shareNote';
  //     }
  //     await systemService
  //         .shareLinkedAsync("https://canawebapp.vercel.app/cnb/YyL17150l$note");
  //     await Future.delayed(const Duration(seconds: 1)); // to avoid double click
  //   } finally {
  //     isLoading = false;
  //     dismiss();
  //   }
  // }

  // Future<void> _copyLinkedAsync() async {
  //   try {
  //     showLoading();
  //     isLoading = true;
  //     var note = '';
  //     if (shareNote.isNotEmpty) {
  //       note = '/$shareNote';
  //     }
  //     await systemService.copyLinktoClipboardAsync(
  //         "https://canawebapp.vercel.app/cnb/YyL17150l$note",
  //         showToast: false);
  //   } finally {
  //     isLoading = false;
  //     dismiss();
  //     Fluttertoast.showToast(
  //       msg: "LinkCopied".tr,
  //       toastLength: Toast.LENGTH_LONG,
  //       gravity: ToastGravity.BOTTOM,
  //       backgroundColor: AppColors.success,
  //     );
  //   }
  // }

  // Widget _sharingNoteWidget() {
  //   return Obx(
  //     () => InkWell(
  //       onTap: () {
  //         FocusManager.instance.primaryFocus?.unfocus();
  //       },
  //       child: Padding(
  //         padding: const EdgeInsets.all(15.0),
  //         child: Column(
  //           mainAxisSize: MainAxisSize.min,
  //           crossAxisAlignment: CrossAxisAlignment.center,
  //           children: [
  //             Text(
  //               "PaymentLink".tr,
  //               style: AppTextStyle.headline6.copyWith(color: AppColors.black),
  //             ),
  //             ContainerDevider.blankSpaceLg,
  //             KTextField(
  //               name: "Note",
  //               label: "Note".tr,
  //               keyboardType: TextInputType.multiline,
  //               iconData: MerchantIcon.note,
  //               maxLine: 5,
  //               maxLength: 250,
  //               valueCounter: shareNote.length,
  //               needIcon: false,
  //               hint: "ShortDescription".tr,
  //               inputFormatters: [
  //                 CustomLengthLimitingTextInputFormatter(250),
  //               ],
  //               onChanged: (value) => shareNote = value ?? '',
  //             ),
  //             ContainerDevider.blankSpaceSm,
  //             AppButtonStyle.secondaryButton(
  //                 label: "Copy".tr,
  //                 child: Row(
  //                   mainAxisAlignment: MainAxisAlignment.center,
  //                   children: [
  //                     const Icon(
  //                       MerchantIcon.copy,
  //                       size: 15,
  //                       color: AppColors.textPrimary,
  //                     ),
  //                     ContainerDevider.blankSpaceSm,
  //                     Text(
  //                       "Copy".tr,
  //                       style: AppTextStyle.body2
  //                           .copyWith(color: AppColors.textPrimary),
  //                     )
  //                   ],
  //                 ),
  //                 onPressed: () async {
  //                   backAsync();
  //                   await _copyLinkedAsync();
  //                 }),
  //             ContainerDevider.blankSpaceSm,
  //             AppButtonStyle.primaryButton(
  //               label: "ShareLink",
  //               child: Row(
  //                 mainAxisAlignment: MainAxisAlignment.center,
  //                 children: [
  //                   const Icon(MerchantIcon.share, size: 15),
  //                   ContainerDevider.blankSpaceSm,
  //                   Text(
  //                     " ${"ShareLink".tr} ",
  //                     style: AppTextStyle.body2,
  //                   )
  //                 ],
  //               ),
  //               onPressed: () async {
  //                 backAsync();
  //                 await _shareLinkedAsync();
  //               },
  //             ),
  //           ],
  //         ),
  //       ),
  //     ),
  //   );
  // }

  void _onImageSavedSuccess() {
    var msg = 'YourQrImageSavedtoPhoto'.tr;
    if (GetPlatform.isAndroid) {
      msg = 'YourQrImageSavedtoGallery'.tr;
    }
    Fluttertoast.cancel();
    Fluttertoast.showToast(
      msg: msg,
      toastLength: Toast.LENGTH_LONG,
      gravity: ToastGravity.BOTTOM,
      backgroundColor: AppColors.success,
    );
  }

  Future<void> increaseBrightness() async {
    try {
      var current = await _screenBrightness.current;
      await _screenBrightness.setScreenBrightness(current + 0.4);
    } catch (e) {
      debugPrint('Failed to set brightness');
    }
  }

  Future<void> listeningTransactionAsync(String txnId) async {
    var info = DeviceInfoTempInstant.deviceInfo;
    var serialNumber = info.identifier.replaceAll(".", "-");
    var selectedLisener = "";

    var cashierId = AppUserTempInstant.isCashier ? AppUserTempInstant.appCacheUser.profileId.replaceAll(".", "-"):"0";
    selectedLisener = "${AppOutletTempInstant.selectedOutlet.id.replaceAll(".", "-")}-$cashierId";
 
    final db2 = FirebaseDatabase.instanceFor(
            app: Firebase.app(), databaseURL: UrlConstant.fireInstant)
        .ref(InvokeMethodName.bankRefer);
    var listenPath =
        "${InvokeMethodName.bankSecret}/$selectedLisener/$serialNumber/$txnId";
    debugPrint(listenPath);
    _listeningTransaction = db2.child(listenPath).onChildAdded.listen(
      (event) async {
        try {
          // Close generate KHQR screen
          Get.back();
          if (event.snapshot.key!.toUpperCase() == "bankref".toUpperCase()) {
            final snapshot = await db2.child(listenPath).get();
            var itemMap = snapshot.value! as Map;
            var result = QrResultOutputModel.fromJson(itemMap);

            // get transaction list data
            _tabTrxController.getTransactionListAsync(checkListEmpty: false, isShowLoading: false);
            _tabHomeController.fetchSaleAnalyticDataAsync(
              _tabHomeController.currentSaleAnalyDatasChartIndex,
              _tabHomeController.saleAnalyDatas.elementAt(_tabHomeController.currentSaleAnalyDatasChartIndex).selectDate,
            );
            // Show transaction invoice
            await toInvoiceDetail(result);
          }
        } catch (e) {
          debugPrint(e.toString());
        }
      },
    );
  }

   Future<void> toInvoiceDetail(QrResultOutputModel detail) async {
    var arguments = <String, dynamic>{
      ArgumentKey.detail: detail,
    };
    await super.navigationToPageArgumentsAsync(
      const TransactionInvoiceScreen(),
      args: arguments,
    );
  }

  Future<void> toInvoice(String trxId) async {
    var input = TransactionItemDetailsInputModel();
    input.transactionId = trxId;
    var arguments = <String, dynamic>{
      ArgumentKey.input: input,
      ArgumentKey.fromPage: ""
    };
    await super.navigationToPageArgumentsAsync(
      const TransactionInvoiceScreen(),
      args: arguments,
    );
  }

  /// to show name on KHQR template
  ///
  /// it's should be Merchant Name = Business name + outlet name
  /// for defualt outlet name is used Business name by defualt when create so we use only Business name when select defualt outlet
  void setQrMerchantName() {
    /// find defualt outletId
    /// defualt outletId is end with .1
    var outletId = AppOutletTempInstant.selectedOutlet.id.split('.').last;
    if (outletId == '1') {
      merchantName = AppBusinessTempInstant.selectedBusiness.businessName;
    } else {
      merchantName =
          "${AppBusinessTempInstant.selectedBusiness.businessName} ${AppOutletTempInstant.selectedOutlet.outletName}";
    }
  }

  void selectCurrency(){
    currency = CurrencyEnum.values.firstWhereOrNull((element) => element.name.toUpperCase()==AppBusinessTempInstant.selectedBusiness.accountCurrency) ?? CurrencyEnum.usd;
  }

  void setMaxAmount(){
    if(currency == CurrencyEnum.khr){
      maximumAmount = 40000000;
    }
  }
  
  //#endregion

  //#region All Command
  void onGenerateQrValue(String transactionId) {
    if (AppBusinessTempInstant.selectedBusiness.mcc.isEmpty) {
      AppBusinessTempInstant.selectedBusiness.mcc = '4722';
    }
    if (AppBusinessTempInstant.selectedBusiness.locationName.isEmpty) {
      AppBusinessTempInstant.selectedBusiness.locationName = 'PHNOM PENH';
    }
    if (AppBusinessTempInstant.selectedBusiness.id.isNotEmpty) {
      var cashierId =AppUserTempInstant.isCashier ? AppUserTempInstant.appCacheUser.profileId : "0";
      qrValue = _qrValueGenerator.tag30(
          merchantId: AppUserTempInstant.appCacheUser.mCID,
          mcc: AppBusinessTempInstant.selectedBusiness.mcc,
          currencyCode: currency.toQrCurrencyCode(),
          amount: amountToPay,
          merchantName: merchantName,
          merchantLocation: AppBusinessTempInstant.selectedBusiness.locationName,
          businessId: AppBusinessTempInstant.selectedBusiness.id,
          outletId: AppOutletTempInstant.selectedOutlet.id,
          cashierId: cashierId,
          txnId: transactionId,
      );
    }

    // Listening transaction when cashier generate KHQR code
    listeningTransactionAsync(transactionId);
  }

  void onBackgroundClick() {
    backAsync();
  }

  void onSetAmountAsync() async {
    await showMaterialModalBottomSheet(
        context: Get.context!,
        enableDrag: true,
        backgroundColor: AppColors.transparent,
        barrierColor: AppColors.barrierBackground,
        builder: (_) {
          return SetAmountDialogWidget(
            initialAmount: amountToPay,
            maximumAmount: maximumAmount,
            isDecimal: isUSDCurrency,
            currency: currency,
            onConfirm: (amount) async {
              await Future.delayed(const Duration(milliseconds: 300));
              amountToPay = amount;
              var txansactionId =
                  DateTime.now().toFormatDateString("yyyymmddhhmmsss");
              onGenerateQrValue(txansactionId);
            },
          );
        });
  }

  void onShare() async {
    return; //Temporary disabled this feature
    // shareNote = '';
    // await AppBottomSheet.bottomSheetBlankContent(_sharingNoteWidget(),
    //     barrierBackground: AppColors.barrierBackground);
  }

  Future<void> onSaveImageAsync(Widget savedScreen) async {
    if (isImageSaved) {
      _onImageSavedSuccess();
      return;
    }

    showLoading();
    // convert Widget to Image
    ///** unknown error!
    ///  createImageFromWidget() for first convert it not complete all image.
    ///   but for second it work as well
    /// */
    await createImageFromWidget(savedScreen, const Size(350, 610));
    await createImageFromWidget(savedScreen, const Size(350, 610))
        .then((value) async {
      if (value.isNotEmpty) {
        final imageSaveService = ImageSaveService();

        // save image to local storage
        var result = await imageSaveService.imageFromStream(image: value);
        if (result) {
          _onImageSavedSuccess();
          isImageSaved = true;
        }
      }
    });
    dismiss();
  }

  Future<void> onScreenshotAsync(Widget savedScreen) async {
    showLoading();
    await createImageFromWidget(
            SizedBox(
              height: 470,
              width: 320,
              child: Material(
                color: Colors.transparent,
                child: savedScreen,
              ),
            ),
            const Size(320, 470))
        .then((value) async {
      if (value.isNotEmpty) {
        final directory =
            (await path_provider.getApplicationDocumentsDirectory()).path;
        File imageFile = File('$directory/qrStant.png');
        final file = await imageFile.writeAsBytes(value);
        await KSystemService.shareFiles(
          [file.path],
        );
      }
    });
    dismiss();
  }

  void onSelectOutletAsync() async {
    await AppBottomSheet.bottomSheetSelectDynamicOption<
        OutletItemListOutputModel>(
      title: "SwitchOutlet".tr,
      items: AppOutletTempInstant.outletList.items,
      onSelectedItem: (index, value) {
        if (AppOutletTempInstant.selectedOutlet != value) isImageSaved = false;
        AppOutletTempInstant.selectedOutlet = value;
        setQrMerchantName();
        var txansactionId =
            DateTime.now().toFormatDateString("yyyymmddhhmmsss");
        onGenerateQrValue(txansactionId);
        _appOutletCacheService.saveSelectedOutlet(value);
      },
      selectedItem: AppOutletTempInstant.selectedOutlet,
      closeOnSelect: true,
      separator: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Container(
          color: AppColors.greyBackground,
          height: 1,
        ),
      ),
      itemBuilder: (index, value) => Row(
        children: [
          AppNetworkImage(
            value.outletLogo,
            height: 35,
            width: 35,
            shape: BoxShape.circle,
            errorWidget: SvgPicture.asset(
              SvgAppAssets.pOutletList,
            ),
          ),
          const SizedBox(width: 10),
          Text(
            value.outletName,
            style: AppTextStyle.body1.copyWith(color: AppColors.textPrimary),
          )
        ],
      ),
    );
  }

  //#endregion

}
