import 'package:cnbmerchantapp/core.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';

class AmountInputScreenController extends AppController {
  final systemService = KSystemService();

  final _homeController = Get.find<TabHomeScreenController>();
  //#region All Override method

  @override
  void onInit() async {
    await _getBusinessRelatedAsync();
    super.onInit();
  }

  @override
  void onReady() {
    // todo: implement onReady
    super.onReady();
  }

  //#endregion

  //#region All Properties

  final _displayName = ''.obs;
  String get displayName => _displayName.value;
  set displayName(String value) => _displayName.value = value;

  final _isMaximumAmount = false.obs;
  bool get isMaximumAmount => _isMaximumAmount.value;
  set isMaximumAmount(bool value) => _isMaximumAmount.value = value;

  final _currency = CurrencyEnum.usd.obs;
  CurrencyEnum get currency => _currency.value;
  set currency(CurrencyEnum value) => _currency.value = value;

  final _inputAmount = '0'.obs;
  String get inputAmount => _inputAmount.value;
  set inputAmount(String value) => _inputAmount.value = value;

  final _isShowMaxAmount = false.obs;
  bool get isShowMaxAmount => _isShowMaxAmount.value;
  set isShowMaxAmount(bool value) => _isShowMaxAmount.value = value;

  final _isMaximumAmountMessage = ''.obs;
  String get isMaximumAmountMessage => _isMaximumAmountMessage.value;
  set isMaximumAmountMessage(String value) =>
      _isMaximumAmountMessage.value = value;

  final _valueAmount = 0.0.obs;
  double get valueAmount => _valueAmount.value;
  set valueAmount(double value) => _valueAmount.value = value;

  final _confirmAmount = ''.obs;
  String get confirmAmount => _confirmAmount.value;
  set confirmAmount(String value) => _confirmAmount.value = value;

  final _limitCharacter = 0.obs;
  int get limitCharacter => _limitCharacter.value;
  set limitCharacter(int value) => _limitCharacter.value = value;

  final _shareNote = ''.obs;
  String get shareNote => _shareNote.value;
  set shareNote(String value) => _shareNote.value = value;
  //#endregion

  //#region All Method Helpers

  void _getTotalConfirmAmount(String part) {
    valueAmount = double.parse(inputAmount.replaceAll(part, ''));
    confirmAmount =
        '${CurrencyConverter.currencyToSymble(currency)}${valueAmount.toThousandSeparatorString(d2: isUSDCurrency)}';
    getMaximumAmount();
  }

  bool getMaximumAmount() {
    var num = inputAmount.replaceAll(',', '');
    if (currency == CurrencyEnum.khr) {
      isMaximumAmount = double.parse(num) > 400000000.00;
      isMaximumAmountMessage = '៛400,000,0000';
    } else {
      isMaximumAmount = double.parse(num) > 10000.00;
      isMaximumAmountMessage = '\$10,000';
    }
    return isMaximumAmount;
  }

  int checkDecimalValue(String text) {
    if (text.contains(ValueConst.point)) {
      int startIndex = text.indexOf('.');
      var value = text.substring(startIndex);
      return value.length;
    }
    return 0;
  }

  Future<void> _getBusinessRelatedAsync() async {}
  //#endregion

  //#region All Command

  Future<void> userTapOnKeyboardAsync(String value) async {
    if (value == ValueConst.point) {
      if (inputAmount.contains(ValueConst.point)) return;
      inputAmount += value;
      _getTotalConfirmAmount('.');
    } else {
      // Check max length user input
      if (inputAmount.length >= 11) return;

      if (inputAmount.length == 1 && inputAmount == '0') {
        inputAmount = value;
        _getTotalConfirmAmount('');
      } else {
        limitCharacter = checkDecimalValue(inputAmount);
        if (limitCharacter == 3) return;

        if (!getMaximumAmount()) {
          inputAmount += value;
          _getTotalConfirmAmount('');
        }
      }
    }
    isShowMaxAmount = isMaximumAmount;
  }

  Future<void> userTabOnBackSpaceAsync() async {
    // ignore: unnecessary_null_comparison
    if (inputAmount != null && inputAmount.isNotEmpty) {
      String result = inputAmount.substring(0, inputAmount.length - 1);
      inputAmount = result.isEmpty ? '0' : result;
    }
    inputAmount = inputAmount.isEmpty ? '0' : inputAmount;

    _getTotalConfirmAmount('');

    getMaximumAmount();
    isShowMaxAmount = isMaximumAmount;
  }

  Future<void> resetAsync() async {
    inputAmount = '0';
    _getTotalConfirmAmount('');
    getMaximumAmount();
    isShowMaxAmount = isMaximumAmount;
  }

  Future<void> generateOrConfirmQRAsync() async {
    var qrPayInput = QrPayInputModel();
    qrPayInput.amountToPay = valueAmount;
    qrPayInput.currency = currency;
    // qrPayInput.selectedBusiness = "BusinessName";
    // qrPayInput.selectedOutlet = "OutletName";
    // await Get.toNamed(Routes.displayQr,
    //         arguments: {ArgumentKey.item: qrPayInput})!
    //     .then((value) => resetAsync());
    await _homeController
        .showQrDisplay(input: qrPayInput)
        .then((value) => resetAsync());
  }

  Future<void> shareLinkedAsync() async {
    // if (isLoading) return;
    try {
      showLoading();
      isLoading = true;
      var note = '';
      if (shareNote.isNotEmpty) {
        note = '/$shareNote';
      }
      await systemService
          .shareLinkedAsync("https://canawebapp.vercel.app/cnb/YyL17150l$note");
      await Future.delayed(const Duration(seconds: 1)); // to avoid double click
    } finally {
      isLoading = false;
      dismiss();
    }
    isLoading = false;
  }

  Future<void> copyLinkedAsync() async {
    try {
      showLoading();
      isLoading = true;
      var note = '';
      if (shareNote.isNotEmpty) {
        note = '/$shareNote';
      }
      await systemService.copyLinktoClipboardAsync(
          "https://canawebapp.vercel.app/cnb/YyL17150l$note",
          showToast: false);
    } finally {
      isLoading = false;
      dismiss();
      Fluttertoast.showToast(
        msg: "LinkCopied".tr,
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: AppColors.success,
      );
    }
  }
  //#endregion
}
