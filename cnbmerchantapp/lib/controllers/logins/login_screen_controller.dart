import 'dart:async';
import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LoginScreenController extends AppController {
  final ILoginService _loginService = LoginService();
  final IAppUserProfileInfoService _appCacheService =
      AppUserProfileInfoService();

  //#region All Override method

  @override
  void onInit() async {
    loginInput.userValidateMessage = "";
    loginInput.passwordValidateMessage = "";
    await getUserCachesAsync();
    super.onInit();
  }
  
  @override
  void onClose() {
    FocusManager.instance.primaryFocus?.unfocus();
    super.onClose();
  }
  //#endregion

  //#region All Properties

  final passwordTextFieldController = TextEditingController();
  final usernameTextFieldController = TextEditingController();

  final _loginInput = UserLoginInputModel().obs;
  UserLoginInputModel get loginInput => _loginInput.value;
  set loginInput(UserLoginInputModel value) => _loginInput.value = value;

  final _isShowRememberedUser = false.obs;
  bool get isShowRememberedUser => _isShowRememberedUser.value;
  set isShowRememberedUser(bool value) => _isShowRememberedUser.value = value;

  final _isRememberedUser = false.obs;
  bool get isRememberedUser => _isRememberedUser.value;
  set isRememberedUser(bool value) => _isRememberedUser.value = value;

  final _isHidePassword = true.obs;
  bool get isHidePassword => _isHidePassword.value;
  set isHidePassword(bool value) => _isHidePassword.value = value;

  bool get usernameValidate {
    if (isShowRememberedUser) return true;
    loginInput.userValidateMessage =
        loginInput.userName.isEmpty ? "PleaseInputYourUsername" : "";
    return loginInput.userValidateMessage.isEmpty;
  }

  bool get passwordValidate {
    loginInput.passwordValidateMessage =
        loginInput.password.isEmpty ? "PleaseInputYourPassword" : "";
    return loginInput.passwordValidateMessage.isEmpty;
  }
  //#endregion

  //#region All Method Helpers
  // void checkRememberUser() {
  //   isRememberedUser = true;
  //   isShowRememberedUser = isRememberedUser;
  // }

  // #endregion

  //#endregion

  //#region All Command

  Future<void> getUserCachesAsync() async {
    var result = await _appCacheService.getUserProfileAsync();
    isRememberedUser = result.userName.isNotEmpty;
    isShowRememberedUser = isRememberedUser;
    loginInput.userName = result.userName;
  }

  Future<void> loginAsync() async {
    showLoading();
    var result = await _loginService.loginAsync(loginInput);
    dismiss();
    if (result.error == null) {
      if (result.success) {
        FocusManager.instance.primaryFocus?.unfocus();
        await navigationToOffAllNamedAsync(Routes.main);
      } else if (result.result!.statusCode == "06") {
        CChoiceDialog.alert(
          title: "Information".tr,
          message: "NotYetSetupBusinessProfile".tr,
          accept: () async {
            Get.back();
            var userAccount = RegisterAccountInputModel();
            userAccount.accountNumber = result.result!.accountNumber;
            userAccount.userName = loginInput.userName;
            var arguments = <String, dynamic>{
              ArgumentKey.fromPage: Routes.login,
              ArgumentKey.item: userAccount,
            };
            navigationToOffAllNamedAsync(Routes.setupBusinessProfile,
                args: arguments);
          },
          acceptTitle: "Ok".tr,
        );
      } else {
        CChoiceDialog.ok(
            title: "Failure".tr,
            message: result.result?.description.tr ??
                "SomethingWentWrongPleaseTryAgainLater".tr);
      }
    } else {
      CChoiceDialog.ok(
          title: result.error!.message, message: result.error!.details);
    }
  }

  void activateOwner() {
    var argurment = {ArgumentKey.fromPage: Routes.activateOwnerScreen};
    navigationToNamedArgumentsAsync(
      Routes.activateTermCondition,
      args: argurment,
    );
  }

  void activateCashier() {
    var argurment = {ArgumentKey.fromPage: Routes.activateCashierScreen};
    navigationToNamedArgumentsAsync(
      Routes.activateTermCondition,
      args: argurment,
    );
  }

  void activateExistingMerchant() {
    var argurment = {
      ArgumentKey.fromPage: Routes.activateExistingMerchantScreen
    };
    navigationToNamedArgumentsAsync(
      Routes.activateTermCondition,
      args: argurment,
    );
  }

  void forgotPassword() {
    toNamedArgumentsAsync(Routes.forgotPassword);
  }

  void onSwitchLoginMode() {
    FocusManager.instance.primaryFocus?.unfocus();
    usernameTextFieldController.clear();
    passwordTextFieldController.clear();
    loginInput.userName = '';
    loginInput.password = '';
    loginInput.userValidateMessage = "";
    loginInput.passwordValidateMessage = "";
    isShowRememberedUser = !isShowRememberedUser;
    isHidePassword = true;
    if (isShowRememberedUser) {
      loginInput.userName = AppUserTempInstant.appCacheUser.userName;
    }
  }

  //#endregion

  // #region VALIDATE_FUNCTION

  void userNameEventChanging(String? value) {
    loginInput.userName = (value ?? '').trim();
    loginInput.userValidateMessage =
        loginInput.userName.isEmpty ? "UsernameRequiredMessage" : "";
  }

  void passwordEventChanging(String? value) {
    loginInput.password = value ?? '';
    loginInput.passwordValidateMessage =
        loginInput.password.isEmpty ? "PasswordRequiredMessage" : "";
  }
  //#endregion
}
