import 'package:cnbmerchantapp/core.dart';
import 'package:cnbmerchantapp/models/merchantPromotions/create_merchant_promotion_output_model.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';

class MerchantPromotionDetailScreenController extends AppController {
  //#region All Override method

  @override
  void onInit() {
    if (Get.arguments != null) {
      final merchantPromotion = Get.arguments[ArgumentKey.item]
          as CreateMerchantPromotionOutputModel?;
      if (merchantPromotion != null) {
        promotionItem = merchantPromotion;
      }
    }
    super.onInit();
  }
  //#endregion

  //#region All Properties

  final _promotionItem = CreateMerchantPromotionOutputModel().obs;
  CreateMerchantPromotionOutputModel get promotionItem => _promotionItem.value;
  set promotionItem(CreateMerchantPromotionOutputModel value) =>
      _promotionItem.value = value;

  //#endregion

  //#region All Method Helpers
  //#endregion

  //#region All Command

  void cancelPromotionRequest() {
    CChoiceDialog.okCancel(
        title: "MerchantCancelTitle".tr,
        message: "MerchantCancelDes".tr,
        okTitle: "MerchantCancel".tr,
        cancelTitle: "GoBack".tr,
        onOk: () {
          Get.back();
          Fluttertoast.showToast(
            msg: "CancelledMerchantPromotionSuccess".tr,
            gravity: ToastGravity.TOP,
          );
          Get.back(result: 0);
        });
  }
  //#endregion

}
