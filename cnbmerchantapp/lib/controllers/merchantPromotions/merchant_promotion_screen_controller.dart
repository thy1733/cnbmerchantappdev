import 'package:cnbmerchantapp/core.dart';
import 'package:cnbmerchantapp/models/merchantPromotions/create_merchant_promotion_output_model.dart';
import 'package:get/get.dart';

class MerchantPromotionScreenController extends AppController {
  //#region All Override method

  @override
  void onReady() {
    getPromotionList();
    getPromotionHistory();
    super.onReady();
  }
  //#endregion

  //#region All Properties
  final _merchantPromotion = <MerchantPromotionModel>[].obs;
  List<MerchantPromotionModel> get merchantPromotion => _merchantPromotion;
  set merchantPromotion(List<MerchantPromotionModel> value) =>
      _merchantPromotion.value = value;

  final _promotionHistory = <MerchantPromotionModel>[].obs;
  List<MerchantPromotionModel> get promotionHistory => _promotionHistory;
  set promotionHistory(List<MerchantPromotionModel> value) =>
      _promotionHistory.value = value;

  final _isAll = true.obs;
  get isAll => _isAll.value;
  set isAll(value) => _isAll.value = value;

  final _isHistory = false.obs;
  get isHistory => _isHistory.value;
  set isHistory(value) => _isHistory.value = value;

  //#endregion

  //#region All Method Helpers

  void changeOptionStatus(int index) {
    if (index == 0) {
      isAll = true;
      isHistory = !isAll;
    } else {
      isAll = false;
      isHistory = !isAll;
    }
  }

  void getPromotionList() {
    String imageUrl =
        'https://images.unsplash.com/photo-1656576412427-9c560349c7e1?crop=entropy&cs=tinysrgb&fit=crop&fm=jpg&h=480&ixid=MnwxfDB8MXxyYW5kb218MHx8fHx8fHx8MTY1ODk5MzIzNg&ixlib=rb-1.2.1&q=80&w=640';

    // ignore: unused_local_variable
    List<MerchantPromotionModel> promotions = [
      MerchantPromotionModel.create(
          id: 1,
          imageUrl: imageUrl,
          title: "MerchantPromotionTitle".tr,
          description: "MerchantPromotionSubtitle".tr.replaceAll('{0}', '30%'))
    ];

    merchantPromotion = promotions;
  }

  void getPromotionHistory() {
    String imageUrl =
        'https://images.unsplash.com/photo-1656576412427-9c560349c7e1?crop=entropy&cs=tinysrgb&fit=crop&fm=jpg&h=480&ixid=MnwxfDB8MXxyYW5kb218MHx8fHx8fHx8MTY1ODk5MzIzNg&ixlib=rb-1.2.1&q=80&w=640';

    List<MerchantPromotionModel> histories = [
      MerchantPromotionModel.create(
          id: 1,
          imageUrl: imageUrl,
          title: "MerchantPromotionTitle".tr,
          description: "MerchantPromotionSubtitle".tr.replaceAll('{0}', '30%'))
    ];

    promotionHistory = histories;
  }
  //#endregion

  //#region All Command

  void shareLink() async {
    return; //Temporary disable this feature
    // final KSystemService systemService = KSystemService();
    // String value =
    //     "${"MerchantPromotionTitle".tr} https://promotion.canamerchant.com.kh/xEk2efep7euys";
    // systemService.shareLinkedAsync(value);
  }

  Future<void> toCreateMerchantPromotionScreen() async {
    final result = await toNamedArgumentsAsync(Routes.merchantPromotionRequest);
    if (result != null) {
      changeOptionStatus(result);
    }
  }

  Future<void> toMerchantPromotionDetailScreen(
      CreateMerchantPromotionOutputModel item) async {
    var arguments = <String, dynamic>{
      ArgumentKey.item: item,
    };
    final result =
        await toNamedArgumentsAsync(Routes.merchantDetail, args: arguments);
    if (result != null) {
      merchantPromotion.clear();
      changeOptionStatus(result);
    }
  }
  //#endregion
}
