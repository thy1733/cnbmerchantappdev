// ignore_for_file: no_leading_underscores_for_local_identifiers, invalid_use_of_protected_member

import 'package:flutter/material.dart';
import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

import 'dart:io' show Platform;

class CreateMerchantPromotionScreenController extends AppController {
  //#region All Override method

  @override
  void onInit() async {
    repeatOption = MerchantPromotionRepeatOption.values
        .map((e) => OptionItemModel.create(e.displayTitle.tr, []))
        .toList();
    discountOption = MerchantPromotionDiscountOption.values
        .map((e) => OptionItemModel.create(e.displayTitle.tr, []))
        .toList();
    rateOption = ratePercentageList
        .map((e) => OptionItemModel.create("$e%", []))
        .toList();
    super.onInit();
  }

  //#endregion

  //#region All Properties

  final fixedAmountTextController = TextEditingController();
  final coveredByBankTextController = TextEditingController();
  final coveredByMerchantTextController = TextEditingController();

  final ScrollController scrollController = ScrollController();

  final _isBankFieldFocused = false.obs;
  bool get isBankFieldFocused => _isBankFieldFocused.value;
  set isBankFieldFocused(bool value) => _isBankFieldFocused.value = value;

  final _isMerchantFieldFocused = false.obs;
  bool get isMerchantFieldFocused => _isMerchantFieldFocused.value;
  set isMerchantFieldFocused(bool value) =>
      _isMerchantFieldFocused.value = value;

  final _createPromotion = CreateMerchantPromotionInputModel().obs;
  CreateMerchantPromotionInputModel get createPromotion =>
      _createPromotion.value;
  set createPromotion(CreateMerchantPromotionInputModel value) =>
      _createPromotion.value = value;

  final _repeatOption = <OptionItemModel>[].obs;
  List<OptionItemModel> get repeatOption => _repeatOption.value;
  set repeatOption(List<OptionItemModel> value) => _repeatOption.value = value;

  String get selectedRepeatOptionForDisplay =>
      createPromotion.repeatOptionIndex != null
          ? repeatOption[createPromotion.repeatOptionIndex!].name.tr
          : "PleaseSelect".tr;

  TextStyle get selectedRepeatOptionTextStyle => TextStyle(
      color: createPromotion.repeatOptionIndex != null
          ? AppColors.textPrimary
          : AppColors.textSecondary);

  final _discountOption = <OptionItemModel>[].obs;
  List<OptionItemModel> get discountOption => _discountOption.value;
  set discountOption(List<OptionItemModel> value) =>
      _discountOption.value = value;

  String get selectedDiscountOptionForDisplay =>
      createPromotion.discountOptionIndex != null
          ? discountOption[createPromotion.discountOptionIndex!].name.tr
          : "PleaseSelect".tr;

  TextStyle get selectedDiscountOptionTextStyle => TextStyle(
      color: createPromotion.discountOptionIndex != null
          ? AppColors.textPrimary
          : AppColors.textSecondary);

  final _rateOption = <OptionItemModel>[].obs;
  List<OptionItemModel> get rateOption => _rateOption.value;
  set rateOption(List<OptionItemModel> value) => _rateOption.value = value;

  String get selectedRateOptionForDisplay =>
      rateOption[createPromotion.rateOptionIndex].name;

  List<int> get ratePercentageList => [
        90,
        60,
        40,
        30,
        20,
      ];

  bool get isSwapButtonEnabled =>
      createPromotion.converedByBothValidatorMessage.isEmpty &&
      createPromotion.coveredByBankValue.isNotEmpty &&
      createPromotion.coveredByMerchantValue.isNotEmpty;
  //#endregion

  //#region All Method Helpers

  void clearCoveredByBothFields() {
    coveredByBankTextController.clear();
    coveredByMerchantTextController.clear();
  }

  void onCoveredByBankChanged(String? value) {
    if (isBankFieldFocused) {
      createPromotion.coveredByBankValue = coveredByBankTextController.text;
      var total = createPromotion.isRatePercentageSelected
          ? ratePercentageList[createPromotion.rateOptionIndex]
          : createPromotion.fixedAmountValue;
      var val = createPromotion.isRatePercentageSelected
          ? int.tryParse(coveredByBankTextController.text)
          : double.tryParse(coveredByBankTextController.text);
      debugPrint("***TOTAL: $total");
      debugPrint("***VALUE: $val");
      if (val == null) {
        coveredByMerchantTextController.clear();
      } else if (val <= total) {
        var diff = total - val;
        coveredByMerchantTextController.text =
            createPromotion.isRatePercentageSelected
                ? diff.toString()
                : diff.toStringAsFixed(2);
        createPromotion.coveredByMerchantValue =
            coveredByMerchantTextController.text;
        createPromotion.converedByBothValidatorMessage = "";
      } else {
        createPromotion.converedByBothValidatorMessage = "Invalid".tr;
      }
    }
  }

  void onCoveredByMerchantChanged(String? value) {
    // if (isMerchantFieldFocused) {
    //   createPromotion.coveredByMerchantValue =
    //       coveredByMerchantTextController.text;
    //   var total = int.tryParse(createPromotion.fixedAmountValue);
    //   var val = int.tryParse(coveredByMerchantTextController.text);
    //   if (val == null) {
    //   } else if (total != null) {
    //     createPromotion.converedByBothValidatorMessage =
    //         val < total ? "" : "Invalid".tr;
    //     if (val < total) {
    //       coveredByBankTextController.text = (total - val).toString();
    //       createPromotion.coveredByBankValue = coveredByBankTextController.text;
    //     }
    //   }
    // }
  }

  void onCoveredBySelectedRadioButtonChanged(
      MerchantPromotionCoveredBy status) {
    createPromotion.coveredBy = status;
    if (status == MerchantPromotionCoveredBy.both) {
      scrollToBottom();
    }
  }

  void _showFromDate(BuildContext context) async {
    DateTime now = DateTime.now();
    final DateTime minimumDate = DateTime(now.year, now.month, now.day);
    if (Platform.isIOS) {
      await showModalBottomSheet<DateTime>(
          context: context,
          builder: (_) {
            return IOSDatePicker(
                initialDateTime: createPromotion.date.fromDate,
                minimumDate: minimumDate,
                onDonePress: (pickedDate) {
                  if (pickedDate != null) {
                    createPromotion.date.fromDate = pickedDate;
                    if (pickedDate.isAfter(createPromotion.date.toDate)) {
                      createPromotion.date.toDate = pickedDate;
                    }
                  }
                });
          });
    } else {
      final pickedDate = await showDatePicker(
          context: context,
          initialDate: createPromotion.date.fromDate,
          builder: (_context, child) => kDatePickerThemData(_context, child),
          firstDate: minimumDate,
          lastDate: DateTime(3000));
      if (pickedDate != null) {
        createPromotion.date.fromDate = pickedDate;
        if (pickedDate.isAfter(createPromotion.date.toDate)) {
          createPromotion.date.toDate = pickedDate;
        }
      }
    }
  }

  void _showToDate(BuildContext context) async {
    if (Platform.isIOS) {
      await showModalBottomSheet<DateTime>(
          context: context,
          builder: (_) {
            return IOSDatePicker(
              initialDateTime: createPromotion.date.toDate,
              minimumDate: createPromotion.date.fromDate,
              onDonePress: (pickedDate) {
                if (pickedDate != null) {
                  createPromotion.date.fromDate = pickedDate;
                }
              },
            );
          });
    } else {
      final pickedDate = await showDatePicker(
          context: context,
          initialDate: createPromotion.date.toDate,
          builder: (_context, child) => kDatePickerThemData(_context, child),
          firstDate: createPromotion.date.fromDate,
          lastDate: DateTime(3000));
      if (pickedDate != null) {
        createPromotion.date.toDate = pickedDate;
      }
    }
  }

  void onFixedAmountEditingChanged(String? value) {
    clearCoveredByBothFields();

    if (value != null) {
      createPromotion.fixedAmountValue = double.tryParse(value) ?? 0.0;
      createPromotion.fixedAmountValidatorMessage =
          value.isEmpty ? "AmountIsRequired".tr : "";
    }
  }

  void proceedSubmit() async {
    if (createPromotion.coveredBy == MerchantPromotionCoveredBy.bank) {
      debugPrint(
          "Covered by bank: ${createPromotion.isRatePercentageSelected ? "${createPromotion.ratePercentageValue}%" : "${createPromotion.fixedAmountValue} USD"}");
    } else if (createPromotion.coveredBy ==
        MerchantPromotionCoveredBy.merchant) {
      debugPrint(
          "Covered by merchant: ${createPromotion.isRatePercentageSelected ? "${createPromotion.ratePercentageValue}%" : "${createPromotion.fixedAmountValue} USD"}");
    } else {
      if (createPromotion.isRatePercentageSelected) {
        debugPrint(
            "Rate: ${createPromotion.ratePercentageValue}; Bank: ${createPromotion.coveredByBankValue}%; Merchant: ${createPromotion.coveredByMerchantValue}%");
      } else {
        debugPrint(
            "Fixed Amount: ${createPromotion.fixedAmountValue}; Bank: ${createPromotion.coveredByBankValue} USD; Merchant: ${createPromotion.coveredByMerchantValue} USD");
      }
    }
    var des = "CorporateSuccess".tr;
    await KAlertScreen.success(
        message: "Success".tr,
        description: des,
        primaryButtonTitle: "Done".tr,
        onPrimaryButtonClick: () {
          Get.back();
          Get.back(result: 1);
        });
  }

  void scrollToBottom() {
    Future.delayed(const Duration(milliseconds: 50), () {
      scrollController.jumpTo(scrollController.position.maxScrollExtent);
    });
  }
  //#endregion

  //#region All Command

  void onCoveredByBankFieldFocused() {
    isBankFieldFocused = true;
    isMerchantFieldFocused = false;
    scrollToBottom();
  }

  void onCoveredByMerchantFieldFocused() {
    isMerchantFieldFocused = true;
    isBankFieldFocused = false;
    scrollToBottom();
  }

  void onSubmit() async {
    if (createPromotion.isValid()) {
      proceedSubmit();
    }
  }

  void onTermConditionAgreementChanged() =>
      createPromotion.isAgreed = !createPromotion.isAgreed;

  void onSwapTapped() {
    debugPrint("Swap button enabled");
    var temp = coveredByBankTextController.text.toString();
    coveredByBankTextController.text =
        coveredByMerchantTextController.text.toString();
    coveredByBankTextController.selection = TextSelection.fromPosition(
        TextPosition(offset: coveredByBankTextController.text.length));
    coveredByMerchantTextController.text = temp;
    coveredByMerchantTextController.selection = TextSelection.fromPosition(
        TextPosition(offset: coveredByMerchantTextController.text.length));
  }

  Future<void> onBottomSheetTapped(
      BuildContext context, MerchantPromotionAction action) async {
    switch (action) {
      case MerchantPromotionAction.startDate:
        _showFromDate(context);
        break;
      case MerchantPromotionAction.endDate:
        _showToDate(context);
        break;
      case MerchantPromotionAction.repeat:
        _showRepeatOption(context);
        break;
      case MerchantPromotionAction.discount:
        _showDiscountOption(context);
        break;
      case MerchantPromotionAction.rate:
        _showRateOption(context);
        break;
    }
  }

  Future<dynamic> _showRateOption(BuildContext context) {
    return showCupertinoModalBottomSheet(
        topRadius: const Radius.circular(23),
        barrierColor: AppColors.barrierBackground,
        context: context,
        builder: (_) => BottomSheetSelectOption(
              title: "Rate".tr,
              items: rateOption,
              currentIndex: 0,
              isDatePicker: false,
              onSelectedItem: (i, _) {
                createPromotion.rateOptionIndex = i;
                createPromotion.ratePercentageValue = ratePercentageList[i];
                clearCoveredByBothFields();
              },
            )..selectedItemIndex = createPromotion.rateOptionIndex);
  }

  Future<dynamic> _showDiscountOption(BuildContext context) {
    return showCupertinoModalBottomSheet(
        topRadius: const Radius.circular(23),
        barrierColor: AppColors.barrierBackground,
        context: context,
        builder: (_) => BottomSheetSelectOption(
              title: "DiscountOption".tr,
              items: discountOption,
              currentIndex: 0,
              isDatePicker: false,
              onSelectedItem: (i, _) {
                createPromotion.discountOptionIndex = i;

                clearCoveredByBothFields();
                if (createPromotion.isRatePercentageSelected) {
                  createPromotion.ratePercentageValue =
                      ratePercentageList[createPromotion.rateOptionIndex];
                  createPromotion.fixedAmountValue = 0.0;
                } else {
                  fixedAmountTextController.clear();
                  createPromotion.ratePercentageValue = 0;
                }
              },
            )..selectedItemIndex = createPromotion.discountOptionIndex);
  }

  Future<dynamic> _showRepeatOption(BuildContext context) {
    return showCupertinoModalBottomSheet(
        topRadius: const Radius.circular(23),
        barrierColor: AppColors.barrierBackground,
        context: context,
        builder: (_) {
          return BottomSheetSelectOption(
            title: "Repeat".tr,
            isDatePicker: false,
            items: repeatOption,
            currentIndex: 0,
            onSelectedItem: (i, _) {
              createPromotion.repeatOptionIndex = i;
            },
          )..selectedItemIndex = createPromotion.repeatOptionIndex;
        });
  }

  //#endregion

}
