import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class NotificationNewsDetailScreenController extends BaseWebController {
  //#region All Override method
  @override
  void onInit() async {
    isLoading = true;
    getArgument();
    super.onInit();
  }

  //#region All Properties

  final _title = 'Detail'.obs;
  String get title => _title.value;
  set title(String value) => _title.value = value;

  final _captionMessage = 'Caption'.obs;
  String get captionMessage => _captionMessage.value;
  set captionMessage(String value) => _captionMessage.value = value;

  final _detailUrl = ''.obs;
  String get detailUrl => _detailUrl.value;
  set detailUrl(String value) => _detailUrl.value = value;

  //#endregion

  //#region All Method Helpers
  void getArgument() {
    if (Get.arguments != null && Get.arguments[ArgumentKey.detail] != null) {
      detailUrl = Get.arguments[ArgumentKey.detail] as String;
    }
  }

  //#endregion

  //#region All Command
  void checkInternet() async {
    isOnline = await checkInternetConnection();
  }

  retryAsync() async {
    final hasInternet = await checkInternetConnection();
    if (!hasInternet) {
      CChoiceDialog.ok(
          title: "NoInternetConnection".tr,
          message: "NoInternetConnectionDetails".tr);
    } else {
      isOnline = hasInternet;
    }
  }
  //#endregion
}
