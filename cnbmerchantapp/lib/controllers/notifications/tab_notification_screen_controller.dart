// ignore_for_file: invalid_use_of_protected_member

import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class TabNotificationScreenController extends BaseWebController {
  final INotifyService _notifyservice = NotifyService();
  final RefreshController refreshController =
      RefreshController(initialRefresh: false);
  late final PageController pageController;
  late InAppWebViewController? newsWebViewController;
  late KWebViewWidget? webViewWidget;

  //#region All Override method

  @override
  void onInit() async {
    pageController = PageController(initialPage: currentIndex, keepPage: true);
    // todo: implement onInit
    super.onInit();
  }


  //#endregion

  //#region All Properties

  final _currentIndex = 0.obs;
  int get currentIndex => _currentIndex.value;
  set currentIndex(int value) => _currentIndex.value = value;

  final _notificationList = NotificationListOutputModel().obs;
  NotificationListOutputModel get notificationList => _notificationList.value;
  set notificationList(NotificationListOutputModel value) =>
      _notificationList.value = value;

  final _isEmptyData = false.obs;
  bool get isEmptyData => _isEmptyData.value;
  set isEmptyData(bool value) => _isEmptyData.value = value;

  String _currentNewsLinkHandle =
      ValueConst.notificationNewsURL.addMultipleLanguagesSupport();

  final _notifyItemList = <NotifyItemOutputModel>[].obs;
  List<NotifyItemOutputModel> get notifyItemList => _notifyItemList.value;
  set notifyItemList(List<NotifyItemOutputModel> value) =>
      _notifyItemList.value = value;

  //#endregion

  //#region All Method Helpers

  Future<void> loadCompleteAync() async {
    await getListAsync();
    refreshController.loadComplete();
  }

  Future<void> refreshCompletedAync() async {
    await getListAsync();
    refreshController.refreshCompleted();
  }

  Future<void> onSwape(int index) async {
    currentIndex = index;
    await pageController.animateToPage(
      currentIndex,
      duration: const Duration(milliseconds: 200),
      curve: Curves.linear,
    );
    if (index == 1) {
      checkInternet();
      if (isOnline) {
        loadWebViewError = false;
      }
    }
    _currentNewsLinkHandle =
        ValueConst.notificationNewsURL.addMultipleLanguagesSupport();
  }

  void checkNewWebViewUrl() async {
    if (isOnline == false) {
      isOnline = await checkInternetConnection();
    }
    var newsLink = ValueConst.notificationNewsURL.addMultipleLanguagesSupport();
    if (currentIndex == 1 && _currentNewsLinkHandle != newsLink) {
      newsWebViewController?.loadUrl(
        urlRequest: URLRequest(
          url: Uri.parse(newsLink),
        ),
      );
      _currentNewsLinkHandle = newsLink;
    }
  }

  Future<void> getListAsync({bool isCheckEmptyList = false}) async {
      if(isCheckEmptyList && notifyItemList.isNotEmpty)return;
    showLoading();
    try {
      var resultDto = await _notifyservice.getItemListAsync();
      if (resultDto.success) {
        notifyItemList = resultDto.result!;
        isNoData = notifyItemList.isEmpty;
      }
    } catch (e) {
      e.printError();
    }
    dismiss();
  }

  Future<void> toTxnDetailAsync(NotifyItemOutputModel item) async {
    if (!isLoading) {
      var arguments = <String, dynamic>{
        ArgumentKey.item: item.bankref,
        ArgumentKey.fromPage: ArgumentValues.notifyDetail
      };
      await super.navigationToNamedArgumentsAsync(Routes.transactionDetail,
          args: arguments);
    }
  }

  //#endregion

  //#region All Command

  NavigationActionPolicy onTap(String url) {
    if (url.contains("p=")) {
      var arg = {ArgumentKey.detail: url};
      toNamedArgumentsAsync(Routes.newsDetail, args: arg);

      return NavigationActionPolicy.CANCEL;
    }
    return NavigationActionPolicy.ALLOW;
  }

  void checkInternet() async {
    isOnline = await checkInternetConnection();
  }

  retryAsync() async {
    final hasInternet = await checkInternetConnection();
    if (!hasInternet) {
      CChoiceDialog.ok(
          title: "NoInternetConnection".tr,
          message: "NoInternetConnectionDetails".tr);
    } else {
      isOnline = hasInternet;
    }
  }
  //#endregion
}
