import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class BaseWebController extends AppController {
  final _loadWebViewError = false.obs;
  bool get loadWebViewError => _loadWebViewError.value;
  set loadWebViewError(bool value) => _loadWebViewError.value = value;
}
