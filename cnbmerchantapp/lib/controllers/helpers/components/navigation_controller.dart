import 'package:cnbmerchantapp/core.dart';
import 'package:cnbmerchantapp/helpers/components/snackBars/xcore.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AppRouteController extends SuperController<GetxController> {
  //#region override methods
  @override
  void onDetached() {}

  @override
  void onInactive() {}

  @override
  void onPaused() {}

  @override
  void onResumed() {}

  //#endregion

  //#region route to page
  void navigationBackToFirstScreen() {
    Navigator.popUntil(Get.context!, (route) => route.isFirst);
  }

  Future<T?> navigationToPageArgumentsAsync<T>(
    T page, {
    dynamic args,
    Transition transition = Transition.fadeIn,
    Duration duration = const Duration(milliseconds: 250),
  }) async {
    var result = await Get.to<T>(() => page,
        duration: duration,
        fullscreenDialog: true,
        transition: transition,
        arguments: args);
    return result;
  }

  Future<String> navigationToNamedArgumentsAsync(String name,
      {dynamic args, bool preventDuplicate = true}) async {
    var result = await Get.toNamed(name,
        arguments: args, preventDuplicates: preventDuplicate);
    return result ?? "";
  }

  Future<dynamic> toNamedArgumentsAsync(String name,
      {dynamic args, bool preventDuplicate = true}) async {
    var result = await Get.toNamed(name,
        arguments: args, preventDuplicates: preventDuplicate);
    return result;
  }

  Future<String> navigationToOffNamedAsync(String name,
      {bool preventDuplicate = true}) async {
    var result = await Get.offNamed(name, preventDuplicates: preventDuplicate);
    return result ?? "";
  }

  Future<String> navigationToOffAllNamedAsync(String name,
      {dynamic args, bool preventDuplicate = true}) async {
    var result = await Get.offAllNamed(name, arguments: args);
    return result ?? "";
  }

  Future<dynamic> toScreenAsync(dynamic name, {dynamic args}) async {
    var result = await Get.to(name,
        duration: const Duration(milliseconds: 250),
        fullscreenDialog: true,
        transition: Transition.fadeIn,
        arguments: args);
    return result ?? "";
  }

  void backAsync() => Get.back();

  void showLoading() {
    // stop timeout count
    AppSessionTimeoutInstant.pauseSession();
    if (Get.isDialogOpen! == false) {
      Get.dialog(const InitLoadingDialog(),
          barrierDismissible: false,
          barrierColor: Colors.black.withOpacity(0.6));
    }
  }

  void dismiss() {
    // begin timeout count
    AppSessionTimeoutInstant.beginSession();
    if (Get.isDialogOpen!) Get.back();
  }

  //#endregion

  //#region helpers method
  Future<void> showSnackBar(
      {required bool isSuccess,
      required String title,
      required String message,
      int duration = 5}) async {
    Get.showSnackbar(
      CSnackBar.appGetSnackBar(isSuccess, title, message, duration),
    );
  }
  //#endregion
}
