import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';

class AppPickerController extends AppRouteController {
  //#region Helper methods

  Future<XFile?> pickImageSourceAsync() async {
    final ImagePicker picker = ImagePicker();
    var picGallery = await picker.pickImage(
      source: ImageSource.gallery,
    );
    return picGallery;
  }

  Future<CroppedFile?> pickImagePushAsync(
      {int quality = 100, double maxH = 500, double maxW = 500}) async {
    final ImagePicker picker = ImagePicker();
    try {
      var result = await Get.dialog(
        ImagePickerDialog(
          onTakePressed: () async {
            try {
              var picCamera = await picker.pickImage(
                  source: ImageSource.camera,
                  imageQuality: quality,
                  maxHeight: maxH,
                  maxWidth: maxW);
              if (picCamera != null) {
                var croppedFile = await doCroppedImage(picCamera.path);
                if (croppedFile != null) {
                  Get.back(result: croppedFile);
                }
              } else {
                Get.back();
              }
            } catch (e) {
              Get.back();
              debugPrint(e.toString());
              CChoiceDialog.alert(
                title: "Error".tr,
                message:
                    "We need Camera access permission to take photos. Please enable it in Settings."
                        .tr,
                accept: () async {
                  await openAppSettings();
                },
                acceptTitle: "Settings".tr,
              );
            }
          },
          onLibraryPressed: () async {
            try {
              var picGallery = await picker.pickImage(
                  source: ImageSource.gallery,
                  imageQuality: quality,
                  maxHeight: maxH,
                  maxWidth: maxW);
              if (picGallery != null) {
                var croppedFile = await doCroppedImage(picGallery.path);
                if (croppedFile != null) {
                  Get.back(result: croppedFile);
                }
              } else {
                Get.back();
              }
            } catch (e) {
              Get.back();
              debugPrint(e.toString());
              CChoiceDialog.alert(
                title: "Error".tr,
                message:
                    "We need Photo access permission. Please enable it in Settings."
                        .tr,
                accept: () async {
                  await openAppSettings();
                },
                acceptTitle: "Settings".tr,
              );
            }
          },
        ),
      );
      return Future.sync(() => result);
    } on Exception {
      //Handle exception of type SomeException
      debugPrint("Exception");
    } catch (e) {
      //Handle all other exceptions
      debugPrint("Error: $e");
    }
    /*
    if (await checkPhotoPermissionAsync()) {
      final ImagePicker picker = ImagePicker();
      try {
        var result = await Get.dialog(
          ImagePickerDialog(
            onTakePressed: () async {
              try {
                var picCamera = await picker.pickImage(
                  source: ImageSource.camera,
                );
                var croppedFile = await doCroppedImage(picCamera!.path);
                Get.back(result: croppedFile);
              } catch (e) {
                //print(e.toString());
                Get.back();
              }
            },
            onLibraryPressed: () async {
              try {
                var picGallery = await picker.pickImage(
                  source: ImageSource.gallery,
                );
                var croppedFile = await doCroppedImage(picGallery!.path);
                Get.back(result: croppedFile);
              } catch (e) {
                Get.back();
              }
            },
          ),
        );
        return Future.sync(() => result);
      } on Exception {
        //Handle exception of type SomeException
        debugPrint("Exception");
      } catch (e) {
        //Handle all other exceptions
        debugPrint("Error: $e");
      }
    }
    */
    return null;
  }

  Future<bool> checkPhotoPermissionAsync() async {
    if (GetPlatform.isIOS || GetPlatform.isMacOS) {
      var status = await Permission.photos.status;
      if (!status.isGranted) {
        var result = await Permission.photos.request();
        if (result.isDenied) {
          await showSnackBar(
              isSuccess: false,
              title: 'CantSelectPhoto'.tr,
              message: 'PermissonDenied'.tr);
          return false;
        } else if (!result.isGranted) {
          await openAppSettings();
          return false;
        }
      }
      return true;
    } else {
      var status = await Permission.storage.status;
      if (!status.isGranted) {
        var result = await Permission.storage.request();
        if (result.isDenied) {
          await showSnackBar(
              isSuccess: false,
              title: 'CantSelectPhoto'.tr,
              message: 'PermissonDenied'.tr);
          return false;
        } else if (!result.isGranted) {
          await openAppSettings();
          return false;
        }
      }
      return true;
    }
  }

  Future<bool> checkCameraPermissionAsync() async {
    var status = await Permission.camera.status;
    if (!status.isGranted) {
      var result = await Permission.camera.request();
      if (result.isDenied) {
        await showSnackBar(
            isSuccess: false,
            title: 'CantOpenCamera'.tr,
            message: 'PermissonDenied'.tr);
        return false;
      } else if (!result.isGranted) {
        // await openAppSettings();
        return false;
      } else {
        return true;
      }
    }
    return true;
  }

  Future<CroppedFile?> doCroppedImage(String path) async {
    CroppedFile? croppedFile = await ImageCropper().cropImage(
      sourcePath: path,
      aspectRatioPresets: [
        CropAspectRatioPreset.square,
      ],
      uiSettings: [
        AndroidUiSettings(
            toolbarTitle: 'Cropper'.tr,
            toolbarColor: AppColors.primary,
            toolbarWidgetColor: Colors.white,
            initAspectRatio: CropAspectRatioPreset.original,
            hideBottomControls: true,
            lockAspectRatio: true),
        IOSUiSettings(
          title: 'Cropper'.tr,
        ),
      ],
    );
    return croppedFile;
  }

  //#endregion
}
