import 'dart:async';

import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class RLayoutFrameController extends AppController {
  late int timeOut = 0;
  late Timer timer = Timer(const Duration(), () {});
  late bool isCheckTimeOut = false;

  void initializeTimer(int seconds) {
    timer = Timer(Duration(seconds: seconds), _logOutUser);
  }

  void _logOutUser() async {
    timer.cancel();

    // Popping all routes and pushing welcome screen
    await updatePINRequiredAsync();
    // final MainScreenController mainScreenController =
    //     Get.find<MainScreenController>();
    // mainScreenController.updateTab(0);
    Navigator.of(Get.context!).popUntil((route) => route.isFirst);
  }

  void pospondTimer() {
    if (timer.isActive) {
      timer.cancel();
    }
  }

  Future<void> updatePINRequiredAsync() async {
    // userSetting = await userSettingService.getAsync();
    // userSetting.isRequiredPIN = true;
    // await userSettingService.updateConfigAsync(userSetting);
  }

  void handleUserInteraction([_]) async {
    if (timer.isActive) {
      timer.cancel();
    }
    // if (isCheckTimeOut == true) {
    //   timeOut = await getTimeOutAsync();
    //   _initializeTimer(timeOut);
    // }
  }

  // Future<int> getTimeOutAsync() async {
  //   var result = await userSettingService.getAsync();
  //   return result.timeOutSeconds;
  // }
}