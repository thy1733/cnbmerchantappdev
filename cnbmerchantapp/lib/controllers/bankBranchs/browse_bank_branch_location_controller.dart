// ignore_for_file: invalid_use_of_protected_member

import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:cnbmerchantapp/core.dart';
import 'dart:ui' as ui;

class BrowseBankBranchLocationController extends AppController {
  //#region All Override method
  @override
  void onInit() {
    darkThemMode();
    super.onInit();
  }

  // @override
  // void onReady() {
  //   getBankOfficeLocationAsync();
  //   super.onReady();
  // }

  @override
  void dispose() {
    lightThemMode();
    super.dispose();
  }

  //#endregion

  //#region All Properties
  final GeolocatorPlatform _geolocatorPlatform = GeolocatorPlatform.instance;
  final IBankOfficeService _bankOfficeService = BankOfficeService();

  final _isMapAvalable = false.obs;
  bool get isMapAvalable => _isMapAvalable.value;
  set isMapAvalable(bool value) => _isMapAvalable.value = value;

  final _isMapLoading = false.obs;
  bool get isMapLoading => _isMapLoading.value;
  set isMapLoading(bool value) => _isMapLoading.value = value;

  final _isSearching = false.obs;
  bool get isSearching => _isSearching.value;
  set isSearching(bool value) => _isSearching.value = value;

  final _isOnSearching = false.obs;
  bool get isOnSearching => _isOnSearching.value;
  set isOnSearching(bool value) => _isOnSearching.value = value;

  List<BankOfficeOutputModel> originalBankBranchItems = <BankOfficeOutputModel>[];

  final _bankBranchItems = <BankOfficeOutputModel>[].obs;
  List<BankOfficeOutputModel> get bankBranchItems => _bankBranchItems.value;
  set bankBranchItems(List<BankOfficeOutputModel> value) =>
      _bankBranchItems.value = value;

  BankOfficeOutputModel? locationOriginalModel;

  static final _emptyBranch = BankOfficeOutputModel(
    locationName: 'Head Office',
    locationAddress: "Phnom Penh",
  );

  final _selectedBranch = _emptyBranch.obs;
  BankOfficeOutputModel get selectedBranch => _selectedBranch.value;
  set selectedBranch(BankOfficeOutputModel value) =>
      _selectedBranch.value = value;

  final _currentLocation = const Position(
          latitude: 0.0,
          longitude: 0.0,
          timestamp: null,
          accuracy: 50.0,
          altitude: 0.0,
          heading: 0.0,
          speed: 1.0,
          speedAccuracy: 1.0)
      .obs;
  Position get currentLocation => _currentLocation.value;
  set currentLocation(Position value) => _currentLocation.value = value;

  Completer<GoogleMapController> mapController = Completer();

  final _initialCameraPosition =
      const CameraPosition(target: LatLng(11.549164, 104.911742), zoom: 16.0)
          .obs;
  CameraPosition get initialCameraPosition => _initialCameraPosition.value;
  set initialCameraPosition(CameraPosition value) =>
      _initialCameraPosition.value = value;

  // LIST MARKERS
  Set<Marker> markers = {};

  BitmapDescriptor? makerIconSelected;
  BitmapDescriptor? makerIcon;

  //#endregion

  //#region Validation
  //#endregion

  // #region All Method Helpers
  Future<void> getBankOfficeLocationAsync() async {
    isMapLoading = true;
    var result = await _bankOfficeService.getListBankOfficeAsync(BankOfficeListInputModel());
    isMapLoading = false;
    if (result.success && result.result != null) {
      originalBankBranchItems.assignAll(result.result!.items);
      bankBranchItems.assignAll(result.result!.items);
    } else {
      // bankBranchItems = BankOfficeOutputModel.sampleList();
    }
    if (locationOriginalModel == null && selectedBranch.id.isEmpty) {
      selectedBranch = originalBankBranchItems.first;
    }
  }

  ///
  /// this function need to call on every show this page
  ///
  void initialData(BankOfficeOutputModel? initialValue) async {
    isLoading = true;
    mapController = Completer();
    locationOriginalModel = initialValue;
    selectedBranch = initialValue ?? BankOfficeOutputModel();

    isOnSearching = false;

    onSearchBranchList();

    // CHECK AND BUILD MARKER ICON
    await buildMarkerIcon();

    // Maker
    generateMarker(name: selectedBranch.locationName);

    // Move Camera
    initialMapCamera(
      LatLng(
        selectedBranch.latitude,
        selectedBranch.longitude,
      ),
    );

    await getCurrentPositionAsync();
    isLoading = false;
  }

  // #region BRANCH_LIST

  ///
  /// To filter [bankBranchItems] by [filter]
  ///
  /// we filter base on these properties: [BankOfficeOutputModel.name],[BankOfficeOutputModel.locationAddress.locationName],[BankOfficeOutputModel.locationAddress.account]
  ///
  ///
  void onSearchBranchList({String filter = ""}) {
    if (filter.isNotEmpty) {
      bankBranchItems = originalBankBranchItems
          .where((e) => (e.locationName
                  .toLowerCase()
                  .contains(filter.toLowerCase()) ||
              e.locationAddress.toLowerCase().contains(filter.toLowerCase()) ||
              e.locationName.toLowerCase().contains(filter.toLowerCase())))
          .toList();
    } else {
      bankBranchItems = originalBankBranchItems;
    }
  }

  ///
  /// if user select on [selectedBranch] we don't do anything to make busy less
  ///
  ///
  /// if [item] different from [selectedBranch]
  /// we make [selectedBranch] equal to [item] and move map to point base on [selectedBranch.locationAddress]
  /// finaly close list view to show map at selected point
  ///
  ///
  void onSelectSearchListItem(BankOfficeOutputModel item) async {
    try {
      isMapLoading = true;

      if (item != selectedBranch) {
        selectedBranch = item;

        await moveMapToCenterAsync(
          LatLng(
            selectedBranch.latitude,
            selectedBranch.longitude,
          ),
        );

        generateMarker(name: selectedBranch.locationName);

        await cancelSearchOrGoBack();
      }
    } catch (e) {
      e.printError();
    } finally {
      isMapLoading = false;
    }
  }

  // #endregion BRANCH_LIST

  // #region Map
  ///
  /// to set map camera position on start up page by [latLng]
  ///
  ///
  initialMapCamera(LatLng latLng) {
    initialCameraPosition = CameraPosition(target: latLng, zoom: 10.0);
  }

  ///
  /// When user click on GPS button
  ///
  /// read and ask permission and get current position then move camera to current postion base on [currentLocation]
  ///
  ///
  void currentLocationAsync() async {
    isMapLoading = true;
    try {
      if (!await _handlePermission()) {
        Get.showSnackbar(const GetSnackBar(
          title: "Permission Denise",
          message: "We're sorry.",
          isDismissible: true,
          duration: Duration(milliseconds: 100),
        ));
        return;
      }

      await getCurrentPositionAsync();

      await moveMapToCenterAsync(
          LatLng(currentLocation.latitude, currentLocation.longitude));
    } catch (e) {
      e.printError();
    } finally {
      isMapLoading = false;
    }
  }

  ///
  /// ** To get current position of user
  ///
  /// !!** This don't move map camera to current user position just give value to [currentLocation]
  ///
  ///
  Future<void> getCurrentPositionAsync() async {
    if (currentLocation.latitude == 0.0 && currentLocation.longitude == 0.0) {
      currentLocation = await Geolocator.getCurrentPosition(
          desiredAccuracy: LocationAccuracy.best);
    }
  }

  ///
  /// ** Move camera to a point depend on [latLng] and [zoom] we set
  /// by default [zoom] set to 16.0
  ///
  Future<void> moveMapToCenterAsync(LatLng latLng, {double zoom = 16.0}) async {
    var value = await mapController.future;

    await value.animateCamera(CameraUpdate.newLatLngZoom(
        LatLng(latLng.latitude, latLng.longitude), zoom));

    value.dispose();
  }

  ///
  /// ** To check and request permission for app to get location of user
  ///
  Future<bool> _handlePermission() async {
    bool serviceEnabled;
    LocationPermission permission;

    // Test if location services are enabled.
    serviceEnabled = await _geolocatorPlatform.isLocationServiceEnabled();
    if (!serviceEnabled) {
      // Location services are not enabled don't continue
      // accessing the position and request users of the
      // App to enable the location services.
      return false;
    }

    permission = await _geolocatorPlatform.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await _geolocatorPlatform.requestPermission();

      if (permission == LocationPermission.denied) {
        // Permissions are denied, next time you could try
        // requesting permissions again (this is also where
        // Android's shouldShowRequestPermissionRationale
        // returned true. According to Android guidelines
        // your App should show an explanatory UI now.
        await showSnackBar(
            isSuccess: false,
            duration: 7,
            title: 'PermissionDenied'.tr,
            message:
                'WehaveNoPermissionToGetLocationPleaseCheckAndTryAgain'.tr);
        return false;
      }
    }

    if (permission == LocationPermission.deniedForever) {
      CChoiceDialog.okCancel(
          title: 'PermissionDenied'.tr,
          message: 'PermissionDeniedForeverMessage'.tr,
          okTitle: 'OpenAppSetting'.tr,
          onOk: () async {
            Get.back();
            await _geolocatorPlatform.openAppSettings();
          });

      return false;
    }

    // When we reach here, permissions are granted and we can
    // continue accessing the position of the device.
    return true;
  }

  // FUNCTION
  ///
  ///
  /// ** Genrate Marker for google map
  ///
  ///
  /// base on [name]
  /// if marker [BankOfficeOutputModel.name] == [name] will show [makerIconSelected] as marker icon
  /// else will show [makerIcon] as maker icon
  ///
  /// ----------------------------------
  ///
  Future<void> generateMarker({String name = ""}) async {
    isLoading = true;
    markers = {};
    for (var e in bankBranchItems) {
      markers.add(Marker(
          //add first marker
          markerId: MarkerId(e.locationName),
          position: LatLng(e.latitude, e.longitude), //position of marker
          onTap: () {
            if (selectedBranch == e) {
              selectedBranch = _emptyBranch;
            } else {
              selectedBranch = e;
            }
            generateMarker(name: selectedBranch.locationName);
          },
          icon: (e.locationName == name ? makerIconSelected : makerIcon) ??
              BitmapDescriptor.defaultMarker));
    }
    isLoading = false;
  }

  Future<BitmapDescriptor> _bitmapDescriptorFromSvgAsset(
    BuildContext context,
    String assetName,
  ) async {
    String svgString =
        await DefaultAssetBundle.of(context).loadString(assetName);
    //Draws string representation of svg to DrawableRoot
    DrawableRoot svgDrawableRoot = await svg.fromSvgString(svgString, 'a');
    ui.Picture picture =
        svgDrawableRoot.toPicture(size: const Size.fromHeight(100));
    ui.Image image = await picture.toImage(100, 100);
    ByteData? bytes = await image.toByteData(format: ui.ImageByteFormat.png);

    if (bytes == null) {
      return BitmapDescriptor.defaultMarker;
    }
    return BitmapDescriptor.fromBytes(bytes.buffer.asUint8List());
  }

  ///
  ///  buildMarkerIcon()
  /// for check and give value to [makerIconSelected] and [makerIcon] ot use in map maker
  ///
  Future<void> buildMarkerIcon() async {
    makerIconSelected ??= await _bitmapDescriptorFromSvgAsset(
        Get.context!, SvgAppAssets.locationOnSelected);

    makerIcon ??= await _bitmapDescriptorFromSvgAsset(
        Get.context!, SvgAppAssets.locationOn);
  }

  ///
  /// {getBytesFromAsset(String path, int width)}
  /// This function use for generate image asset from [path] to [Uint8List]
  ///
  Future<Uint8List> getBytesFromAsset(String path, int width) async {
    try {
      ByteData data = await rootBundle.load(path);
      ui.Codec codec = await ui.instantiateImageCodec(
        data.buffer.asUint8List(),
        targetWidth: width,
      );
      ui.FrameInfo fi = await codec.getNextFrame();
      return (await fi.image.toByteData(format: ui.ImageByteFormat.png))!
          .buffer
          .asUint8List();
    } catch (e) {
      return Uint8List(0);
    }
  }

// #endregion
// #endregion

  //#region All Command

  ///
  /// cancelSearchOrGoBack()
  /// [BackButton.onPressed] to handle should hide list or navigate back to leave this page
  /// it depend on [isOnSearching]
  ///
  Future<void> cancelSearchOrGoBack() async {
    if (isOnSearching) {
      isOnSearching = false;
      FocusManager.instance.primaryFocus?.unfocus();
    } else {
      selectedBranch = locationOriginalModel ?? selectedBranch;
      Get.back();
    }
  }
  //#endregion
}
