import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class WebViewScreenController extends BaseWebController {
  final _title = ''.obs;
  String get title => _title.value;
  set title(String value) => _title.value = value;

  final _url = ''.obs;
  String get url => _url.value;
  set url(String value) => _url.value = value;

  @override
  void onInit() async {
    final args = Get.arguments;
    if (args == null) return;
    title = args[ArgumentKey.pageTitle] as String? ?? "";
    url = args[ArgumentKey.detail] as String? ?? "";
    super.onInit();
  }

  retryAsync() async {
    isOnline = await checkInternetConnection();
    if (!isOnline) {
      CChoiceDialog.ok(
          title: "NoInternetConnection".tr,
          message: "NoInternetConnectionDetails".tr);
      return;
    }
  }
}
