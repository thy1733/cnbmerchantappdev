import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'dart:async';

class VerifyOtpScreenController extends AppController {
  final TextEditingController textEditingController = TextEditingController();
  final AuthenticateService _authenticateService = AuthenticateService();
  final LoginService _loginService = LoginService();
  final shakeKey = GlobalKey<ShakeWidgetState>();
  final EventChannel eventChannel =
      const EventChannel("com.cnbmerchantapp.mobile/sms_retriever");

  // #region All Properties

  String fromPage = "";

  final _timeDisplay = ''.obs;
  String get timeDisplay => _timeDisplay.value;
  set timeDisplay(String value) => _timeDisplay.value = value;

  late Timer _timer;

  late OnVerifyOTPSuccess onSuccess;

  final _counter = 0.obs;
  int get counter => _counter.value;
  set counter(int value) => _counter.value = value;

  final _isSecondTime = true.obs;
  bool get isSecondTime => _isSecondTime.value;
  set isSecondTime(bool value) => _isSecondTime.value = value;

  final _isOtpIncorrect = false.obs;
  bool get isOtpIncorrect => _isOtpIncorrect.value;
  set isOtpIncorrect(bool value) => _isOtpIncorrect.value = value;

  final _verifyOtpInputModel = VerifyOtpInputModel().obs;
  VerifyOtpInputModel get verifyOtpInputModel => _verifyOtpInputModel.value;
  set verifyOtpInputModel(VerifyOtpInputModel value) =>
      _verifyOtpInputModel.value = value;

  // #endregion

  //#region All Override method
  @override
  void onReady() {
    startTimer();
    smsRetriever();
    super.onReady();
  }
  //#endregion

  //#region All Method Helpers

  void smsRetriever() {
    if (GetPlatform.isAndroid) {
      eventChannel.receiveBroadcastStream().map((otpCode) {
        textEditingController.text = otpCode.toString();
      }).listen((event) {});
    }
  }

  void onInvalidOtp() {
    shakeKey.currentState?.shake();
    isOtpIncorrect = true;
  }

  void onOtpChanging(String value) {
    if (value.isNotEmpty) {
      isOtpIncorrect = false;
    }
  }

  Future<void> verifyOTPAsync(String otpCode) async {
    if (isLoading) return;
    showLoading();
    verifyOtpInputModel.otpCode = otpCode;
    var result = await _authenticateService.verifyOtpAsync(verifyOtpInputModel);
    dismiss();
    if (result.error == null) {
      if (result.success && result.result != null) {
        onSuccess(otpCode, result.result!.cidOrAccountNumber);
      } else {
        onInvalidOtp();
        textEditingController.clear();
      }
    } else {
      CChoiceDialog.ok(title: "Failure".tr, message: result.error!.details.tr);
    }
  }

  Future<void> resendAsync(
      {required String activateNumber,
      required String phoneNumber,
      required ActivateType activateType}) async {
    if (isLoading) return;
    showLoading();

    if (fromPage == Routes.forgotPassword) {
      UserForgotPassInputModel input = UserForgotPassInputModel()
        ..activationNumber = activateNumber
        ..phoneNumber = phoneNumber;
      var result = await _loginService.forgotPasswordAsync(input);
      dismiss();
      if (result.error == null) {
        if (result.success) {
          startTimer();
        } else {
          CChoiceDialog.ok(
              title: "Failure".tr,
              message: result.result?.description.tr ??
                  "SomethingWentWrongPleaseTryAgainLater".tr);
        }
      } else {
        CChoiceDialog.ok(
            title: "Failure".tr, message: result.error!.details.tr);
      }
    } else {
      var activateInput = ActivateInputModel()
        ..cidOrAccountNumber = activateNumber
        ..phoneNumber = phoneNumber
        ..activateType = activateType.index;
      var result = await _authenticateService.activateAsync(activateInput);
      dismiss();
      if (result.error == null) {
        if (result.success) {
          startTimer();
        } else {
          CChoiceDialog.ok(
              title: "Failure".tr,
              message: result.result?.description.tr ??
                  "SomethingWentWrongPleaseTryAgainLater".tr);
        }
      } else {
        CChoiceDialog.ok(
            title: "Failure".tr, message: result.error!.details.tr);
      }
    }
  }

  void startTimer() {
    counter = 60;
    timeDisplay = '';
    _timer = Timer.periodic(const Duration(seconds: 1), (timer) {
      if (_counter > 0) {
        timeDisplay = "${formatMMSS(counter)}.";
        counter--;
      } else {
        //on end timer
        _timer.cancel();
        if (isSecondTime) {
          isSecondTime = false;
        }
      }
    });
  }
  //#endregion

  //#region All Command
  //#endregion
}
