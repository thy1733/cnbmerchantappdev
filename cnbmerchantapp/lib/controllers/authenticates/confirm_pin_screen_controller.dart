import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

typedef RequestCallback = Function(bool, String);

class ConfirmPinScreenController extends AppController {
  //#region All Override method

  //#endregion

  //#region All Properties

  final shakeKey = GlobalKey<ShakeWidgetState>();
  TextEditingController pinCodeFieldController = TextEditingController();

  Function(RequestCallback callback)? onSuccess;

  final _isPinIncorrect = false.obs;
  bool get isPinIncorrect => _isPinIncorrect.value;
  set isPinIncorrect(bool value) => _isPinIncorrect.value = value;

  final _isConfirmPinIncorrect = false.obs;
  bool get isConfirmPinIncorrect => _isConfirmPinIncorrect.value;
  set isConfirmPinIncorrect(bool value) => _isConfirmPinIncorrect.value = value;

  final _isShowError = false.obs;
  bool get isShowError => _isShowError.value;
  set isShowError(bool value) => _isShowError.value = value;

  final _pinConfirmValue = ''.obs;
  String get pinConfirmValue => _pinConfirmValue.value;
  set pinConfirmValue(String value) => _pinConfirmValue.value = value;

  String _pinValue = '';

  bool _isDisableTouch = false;

  //#endregion

  //#region All Method Helpers

  Future<void> checkPinSession() async {}

  //#endregion

  //#region All Command

  void onNumPressed(String value) async {
    if (_isDisableTouch) return;

    isConfirmPinIncorrect = false;

    confirmPinCheck(value);
  }

  void confirmPinCheck(String value) async {
    if (_pinValue.length < ValueConst.appPinLength) {
      _pinValue += value;
      pinCodeFieldController.text = _pinValue;
      if (_pinValue.length == ValueConst.appPinLength) {
        if (_pinValue == AppUserTempInstant.appCacheUser.userPin) {
          if (onSuccess != null) {
            onSuccess!(((isSuccess, message) {
              if (isSuccess) {
                Get.back(result: true);
              } else {
                shakeKey.currentState?.shake();
                clearPin();
                CChoiceDialog.ok(title: "Failure".tr, message: message.tr);
              }
            }));
          }
        } else {
          shakeKey.currentState?.shake();
          _isDisableTouch = true;
          isConfirmPinIncorrect = true;
          await Future.delayed(const Duration(milliseconds: 520));
          clearPin();
        }
      }
    }
  }

  void clearPin() async {
    _pinValue = '';
    pinCodeFieldController.text = _pinValue;
    _isDisableTouch = false;
  }

  void onClearPress() {
    isConfirmPinIncorrect = false;
    _pinValue = '';
    pinCodeFieldController.text = _pinValue;
  }

  void onBackSpacePressed() {
    if (_pinValue.isNotEmpty) {
      _pinValue = _pinValue.substring(0, _pinValue.length - 1);
      pinCodeFieldController.text = _pinValue;
    }
    // isPinIncorrect = false;
  }

  void clearPinValue() {
    _pinValue = '';
    isConfirmPinIncorrect = false;
    isPinIncorrect = false;
  }

  //#endregion
}
