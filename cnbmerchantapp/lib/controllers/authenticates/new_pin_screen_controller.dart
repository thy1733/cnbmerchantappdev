import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class NewPinScreenController extends AppController {
  final IBusinessService _businessService = BusinessService();
  //#region All Override method
  @override
  void onInit() {
    getArgument();
    super.onInit();
  }

  //#endregion

  //#region All Properties
  final _title = ''.obs;
  String get title => _title.value;
  set title(String value) => _title.value = value;

  final _subTitle = ''.obs;
  String get subTitle => _subTitle.value;
  set subTitle(String value) => _subTitle.value = value;

  String fromPage = "";

  bool _isDisableTouch = false;
  final shakeKey = GlobalKey<ShakeWidgetState>();
  TextEditingController pinCodeFieldController = TextEditingController();

  final _isConfirmPinIncorrect = false.obs;
  bool get isConfirmPinIncorrect => _isConfirmPinIncorrect.value;
  set isConfirmPinIncorrect(bool value) => _isConfirmPinIncorrect.value = value;

  final _isPinIncorrect = false.obs;
  bool get isPinIncorrect => _isPinIncorrect.value;
  set isPinIncorrect(bool value) => _isPinIncorrect.value = value;

  final _errorLabel = ''.obs;
  String get errorLabel => _errorLabel.value;
  set errorLabel(String value) => _errorLabel.value = value;

  //model part
  UserLoginInputModel userInput = UserLoginInputModel();

  // register
  final AuthenticateService _authenticateService = AuthenticateService();
  final LoginService _loginService = LoginService();

  final _registerAccountInput = RegisterAccountInputModel().obs;
  RegisterAccountInputModel get registerAccountInput =>
      _registerAccountInput.value;
  set registerAccountInput(RegisterAccountInputModel value) =>
      _registerAccountInput.value = value;

  String respoonseMcid = "";
  // #endregion

  // #region private field
  String _pinValue = '';
  // String pinConfirmValue = '';

  final _pinConfirmValue = ''.obs;
  String get pinConfirmValue => _pinConfirmValue.value;
  set pinConfirmValue(String value) => _pinConfirmValue.value = value;
  //#endregion

  //#region All Method Helpers
  // ON_START SECTION
  void getArgument() {
    if (Get.arguments == null) return;
    if (Get.arguments[ArgumentKey.fromPage] != null) {
      fromPage = Get.arguments[ArgumentKey.fromPage] as String;
      checkPageOnStart();
    }
    if (Get.arguments[ArgumentKey.item] != null) {
      registerAccountInput =
          Get.arguments[ArgumentKey.item] as RegisterAccountInputModel;
    }
  }

  void checkPageOnStart() {
    setupSubTitle();
    switch (fromPage) {
      case Routes.activateOwnerScreen:
        fromPageActivateOwner();
        break;
      case Routes.activateExistingMerchantScreen:
        fromPageActivateOwner();
        break;
      case Routes.activateOwnerExistingAccount:
        fromPageActivateOwner();
        break;
      case Routes.activateCashierScreen:
        fromPageActivateCashier();
        break;
      case Routes.resetPin:
        fromPageResetPin();
        break;
      default:
    }
  }

  void setupSubTitle() {
    if (fromPage == Routes.resetPin && pinConfirmValue.isEmpty) {
      subTitle = "EnterYourNewDigitsPin"
          .tr
          .replaceAll("{0}", "${ValueConst.appPinLength}");
    } else if (fromPage == Routes.resetPin && pinConfirmValue.isNotEmpty) {
      subTitle = "ConfirmNewPin".tr;
    } else if (pinConfirmValue.isNotEmpty) {
      subTitle = 'ConfirmPin'.tr;
    } else {
      subTitle =
          "SetUpPinTitle".tr.replaceAll("{0}", "${ValueConst.appPinLength}");
    }
  }

  /// FOR_FLOW_ACTIVATE_OWNER
  void fromPageActivateOwner() {
    title = "ActivateAsBusinessOwner".tr;
  }

  /// FOR_FLOW_ACTIVATE_OWNER
  void fromPageActivateCashier() {
    title = "ActivateAsCashier".tr;
  }

  /// FOR_FLOW_ACTIVATE_OWNER
  void fromPageResetPin() {
    title = "ResetPIN".tr;
  }

  // PIN_AND_CONFIRM_PIN_CORRECT
  void checkPageOnSuccess() async {
    switch (fromPage) {
      case Routes.activateOwnerScreen:
        await activateOwnerAsync();
        break;
      case Routes.activateExistingMerchantScreen:
        await activateExistingMerchantAsync();
        break;
      case Routes.activateOwnerExistingAccount:
        await activateOwnerExistingAccountAsync();
        break;
      case Routes.activateCashierScreen:
        await activateCashierAsync();
        break;
      case Routes.resetPin:
        await resetPINAsync();
        break;
      default:
    }
    // TO CLEAR PIN PROCESS
    await Future.delayed(const Duration(milliseconds: 100));
    onCancelBtnPressed();
  }

  /// FOR_FLOW_ACTIVATE_OWNER
  Future<void> activateOwnerAsync() async {
    // to implement register user as owner
    var result = await registerAsync(isCashier: false);
    if (result) {
      var arguments = <String, dynamic>{
        ArgumentKey.fromPage: fromPage,
        ArgumentKey.item: registerAccountInput
      };
      await KAlertScreen.success(
        message: "Congratulation".tr,
        description: 'ActivateIsComplete'.tr,
        primaryButtonTitle: 'GetStarted'.tr,
      );

      navigationToOffAllNamedAsync(Routes.setupBusinessProfile,
          args: arguments);
    }
  }

  /// FOR_FLOW_ACTIVATE_EXISTING_MERCHANT
  Future<void> activateExistingMerchantAsync() async {
    // to implement register user as owner
    var result = await registerAsync(isCashier: false);
    if (result) {
      var arguments = <String, dynamic>{
        ArgumentKey.fromPage: fromPage,
        ArgumentKey.item: registerAccountInput
      };
      var isHasBusiness = false;
      if(respoonseMcid.isNotEmpty){
        isHasBusiness = await checkMerchantExistingBusinessAsync(respoonseMcid);
      }
      await KAlertScreen.success(
        message: "Congratulation".tr,
        description: 'ActivateIsComplete'.tr,
        primaryButtonTitle: 'GetStarted'.tr,
      );
      if(isHasBusiness){
        navigationToOffAllNamedAsync(Routes.main);
      }else{
        navigationToOffAllNamedAsync(Routes.setupBusinessProfile, args: arguments);
      }
    }
  }

  Future<bool> checkMerchantExistingBusinessAsync(String mcid) async{
    var input = ManageBusinessListInputModel()..mCID =  mcid;
    var result = await _businessService.getListBusiness(input);
    if(result.success && result.result!=null){
      return result.result!.items.isNotEmpty;
    }
    return false;
  }

  Future<void> activateOwnerExistingAccountAsync() async {
    // to implement register user as owner
    var result = await registerAsync(isCashier: false);
    if (result) {
      await KAlertScreen.success(
        message: "Congratulation".tr,
        description: 'YourBusinessAreReadyToReceivePayment'.tr,
        primaryButtonTitle: 'GetStarted'.tr,
      );
      navigationToOffAllNamedAsync(Routes.main);
    }
  }

  Future<void> activateCashierAsync() async {
    // to implement register user as owner
    var result = await registerAsync(isCashier: true);
    if (result) {
      await KAlertScreen.success(
        message: "Congratulation".tr,
        description: 'ActivateIsComplete'.tr,
        primaryButtonTitle: 'StartNow'.tr,
        onPrimaryButtonClick: () => navigationToOffAllNamedAsync(Routes.main),
      );
    }
  }

  Future<bool> registerAsync({required bool isCashier}) async {
    showLoading();
    registerAccountInput.pinCode = _pinValue;
    // REQUEST_REGISTER
    var result =
        await _authenticateService.registerAcountAsync(registerAccountInput);

    // clearPin();

    // CHECK_RESULT_TO_SHOW_MESSAGE

    if (result.error == null) {
      if (result.success) {
        await _loginService.loginAsync(UserLoginInputModel.create(
            registerAccountInput.userName,
            registerAccountInput.password));
        respoonseMcid = result.result!.mCID;
        return result.success;
      } else {
        await KAlertScreen.failure(
          message: "Failure".tr,
          description: result.result?.description ?? "",
          primaryButtonTitle: 'TryAgain'.tr,
        );
        return false;
      }
    } else {
      CChoiceDialog.ok(title: "Failure".tr, message: result.error!.details.tr);
      return false;
    }
  }

  /// this function is sample when doing reset PIN flow
  Future<void> resetPINAsync() async {
    var args = <String, dynamic>{
      ArgumentKey.fromPage: fromPage,
      ArgumentKey.item: pinConfirmValue,
    };
    //open ConfirmPasswordScreen screen
    await toNamedArgumentsAsync(Routes.confirmPassword, args: args);
  }

  /// this function is sample when doing onactivate by invite code flow

  // PIN_PART

  void onIncorrectConfirmPin() async {
    isConfirmPinIncorrect = true;
    shakeKey.currentState?.shake();
    _isDisableTouch = true;
    await Future.delayed(const Duration(milliseconds: 520));
    clearPin();
  }

  void onNumPressed(String value) async {
    if (_isDisableTouch) return;
    isConfirmPinIncorrect = false;

    setupPinCheck(value);
  }

  void setupPinCheck(String value) async {
    try {
      if (pinConfirmValue.isEmpty) {
        _pinValue += value;
        pinCodeFieldController.text = _pinValue;
        if (_pinValue.length == ValueConst.appPinLength) {
          pinConfirmValue = _pinValue;
          _pinValue = '';
          pinCodeFieldController.text = _pinValue;
          setupSubTitle();
        }
      } else {
        if (_pinValue.length < ValueConst.appPinLength) {
          _pinValue += value;
          pinCodeFieldController.text = _pinValue;
          if (_pinValue.length == ValueConst.appPinLength) {
            if (pinConfirmValue == _pinValue) {
              // when confirm done! final step
              checkPageOnSuccess();
            } else {
              onIncorrectConfirmPin();
            }
          }
        }
      }
    } catch (e) {
      e.printError();
    }
  }

  //#endregion

  //#region Validation
  //#endregion

  //#region All Command

  void onCancelBtnPressed() {
    if (pinConfirmValue.isNotEmpty) {
      pinConfirmValue = '';
      _pinValue = '';
      pinCodeFieldController.text = _pinValue;
      isConfirmPinIncorrect = false;
    } else {
      Get.back();
    }
  }

  void clearPin() async {
    _pinValue = '';
    _isDisableTouch = false;
    pinCodeFieldController.text = _pinValue;
  }

  void onClearPress() {
    _pinValue = '';
    isConfirmPinIncorrect = false;
    pinCodeFieldController.text = _pinValue;
  }

  void onBackSpacePressed() {
    if (_pinValue.isNotEmpty) {
      _pinValue = _pinValue.substring(0, _pinValue.length - 1);
      _isDisableTouch = false;
      pinCodeFieldController.text = _pinValue;
    }
  }
  //#endregion

}
