import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class NewPasswordScreenController extends AppController {
  final IAuthenticateService _authenticateService = AuthenticateService();

  //#region All Override method
  @override
  void onInit() {
    getArgument();
    super.onInit();
  }

  //#endregion

  //#region All Properties

  final _title = ''.obs;
  String get title => _title.value;
  set title(String value) => _title.value = value;

  String fromPage = "";

  final _newPasswordValidateMessage = ''.obs;
  String get newPasswordValidateMessage => _newPasswordValidateMessage.value;
  set newPasswordValidateMessage(String value) =>
      _newPasswordValidateMessage.value = value;

  final _confirmPasswordValidateMessage = ''.obs;
  String get confirmPasswordValidateMessage =>
      _confirmPasswordValidateMessage.value;
  set confirmPasswordValidateMessage(String value) =>
      _confirmPasswordValidateMessage.value = value;

  final _newPasswordInput = ''.obs;
  String get newPasswordInput => _newPasswordInput.value;
  set newPasswordInput(String value) => _newPasswordInput.value = value;

  final _confirmPasswordInput = ''.obs;
  String get confirmPasswordInput => _confirmPasswordInput.value;
  set confirmPasswordInput(String value) => _confirmPasswordInput.value = value;

  final _isPasswordVisible = false.obs;
  bool get isPasswordVisible => _isPasswordVisible.value;
  set isPasswordVisible(bool value) => _isPasswordVisible.value = value;

  final _isConfirmedPasswordVisible = false.obs;
  bool get isConfirmedPasswordVisible => _isConfirmedPasswordVisible.value;
  set isConfirmedPasswordVisible(bool value) =>
      _isConfirmedPasswordVisible.value = value;

  bool get shouldShowConfirmedField =>
      newPasswordInput.isNotEmpty && newPasswordValidateMessage.isEmpty;

  RegisterAccountInputModel _registerAccountInput = RegisterAccountInputModel();
  //#endregion

  //#region All Method Helpers

  // ON_START SECTION
  void getArgument() {
    if (Get.arguments == null) return;
    if (Get.arguments[ArgumentKey.fromPage] != null) {
      fromPage = Get.arguments[ArgumentKey.fromPage] as String;
      checkPageOnStart();
    }
  }

  void checkPageOnStart() {
    switch (fromPage) {
      case Routes.forgotPassword:
        fromPageForgotPassword();
        break;
      case Routes.activateOwnerScreen:
        fromPageActivateOwner();
        break;
      case Routes.activateExistingMerchantScreen:
        fromPageActivateOwner();
        break;
      case Routes.activateOwnerExistingAccount:
        fromPageActivateOwner();
        break;
      case Routes.activateCashierScreen:
        fromPageActivateCashier();
        break;
      case Routes.changePassword:
        fromPageChangePassword();
        break;
      default:
    }
  }

  /// FOR_FLOW_FOGOT_PASSWORD
  void fromPageForgotPassword() {
    title = "ForgotPassword".tr;
  }

  /// FOR_FLOW_ACTIVATE_OWNER
  void fromPageActivateOwner() {
    title = "ActivateAsBusinessOwner".tr;
    if (Get.arguments[ArgumentKey.item] != null) {
      _registerAccountInput =
          Get.arguments[ArgumentKey.item] as RegisterAccountInputModel;
    }
  }

  /// FOR_FLOW_ACTIVATE_CASHIER
  void fromPageActivateCashier() {
    title = "ActivateAsCashier".tr;
    if (Get.arguments[ArgumentKey.item] != null) {
      _registerAccountInput =
          Get.arguments[ArgumentKey.item] as RegisterAccountInputModel;
    }
  }

  void fromPageChangePassword() {
    title = "ChangePassword".tr;
  }

  // ON_NEXT_PRESS SECTION
  void checkPageOnNextBtnPress() {
    switch (fromPage) {
      case Routes.forgotPassword:
        forgetPasswordAsync();
        break;
      case Routes.activateOwnerScreen:
        activateOwnerAsync();
        break;
      case Routes.activateExistingMerchantScreen:
        activateOwnerAsync();
        break;
      case Routes.activateOwnerExistingAccount:
        activateOwnerAsync();
        break;
      case Routes.activateCashierScreen:
        activateCashierAsync();
        break;
      case Routes.changePassword:
        changePasswordAsync();
        break;
      default:
    }
  }

  /// FOR_FLOW_CHANGE_PASSWORD
  Future<void> changePasswordAsync() async {
    var arguments = {
      ArgumentKey.fromPage: Routes.changePassword,
      ArgumentKey.item: newPasswordInput
    };
    super.navigationToNamedArgumentsAsync(Routes.confirmPassword,
        args: arguments);
  }

  /// FOR_FLOW_FOGOT_PASSWORD
  Future<void> forgetPasswordAsync() async {
    var resetPasswordInput = ResetPasswordInputModel();
    // SAMPLE_INPUT_DATA
    if (Get.arguments[ArgumentKey.item] != null) {
      final accountNumberOrInviteCode =
          Get.arguments[ArgumentKey.item] as String;
      resetPasswordInput.accountNumberOrInviteCode = accountNumberOrInviteCode;
    }
    resetPasswordInput.newPassword = newPasswordInput;

    showLoading();
    var result =
        await _authenticateService.resetPasswordAsync(resetPasswordInput);
    dismiss();

    if (result.error == null) {
      if (result.success && result.result != null) {
        KAlertScreen.success(
            message: "Success".tr,
            description: "ResetPasswordMessage".tr,
            primaryButtonTitle: "Done".tr,
            onPrimaryButtonClick: () {
              navigationToOffAllNamedAsync(Routes.login);
            });
      } else {
        KAlertScreen.failure(
          message: "Failure".tr,
          description: result.result?.description.tr ??
              "SomethingWentWrongPleaseTryAgainLater".tr,
        );
      }
    } else {
      CChoiceDialog.ok(title: "Failure".tr, message: result.error!.details.tr);
    }
  }

  /// FOR_FLOW_ACTIVATE_OWNER
  Future<void> activateOwnerAsync() async {
    _registerAccountInput.password = newPasswordInput;
    var arguments = <String, dynamic>{
      ArgumentKey.fromPage: fromPage,
      ArgumentKey.item: _registerAccountInput
    };
    toNamedArgumentsAsync(Routes.newPinScreen, args: arguments);
  }

  /// FOR_FLOW_ACTIVATE_CASHIER
  Future<void> activateCashierAsync() async {
    _registerAccountInput.password = newPasswordInput;
    var arguments = <String, dynamic>{
      ArgumentKey.fromPage: fromPage,
      ArgumentKey.item: _registerAccountInput
    };
    toNamedArgumentsAsync(Routes.newPinScreen, args: arguments);
  }
  //#endregion

  //#region All Command
  void onNextBtnPress() {
    FocusManager.instance.primaryFocus?.unfocus();
    newPasswordValidator();
    confirmPasswordValidator();

    if (isEnable) {
      checkPageOnNextBtnPress();
    }
  }

  //#endregion

  //#region Validation

  void newPasswordValidator() {
    if (!validatePassword(newPasswordInput)) {
      // CASE: PASSWORD_INVALID
      newPasswordValidateMessage = "PasswordInvalidMessage";
    } else if (newPasswordInput.isEmpty) {
      // CASE: INVITE_EMPTY
      newPasswordValidateMessage = "PasswordRequiredMessage";
    } else if (confirmPasswordInput.isNotEmpty &&
        newPasswordInput != confirmPasswordInput) {
      newPasswordValidateMessage = "PasswordNotMatch";
    } else {
      newPasswordValidateMessage = "";
      if (newPasswordInput == confirmPasswordInput) {
        confirmPasswordValidateMessage = "";
      }
    }
    checkEnable();
  }

  void confirmPasswordValidator() {
    if (newPasswordInput.isNotEmpty &&
        confirmPasswordInput != newPasswordInput) {
      // CASE: PASSWORD_NOT_MATCH
      confirmPasswordValidateMessage = "PasswordNotMatch";
    } else if (confirmPasswordInput.isEmpty) {
      // CASE: INVITE_EMPTY
      confirmPasswordValidateMessage = "ConfirmPasswordRequiredMessage";
    } else if (!validatePassword(confirmPasswordInput)) {
      confirmPasswordValidateMessage = "PasswordInvalidMessage";
    } else {
      confirmPasswordValidateMessage = "";
      if (confirmPasswordInput == newPasswordInput) {
        newPasswordValidateMessage = "";
      }
    }
    checkEnable();
  }

  void checkEnable() {
    isEnable = confirmPasswordValidateMessage.isEmpty &&
        newPasswordValidateMessage.isEmpty &&
        newPasswordInput.isNotEmpty &&
        confirmPasswordInput.isNotEmpty;
  }
  //#endregion
}
