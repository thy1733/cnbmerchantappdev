import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'dart:async';

class LocationScreenController extends AppController {
  final MapService _locationService = MapService();
  final Completer<GoogleMapController> mapController = Completer();
  EdgeInsets safeArea = const EdgeInsets.all(20);
  final Duration animateDuration = const Duration(milliseconds: 200);
  //late bool isSearchingPlace = false;

  /* ************ all override method *******************
 * All override method.
 * Should declare all method in this section.
 ************************************************** */
  @override
  void onInit() async {
    isMapAvalable = true;
    super.onInit();
  }

  @override
  void onReady() async {
    safeArea = MediaQuery.of(Get.context!).viewPadding;
    super.onReady();
  }

  @override
  void onClose() {
    isMapAvalable = true;
    FocusManager.instance.primaryFocus?.unfocus();
    super.onClose();
  }

/* ************ all properties *******************
 * All properties.
 * Should declare all properties in this section.
 ************************************************** */

  //Marker pinLocation;
  //var location = LatLng(11.553181, 104.905302);

  final _isCurrent = false.obs;
  bool get isCurrent => _isCurrent.value;
  set isCurrent(bool value) => _isCurrent.value = value;

  final _isMapAvalable = false.obs;
  bool get isMapAvalable => _isMapAvalable.value;
  set isMapAvalable(bool value) => _isMapAvalable.value = value;

  final _isMapLoading = false.obs;
  bool get isMapLoading => _isMapLoading.value;
  set isMapLoading(bool value) => _isMapLoading.value = value;

  final _isSearchingPlace = false.obs;
  bool get isSearchingPlace => _isSearchingPlace.value;
  set isSearchingPlace(bool value) => _isSearchingPlace.value = value;

  final _location = const LatLng(7.553181, 23.905302).obs;
  LatLng get location => _location.value;
  set location(LatLng value) => _location.value = value;

  final _initialCameraPosition =
      const CameraPosition(target: LatLng(0.0, 0.0), zoom: 16.0).obs;
  CameraPosition get initialCameraPosition => _initialCameraPosition.value;
  set initialCameraPosition(CameraPosition value) =>
      _initialCameraPosition.value = value;

  final _locationModel = LocationAddressModel().obs;
  LocationAddressModel get locationModel => _locationModel.value;
  set locationModel(LocationAddressModel value) => _locationModel.value = value;

  final _locationOriginalModel = LocationAddressModel().obs;
  LocationAddressModel get locationOriginalModel =>
      _locationOriginalModel.value;
  set locationOriginalModel(LocationAddressModel value) =>
      _locationOriginalModel.value = value;

  final _currentLocation = const Position(
          latitude: 0.0,
          longitude: 0.0,
          timestamp: null,
          accuracy: 50.0,
          altitude: 0.0,
          heading: 0.0,
          speed: 1.0,
          speedAccuracy: 1.0)
      .obs;
  Position get currentLocation => _currentLocation.value;
  set currentLocation(Position value) => _currentLocation.value = value;

  final _searchAddress = ''.obs;
  String get searchAddress => _searchAddress.value;
  set searchAddress(String value) => _searchAddress.value = value;

  final _searchAddressList = SearchAddressModel().obs;
  SearchAddressModel get searchAddressList => _searchAddressList.value;
  set searchAddressList(SearchAddressModel value) =>
      _searchAddressList.value = value;

  //late LocationData _locationData;

/* ************ method helpers *******************
 * All methods helpers.
 * Should write all method helpers in this section.
 ************************************************** */

  createBusinessLocation(Function(LocationAddressModel) callback) async {
    // final input = CreateBusinessLocationInputDto(
    //     locationName: locationModel.locationName,
    //     latitude: locationModel.latitude,
    //     longitude: locationModel.longtitude,
    //     address: locationModel.address);

    // Testing Version
    callback(locationModel);

    // final resultData =
    //     await _businessLocationService.createBusinessLocationAsync(input);
    // if (resultData.result != null) {
    //   callback(resultData.result!);
    // } else {
    //   String errorMessage = '';
    //   try {
    //     var dioError = resultData.error as DioError?;
    //     if (dioError?.message == null) {
    //       log('Unknown Error');
    //       errorMessage = 'Unknown Error';
    //       if (dioError?.type == DioErrorType.connectTimeout) {
    //         log('Connection Timeout');
    //         errorMessage = 'Connection Timeout';
    //       }
    //     }
    //   } catch (err) {
    //     var error = resultData.error as ApiOutputError;
    //     log(error.message);
    //     errorMessage = error.message;
    //   }
    //   Get.dialog(
    //     CustomDialogBox(
    //       title: null,
    //       description: errorMessage.tr,
    //       actionTitle: 'Ok'.tr,
    //       onActionPressed: () => Get.back(),
    //     ),
    //   );
    // }
  }

  Future<String> confirmLocationAsync() async {
    var value = {ParameterKey.value: locationModel};
    Get.back(result: value);
    return '';
  }

  Future<bool> getCurrentPosition() async {
    final permission = await _handlePermission();
    if (!permission) {
      return false;
    }
    isLoading = true;
    Position position = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.best);
    currentLocation = position;
    isCurrent = position.latitude > 0 && position.longitude > 0;
    isLoading = false;
    return true;
  }

  Future<bool> _handlePermission() async {
    // Test if location services are enabled.
    bool serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      // Location services are not enabled don't continue
      // accessing the position and request users of the
      // App to enable the location services.

      CChoiceDialog.alert(
          title: "Information".tr,
          message:
              "We need location access permission to use current location. Please enable it in Settings.",
          accept: () async {
            Get.back();
            await Geolocator.openLocationSettings();
          },
          acceptTitle: "Settings".tr);
      return false;
    }

    LocationPermission permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        // Permissions are denied, next time you could try
        // requesting permissions again (this is also where
        // Android's shouldShowRequestPermissionRationale
        // returned true. According to Android guidelines
        // your App should show an explanatory UI now.
        await showSnackBar(
            isSuccess: false,
            duration: 7,
            title: 'PermissionDenied'.tr,
            message:
                'WehaveNoPermissionToGetLocationPleaseCheckAndTryAgain'.tr);
        return false;
      }
    }

    if (permission == LocationPermission.deniedForever) {
      CChoiceDialog.alert(
          title: 'PermissionDenied'.tr,
          message: 'PermissionDeniedForeverMessage'.tr,
          accept: () async {
            Get.back();
            await Geolocator.openAppSettings();
          });

      return false;
    }

    // When we reach here, permissions are granted and we can
    // continue accessing the position of the device.
    return true;
  }

  Future<void> findAddressesFromCoordinates(double lat, double lon) async {
    isMapLoading = true;
    var result = await _locationService.getLocationNameByLatLgAsync(lat, lon);
    locationModel = result;
    locationModel.latitude = lat;
    locationModel.longitude = lon;
    isMapLoading = false;
  }

  Future<void> initailizeMapCamera() async {
    initialCameraPosition = CameraPosition(
        target: LatLng(locationModel.latitude, locationModel.longitude),
        zoom: 16.0);

    await movingCameraAsync(
        LatLng(locationModel.latitude, locationModel.longitude));
  }

  Future<void> searchAddressesFromText(String value) async {
    isMapLoading = true;
    if (value.isEmpty) {
      isMapLoading = false;
      return;
    }
    var result = await _locationService.searchPlaceAddressAsync(value);
    searchAddressList = result;
    isMapLoading = false;
  }

  Future<void> getGeoLocationFromAddress(AddressListModel value) async {
    var result = await _locationService.getPlaceAddressByLatLong(value.placeId);
    locationModel = result;
    if (!isMapAvalable) {
      isMapAvalable = true;
      isMapLoading = false;
      FocusManager.instance.primaryFocus?.unfocus();
      await movingCameraAsync(
          LatLng(locationModel.latitude, locationModel.longitude));
    }
  }

  Future<void> cancelSearchOrGoBack() async {
    if (!isMapAvalable) {
      isMapAvalable = true;
      isMapLoading = false;
      FocusManager.instance.primaryFocus?.unfocus();
    } else {
      locationModel = locationOriginalModel;
      Get.back();
    }
  }

  void dismissKeyboard() {
    isMapAvalable = true;
    FocusManager.instance.primaryFocus?.unfocus();
  }

  void getCurrentLocationAsync() async {
    if (currentLocation.latitude == 0.0 && currentLocation.longitude == 0.0) {
      final currPosition = await getCurrentPosition();
      if (!currPosition) return;
    }
    await movingCameraAsync(
        LatLng(currentLocation.latitude, currentLocation.longitude));
  }

  //*** on camera move we get the current position from the center of map */
  void onCameraMove(CameraPosition position) {
    locationModel.latitude = position.target.latitude;
    locationModel.longitude = position.target.longitude;
  }

  ///** when camera move end this function is raise to get address *///
  void onCamaraIdle() {
    findAddressesFromCoordinates(
        locationModel.latitude, locationModel.longitude);
  }

  Future<void> movingCameraAsync(LatLng latLng) async {
    await moveMapToCenterAsync(LatLng(latLng.latitude, latLng.longitude));
  }

  Future<void> moveMapToCenterAsync(LatLng latLng) async {
    try {
      await mapController.future.then((value) async =>
          await value.animateCamera(CameraUpdate.newLatLngZoom(
              LatLng(latLng.latitude, latLng.longitude), 16.0)));
    } catch (e) {
      // Get.showSnackbar(const GetSnackBar(
      //   title: "Error",
      //   message: "We're sorry.",
      // ));
    }
  }
}
