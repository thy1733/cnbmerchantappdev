import 'dart:async';

import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MainScreenController extends AppController {
  final IAppOnboardingScreenService _launchService =
      AppOnboardingScreenService();
  final IAppUserProfileInfoService _appCacheService =
      AppUserProfileInfoService();
  final IAppBusinessCacheService _appBusinessCacheService =
      AppBusinessCacheService();
  final IAppSettingCacheService _appSettingCacheService =
      AppSettingCacheService();

  late TabController tabController;

  final tabHomeCon = Get.put(TabHomeScreenController());
  final notificationTabCon = Get.put<TabNotificationScreenController>(
      TabNotificationScreenController());

  final transactionTab =
      Get.put<TabTransactionScreenController>(TabTransactionScreenController());

  final IAppShieldingProvider _runCheck = AppShieldingProvider();

  //#region All Override method
  @override
  void onReady() {
    super.onReady();
    checkAppShield();
    tabController.addListener(() async {
      if (tabHomeCon.isOnline) {
        if (tabController.indexIsChanging) {
          if (tabController.index == 0 && tabHomeCon.isNeedReloadBussiness) {
            tabHomeCon.clearAnalyData();
            await tabHomeCon.fetchBusinessListAsync(isShowLoading: true);
            await tabHomeCon.onSelectedYearFilterAsync(
                tabHomeCon.availableYearsFilter.first,
                checkSelectedYear: false);
          } else if (tabController.index == 1) {
            transactionTab.getTransactionListAsync();
          } else if (tabController.index == 2) {
            notificationTabCon.getListAsync(isCheckEmptyList: true);
            notificationTabCon.checkNewWebViewUrl();
          }
        }
      }
    });

    getAppUserSettingCache();
  }
  //#endregion

  //#region All Properties

  //#endregion

  /// [logoutAsync] call clear caches data, update userLog to false, delete [MainScreenController] and route to login
  Future<void> logoutAsync() async {
    super.navigationToOffAllNamedAsync(Routes.login);
    AppSessionTimeoutInstant.stopSession();
    await _launchService.updateUserLoginAsync(false);
    await _appBusinessCacheService.clear();
    await _appCacheService.logoutAsync();
    await Get.delete<MainScreenController>();
    removeCheckAppShield();
  }

  void getAppUserSettingCache() async {
    _appSettingCacheService
        .getAppSetting(AppUserTempInstant.appCacheUser.userName);
  }

  void checkAppShield() async {
    //#region init route page at the app startup
    await _runCheck.listeningRunAppCheckingAsync(
        MethodChannelName.merchantChannelRunCheckAuto, (data) {
      var eventData = data as String;
      AppShieldingTemp.info = "";
      var items = eventData.split(",");
      if(items.length >= 2){
         AppShieldingTemp.info = " ${items[0]}";
      }
      AppShieldingTemp.isShowStatusbar = true;
    });
  }

  void removeCheckAppShield() {
    _runCheck.removeListener(MethodChannelName.merchantChannelRunCheckAuto);
  }
  //#endregion

  //#region All Command

  //#endregion
}
