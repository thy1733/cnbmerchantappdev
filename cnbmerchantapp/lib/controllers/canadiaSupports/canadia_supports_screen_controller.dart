import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';

class CanadiaSupportsScreenController extends AppController {
  final CanadiaSupportsService _canadiaSupportsService =
      CanadiaSupportsService();
  //#region All Override method


  //#endregion

  //#region All Properties

  List<CanadiaSupportModel> supports = <CanadiaSupportModel>[
    CanadiaSupportModel(
      icon: SvgAppAssets.icFeedback,
      title: "Feedback",
      subtitle: "FeedbackSubtitle",
    ),
    CanadiaSupportModel(
        icon: SvgAppAssets.icPhone,
        title: "CallSupportTeam",
        subtitle: "023 868 222"),
    CanadiaSupportModel(
        icon: SvgAppAssets.icOnlineChat,
        title: "OnlineChat",
        subtitle: "OnlineChatSubtitle"),
  ];

  //#endregion

  //#region All Method Helpers

  //#endregion

  //#region All Command

  Future<void> onSendFeedbackAsync(FeedbackInputModel input) async {
    try {
      showLoading();
      var result = await _canadiaSupportsService.sendFeedback(input);
      dismiss();
      if (result.success) {
        {
          debugPrint(
              "Submitting review: ${input.review} with rating: ${input.rating}");
          KAlertScreen.success(
            message: "Success".tr,
            description: "FeedbackSuccess".tr,
            primaryButtonTitle: "Done".tr,
            onPrimaryButtonClick: () => Get.close(2),
          );
        }
      } else {
        debugPrint("Failed to send feedback");
        KAlertScreen.failure(
          message: "Failure".tr,
          description: result.error?.message ?? "Failure".tr,
        );
      }
    } catch (e) {
      debugPrint(e.toString());
    }
  }

  Future<void> onTapCanadiaSupportsAsync(
      BuildContext context, int index) async {
    switch (index) {
      case 0:
        final args = {
          ArgumentKey.pageTitle: "Feedback".tr,
          ArgumentKey.detail:
              ValueConst.feedbackURL.addMultipleLanguagesSupport(),
        };
        navigationToNamedArgumentsAsync(Routes.webView, args: args);
        break;
      case 1:
        await launchCall();
        break;
      case 2:
        await launchFacebookMessenger();
        break;
      default:
        break;
    }
  }

  Future<void> launchFacebookMessenger() async {
    String messengerUrl =
        "fb-messenger://user-thread/${ValueConst.canadiaMessengerId}";
    bool canLaunch = await canLaunchUrl(Uri.parse(messengerUrl));
    if (canLaunch) {
      await launchUrl(Uri.parse(messengerUrl));
    } else {
      String url = "https://m.me/${ValueConst.canadiaMessengerId}";
      await launchUrl(Uri.parse(url), mode: LaunchMode.externalApplication);
    }
  }

  Future<void> launchCall() async {
    String url = "tel:${ValueConst.canadiaPhoneNumber}";
    bool canLaunch = await canLaunchUrl(Uri.parse(url));
    if (canLaunch) {
      await launchUrl(Uri.parse(url));
    } else {
      await launchUrl(Uri.parse(url), mode: LaunchMode.externalApplication);
    }
  }

  //#endregion
}
