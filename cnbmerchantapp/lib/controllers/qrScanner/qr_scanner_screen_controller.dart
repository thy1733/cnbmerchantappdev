import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';
import 'package:permission_handler/permission_handler.dart';

class QrScannerScreenController extends AppController {
  final qrValueGenerator = QrValueGenerator();
  //#region All Override method

  @override
  void onInit() {
    checkCameraPermission();
    super.onInit();
  }

  @override
  void onResumed() async {
    isAppActive = true;
    super.onResumed();
  }

  @override
  void onInactive() {
    isAppActive = false;
    super.onInactive();
  }

  //#endregion

  //#region All Properties

  // MobileScannerController? mcontroller;

  final _isAllowFlash = true.obs;
  bool get isAllowFlash => _isAllowFlash.value;
  set isAllowFlash(bool value) => _isAllowFlash.value = value;

  final _isFlashOn = false.obs;
  bool get isFlashOn => _isFlashOn.value;
  set isFlashOn(bool value) => _isFlashOn.value = value;

  final _qrValue = ''.obs;
  String get qrValue => _qrValue.value;
  set qrValue(String value) => _qrValue.value = value;

  final _isPermissionAllowed = true.obs;
  bool get isPermissionAllowed => _isPermissionAllowed.value;
  set isPermissionAllowed(bool value) => _isPermissionAllowed.value = value;

  final _isAppActive = true.obs;
  bool get isAppActive => _isAppActive.value;
  set isAppActive(bool value) => _isAppActive.value = value;

  final _canStartScanner = false.obs;
  bool get canStartScanner => _canStartScanner.value;
  set canStartScanner(bool value) => _canStartScanner.value = value;

  final _khqrTagList = <KHQRTagOutputDto>[].obs;
  List<KHQRTagOutputDto> get khqrTagList => _khqrTagList;
  set khqrTagList(List<KHQRTagOutputDto> value) => _khqrTagList.value = value;

  final _isScanner = false.obs;
  bool get isScanner => _isScanner.value;
  set isScanner(bool value) => _isScanner.value = value;

  //#endregion

  //#region All Method Helpers

  void checkCameraPermission() async {
    isPermissionAllowed = await checkCameraPermissionAsync();
    if (isPermissionAllowed) {
      startCameraAction();
    }
  }

  void startCameraAction() {
    //mcontroller = MobileScannerController();
    canStartScanner = true;
  }

  //#endregion

  //#region All Command
  void pickImageToScan() async {
    var imageSource = await pickImageSourceAsync();
    if (imageSource != null) {}
  }

  void qrCodeScanningResult(String result) async {
    isScanner = true;
    qrValue = result;
    var tagData = qrValueGenerator.tagToModelListMapper(qrValue);
    if (tagData != null) {
      khqrTagList = tagData;
    }
  }

  void openAppSettingAsync() async {
    await openAppSettings();
  }

  //#endregion
}
