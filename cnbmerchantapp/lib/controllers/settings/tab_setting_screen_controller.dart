import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class TabSettingScreenController extends AppController {
  late MainScreenController _mainScreenController;
  final IAppSettingCacheService _appSettingCacheService =
      AppSettingCacheService();
  final IAppUserProfileInfoService _profileInfoService =
      AppUserProfileInfoService();
  final IAuthenticateService _authenticateService = AuthenticateService();

  final IAppFileUploadService _fileUploadService = AppFileUploadService();
  //#region All Override method
  
  @override
  void onReady() {
    _mainScreenController = Get.find<MainScreenController>();
    super.onReady();
  }
  //#endregion

  //#region All Properties


  final _isPushNotification = true.obs;
  bool get isPushNotification => _isPushNotification.value;
  set isPushNotification(bool value) => _isPushNotification.value = value;

  final _isUploadingProfileImage = false.obs;
  bool get isUploadingProfileImage => _isUploadingProfileImage.value;
  set isUploadingProfileImage(bool value) =>
      _isUploadingProfileImage.value = value;

  // final _userSetting = UserSettingCachDto().obs;
  // UserSettingCachDto get userSetting => _userSetting.value;
  // set userSetting(UserSettingCachDto value) => _userSetting.value = value;

  final AppInfoHelper appInfoHelper = AppInfoHelper();

  ItemSelectOptionModel get currentSelectedLanguage {
    var locale = Get.locale;
    if (locale != null) {
      var selectedLangIndex = locale.languageCode == LanguageOption.lanKhmer
          ? 0
          : locale.languageCode == LanguageOption.lanChinese
              ? 2
              : 1;
      for (var item in languageItem) {
        item.isSelected = item.index == selectedLangIndex ? true : false;
      }
      languageSelected = languageItem[selectedLangIndex];
    }
    return languageSelected;
  }

  //#endregion

  //#region All Method Helpers

  void saveInactivityTimoutCache(int second) async {
    AppSettingInstant.setting.inactivityTimeoutSeccond = second;
    _appSettingCacheService.saveAppStting(AppSettingInstant.setting);
  }
  //#endregion

  //#region All Command
  Future<void> resetPinAsync() async {
    var args = {ArgumentKey.fromPage: Routes.resetPin};
    navigationToNamedArgumentsAsync(Routes.newPinScreen, args: args);
  }

  void sessionTimeoutCheck({
    required String route,
    dynamic args,
    String pinTitle = "ConfirmPin",
  }) async {
    await AppSessionTimeoutInstant.startSession(
      title: pinTitle.tr,
      onSuccess: (callback) {
        super
            .navigationToNamedArgumentsAsync(route, args: args)
            .then((value) => AppSessionTimeoutInstant.leaveLockRoute());
      },
    );
  }

  Future<void> timeOutScreenOptionSettingAsync() async {
    AppBottomSheet.bottomSheetSelectDynamicOption<int>(
      title: "AutoLogout".tr,
      items: AppSessionTimeoutInstant.timeoutList,
      selectedItem: AppSettingInstant.setting.inactivityTimeoutSeccond,
      onSelectedItem: (index, value) {
        if (AppSettingInstant.setting.inactivityTimeoutSeccond != value) {
          saveInactivityTimoutCache(value);
        }
      },
      closeOnSelect: true,
      separator: const SizedBox(),
      selectedItemDecoration: const BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(10)),
        color: AppColors.greyishDisable,
      ),
      itemBuilder: (index, value) => Padding(
        padding: const EdgeInsets.symmetric(vertical: 10),
        child: Text(
          convertSecondToMinute(value),
          style: AppTextStyle.body1.copyWith(color: AppColors.black),
        ),
      ),
    );
  }

  void updateProfileImageAsync() async {
    // var updateResult = await _authenticateService.updateUserProfile("202321130221026");
    // print(updateResult);
    var result = await pickImagePushAsync();
    if (result != null) {
      photoFile = await result.readAsBytes();

      if (photoFile.isEmpty) return;

      // SET_DTO
      //file name and upload to temporary file path.
      var fileName = DateTime.now().toFormatDateString("yyyymmddhhmmsss");
      // upload for outlet profile
      var fileInput = UploadFileInputDto();
      fileInput.uploadType = UploadProfileType.userProfile;
      fileInput.filePath = result.path;
      fileInput.id = AppUserTempInstant.appCacheUser.userType == 0
          ? AppUserTempInstant.appCacheUser.mCID
          : AppUserTempInstant.appCacheUser.profileId;
      bool isCreate = AppUserTempInstant.appCacheUser.profilePhotoId.isEmpty;
      fileInput.fileName =
          isCreate ? fileName : AppUserTempInstant.appCacheUser.profilePhotoId;

      // CHECK_RESULT
      if (fileInput.filePath.isNotEmpty) {
        isUploadingProfileImage = true;
        // upload to firebase
        var uploadResult =
            await _fileUploadService.uploadFileAsync(input: fileInput);
        // upload to firebase success
        if (uploadResult.fileName.isNotEmpty) {
          if (isCreate) {
            // update core banking api
            var updateResult = await _authenticateService
                .updateUserProfile(uploadResult.fileName);
            if (updateResult.success && updateResult.result != null) {
              if (updateResult.result!.isNotEmpty) {
                updateProfileCache(updateResult.result!);
              }
            }
          } else {
            updateProfileCache(uploadResult.fileName);
          }
        }
        isUploadingProfileImage = false;
      }
    }
  }

  void updateProfileCache(String profilePhotoId) async {
    AppUserTempInstant.appCacheUser.profilePhotoId = profilePhotoId;
    AppUserTempInstant.appCacheUser.photo = profilePhotoId.downloadUrl(
      UploadProfileType.userProfile,
      AppUserTempInstant.appCacheUser.userName,
      profilePhotoId,
    );
    await _profileInfoService.updateAuthenticateAsync(
      AppUserTempInstant.appCacheUser,
    );
  }

  Future<void> updateAppConfig() async {
    isPushNotification = !isPushNotification;
  }

  Future<void> logout() async {
    await CChoiceDialog.alertBottomSheetVerticalActions(
      title: "LogOut".tr,
      message: "",
      child: Padding(
        padding: const EdgeInsets.only(bottom: 15),
        child: Text.rich(
          TextSpan(
            style: AppTextStyle.caption,
            children: [
              TextSpan(text: "AreYouSureYouWouldLikeToLogOut".tr),
              // TextSpan(
              //     text: "\n${"Warning".tr}! ",
              //     style: const TextStyle(
              //       fontWeight: FontWeight.bold,
              //     )),
              // TextSpan(text: "AfterLogOutYoullHaveToLogInAgain".tr),
            ],
          ),
          textAlign: TextAlign.center,
        ),
      ),
      hasCancel: false,
      acceptTitle: "LogOut".tr,
      accept: () async {
        await _mainScreenController.logoutAsync();
      },
    );
  }

  void getProfile() {
    userProfile.displayName = AppUserTempInstant.appCacheUser.displayName;
    userProfile.firstName = AppUserTempInstant.appCacheUser.firstName;
    userProfile.lastName = AppUserTempInstant.appCacheUser.lastName;
    userProfile.displayName = AppUserTempInstant.appCacheUser.displayName;
    userProfile.phoneNumber = AppUserTempInstant.appCacheUser.phoneNumber;
    userProfile.pinCode = AppUserTempInstant.appCacheUser.userPin;
    userProfile.refundLimited = AppUserTempInstant.appCacheUser.refundLimited;
    userProfile.isRefundable = AppUserTempInstant.appCacheUser.isRefundable;
    userProfile.activateType = AppUserTempInstant.appCacheUser.userType;
    userProfile.profileId = AppUserTempInstant.appCacheUser.profileId;
  }
  //#endregion

}
