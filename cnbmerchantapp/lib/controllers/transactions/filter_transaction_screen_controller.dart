import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

class FilterTransactionScreenController extends AppController {
  final ICashierListService _cashierListService = CashierService();

  //#region All Override method

  @override
  void onInit() async {
    if (Get.arguments != null) {
      var result =
          Get.arguments[ArgumentKey.id] as TransactionListFilterInputModel;
      filterTrxnInput = result;
      selectedPaymentMethodId = result.paymentMethodId;
      selectedTransactionTypeId = result.transactionTypeId;
    }
    super.onInit();

    datePeriodOptions = DatePeriodOption.values
        .map((e) => OptionItemModel.create(e.displayTitle.tr, []))
        .toList();

    paymentMethods = FilterPaymentMethod.values
        .map((e) => OptionItemModel.create(e.getTitle.tr, []))
        .toList();
    transactionTypes = FilterTransactionType.values
        .map((e) => OptionItemModel.create(e.getTitle.tr, []))
        .toList();

    outlets.insert(
      0,
      OutletItemListOutputModel()..outletName = "AllOutlets".tr,
    );

    _fetchCashierAsync();
  }

  //#endregion

  //#region All Properties

  final _filterTrxnInput = TransactionListFilterInputModel.initial().obs;
  TransactionListFilterInputModel get filterTrxnInput => _filterTrxnInput.value;
  set filterTrxnInput(TransactionListFilterInputModel value) =>
      _filterTrxnInput.value = value;

  final _datePeriodOptions = <OptionItemModel>[].obs;
  List<OptionItemModel> get datePeriodOptions => _datePeriodOptions;
  set datePeriodOptions(List<OptionItemModel> value) =>
      _datePeriodOptions.value = value;

  String get selectedDateForDisplay => filterTrxnInput.dateId == 0
      ? "AllPeriod".tr
      : DatePeriodOption.values[filterTrxnInput.dateId] !=
              DatePeriodOption.custom
          ? datePeriodOptions[filterTrxnInput.dateId].name
          : "${kDateTimeFormat(filterTrxnInput.date.fromDate, showStandardDate: true)} - ${kDateTimeFormat(filterTrxnInput.date.toDate, showStandardDate: true)}";

  final _outlets = AppOutletTempInstant.outletList.items.obs;
  List<OutletItemListOutputModel> get outlets => _outlets;
  set outlets(List<OutletItemListOutputModel> value) => _outlets.value = value;

  String get selectedOutletForDisplay =>
      outlets
          .firstWhereOrNull((e) => e.id == filterTrxnInput.outletId)
          ?.outletName ??
      "AllOutlets".tr;

  final _cashierList = <CashierItemListOutputModel>[].obs;
  List<CashierItemListOutputModel> get cashierList => _cashierList;
  set cashierList(List<CashierItemListOutputModel> value) =>
      _cashierList.value = value;

  final _selectedCashierIndex = 0.obs;
  int get selectedCashierIndex => _selectedCashierIndex.value;
  set selectedCashierIndex(int value) => _selectedCashierIndex.value = value;

  String get selectedCashierForDisplay =>
      cashierList
          .firstWhereOrNull((e) => e.id == filterTrxnInput.cashierId)
          ?.displayName ??
      "AllCashiers".tr;

  final _paymentMethods = <OptionItemModel>[].obs;
  List<OptionItemModel> get paymentMethods => _paymentMethods;
  set paymentMethods(List<OptionItemModel> value) =>
      _paymentMethods.value = value;

  final _selectedPaymentMethodId = 0.obs;
  int get selectedPaymentMethodId => _selectedPaymentMethodId.value;
  set selectedPaymentMethodId(int value) =>
      _selectedPaymentMethodId.value = value;

  String get selectedPaymentMethodForDisplay => selectedPaymentMethodId == 0
      ? "AllPaymentMethods".tr
      : paymentMethods[selectedPaymentMethodId].name;

  final _transactionTypes = <OptionItemModel>[].obs;
  List<OptionItemModel> get transactionTypes => _transactionTypes;
  set transactionTypes(List<OptionItemModel> value) =>
      _transactionTypes.value = value;

  final _selectedTransactionTypeId = 0.obs;
  int get selectedTransactionTypeId => _selectedTransactionTypeId.value;
  set selectedTransactionTypeId(int value) =>
      _selectedTransactionTypeId.value = value;

  String get selectedTransactionTypeForDisplay => selectedTransactionTypeId == 0
      ? "AllTransactionTypes".tr
      : transactionTypes[selectedTransactionTypeId].name;

  //#endregion

  //#region All Method Helpers

  Future<void> _fetchCashierAsync() async {
    var input = CashierListInputModel();
    input.businessId = AppBusinessTempInstant.selectedBusiness.id;
    var result = await _cashierListService.getListCashier(input);
    if (result.success && result.result != null) {
      cashierList = result.result!.items;
    }
    cashierList.insert(
      0,
      CashierItemListOutputModel()
        ..firstName = "AllCashiers".tr
        ..id = "",
    );
  }

  //#endregion

  //#region All Command

  Future<void> refundTransactionAsync(String txnId) async {}

  Future<void> filterOptionsAsync(String value) async {
    if (value == "TransactionPeriod".tr) {
      showCupertinoModalBottomSheet(
          topRadius: const Radius.circular(23),
          barrierColor: AppColors.barrierBackground,
          context: Get.context!,
          builder: (_) {
            final bottomSheetSelectionOption = BottomSheetSelectOption(
              title: "TransactionPeriod".tr,
              items: datePeriodOptions,
              currentIndex: 0,
              isDatePicker: true,
              onSelectedItem: (i, date) {
                filterTrxnInput.dateId = i;
                filterTrxnInput.date = date;
              },
            );
            bottomSheetSelectionOption.selectedItemIndex =
                filterTrxnInput.dateId;
            bottomSheetSelectionOption.selectedDate = filterTrxnInput.date;
            return bottomSheetSelectionOption;
          });
    } else if (value == "Outlet".tr) {
      onSelectOutletAsync();
    } else if (value == "Cashier".tr) {
      onSelectCashierAsync();
    } else if (value == "PaymentMethod".tr) {
      showCupertinoModalBottomSheet(
          topRadius: const Radius.circular(23),
          barrierColor: AppColors.barrierBackground,
          context: Get.context!,
          builder: (_) => BottomSheetSelectOption(
                title: "PaymentMethod".tr,
                items: paymentMethods,
                currentIndex: 0,
                isDatePicker: false,
                onSelectedItem: (i, _) {
                  selectedPaymentMethodId = i;
                  filterTrxnInput.paymentMethodId = i;
                  filterTrxnInput.paymentMethodValue =
                      i == 0 ? "" : FilterPaymentMethod.values[i].getTitle;
                },
              )..selectedItemIndex = selectedPaymentMethodId);
    } else if (value == "TransactionType".tr) {
      showCupertinoModalBottomSheet(
          topRadius: const Radius.circular(23),
          barrierColor: AppColors.barrierBackground,
          context: Get.context!,
          builder: (_) => BottomSheetSelectOption(
                title: "TransactionType".tr,
                items: transactionTypes,
                currentIndex: 0,
                isDatePicker: false,
                onSelectedItem: (i, _) {
                  selectedTransactionTypeId = i;
                  filterTrxnInput.transactionTypeId = i;
                  filterTrxnInput.transactionTypeValue =
                      i == 0 ? "" : FilterTransactionType.values[i].getTitle;
                },
              )..selectedItemIndex = selectedTransactionTypeId);
    }
  }

  void onSelectOutletAsync() async {
    await AppBottomSheet.bottomSheetSelectDynamicOption<
        OutletItemListOutputModel>(
      title: "SwitchOutlet".tr,
      items: outlets,
      onSelectedItem: (index, value) {
        filterTrxnInput.outletId = value.id;
      },
      selectedItem:
          outlets.firstWhereOrNull((e) => e.id == filterTrxnInput.outletId),
      closeOnSelect: true,
      selectedItemDecoration: _seletedItemDecoration,
      itemBuilder: (index, value) => Padding(
        padding: const EdgeInsets.symmetric(vertical: 10),
        child: Text(
          value.outletName,
          style: AppTextStyle.body1.copyWith(color: AppColors.textPrimary),
        ),
      ),
    );
  }

  void onSelectCashierAsync() async {
    await AppBottomSheet.bottomSheetSelectDynamicOption<
        CashierItemListOutputModel>(
      title: "Cashier".tr,
      items: cashierList,
      onSelectedItem: (index, value) {
        filterTrxnInput.cashierId = value.id;
      },
      selectedItem: cashierList
          .firstWhereOrNull((e) => e.id == filterTrxnInput.cashierId),
      closeOnSelect: true,
      selectedItemDecoration: _seletedItemDecoration,
      itemBuilder: (index, value) => SizedBox(
        height: 50,
        child: Row(
          children: [
            if (value.id != "") ...[
              AppNetworkImage(
                value.photoUrl,
                height: 35,
                width: 35,
                shape: BoxShape.circle,
                errorWidget: SvgPicture.asset(
                  SvgAppAssets.pOutletList,
                ),
              ),
              const SizedBox(width: 10),
            ],
            Text(
              value.displayName,
              style: AppTextStyle.body1.copyWith(color: AppColors.textPrimary),
            )
          ],
        ),
      ),
    );
  }

  BoxDecoration get _seletedItemDecoration => BoxDecoration(
      color: AppColors.greyBackground, borderRadius: BorderRadius.circular(10));

  Future<void> applyFilterAsync() async {
    filterTrxnInput.hasFilter = true;
    Get.back(result: filterTrxnInput);
  }

  //#endregion
}
