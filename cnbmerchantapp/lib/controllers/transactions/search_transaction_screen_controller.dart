import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SearchTransactionScreenController extends AppController {
  final ITransactionService _transactionService = TransactionService();
  final Duration animateDuration = const Duration(milliseconds: 200);
  late List<TransactionItemListModel> itemTransactionList =
      <TransactionItemListModel>[];

  //#region All Override method

  @override
  void onInit() {
    if (Get.arguments != null) {
      itemTransactionList =
          Get.arguments[ArgumentKey.item] as List<TransactionItemListModel>;
      transactionList =
          Get.arguments[ArgumentKey.item] as List<TransactionItemListModel>;
    }
    super.onInit();
  }

  //#endregion

  //#region All Properties

  final _transactionFilter = TransactionListFilterInputModel.initial().obs;
  TransactionListFilterInputModel get transactionFilter =>
      _transactionFilter.value;
  set transactionFilter(TransactionListFilterInputModel value) =>
      _transactionFilter.value = value;

  final _transactionList = <TransactionItemListModel>[].obs;
  List<TransactionItemListModel> get transactionList => _transactionList;
  set transactionList(List<TransactionItemListModel> value) =>
      _transactionList.value = value;

  //#endregion

  //#region All Method Helpers

  Future<void> fetchTransactionList() async {
    isLoading = true;
    var resultModel = await _transactionService.getListAsync(transactionFilter);
    isNoData = resultModel.result!.items.isEmpty;
    if (resultModel.success) {
      transactionList = resultModel.result!.items;
    } else {}
    isLoading = false;
  }

  //#endregion

  //#region All Command

  Future<void> onUserChange(String text) async {
    var result = itemTransactionList
        .where((e) =>
            e.accountName.toUpperCase().contains(text.toUpperCase()) ||
            e.transactionId.toUpperCase().contains(text.toUpperCase()))
        .toList();
    transactionList = result;
  }

  Future<void> onUserSubmit(dynamic text) async {
    // request to server
    var input = text as String;
    transactionFilter.filter = input;
    await fetchTransactionList();
  }

  Future<void> onUserClear(String text) async {
    transactionList = itemTransactionList;
  }

  Future<void> toTxnDetailAsync(TransactionItemListModel item) async {
    FocusManager.instance.primaryFocus?.unfocus();
    //Get.back(result: item);
    super.lightThemMode();
    var arguments = <String, dynamic>{
      ArgumentKey.item: item,
      ArgumentKey.fromPage: "Search"
    };
    await super.navigationToNamedArgumentsAsync(Routes.transactionDetail,
        args: arguments);
  }

  //#endregion
}
