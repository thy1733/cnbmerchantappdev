import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class RefundTransactionController extends AppController {
  final TransactionService _transactionService = TransactionService();

  //#region All Override method

  @override
  void onInit() {
    if (Get.arguments != null) {
      var result =
          Get.arguments[ArgumentKey.item] as TransactionItemDetailsOutputModel?;
      if (result != null) {
        transaction = result;
      }
    }
    super.onInit();
  }

  @override
  void onClose() async {
    // Update status bar to black
    super.onClose();
  }

  //#endregion

  //#region All Properties

  final _userName = ''.obs;
  get userName => _userName.value;
  set userName(value) => _userName.value = value;

  final _remark = ''.obs;
  String get remark => _remark.value;
  set remark(String value) => _remark.value = value;

  final _validateMessage = ''.obs;
  String get validationMessage => _validateMessage.value;
  set validationMessage(String value) => _validateMessage.value = value;

  final _amountToRefund = 0.0.obs;
  double get amountToRefund => _amountToRefund.value;
  set amountToRefund(double value) => _amountToRefund.value = value;

  final _transaction = TransactionItemDetailsOutputModel().obs;
  TransactionItemDetailsOutputModel get transaction => _transaction.value;
  set transaction(TransactionItemDetailsOutputModel value) =>
      _transaction.value = value;

  //#endregion

  //#region All Method Helpers

  Future<void> updateTransactionAsync() async {}

  Future<void> getTransactionAsync() async {}

  //#endregion

  //#region All Command

  Future<void> confirmRefundTransactionAsync() async {
    showLoading();
    TransactionCreateRefundInputModel input =
        TransactionCreateRefundInputModel();
    input.amountToRefund = amountToRefund;
    input.remark = remark;
    var resultModel = await _transactionService.refundTxnAsync(input);
    dismiss();
    if (resultModel.success) {
      var description =
          '${"Refund".tr} $amountToRefund ${transaction.currency.toUpperCase()}\n${'to'.tr} ${transaction.customerBankName} (${transaction.customerAccount})';

      await requestToConfirmPIN(
        title: "Refund".tr,
        image: SvgAppAssets.confirmRefund,
        description: description,
        isPermanentRequired: true,
        message: "EnterPinToConfirm".tr,
        onBackPress: () {},
        onSuccess: (_) async {
          Get.back(result: true);
          await KAlertScreen.success(
              primaryButtonTitle: "Done".tr,
              message: "Success".tr,
              description: description,
              onPrimaryButtonClick: () {
                Get.close(2);
              });
          updateTransactionAsync();
        },
      );
    } else {
      debugPrint("Refund failed");
    }
  }

  //#endregion
}
