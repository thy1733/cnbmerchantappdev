import 'dart:io';

import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_beep/flutter_beep.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:path_provider/path_provider.dart' as path_provider;

class TransactionInvoiceScreenController extends AppController {
  final systemService = KSystemService();
  final trxController = Get.find<TabTransactionScreenController>();

  //#region All Override method

  @override
  void onReady() async {
    HapticFeedback.vibrate();
    FlutterBeep.playSysSound(iOSSoundIDs.SMSReceived_Selection21);
    FlutterBeep.playSysSound(AndroidSoundIDs.TONE_CDMA_ALERT_CALL_GUARD);
    if (Get.arguments != null) {
      if (Get.arguments[ArgumentKey.detail] != null) {
        invoiceDetail = Get.arguments[ArgumentKey.detail] as QrResultOutputModel;
        isLoading = false;
        setQrMerchantName();
      }
    } else {}
    trxController.getTransactionListAsync();
    super.onReady();
  }

  //#endregion

  //#region All Properties

  final _trxDetail = TransactionItemDetailsOutputModel().obs;
  TransactionItemDetailsOutputModel get trxDetail => _trxDetail.value;
  set trxDetail(TransactionItemDetailsOutputModel value) =>
      _trxDetail.value = value;

  final _invoiceDetail = QrResultOutputModel().obs;
  QrResultOutputModel get invoiceDetail => _invoiceDetail.value;
  set invoiceDetail(QrResultOutputModel value) => _invoiceDetail.value = value;

  TransactionItemDetailsInputModel trxInput =
      TransactionItemDetailsInputModel();

  bool isImageSaved = false;
  
  final _merchantName = ''.obs;
  String get merchantName => _merchantName.value;
  set merchantName(String value) => _merchantName.value = value;

  //#endregion

  //#region All Function
  //#endregion

  //#region All Command
  Future<void> onScreenshotAsync(Widget savedScreen, Size size) async {
    showLoading();

    await createImageFromWidget(
      SizedBox(
        width: size.width,
        height: size.height,
        child: Material(
          color: Colors.transparent,
          child: savedScreen,
        ),
      ),
      size,
    ).then((value) async {
      if (value.isNotEmpty) {
        final directory =
            (await path_provider.getApplicationDocumentsDirectory()).path;
        File imageFile = File('$directory/transactioninvoice.png');
        final file = await imageFile.writeAsBytes(value);
        await KSystemService.shareFiles(
          [file.path],
        );
      }
    });
    dismiss();
  }

  Future<void> onSaveImageAsync(Widget savedScreen, Size size) async {
    if (isImageSaved) {
      _onImageSavedSuccess();
      return;
    }

    showLoading();
    // convert Widget to Image
    ///** unknown error!
    ///  createImageFromWidget() for first convert it not complete all image.
    ///   but for second it work as well
    /// */
    try {
      await createImageFromWidget(
              SizedBox(
                width: size.width.sp,
                height: size.height.sp,
                child: Material(
                  color: Colors.transparent,
                  child: savedScreen,
                ),
              ),
              //  savedScreen,
              Size(size.width, size.height))
          .then((value) async {
        if (value.isNotEmpty) {
          final imageSaveService = ImageSaveService();

          // save image to local storage
          var result = await imageSaveService.imageFromStream(image: value);
          if (result) {
            _onImageSavedSuccess();
            isImageSaved = true;
          }
        }
      });
    } catch (e) {
      e.printError();
    }
    dismiss();
  }

  void _onImageSavedSuccess() {
    var msg = 'YourQrImageSavedtoPhoto'.tr;
    if (GetPlatform.isAndroid) {
      msg = 'YourQrImageSavedtoGallery'.tr;
    }
    Fluttertoast.cancel();
    Fluttertoast.showToast(
      msg: msg,
      toastLength: Toast.LENGTH_LONG,
      gravity: ToastGravity.BOTTOM,
      backgroundColor: AppColors.success,
    );
  }

  Future<void> toTransactionDetail() async {
    var input = TransactionItemListModel();
    input.transactionId = invoiceDetail.bankref;
    var arguments = <String, dynamic>{
      ArgumentKey.item: input,
      ArgumentKey.fromPage: ""
    };
    backAsync();
    await super.navigationToNamedArgumentsAsync(
      Routes.transactionDetail,
      args: arguments,
    );
  }

  void setQrMerchantName() {
    /// find defualt outletId
    /// defualt outletId is end with .1
    var outletId = AppOutletTempInstant.selectedOutlet.id.split('.').last;
    if (outletId == '1') {
      merchantName = AppOutletTempInstant.selectedOutlet.outletName;
    } else {
      merchantName =
          "${AppBusinessTempInstant.selectedBusiness.businessName} ${AppOutletTempInstant.selectedOutlet.outletName}";
    }
  }

  void onBackgroundClick() {
    backAsync();
  }
  //#endregion
}
