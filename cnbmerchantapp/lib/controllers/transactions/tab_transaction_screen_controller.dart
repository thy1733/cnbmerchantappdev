// ignore_for_file: invalid_use_of_protected_member

import 'package:cnbmerchantapp/core.dart';
import 'package:cnbmerchantapp/services/exportServices/xcore.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class TabTransactionScreenController extends AppController {
  final ITransactionService _transactionService = TransactionService();
  final Duration animateDuration = const Duration(milliseconds: 200);
  RefreshController refreshController =
      RefreshController(initialRefresh: false);

  //#region All Override method

  //#endregion

  //#region All Properties

  var isRetryPressed = false;

  final _totalTransaction = 0.0.obs;
  double get totalTransaction => _totalTransaction.value;
  set totalTransaction(double value) => _totalTransaction.value = value;

  final _totalNetSale = 0.0.obs;
  double get totalNetSale => _totalNetSale.value;
  set totalNetSale(double value) => _totalNetSale.value = value;

  final _transactionFilter = TransactionListFilterInputModel.initial().obs;
  TransactionListFilterInputModel get transactionFilter =>
      _transactionFilter.value;
  set transactionFilter(TransactionListFilterInputModel value) =>
      _transactionFilter.value = value;

  final _transactionList = <TransactionItemListModel>[].obs;
  List<TransactionItemListModel> get transactionList => _transactionList.value;
  set transactionList(List<TransactionItemListModel> value) =>
      _transactionList.value = value;

  final _isHeaderExpand = false.obs;
  bool get isHeaderExpand => _isHeaderExpand.value;
  set isHeaderExpand(bool value) => _isHeaderExpand.value = value;

  final _quickDatePeriodOptions = <OptionItemModel>[].obs;
  List<OptionItemModel> get quickDatePeriodOptions => _quickDatePeriodOptions;
  set quickDatePeriodOptions(List<OptionItemModel> value) =>
      _quickDatePeriodOptions.value = value;

  final _selectedQuickDateForDisplay = ''.obs;
  String get selectedQuickDateForDisplay => _selectedQuickDateForDisplay.value;
  set selectedDateForDisplay(String value) =>
      _selectedQuickDateForDisplay.value = value;

  final _selectedQuickDate = DatePeriodModel().obs;
  DatePeriodModel get selectedQuickDate => _selectedQuickDate.value;
  set selectedQuickDate(DatePeriodModel value) =>
      _selectedQuickDate.value = value;

  String get downloadButtonTitle =>
      transactionFilter.hasFilter ? "DownloadReport" : "QuickDownloadReport";

  IconData get filterIconData => transactionFilter.hasFilter
      ? Icons.filter_alt
      : Icons.filter_alt_outlined;

  // use for handle biz id to check if changing selected biz
  String _selectedBizId = "";
  //#endregion

  //#region All Method Helpers

  Future<void> getTransactionListAsync(
      {bool checkListEmpty = true, bool isShowLoading = true}) async {
    // check to prevent reload data when user not change biz
    if (checkListEmpty &&
        transactionList.isNotEmpty &&
        AppBusinessTempInstant.selectedBusiness.id == _selectedBizId) {
      return;
    }
    // clear filter when use change biz
    if (AppBusinessTempInstant.selectedBusiness.id != _selectedBizId) {
      transactionFilter = TransactionListFilterInputModel.initial();
    }
    _selectedBizId = AppBusinessTempInstant.selectedBusiness.id;
    if (isShowLoading) {
      showLoading();
    }
    var resultModel = await _transactionService.getListAsync(transactionFilter);
    isNoData = resultModel.result?.items.isEmpty ?? true;
    if (resultModel.success) {
      totalTransaction = resultModel.result!.totalTransaction;
      transactionList = resultModel.result!.items.reversed.toList();
      totalNetSale = resultModel.result!.netSale;
      isOnline = true;
    } else {
      if (isRetryPressed) {
        CChoiceDialog.ok(
            title: "Failure".tr, message: resultModel.error!.details.tr);
      }
      if (resultModel.error!.details == "NoInternetConnectionDetails") {
        isOnline = false;
      }
    }
    if (isShowLoading) {
      dismiss();
    }
    refreshController.refreshCompleted();
  }

  //#endregion

  //#region All Command

  void clearAllFilters() {
    transactionFilter = TransactionListFilterInputModel.initial();
    getTransactionListAsync(checkListEmpty: false);
  }

  Future<void> toSearchTxnAsync() async {
    var arg = <String, dynamic>{ArgumentKey.item: transactionList};
    //await toScreenAsync(const SearchTransactionScreen(),args: arg);
    var result = await Get.to(() => const SearchTransactionScreen(),
        duration: const Duration(milliseconds: 250),
        fullscreenDialog: true,
        transition: Transition.fadeIn,
        arguments: arg);
    if (result == null) {}
  }

  Future<void> toFilterTxnAsync() async {
    var arguments = <String, dynamic>{ArgumentKey.id: transactionFilter};
    var result = await super.toNamedArgumentsAsync(Routes.filterTransaction,
        args: arguments) as TransactionListFilterInputModel?;

    if (result != null) {
      transactionFilter = result;
      selectedDateForDisplay = transactionFilter.dateForDisplay;
      debugPrint(
          "Selected date: ${transactionFilter.date.fromDate} && ${transactionFilter.date.toDate}");
      getTransactionListAsync(checkListEmpty: false);
    }
  }

  Future<void> toTxnDetailAsync(TransactionItemListModel item) async {
    if (!isLoading) {
      var arguments = <String, dynamic>{
        ArgumentKey.item: item,
        ArgumentKey.fromPage: ""
      };
      await super.navigationToNamedArgumentsAsync(Routes.transactionDetail,
          args: arguments);
    }
  }

  Future<void> onDownloadButtonTapAsync(BuildContext context) async {
    if (transactionFilter.hasFilter) {
      _downloadReport(transactionFilter.dateForDisplay, transactionFilter);
    } else {
      _showQuickDownloadPeriodModal(context);
    }
  }

  void _showQuickDownloadPeriodModal(BuildContext context) async {
    quickDatePeriodOptions = QuickDatePeriodOption.values
        .where((e) => e != QuickDatePeriodOption.custom)
        .map((e) => OptionItemModel.create(e.displayTitle.tr, []))
        .toList();
    var period = "";
    final result = await showCupertinoModalBottomSheet(
      topRadius: const Radius.circular(23),
      barrierColor: AppColors.barrierBackground,
      context: context,
      builder: (_) => BottomSheetSelectOption(
        title: "TransactionPeriod".tr,
        items: quickDatePeriodOptions,
        currentIndex: 0,
        isDatePicker: true,
        onSelectedItem: (i, date) {
          period = quickDatePeriodOptions[i].name;
          selectedQuickDate = date;
        },
      ),
    );
    if (result != null) {
      var filter = TransactionListFilterInputModel.initial();
      // filter.startDate = selectedQuickDate.fromDate.toFormatDateString("yyyyMMdd");
      // filter.endDate = selectedQuickDate.toDate.toFormatDateString("yyyyMMdd");
      filter.date = selectedQuickDate;
      filter.businessId = AppBusinessTempInstant.selectedBusiness.id;
      _downloadReport(period, filter);
    }
  }

  void _downloadReport(
    String period,
    TransactionListFilterInputModel filter,
  ) =>
      showCupertinoModalBottomSheet(
        topRadius: const Radius.circular(23),
        barrierColor: AppColors.barrierBackground,
        context: Get.context!,
        builder: (_) => TransactionReportBottomSheet(
          period: period,
          datePeriod: transactionFilter.date,
          totalNetSale: totalNetSale,
          transactionListModel: transactionList,
          onDownload: (fileFormat) async {
            await getDownloadReportAsync(filter, fileFormat);
          },
        ),
      );

  final DateFormat dateFormatter = DateFormat("dd-MM-yyyy");

  Future<void> getDownloadReportAsync(
      TransactionListFilterInputModel filter, FileFormat fileFormat) async {
    Get.back();
    showLoading();
    var result = await _transactionService.getDownloadReportAsync(filter);
    dismiss();
    if (result.success && result.result != null) {
      // check if empty data should not export
      if (result.result!.items.isNotEmpty) {
        String fileName =
            "CANAMERCHANT_REPORT_${DateFormat("yyyyMMddhhmmss").format(DateTime.now())}";
        var startDate = dateFormatter.format(filter.hasFilter
            ? filter.date.fromDate
            : selectedQuickDate.fromDate);
        var endDate = dateFormatter.format(
            filter.hasFilter ? filter.date.toDate : selectedQuickDate.toDate);
        var period = "From Date: $startDate to $endDate";

        if (fileFormat == FileFormat.excel) {
          AppExportExcelService().exportTransactionReport(
            fileName: fileName,
            period: period,
            data: result.result!,
          );
        } else if (fileFormat == FileFormat.pdf) {
          AppExportPdfService().exportTransactionReport(
            fileName: fileName,
            period: period,
            data: result.result!,
          );
        }
      } else {
        CChoiceDialog.ok(
          title: "Failure".tr,
          message: "ThereAreNoTrxToExport".tr,
        );
      }
    } else {
      CChoiceDialog.ok(
        title: result.error?.message.tr ?? "Failure".tr,
        message: result.error?.details.tr ??
            "SomethingWentWrongPleaseTryAgainLater".tr,
      );
    }
  }

  String getTransactionTypeValue(int transactionTypeId) {
    switch (transactionTypeId) {
      case 1:
        return "Receive";
      case 2:
        return "Refund";
      default:
        return "";
    }
  }

  String getPaymentMethodValue(int paymentMethodId) {
    switch (paymentMethodId) {
      case 1:
        return "KHQR";
      case 2:
        return "Canadia";
      default:
        return "";
    }
  }

  void getDateRange(int param) {
    DateTime now = DateTime.now();
    DateTime start, end = now;
    switch (param) {
      case 1: // Today
        start = DateTime(now.year, now.month, now.day - 1);
        break;
      case 2: // This week
        start = DateTime(now.year, now.month, now.day - now.weekday + 1);
        break;
      case 3: // This month
        start = DateTime(now.year, now.month, 1);
        break;
      case 4: // This year
        start = DateTime(now.year, 1, 1);
        break;
      case 5: // Last week
        start = now.subtract(const Duration(days: 7));
        break;
      case 6: // Last month
        start = now.subtract(const Duration(days: 30));
        break;
      case 7: // Last year
        start = now.subtract(const Duration(days: 365));
        break;
      case 8: // Custom
        start = transactionFilter.date.fromDate;
        end = transactionFilter.date.toDate;
        break;
      default: // Currently show last 365 days for show all case (define later)
        start = now.subtract(const Duration(days: 365));
        break;
    }
    transactionFilter.date.fromDate = start;
    transactionFilter.date.toDate = end;
  }

  retryAsync() async {
    isRetryPressed = true;
    await getTransactionListAsync();
  }

  //#endregion
}
