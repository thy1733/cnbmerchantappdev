import 'dart:io';

import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:path_provider/path_provider.dart' as path_provider;

class TransactionDetailScreenController extends AppController {
  final ITransactionService _transactionService = TransactionService();

  //#region All Override method

  @override
  void onInit() async {
    isLoading = true;
    getArgument();
    super.onInit();
  }

  void getArgument() {
    if (Get.arguments != null) {
      fromPage = Get.arguments[ArgumentKey.fromPage] as String;
      if (fromPage.isNotEmpty && fromPage == ArgumentValues.notifyDetail) {
        var txnId = Get.arguments[ArgumentKey.item] as String;
        inputModel.transactionId = txnId;
      } else {
        transactionItem =
            Get.arguments[ArgumentKey.item] as TransactionItemListModel;
        inputModel.transactionId = transactionItem.transactionId;
      }
    }
  }

  @override
  void onReady() async {
    await fetchTransactionDetailAsync();
    super.onReady();
  }

  @override
  void onClose() async {
    // Update status bar to black
    if (fromPage.toUpperCase() == ArgumentValue.search.toUpperCase()) {
      await darkThemMode();
    }
    super.onClose();
  }

  //#endregion

  //#region All Properties

  final _fromPage = ''.obs;
  String get fromPage => _fromPage.value;
  set fromPage(String value) => _fromPage.value = value;

  final _transactionItem = TransactionItemListModel().obs;
  TransactionItemListModel get transactionItem => _transactionItem.value;
  set transactionItem(TransactionItemListModel value) =>
      _transactionItem.value = value;

  final _inputModel = TransactionItemDetailsInputModel().obs;
  TransactionItemDetailsInputModel get inputModel => _inputModel.value;
  set inputModel(TransactionItemDetailsInputModel value) =>
      _inputModel.value = value;

  final _itemDetail = TransactionItemDetailsOutputModel().obs;
  TransactionItemDetailsOutputModel get itemDetail => _itemDetail.value;
  set itemDetail(TransactionItemDetailsOutputModel value) =>
      _itemDetail.value = value;

  final _outletDetail = OutletItemListOutputModel().obs;
  OutletItemListOutputModel get outletDetail => _outletDetail.value;
  set outletDetail(OutletItemListOutputModel value) =>
      _outletDetail.value = value;

  //#endregion

  //#region All Method Helpers

  Future<void> fetchTransactionDetailAsync() async {
    showLoading();
    var resultModel = await _transactionService.getDetailsAsync(inputModel);
    if (resultModel.success) {
      itemDetail = resultModel.result!;
    } else {}
    outletDetail = AppOutletTempInstant.outletList.items.firstWhereOrNull((e) =>
            e.outletName.toUpperCase() ==
            itemDetail.outletName.toUpperCase()) ??
        OutletItemListOutputModel();
    dismiss();
    isLoading = false;
  }

  //#endregion

  //#region All Command

  Future<void> toRefundTransactionAsync() async {
    await showMaterialModalBottomSheet(
        context: Get.context!,
        enableDrag: false,
        backgroundColor: AppColors.transparent,
        barrierColor: AppColors.barrierBackground,
        isDismissible: false,
        builder: (_) {
          return RefundTransactionDialogWidget(
            isDecimal: isUSDCurrency,
            initialAmount: 0,
            maximumAmount: itemDetail.availableRefundAmount,
            currency: itemDetail.currency.toUpperCase() ==
                    CurrencyEnum.usd.name.toUpperCase()
                ? CurrencyEnum.usd
                : CurrencyEnum.khr,
            onConfirm: (amount, remark) async {
              await Future.delayed(const Duration(milliseconds: 300));
              confirmRefundTransactionAsync(
                  transactionId: itemDetail.transactionId,
                  currency: itemDetail.currency,
                  amountToRefund: amount,
                  originalAmount: itemDetail.amount,
                  remark: remark);
            },
          );
        });

    // var arguments = <String, dynamic>{ArgumentKey.item: itemDetail};
    // await super.navigationToNamedArgumentsAsync(Routes.refundTransaction,
    //     args: arguments);
  }

  Future<void> toRefundTransactionDetailAsync(String txnId) async {
    var arguments = <String, dynamic>{
      ArgumentKey.fromPage: Routes.transactionDetail,
      ArgumentKey.id: txnId,
      ArgumentKey.item: transactionItem
    };
    await super.navigationToNamedArgumentsAsync(Routes.refundTransactionDetail,
        args: arguments);
  }

  Future<void> shareTransactionAsync({required Widget widget}) async {
    showLoading();
    final screenshotImage = await ScreenshotController().captureFromWidget(
      widget,
      delay: const Duration(seconds: 0),
    );

    final directory =
        (await path_provider.getApplicationDocumentsDirectory()).path;
    File imageFile = File('$directory/transaction.png');
    final file = await imageFile.writeAsBytes(screenshotImage);
    await KSystemService.shareFiles(
      [file.path],
    );
    dismiss();
  }

  //#endregion

  //#region Refund Transaction Bottom Sheet

  Future<void> confirmRefundTransactionAsync(
      {required String transactionId,
      required String currency,
      required double amountToRefund,
      required double originalAmount,
      required String remark}) async {
    var description =
        '${"Refund".tr} $amountToRefund ${itemDetail.currency.toUpperCase()}\n${'to'.tr} ${itemDetail.customerBankName} (${itemDetail.customerAccount})';

    await requestToConfirmPIN(
      title: "Refund".tr,
      image: SvgAppAssets.confirmRefund,
      description: description,
      isPermanentRequired: true,
      message: "EnterPinToConfirm".tr,
      onBackPress: () {},
      onSuccess: (callback) async {
        showLoading();
        TransactionCreateRefundInputModel input =
            TransactionCreateRefundInputModel();
        input.transactionId = transactionId;
        input.amountToRefund = amountToRefund;
        input.originalAmount = originalAmount;
        input.remark = remark;
        String profileId = AppUserTempInstant.appCacheUser.profileId;
        input.businessId = AppBusinessTempInstant.selectedBusiness.id;
        input.outletId = AppOutletTempInstant.selectedOutlet.id;
        debugPrint("Profile Id: $profileId");
        input.profileId = profileId.isEmpty ? "0" : profileId;
        var result = await _transactionService.refundTxnAsync(input);
        dismiss();
        if (result.error == null) {
          if (result.success && result.result != null) {
            callback(true, "");
            await KAlertScreen.success(
                primaryButtonTitle: "Done".tr,
                message: "Success".tr,
                description: description,
                onPrimaryButtonClick: () {
                  dismiss(); // dismiss success dialog
                  var arguments = <String, dynamic>{
                    ArgumentKey.fromPage: Routes.refundTransaction,
                    ArgumentKey.id: result.result!.transactionId,
                    ArgumentKey.item: transactionItem
                  };
                  navigationToNamedArgumentsAsync(
                      Routes.refundTransactionDetail,
                      args: arguments);
                });
          } else {
            callback(
                false,
                result.result?.description ??
                    "SomethingWentWrongPleaseTryAgainLater");
          }
        } else {
          callback(false, result.error!.details);
        }
      },
    );
  }
  //#endregion
}
