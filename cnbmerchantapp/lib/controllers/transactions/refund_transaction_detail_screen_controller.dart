import 'dart:io';

import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:path_provider/path_provider.dart' as path_provider;

class RefundTransactionDetailScreenController extends AppController {
  final ITransactionService _transactionService = TransactionService();

  //#region All Override method

  @override
  void onInit() {
    isLoading = true;
    getArgument();
    super.onInit();
  }

  @override
  void onReady() async {
    await fetchTransactionDetailAsync(inputModel);
    super.onReady();
  }

  //#endregion

  //#region All Properties
  final _tabTransactionController = Get.find<TabTransactionScreenController>();
  final _tabHomeController = Get.find<TabHomeScreenController>();

  final _transactionItem = TransactionItemListModel().obs;
  TransactionItemListModel get transactionItem => _transactionItem.value;
  set transactionItem(TransactionItemListModel value) =>
      _transactionItem.value = value;

  final _inputModel = TransactionItemDetailsInputModel().obs;
  TransactionItemDetailsInputModel get inputModel => _inputModel.value;
  set inputModel(TransactionItemDetailsInputModel value) =>
      _inputModel.value = value;

  final _itemDetail = TransactionItemDetailsOutputModel().obs;
  TransactionItemDetailsOutputModel get itemDetail => _itemDetail.value;
  set itemDetail(TransactionItemDetailsOutputModel value) =>
      _itemDetail.value = value;

  final _outletDetail = OutletItemListOutputModel().obs;
  OutletItemListOutputModel get outletDetail => _outletDetail.value;
  set outletDetail(OutletItemListOutputModel value) =>
      _outletDetail.value = value;

  String fromPage = "";

  //#endregion

  //#region All Method Helpers

  void getArgument() async {
    if (Get.arguments != null) {
      fromPage = Get.arguments[ArgumentKey.fromPage] as String;
      String txnId = Get.arguments[ArgumentKey.id] as String;
      transactionItem =
          Get.arguments[ArgumentKey.item] as TransactionItemListModel;
      inputModel.transactionId = txnId;
    }
  }

  Future<void> fetchTransactionDetailAsync(
      TransactionItemDetailsInputModel input) async {
    showLoading();
    var resultModel = await _transactionService.getDetailsAsync(input);
    if (resultModel.success) {
      itemDetail = resultModel.result!;
    } else {
      debugPrint("Failed to fetch transaction detail");
    }
    outletDetail = AppOutletTempInstant.outletList.items.firstWhereOrNull((e) =>
            e.outletName.toUpperCase() ==
            itemDetail.outletName.toUpperCase()) ??
        OutletItemListOutputModel();
    dismiss();
    isLoading = false;
  }

  //#endregion

  //#region All Command

  Future<void> refundTransactionAsync(String txnId) async {
    var arguments = <String, dynamic>{
      ArgumentKey.id: txnId,
      ArgumentKey.item: transactionItem
    };
    await super.navigationToNamedArgumentsAsync(Routes.refundTransactionDetail,
        args: arguments);
  }

  Future<void> shareTransactionAsync({required Widget widget}) async {
    showLoading();
    final screenshotImage = await ScreenshotController().captureFromWidget(
      widget,
      delay: const Duration(seconds: 0),
    );

    final directory =
        (await path_provider.getApplicationDocumentsDirectory()).path;
    File imageFile = File('$directory/transaction.png');
    final file = await imageFile.writeAsBytes(screenshotImage);
    await KSystemService.shareFiles(
      [file.path],
    );
    dismiss();
  }

  void _goBackToTabTransactionScreen() {
    _tabTransactionController.getTransactionListAsync(
        checkListEmpty: false, isShowLoading: false);
    _tabHomeController.onSelectedYearFilterAsync(
        _tabHomeController.availableYearsFilter.first,
        checkSelectedYear: false);
    Get.close(2);
  }

  Future<bool> Function()? onWillPop() {
    if (fromPage == Routes.refundTransaction) {
      return () async {
        Get.close(2);
        return true;
      };
    } else {
      return null;
    }
  }

  void onBackPressed() {
    switch (fromPage) {
      case Routes.transactionDetail:
        Get.back();
        break;
      case Routes.refundTransaction:
        _goBackToTabTransactionScreen();
        break;
      default:
        Get.back();
        break;
    }
  }

  //#endregion
}
