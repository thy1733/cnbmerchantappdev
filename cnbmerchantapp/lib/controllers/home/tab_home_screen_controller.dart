import 'package:cnbmerchantapp/core.dart';
import 'package:cnbmerchantapp/helpers/enums/outlet_status_enum.dart';
import 'package:cnbmerchantapp/views/qrScanner/qr_scanner_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class TabHomeScreenController extends AppController {
  final IBusinessService _manageBusinessService = BusinessService();
  final OutletService _outletService = OutletService();
  final IAppBusinessCacheService _appBusinessCacheService =
      AppBusinessCacheService();
  final IAppOutletCacheService _appOutletCacheService = AppOutletCacheService();
  final ILoginService _loginService = LoginService();
  final ISaleAnalyticService _analyticService = SaleAnalyticService();
  late MainScreenController _mainScreenController;
  final RefreshController homeRefreshController = RefreshController();
  //#region All Override method
  @override
  void onInit() {
    // Sample data
    isLoading = true;
    super.onInit();
  }

  @override
  void onReady() async {
    _mainScreenController = Get.find<MainScreenController>();
    _generateYearFilter();
    showLoading();
    isOnline = await checkInternetConnection();
    dismiss();
    if (!isOnline) {
      onNoInternet();
      isLoading = false;
      return;
    }

    // call user profile to check if caches user still exist
    var isUserExist = await fetchUserProfile();

    if (isUserExist) {
      if (AppUserTempInstant.isCashier) {
        await fetchBusinessListAsync();
      } else {
        await getBusinessListCaches();
      }
      await onSelectedYearFilterAsync(availableYearsFilter.first);
      isLoading = false;
    } else {
      // if user not exist call logout
      showLoading();
      await _mainScreenController.logoutAsync();
      dismiss();
    }
    super.onReady();
  }
  //#endregion

  //#region All Properties

  List<SaleAnalyMonthlyOutputModel> saleAnalyDatasOriginal = [];

  final _saleAnalyDatas = [
    SaleAnalyMonthlyOutputModel(),
  ].obs;
  List<SaleAnalyMonthlyOutputModel> get saleAnalyDatas => _saleAnalyDatas;
  set saleAnalyDatas(List<SaleAnalyMonthlyOutputModel> value) =>
      _saleAnalyDatas.value = value;

  final _currentSaleAnalyDatasChartIndex = 0.obs;
  int get currentSaleAnalyDatasChartIndex =>
      _currentSaleAnalyDatasChartIndex.value;
  set currentSaleAnalyDatasChartIndex(int value) =>
      _currentSaleAnalyDatasChartIndex.value = value;

  final _availableYearsFilter = <String>[].obs;
  List<String> get availableYearsFilter => _availableYearsFilter;
  set availableYearsFilter(List<String> value) =>
      _availableYearsFilter.value = value;

  final _selectedYearFilter = "2000".obs;
  String get selectedYearFilter => _selectedYearFilter.value;
  set selectedYearFilter(String value) => _selectedYearFilter.value = value;

  final _canShowChat = false.obs;
  bool get canShowChat => _canShowChat.value;
  set canShowChat(bool value) => _canShowChat.value = value;

  final _latestMonthSale = SaleAnalyMonthlyOutputModel().obs;
  SaleAnalyMonthlyOutputModel get latestMonthSale => _latestMonthSale.value;
  set latestMonthSale(SaleAnalyMonthlyOutputModel value) =>
      _latestMonthSale.value = value;

  final _todaySaleAmount = .0.obs;
  double get todaySaleAmount => _todaySaleAmount.value;
  set todaySaleAmount(double value) => _todaySaleAmount.value = value;

  final _todaySaleTransaction = 0.obs;
  int get todaySaleTransaction => _todaySaleTransaction.value;
  set todaySaleTransaction(int value) => _todaySaleTransaction.value = value;

  final _selectedMonthSale = SaleAnalyMonthlyOutputModel().obs;
  SaleAnalyMonthlyOutputModel get selectedMonthSale => _selectedMonthSale.value;
  set selectedMonthSale(SaleAnalyMonthlyOutputModel value) =>
      _selectedMonthSale.value = value;

  final _merchantName = ''.obs;
  String get merchantName => _merchantName.value;
  set merchantName(String value) => _merchantName.value = value;

  final _isNeedReloadBussiness = false.obs;
  bool get isNeedReloadBussiness => _isNeedReloadBussiness.value;
  set isNeedReloadBussiness(bool value) => _isNeedReloadBussiness.value = value;

  final _isShowQRDisplay = false.obs;
  bool get isShowQRDisplay => _isShowQRDisplay.value;
  set isShowQRDisplay(bool value) => _isShowQRDisplay.value = value;

  //#endregion

  //#region All Method Helpers

  void clearAnalyData() {
    selectedMonthSale = SaleAnalyMonthlyOutputModel();
    todaySaleAmount = 0;
    todaySaleTransaction = 0;
  }

  /// WHEN_SELECT_FILTER_BY_YEAR
  Future<void> onSelectedYearFilterAsync(
    String value, {
    bool checkSelectedYear = true,
  }) async {
    if (selectedYearFilter == value && checkSelectedYear) {
      return;
    }
    selectedYearFilter = value;
    canShowChat = false;
    await _generateEmptyData();
    currentSaleAnalyDatasChartIndex =
        saleAnalyDatas.isNotEmpty ? saleAnalyDatas.length - 1 : 0;
    await Future.delayed(const Duration(milliseconds: 50));
    canShowChat = true;
  }

  Future<void> fetchSaleAnalyticDataAsync(int index, DateTime date) async {
    // generate Sample data Sale Analy
    var input = SaleAnalyticInputModel(
      businessId: AppBusinessTempInstant.selectedBusiness.id,
      selectedDate: date,
    );
    var result = await _analyticService.getSaleAnalytic(input);
    if (result.result != null && result.success) {
      // saleAnalyDatasOriginal.add(result.result!);
      saleAnalyDatas[index] = result.result!;
      selectedMonthSale = saleAnalyDatas[index];
      if (date.year == DateTime.now().year &&
          date.month == DateTime.now().month) {
        latestMonthSale = result.result!;
        if (latestMonthSale.items.isNotEmpty) {
          var todaySale = latestMonthSale.items
              .firstWhereOrNull((element) => element.date.isToday());
          todaySaleAmount = todaySale?.amount ?? 0;
          todaySaleTransaction = todaySale?.numberOfTransaction ?? 0;
        }
      }
    } else {
      saleAnalyDatas[index] = SaleAnalyMonthlyOutputModel();
      selectedMonthSale = saleAnalyDatas[index];
    }
  }

  /// Generate month in [selectedYearFilter] in whole year
  Future<void> _generateEmptyData() async {
    saleAnalyDatas.clear();

    // Find latest month on year
    var year = int.parse(selectedYearFilter);
    var latestMonth = 12;
    if (year == DateTime.now().year) {
      latestMonth = DateTime.now().month;
    }

    // Create empty List<SaleAnalyMonthlyOutputModel> from first to last month of year
    for (int i = latestMonth - 1; i >= 0; i--) {
      var monthly = SaleAnalyMonthlyOutputModel();
      monthly.year = year;
      monthly.selectDate = DateTime(year, latestMonth - i);
      saleAnalyDatas.add(monthly);
    }

    // fetch data
    await fetchSaleAnalyticDataAsync(
      saleAnalyDatas.length - 1,
      saleAnalyDatas.last.selectDate,
    );
  }

  void _generateYearFilter() {
    var currentYear = DateTime.now().year;
    while (currentYear >= 2022) {
      availableYearsFilter.add("$currentYear");
      currentYear--;
    }
  }

  /// this func call user profile to check if caches user still exist
  Future<bool> fetchUserProfile() async {
    final IAppUserProfileInfoService appCacheService =
        AppUserProfileInfoService();
    await appCacheService.getUserProfileAsync();

    /// WHEN LOGIN AS CASHIER
    /// we can't detect user type so with both accountNumber and inviteCode
    var accNumInviteCode = AppUserTempInstant.appCacheUser.accountNumber;
    if (accNumInviteCode.isEmpty) {
      accNumInviteCode = AppUserTempInstant.appCacheUser.inviteCode;
    }
    var userName = AppUserTempInstant.appCacheUser.userName;

    if (accNumInviteCode.isEmpty || userName.isEmpty) {
      return false;
    }

    var result = await _loginService.getUserProfile(accNumInviteCode, userName);
    if (result.error?.code == 5) {
      // clean remember user data
      await appCacheService.removeUserProfileAsync();
      await CChoiceDialog.ok(
        title: result.error?.details?? "Failure".tr,
        message: "UserProfileErrorMsg".tr,
        buttonTitle: "Logout".tr,
        barrierDismissible: false,
        ok: () => backAsync(),
      );
      return false;
    }
    return true;
  }

  Future<void> getBusinessListCaches() async {
    if (AppUserTempInstant.appCacheUser.mCID.isNotEmpty) {
      var cachesBus = await _appBusinessCacheService.getBusiness();
      if (cachesBus.items.isEmpty) {
        await fetchBusinessListAsync(isShowLoading: true);
      } else {
        await _appBusinessCacheService.getSelectedBusiness();
        await getOutletListCaches();
      }
      checkToShowQr();
    }
  }

  Future<bool> fetchBusinessListAsync({bool isShowLoading = false}) async {
    if (isShowLoading) {
      showLoading();
    }
    var input = ManageBusinessListInputModel();
    input.mCID = AppUserTempInstant.appCacheUser.mCID;
    var result = await _manageBusinessService.getListBusiness(input);
    if (isShowLoading) {
      dismiss();
    }
    if (result.success && result.result != null) {
      var business = result.result!;
      isNeedReloadBussiness = false;
      if (AppUserTempInstant.appCacheUser.userType == 1 &&
          AppUserTempInstant.appCacheUser.businessId.isNotEmpty) {
        business.items = business.items
            .where((element) =>
                element.id == AppUserTempInstant.appCacheUser.businessId)
            .toList();
      }

      var activeBusinessList = ManageBusinessListOutputModel();
      activeBusinessList.items = business.items
          .where((e) =>
              e.status.toUpperCase() ==
              BusinessStatus.Active.name.toUpperCase())
          .toList();
      await _appBusinessCacheService.saveBusiness(activeBusinessList);

      // to check if selected business is deactive so we must select other business if have more active data
      var findSelectedBiz = activeBusinessList.items.firstWhereOrNull(
          (e) => e.id == AppBusinessTempInstant.selectedBusiness.id);
      if (findSelectedBiz == null) {
        if (activeBusinessList.items.isNotEmpty) {
          await _appBusinessCacheService
              .saveSelectedBusiness(activeBusinessList.items.first);
        } else {
          await _appBusinessCacheService
              .saveSelectedBusiness(ManageBusinessItemListOutputModel());
        }
      }

      if (activeBusinessList.items.isEmpty) {
        if (!AppUserTempInstant.isCashier) {
          CChoiceDialog.ok(
            title: "Failure".tr,
            message: "NoActiveBusiness".tr,
          );
          isLoading = false;
        }
        return false;
      }

      if (AppBusinessTempInstant.selectedBusiness.id.isNotEmpty) {
        await fetchOutletListAsync();
      }
      checkToShowQr();
      return true;
    } else {
      await CChoiceDialog.ok(
          title: "Fails".tr,
          message: "Can not get business list! Please try again.",
          ok: () {
            dismiss();
          });

      return false;
    }
  }

  Future<void> getOutletListCaches() async {
    if (AppBusinessTempInstant.selectedBusiness.id.isNotEmpty) {
      var cachesOutlet = await _appOutletCacheService.getOutlets(
        AppBusinessTempInstant.selectedBusiness.id,
      );
      if (cachesOutlet.items.isEmpty) {
        await fetchOutletListAsync();
      } else {
        await _appOutletCacheService.getSelectedOutlet();
      }
    }
    setQrMerchantName();
  }

  Future<OutletListOutputModel> fetchOutletListAsync() async {
    var input = OutletListInputModel();

    input.businessId = AppBusinessTempInstant.selectedBusiness.id;
    input.filter = "";
    input.maxResultCount = 10;
    input.skipCount = 1;

    // clear old caches
    await _appOutletCacheService.clear();

    var response = await _outletService.getListOutlet(input);

    if (response.result != null && response.success) {
      var outlets = response.result!;
      if (AppUserTempInstant.isCashier) {
        var outletByCashier = outlets.items
            .where(
              (outlet) =>
                  outlet.cashiers.firstWhereOrNull((c) =>
                      c.id == AppUserTempInstant.appCacheUser.profileId) !=
                  null,
            )
            .toList();
        outlets.items.clear();
        outlets.items.addAll(outletByCashier);
      }
      if (outlets.items.isNotEmpty) {
        try {
          // if (isBizOwner) {
          //   outlets = response.result!;
          // } else {
          //   outlets.items = response.result!.items
          //       .where((e) =>
          //           e.cashiers.where((c) => c.id == casId).toList().isNotEmpty)
          //       .toList();
          // }

          outlets.items = outlets.items
              .where((e) =>
                  e.status.toUpperCase() ==
                      OutletStatus.Active.name.toUpperCase() ||
                  e.status.toUpperCase() == "TRUE")
              .toList();
          await _appOutletCacheService.saveOutlets(outlets);
          await _appOutletCacheService.saveSelectedOutlet(outlets.items.first);
          return response.result!;
        } catch (e) {
          e.printError();
        }
      }
    }

    return OutletListOutputModel();
  }

  void checkToShowQr() {
    isShowQRDisplay = AppOutletTempInstant.outletList.items.isNotEmpty &&
        AppBusinessTempInstant.selectedBusiness.id.isNotEmpty;
  }

  //#endregion

  //#region Validation
  //#endregion

  //#region All Command
  void selectYearFilter() async {
    if (!isOnline) {
      onNoInternet();
      return;
    }
    await AppBottomSheet.bottomSheetSelectDynamicOption<String>(
      title: "SelectPeriod".tr,
      selectedItem: selectedYearFilter,
      itemBuilder: (index, value) => Padding(
        padding: const EdgeInsets.symmetric(vertical: 10),
        child: Text(
          value,
          style: AppTextStyle.body1.copyWith(color: AppColors.textPrimary),
        ),
      ),
      onSelectedItem: (int index, value) async {
        backAsync();
        showLoading();
        await onSelectedYearFilterAsync(value);
        dismiss();
      },
      items: availableYearsFilter,
    );
  }

  void onSelectBusinessAsync() async {
    /// CHECK BUSINESS RESULT
    if (AppBusinessTempInstant.appCacheBusiness.items.isEmpty) {
      if (!isOnline) {
        onNoInternet();
        return;
      }
      var busiessResult = await fetchBusinessListAsync(isShowLoading: true);

      if (busiessResult == false) {
        return;
      }
    }

    if (AppUserTempInstant.isCashier) {
      return;
    }
    var selectedBusinessIndex = -1;
    for (int i = 0;
        i < AppBusinessTempInstant.appCacheBusiness.items.length;
        i++) {
      var element = AppBusinessTempInstant.appCacheBusiness.items.elementAt(i);
      if (element.id == AppBusinessTempInstant.selectedBusiness.id) {
        selectedBusinessIndex = i;
        break;
      }
    }

    await AppBottomSheet.bottomSheetSelectDynamicOption<
        ManageBusinessItemListOutputModel>(
      title: "SwitchBusiness".tr,
      items: AppBusinessTempInstant.appCacheBusiness.items,
      onSelectedItem: (index, value) async {
        if (value.id != AppBusinessTempInstant.selectedBusiness.id) {
          showLoading();
          clearAnalyData();
          await _appBusinessCacheService.saveSelectedBusiness(value);
          // update sale anaytic
          await onSelectedYearFilterAsync(availableYearsFilter.first,
              checkSelectedYear: false);
          // fetch outlet
          await fetchOutletListAsync();

          checkToShowQr();
          dismiss();
        }
      },
      selectedItemIndex: selectedBusinessIndex,
      closeOnSelect: true,
      separator: ContainerDevider.vertical(),
      itemBuilder: (index, value) => Row(
        children: [
          AppNetworkImage(
            value.businessLogo,
            height: 30,
            width: 30,
            shape: BoxShape.circle,
            errorWidget: SvgPicture.asset(SvgAppAssets.pBusinessList),
          ),
          const SizedBox(width: 10),
          Text(
            value.businessName,
            style: AppTextStyle.body1.copyWith(color: AppColors.textPrimary),
          )
        ],
      ),
    );
  }

  void onSelectOutletAsync() async {
    await AppBottomSheet.bottomSheetSelectDynamicOption<
        OutletItemListOutputModel>(
      title: "SwitchOutlet".tr,
      items: AppOutletTempInstant.outletList.items,
      onSelectedItem: (index, value) {
        AppOutletTempInstant.selectedOutlet = value;
        _appOutletCacheService.saveSelectedOutlet(value);
        setQrMerchantName();
      },
      selectedItem: AppOutletTempInstant.selectedOutlet,
      closeOnSelect: true,
      separator: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Container(
          color: AppColors.greyBackground,
          height: 1,
        ),
      ),
      itemBuilder: (index, value) => Row(
        children: [
          AppNetworkImage(
            value.outletLogo,
            height: 35,
            width: 35,
            shape: BoxShape.circle,
            errorWidget: SvgPicture.asset(
              SvgAppAssets.pOutletList,
            ),
          ),
          const SizedBox(width: 10),
          Text(
            value.outletName,
            style: AppTextStyle.body1.copyWith(color: AppColors.textPrimary),
          )
        ],
      ),
    );
  }

  /// to show name on KHQR template
  ///
  /// it's should be Merchant Name = Business name + outlet name
  /// for defualt outlet name is used Business name by defualt when create so we use only Business name when select defualt outlet
  void setQrMerchantName() {
    /// find defualt outletId
    /// defualt outletId is end with .1
    var outletId = AppOutletTempInstant.selectedOutlet.id.split('.').last;
    if (outletId == '1') {
      merchantName = AppOutletTempInstant.selectedOutlet.outletName;
    } else {
      merchantName =
          "${AppBusinessTempInstant.selectedBusiness.businessName} ${AppOutletTempInstant.selectedOutlet.outletName}";
    }
  }

  Future<void> showQrDisplay({QrPayInputModel? input}) async {
    /// CHECK BUSINESS RESULT
    if (AppBusinessTempInstant.appCacheBusiness.items.isEmpty) {
      if (!isOnline) {
        onNoInternet();
        return;
      }
      var busiessResult = await fetchBusinessListAsync(isShowLoading: true);
      if (busiessResult == false) {
        return;
      }
    }

    if (AppOutletTempInstant.outletList.items.isEmpty) {
      if (!isOnline) {
        onNoInternet();
        return;
      }
      showLoading();
      var outlet = await fetchOutletListAsync();
      dismiss();
      if (outlet.items.isEmpty) {
        return;
      }
    }

    Map<String, dynamic> argument = {ArgumentKey.input: input};
    await navigationToPageArgumentsAsync(const QrDisplayScreen(),
        args: argument);
  }

  void showQrScanner() async {
    await navigationToPageArgumentsAsync(const QrScannerScreen());
  }

  void onNoInternet() {
    CChoiceDialog.ok(
        title: "NoInternetConnection".tr,
        message: "NoInternetConnectionDetails".tr);
    isLoading = false;
  }
  //#endregion
}
