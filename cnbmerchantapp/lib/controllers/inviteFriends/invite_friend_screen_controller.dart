// ignore_for_file: invalid_use_of_protected_member
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:get/get.dart';
import 'package:cnbmerchantapp/core.dart';

class InviteFriendScreenController extends AppController {
  //#region All Override method
  @override
  void onInit() {
    friends = _sampleFriends();
    super.onInit();
  }

  //#endregion

  //#region All Properties
  final _title = "".obs;
  String get title => _title.value;
  set title(String value) => _title.value = value;

  final _friends = <FriendModel>[].obs;
  List<FriendModel> get friends => _friends.value;
  set friends(List<FriendModel> value) => _friends.value = value;

  //#endregion

  //#region All Method Helpers

  List<FriendModel> _sampleFriends() {
    return <FriendModel>[
      FriendModel.create(
          id: 0011,
          firstName: "Robert",
          lastName: "Baratheon",
          status: FriendStatus.inactive,
          photoUrl: "https://www.w3schools.com/howto/img_avatar.png",
          account: "000 100 129 6734"),
      FriendModel.create(
          id: 0012,
          firstName: "Tony",
          lastName: "Stark",
          status: FriendStatus.active,
          photoUrl: "https://www.w3schools.com/howto/img_avatar.png",
          account: "000 100 129 6723"),
    ];
  }

  // void _shareLink() async {
  //   final KSystemService systemService = KSystemService();
  //   String value = "";
  //
  //   BranchUniversalObject buo = BranchUniversalObject(
  //     canonicalIdentifier: 'flutter/branch',
  //     title: 'CNBMerchant',
  //     contentDescription: '',
  //     imageUrl:
  //     'https://cdn.branch.io/branch-assets/1665735933090-og_image.png',
  //     publiclyIndex: true,
  //     locallyIndex: true,
  //   );
  //
  //   BranchLinkProperties lp = BranchLinkProperties();
  //   lp.addControlParam('', '');
  //
  //   BranchResponse response =
  //   await FlutterBranchSdk.getShortUrl(buo: buo, linkProperties: lp);
  //   if (response.success) {
  //     value = response.result.toString();
  //   }
  //   String invitedFriendLink =
  //   "InviteFriendLink".tr.replaceAll("{0}", value);
  //   await systemService.shareLinkedAsync(invitedFriendLink);
  // }

  void _shareLink() async {
    final KSystemService systemService = KSystemService();
    String value = await createDynamicLink();
      String invitedFriendLink =
      "InviteFriendLink".tr.replaceAll("{0}", value);
      await systemService.shareLinkedAsync(invitedFriendLink);
  }

  Future<String> createDynamicLink() async {
    var parameters = DynamicLinkParameters(
      uriPrefix: 'https://canamerchantapp.page.link',
      link: Uri.parse('https://canamerchantapp.page.link/CNB'),
      androidParameters: const AndroidParameters(
        packageName: "com.canadiabank.canamerchantapp.android",
      ),
      iosParameters: const IOSParameters(
        bundleId: 'com.canadiabank.canamerchantapp.iphone',
        minimumVersion: '0',
      ),
    );
    FirebaseDynamicLinks dynamicLinks = FirebaseDynamicLinks.instance;
    final ShortDynamicLink shortLink = await dynamicLinks.buildShortLink(parameters);
    Uri url = shortLink.shortUrl;
    return url.toString();
  }

  //#endregion

  //#region All Command

  Future<void> inviteFriendAsync() async {
    if (isLoading) return;
    isLoading = true;
    _shareLink();
    await Future.delayed(const Duration(seconds: 1));
    isLoading = false;
  }

  //#endregion
}
