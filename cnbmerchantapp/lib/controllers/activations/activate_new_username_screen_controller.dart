import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';

class ActivateCreateUsernameScreenController extends AppController {
  //#region All Override method
  @override
  void onInit() {
    getArgument();
    super.onInit();
  }

  @override
  void onReady() async {
    if (fromPage == Routes.activateOwnerScreen || fromPage == Routes.activateExistingMerchantScreen) {
      await getAccountInfoListAsync();
    }
    super.onReady();
  }
  //#endregion

  //#region All Properties

  final AuthenticateService _authenticateService = AuthenticateService();

  String fromPage = "";

  final _title = 'Title'.obs;
  String get title => _title.value;
  set title(String value) => _title.value = value;

  final _captionMessage = 'Caption'.obs;
  String get captionMessage => _captionMessage.value;
  set captionMessage(String value) => _captionMessage.value = value;

  final _registerAccountInput = RegisterAccountInputModel().obs;
  RegisterAccountInputModel get registerAccountInput =>
      _registerAccountInput.value;
  set registerAccountInput(RegisterAccountInputModel value) =>
      _registerAccountInput.value = value;

  final _bankAccountList = GetAccountInfoListOutputModel().obs;
  GetAccountInfoListOutputModel get bankAccountList => _bankAccountList.value;
  set bankAccountList(GetAccountInfoListOutputModel value) =>
      _bankAccountList.value = value;

  GetAccountInfoItemOutputModel? selectedAccountInfoItem;

  final _selectedBankAccountIndex = 0.obs;
  int get selectedBankAccountIndex => _selectedBankAccountIndex.value;
  set selectedBankAccountIndex(int value) =>
      _selectedBankAccountIndex.value = value;

  final _usernameValidateMessage = "".obs;
  String get usernameValidateMessage => _usernameValidateMessage.value;
  set usernameValidateMessage(String value) =>
      _usernameValidateMessage.value = value;

  final _isNeedSelectBankAccount = false.obs;
  bool get isNeedSelectBankAccount => _isNeedSelectBankAccount.value;
  set isNeedSelectBankAccount(bool value) =>
      _isNeedSelectBankAccount.value = value;

  final _enableSelectBankAccount = true.obs;
  bool get enableSelectBankAccount => _enableSelectBankAccount.value;
  set enableSelectBankAccount(bool value) =>
      _enableSelectBankAccount.value = value;

  final _getAccountInfoInput = GetAccountInfoInputModel().obs;
  GetAccountInfoInputModel get getAccountInfoInput =>
      _getAccountInfoInput.value;
  set getAccountInfoInput(GetAccountInfoInputModel value) =>
      _getAccountInfoInput.value = value;

  //#endregion

  //#region All Method Helpers

  void getArgument() {
    if (Get.arguments == null) return;
    if (Get.arguments[ArgumentKey.fromPage] != null) {
      fromPage = Get.arguments[ArgumentKey.fromPage];
      checkPageOnStart();
    }
    if (Get.arguments[ArgumentKey.item] != null) {
      registerAccountInput =
          Get.arguments[ArgumentKey.item] as RegisterAccountInputModel;
    }
  }

  void checkPageOnStart() {
    switch (fromPage) {
      case Routes.activateOwnerScreen:
        title = "ActivateAsBusinessOwner".tr;
        captionMessage = "CreateNewUsernameForBusinessOwnerLabel".tr;
        isNeedSelectBankAccount = true;
        break;
      case Routes.activateExistingMerchantScreen:
        title = "ActivateAsBusinessOwner".tr;
        captionMessage = "CreateNewUsernameForBusinessOwnerLabel".tr;
        isNeedSelectBankAccount = true;
        enableSelectBankAccount = false;
        break;
      case Routes.activateCashierScreen:
        title = "ActivateAsCashier".tr;
        captionMessage = "CreateNewUsernameForCashierLabel".tr;
        break;
      default:
    }
  }

  Future<void> getAccountInfoListAsync() async {
    getAccountInfoInput.cidOrAccountNumber = registerAccountInput.cid;
    getAccountInfoInput.merchantId = registerAccountInput.merchantId;
    showLoading();
    var result =
        await _authenticateService.getAccountInfoListAsync(getAccountInfoInput);
    dismiss();

    if (result.error == null) {
      if (result.success || bankAccountList.items.isNotEmpty) {
        bankAccountList = result.result ?? GetAccountInfoListOutputModel();
        if (!enableSelectBankAccount) {
          selectedAccountInfoItem = bankAccountList.items.firstWhereOrNull(
                (element) =>
                    element.accountNumber ==
                    getAccountInfoInput.cidOrAccountNumber,
              ) ??
              bankAccountList.items.elementAt(0);
        } else {
          selectedAccountInfoItem = bankAccountList.items.elementAt(0);
        }
        if (selectedAccountInfoItem != null) {
          registerAccountInput.accountNumber = selectedAccountInfoItem!.accountNumber;
        }
      } else {
        getBankAccountListFails();
      }
    } else {
      CChoiceDialog.okCancel(
          title: "Failure".tr,
          message: result.error!.details.tr,
          okTitle: "TryAgain".tr,
          onOk: () {
            backAsync();
            getAccountInfoListAsync();
          });
    }
  }

  Future<bool> checkExistingUsernameAsync() async {
    var input = CheckExistingUsernameInputModel()
      ..username = registerAccountInput.userName;
    var result = await _authenticateService.checkExistingUsernameAsync(input);
    dismiss();
    if (result.error == null) {
      var isUsernameAlreadyExisted = result.result?.isAlreadyExisted ?? false;
      if (isUsernameAlreadyExisted) {
        usernameValidateMessage = "The username is already taken.";
        CChoiceDialog.ok(
            title: "Failure".tr, message: "The username is already taken.");
      }
      return isUsernameAlreadyExisted;
    } else {
      throw result.error!.details;
    }
  }

  void getBankAccountListFails() {
    CChoiceDialog.okCancel(
        title: "CantGetAccount".tr,
        message: "CantGetAccountMessage".tr,
        okTitle: "TryAgain".tr,
        onOk: () {
          backAsync();
          getAccountInfoListAsync();
        });
  }

  // ON_NEXT_PRESS SECTION
  void checkPageOnSuccess() {
    switch (fromPage) {
      case Routes.activateOwnerScreen:
        activateOwnerAsync();
        break;
      case Routes.activateExistingMerchantScreen:
        activateOwnerAsync();
        break;
      case Routes.activateCashierScreen:
        activateCashierAsync();
        break;
      default:
    }
  }

  Future<void> activateOwnerAsync() async {
    // VALIDATE_SELECTED_ACCOUNT
    if (selectedAccountInfoItem == null) {
      CChoiceDialog.ok(
          title: "NoSelectedAccount".tr,
          message: "YouMustSelectAnAccountToContinues".tr,
          buttonTitle: "TryAgain".tr,
          ok: () {
            Get.back();
          });
      return;
    }

    // SAMPLE: IF_USER_SELECT_EXISTING_ACCOUNT
    showLoading();
    try {
      if (!(await checkExistingUsernameAsync())) {
        var input = CheckExistingAccountInputModel()
          ..accountNumber = selectedAccountInfoItem!.accountNumber
          ..username = registerAccountInput.userName;
        var result =
            await _authenticateService.checkExistingAccountAsync(input);
        dismiss();
        if (result.error == null) {
          if (!result.success) {
            // CHECK IF USER ACCOUNT IS ALREADY EXISTED
            CChoiceDialog.ok(
                title: "Failure".tr,
                message: result.result?.description ??
                    "SomethingWentWrongPleaseTryAgainLater".tr);
            // await CChoiceDialog.alertBottomSheetVerticalActions(
            //   title: "RegisterExistingAccountTitle".tr,
            //   acceptTitle: "Continue".tr,
            //   message: "",
            //   child: _alertExistingAccountChildWidget(selectedAccountInfoItem!),
            //   accept: () {
            //     Get.back(); // CLOSE_DIALOG
            //     arguments[ArgumentKey.fromPage] =
            //         Routes.activateOwnerExistingAccount;
            //     toNamedArgumentsAsync(Routes.newPasswordScreen, args: arguments);
            //   },
            //   hasCancel: false,
            // );
          } else {
            // SET_ARGUMENT
            var arguments = <String, dynamic>{
              ArgumentKey.fromPage: Routes.activateOwnerScreen,
              ArgumentKey.item: registerAccountInput
            };

            AppUserTempInstant.appCacheUser.userName =
                registerAccountInput.userName;

            // IF USER ACCOUNT HASN'T BEEN REGISTED YET, ASK USER TO SETUP BUSINESS
            arguments[ArgumentKey.fromPage] = Routes.activateOwnerScreen;
            toNamedArgumentsAsync(Routes.newPasswordScreen, args: arguments);
          }
        } else {
          CChoiceDialog.ok(
              title: "Failure".tr, message: result.error!.details.tr);
        }
      }
    } on String catch (m) {
      CChoiceDialog.ok(title: "Failure".tr, message: m.tr);
    }
  }

  Future<void> activateCashierAsync() async {
    showLoading();
    try {
      if (!(await checkExistingUsernameAsync())) {
        dismiss();
        var arguments = <String, dynamic>{
          ArgumentKey.fromPage: Routes.activateCashierScreen,
          ArgumentKey.item: registerAccountInput
        };
        toNamedArgumentsAsync(Routes.newPasswordScreen, args: arguments);
      }
    } on String catch (m) {
      CChoiceDialog.ok(title: "Failure".tr, message: m.tr);
    }
  }
  //#endregion

  //#region All Command
  void onNextBtnPress() async {
    checkPageOnSuccess();
  }

  void onSelectBankAcountAsync() async {
    if (bankAccountList.items.isEmpty) {
      getBankAccountListFails();
      return;
    }
    AppBottomSheet.bottomSheetSelectDynamicOption<
        GetAccountInfoItemOutputModel>(
      title: "SelectAccount".tr,
      items: bankAccountList.items,
      separator: const SizedBox(height: 25),
      closeOnSelect: true,
      selectedItem: selectedAccountInfoItem,
      onSelectedItem: (index, value) {
        // set selectedBankAccountIndex for UI
        selectedBankAccountIndex = index;
        selectedAccountInfoItem = value;
        // set accountNumber for registerAccountInput
        registerAccountInput.accountNumber =
            bankAccountList.items.elementAt(index).accountNumber;
      },
      itemBuilder: (index, value) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              value.accountName.tr,
              style: const TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.w400,
              ),
            ),
            Text(
              '${value.currency.toUpperCase()} | ${value.accountNumber.maskAccountNumber()}',
              style: const TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.w700,
              ),
            ),
          ],
        );
      },
    );
  }
  //#endregion

  //#region Validation
  void usernameValidator() {
    if (registerAccountInput.userName.length >= 5 &&
        !registerAccountInput.userName.contains(" ")) {
      usernameValidateMessage = "";
    } else if (registerAccountInput.userName.isEmpty) {
      usernameValidateMessage = "UsernameRequiredMessage";
    } else {
      usernameValidateMessage = "UsernameNotvalid";
    }
    checkEnable();
  }

  void checkEnable() {
    isEnable = usernameValidateMessage.isEmpty &&
        registerAccountInput.userName.length >= 5;
  }
  //#endregion

  /// widget for use in alert modal when selected existing account
  // Widget _alertExistingAccountChildWidget(GetAccountInfoItemOutputModel data) {
  //   return Text.rich(
  //     TextSpan(
  //       style: const TextStyle(color: AppColors.textPrimary),
  //       children: [
  //         TextSpan(
  //           text:
  //               "${data.accountName} ${data.accountNumber.maskAccountNumber()} ${"RegisterExistingAccountMsg".tr}",
  //         ),
  //         TextSpan(
  //             text: " ${registerAccountInput.userName} ",
  //             style: const TextStyle(fontWeight: FontWeight.bold)),
  //       ],
  //     ),
  //     textAlign: TextAlign.center,
  //   );
  // }
}
