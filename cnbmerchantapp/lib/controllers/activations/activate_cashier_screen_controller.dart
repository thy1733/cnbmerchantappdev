import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class ActivateCashierScreenController extends AppController {
  //#region All Override method
  //#endregion

  //#region All Properties

  final AuthenticateService _authenticateService = AuthenticateService();
  final AppVerifyOTP _appVerifyOTP = AppVerifyOTP();

  final _activateInput = ActivateInputModel().obs;
  ActivateInputModel get activateInput => _activateInput.value;
  set activateInput(ActivateInputModel value) => _activateInput.value = value;

  RegisterAccountInputModel registerAccountInput = RegisterAccountInputModel();

  final _cidValidateMessage = ''.obs;
  String get cidValidateMessage => _cidValidateMessage.value;
  set cidValidateMessage(String value) => _cidValidateMessage.value = value;

  final _phoneValidateMessage = ''.obs;
  String get phoneValidateMessage => _phoneValidateMessage.value;
  set phoneValidateMessage(String value) => _phoneValidateMessage.value = value;

  final _inviteCodeInput = ''.obs;
  String get inviteCodeInput => _inviteCodeInput.value;
  set inviteCodeInput(String value) => _inviteCodeInput.value = value;

  final _phoneInput = ''.obs;
  String get phoneInput => _phoneInput.value;
  set phoneInput(String value) => _phoneInput.value = value;

  //#endregion

  //#region All Method Helpers
  void onVerifyOTPSuccess(String otpCode, String cidOrAccountNumber){
    Get.back(); // CLOSE OPT SCREEN
    registerAccountInput.optCode = otpCode;
    registerAccountInput.phoneNumber = phoneInput;
    registerAccountInput.inviteCode = inviteCodeInput;
    var arguments = <String, dynamic>{
      ArgumentKey.fromPage: Routes.activateCashierScreen,
      ArgumentKey.item: registerAccountInput
    };
    navigationToNamedArgumentsAsync(Routes.activateCreateUsernameScreen,
        args: arguments);
  }

  Future<void> onActivateAsyn() async {
    // input model
    activateInput.phoneNumber = phoneInput.removeAllWhitespace;
    activateInput.cidOrAccountNumber = inviteCodeInput.removeAllWhitespace;
    activateInput.activateType = ActivateType.cashier.index;

    showLoading();
    var result = await _authenticateService.activateAsync(activateInput);
    dismiss();

    // check result
    if (result.error == null) {
      if (result.success && result.result != null && result.result!.isSuccess) {
        _appVerifyOTP.requestVerifyOTP(
          title: "ActivateAsCashier".tr,
          cidOrAccountNumber: activateInput.cidOrAccountNumber,
          phoneNumber: activateInput.phoneNumber,
          merchantId: "",
          activateType: ActivateType.cashier,
          onSuccess: onVerifyOTPSuccess,
        );
      } else {
        var errorMessage = result.result?.description ??
            "SomethingWentWrongPleaseTryAgainLater".tr;
        CChoiceDialog.ok(
            title: "Failure".tr,
            message: errorMessage,
            buttonTitle: "Ok".tr,
            ok: () {
              dismiss();
            });
      }
    } else {
      CChoiceDialog.ok(title: "Failure".tr, message: result.error!.details.tr);
    }
  }
  //#endregion

  //#region Validation

  void cidValidator() {
    if (inviteCodeInput.isEmpty) {
      cidValidateMessage = "InviteCodeRequired";
    } else {
      cidValidateMessage = ""; // CASE: INVITE_CODE_VALID
    }

    checkEnable();
  }

  void phoneValidator() {
    if (phoneInput.removeAllWhitespace.length >= 8) {
      phoneValidateMessage = "";
    } else if (phoneInput.isEmpty) {
      phoneValidateMessage = "PhoneNumberRequiredMessage";
    } else {
      phoneValidateMessage = "PhoneNumberInvalidMessage";
    }
    checkEnable();
  }

  void checkEnable() {
    isEnable = phoneValidateMessage.isEmpty &&
        cidValidateMessage.isEmpty &&
        inviteCodeInput.isNotEmpty &&
        phoneInput.isNotEmpty;
  }
  //#endregion

  //#region All Command
  Future<void> onNextPress() async {
    // check vailidation
    cidValidator();
    phoneValidator();

    await onActivateAsyn();
  }

  //#endregion

}
