import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class ActivateOwnerScreenController extends AppController {
  //#region All Override method
  //#endregion

  //#region All Properties

  final AuthenticateService _authenticateService = AuthenticateService();
  final AppVerifyOTP _appVerifyOTP = AppVerifyOTP();

  final _activateInput = ActivateInputModel().obs;
  ActivateInputModel get activateInput => _activateInput.value;
  set activateInput(ActivateInputModel value) => _activateInput.value = value;

  RegisterAccountInputModel registerAccountInput = RegisterAccountInputModel();

  final _activateType = ActivateType.businessOwner.obs;
  ActivateType get activateType => _activateType.value;
  set activateType(ActivateType value) => _activateType.value = value;

  final _cidValidateMessage = ''.obs;
  String get cidValidateMessage => _cidValidateMessage.value;
  set cidValidateMessage(String value) => _cidValidateMessage.value = value;

  final _phoneValidateMessage = ''.obs;
  String get phoneValidateMessage => _phoneValidateMessage.value;
  set phoneValidateMessage(String value) => _phoneValidateMessage.value = value;

  final _merchantIdValidateMessage = ''.obs;
  String get merchantIdValidateMessage => _merchantIdValidateMessage.value;
  set merchantIdValidateMessage(String value) =>
      _merchantIdValidateMessage.value = value;

  final _cidOrAccountNumberInput = ''.obs;
  String get cidOrAccountNumberInput => _cidOrAccountNumberInput.value;
  set cidOrAccountNumberInput(String value) =>
      _cidOrAccountNumberInput.value = value;

  final _phoneInput = ''.obs;
  String get phoneInput => _phoneInput.value;
  set phoneInput(String value) => _phoneInput.value = value;

  final _merchantId = ''.obs;
  String get merchantId => _merchantId.value;
  set merchantId(String value) => _merchantId.value = value;
  //#endregion

  //#region All Method Helpers
  void onVerifyOTPSuccess(
    String otpCode,
    String cidOrAccountNumber, {
    String merchantId = "",
  }) {
    Get.back(); // CLOSE OPT SCREEN
    registerAccountInput.optCode = otpCode;
    registerAccountInput.phoneNumber =
        activateInput.phoneNumber.removeAllWhitespace;
    var fromPage = "";
    if (activateType == ActivateType.existingMerchant) {
      registerAccountInput.cid = cidOrAccountNumber.removeAllWhitespace;
      registerAccountInput.merchantId = merchantId;
      fromPage = Routes.activateExistingMerchantScreen;
      // registerAccountInput. = 2;
    } else if (activateType == ActivateType.businessOwner) {
      registerAccountInput.cid = cidOrAccountNumberInput.removeAllWhitespace;
      fromPage = Routes.activateOwnerScreen;
      // activateInput.activateType = 0;
    }
    var arguments = <String, dynamic>{
      ArgumentKey.fromPage: fromPage,
      ArgumentKey.item: registerAccountInput
    };
    navigationToNamedArgumentsAsync(Routes.activateCreateUsernameScreen,
        args: arguments);
  }

  Future<void> onActivateAsyn() async {
    // input model
    activateInput.phoneNumber = phoneInput.removeAllWhitespace;

    if (activateType == ActivateType.existingMerchant) {
      activateInput.cidOrAccountNumber = merchantId.removeAllWhitespace;
      activateInput.activateType = 2;
    } else if (activateType == ActivateType.businessOwner) {
      activateInput.cidOrAccountNumber =
          cidOrAccountNumberInput.removeAllWhitespace;
      activateInput.activateType = 0;
    }

    showLoading();
    var result = await _authenticateService.activateAsync(activateInput);
    dismiss();

    // check result
    if (result.error == null) {
      if (result.success && result.result != null && result.result!.isSuccess) {
        if (activateType == ActivateType.existingMerchant) {
          _appVerifyOTP.requestVerifyOTP(
              title: "ActivateAsBusinessOwner".tr,
              cidOrAccountNumber: "",
              merchantId: merchantId.removeAllWhitespace,
              phoneNumber: activateInput.phoneNumber,
              onSuccess: (otpCode, cidOrAccountNumber) {
                onVerifyOTPSuccess(
                  otpCode,
                  cidOrAccountNumber,
                  merchantId: merchantId.removeAllWhitespace,
                );
              },
              activateType: activateType);
        } else if (activateType == ActivateType.businessOwner) {
          _appVerifyOTP.requestVerifyOTP(
              title: "ActivateAsBusinessOwner".tr,
              cidOrAccountNumber: cidOrAccountNumberInput.removeAllWhitespace,
              merchantId: "",
              phoneNumber: activateInput.phoneNumber,
              onSuccess: onVerifyOTPSuccess,
              activateType: activateType);
        }
      } else {
        var errorMessage = result.result?.description ??
            "SomethingWentWrongPleaseTryAgainLater".tr;
        CChoiceDialog.ok(
            title: "Failure".tr,
            message: errorMessage,
            buttonTitle: "Ok".tr,
            ok: () {
              dismiss();
            });
      }
    } else {
      CChoiceDialog.ok(title: "Failure".tr, message: result.error!.details.tr);
    }
  }
  //#endregion

  //#region Validation

  void cidOrAccountNumberValidator() {
    if (cidOrAccountNumberInput.removeAllWhitespace.length >= 6) {
      cidValidateMessage = ""; // CASE: CID_VALID
    } else if (cidOrAccountNumberInput.isEmpty) {
      cidValidateMessage = "CIDRequiredMessage";
    } else {
      cidValidateMessage = "CIDInvalidMessage";
    }
    checkEnable();
  }

  void phoneValidator() {
    if (phoneInput.removeAllWhitespace.length >= 8) {
      phoneValidateMessage = "";
    } else if (phoneInput.isEmpty) {
      phoneValidateMessage = "PhoneNumberRequiredMessage";
    } else {
      phoneValidateMessage = "PhoneNumberInvalidMessage";
    }
    checkEnable();
  }

  void merchantIdValidator() {
    if (merchantId.isEmpty) {
      merchantIdValidateMessage = "MerchantIdRequired";
    } else if (merchantId.length < 9) {
      merchantIdValidateMessage = "MerchantIdInvalid";
    } else {
      merchantIdValidateMessage = ""; // CASE: MERCHANT_ID_VALID
    }

    checkEnable();
  }

  void checkEnable() {
    if (activateType == ActivateType.businessOwner) {
      isEnable = phoneValidateMessage.isEmpty &&
          cidValidateMessage.isEmpty &&
          cidOrAccountNumberInput.isNotEmpty &&
          phoneInput.isNotEmpty;
    } else if (activateType == ActivateType.existingMerchant) {
      var midNoSpace = merchantId.removeAllWhitespace;
      isEnable = phoneValidateMessage.isEmpty &&
          merchantIdValidateMessage.isEmpty &&
          merchantId.isNotEmpty &&
          (midNoSpace.length == 9 || midNoSpace.length == 15) &&
          phoneInput.isNotEmpty;
    }
  }
  //#endregion

  //#region All Command
  Future<void> onNextPress() async {
    // check vailidation
    cidOrAccountNumberValidator();
    phoneValidator();

    await onActivateAsyn();
  }

  //#endregion
}
