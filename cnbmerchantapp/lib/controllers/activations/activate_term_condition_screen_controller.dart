import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ActivateTermConditionScreenController extends BaseWebController {
  //#region All Override method
  @override
  void onInit() async {
    getArgument();
    super.onInit();
  }

  //#endregion

  //#region All Properties
  String fromPage = "";

  final _termsAggree = [false, false, false].obs;
  List<bool> get termsAggree => _termsAggree;
  set termsAggree(List<bool> value) => _termsAggree.value = value;

  final GlobalKey webViewKey = GlobalKey();
  String url = "";

  //#endregion

  //#region All Method Helpers
  void getArgument() {
    String locale = Get.locale?.countryCode ?? LanguageOption.conCodeEn;
    if (locale == LanguageOption.conCodeZh) {
      locale = 'CN';
    } else if (locale == LanguageOption.conCodeKh) {
      locale = 'KH';
    } else {
      locale = 'EK';
    }
    url = ValueConst.activateTermConditonUrl + locale;
    if (Get.arguments == null) return;
    if (Get.arguments[ArgumentKey.fromPage] != null) {
      fromPage = Get.arguments[ArgumentKey.fromPage];
    }
  }
  //#endregion

  //#region Validation
  //#endregion

  //#region All Command
  /// checking on user selected accept terms condition radio button
  void checkChange(int index) {
    termsAggree[index] = !termsAggree[index];

    /// index == 2 mean user select on accept all radio box button
    if (index == 2) {
      isEnable = !isEnable;
      termsAggree[0] = isEnable;
      termsAggree[1] = isEnable;
    } else {
      /// index == 0, 1 mean user select on terms 1, 2
      if (termsAggree[0] == true && termsAggree[1] == true) {
        isEnable = true;
      } else {
        isEnable = false;
      }
    }
  }

  onNextBtnPress() {
    var arguments = {ArgumentKey.fromPage: fromPage};
    toNamedArgumentsAsync(fromPage, args: arguments);
  }

  onConditionPress() {
    toNamedArgumentsAsync(Routes.merchantTermsAndCondition);
  }

  onAgreementPress() {
    toNamedArgumentsAsync(Routes.partnershipAgreementScreen);
  }
  //#endregion

}
