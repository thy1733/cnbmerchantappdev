import 'package:flutter/material.dart';
import 'package:teledart/model.dart';
import 'package:teledart/teledart.dart';

enum AppBotCommand { start, showmyid }

class TelegramBot {
  static final TelegramBot _instance = TelegramBot._internal();
  late TeleDart _teleDart;

  factory TelegramBot({required String token, required String username}) {
    _instance._teleDart = TeleDart(token, Event(username));
    return _instance;
  }

  void start() {
    try {
      _teleDart.start();
    } on Exception catch (e) {
      debugPrint("Exception: ${e.toString()}");
    }
  }

  void stop() {
    _teleDart.stop();
    _teleDart.close();
  }

  void onMessage(Function(TeleDart, Message) handler) {
    _teleDart.onMessage().listen((message) => handler(_teleDart, message));
  }

  void onCommand(AppBotCommand command, Function(TeleDart, Message) handler) {
    switch (command) {
      case AppBotCommand.start:
        _teleDart
            .onCommand('start')
            .listen((message) => handler(_teleDart, message));
        break;
      case AppBotCommand.showmyid:
        _teleDart
            .onCommand('showmyid')
            .listen((message) => handler(_teleDart, message));
        break;
      default:
        break;
    }
  }

  TelegramBot._internal();
}
