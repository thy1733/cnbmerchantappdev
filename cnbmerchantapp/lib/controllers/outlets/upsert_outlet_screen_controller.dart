import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

class UpsertOutletScreenController extends AppController {
  final IAppFileUploadService _fileUploadService = AppFileUploadService();
  final OutletService _outletService = OutletService();
  final CashierService _cashierService = CashierService();
  final OutletListScreenController outletListController =
      Get.find<OutletListScreenController>();
  final TextEditingController outletNameController = TextEditingController();
  final TextEditingController addressInputConroller = TextEditingController();
  final TextEditingController locationNameConroller = TextEditingController();
  final TextEditingController telegramGroupIDConroller =
      TextEditingController();

  //#region All Override method
  @override
  void onInit() {
    getBusinessInfoFromArgument();
    getOutletInfoFromArgument();

    onSetValue();
    if (isUpdated) onSetUpdateValue();

    super.onInit();
  }

  @override
  void onReady() {
    retrieveListCashiers();

    super.onReady();
  }

  @override
  void onClose() {
    outletNameFocus.dispose();

    super.onClose();
  }

  //#endregion

  //#region All Properties

  final _businessInfo = ManageBusinessItemListOutputModel().obs;
  ManageBusinessItemListOutputModel get businessInfo => _businessInfo.value;
  set businessInfo(ManageBusinessItemListOutputModel value) =>
      _businessInfo.value = value;

  final _outletInfo = OutletDetailOutputModel().obs;
  OutletDetailOutputModel get outletInfo => _outletInfo.value;
  set outletInfo(OutletDetailOutputModel value) => _outletInfo.value = value;

  final _isFormValidated = true.obs;
  bool get isFormValidated => _isFormValidated.value;
  set isFormValidated(bool value) => _isFormValidated.value = value;

  final _businessName = "".obs;
  String get businessName => _businessName.value;
  set businessName(String value) => _businessName.value = value;

  final _outletName = "".obs;
  String get outletName => _outletName.value;
  set outletName(String value) => _outletName.value = value;

  final _outletNameValidateMsg = "".obs;
  String get outletNameValidateMsg => _outletNameValidateMsg.value;
  set outletNameValidateMsg(String value) =>
      _outletNameValidateMsg.value = value;

  final _outletNameFocus = FocusNode().obs;
  FocusNode get outletNameFocus => _outletNameFocus.value;
  set outletNameFocus(FocusNode value) => _outletNameFocus.value = value;

  final _outletLocationNameValidateMsg = "".obs;
  String get outletLocationNameValidateMsg =>
      _outletLocationNameValidateMsg.value;
  set outletLocationNameValidateMsg(String value) =>
      _outletLocationNameValidateMsg.value = value;

  final _outletLocationAddress = "".obs;
  String get outletLocationAddress => _outletLocationAddress.value;
  set outletLocationAddress(String value) =>
      _outletLocationAddress.value = value;

  final _outletLocationAddressValidateMsg = "".obs;
  String get outletLocationAddressValidateMsg =>
      _outletLocationAddressValidateMsg.value;
  set outletLocationAddressValidateMsg(String value) =>
      _outletLocationAddressValidateMsg.value = value;

  final _cashiersInOutletValidateMsg = "".obs;
  String get cashiersInOutletValidateMsg => _cashiersInOutletValidateMsg.value;
  set cashiersInOutletValidateMsg(String value) =>
      _cashiersInOutletValidateMsg.value = value;

  final _cashiers = <ItemSelectOptionModel>[].obs;
  List<ItemSelectOptionModel> get cashiers => _cashiers;
  set cashiers(List<ItemSelectOptionModel> value) => _cashiers.value = value;

  final _selectedCashiers = <ItemSelectOptionModel>[].obs;
  List<ItemSelectOptionModel> get selectedCashiers => _selectedCashiers;
  set selectedCashiers(List<ItemSelectOptionModel> value) =>
      _selectedCashiers.value = value;

  final _locationModel = LocationAddressModel().obs;
  LocationAddressModel get locationModel => _locationModel.value;
  set locationModel(LocationAddressModel value) => _locationModel.value = value;

  final _hasPrimary = false.obs;
  bool get hasPrimary => _hasPrimary.value;
  set hasPrimary(bool value) => _hasPrimary.value = value;

  final _isPrimary = false.obs;
  bool get isPrimary => _isPrimary.value;
  set isPrimary(bool value) => _isPrimary.value = value;

  final _isUpdated = false.obs;
  bool get isUpdated => _isUpdated.value;
  set isUpdated(bool value) => _isUpdated.value = value;

  final _telegramGroupID = "".obs;
  String get telegramGroupID => _telegramGroupID.value;
  set telegramGroupID(String value) => _telegramGroupID.value = value;

  final _telegramGroupName = "".obs;
  String get telegramGroupName => _telegramGroupName.value;
  set telegramGroupName(String value) => _telegramGroupName.value = value;

  final _title = "".obs;
  String get title => _title.value;
  set title(String value) => _title.value = value;

  final _titleButton = "".obs;
  String get titleButton => _titleButton.value;
  set titleButton(String value) => _titleButton.value = value;

  final _isOutletPatentRegister = false.obs;
  bool get isOutletPatentRegister => _isOutletPatentRegister.value;
  set isOutletPatentRegister(bool value) =>
      _isOutletPatentRegister.value = value;

  final _cacheKey = "".obs;
  String get cacheKey => _cacheKey.value;
  set cacheKey(String value) => _cacheKey.value = value;

  //#endregion

  //#region All Method Helpers

  Future<void> upsertOutletAsync() async {
    isUpdated ? updateOutletAsync() : createOutletAsync();
  }

  Future<void> retrieveListCashiers() async {
    var input = CashierListInputModel();
    input.businessId = businessInfo.id;

    showLoading();
    var response = await _cashierService.getListCashier(input);

    if (response.success && response.result != null) {
      var items = response.result!.items;

      cashiers = items
          .where((element) =>
              element.status.toLowerCase() !=
              CashierStatus.deleted.displayTitle.toLowerCase())
          .map(
              (element) => ItemSelectOptionModel.createItemData(element, false))
          .toList();

      for (var cashier in cashiers) {
        for (var selectedCashier in selectedCashiers) {
          if (cashier.id == selectedCashier.id) {
            cashier.isSelected = cashier.id == selectedCashier.id;
            break;
          }
        }
      }

      dismiss();
    } else {
      dismiss();
    }
  }

  Future<void> createOutletAsync() async {
    if (!onFormValidated()) return;

    showLoading();

    try {
      var input = SetupOutletInputModel();
      input.businessId = businessInfo.id;
      input.outletName = outletName.trim();
      input.outletLogo = fileUpload.fileName;
      input.locationName = locationModel.locationName.trim();
      input.locationAddress =
          locationModel.address.replaceSemiColonToComma().trim();
      input.latitude = locationModel.latitude;
      input.longitude = locationModel.longitude;

      if (selectedCashiers.isNotEmpty) {
        var ids = selectedCashiers.map((e) => e.data.id).toList();
        input.cashiers = ids.join("!");
      }
      input.isPrimary = isPrimary;
      input.telegramGroupId = telegramGroupID;
      var response = await _outletService.createOutlet(input);
      if (response.success && response.result != null) {
        var inputFile = UploadFileInputDto();
        inputFile.uploadType = UploadProfileType.outletProfile;
        inputFile.fileName = input.outletLogo;
        inputFile.id = businessInfo.id;
        inputFile.filePath = fileUpload.filePath;
        if (inputFile.filePath.isNotEmpty && inputFile.fileName.isNotEmpty) {
          await _fileUploadService.uploadFileAsync(input: inputFile);
        }

        super.dismiss();

        Get.back(result: "${response.result}");
      } else {
        super.dismiss();

        Fluttertoast.showToast(
            msg:
                response.result?.description ?? "Error: Cannot create outlet.");
      }
    } catch (e) {
      super.dismiss();
      debugPrint("Error: ${e.toString()}");
    }
  }

  Future<void> updateOutletAsync() async {
    if (!onFormValidated()) return;

    try {
      var input = UpdateOutletInputModel();
      input.outletId = outletInfo.outletId;
      input.businessId = businessInfo.id;
      input.accountNumber = outletInfo.accountNumber;
      input.outletName = outletName.trim();
      input.outletLogo = outletInfo.outletLogoId.isEmpty
          ? fileUpload.fileName
          : outletInfo.outletLogoId;
      input.locationName =
          locationModel.locationName.replaceSemiColonToComma().trim();
      input.locationAddress =
          outletLocationAddress.replaceSemiColonToComma().trim();
      input.latitude = locationModel.latitude;
      input.longitude = locationModel.longitude;

      var ids = selectedCashiers.map((e) => e.id).toList();
      input.cashiers = ids.join("!");

      input.isPrimary = isPrimary;
      input.telegramGroupId = telegramGroupID;
      input.telegramGroupName = telegramGroupName;

      showLoading();
      var response = await _outletService.updateOutlet(input);

      if (response.success && response.result != null) {
        var inputFile = UploadFileInputDto();
        inputFile.uploadType = UploadProfileType.outletProfile;
        inputFile.fileName = input.outletLogo;
        inputFile.id = input.businessId;
        inputFile.filePath = fileUpload.filePath;
        if (inputFile.filePath.isNotEmpty && inputFile.fileName.isNotEmpty) {
          await _fileUploadService.uploadFileAsync(input: inputFile);
          await clearCacheImage(outletInfo.outletLogo, cacheKey: cacheKey);
        }

        dismiss();
        Get.back(result: "${response.result}");
      } else {
        dismiss();

        Fluttertoast.showToast(
            msg:
                response.result?.description ?? "Error: Cannot update outlet.");
      }
    } catch (e) {
      dismiss();

      debugPrint("Error: ${e.toString()}");
    }
  }

  void onSetUpdateValue() {
    telegramGroupID = outletInfo.telegramGroupId;
    telegramGroupName = outletInfo.telegramGroupName;

    outletName = outletInfo.outletName;
    outletNameController.text = outletInfo.outletName;

    locationModel.locationName = outletInfo.locationName;
    locationModel.address = outletInfo.locationAddress;
    locationModel.latitude = outletInfo.latitude;
    locationModel.longitude = outletInfo.longitude;
    locationNameConroller.text = locationModel.locationName;
    addressInputConroller.text = locationModel.address.replaceToComma();
    // outletLocationAddress = locationModel.address.replaceToComma();

    selectedCashiers = outletInfo.cashiers
        .where((element) =>
            element.status.toLowerCase() !=
            CashierStatus.deleted.displayTitle.toLowerCase())
        .map((e) => ItemSelectOptionModel.createItemData(e, true))
        .toList();

    isPrimary = outletInfo.isPrimary;
    // isOutletPatentRegister = !isPrimary;
  }

  void onSetValue() {
    businessName = businessInfo.businessName;
    isPrimary = outletInfo.isPrimary;
    title = isUpdated ? "Edit".tr : "NewOutlet".tr;
    titleButton = isUpdated ? "Save".tr : "CreateOutlet".tr;
  }

  Future<void> getBusinessInfoFromArgument() async {
    if (Get.arguments != null &&
        Get.arguments.containsKey(ArgumentKey.detail)) {
      var result = Get.arguments[ArgumentKey.detail]
          as ManageBusinessItemListOutputModel;
      businessInfo = result;
    }
  }

  Future<void> getOutletInfoFromArgument() async {
    if (Get.arguments != null && Get.arguments.containsKey(ArgumentKey.item)) {
      var result = Get.arguments[ArgumentKey.item] as OutletDetailOutputModel;
      outletInfo = result;
      isUpdated = Get.arguments[ArgumentKey.fromPage] == OutletMode.UPDATE;
      hasPrimary = Get.arguments[ArgumentKey.id] || outletInfo.isPrimary;
      cacheKey = Get.arguments[ArgumentKey.imgCacheKey];
    }
  }

  void uploadOutletLogo() async {
    try {
      await super.pickImagePushAsync().then((value) async {
        if (value != null) {
          photoFile = await value.readAsBytes();
          var fileName = DateTime.now().toFormatDateString("yyyymmddhhmmsss");
          fileUpload.fileName = fileName;
          fileUpload.filePath = value.path;
        }
      });
    } catch (e) {
      debugPrint("Error: ${e.toString()}");
    }
  }

  void callDeleted() {
    var locationController = Get.find<LocationScreenController>();
    locationController.dismissKeyboard();
    Get.delete<LocationScreenController>();
  }

  void addCashier() {
    cashiers.isNotEmpty
        ? showDialog(
            context: Get.context!,
            builder: (_) {
              return UpdateCashierInOutletDialog(
                title: 'AddCashier'.tr,
                items:
                    cashiers.where((element) => !element.isSelected).toList(),
                onSelected: (ItemSelectOptionModel value) {
                  cashiers
                      .where((element) => element.id == value.id)
                      .map((e) => e.isSelected = true)
                      .toList();
                  selectedCashiers.add(value);

                  Get.back();
                },
              );
            })
        : Fluttertoast.showToast(
            msg: "NoCashierYet".tr, toastLength: Toast.LENGTH_LONG);
  }

  void removeCashier(ItemSelectOptionModel value) {
    CChoiceDialog.okCancel(
      title: "RemoveCashier".tr,
      message: "RemoveCashierFromOutletsMsg".tr,
      okTitle: "Remove".tr,
      onOk: () {
        cashiers
            .where((element) => element.id == value.id)
            .map((e) => e.isSelected = false)
            .toList();
        selectedCashiers.remove(value);

        Get.back();
      },
    );
  }

  //#endregion

  //#region All Command

  bool onFormValidated() {
    onOutletNameValidate();
    onOutletNameFocus();

    onOutletLocationValidate();

    return outletNameValidateMsg.isEmpty &&
        outletLocationNameValidateMsg.isEmpty &&
        outletLocationAddressValidateMsg.isEmpty;
  }

  void onOutletNameChanged(String value) {
    outletName = value;
    onOutletNameValidate();
  }

  /* 
    As the KHQR String limit charactors, so the available length for Business Name and 
    Outlet Name Cannot be longer 25 charactors.
   */
  void onOutletNameValidate() {
    outletNameValidateMsg = outletName.isEmpty
        ? "OutletNameRequired".tr
        : outletName.isNotEmpty &&
                (outletName.length + businessName.length > 25)
            ? "NotMoreThenFifteenChars".tr
            : "";
  }

  void onOutletNameFocus() {
    outletNameValidateMsg.isNotEmpty
        ? outletNameFocus.requestFocus()
        : outletNameFocus.unfocus();
  }

  void onOutletLocationAddressChanged(String value) {
    outletLocationAddress = value;
    onOutletLocationValidate();
  }

  void onPrimaryChanged(bool value) {
    isPrimary = value;
  }

  Future<void> browseLocation() async {
    await showCupertinoModalPopup(
        context: Get.context!,
        builder: (context) => LocationScreen(
              initLocation: locationModel,
              locationCallBack: (value) {
                locationModel = value;

                locationNameConroller.text = locationModel.locationName;
                addressInputConroller.text = locationModel.address;

                outletLocationAddress =
                    addressInputConroller.text.replaceSemiColonToComma();

                onOutletLocationValidate();
              },
            )).then((value) => callDeleted());
  }

  void onOutletLocationValidate() {
    outletLocationNameValidateMsg = locationNameConroller.text.isEmpty
        ? "OutletLocationRequired".tr
        : AppTextInputRegx.isUnicode(locationNameConroller.text)
            ? "UnicodeNotAllow".tr
            : "";

    outletLocationAddressValidateMsg = addressInputConroller.text.isEmpty
        ? "OutletAddressRequired".tr
        : AppTextInputRegx.isUnicode(addressInputConroller.text)
            ? "UnicodeNotAllow".tr
            : "";
  }

  void showLinkedTelegramGroup() {
    final double wid = Get.width;
    final double size = wid / 8;
    final double padding = 16.r;

    showCupertinoModalBottomSheet(
        context: Get.context!,
        isDismissible: false,
        builder: (_) {
          return BottomSheetBusinessDetail(
              child: Column(
            children: [
              SizedBox(
                height: padding,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SvgPicture.asset(
                    SvgAppAssets.icLinkTelegramGroup,
                    width: size,
                    height: size,
                  ),
                  const Icon(
                    Icons.telegram,
                    color: Colors.blue,
                  ),
                ],
              ),
              SizedBox(
                height: padding - 6,
              ),
              Text(
                "LinkTelegramGroup".tr,
                textAlign: TextAlign.center,
                style: AppTextStyle.header2,
              ),
              SizedBox(
                height: padding,
              ),
              Padding(
                padding: EdgeInsets.only(left: padding, right: padding),
                child: Column(
                  children: [
                    KTextField(
                      controller: telegramGroupIDConroller,
                      maxLength: 21,
                      name: "EnterTelegramGroupID".tr,
                      needIcon: false,
                      validatorMessage: "",
                      keyboardType: TextInputType.number,
                      onChanged: (value) {
                        telegramGroupID = value!;
                      },
                    ),
                    ButtonCircleRadius(
                      title: "Done".tr,
                      submit: (value) {
                        if (telegramGroupID.isNotEmpty) {
                          Get.back();
                        } else {
                          Fluttertoast.showToast(
                              msg: "PleaseEnterTelegramGroupID".tr);
                        }
                      },
                    ),
                    SizedBox(
                      height: padding,
                    )
                  ],
                ),
              ),
            ],
          ));
        });
  }

  //#endregion
}
