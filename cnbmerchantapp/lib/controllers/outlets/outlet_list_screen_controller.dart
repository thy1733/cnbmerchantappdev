import 'dart:async';
import 'dart:math';

import 'package:cnbmerchantapp/core.dart';
import 'package:cnbmerchantapp/helpers/components/snackBars/c_snack_bar.dart';
import 'package:cnbmerchantapp/helpers/enums/outlet_status_enum.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:teledart/model.dart';
import 'package:teledart/telegram.dart';
import 'package:url_launcher/url_launcher_string.dart';

class OutletListScreenController extends AppController {
  final OutletService _outletService = OutletService();
  final BusinessDetailScreenController businessDetailController =
      Get.find<BusinessDetailScreenController>();
  final IAppOutletCacheService _outletCacheService = AppOutletCacheService();

  //#region All Override method

  @override
  void onInit() async {
    super.onInit();

    await getTelegramToken();
  }

  @override
  void onReady() async {
    await retrieveOutletList();

    super.onReady();
  }

  @override
  void onClose() {
    teledart.stop();

    super.onClose();
  }

  //#endregion

  //#region All Properties

  final _outlets = <OutletItemListOutputModel>[].obs;
  List<OutletItemListOutputModel> get outlets => _outlets;
  set outlets(List<OutletItemListOutputModel> value) => _outlets.value = value;

  final _cashiersInOutlet = <OptionItemModel>[].obs;
  List<OptionItemModel> get cashiersInOutlet => _cashiersInOutlet;
  set cashiersInOutlet(List<OptionItemModel> value) =>
      _cashiersInOutlet.value = value;

  final _isActivateOutlet = true.obs;
  bool get isActivateOutlet => _isActivateOutlet.value;
  set isActivateOutlet(bool value) => _isActivateOutlet.value = value;

  final _isEmptyData = false.obs;
  bool get isEmptyData => _isEmptyData.value;
  set isEmptyData(bool value) => _isEmptyData.value = value;

  final _isRegisteredOutlet = false.obs;
  bool get isRegisteredOutlet => _isRegisteredOutlet.value;
  set isRegisteredOutlet(bool value) => _isRegisteredOutlet.value = value;

  final _detailBusiness = ManageBusinessItemListOutputModel().obs;
  ManageBusinessItemListOutputModel get detailBusiness => _detailBusiness.value;
  set detailBusiness(ManageBusinessItemListOutputModel value) =>
      _detailBusiness.value = value;

  final _outletDetail = OutletItemListOutputModel().obs;
  OutletItemListOutputModel get outletDetail => _outletDetail.value;
  set outletDetail(OutletItemListOutputModel value) =>
      _outletDetail.value = value;

  final _chatId = "".obs;
  String get chatId => _chatId.value;
  set chatId(String value) => _chatId.value = value;

  final _chatGroupName = "".obs;
  String get chatGroupName => _chatGroupName.value;
  set chatGroupName(String value) => _chatGroupName.value = value;

  final _fourDigits = "".obs;
  String get fourDigits => _fourDigits.value;
  set fourDigits(String value) => _fourDigits.value = value;

  final _isTelegramLinked = false.obs;
  bool get isTelegramLinked => _isTelegramLinked.value;
  set isTelegramLinked(bool value) => _isTelegramLinked.value = value;

  final _isUpdateTelegramLinked = false.obs;
  bool get isUpdateTelegramLinked => _isUpdateTelegramLinked.value;
  set isUpdateTelegramLinked(bool value) =>
      _isUpdateTelegramLinked.value = value;

  final _isTelegramInit = false.obs;
  bool get isTelegramInit => _isTelegramInit.value;
  set isTelegramInit(bool value) => _isTelegramInit.value = value;

  final _isTelegramAvaliable = false.obs;
  bool get isTelegramAvaliable => _isTelegramAvaliable.value;
  set isTelegramAvaliable(bool value) => _isTelegramAvaliable.value = value;

  final _teledart = TelegramBot(token: "", username: "").obs;
  TelegramBot get teledart => _teledart.value;
  set teledart(TelegramBot value) => _teledart.value = value;

  final _username = "".obs;
  String get username => _username.value;
  set username(String value) => _username.value = value;

  final _selectedOutlet = OutletDetailOutputModel().obs;
  OutletDetailOutputModel get selectedOutlet => _selectedOutlet.value;
  set selectedOutlet(OutletDetailOutputModel value) =>
      _selectedOutlet.value = value;

  final _replyFromMsgId = "".obs;
  String get replyFromMsgId => _replyFromMsgId.value;
  set replyFromMsgId(String value) => _replyFromMsgId.value = value;

  final _telegramToken = "".obs;
  String get telegramToken => _telegramToken.value;
  set telegramToken(String value) => _telegramToken.value = value;

  String get welcomeMsg =>
      "👋 Hello! This is CanaMerchant Bot by Canadia Bank.\nPlease enter the 4 digits code given in the previous screen and send.";
  String get successMsg =>
      "🎉 Congratulations! You’ve successfully linked ${outletDetail.outletName} to this Telegram group.\nAll CanaMerchant Pay payment notifications will appear here.\nThank you for choosing Canadia Bank!";
  String get wrongFourditMsg =>
      "Please enter the 4 digits code given in the previous screen and send!";
  String get alreadyLinkedGroupMsg =>
      "This outlet has been linked to this telegram group!";

  //#endregion

  //#region All Method Helpers

  Future<void> getTelegramToken() async {
    telegramToken = AppUserTempInstant.appCacheUser.telegramToken;
    extractTelegramToken();
  }

  Future<void> initializeTelegram() async {
    runZonedGuarded(() async {
      teledart = TelegramBot(token: telegramToken, username: username);
      teledart.start();

      await listenToCommand();
      await listenToMessage();
    }, (error, stack) {
      debugPrint("Reason: ${error.toString()}");
    });
  }

  void setValueChatAttribute(Message message) {
    chatId = message.chat.id.toString();
    chatGroupName = message.chat.title ?? "";
    isTelegramLinked = chatId.isNotEmpty;
  }

  void showSuccessLinkedTelegram() {
    Get.back();

    KAlertScreen.success(
        message: "Success".tr,
        description:
            "${selectedOutlet.outletName} has been linked to $chatGroupName",
        onPrimaryButtonClick: () {
          Get.back();
          upsertOutletLinkTelegramGroup();
        });
  }

  void showSuccessUnlinkedTelegram() async {
    KAlertScreen.success(
        message: "Success".tr,
        description:
            "${selectedOutlet.outletName} has been unlinked from $chatGroupName",
        onPrimaryButtonClick: () {
          Get.back();
          onClearTelegramData();
          upsertOutletLinkTelegramGroup();
        });
  }

  Widget upsertLinkedTelegramSection() {
    return isTelegramAvaliable && isTelegramLinked
        ? successLinkedTelegramSection()
        : linkTelegramSection();
  }

  String generateFourDigits() {
    var random = Random();
    String value = random.nextInt(10000).toString();
    fourDigits = value.padRight(4, '0');

    return fourDigits;
  }

  Future<void> listenToCommand() async {
    if (GetPlatform.isMobile) {
      teledart.onCommand(AppBotCommand.start, (teledart, message) {
        replyFromMsgId = message.from?.id.toString() ?? "";
        teledart.sendMessage(message.chat.id, welcomeMsg);
      });
      teledart.onCommand(AppBotCommand.showmyid, (teledart, message) {
        teledart.sendMessage(
            message.chat.id, "Your Telegram Group ID: `${message.chat.id}`");
      });
    }
  }

  Future<void> listenToMessage() async {
    teledart.onMessage((teledart, message) async {
      replyFromMsgId = message.from?.id.toString() ?? "";

      if (replyFromMsgId.isNotEmpty &&
          message.text != null &&
          int.tryParse(message.text!) != null) {
        if (message.text!.length == fourDigits.length &&
            message.text! == fourDigits) {
          setValueChatAttribute(message);
          teledart.sendMessage(message.chat.id, successMsg);
          showSuccessLinkedTelegram();
        } else if (message.text!.length != fourDigits.length ||
            message.text != fourDigits) {
          teledart.sendMessage(message.chat.id, wrongFourditMsg);
        } else if (selectedOutlet.telegramGroupId ==
            message.chat.id.toString()) {
          teledart.sendMessage(message.chat.id, alreadyLinkedGroupMsg);
          Get.back();
        }
      }
    });
  }

  Future<void> linkedTelegraGroup() async {
    if (username.isNotEmpty) {
      String url =
          "https://t.me/$username?startgroup=true&admin=manage_chat,change_info,delete_messages,restrict_members,invite_users,pin_messages,manage_video_chats";

      try {
        await launchUrlString(
          url,
          mode: LaunchMode.externalNonBrowserApplication,
        );
      } catch (e) {
        Fluttertoast.showToast(
            msg: "Cannot join telegram group, due to: ${e.toString()}",
            toastLength: Toast.LENGTH_LONG);
      }
    }
  }

  Future<void> getTelegramUsername() async {
    var telegram = await Telegram(telegramToken).getMe();
    username = telegram.username ?? "";
    await initializeTelegram();
  }

  Future<void> handleChangeTelegramGroup() async {
    requestToConfirmPIN(
        title: "EnterPIN".tr,
        message: "ChangeTelegramGroup".tr,
        description: "YouAreGoingToCompleteTheProcess".tr,
        onSuccess: (value) async {
          Get.back();
          Get.back();

          showFourDigitsDialog();
        });
  }

  Future<void> handleUnlinkedTelegramGroup() async {
    Get.back();

    requestToConfirmPIN(
        title: "EnterPIN".tr,
        message: "UnlinkedTelegramGroup".tr,
        description:
            "${"UnlinkTelegramGroupMessage".tr}\n$chatGroupName($chatId)\nfrom ${selectedOutlet.outletName}",
        onSuccess: (value) async {
          Get.back();

          showSuccessUnlinkedTelegram();
        });
  }

  void onClearTelegramData() {
    isTelegramLinked = false;
    chatId = "";
    chatGroupName = "";
  }

  Future<void> upsertOutletLinkTelegramGroup() async {
    var input = UpdateOutletInputModel();
    input.outletId = selectedOutlet.outletId;
    input.businessId = selectedOutlet.businessId;
    input.accountNumber = selectedOutlet.accountNumber;
    input.outletName = selectedOutlet.outletName;
    input.outletLogo = selectedOutlet.outletLogoId;
    input.locationName = selectedOutlet.locationName;
    input.locationAddress = selectedOutlet.locationAddress;
    input.latitude = selectedOutlet.latitude;
    input.longitude = selectedOutlet.longitude;
    var ids = selectedOutlet.cashiers.map((e) => e.id).toList();
    input.cashiers = ids.join("!");
    input.isPrimary = selectedOutlet.isPrimary;
    input.telegramGroupId = chatId;
    input.telegramGroupName = chatGroupName;

    showLoading();
    var response = await _outletService.updateOutlet(input);
    dismiss();

    if (response.success) {
      isTelegramLinked = chatId.isNotEmpty;
      isUpdateTelegramLinked = true;

      retrieveOutletDetail(outletDetail);
    } else {
      Fluttertoast.showToast(
          msg: response.result?.description ??
              "Error: Cannot link outlet to telegram group!",
          toastLength: Toast.LENGTH_LONG);
    }
  }

  Future<void> retrieveOutletList() async {
    try {
      var input = OutletListInputModel();
      input.businessId = businessDetailController.businessDetail.id;
      input.filter = "";
      input.maxResultCount = 10;
      input.skipCount = 1;

      showLoading();
      var response = await _outletService.getListOutlet(input);

      if (response.result != null && response.success) {
        var data = response.result!.items;
        data.sort((a, b) => a.isPrimary
            .toString()
            .length
            .compareTo(b.isPrimary.toString().length));

        outlets = data;

        udpateOutletCache();

        dismiss();
      } else {
        dismiss();

        Fluttertoast.showToast(
            msg: response.result?.description ??
                "Error: Cannot get list outlets!");
      }
      isEmptyData = !response.success;
    } finally {
      detailBusiness = businessDetailController.businessDetail;
    }
  }

  Future<void> deactivateOutlet() async {
    try {
      var input = DeactivateOutletInputModel();
      input.outletId = selectedOutlet.outletId;

      showLoading();
      var response = await _outletService.deactivateOutlet(input);

      if (response.success) {
        Get.back();
        Get.back();

        dismiss();

        CSnackBar.showSnackBarTop(message: "DeactivatedOutletMsg".tr);
        udpateOutletCache();
      } else {
        dismiss();

        Fluttertoast.showToast(
            msg: response.error?.message ?? "Error: Cannot deactivate outlet.");
      }
    } finally {}
  }

  Future<void> activateOutlet() async {
    try {
      var input = ActivateOutletInputModel();
      input.outletId = selectedOutlet.outletId;

      showLoading();
      var response = await _outletService.activateOutlet(input);

      if (response.success) {
        Get.back();
        Get.back();

        dismiss();

        CSnackBar.showSnackBarTop(message: "ActivateOutletSuccess".tr);
        udpateOutletCache();
      } else {
        dismiss();

        Fluttertoast.showToast(
            msg: response.error?.message ?? "Error: Cannot activate outlet.");
      }
    } finally {}
  }

  void udpateOutletCache() async {
    if (AppBusinessTempInstant.selectedBusiness.id ==
        selectedOutlet.businessId) {
      var input = OutletListInputModel();
      input.businessId = AppBusinessTempInstant.selectedBusiness.id;
      var outletResult = await _outletService.getListOutlet(input);

      var toSaveoutlet = OutletListOutputModel();
      if (outletResult.result != null) {
        toSaveoutlet.items.assignAll(outletResult.result!.items);
      }
      toSaveoutlet.items = toSaveoutlet.items
          .where((e) =>
              e.status.toUpperCase() ==
                  OutletStatus.Active.name.toUpperCase() ||
              e.status.toUpperCase() == "TRUE")
          .toList();

      await _outletCacheService.saveOutlets(toSaveoutlet);

      // await _outletCacheService.getOutlets(AppBusinessTempInstant.selectedBusiness.id);
      if (AppOutletTempInstant.selectedOutlet.id == selectedOutlet.outletId) {
        await _outletCacheService
            .saveSelectedOutlet(AppOutletTempInstant.outletList.items.first);
      }
    }
  }

  Future<void> retrieveOutletDetail(OutletItemListOutputModel item) async {
    try {
      var input = OutletDetailInputModel();
      input.outletId = item.id;

      showLoading();
      var response = await _outletService.getOutletDetail(input);

      if (response.success && response.result != null) {
        isActivateOutlet =
            OutletStatus.Active.setValue(response.result!.status) ==
                OutletStatus.Active;

        selectedOutlet = response.result!;
        chatId = selectedOutlet.telegramGroupId;
        chatGroupName = selectedOutlet.telegramGroupName;
        isTelegramLinked = chatId.isNotEmpty;
        isTelegramAvaliable = telegramToken.isNotEmpty;

        dismiss();

        if (!isUpdateTelegramLinked) {
          showCupertinoModalBottomSheet(
              topRadius: const Radius.circular(23),
              backgroundColor: AppColors.barrierBackground,
              context: Get.context!,
              builder: (_) {
                return Obx(
                  () => BottomSheetBusinessDetail(
                      child: outletDetailBuilder(item.cacheKey)),
                );
              });
        }
      } else {
        dismiss();

        Fluttertoast.showToast(
            msg: response.error?.message ??
                "Error: Cannot retrieve outlet detail.");
      }
    } finally {}
  }

  Future<void> extractTelegramToken() async {
    telegramToken = telegramToken.contains("bot")
        ? telegramToken.replaceFirst("bot", "")
        : telegramToken;
    await getTelegramUsername();
  }

  //#endregion

  //#region All Command

  Future<void> toUpdateOutletScreenAsync(String cacheKey) async {
    try {
      var arguments = {
        ArgumentKey.detail: businessDetailController.businessDetail,
        ArgumentKey.item: selectedOutlet,
        ArgumentKey.fromPage: OutletMode.UPDATE,
        ArgumentKey.id: outlets.length == 1,
        ArgumentKey.imgCacheKey: cacheKey
      };
      var result = await navigationToNamedArgumentsAsync(
          Routes.setupOutletScreen,
          args: arguments);
      if (result.isNotEmpty) {
        showSuccessDialog(msg: "SavedOutletSuccessfully".tr);

        await clearCacheImage(selectedOutlet.outletLogo);
        retrieveOutletList();
        recallBusinessDetail();
      }
    } finally {
      dismiss();
    }
  }

  Future<void> toCreateOutletScreenAsync() async {
    try {
      var arguments = <String, dynamic>{
        ArgumentKey.detail: businessDetailController.businessDetail
      };
      var result = await navigationToNamedArgumentsAsync(
          Routes.setupOutletScreen,
          args: arguments);
      if (result.isNotEmpty) {
        showSuccessDialog(msg: "CreatedOutletSuccessfully".tr);

        retrieveOutletList();
        recallBusinessDetail();
      }
    } finally {
      dismiss();
    }
  }

  void recallBusinessDetail() {
    BusinessDetailScreenController businessDetailScreenController =
        Get.find<BusinessDetailScreenController>();
    businessDetailScreenController.retrieveBusinessDetail();
  }

  Future<void> onSearchOutletAsync() async {
    await showFullScreenSearch(
        context: Get.context!, delegate: SearchOutletScreenDelegate());
  }

  void showOutletDetail(OutletItemListOutputModel item) {
    isUpdateTelegramLinked = false;
    outletDetail = item;
    retrieveOutletDetail(item);
  }

  Widget outletDetailBuilder(String cacheKey) {
    final double padding = 16.r;

    return Column(
      children: [
        AbsorbPointer(
          absorbing: !isActivateOutlet,
          child: Padding(
            padding: EdgeInsets.all(padding),
            child: Column(
              children: [
                imageSection(selectedOutlet, cacheKey),
                Visibility(
                  visible: !isActivateOutlet,
                  child: Column(
                    children: [
                      SizedBox(
                        height: padding - 6,
                      ),
                      Container(
                          padding: const EdgeInsets.fromLTRB(5, 2, 5, 2),
                          decoration: BoxDecoration(
                              color: AppColors.greyishDisable,
                              borderRadius: BorderRadius.circular(5)),
                          child: Text(
                              style: AppTextStyle.caption,
                              !isActivateOutlet ? "OutletInactive".tr : "")),
                    ],
                  ),
                ),
                SizedBox(
                  height: padding - 6,
                ),
                Text(
                  selectedOutlet.outletName,
                  textAlign: TextAlign.center,
                  style: AppTextStyle.header1,
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 4),
                  child: Text(
                    "${"OutletID".tr}: ${selectedOutlet.outletId}",
                    style: AppTextStyle.caption
                        .copyWith(color: AppColors.textSecondary),
                  ),
                ),
                TextButton(
                  onPressed: () {
                    Get.back();
                    toUpdateOutletScreenAsync(cacheKey);
                  },
                  style: AppButtonStyle.kSecondaryButtonStyle(),
                  child: Text(
                    "Edit".tr,
                    textAlign: TextAlign.center,
                    style: AppTextStyle.primary,
                  ),
                ),
                cashierSection(),
                outletInfoSection(),
                SizedBox(
                  height: padding,
                ),
                upsertLinkedTelegramSection(),
              ],
            ),
          ),
        ),
        Container(
          width: Get.width,
          height: 1,
          color: AppColors.greyishDisable,
        ),
        Visibility(
          visible: !selectedOutlet.isPrimary,
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: padding),
            child: TextButton(
              style: AppButtonStyle.kSecondaryButtonExpandStyle(),
              onPressed: () {
                showDeactivateOutletDailog();
              },
              child: Text(
                isActivateOutlet ? "DeactivateOutlet".tr : "ActivateOutlet".tr,
                style: AppTextStyle.primary1,
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget imageSection(OutletDetailOutputModel item, String cacheKey) {
    final double wid = Get.width;
    final double size = wid / 5.5;
    // final padding = 16.w;

    return Container(
      padding: const EdgeInsets.all(1),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(60.r),
          border: Border.all(color: AppColors.grey)),
      child: ClipOval(
        child: AppNetworkImage(
          item.outletLogo,
          cacheKey: cacheKey,
          width: size,
          height: size,
          errorWidget: SvgPicture.asset(SvgAppAssets.pOutletDetail),
        ),
      ),
    );
  }

  void showDeactivateOutletDailog() {
    if (isActivateOutlet) {
      CChoiceDialog.okCancel(
          title: "DeactivateOutlet".tr,
          okTitle: "Deactivate".tr,
          message: "ConfirmDeactivationMsg".tr,
          onOk: () {
            Get.back();
            deactivateOutlet();
          });
    } else {
      CChoiceDialog.okCancel(
          title: "ActivateOutlet".tr,
          okTitle: "Activate".tr,
          message: "ActivateOutletMsg".tr,
          onOk: () {
            Get.back();

            activateOutlet();
          });
    }
  }

  Widget cashierSection() {
    final double wid = Get.width;
    final double itemHeight = wid / 4;
    final double size = 34.w;
    // final double padding = 8.w;

    return Visibility(
      visible: selectedOutlet.cashiers.isNotEmpty,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Text(
            "Cashiers".tr,
            style: AppTextStyle.header1,
          ),
          SizedBox(
            height: itemHeight,
            child: ListView.builder(
                itemCount: selectedOutlet.cashiers.length,
                physics: const AlwaysScrollableScrollPhysics(),
                shrinkWrap: true,
                scrollDirection: Axis.horizontal,
                itemBuilder: (context, index) {
                  CashierItemListOutputModel cashier =
                      selectedOutlet.cashiers[index];
                  return Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(50),
                              border: Border.all(color: AppColors.grey)),
                          child: ClipOval(
                              child: AppNetworkImage(
                            cashier.photoUrl,
                            cacheKey: cashier.cacheImage,
                            width: size,
                            height: size,
                            errorWidget: PlaceholderImage.userName(
                                selectedOutlet.cashiers[index].firstName,
                                selectedOutlet.cashiers[index].lastName,
                                fontSize: 14),
                          )),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Text(
                          "${selectedOutlet.cashiers[index].firstName} ${selectedOutlet.cashiers[index].lastName}",
                          style: AppTextStyle.value,
                        )
                      ],
                    ),
                  );
                }),
          )
        ],
      ),
    );
  }

  Widget outletInfoSection() {
    final double padding = 16.r;

    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Text(
          "Information".tr,
          style: AppTextStyle.header1,
        ),
        SizedBox(
          height: padding - 6,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Icon(Icons.location_on_outlined),
            SizedBox(
              width: padding - 6,
            ),
            Expanded(
                child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "OutletLocation".tr,
                  style: AppTextStyle.label1,
                ),
                const SizedBox(
                  height: 6,
                ),
                Text(
                  selectedOutlet.locationName.replaceSemiColonToComma(),
                  style: AppTextStyle.value,
                ),
              ],
            ))
          ],
        ),
        SizedBox(
          height: padding,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Icon(Icons.location_on_outlined),
            SizedBox(
              width: padding - 6,
            ),
            Expanded(
                child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "OutletAddress".tr,
                  style: AppTextStyle.label1,
                ),
                const SizedBox(
                  height: 6,
                ),
                Text(
                  selectedOutlet.locationAddress.replaceToComma(),
                  style: AppTextStyle.value,
                ),
              ],
            ))
          ],
        )
      ],
    );
  }

  Widget successLinkedTelegramSection() {
    return Obx(
      () => Card(
        elevation: 10,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(
                    flex: 1,
                    child: Icon(
                      Icons.telegram,
                      color: Colors.blue,
                      size: 60.r,
                    )),
                SizedBox(width: 16.r),
                Expanded(
                    flex: 4,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Text(
                          chatGroupName.isNotEmpty ? chatGroupName : "N/A",
                          style: AppTextStyle.header2,
                        ),
                        Text(
                          "${"GroupID".tr}: ${chatId.isNotEmpty ? chatId : "N/A"}",
                          style: AppTextStyle.label2,
                        ),
                      ],
                    )),
                SizedBox(width: 10.r),
                Expanded(
                    flex: 1,
                    child: IconButton(
                      onPressed: () => showMoreOptions(),
                      icon: Icon(
                        Icons.more_horiz,
                        size: 30.r,
                      ),
                      color: AppColors.grey,
                    )),
              ]),
        ),
      ),
    );
  }

  Widget linkTelegramSection() {
    final double he = 65.r;

    return Card(
      elevation: 10,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Expanded(
                flex: 3,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "TelegramGroup".tr,
                      style: AppTextStyle.header2,
                    ),
                    Text(
                      "TelegramGroupMessage".tr,
                      style: AppTextStyle.label2,
                    ),
                  ],
                )),
            const SizedBox(
              width: 5,
            ),
            Container(
              width: 1,
              color: AppColors.grey,
              height: he,
            ),
            const SizedBox(
              width: 5,
            ),
            Expanded(
                flex: 1,
                child: InkWell(
                  onTap: () {
                    showFourDigitsDialog();
                  },
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      const Icon(
                        Icons.telegram,
                        color: Colors.blue,
                        size: 50,
                      ),
                      Text(
                        "LinkNow".tr,
                        style: AppTextStyle.caption,
                      )
                    ],
                  ),
                )),
          ],
        ),
      ),
    );
  }

  void showMoreOptions() {
    showCupertinoModalBottomSheet(
        context: Get.context!,
        builder: (_) {
          return BottomSheetBusinessDetail(
              child: SafeArea(
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Text(
                    "MoreAction".tr,
                    textAlign: TextAlign.center,
                    style: AppTextStyle.header1,
                  ),
                  SizedBox(
                    height: 16.r,
                  ),
                  FloatingActionButton(
                    heroTag: "ChangeTelegramGroup",
                    elevation: 0,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    backgroundColor: AppColors.white,
                    onPressed: () => handleChangeTelegramGroup(),
                    child: Row(children: [
                      Icon(
                        MerchantIcon.edit,
                        size: 18.r,
                        color: AppColors.black,
                      ),
                      SizedBox(
                        width: 16.r,
                      ),
                      Text(
                        "ChangeTelegramGroup".tr,
                        style: AppTextStyle.header2,
                      )
                    ]),
                  ),
                  Container(
                    height: 1.5,
                    color: AppColors.greyBackground,
                  ),
                  FloatingActionButton(
                    heroTag: "UnlinkedTelegramGroup",
                    elevation: 0,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    backgroundColor: AppColors.white,
                    onPressed: () => handleUnlinkedTelegramGroup(),
                    child: Row(children: [
                      Icon(
                        MerchantIcon.link,
                        size: 18.r,
                        color: AppColors.black,
                      ),
                      SizedBox(
                        width: 16.r,
                      ),
                      Text(
                        "UnlinkedTelegramGroup".tr,
                        style: AppTextStyle.header2,
                      )
                    ]),
                  ),
                ],
              ),
            ),
          ));
        });
  }

  void showFourDigitsDialog() {
    generateFourDigits();

    showCupertinoModalBottomSheet(
        isDismissible: false,
        topRadius: const Radius.circular(23),
        backgroundColor: AppColors.barrierBackground,
        context: Get.context!,
        builder: (_) {
          return BottomSheetBusinessDetail(
              child: SafeArea(
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Text(
                    "LinkTelegramGroup".tr,
                    style: AppTextStyle.header1,
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(
                    height: 16.r,
                  ),
                  Text(
                    fourDigits,
                    textAlign: TextAlign.center,
                    style: AppTextStyle.headline7,
                  ),
                  SizedBox(
                    height: 16.r,
                  ),
                  Container(
                    padding: EdgeInsets.all(10.r),
                    decoration: BoxDecoration(
                        border: Border.all(
                            color: AppColors.greyBackground, width: 2),
                        borderRadius: BorderRadius.circular(10)),
                    child: Text(
                      "TelegramInstructionMessage".tr,
                      textAlign: TextAlign.center,
                      style: AppTextStyle.caption,
                    ),
                  ),
                  SizedBox(
                    height: 16.r,
                  ),
                  FloatingActionButton.extended(
                      backgroundColor: AppColors.primary,
                      onPressed: () {
                        linkedTelegraGroup();
                      },
                      label: Text(
                        "OpenTelegram".tr,
                        style: AppTextStyle.button,
                      )),
                ],
              ),
            ),
          ));
        });
  }

  //#endregion
}
