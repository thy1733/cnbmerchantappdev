import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';

class BusinessListScreenController extends AppController {
  final IAppBusinessCacheService _appBusinessCacheService =
      AppBusinessCacheService();

  //#region All Override method
  @override
  void onReady() {
    if (AppUserTempInstant.appCacheUser.mCID.isNotEmpty) {
      retrieveListBusinesses();
    }

    super.onReady();
  }

  //#endregion

  //#region All Properties
  final BusinessService businessService = BusinessService();

  final _businesses = <ManageBusinessItemListOutputModel>[].obs;
  List<ManageBusinessItemListOutputModel> get businesses => _businesses;
  set businesses(List<ManageBusinessItemListOutputModel> value) =>
      _businesses.value = value;

  final _isEmptyData = false.obs;
  bool get isEmptyData => _isEmptyData.value;
  set isEmptyData(bool value) => _isEmptyData.value = value;

  //#endregion

  //#region All Method Helpers

  Future<void> retrieveListBusinesses() async {
    try {
      var input = ManageBusinessListInputModel();
      input.mCID = AppUserTempInstant.appCacheUser.mCID;

      showLoading();
      var result = await businessService.getListBusiness(input);
      if (result.success && result.result != null) {
        businesses.clear();
        businesses = result.result!.items.reversed.toList();
        var business = ManageBusinessListOutputModel();
        business.items = businesses;

        var activeBusinessList = ManageBusinessListOutputModel();
        activeBusinessList.items = business.items
            .where((e) =>
                e.status.toUpperCase() ==
                BusinessStatus.Active.name.toUpperCase())
            .toList();
        var ifSelectedBizExist = activeBusinessList.items.firstWhereOrNull(
            (e) => e.id == AppBusinessTempInstant.selectedBusiness.id);
        if (ifSelectedBizExist == null) {
          if (activeBusinessList.items.isNotEmpty) {
            await _appBusinessCacheService
                .saveSelectedBusiness(activeBusinessList.items.first);
          }
        }
        await _appBusinessCacheService.saveBusiness(activeBusinessList);
      } else {
        Fluttertoast.showToast(
            msg: result.error?.message ??
                "SomethingWentWrongPleaseTryAgainLater".tr);
      }
      isEmptyData = businesses.isEmpty;
      dismiss();
    } finally {}
  }

  //#endregion

  //#region All Command

  Future<void> toSetupBusinessAsync() async {
    try {
      var result =
          await navigationToNamedArgumentsAsync(Routes.setupBusinessProfile);
      if (result.isNotEmpty) {
        showSuccessDialog(msg: "CreatedBusinessSuccessfully".tr);
        Future.delayed(const Duration(seconds: 1));
        retrieveListBusinesses();
      }
    } catch (e) {
      debugPrint("Error: ${e.toString()}");
    } finally {
      dismiss();
    }
  }

  Future<void> toBusinessDetailAsync(
      ManageBusinessItemListOutputModel itemDetail) async {
    var arguments = <String, dynamic>{ArgumentKey.item: itemDetail};
    await navigationToNamedArgumentsAsync(Routes.businessDetailScreen,
        args: arguments);
  }

  //#endregion
}
