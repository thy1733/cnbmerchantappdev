import 'package:cnbmerchantapp/core.dart';
import 'package:cnbmerchantapp/helpers/components/snackBars/c_snack_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

class BusinessDetailScreenController extends AppController {
  final IAppFileUploadService _fileUploadService = AppFileUploadService();
  final BusinessService _businessService = BusinessService();
  final tabHomeCon = Get.find<TabHomeScreenController>();

  //#region All Override method

  @override
  void onInit() {
    getBusinessDetailFromArgument();

    super.onInit();
  }

  @override
  void onReady() {
    retrieveBusinessDetail();

    super.onReady();
  }
  //#endregion

  //#region All Properties

  /* This field is used for receiving the data from arguement */
  final _businessDetail = ManageBusinessItemListOutputModel().obs;
  ManageBusinessItemListOutputModel get businessDetail => _businessDetail.value;
  set businessDetail(ManageBusinessItemListOutputModel value) =>
      _businessDetail.value = value;

  /* This field is used for receiving the data from api */
  final _detailBusiness = DetailBusinessOutputModel().obs;
  DetailBusinessOutputModel get detailBusiness => _detailBusiness.value;
  set detailBusiness(DetailBusinessOutputModel value) =>
      _detailBusiness.value = value;

  final _businessTypes = BusinessTypeListOutputModel().obs;
  BusinessTypeListOutputModel get businessTypes => _businessTypes.value;
  set businessTypes(BusinessTypeListOutputModel value) =>
      _businessTypes.value = value;

  final _selectedBusinessTypeId = 0.obs;
  int get selectedBusinessTypeId => _selectedBusinessTypeId.value;
  set selectedBusinessTypeId(int value) =>
      _selectedBusinessTypeId.value = value;

  final _selectedBusinessTypeForDisplay = "".obs;
  String get selectedBusinessTypeForDisplay =>
      _selectedBusinessTypeForDisplay.value;
  set selectedBusinessTypeForDisplay(String value) =>
      _selectedBusinessTypeForDisplay.value = value;

  final _isActivateBusiness = true.obs;
  bool get isActivateBusiness => _isActivateBusiness.value;
  set isActivateBusiness(bool value) => _isActivateBusiness.value = value;

  final _selectedBusinessType = BusinessTypeItemListOutputModel().obs;
  BusinessTypeItemListOutputModel get selectedBusinessType =>
      _selectedBusinessType.value;
  set selectedBusinessType(BusinessTypeItemListOutputModel value) =>
      _selectedBusinessType.value = value;

  //#endregion

  //#region All Method Helpers

  Future<void> getBusinessDetailFromArgument() async {
    if (Get.arguments != null) {
      var result =
          Get.arguments[ArgumentKey.item] as ManageBusinessItemListOutputModel;
      businessDetail = result;
    }
  }

  Future<void> retrieveBusinessDetail() async {
    try {
      var input = DetailBusinessInputModel();
      input.businessId = businessDetail.id;

      showLoading();
      var response = await _businessService.getDetailBusiness(input);
      if (response.success && response.result != null) {
        detailBusiness = response.result!;

        setData();
        retrieveBusinessTypeList();

        dismiss();
      } else {
        dismiss();

        Fluttertoast.showToast(
            msg: response.error?.message ??
                "Error: Cannot retrieve business detail");
      }
    } finally {}
  }

  Future<void> retrieveBusinessTypeList() async {
    var input = BusinesTypeListInputModel();

    var response = await _businessService.getListBusinessTypeAsync(input);

    if (response.success && response.result != null) {
      businessTypes = response.result!;

      var localBusinessId = detailBusiness.businessType.id;
      selectedBusinessType = businessTypes.items
          .singleWhere((element) => element.id == localBusinessId);

      selectedBusinessTypeId = (int.tryParse(selectedBusinessType.id) ?? 1) - 1;
    } else {
      Fluttertoast.showToast(
          msg: response.error?.message ?? "ErrorGettingBusinessTypes".tr);
    }

    setSelectedBusinessType();
  }

  void setSelectedBusinessType() {
    selectedBusinessTypeForDisplay =
        businessTypes.items[selectedBusinessTypeId].name;
    selectedBusinessType = businessTypes.items[selectedBusinessTypeId];
  }

  Future<void> deativateBusiness() async {
    try {
      var input = DeactivateBusinessInputModel();
      input.businessId = detailBusiness.businessId;

      showLoading();
      var response = await _businessService.deactivateBusinessAsync(input);

      if (response.success) {
        Get.back();
        tabHomeCon.isNeedReloadBussiness = true;
        CSnackBar.showSnackBarTop(message: "DeactivatedBusinessSuccess".tr);

        retrieveBusinessDetail();
      } else {
        Fluttertoast.showToast(
            msg: response.error?.message ??
                "Error: Cannot deactivate business!");
      }
    } finally {
      dismiss();
    }
  }

  Future<void> activateBusiness() async {
    try {
      var input = ActivateBusinessInputModel();
      input.businessId = detailBusiness.businessId;

      showLoading();
      var response = await _businessService.activateBusinessAsync(input);

      if (response.success) {
        Get.back();
        tabHomeCon.isNeedReloadBussiness = true;
        CSnackBar.showSnackBarTop(message: "ActivatedBusinessSuccess".tr);

        retrieveBusinessDetail();
      } else {
        Fluttertoast.showToast(
            msg: response.error?.message ?? "Error: Cannot activate business!");
      }
    } finally {
      dismiss();
    }
  }

  Future<void> updateBusiness() async {
    try {
      var input = UpdateBusinessInputModel();
      input.businessId = detailBusiness.businessId;
      input.businessLogo = businessDetail.businessLogoId.isEmpty
          ? DateTime.now().toFormatDateString("yyyymmddhhmmsss")
          : businessDetail.businessLogoId;
      input.businessTypeId = selectedBusinessType.id;

      showLoading();
      var response = await _businessService.updateBusinessAsync(input);

      if (response.success) {
        // Update busines profile
        if (photoFile.isNotEmpty) {
          var inputFile = UploadFileInputDto();
          inputFile.uploadType = UploadProfileType.businessProfile;
          inputFile.fileName = input.businessLogo;
          inputFile.id = businessDetail.id;
          inputFile.filePath = fileUpload.filePath;

          await _fileUploadService.uploadFileAsync(input: inputFile);
          await clearCacheImage(detailBusiness.businessLogo,
              cacheKey: detailBusiness.cacheImageKey);
        }

        Get.back();
        CSnackBar.showSnackBarTop(message: "UpdatedBusinessSuccess".tr);

        retrieveBusinessDetail();

        dismiss();
      } else {
        dismiss();

        Fluttertoast.showToast(
            msg: response.result?.description ??
                "Error: Cannot update business!");
      }
    } finally {}
  }

  Future<void> uploadBusinessProfile() async {
    await super.pickImagePushAsync().then((value) async {
      if (value != null) {
        photoFile = await value.readAsBytes();
        var fileName = businessDetail.businessLogoId.isEmpty
            ? DateTime.now().toFormatDateString("yyyymmddhhmmsss")
            : businessDetail.businessLogoId;
        fileUpload.fileName = fileName;
        fileUpload.filePath = value.path;
        updateBusiness();
      }
    });
  }

  //#endregion

  //#region All Command

  void setData() {
    isActivateBusiness =
        BusinessStatus.Active.setValue(detailBusiness.status) ==
            BusinessStatus.Active;
  }

  void refreshDetail() async {
    Get.back();
    await retrieveBusinessDetail();
    onShowDetailBusiness();
  }

  void onShowDetailBusiness() {
    final double wid = Get.width;
    final double size = wid / 4;

    const double padding = 16;

    Widget businessStatusSection() {
      Widget child = Container();

      CorporateStatus status =
          CorporateStatus.None.setValue(detailBusiness.corporateStatus);

      switch (status) {
        case CorporateStatus.None:
          // Temporary disable this feature (pharse II)
          // child = InkWell(
          //   onTap: () {
          //     navigationToNamedArgumentsAsync(Routes.upgradeBusiness,
          //             args: detailBusiness)
          //         .then((value) {
          //       debugPrint("ResultOfUpgradeBusiness: $value");
          //       if (value.isNotEmpty) {
          //         refreshDetail();
          //       }
          //     });
          //   },
          //   child: KStatusWidget(
          //     name: "UpgradeNow".tr,
          //     color: AppColors.primaryDark,
          //   ),
          // );

          break;
        case CorporateStatus.Requested:
          child = KStatusWidget(name: "Requested".tr, color: AppColors.grey);
          break;
        case CorporateStatus.Pending:
          child = KStatusWidget(
            name: "Pending".tr,
            color: AppColors.primary,
          );

          break;
        case CorporateStatus.Corporate:
          child = KStatusWidget(
            name: "Corporate".tr,
            color: AppColors.darkYellow,
          );

          break;
      }
      return child;
    }

    Widget businessTypeSection() => Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 5),
              child: Icon(
                MerchantIcon.businessType,
                color: AppColors.black,
                size: AppFontSize.body,
              ),
            ),
            const SizedBox(
              width: 16,
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "BusinessCategory".tr,
                    style: AppTextStyle.label2,
                  ),
                  const SizedBox(
                    height: 5,
                  ),
                  Text(
                    selectedBusinessTypeForDisplay.tr,
                    style: AppTextStyle.value,
                  ),
                ],
              ),
            ),
            IconButton(
              onPressed: () {
                onRequestChangeBusinessType();
              },
              icon: Icon(
                MerchantIcon.edit,
                color: AppColors.black,
                size: AppFontSize.body,
              ),
            ),
          ],
        );

    Widget merchantIDSection = Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 5),
          child: Icon(
            MerchantIcon.bank,
            color: AppColors.black,
            size: AppFontSize.body,
          ),
        ),
        const SizedBox(
          width: 16,
        ),
        Expanded(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "MerchantID".tr,
                style: AppTextStyle.label2,
              ),
              const SizedBox(
                height: 5,
              ),
              Text(
                detailBusiness.businessId.split(".")[0].maskAccountNumber(),
                style: AppTextStyle.value,
              ),
            ],
          ),
        )
      ],
    );

    Widget linkedAccountNameSection = Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 5),
          child: Icon(
            MerchantIcon.user,
            color: AppColors.black,
            size: AppFontSize.body,
          ),
        ),
        const SizedBox(
          width: 16,
        ),
        Expanded(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "LinkedAccountName".tr,
                style: AppTextStyle.label2,
              ),
              const SizedBox(
                height: 5,
              ),
              Text(
                detailBusiness.accountHolderFullName,
                style: AppTextStyle.value,
              ),
            ],
          ),
        )
      ],
    );

    Widget linkedAccountNumberSection = Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 5),
          child: Icon(
            MerchantIcon.bank,
            color: AppColors.black,
            size: AppFontSize.body,
          ),
        ),
        const SizedBox(
          width: 16,
        ),
        Expanded(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "LinkedAccountNumber".tr,
                style: AppTextStyle.label2,
              ),
              const SizedBox(
                height: 5,
              ),
              Text(
                detailBusiness.accountNumber.maskAccountNumber(),
                style: AppTextStyle.value,
              ),
            ],
          ),
        ),
      ],
    );

    Widget totalSection = Container(
      height: size / 1.2,
      padding: const EdgeInsets.only(left: 15, top: 5, right: 15, bottom: 5),
      decoration: BoxDecoration(
        color: AppColors.grey.withOpacity(.1),
        borderRadius: const BorderRadius.all(Radius.circular(15)),
      ),
      child: Row(
        children: [
          Row(
            children: [
              SvgPicture.asset(SvgAppAssets.shop),
              const SizedBox(
                width: 10,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "TotalSalePoint".tr,
                    textAlign: TextAlign.end,
                    style: AppTextStyle.value,
                  ),
                  Text(
                    detailBusiness.totalOutlet,
                    textAlign: TextAlign.start,
                    style: AppTextStyle.header1,
                  )
                ],
              )
            ],
          ),
          Expanded(child: Container()),
          Row(
            children: [
              SvgPicture.asset(SvgAppAssets.cashier),
              const SizedBox(
                width: 10,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "TotalCashier".tr,
                    textAlign: TextAlign.end,
                    style: AppTextStyle.value,
                  ),
                  Text(
                    detailBusiness.totalCashier,
                    textAlign: TextAlign.start,
                    style: AppTextStyle.header1,
                  )
                ],
              )
            ],
          ),
        ],
      ),
    );

    Widget deactivateBusiness = Obx(
      () => TextButton(
        style: AppButtonStyle.kSecondaryButtonExpandStyle(),
        onPressed: () {
          if (isActivateBusiness) {
            CChoiceDialog.okCancel(
                title: "DeactiveBusiness".tr,
                okTitle: "Deactivate".tr,
                message: "ConfirmDeactivateBusinessMsg".tr,
                onOk: () {
                  Get.back();
                  deativateBusiness();
                });
          } else {
            CChoiceDialog.okCancel(
                title: "ActivateBusiness".tr,
                okTitle: "Activate".tr,
                message: "ConfirmActivateBusinessMsg".tr,
                onOk: () {
                  Get.back();
                  activateBusiness();
                });
          }
        },
        child: Text(
          isActivateBusiness ? "DeactiveBusiness".tr : "ActivateBusiness".tr,
          style: AppTextStyle.primary1,
        ),
      ),
    );

    showCupertinoModalBottomSheet(
        backgroundColor: AppColors.barrierBackground,
        topRadius: const Radius.circular(23),
        context: Get.context!,
        builder: (context) {
          return Obx(
            () => BottomSheetBusinessDetail(
              child: Column(
                children: [
                  AbsorbPointer(
                    absorbing: !isActivateBusiness,
                    child: Padding(
                      padding: const EdgeInsets.all(padding),
                      child: Column(
                        children: [
                          CircleImageUploadView(
                              placeHolder: SvgAppAssets.icDefaultShop,
                              imageFile: photoFile,
                              imageUrl: detailBusiness.businessLogo,
                              cacheKey: detailBusiness.cacheImageKey,
                              onImagePicked: () {
                                uploadBusinessProfile();
                              }),
                          const SizedBox(
                            height: padding,
                          ),
                          Visibility(
                            visible: !isActivateBusiness,
                            child: Container(
                              padding: const EdgeInsets.fromLTRB(5, 3, 5, 3),
                              decoration: BoxDecoration(
                                  color: AppColors.greyishDisable,
                                  borderRadius: BorderRadius.circular(5)),
                              child: Text(
                                "Business is Inactive!",
                                style: AppTextStyle.caption,
                              ),
                            ),
                          ),
                          Text(
                            businessDetail.businessName,
                            style: AppTextStyle.header2,
                          ),
                          const SizedBox(
                            height: padding,
                          ),
                          businessStatusSection(),
                          const SizedBox(
                            height: padding,
                          ),
                          businessTypeSection(),
                          const SizedBox(
                            height: padding,
                          ),
                          merchantIDSection,
                          const SizedBox(
                            height: padding,
                          ),
                          linkedAccountNameSection,
                          const SizedBox(
                            height: padding,
                          ),
                          linkedAccountNumberSection,
                          const SizedBox(
                            height: padding,
                          ),
                          totalSection,
                        ],
                      ),
                    ),
                  ),
                  Container(
                    width: Get.width,
                    height: 1,
                    color: AppColors.listSeparator,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                        left: padding, right: padding, bottom: padding * 2),
                    child: deactivateBusiness,
                  ),
                ],
              ),
            ),
          );
        });
  }

  void onRequestChangeBusinessType() {
    showCupertinoModalBottomSheet(
        context: Get.context!,
        topRadius: const Radius.circular(23),
        barrierColor: AppColors.barrierBackground,
        builder: (_) {
          final bottomSheetSelectionOption = BottomSheetSelectOption(
            title: "BusinessType".tr,
            items: businessTypes.items
                .map((e) => OptionItemModel.create(e.name, []))
                .toList(),
            currentIndex: 0,
            isDatePicker: false,
            onSelectedItem: (i, date) {
              selectedBusinessTypeId = i;
              selectedBusinessType =
                  businessTypes.items[selectedBusinessTypeId];

              updateBusiness();
            },
          );

          bottomSheetSelectionOption.selectedItemIndex = selectedBusinessTypeId;

          return bottomSheetSelectionOption;
        });
  }
  //#endregion
}
