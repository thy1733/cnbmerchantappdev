import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:image_cropper/image_cropper.dart';

class UpgradeScreenController extends AppController {
  //#region All Override Methods
  final IAppFileUploadService _fileUploadService = AppFileUploadService();

  @override
  void onInit() {
    getDataFromArguement();

    super.onInit();
  }

  //#endregion

  //#region All Properties

  final BusinessService businessService = BusinessService();

  final _businessInfo = DetailBusinessOutputModel().obs;
  DetailBusinessOutputModel get businessInfo => _businessInfo.value;
  set businessInfo(DetailBusinessOutputModel value) =>
      _businessInfo.value = value;

  final _documents = List<CroppedFile>.empty().obs;
  List<CroppedFile> get documents => _documents;
  set documents(List<CroppedFile> value) => _documents.value = value;

  final _documentFile = CroppedFile("").obs;
  CroppedFile get documentFile => _documentFile.value;
  set documentFile(CroppedFile value) => _documentFile.value = value;

  final _isUploadDocsReachLimit = true.obs;
  bool get isUploadDocsReachLimit => _isUploadDocsReachLimit.value;
  set isUploadDocsReachLimit(bool value) =>
      _isUploadDocsReachLimit.value = value;

  final _note = "".obs;
  String get note => _note.value;
  set note(String value) => _note.value = value;

  final int numberOfFileUploaded = 4;

  //#endregion

  //#region All Methods Helpers

  void getDataFromArguement() {
    try {
      businessInfo = Get.arguments as DetailBusinessOutputModel;
    } catch (e) {
      debugPrint("Error: ${e.toString()}");
    }
  }

  Future<void> chooseDocument() async {
    CroppedFile? data = await super.pickImagePushAsync();
    if (data != null) {
      documentFile = data;
      addDocument();
    }
  }

  void reachingLimit() {
    isUploadDocsReachLimit = !(documents.length == numberOfFileUploaded);
  }

  void addDocument() async {
    // var files = await documentFile.readAsBytes();
    documents.add(documentFile);
    reachingLimit();
  }

  void removeDocument(int index) {
    if (documents.isNotEmpty) documents.removeAt(index);
    reachingLimit();
  }

  void onNoteChanged(String value) {
    note = value;
  }

  bool onFieldsValidation() {
    return documents.isNotEmpty;
  }

  //#endregion

  //#region All Commands Methods

  Future<void> onUpgradeBusiness() async {
    if (!onFieldsValidation()) {
      Fluttertoast.showToast(msg: "SupportDocsDescription".tr);
      return;
    }

    // upload document
    List<String> docsUrl = [];
    for (var e in documents) {
      var input = UploadFileInputDto();
      input.fileName = DateTime.now().toFormatDateString("yyyymmddhhmmsss");
      input.filePath = e.path;
      input.id = businessInfo.businessId;
      input.uploadType = UploadProfileType.attachment;
      var result = await _fileUploadService.uploadFileAsync(input: input);
      if (result.fileName.isNotEmpty) {
        docsUrl.add(result.fileName.downloadUrl(
          UploadProfileType.attachment,
          businessInfo.businessId,
          result.fileName,
        ));
      }
    }

    var input = UpgradeBusinessInputModel();
    input.businessId = businessInfo.businessId;
    input.patentPhotos = docsUrl.join("!");
    input.note = note;

    showLoading();
    var response = await businessService.upgradeBusinessAsync(input);
    dismiss();

    if (response.success) {
      KAlertScreen.review(
          onPrimaryButtonClick: () {
            Get.back();
            Get.back(result: "${response.result?.corporateStatus.index ?? ''}");
          },
          primaryButtonTitle: "Done".tr,
          message: "UnderReview".tr,
          description: "UnderReviewLabel".tr,
          icon: ClipRRect(
              borderRadius: BorderRadius.circular(50.r),
              child: Container(
                width: 100.r,
                height: 100.r,
                color: AppColors.white,
                child: Icon(
                  MerchantIcon.review,
                  size: 32.r,
                  color: AppColors.primary,
                ),
              )));
    } else {
      Fluttertoast.showToast(
          msg: response.error?.message ?? "Error: Failed to upgrade business.");
    }
  }

  //#endregion
}
