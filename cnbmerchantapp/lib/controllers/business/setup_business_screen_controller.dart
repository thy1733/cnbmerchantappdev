import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/cupertino.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

class SetupBusinessScreenController extends AppController {
  final IAppFileUploadService _fileUploadService = AppFileUploadService();
  final IAuthenticateService _authenticateService = AuthenticateService();
  final IBusinessService _setupService = BusinessService();

  TextEditingController addressInputConroller = TextEditingController();
  TextEditingController locationNameConroller = TextEditingController();

  //#region All Override method

  @override
  void onInit() {
    getArgument();

    super.onInit();
  }

  @override
  void onReady() async {
    //todo: It is called immediately after the widget is rendered on screen.

    if (!(fromPage == Routes.activateOwnerScreen || fromPage == Routes.login)) {
      await getAccountInfoListAsync();
    }
    await getBusinessTypeListAsync();
    dismiss();
    super.onReady();
  }

  @override
  void onClose() async {
    //todo: It is called just before the controller is deleted from memory.

    businessNameFocus.dispose();
    super.onClose();
  }

  //#endregion

  //#region All Properties

  final _businessName = "".obs;
  String get businessName => _businessName.value;
  set businessName(String value) => _businessName.value = value;

  final _businessLocationAddress = "".obs;
  String get businessLocationAddress => _businessLocationAddress.value;
  set businessLocationAddress(String value) =>
      _businessLocationAddress.value = value;

  final _businessNameValidateMsg = "".obs;
  String get businessNameValidateMsg => _businessNameValidateMsg.value;
  set businessNameValidateMsg(String value) =>
      _businessNameValidateMsg.value = value;

  final _businessLocationNameValidateMsg = "".obs;
  String get businessLocationNameValidateMsg =>
      _businessLocationNameValidateMsg.value;
  set businessLocationNameValidateMsg(String value) =>
      _businessLocationNameValidateMsg.value = value;

  final _businessLocationAddressValidateMsg = "".obs;
  String get businessLocationAddressValidateMsg =>
      _businessLocationAddressValidateMsg.value;
  set businessLocationAddressValidateMsg(String value) =>
      _businessLocationAddressValidateMsg.value = value;

  final _businessNameFocus = FocusNode().obs;
  FocusNode get businessNameFocus => _businessNameFocus.value;
  set businessNameFocus(FocusNode value) => _businessNameFocus.value = value;

  final _fromPage = ''.obs;
  String get fromPage => _fromPage.value;
  set fromPage(String value) => _fromPage.value = value;

  final _accountNumber = ''.obs;
  String get accountNumber => _accountNumber.value;
  set accountNumber(String value) => _accountNumber.value = value;

  final _username = ''.obs;
  String get username => _username.value;
  set username(String value) => _username.value = value;

  final _selectedBusinessTypeId = 0.obs;
  int get selectedBusinessTypeId => _selectedBusinessTypeId.value;
  set selectedBusinessTypeId(int value) =>
      _selectedBusinessTypeId.value = value;

  final _accounts = GetAccountInfoListOutputModel().obs;
  GetAccountInfoListOutputModel get accounts => _accounts.value;
  set accounts(GetAccountInfoListOutputModel value) => _accounts.value = value;

  final _businessTypes = BusinessTypeListOutputModel().obs;
  BusinessTypeListOutputModel get businessTypes => _businessTypes.value;
  set businessTypes(BusinessTypeListOutputModel value) =>
      _businessTypes.value = value;

  final _selectedAccountId = 0.obs;
  int get selectedAccountId => _selectedAccountId.value;
  set selectedAccountId(int value) => _selectedAccountId.value = value;

  final _locationModel = LocationAddressModel().obs;
  LocationAddressModel get locationModel => _locationModel.value;
  set locationModel(LocationAddressModel value) => _locationModel.value = value;

  final _staffRef = "".obs;
  String get staffRef => _staffRef.value;
  set staffRef(String value) => _staffRef.value = value;

  //#endregion

  //#region All Method Helpers

  void getArgument() {
    if (Get.arguments != null && Get.arguments[ArgumentKey.fromPage] != null) {
      fromPage = Get.arguments[ArgumentKey.fromPage] as String;
      final userAccount =
          Get.arguments[ArgumentKey.item] as RegisterAccountInputModel?;
      if (userAccount != null) {
        accountNumber = userAccount.accountNumber;
        username = userAccount.userName;
      }
    }
  }

  Future<void> createBusinessAsync() async {
    FocusManager.instance.primaryFocus?.unfocus();

    if (onFormsValidation()) return;

    try {
      showLoading();

      var input = SetupBusinessInputModel();
      input.accountNumber = accountNumber;
      input.businessName = businessName;
      input.businessLogo = fileUpload.fileName;
      input.businessTypeId = businessTypes.items[selectedBusinessTypeId].id;
      input.locationName = locationNameConroller.text.replaceSemiColonToComma();
      input.locationAddress = businessLocationAddress.replaceSemiColonToComma();
      input.latitude = locationModel.latitude.toString();
      input.longitude = locationModel.longitude.toString();
      input.staffRefId = staffRef;
      input.username = AppUserTempInstant.appCacheUser.userName;

      var response = await _setupService.createBusinessAsync(input);
      if (response.success) {
        // upload for business profile
        var inputFile = UploadFileInputDto();
        inputFile.uploadType = UploadProfileType.businessProfile;
        inputFile.fileName = response.result!.businessLogo;
        inputFile.id = response.result!.id;
        inputFile.filePath = fileUpload.filePath;
        if (fileUpload.filePath.isNotEmpty) {
          await _fileUploadService.uploadFileAsync(input: inputFile);
        }

        // upload for outlet profile
        var inputFileOutlet = UploadFileInputDto();
        inputFileOutlet.uploadType = UploadProfileType.outletProfile;
        inputFileOutlet.fileName = response.result!.businessLogo;
        inputFileOutlet.id = response.result!.id;
        inputFileOutlet.filePath = fileUpload.filePath;
        if (fileUpload.filePath.isNotEmpty) {
          await _fileUploadService.uploadFileAsync(input: inputFileOutlet);
        }

        // activateOwnerScreen: Register
        if (fromPage == Routes.activateOwnerScreen ||
            fromPage == Routes.login) {
          KAlertScreen.success(
              message: "Congratulation".tr,
              description: "YourBusinessAreReadyToReceivePayment".tr,
              primaryButtonTitle: "Done".tr,
              onPrimaryButtonClick: () async {
                await AppOnboardingScreenService().updateUserLoginAsync(true);
                navigationToOffAllNamedAsync(Routes.main);
              });
          return;
        }

        dismiss();
        Get.back(result: "${response.result}");
      } else {
        dismiss();

        Fluttertoast.showToast(
            msg: response.result != null
                ? response.result!.description
                : "Error: Failed to create business.");
      }
    } catch (e) {
      dismiss();
      debugPrint("Error: ${e.toString()}");
    }
  }

  Future<void> getAccountInfoListAsync() async {
    try {
      var input = GetAccountInfoInputModel();
      final mCID = AppUserTempInstant.appCacheUser.mCID;
      input.cidOrAccountNumber = mCID.isNotEmpty
          ? mCID
          : AppUserTempInstant.appCacheUser.accountNumber;

      showLoading();
      var response = await _authenticateService.getAccountInfoListAsync(input);

      if (response.result != null && response.success) {
        dismiss();

        accounts = response.result!;
        accountNumber = accounts.items.first.accountNumber;
      } else {
        dismiss();

        Fluttertoast.showToast(
            msg: response.result?.description ??
                "Error: Cannot get bank accounts.");
      }
    } finally {}
  }

  Future<void> getBusinessTypeListAsync() async {
    var input = BusinesTypeListInputModel();
    var result = await _setupService.getListBusinessTypeAsync(input);
    if (result.success) {
      businessTypes = result.result ?? BusinessTypeListOutputModel();
    } else {
      Fluttertoast.showToast(
          msg: result.error?.message ?? "Error: Cannot get business types.");
    }
  }

  void callDeleted() {
    var locationController = Get.find<LocationScreenController>();
    locationController.dismissKeyboard();
    Get.delete<LocationScreenController>();
  }

  void uploadBusinessProfile() async {
    try {
      await super.pickImagePushAsync().then((value) async {
        if (value != null) {
          //display image in local
          photoFile = await value.readAsBytes();

          //file name and upload to temporary file path.
          var fileName = DateTime.now().toFormatDateString("yyyymmddhhmmsss");
          fileUpload.fileName = fileName;
          fileUpload.filePath = value.path;
        }
      });
    } catch (e) {
      debugPrint("Error: ${e.toString()}");
    }
  }
  //#endregion

  //#region All Command

  bool onFormsValidation() {
    onBusinessNameValidate();

    onBusinessLocationValidate();

    return businessNameValidateMsg.isNotEmpty ||
        businessLocationNameValidateMsg.isNotEmpty ||
        businessLocationAddressValidateMsg.isNotEmpty;
  }

  void onBusinessNameChanged(String value) {
    businessName = value;
    onBusinessNameValidate();
  }

  void onBusinessNameValidate() {
    if (businessName.trim().isEmpty) {
      businessNameValidateMsg = "BusinessNameRequired".tr;
    } else if (businessName.length > ValueConst.businessNameLength) {
      businessNameValidateMsg = "LimitationCharacters".tr;
    } else if (AppTextInputRegx.isUnicode(businessName.tr)) {
      businessNameValidateMsg = "UnicodeNotAllow".tr;
    } else {
      businessNameValidateMsg = "";
    }
  }

  void onBusinessNameFocus() {
    businessNameValidateMsg.isNotEmpty
        ? businessNameFocus.requestFocus()
        : businessNameFocus.unfocus();
  }

  void onBusinessLocationAddressChanged(String value) {
    businessLocationAddress = value;
    onBusinessLocationValidate();
  }

  void onBusinessLocationValidate() {
    businessLocationNameValidateMsg = locationNameConroller.text.isEmpty
        ? "BusinessLocationRequired".tr
        : AppTextInputRegx.isUnicode(locationNameConroller.text)
            ? "UnicodeNotAllow".tr
            : "";
    businessLocationAddressValidateMsg = addressInputConroller.text.isEmpty
        ? "BusinessLocationAddressRequired".tr
        : AppTextInputRegx.isUnicode(addressInputConroller.text)
            ? "UnicodeNotAllow".tr
            : "";
  }

  Future onBusinessTypesSelected(String value) async {
    showCupertinoModalBottomSheet(
        topRadius: const Radius.circular(23),
        barrierColor: AppColors.barrierBackground,
        context: Get.context!,
        builder: (_) {
          final bottomSheetSelectionOption = BottomSheetSelectOption(
            title: "BusinessType".tr,
            items: businessTypes.items
                .map((element) => OptionItemModel.create(element.name, []))
                .toList(),
            currentIndex: 0,
            isDatePicker: false,
            onSelectedItem: (i, date) {
              selectedBusinessTypeId = i;
            },
          );

          bottomSheetSelectionOption.selectedItemIndex = selectedBusinessTypeId;

          return bottomSheetSelectionOption;
        });
  }

  void onSelectBankAcountAsync() async {
    AppBottomSheet.bottomSheetSelectDynamicOption(
      title: "SelectAccount".tr,
      items: accounts.items,
      separator: const SizedBox(height: 25),
      closeOnSelect: true,
      selectedItem: accounts.items[selectedAccountId],
      onSelectedItem: (index, value) {
        // set selectedBankAccountIndex for UI
        selectedAccountId = index;
        accountNumber = accounts.items[selectedAccountId].accountNumber;
      },
      itemBuilder: (index, value) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              value.accountName.tr,
              style: const TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.w400,
              ),
            ),
            Text(
              '${value.currency.toUpperCase()} | ${value.accountNumber}',
              style: const TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.w700,
              ),
            ),
          ],
        );
      },
    );
  }

  Future<void> onBrowseLocation() async {
    await showCupertinoModalPopup(
        context: Get.context!,
        builder: (context) => LocationScreen(
              initLocation: locationModel,
              locationCallBack: (value) {
                locationModel = value;

                locationNameConroller.text = locationModel.locationName;
                addressInputConroller.text = locationModel.address;
                businessLocationAddress = addressInputConroller.text;

                onBusinessLocationValidate();
              },
            )).then((value) => callDeleted());
  }

  void onStaffRefChanged(String value) {
    staffRef = value;
  }
  //#endregion
}
