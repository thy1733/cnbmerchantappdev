import 'dart:io';
import 'dart:typed_data';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_cropper/image_cropper.dart';

class AppController extends AppManageController {
  late final IAppLocalizeService _localizeService = AppLocalizeService();
  final DeviceSecurityChecker deviceSecurityChecker =
      DeviceSecurityChecker.shared;
  //#region override methods
  @override
  void onInit() async {
    await currentLanguagesAsync();
    super.onInit();
    await deviceSecurityChecker.checkSecuredDevice(Get.context!);
    isOnline = await checkInternetConnection();
  }
  //#endregion

  //#region All Properties

  final _isOnline = true.obs;
  bool get isOnline => _isOnline.value;
  set isOnline(bool value) => _isOnline.value = value;

  final _isLoading = false.obs;
  bool get isLoading => _isLoading.value;
  set isLoading(bool value) => _isLoading.value = value;

  final _isEnable = false.obs;
  bool get isEnable => _isEnable.value;
  set isEnable(bool value) => _isEnable.value = value;

  final _isNoData = false.obs;
  bool get isNoData => _isNoData.value;
  set isNoData(bool value) => _isNoData.value = value;

  final _photoFile = Uint8List(0).obs;
  Uint8List get photoFile => _photoFile.value;
  set photoFile(Uint8List value) => _photoFile.value = value;

  final _photoXFile = CroppedFile('').obs;
  CroppedFile get photoXFile => _photoXFile.value;
  set photoXFile(CroppedFile value) => _photoXFile.value = value;

  final _userProfile = UserProfileInfoModel().obs;
  UserProfileInfoModel get userProfile => _userProfile.value;
  set userProfile(UserProfileInfoModel value) => _userProfile.value = value;

  final _languageItem = <ItemSelectOptionModel>[
    ItemSelectOptionModel.create(0, LanguageOption.lanName0, false,
        data: "KH", imageSvg: SvgAppAssets.flagKhmer),
    ItemSelectOptionModel.create(1, LanguageOption.lanName1, false,
        data: "EN", imageSvg: SvgAppAssets.flagEnglish),
    ItemSelectOptionModel.create(2, LanguageOption.lanName2, false,
        data: "ZN", imageSvg: SvgAppAssets.flagChinese),
  ].obs;
  List<ItemSelectOptionModel> get languageItem => _languageItem;
  set languageItem(List<ItemSelectOptionModel> value) =>
      _languageItem.value = value;

  final _languageSelected = ItemSelectOptionModel().obs;
  ItemSelectOptionModel get languageSelected => _languageSelected.value;
  set languageSelected(ItemSelectOptionModel value) =>
      _languageSelected.value = value;

  final _fileUpload = FileUploadOutputModel().obs;
  FileUploadOutputModel get fileUpload => _fileUpload.value;
  set fileUpload(FileUploadOutputModel value) => _fileUpload.value = value;

  //#endregion

  //#region Method Helpers

  Future<void> clearCacheImage(String url, {String? cacheKey}) async {
    await CachedNetworkImage.evictFromCache(url, cacheKey: cacheKey);
  }

  String generateCacheKey =
      DateTime.now().toFormatDateString("yyyymmddhhmmsss");

  Future<void> currentLanguagesAsync() async {
    try {
      var result = await _localizeService.getAppLocalizeAsync();
      var languageCode = result.language == 0
          ? LanguageOption.lanKhmer
          : result.language == 1
              ? LanguageOption.lanEnglish
              : LanguageOption.lanChinese;
      var countryCode = result.language == 0
          ? LanguageOption.conCodeKh
          : result.language == 1
              ? LanguageOption.conCodeEn
              : LanguageOption.conCodeZh;
      var locale = Locale(languageCode, countryCode);
      Get.updateLocale(locale);

      for (var item in languageItem) {
        item.isSelected = item.index == result.language ? true : false;
      }
      languageSelected = languageItem[result.language];
    } catch (e) {
      //Handle all other exceptions
    }
  }

  Future<void> requestToConfirmPIN(
      {required String title,
      bool isPermanentRequired = false,
      String image = "",
      String? message,
      String? description,
      required Function(RequestCallback callback) onSuccess,
      VoidCallback? onBackPress,
      VoidCallback? reachManyAttempts}) async {
    await Get.to(ConfirmPinScreen(
      onSuccess: onSuccess,
      onBackPress: () {
        if (onBackPress != null) {
          onBackPress();
        }
      },
      title: title,
      message: message,
      description: description,
      reachManyAttemp: reachManyAttempts,
      isPermanentRequired: isPermanentRequired,
      image: image,
    ));
  }

  Future<void> updateLanguagesAsync() async {
    try {
      var selectedLang = languageSelected;

      await AppBottomSheet.bottomSheetSelectDynamicOption<
          ItemSelectOptionModel>(
        title: "SelectLanguage".tr,
        selectedItem: languageSelected,
        items: languageItem,
        onSelectedItem: (index, value) {
          languageSelected = value;
          Get.back();
        },
        selectedItemDecoration: BoxDecoration(
            color: AppColors.textFieldBackground,
            borderRadius: BorderRadius.circular(5)),
        itemBuilder: (index, value) => Row(
          children: [
            Container(
              width: 25,
              height: 25,
              margin: EdgeInsets.symmetric(
                  vertical: selectedLang == value ? 10 : 5),
              decoration: const BoxDecoration(shape: BoxShape.circle),
              clipBehavior: Clip.hardEdge,
              child: SvgPicture.asset(
                value.imageSvg,
                fit: BoxFit.cover,
              ),
            ),
            ContainerDevider.blankSpaceMd,
            Text(
              value.name,
              style: TextStyle(
                  height: 1.3,
                  fontFamily: value.data == 'KH'
                      ? GoogleFonts.notoSansKhmer().fontFamily
                      : GoogleFonts.roboto().fontFamily),
            ),
          ],
        ),
      );

      if (languageSelected != selectedLang) {
        var languageCode = languageSelected.index == 0
            ? LanguageOption.lanKhmer
            : languageSelected.index == 1
                ? LanguageOption.lanEnglish
                : LanguageOption.lanChinese;
        var countryCode = languageSelected.index == 0
            ? LanguageOption.conCodeKh
            : languageSelected.index == 1
                ? LanguageOption.conCodeEn
                : LanguageOption.conCodeZh;
        var locale = Locale(languageCode, countryCode);
        Get.updateLocale(locale);
        var input = AppLocalizeModel.create(
            isActivte: true, language: languageSelected.index);
        await _localizeService.updateLocalizeAsync(input);
      }
    } on Exception {
      //Handle exception of type SomeException
    } catch (e) {
      //Handle all other exceptions
    }
  }

  Future<bool> checkInternetConnection() async {
    var isOnline = false;
    try {
      final result = await InternetAddress.lookup('google.com');
      isOnline = result.isNotEmpty && result[0].rawAddress.isNotEmpty;
    } on SocketException catch (_) {
      isOnline = false;
    }
    return isOnline;
  }

  void showSuccessDialog({required String msg}) async {
    showCupertinoDialog(
        context: Get.context!,
        builder: (BuildContext context) {
          return CupertinoAlertDialog(
            actions: [
              TextButton(
                onPressed: () {
                  Get.back();
                },
                child: Text("Congratulation".tr, style: AppTextStyle.primary2),
              )
            ],
            title: Text("SuccessTitle".tr),
            content: Text(
              msg,
              style: AppTextStyle.value,
            ),
          );
        });
  }

  //#endregion
}
