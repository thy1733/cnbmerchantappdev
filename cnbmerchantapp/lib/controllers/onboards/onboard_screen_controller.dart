import 'package:cnbmerchantapp/core.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class OnboardScreenController extends AppController {
  final IAppOnboardingScreenService _appOnboardScreenService =
      AppOnboardingScreenService();

  //#region All Override method
  @override
  void onInit() {
    darkThemMode();
    setupData();
    super.onInit();
  }

  @override
  void onReady() async {
    super.onReady();
    pageController = PageController(initialPage: currentIndex);
    await currentLanguagesAsync();
  }

  @override
  void onClose() {
    lightThemMode();
    pageController.dispose();
    super.onClose();
  }
  //#endregion

  //#region All Properties
  PageController pageController = PageController(initialPage: 0);

  final _currentIndex = 0.obs;
  int get currentIndex => _currentIndex.value;
  set currentIndex(int value) => _currentIndex.value = value;

  final _data = <OnboardModel>[].obs;
  List<OnboardModel> get data => _data;
  set data(List<OnboardModel> value) => _data.value = value;

  //#endregion

  //#region All Method Helpers
  Future<void> setupData() async {
    data.addAll(List.generate(
      3,
      (index) {
        index = index + 1;
        return OnboardModel.create(
          imageUrl: "assets/svgs/samples/boarding$index.svg",
          title: "OnboardingTitle$index",
          description: "OnboardingDescription$index",
        );
      },
    ));
  }
  //#endregion

  //#region All Command

  void nextPageAction() {
    if (currentIndex < data.length) {
      pageController.nextPage(
          duration: const Duration(milliseconds: 150),
          curve: Curves.decelerate);
    } else {
      skipAction();
    }
  }

  void skipAction() async {
    DeviceInfoPlugin info = DeviceInfoPlugin();
    await _appOnboardScreenService.saveAppOnboardingScreenAsync(info);
    Get.offAllNamed(Routes.login);
  }

  //#endregion

}
