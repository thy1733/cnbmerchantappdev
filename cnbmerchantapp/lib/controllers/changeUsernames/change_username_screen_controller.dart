import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:cnbmerchantapp/core.dart';

class ChangeUsernameScreenController extends AppController {
  //#region All Override method


  //#endregion

  //#region All Properties

  final _userName = ''.obs;
  String get userName => _userName.value;
  set userName(String value) => _userName.value = value;

  final _userNameValidate = ''.obs;
  String get userNameValidate => _userNameValidate.value;
  set userNameValidate(String value) => _userNameValidate.value = value;

  final _confirmeUserName = ''.obs;
  String get confirmedUserName => _confirmeUserName.value;
  set confirmedUserName(String value) => _confirmeUserName.value = value;

  final _confirmUserNameValidate = ''.obs;
  String get confirmUserNameValidate => _confirmUserNameValidate.value;
  set confirmUserNameValidate(String value) =>
      _confirmUserNameValidate.value = value;

  final FocusNode usernameFocusNode = FocusNode();
  final FocusNode confirmUsernameFocusNode = FocusNode();

  bool get shouldShowConfirmedField =>
      userName.isNotEmpty && userNameValidate.isEmpty;

  bool get isNextButtonEnabled =>
      userName.isNotEmpty &&
      confirmedUserName.isNotEmpty &&
      userNameValidate.isEmpty &&
      confirmUserNameValidate.isEmpty;

  //#endregion

  //#region All Method Helpers

  //#endregion

  //#region All Command

  void onSubmit() async {
    onUserNameChange();
    onConfirmsUserNameChange();
    if (userNameValidate.isNotEmpty) {
      usernameFocusNode.requestFocus();
      return;
    } else if (confirmUserNameValidate.isNotEmpty) {
      confirmUsernameFocusNode.requestFocus();
      return;
    }
    var arguments = {ArgumentKey.fromPage: Routes.changeUserName};
    super.navigationToNamedArgumentsAsync(Routes.confirmPassword,
        args: arguments);
  }

  void onUserNameChange() {
    if (userName.isEmpty) {
      userNameValidate = "UsernameRequiredMessage".tr;
    } else if (userName.length < 5) {
      userNameValidate = "UsernameNotvalid".tr;
    } else if (confirmedUserName.isNotEmpty && userName != confirmedUserName) {
      userNameValidate = "UsernameNotMatch".tr;
    } else {
      userNameValidate = "";
      if (userName == confirmedUserName) {
        confirmUserNameValidate = "";
      }
    }
  }

  void onConfirmsUserNameChange() {
    if (confirmedUserName.isEmpty) {
      confirmUserNameValidate = "UsernameRequiredMessage".tr;
    } else if (confirmedUserName.length < 5) {
      confirmUserNameValidate = "UsernameNotvalid".tr;
    } else if (userName.isNotEmpty && confirmedUserName != userName) {
      confirmUserNameValidate = "UsernameNotMatch".tr;
    } else {
      confirmUserNameValidate = "";
      if (confirmedUserName == userName) {
        userNameValidate = "";
      }
    }
  }

  //#endregion
}
