import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/services.dart';

class AppManageController extends AppPickerController {
  
  //#region System theme mode
  
  Future<void> darkThemMode()async{
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark);
  }

  Future<void> lightThemMode()async{
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light);
  }
  
  //#endregion
}