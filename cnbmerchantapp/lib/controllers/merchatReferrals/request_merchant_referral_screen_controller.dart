import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

class RequestMerchantReferralScreenController extends AppController {
  final IMerchantReferralService _merchantReferralService =
      MerchantReferralService();
  //#region All Override method
  //#endregion

  //#region All Properties
  final _businessNameValidateMessage = ''.obs;
  String get businessNameValidateMessage => _businessNameValidateMessage.value;
  set businessNameValidateMessage(String value) =>
      _businessNameValidateMessage.value = value;

  final _phoneValidateMessage = ''.obs;
  String get phoneValidateMessage => _phoneValidateMessage.value;
  set phoneValidateMessage(String value) => _phoneValidateMessage.value = value;

  // final _businessNameInput = ''.obs;
  // String get businessNameInput => _businessNameInput.value;
  // set businessNameInput(String value) => _businessNameInput.value = value;

  // final _phoneInput = ''.obs;
  // String get phoneInput => _phoneInput.value;
  // set phoneInput(String value) => _phoneInput.value = value;

  // final _location = LocationAddressModel().obs;
  // LocationAddressModel get location => _location.value;
  // set location(LocationAddressModel value) => _location.value = value;

  final _reqInput = ReqMerchantReferralInputModel().obs;
  ReqMerchantReferralInputModel get reqInput => _reqInput.value;
  set reqInput(ReqMerchantReferralInputModel value) => _reqInput.value = value;

  TextEditingController locationNameConroller = TextEditingController();
  //#endregion

  //#region All Method Helpers
  void callDeleted() {
    var locationController = Get.find<LocationScreenController>();
    locationController.dismissKeyboard();
    Get.delete<LocationScreenController>();
  }
  //#endregion

  //#region Validation

  void businessNameValidator() {
    if (reqInput.businessName.isEmpty) {
      businessNameValidateMessage = "BusinessNameRequired";
    } else {
      businessNameValidateMessage = ""; // CASE: BUSINESS_NAME_VALID
    }

    checkEnable();
  }

  void phoneValidator() {
    if (reqInput.businessContact.removeAllWhitespace.length >= 8) {
      phoneValidateMessage = "";
    } else if (reqInput.businessContact.isEmpty) {
      phoneValidateMessage = "PhoneNumberRequiredMessage";
    } else {
      phoneValidateMessage = "PhoneNumberInvalidMessage";
    }
    checkEnable();
  }

  void checkEnable() {
    isEnable = phoneValidateMessage.isEmpty &&
        businessNameValidateMessage.isEmpty &&
        reqInput.businessName.isNotEmpty &&
        reqInput.businessContact.isNotEmpty;
  }

  Future<void> requestMerchantReferralAsync() async {
    showLoading();
    var result =
        await _merchantReferralService.reqMerchantReferralAsync(reqInput);
    dismiss();

    if (result.success && result.result != null) {
      KAlertScreen.success(
          message: "Success".tr,
          description: "RequestMerchantReferralSuccessMsg".tr.tr,
          primaryButtonTitle: "Done".tr,
          onPrimaryButtonClick: () {
            navigationBackToFirstScreen();
          });
    } else {
      CChoiceDialog.ok(
        title: result.error?.message ?? "Failure".tr,
        message: result.error?.details ?? "SomethingWentWrongPleaseTryAgainLater".tr,
        buttonTitle: "Okay".tr,
      );
    }
  }
  //#endregion

  //#region All Command
  void onNextBtnPress() {
    FocusManager.instance.primaryFocus?.unfocus();
    requestMerchantReferralAsync();
  }

  Future<void> browseLocation() async {
    await showCupertinoModalPopup(
        context: Get.context!,
        builder: (context) => LocationScreen(
              initLocation: reqInput.businessLocation,
              locationCallBack: (result) {
                reqInput.businessLocation = result;
                locationNameConroller.text = result.address;
              },
            )).then((value) => callDeleted());
  }
  //#endregion
}
