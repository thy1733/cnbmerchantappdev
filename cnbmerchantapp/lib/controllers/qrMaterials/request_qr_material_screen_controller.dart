import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:cnbmerchantapp/core.dart';

class RequestQRMaterialScreenController extends AppController {
  final locationNameConroller = TextEditingController();
  final bankBranchConroller = TextEditingController();
  final IReqPrintedQrService _reqPrintedQrService = ReqPrintedQrService();
  final _bankOfficeController = Get.put(BrowseBankBranchLocationController());

  final OutletService _outletService = OutletService();
  //#region All Override method
  @override
  void onReady() async {
    super.onReady();
    await getBankOfficeLocationAsync();
  }
  //#endregion

  //#region All Properties

  final pickUpOptions = QrMaterialRecieverOptionEnum.values
      .where((element) => element != QrMaterialRecieverOptionEnum.none)
      .toList();

  final _requestQrMaterialInput = ReqPrintedQrInputModel().obs;
  ReqPrintedQrInputModel get requestQrMaterialInput =>
      _requestQrMaterialInput.value;
  set requestQrMaterialInput(ReqPrintedQrInputModel value) =>
      _requestQrMaterialInput.value = value;

  final _recieverOptoin = QrMaterialRecieverOptionEnum.none.obs;
  QrMaterialRecieverOptionEnum get recieverOptoin => _recieverOptoin.value;
  set recieverOptoin(QrMaterialRecieverOptionEnum value) =>
      _recieverOptoin.value = value;

  final _selectedBusiness = ManageBusinessItemListOutputModel().obs;
  ManageBusinessItemListOutputModel get selectedBusiness =>
      _selectedBusiness.value;
  set selectedBusiness(ManageBusinessItemListOutputModel value) =>
      _selectedBusiness.value = value;

  OutletListOutputModel outlets = OutletListOutputModel();

  final _selectedOutlet = OutletItemListOutputModel().obs;
  OutletItemListOutputModel get selectedOutlet => _selectedOutlet.value;
  set selectedOutlet(OutletItemListOutputModel value) =>
      _selectedOutlet.value = value;

  List<BankOfficeOutputModel> bankOffice = [];
  //#endregion

  //#region All Method Helpers

  Future<void> getOutletListAsync() async {
    var input = OutletListInputModel();
    input.businessId = selectedBusiness.id;
    input.filter = "";
    input.maxResultCount = 10;
    input.skipCount = 1;

    showLoading();
    var response = await _outletService.getListOutlet(input);
    dismiss();

    if (response.result != null && response.success) {
      outlets = response.result!;
      if (outlets.items.isNotEmpty) {
        selectedOutlet =
            outlets.items.firstWhereOrNull((element) => element.isPrimary) ??
                outlets.items.first;
      }
    }
  }

  Future<void> getBankOfficeLocationAsync() async {
    await _bankOfficeController.getBankOfficeLocationAsync();

    requestQrMaterialInput.pickUpLocation =  _bankOfficeController.bankBranchItems.first;
    bankBranchConroller.text = requestQrMaterialInput.pickUpLocation.locationName;
  }
  //#endregion

  //#region Validation
  void checkIsEnable() {
    int numberOfQr = requestQrMaterialInput.standee +
        requestQrMaterialInput.stickerBillPad +
        requestQrMaterialInput.lanyard;
    if (numberOfQr > 0 &&
        recieverOptoin != QrMaterialRecieverOptionEnum.none &&
        selectedOutlet.id.isNotEmpty) {
      if (recieverOptoin == QrMaterialRecieverOptionEnum.Delivery &&
          requestQrMaterialInput.locationDeliver.address.isEmpty) {
        isEnable = false;
      } else if (recieverOptoin == QrMaterialRecieverOptionEnum.SelfPickUp &&
          requestQrMaterialInput.pickUpLocation.id.isEmpty) {
        isEnable = false;
      } else {
        isEnable = true;
      }
    } else {
      isEnable = false;
    }
  }

  void callDeleted() {
    var locationController = Get.find<LocationScreenController>();
    locationController.dismissKeyboard();
    Get.delete<LocationScreenController>();
  }
  //#endregion

  //#region All Command
  void selectBusiness() async {
    await AppBottomSheet.bottomSheetSelectDynamicOption<
        ManageBusinessItemListOutputModel>(
      title: "SwitchBusiness".tr,
      items: AppBusinessTempInstant.appCacheBusiness.items,
      onSelectedItem: (index, value) async {
        if (value == selectedBusiness) return;
        selectedBusiness = value;
        await getOutletListAsync();
        checkIsEnable();
      },
      selectedItem: selectedBusiness,
      closeOnSelect: true,
      separator: ContainerDevider.vertical(),
      itemBuilder: (index, value) => Row(
        children: [
          AppNetworkImage(
            value.businessLogo,
            height: 30,
            width: 30,
            shape: BoxShape.circle,
            errorWidget: SvgPicture.asset(SvgAppAssets.logo),
          ),
          const SizedBox(width: 10),
          Text(
            value.businessName,
            style: AppTextStyle.body1.copyWith(color: AppColors.textPrimary),
          )
        ],
      ),
    );
  }

  void onSelectOutlet() async {
    await AppBottomSheet.bottomSheetSelectDynamicOption<
        OutletItemListOutputModel>(
      title: "SwitchOutlet".tr,
      items: outlets.items,
      onSelectedItem: (index, value) {
        if (selectedOutlet != value) {
          selectedOutlet = value;
          checkIsEnable();
        }
      },
      selectedItem: selectedOutlet,
      closeOnSelect: true,
      separator: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Container(
          color: AppColors.greyBackground,
          height: 1,
        ),
      ),
      itemBuilder: (index, value) => Row(
        children: [
          AppNetworkImage(
            value.outletLogo,
            height: 30,
            width: 30,
            shape: BoxShape.circle,
            errorWidget: Padding(
              // color: Colors.white,
              padding: const EdgeInsets.all(5.0),
              child: SvgPicture.asset(
                SvgAppAssets.icDefaultShop,
                color: AppColors.primary,
              ),
            ),
          ),
          const SizedBox(width: 10),
          Text(
            value.outletName,
            style: AppTextStyle.body1.copyWith(color: AppColors.textPrimary),
          )
        ],
      ),
    );
  }

  void selectPickUpOPtion() async {
    await AppBottomSheet.bottomSheetSelectDynamicOption<
            QrMaterialRecieverOptionEnum>(
        title: "ReceiveOption".tr,
        items: pickUpOptions,
        selectedItem: recieverOptoin,
        closeOnSelect: true,
        onSelectedItem: (i, d) {
          recieverOptoin = d;
          checkIsEnable();
        },
        selectedItemDecoration: BoxDecoration(
            color: AppColors.textFieldBackground,
            borderRadius: BorderRadius.circular(5)),
        itemBuilder: (i, d) {
          return Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 15),
            child: Text(
              d.name.tr,
              style: AppTextStyle.body2.copyWith(color: AppColors.textPrimary),
            ),
          );
        });
  }

  Future<void> browseDeliveryLocation() async {
    await showCupertinoModalPopup(
        context: Get.context!,
        builder: (context) => LocationScreen(
              initLocation: requestQrMaterialInput.locationDeliver,
              locationCallBack: (result) {
                requestQrMaterialInput.locationDeliver = result;
                locationNameConroller.text = result.address;
                checkIsEnable();
              },
            )).then(
      (value) => callDeleted(),
    );
  }

  void browseBankBranchLocation() async {
    await showCupertinoModalPopup(
      context: Get.context!,
      builder: (context) => BrowseBankBranchLocation(
        initialValue: requestQrMaterialInput.pickUpLocation,
        onConfirm: (result) {
          requestQrMaterialInput.pickUpLocation = result;
          bankBranchConroller.text = result.locationName;
          Get.back();
        },
      ),
    );
  }

  void reviewRequest() {
    FocusManager.instance.primaryFocus?.unfocus();

    /// SET_FEE
    requestQrMaterialInput.selectedRecieveOption = recieverOptoin;
    requestQrMaterialInput.selectedBusiness = selectedBusiness;
    requestQrMaterialInput.selectedOutlet = selectedOutlet;

    /// SET_ARGUMENTS
    var arguments = {
      ArgumentKey.input: requestQrMaterialInput,
      ArgumentKey.detail: requestQrMaterialInput.toDetailOutput(),
      ArgumentKey.fromPage: Routes.requestQrMaterialScreen
    };

    /// NAVIGATE
    toNamedArgumentsAsync(
      Routes.reviewRequestQrMaterialScreen,
      args: arguments,
    );
  }

  Future<void> getFeeReqPrintedQr() async {
    /// SET_INPUT
    requestQrMaterialInput.selectedRecieveOption = recieverOptoin;
    requestQrMaterialInput.selectedBusiness = selectedBusiness;
    requestQrMaterialInput.selectedOutlet = selectedOutlet;

    showLoading();
    var result =
        await _reqPrintedQrService.getFeeReqPrintedQr(requestQrMaterialInput);
    dismiss();

    /// ON_SUCCESS
    if (result.success && result.result != null) {
      FocusManager.instance.primaryFocus?.unfocus();

      /// SET_FEE
      requestQrMaterialInput.deliveryFee = result.result!.deliveryFee;
      requestQrMaterialInput.printFee = result.result!.printFee;
      requestQrMaterialInput.totalFee = result.result!.totalFee;

      /// SET_ARGUMENTS
      var arguments = {
        ArgumentKey.input: requestQrMaterialInput,
        ArgumentKey.detail: requestQrMaterialInput.toDetailOutput(),
        ArgumentKey.fromPage: Routes.requestQrMaterialScreen
      };

      /// NAVIGATE
      toNamedArgumentsAsync(
        Routes.reviewRequestQrMaterialScreen,
        args: arguments,
      );
    } else if (result.error != null) {
      await CChoiceDialog.ok(
          title: "Failure".tr,
          message: result.error!.message,
          ok: () {
            dismiss();
          });
    } else {
      await CChoiceDialog.ok(
          title: "Failure".tr,
          message: "SomethingWentWrongPleaseTryAgainLater".tr,
          ok: () {
            dismiss();
          });
    }

    // /// This block use to test for review scree
    // /// DELETE_THIS_WHEN_API_DONE
    // FocusManager.instance.primaryFocus?.unfocus();
    // var arguments = {
    //     ArgumentKey.input: requestQrMaterialInput,
    //     ArgumentKey.detail: requestQrMaterialInput.toDetailOutput(),
    //   ArgumentKey.fromPage: Routes.requestQrMaterialScreen
    // };

    // toNamedArgumentsAsync(
    //   Routes.reviewRequestQrMaterialScreen,
    //   args: arguments,
    // );
    // ///// END_OF_TEST_BLOCK
  }

  //#endregion

}
