import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class RequestPrintedQrListScreenController extends AppController {
  final refreshController = RefreshController();
  final IReqPrintedQrService _reqPrintedQrService = ReqPrintedQrService();
  final IBusinessService _setupService = BusinessService();

  //#region All Override method

  @override
  void onReady() async {
    showLoading();
    isLoading = true;
    await getListReqPrintedQrAsync();
    dismiss();


  /// due to API list requested qr can't biz type name so we need to call this
    if(requestPrintedQrDatas.isNotEmpty){
        await getListBusinessTypeAsync();
    }
    isLoading = false;
    super.onReady();
  }
  //#endregion

  //#region All Properties
  final _requestPrintedQrDatas = <ReqPrintedQrDetailOutputModel>[].obs;
  List<ReqPrintedQrDetailOutputModel> get requestPrintedQrDatas =>
      _requestPrintedQrDatas;
  set requestPrintedQrDatas(List<ReqPrintedQrDetailOutputModel> value) =>
      _requestPrintedQrDatas.value = value;

  final _bizTypeList = BusinessTypeListOutputModel().obs;
  BusinessTypeListOutputModel get bizTypeList => _bizTypeList.value;
  set bizTypeList(BusinessTypeListOutputModel value) =>
      _bizTypeList.value = value;

  //#endregion

  //#region All Method Helpers
  void onRefresh() async {
    await getListReqPrintedQrAsync();
    refreshController.refreshCompleted();
  }

  Future<void> getListReqPrintedQrAsync() async {
    try {
      // showLoading();
      var result = await _reqPrintedQrService.getListReqPrintedQr();
      // dismiss();
      if (result.success && result.result != null) {
        requestPrintedQrDatas = result.result!.items;
      }
    } catch (e) {
      e.printError();
    }
  }

  Future<void> getListBusinessTypeAsync() async {
    var input = BusinesTypeListInputModel();
    var result = await _setupService.getListBusinessTypeAsync(input);
    if (result.success && result.result != null) {
      bizTypeList = result.result!;
    }
  }

  //#endregion

  //#region Validation
  //#endregion

  //#region All Command
  void onRequestQrMaterial() {
    toNamedArgumentsAsync(Routes.requestQrMaterialScreen).then((value) async {
      if (value != null) {
        // if (value[ArgumentKey.detail] != null) {
        //   requestPrintedQrDatas.insert(0, value[ArgumentKey.detail]);
        // }
        showLoading();
        await getListReqPrintedQrAsync();
        dismiss();
      }
    });
  }

  void onReviewRequestMaterial(ReqPrintedQrDetailOutputModel data) {
    var result =
        bizTypeList.items.firstWhereOrNull((e) => e.id == data.businessTypeId);
    data.businessTypeName = result?.name ?? "";
    var arguments = {
      ArgumentKey.detail: data,
    };
    toNamedArgumentsAsync(
      Routes.reviewRequestQrMaterialScreen,
      args: arguments,
    );
  }
  //#endregion
}
