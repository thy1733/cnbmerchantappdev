import 'dart:async';
import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:url_launcher/url_launcher.dart';
import 'dart:ui' as ui;

class ReviewRequestQRMaterialScreenController extends AppController {
  final IReqPrintedQrService _reqPrintedQrService = ReqPrintedQrService();

  //#region All Override method
  @override
  void onInit() {
    getArgument();
    super.onInit();
  }

  @override
  void onReady() {
    generateMarker();
    super.onReady();
  }
  //#endregion

  //#region All Properties

  final _fromPage = ''.obs;
  String get fromPage => _fromPage.value;
  set fromPage(String value) => _fromPage.value = value;

  final _makerIcon = BitmapDescriptor.defaultMarker.obs;
  BitmapDescriptor get makerIcon => _makerIcon.value;
  set makerIcon(BitmapDescriptor value) => _makerIcon.value = value;

  final _requestQrMaterialOutput = ReqPrintedQrDetailOutputModel().obs;
  ReqPrintedQrDetailOutputModel get requestQrMaterialOutput =>
      _requestQrMaterialOutput.value;
  set requestQrMaterialOutput(ReqPrintedQrDetailOutputModel value) =>
      _requestQrMaterialOutput.value = value;
  ReqPrintedQrInputModel requestQrMaterialInput = ReqPrintedQrInputModel();

  Completer<GoogleMapController> mapController = Completer();
  Set<Marker> markers = {};

  bool isOpeningUrl = false;

  //#endregion

  //#region All Method Helpers

  void getArgument() {
    if (Get.arguments == null) return;

    // CREATE_REQ_PRINTED_QR_FLOW
    if (Get.arguments[ArgumentKey.fromPage] != null) {
      fromPage = Get.arguments[ArgumentKey.fromPage] as String;
    }
    if (Get.arguments[ArgumentKey.fromPage] != null) {
      requestQrMaterialInput =
          Get.arguments[ArgumentKey.input] as ReqPrintedQrInputModel;
    }

    // REVIEW_&_DETAIL
    if (Get.arguments[ArgumentKey.detail] != null) {
      requestQrMaterialOutput =
          Get.arguments[ArgumentKey.detail] as ReqPrintedQrDetailOutputModel;
    }
  }

  void generateMarker({String name = ""}) async {
    makerIcon = await _bitmapDescriptorFromSvgAsset(
        Get.context!, SvgAppAssets.locationOn);
    isLoading = true;
    markers = {};
    LocationAddressModel location;
    if (requestQrMaterialOutput.selectedRecieveOption ==
        QrMaterialRecieverOptionEnum.Delivery) {
      location = requestQrMaterialOutput.locationDeliver;
    } else {
      location = LocationAddressModel(
        address: requestQrMaterialOutput.pickUpLocation.locationAddress,
        locationName: requestQrMaterialOutput.pickUpLocation.locationName,
        id: requestQrMaterialOutput.pickUpLocation.id,
        latitude: requestQrMaterialOutput.pickUpLocation.latitude,
        longitude: requestQrMaterialOutput.pickUpLocation.longitude,
      );
    }

    markers.add(
      Marker(
        //add first marker
        markerId: MarkerId(location.locationName),
        position:
            LatLng(location.latitude, location.longitude), //position of marker
        icon: makerIcon, //Icon for Marker
      ),
    );

    moveMapToCenterAsync(LatLng(location.latitude, location.longitude));

    isLoading = false;
  }

  Future<BitmapDescriptor> _bitmapDescriptorFromSvgAsset(
    BuildContext context,
    String assetName,
  ) async {
    String svgString =
        await DefaultAssetBundle.of(context).loadString(assetName);
    //Draws string representation of svg to DrawableRoot
    DrawableRoot svgDrawableRoot = await svg.fromSvgString(svgString, 'a');
    ui.Picture picture =
        svgDrawableRoot.toPicture(size: const Size.fromHeight(100));
    ui.Image image = await picture.toImage(100, 100);
    ByteData? bytes = await image.toByteData(format: ui.ImageByteFormat.png);

    if (bytes == null) {
      return BitmapDescriptor.defaultMarker;
    }
    return BitmapDescriptor.fromBytes(bytes.buffer.asUint8List());
  }

  Future<void> moveMapToCenterAsync(LatLng latLng) async {
    try {
      await mapController.future.then(
        (value) async => await value.animateCamera(
          CameraUpdate.newLatLngZoom(
            LatLng(latLng.latitude, latLng.longitude),
            16.0,
          ),
        ),
      );
    } catch (e) {
      Get.showSnackbar(const GetSnackBar(
        title: "Error",
        message: "We're sorry.",
      ));
    }
  }

  Future<void> getFeeReqPrintedQr() async {
    showLoading();
    var result =
        await _reqPrintedQrService.reqPrintedQr(requestQrMaterialInput);
    dismiss();

    /// ON_SUCCESS
    // if (result.success && result.result != null) {
    if (result.success) {
      await KAlertScreen.success(
          message: "Success".tr,
          description: "RequestPrintedQrSuccessMessage".tr,
          primaryButtonTitle: "Done".tr,
          onPrimaryButtonClick: () {
            // Get.back();
            Get.close(3); // close until list
            // add value to list
            Get.back(result: {ArgumentKey.detail: requestQrMaterialOutput});
          });
    } else if (result.error != null) {
      await CChoiceDialog.ok(
          title: "Failure".tr,
          message: result.error!.message,
          ok: () {
            Get.close(2); // close pin dialog,screeen
          });
    } else {
      await CChoiceDialog.ok(
          title: "Failure".tr,
          message: "SomethingWentWrongPleaseTryAgainLater".tr,
          ok: () {
            Get.close(2); // close pin dialog,screeen
          });
    }
  }
  //#endregion

  //#region Validation
  //#endregion

  //#region All Command
  void onNextBtnPress() async {
    // Todo: CALL_REQUEST_QR_MATERIAL_API
    requestToConfirmPIN(
      title: "RequestPrintedQr".tr,
      message: "ConfirmPin".tr,
      description: "YouAreGoingToCompleteTheProcess".tr,
      isPermanentRequired: true,
      onSuccess: (_) {
        getFeeReqPrintedQr();
      },
    );
  }

  void openMapAsync() async {
    if (isOpeningUrl) return;
    isOpeningUrl = true;
    LocationAddressModel location;
    if (requestQrMaterialOutput.selectedRecieveOption ==
        QrMaterialRecieverOptionEnum.Delivery) {
      location = requestQrMaterialOutput.locationDeliver;
    } else {
      location = LocationAddressModel(
        address: requestQrMaterialOutput.pickUpLocation.locationAddress,
        locationName: requestQrMaterialOutput.pickUpLocation.locationName,
        id: requestQrMaterialOutput.pickUpLocation.id,
        latitude: requestQrMaterialOutput.pickUpLocation.latitude,
        longitude: requestQrMaterialOutput.pickUpLocation.longitude,
      );
    }
    String googleUrl =
        'https://www.google.com/maps/search/?api=1&query=${location.latitude},${location.longitude}';
    if (!await launchUrl(Uri.parse(googleUrl),
        mode: LaunchMode.externalNonBrowserApplication)) {
      if (!await launchUrl(Uri.parse(googleUrl))) {
        throw 'Could not launch $googleUrl';
      }
    }
    await Future.delayed(const Duration(milliseconds: 200));
    isOpeningUrl = false;
  }
  //#endregion

}
