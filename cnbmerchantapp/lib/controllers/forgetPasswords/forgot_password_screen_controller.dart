import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';

class ForgotPasswordScreenController extends AppController {
  //#region All Override method
  //#endregion

  //#region All Properties

  final LoginService _loginService = LoginService();

  final AppVerifyOTP _appVerifyOTP = AppVerifyOTP();

  final _accountNumberOrInviteCodeValidateMessage = ''.obs;
  String get accountNumberOrInviteCodeValidateMessage =>
      _accountNumberOrInviteCodeValidateMessage.value;
  set accountNumberOrInviteCodeValidateMessage(String value) =>
      _accountNumberOrInviteCodeValidateMessage.value = value;

  final _phoneNumberValidateMessage = ''.obs;
  String get phoneNumberValidateMessage => _phoneNumberValidateMessage.value;
  set phoneNumberValidateMessage(String value) =>
      _phoneNumberValidateMessage.value = value;

  final _accountNumberOrInviteCode = ''.obs;
  String get accountNumberOrInviteCode => _accountNumberOrInviteCode.value;
  set accountNumberOrInviteCode(String value) =>
      _accountNumberOrInviteCode.value = value;

  final _phoneNumber = ''.obs;
  String get phoneNumber => _phoneNumber.value;
  set phoneNumber(String value) => _phoneNumber.value = value;

  //#endregion

  //#region All Method Helpers
  void onVerifyOTPSuccess() {
    Get.back(); // CLOSE OPT SCREEN
    var arguments = {
      ArgumentKey.fromPage: Routes.forgotPassword,
      ArgumentKey.item: accountNumberOrInviteCode
    };
    navigationToNamedArgumentsAsync(Routes.newPasswordScreen, args: arguments);
  }
  //#endregion

  //#region Validation

  void accountValidator() {
    if (accountNumberOrInviteCode.removeAllWhitespace.length == 6) {
      accountNumberOrInviteCodeValidateMessage = ""; // CASE: INVITE_CODE
    } else if (accountNumberOrInviteCode.removeAllWhitespace.length == 13) {
      accountNumberOrInviteCodeValidateMessage = ""; // CASE: ACCOUNT_NUMBER
    } else if (accountNumberOrInviteCode.isEmpty) {
      accountNumberOrInviteCodeValidateMessage =
          "AccountNumberOrInviteCodeRequiredMessage";
    } else {
      accountNumberOrInviteCodeValidateMessage =
          "AccountNumberOrInviteCodeInvalidMessage";
    }
    checkEnable();
  }

  void phoneValidator() {
    if (phoneNumber.removeAllWhitespace.length >= 8) {
      phoneNumberValidateMessage = "";
    } else if (phoneNumber.isEmpty) {
      phoneNumberValidateMessage = "PhoneNumberRequiredMessage";
    } else {
      phoneNumberValidateMessage = "PhoneNumberInvalidMessage";
    }
    checkEnable();
  }

  void checkEnable() {
    isEnable = phoneNumberValidateMessage.isEmpty &&
        accountNumberOrInviteCodeValidateMessage.isEmpty &&
        accountNumberOrInviteCode.isNotEmpty &&
        phoneNumber.isNotEmpty;
  }
  //#endregion

  Future<void> onForgotPasswordAsync() async {
    showLoading();
    UserForgotPassInputModel input = UserForgotPassInputModel();
    input.activationNumber = accountNumberOrInviteCode;
    input.phoneNumber = phoneNumber;
    var result = await _loginService.forgotPasswordAsync(input);
    dismiss();
    if (result.error == null) {
      if (result.success) {
        _appVerifyOTP.requestVerifyOTP(
          title: "ForgotPassword".tr,
          cidOrAccountNumber: accountNumberOrInviteCode,
          merchantId: "",
          phoneNumber: phoneNumber,
          onSuccess: (otpCode, cidOrAccountNumber) {
            onVerifyOTPSuccess();
          },
          fromPage: Routes.forgotPassword,
        );
      } else {
        CChoiceDialog.ok(
            title: "Failure".tr,
            message: result.result?.description.tr ??
                "SomethingWentWrongPleaseTryAgainLater".tr);
      }
    } else {
      CChoiceDialog.ok(title: "Failure".tr, message: result.error!.details.tr);
    }
  }

  //#region All Command
  void onNextPress() async {
    // check vailidation
    accountValidator();
    phoneValidator();

    await onForgotPasswordAsync();
  }

  //#endregion
}
