import 'package:cnbmerchantapp/core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ConfirmPasswordScreenController extends AppController {
  //#region All Override method
  @override
  void onInit() {
    getArgument();
    super.onInit();
  }
  //#endregion
  //#region All Properties

  final ILoginService _loginService = LoginService();

  final TextEditingController passwordTextController = TextEditingController();

  final _userName = ''.obs;
  String get userName => _userName.value;
  set userName(String value) => _userName.value = value;

  final _password = ''.obs;
  String get password => _password.value;
  set password(String value) => _password.value = value;

  final _isPasswordVisible = false.obs;
  bool get isPasswordVisible => _isPasswordVisible.value;
  set isPasswordVisible(value) => _isPasswordVisible.value = value;

  String fromPage = "";
  String title = "";
  String subTitle = "";

  //#endregion

  //#region All Method Helpers
  void getArgument() {
    if (Get.arguments == null) return;
    if (Get.arguments[ArgumentKey.fromPage] != null) {
      fromPage = Get.arguments[ArgumentKey.fromPage];
      checkPageOnStart();
    }
  }

  void checkPageOnStart() {
    switch (fromPage) {
      case Routes.changeUserName:
        subTitle = "ConfirmPassword";
        title = "ChangeUsername";
        break;
      case Routes.changePassword:
        subTitle = "EnterPassword";
        title = "ChangePassword";
        break;
      case Routes.resetPin:
        subTitle = "EnterPassword";
        title = "ResetPIN";
        break;
      default:
        break;
    }
  }

  void clearPasswordField() => passwordTextController.text = "";

  //#endregion

  //#region All Command
  onSubmit() {
    FocusManager.instance.primaryFocus?.unfocus();
    switch (fromPage) {
      case Routes.changeUserName:
        changeUsernameAsync();
        break;
      case Routes.changePassword:
        changePasswordAsync();
        break;
      case Routes.resetPin:
        resetPinAsync();
        break;
      default:
        break;
    }
  }

  Future<void> changeUsernameAsync() async {
    if (password == "123456") {
      KAlertScreen.success(
        message: "Success".tr,
        primaryButtonTitle: "Done".tr,
        description: "ChangeUsernameSuccessDescription".tr,
        onPrimaryButtonClick: () => Get.close(3), // Pop to setting tab
      );
    } else {
      clearPasswordField();
      KAlertScreen.failure(
        message: "${"Failure".tr}!",
        description: "WrongPasswordMessage".tr.replaceAll('0', '2'),
        primaryButtonTitle: "Ok".tr,
        onPrimaryButtonClick: () => Get.back(),
      );
    }
  }

  Future<void> changePasswordAsync() async {
    try {
      var newPassword = "";
      if (Get.arguments[ArgumentKey.item] != null) {
        newPassword = Get.arguments[ArgumentKey.item] as String;
      }
      final userInfo = AppUserTempInstant.appCacheUser;
      var input = UserChangePasswordInputModel(
        username: userInfo.userName,
        currentPassword: password,
        newPassword: newPassword,
        accountNumber: userInfo.accountNumber,
        inviteCode: userInfo.inviteCode,
      );
      showLoading();
      var result = await _loginService.changePasswordAsync(input);
      dismiss();
      if (result.success) {
        KAlertScreen.success(
          message: "Success".tr,
          description: "ChangePasswordSuccessDescription".tr,
          primaryButtonTitle: "Done".tr,
          onPrimaryButtonClick: () => Get.close(3), // Pop to setting tab
        );
      } else {
        clearPasswordField();
        showErrorMessage(
            "${result.result?.description ?? ''} \n${"WrongPasswordMessage".tr.replaceAll('0', '2')}");
      }
    } catch (e) {
      debugPrint(e.toString());
      clearPasswordField();
      showErrorMessage(e.toString());
    }
  }

  void resetPinAsync() async {
    if (Get.arguments[ArgumentKey.item] is String) {
      var input = UserResetPinInputModel(
        mCID: AppUserTempInstant.appCacheUser.mCID,
        currentPassword: password,
        newPIN: Get.arguments[ArgumentKey.item] as String,
        userName: AppUserTempInstant.appCacheUser.userName,
      );
      try {
        showLoading();
        var resutl = await _loginService.resetPINAsync(input);
        dismiss();
        if (resutl.success) {
          KAlertScreen.success(
            message: "Success".tr,
            primaryButtonTitle: "Done".tr,
            description: "ChangedPinSuccessDescription".tr,
            onPrimaryButtonClick: () =>
                navigationBackToFirstScreen(), // Pop to setting tab
          );
        } else {
          showErrorMessage(
              "${resutl.result?.description ?? ''} \n${"WrongPasswordMessage".tr.replaceAll('0', '2')}");
        }
      } catch (error) {
        debugPrint(error.toString());
        showErrorMessage("WrongPasswordMessage".tr.replaceAll('0', '2'));
      }
    } else {
      Get.back();
      showErrorMessage("WrongPasswordMessage".tr.replaceAll('0', '2'));
    }
  }

  void showErrorMessage(String description) async {
    await KAlertScreen.failure(
      message: "${"Failure".tr}!",
      description: description,
      primaryButtonTitle: "Ok".tr,
      onPrimaryButtonClick: () => backAsync(),
    );
  }
  //#endregion
}
