import 'dart:async';

import 'package:cnbmerchantapp/core.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';

class UserGuidesScreenController extends BaseWebController {
  //#region All Override method

  final UserGuidesService _userGuidesService = UserGuidesService();


  //#endregion

  //#region All Properties

  final _userGuides = UserGuideListOutputModel().obs;
  UserGuideListOutputModel get userGuides => _userGuides.value;
  set userGuides(UserGuideListOutputModel value) => _userGuides.value = value;

  //#endregion

  //#region All Method Helpers

  Future<void> getUserGuidesAsync() async {
    showLoading();
    var input = UserGuideItemInputModel();
    var result = await _userGuidesService.getUserGuides(input);
    dismiss();
    if (result.success) {
      userGuides = result.result ?? UserGuideListOutputModel();
    } else {
      generateSampleUserGuides();
    }
  }

  void generateSampleUserGuides() {
    String videoUrl1 = 'https://youtu.be/uNo-ihCCjiU';
    String? videoId1 = YoutubeHelper.convertUrlToId(videoUrl1);
    String imageUrl1 = videoId1 != null
        ? YoutubeHelper.getThumbnail(
            videoId: videoId1,
            webp: false,
          )
        : '';

    String videoUrl2 = 'https://youtu.be/MiykKmcP-ig';
    String? videoId2 = YoutubeHelper.convertUrlToId(videoUrl2);
    String imageUrl2 = videoId2 != null
        ? YoutubeHelper.getThumbnail(
            videoId: videoId2,
            webp: false,
          )
        : '';

    String videoUrl3 = 'https://youtu.be/-rj98TogZZg';
    String? videoId3 = YoutubeHelper.convertUrlToId(videoUrl3);
    String imageUrl3 = videoId3 != null
        ? YoutubeHelper.getThumbnail(
            videoId: videoId3,
            webp: false,
          )
        : '';

    List<UserGuideItemOutputModel> guides = [
      UserGuideItemOutputModel(
          imageUrl: imageUrl1,
          videoUrl: videoUrl1,
          title: 'របៀបបង់ពន្ធមធ្យោបាយដឺកជញ្ជូននិងយានជំនិះគ្រប់ប្រភេទ',
          description:
              'របៀបបង់ពន្ធលើមធ្យោបាយដឹកជញ្ជូននិងយានជំនិះគ្រប់ប្រភេទតាមរយកម្មវិធីទូរស័ព្ទដៃGDT Taxpayer App!'),
      UserGuideItemOutputModel(
          imageUrl: imageUrl2,
          videoUrl: videoUrl2,
          title:
              "Interested in opening a Teen account but don't know how? Watch this video to find out how!",
          description:
              "Interested in opening a Teen account but don't know how? Watch this video to find out how!"),
      UserGuideItemOutputModel(
          imageUrl: imageUrl3,
          videoUrl: videoUrl3,
          title:
              'របៀបបង់ពន្ធគយជូនអគ្គនាយកដ្ឋានគយនិងរដ្ឋាករកម្ពុជានៅលើCanadia Bank App',
          description:
              'បង់ពន្ធជូនអគ្គនាយកដ្ឋានគយនិងរដ្ឋាករកម្ពុជាកាន់តែងាយស្រួលសម្រាប់អតិថិជនធនាគារកាណាឌីយ៉ាដោយធ្វើតាមវិធីណែនាំខាងក្រោមនៅលើCanadia Bank App'),
    ];

    userGuides.items = guides;
  }

  //#endregion

  //#region All Command

  retryAsync() async {
    isOnline = await checkInternetConnection();
    if (!isOnline) {
      CChoiceDialog.ok(
          title: "NoInternetConnection".tr,
          message: "NoInternetConnectionDetails".tr);
      return;
    }
  }

  Future<void> launchVideoUrlAsync(String videoUrl) async {
    await launchUrl(
        Uri.parse(
          videoUrl,
        ),
        mode: LaunchMode.externalApplication);
  }

  //#endregion
}
