import 'package:cnbmerchantapp/core.dart';
import 'package:cnbmerchantapp/helpers/components/snackBars/c_snack_bar.dart';
import 'package:cnbmerchantapp/helpers/components/widgets/cashier_status_item_widget.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:share_plus/share_plus.dart';

class CashierListScreenController extends AppController {
  final ICashierListService _cashierService = CashierService();
  final BusinessDetailScreenController _businessController =
      Get.find<BusinessDetailScreenController>();
  final IAppFileUploadService _fileUploadService = AppFileUploadService();
  //#region All Override method

  @override
  void onReady() {
    getListCashier();

    super.onReady();
  }

  //#endregion

  //#region All Properties

  final _item = CashierItemListOutputModel().obs;
  CashierItemListOutputModel get item => _item.value;
  set item(CashierItemListOutputModel value) => _item.value = value;

  final _isRefundable = false.obs;
  bool get isRefundable => _isRefundable.value;
  set isRefundable(bool value) => _isRefundable.value = value;

  final _refundAmount = "0".obs;
  String get refundAmount => _refundAmount.value;
  set refundAmount(String value) => _refundAmount.value = value;

  final _isEmptyData = false.obs;
  bool get isEmptyData => _isEmptyData.value;
  set isEmptyData(bool value) => _isEmptyData.value = value;

  final _inviteCode = "123456".obs;
  String get inviteCode => _inviteCode.value;
  set inviteCode(String value) => _inviteCode.value = value;

  final _expiredIn = "72".obs;
  String get expiredIn => _expiredIn.value;
  set expiredIn(String value) => _expiredIn.value = value;

  final _currency = CurrencyEnum.usd.obs;
  CurrencyEnum get currency => _currency.value;
  set currency(CurrencyEnum value) => _currency.value = value;

  final _cashierList = <CashierItemListOutputModel>[].obs;
  List<CashierItemListOutputModel> get cashierList => _cashierList;
  set cashierList(List<CashierItemListOutputModel> value) =>
      _cashierList.value = value;
  //#endregion

  //#region All Method Helperss
  //#endregion

  //#region All Command

  Future<void> toSetupCashierScreenAsync() async {
    var arguments = <String, dynamic>{
      ArgumentKey.item: _businessController.businessDetail
    };
    var result = await navigationToNamedArgumentsAsync(
        Routes.setupCashierScreen,
        args: arguments);
    if (result.isNotEmpty) {
      CSnackBar.showSnackBarTop(message: "CreatedCashierSuccessfully".tr);
    }
  }

  Future<void> toUpdateCashierScreen(CashierItemListOutputModel oldItem) async {
    var arguments = <String, dynamic>{ArgumentKey.detail: oldItem};
    var result = await navigationToNamedArgumentsAsync(
        Routes.updateCashierScreen,
        args: arguments);
    if (result.isNotEmpty) {
      CSnackBar.showSnackBarTop(message: "SavedOutletSuccessfully".tr);
    }
  }

  Future<void> onSearchCashierAsync() async {
    await showFullScreenSearch(
        context: Get.context!, delegate: SearchCashierScreenDelegate());
  }

  void showCashierDetail() {
    isRefundable = item.isRefundable == 'true';
    refundAmount = item.refundLimited;
    showCupertinoModalBottomSheet(
        topRadius: const Radius.circular(23),
        backgroundColor: AppColors.barrierBackground,
        context: Get.context!,
        builder: (_) {
          return BottomSheetBusinessDetail(child: cashierDetailBuilder());
        });
  }

  Widget cashierDetailBuilder() {
    final double padding = 10.r;

    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        header(item),
        Padding(
          padding:
              EdgeInsets.only(left: padding, right: padding, bottom: padding),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              linkedOutlet(item),
              SizedBox(
                height: padding,
              ),
              termCondtionSection(),
              SizedBox(
                height: padding,
              ),
              refundSection(),
              SizedBox(
                height: padding,
              ),
              Container(
                height: 1,
                width: Get.width,
                color: AppColors.greyishDisable,
              ),
              deactivateSection()
            ],
          ),
        ),
      ],
    );
  }

  Widget header(CashierItemListOutputModel item) {
    final double padding = 16.r;
    final double imageSize = Get.width / 6;

    return Row(
      children: [
        Container(
            margin: EdgeInsets.all(padding),
            width: imageSize,
            height: imageSize,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(50),
                border: Border.all(color: AppColors.grey, width: 1)),
            child: ClipOval(
              child: AppNetworkImage(
                item.photoUrl,
                cacheKey: item.cacheImage,
                errorWidget:
                    PlaceholderImage.userName(item.firstName, item.lastName),
              ),
            )),
        Expanded(
            flex: 10,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "${item.firstName} ${item.lastName}",
                  style: AppTextStyle.header1,
                ),
                Text(
                  "${"ID".tr}: ${item.id}",
                  style: AppTextStyle.caption
                      .copyWith(color: AppColors.textSecondary),
                ),
                Text(
                  item.outlets[0].outletName,
                  style: AppTextStyle.caption
                      .copyWith(color: AppColors.textSecondary),
                ),
              ],
            )),
        CashierStatusItemWidget(status: item.cashierStatus),
        Expanded(
          flex: 3,
          child: Container(
            width: padding * 3.8.r,
          ),
        )
      ],
    );
  }

  Widget linkedOutlet(CashierItemListOutputModel item) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "LinkedOutlets".tr,
          style: AppTextStyle.label1,
        ),
        const SizedBox(
          height: 10,
        ),
        Wrap(
          children: item.outlets
              .map((outlet) => Card(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15)),
                    child: Container(
                      padding: const EdgeInsets.fromLTRB(2, 2, 10, 2),
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          AppNetworkImage(
                            outlet.logoUrl,
                            shape: BoxShape.circle,
                            width: 25,
                            height: 25,
                            errorWidget: SvgPicture.asset(
                              SvgAppAssets.pOutletList,
                            ),
                          ),
                          const SizedBox(
                            width: 5,
                          ),
                          Text(
                            outlet.outletName,
                            style: const TextStyle(
                                color: Colors.black,
                                fontSize: 11,
                                height: 1,
                                fontWeight: FontWeight.w400),
                          ),
                        ],
                      ),
                    ),
                  ))
              .toList(),
        )
      ],
    );
  }

  Widget linkedOutletItem() {
    final double imageSize = Get.width / 12;

    return Center(
        child: Row(
      children: [
        ClipOval(
          child: Container(
            width: imageSize,
            height: imageSize,
            padding: const EdgeInsets.all(8),
            decoration: BoxDecoration(
                border: Border.all(color: AppColors.grey, width: 1),
                borderRadius: BorderRadius.circular(50)),
            child: SvgPicture.asset(
              SvgAppAssets.icShopRed,
            ),
          ),
        ),
        const SizedBox(
          width: 8,
        ),
        Text(
          "Outlet's Location",
          style: AppTextStyle.value,
        ),
        const SizedBox(
          width: 8,
        ),
      ],
    ));
  }

  Widget termCondtionSection() {
    return Text.rich(
      TextSpan(text: "TermConditionMsg".tr, children: [
        const TextSpan(text: " "),
        TextSpan(
            recognizer: TapGestureRecognizer()
              ..onTap = () => Get.toNamed(Routes.merchantTermsAndCondition),
            text: "TermsAndConditions".tr,
            style: const TextStyle(color: AppColors.primary)),
        TextSpan(text: ".", style: AppTextStyle.label1)
      ]),
      textAlign: TextAlign.left,
      style: AppTextStyle.label1,
    );
  }

  Widget refundSection() {
    return Obx(
      () => Row(
        children: [
          Expanded(
              child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  Text(
                    "Refund".tr,
                    style: AppTextStyle.header2,
                  )
                ],
              )
            ],
          )),
          CupertinoSwitch(
            activeColor: AppColors.primary,
            value: isRefundable,
            onChanged: (value) {
              onRefundableChanged(value);
            },
          ),
        ],
      ),
    );
  }

  Widget deactivateSection() {
    final double padding = 16.r;
    bool cashierStatus = item.cashierStatus == CashierStatus.active;
    String title = cashierStatus ? "Deactivate".tr : "Active".tr;

    String message =
        cashierStatus ? "DeactivatedCashierMsg".tr : "ActivatedCashierMsg".tr;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        TextButton(
            style: AppButtonStyle.kSecondaryButtonExpandStyle(),
            child: Text(
              "Edit".tr,
              style: AppTextStyle.header2,
            ),
            onPressed: () {
              Get.back();
              toUpdateCashierScreen(item);
            }),
        Container(
          height: 1,
          width: Get.width,
          color: AppColors.greyishDisable,
        ),
        TextButton(
          onPressed: () {
            Get.back();
            requestToConfirmPIN(
                title: "NewInvitationCode".tr,
                onSuccess: (_) {
                  reInviteCashier();
                });
          },
          style: AppButtonStyle.kSecondaryButtonExpandStyle(),
          child: Text(
            "NewInvitationCode".tr,
            style: AppTextStyle.header2,
          ),
        ),
        Container(
          height: 1,
          width: Get.width,
          color: AppColors.greyishDisable,
        ),
        TextButton(
          onPressed: () {
            Get.back();
            requestToConfirmPIN(
                title: title,
                onSuccess: (_) {
                  deactivateCashier(cashierStatus, message);
                });
          },
          style: AppButtonStyle.kSecondaryButtonExpandStyle(),
          child: Text(
            title,
            style: AppTextStyle.header2,
          ),
        ),
        Container(
          height: 1,
          width: Get.width,
          color: AppColors.greyishDisable,
        ),
        TextButton(
          onPressed: () {
            Get.back();
            requestToConfirmPIN(
                title: "ClosePermanently".tr,
                onSuccess: (_) {
                  deleteCashier(cashierStatus, message);
                });
          },
          style: AppButtonStyle.kSecondaryButtonExpandStyle(),
          child: Text(
            "ClosePermanently".tr,
            style: AppTextStyle.primary1,
          ),
        ),
        SizedBox(
          height: padding * 2,
        ),
      ],
    );
  }

  void showSuccessInviteCode(CashierItemListOutputModel item) {
    String fullName = "${item.firstName} ${item.lastName}";
    KAlertScreen.successInviteCode(
      primaryButtonTitle: "Title",
      onPrimaryButtonClick: () async {
        await shareInviteCode();
      },
      secondaryButtonTitle: "Done".tr,
      onSecondaryButtonClick: () {
        Get.back();
      },
      message: "Congratulation".tr,
      description: fullName,
      formatedInviteCode: formatedInviteCodeString(inviteCode),
      expiredIn: expiredIn,
    );
  }

  Future<void> shareInviteCode() async {
    if (isLoading) return;
    isLoading = true;
    shareLink();
    await Future.delayed(const Duration(seconds: 1));
    isLoading = false;
  }

  Future<void> onRefundableChanged(bool value) async {
    requestToConfirmPIN(
        title: "confirmUpdate".tr,
        onSuccess: (_) {
          Get.back();
          isRefundable = value;
          updateCashierAsync(
              mode: CashierUpdateStatus.updateRefundStatus,
              refundStatus: isRefundable);
        });
    // if (isRefundable) {
    //   onSetRefundableLimit();
    // } else {
    //   requestToConfirmPIN(
    //       title: "confirmUpdate".tr,
    //       onSuccess: (_) {
    //         Get.back();
    //         updateCashierAsync(
    //             mode: CashierUpdateStatus.updateRefundStatus,
    //             refundStatus: false);
    //         refundAmount = "0";
    //       });
    // }
  }

  void onSetRefundableLimit({String amount = "0"}) {
    showCupertinoModalBottomSheet(
        context: Get.context!,
        isDismissible: false,
        builder: (_) {
          return BottomSheetRefundLimit(
            initialAmount: amount,
            title: "SetRefundTitle".tr,
            onConfirmed: (String value) {
              Get.back();
              updateCashierAsync(
                  mode: CashierUpdateStatus.updateRefundAmount,
                  refundAmount: value,
                  refundStatus: true);
              refundAmount = value;
            },
            onCanceled: () {
              if (amount == "0") isRefundable = false;
            },
          );
        });
  }

  Future<void> getListCashier() async {
    isLoading = true;
    CashierListInputModel input = CashierListInputModel();
    input.businessId = _businessController.businessDetail.id;
    input.totalPage = "10";
    input.perPage = "1";
    input.outletId = "";
    input.filter = "";
    var resultModel = await _cashierService.getListCashier(input);
    if (resultModel.success && resultModel.result != null) {
      cashierList = resultModel.result!.items
          .where((obj) => obj.status.toLowerCase() != "deleted")
          .toList();
    }
    isEmptyData = !resultModel.success || cashierList.isEmpty;
    isLoading = false;
  }

  Future<void> deactivateCashier(bool cashierStatus, String message) async {
    try {
      CashierDeactivateInputModel input = CashierDeactivateInputModel();
      input.phoneNumber = item.phoneNumber;
      input.cashierId = item.id;
      input.cashierStatus =
          cashierStatus ? CashierStatus.inactive : CashierStatus.active;
      var resultModel = await _cashierService.deactivateCashier(input);
      if (resultModel.success && resultModel.result != null) {
        dismiss();
        Get.back(result: true);
        CSnackBar.showSnackBarTop(message: message);
        item.cashierStatus =
            cashierStatus ? CashierStatus.inactive : CashierStatus.active;
      } else {
        dismiss();
        Get.back(result: true);
        CSnackBar.showSnackBarTop(message: "something's went wrong");
      }
    } finally {
      dismiss();
    }
  }

  Future<void> deleteCashier(bool cashierStatus, String message) async {
    try {
      CashierDeactivateInputModel input = CashierDeactivateInputModel();
      input.phoneNumber = item.phoneNumber;
      input.cashierId = item.id;
      input.cashierStatus = CashierStatus.deleted;
      showLoading();
      var resultModel = await _cashierService.deactivateCashier(input);
      if (resultModel.success && resultModel.result != null) {
        getListCashier();
        dismiss();
        Get.back(result: true);
        CSnackBar.showSnackBarTop(message: "CashierAccountClosedMsg".tr);
        item.cashierStatus = CashierStatus.deleted;
      } else {
        getListCashier();
        dismiss();
        Get.back(result: true);
        CSnackBar.showSnackBarTop(message: "something's went wrong");
      }
    } finally {
      dismiss();
    }
  }

  Future<void> reInviteCashier() async {
    CashierDeactivateInputModel input = CashierDeactivateInputModel();
    input.phoneNumber = item.phoneNumber;
    input.cashierId = item.id;
    input.cashierStatus = CashierStatus.renew;
    var resultModel = await _cashierService.renewCashier(input);
    if (resultModel.success && resultModel.result != null) {
      Get.back(result: true);
      item.inviteCode = resultModel.result!.inviteCode;
      inviteCode = resultModel.result!.inviteCode;
      showSuccessInviteCode(item);
    } else {
      Get.back(result: true);
      CSnackBar.showSnackBarTop(
          message: "SomethingWentWrongPleaseTryAgainLater".tr);
    }
  }

  Future<void> updateCashierAsync(
      {required CashierUpdateStatus mode,
      String? refundAmount,
      bool? refundStatus,
      String? cashierInfo}) async {
    var input = SetupCashierInputModel.getCurrentItem(item);
    input.photoUrl = item.photoUrlId;
    switch (mode) {
      case CashierUpdateStatus.updateRefundAmount:
        input.refundLimited = refundAmount.toString();
        input.isRefundable = refundStatus.toString();
        break;
      case CashierUpdateStatus.updateRefundStatus:
        input.isRefundable = refundStatus.toString();
        break;
      case CashierUpdateStatus.updateCashierInfo:
        final info = cashierInfo!.split("!");
        input.photoUrl = info[0];
        input.phoneNumber = info[1];
        input.firstName = info[2];
        input.lastName = info[3];
        input.nationalId = info[4];

        break;
    }

    showLoading();

    try {
      input.phoneNumber = input.phoneNumber.startsWith("0")
          ? input.phoneNumber.substring(1)
          : input.phoneNumber;
      var result = await _cashierService.updateCashier(input);
      dismiss();
      if (result.success) {
        if (mode != CashierUpdateStatus.updateRefundStatus) {
          var inputFileOutlet = UploadFileInputDto();
          inputFileOutlet.uploadType = UploadProfileType.cashierProfile;
          inputFileOutlet.fileName = cashierInfo!.split("!")[0];
          inputFileOutlet.id = _businessController.businessDetail.id;
          inputFileOutlet.filePath = cashierInfo.split("!")[5];
          if (inputFileOutlet.filePath.isNotEmpty) {
            await _fileUploadService.uploadFileAsync(input: inputFileOutlet);
          }
        }

        getListCashier();
        Get.back();
        CSnackBar.showSnackBarTop(message: "CashierAccountUpdatedMsg".tr);
      } else {
        Fluttertoast.showToast(msg: "Error: Failed to update cashier.");
      }
    } catch (e) {
      dismiss();
      Fluttertoast.showToast(msg: "Error: Failed to update cashier.");
    }
  }

  void shareLink() async {

    var value = await createDynamicLink(inviteCode);

    String invitedLink = "CongratulationMessage"
        .tr
        .replaceAll("#1#", "${item.firstName} ${item.lastName}")
        .replaceAll("#2#", inviteCode)
        .replaceAll("#3#", value);

    await Share.share(invitedLink, subject: invitedLink);
  }

  Future<String> createDynamicLink(String inviteCode) async {
    var parameters = DynamicLinkParameters(
      uriPrefix: 'https://canamerchantapp.page.link',
      link: Uri.parse('https://canamerchantapp.page.link/CNB?invitecode=$inviteCode'),
      androidParameters: const AndroidParameters(
        packageName: "com.canadiabank.canamerchantapp.android",
      ),
      iosParameters: const IOSParameters(
        bundleId: 'com.canadiabank.canamerchantapp.iphone',
        minimumVersion: '0',
      ),
    );
    FirebaseDynamicLinks dynamicLinks = FirebaseDynamicLinks.instance;
    final ShortDynamicLink shortLink = await dynamicLinks.buildShortLink(parameters);
    Uri url = shortLink.shortUrl;
    return url.toString();
  }

  //#endregion
}
