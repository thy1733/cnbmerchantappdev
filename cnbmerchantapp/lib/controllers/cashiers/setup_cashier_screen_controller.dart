import 'package:cnbmerchantapp/core.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:share_plus/share_plus.dart';

class SetupCashierScreenController extends AppController {
  final IAppFileUploadService _fileUploadService = AppFileUploadService();
  final CashierService _setupCashierService = CashierService();
  final OutletService _outletService = OutletService();
  final CashierListScreenController _cashierListScreenController =
      Get.find<CashierListScreenController>();
  final OutletListScreenController _outletListScreenController =
      Get.find<OutletListScreenController>();
  final BusinessListScreenController _businessController =
      Get.put(BusinessListScreenController());

  //#region All Override method
  @override
  void onInit() {
    getBusinessDetailFromArgument();
    getCashierInfoFromArgument();
    retrieveOutletList();
    super.onInit();
  }

  @override
  void onClose() {
    phoneNumberFocus.dispose();
    firstNameFocus.dispose();
    lastNameFocus.dispose();

    super.onClose();
  }

  //#endregion

  //#region All Properties

  final _phoneNumber = "".obs;
  String get phoneNumber => _phoneNumber.value;
  set phoneNumber(String value) => _phoneNumber.value = value;

  final _phoneNumberValidateMsg = "".obs;
  String get phoneNumberValidateMsg => _phoneNumberValidateMsg.value;
  set phoneNumberValidateMsg(String value) =>
      _phoneNumberValidateMsg.value = value;

  final _phoneNumberFocus = FocusNode().obs;
  FocusNode get phoneNumberFocus => _phoneNumberFocus.value;
  set phoneNumberFocus(FocusNode value) => _phoneNumberFocus.value = value;

  final _firstName = "".obs;
  String get firstName => _firstName.value;
  set firstName(String value) => _firstName.value = value;

  final _firstNameValidateMsg = "".obs;
  String get firstNameValidateMsg => _firstNameValidateMsg.value;
  set firstNameValidateMsg(String value) => _firstNameValidateMsg.value = value;

  final _firstNameFocus = FocusNode().obs;
  FocusNode get firstNameFocus => _firstNameFocus.value;
  set firstNameFocus(FocusNode value) => _firstNameFocus.value = value;

  final _lastName = "".obs;
  String get lastName => _lastName.value;
  set lastName(String value) => _lastName.value = value;

  final _lastNameValidateMsg = "".obs;
  String get lastNameValidateMsg => _lastNameValidateMsg.value;
  set lastNameValidateMsg(String value) => _lastNameValidateMsg.value = value;

  final _lastNameFocus = FocusNode().obs;
  FocusNode get lastNameFocus => _lastNameFocus.value;
  set lastNameFocus(FocusNode value) => _lastNameFocus.value = value;

  final _nationalId = "".obs;
  String get nationalId => _nationalId.value;
  set nationalId(String value) => _nationalId.value = value;

  final _nationalIdValidateMsg = "".obs;
  String get nationalIdValidateMsg => _nationalIdValidateMsg.value;
  set nationalIdValidateMsg(String value) =>
      _nationalIdValidateMsg.value = value;

  final _isRefund = false.obs;
  bool get isRefund => _isRefund.value;
  set isRefund(bool value) => _isRefund.value = value;

  final _refundAmount = "0".obs;
  String get refundAmount => _refundAmount.value;
  set refundAmount(String value) => _refundAmount.value = value;

  final _currency = CurrencyEnum.usd.obs;
  CurrencyEnum get currency => _currency.value;
  set currency(CurrencyEnum value) => _currency.value = value;

  final _outlets = <ItemSelectOptionModel>[].obs;
  List<ItemSelectOptionModel> get outlets => _outlets;
  set outlets(List<ItemSelectOptionModel> value) => _outlets.value = value;

  final _selectedOutlets = <ItemSelectOptionModel>[].obs;
  List<ItemSelectOptionModel> get selectedOutlets => _selectedOutlets;
  set selectedOutlets(List<ItemSelectOptionModel> value) =>
      _selectedOutlets.value = value;

  final _relatedOutlets = <OutletItemListOutputModel>[].obs;
  List<OutletItemListOutputModel> get relatedOutlets => _relatedOutlets;
  set relatedOutlets(List<OutletItemListOutputModel> value) =>
      _relatedOutlets.value = value;

  final _linkedOutletValidateMsg = "".obs;
  String get linkedOutletValidateMsg => _linkedOutletValidateMsg.value;
  set linkedOutletValidateMsg(String value) =>
      _linkedOutletValidateMsg.value = value;

  final _businessDetail = ManageBusinessItemListOutputModel().obs;
  ManageBusinessItemListOutputModel get businessDetail => _businessDetail.value;
  set businessDetail(ManageBusinessItemListOutputModel value) =>
      _businessDetail.value = value;

  final _cashierOldItem = CashierItemListOutputModel().obs;
  CashierItemListOutputModel get cashierOldItem => _cashierOldItem.value;
  set cashierOldItem(CashierItemListOutputModel value) =>
      _cashierOldItem.value = value;

  final _inviteCode = "".obs;
  String get inviteCode => _inviteCode.value;
  set inviteCode(String value) => _inviteCode.value = value;

  final _expiredIn = "72".obs;
  String get expiredIn => _expiredIn.value;
  set expiredIn(String value) => _expiredIn.value = value;

  //#endregion

  //#region All Method Helpers

  Future<void> getBusinessDetailFromArgument() async {
    if (Get.arguments != null && Get.arguments[ArgumentKey.item] != null) {
      var result =
          Get.arguments[ArgumentKey.item] as ManageBusinessItemListOutputModel;
      businessDetail = result;
    }
  }

  Future<void> getCashierInfoFromArgument() async {
    if (Get.arguments != null && Get.arguments[ArgumentKey.detail] != null) {
      var result =
          Get.arguments[ArgumentKey.detail] as CashierItemListOutputModel;
      cashierOldItem = result;
    }
  }

  Future<void> createCashierAsync() async {
    if (!onFormValidated(true)) return;
    showLoading();
    var input = SetupCashierInputModel();
    input.outletsId = selectedOutlets.map((e) => e.id).toList().join("!");
    input.businessId = businessDetail.id;
    input.isRefundable = isRefund.toString();
    input.refundLimited = refundAmount;
    input.photoUrl = fileUpload.filePath.isNotEmpty ? fileUpload.fileName : "";
    input.phoneNumber =
    phoneNumber.startsWith("0") ? phoneNumber.substring(1) : phoneNumber;
    input.firstName = firstName;
    input.lastName = lastName;
    input.nationalId = nationalId;
    input.username = AppUserTempInstant.appCacheUser.userName;

    try{
      var result = await _setupCashierService.createCashier(input);

      if (result.success) {
        // upload for outlet profile
        var inputFileOutlet = UploadFileInputDto();
        inputFileOutlet.uploadType = UploadProfileType.cashierProfile;
        inputFileOutlet.fileName = fileUpload.fileName;
        inputFileOutlet.id = businessDetail.id;
        inputFileOutlet.filePath = fileUpload.filePath;

        if (inputFileOutlet.filePath.isNotEmpty) {
          await _fileUploadService.uploadFileAsync(input: inputFileOutlet);
        }

        dismiss();
        Get.back();
        showSuccessCreateCashier(result.result?.inviteCode);
      } else {
        dismiss();
        var error = result.result?.description;
        if (error == "") {
          error = "SomethingWentWrongPleaseTryAgainLater".tr;
        }
        Fluttertoast.showToast(msg: error!);
      }
    }catch(e){
      dismiss();
      var error = "SomethingWentWrongPleaseTryAgainLater".tr;
      Fluttertoast.showToast(msg: error);
    }


  }

  Future<void> updateCashierAsync({required CashierUpdateStatus mode}) async {
    if (!onFormValidated(false)) return;
    String photoUrl = cashierOldItem.photoUrlId.isEmpty
        ? fileUpload.fileName
        : cashierOldItem.photoUrlId;
    String cashierInfo =
        "$photoUrl!$phoneNumber!$firstName!$lastName!$nationalId!${fileUpload.filePath}";
    _cashierListScreenController.updateCashierAsync(
        mode: mode, cashierInfo: cashierInfo);
  }

  void showSuccessCreateCashier(String? code) {
    String cashierName = "$firstName $lastName";
    inviteCode = code!;
    KAlertScreen.successInviteCode(
      message: "Congratulation".tr,
      description: cashierName,
      primaryButtonTitle: "ShareInviteCode".tr,
      onPrimaryButtonClick: () async {
        _cashierListScreenController.getListCashier();
        _outletListScreenController.retrieveOutletList();
        await shareInviteCode();
      },
      secondaryButtonTitle: "Done".tr,
      onSecondaryButtonClick: () {
        Get.back();
        _cashierListScreenController.getListCashier();
        _outletListScreenController.retrieveOutletList();
        _businessController.toBusinessDetailAsync(businessDetail);
      },
      formatedInviteCode: formatedInviteCodeString(inviteCode),
      expiredIn: expiredIn,
    );
  }

  Future<void> shareInviteCode() async {
    if (isLoading) return;
    isLoading = true;
    shareLink();
    await Future.delayed(const Duration(seconds: 1));
    isLoading = false;
  }

  Future<void> retrieveOutletList() async {
    var input = OutletListInputModel();
    input.businessId = businessDetail.id;
    input.filter = "";
    input.maxResultCount = 10;
    input.skipCount = 1;
    var response = await _outletService.getListOutlet(input);
    if (response.result != null && response.success) {
      relatedOutlets = response.result!.items;
      outlets = response.result!.items
          .map((e) =>
              ItemSelectOptionModel.createItem(e.id, e.outletName, false))
          .toList();
      selectedOutlets = outlets.where((element) => element.isSelected).toList();
    }
  }

  void uploadCashierProfile() async {
    await super.pickImagePushAsync().then((value) async {
      if (value != null) {
        photoFile = await value.readAsBytes();
        //file name and upload to temporary file path.
        var fileName = DateTime.now().toFormatDateString("yyyymmddhhmmsss");
        fileUpload.fileName = fileName;
        fileUpload.filePath = value.path;
      }
    });
  }

  //#endregion

  //#region All Command

  bool onFormValidated(bool isCreate) {
    onPhoneNumberValidate();
    onFirstNameValidate();
    onLastNameValidate();

    onNationalIDValidate();

    onPhoneNumberFocus();
    onFirstNameFocus();
    onLastNameFocus();

    if(isCreate) {
      onLinkedOutletsValidate();
      return phoneNumberValidateMsg.isEmpty &&
          firstNameValidateMsg.isEmpty &&
          lastNameValidateMsg.isEmpty &&
          selectedOutlets.isNotEmpty;
    }else{
      return phoneNumberValidateMsg.isEmpty &&
          firstNameValidateMsg.isEmpty &&
          lastNameValidateMsg.isEmpty;
    }
  }

  void onPhoneNumberChanged(String value) {
    phoneNumber = value;
  }

  void onPhoneNumberValidate() {
    String phone = phoneNumber.removeAllWhitespace;
    bool isValidPhoneNumber = phone.length < 8;
    bool isValidPrefix = false;
    if (phone.length >= 2) {
      isValidPrefix = [
        '11',
        '12',
        '17',
        '61',
        '76',
        '77',
        '78',
        '79',
        '85',
        '89',
        '92',
        '95',
        '10',
        '15',
        '16',
        '69',
        '70',
        '81',
        '86',
        '87',
        '93',
        '98',
        '96',
        '31',
        '60',
        '66',
        '67',
        '68',
        '71',
        '88',
        '90',
        '97',
        '13',
        '80',
        '83',
        '84',
        '38',
        '99',
        '18'
      ].contains(phone.substring(0, 2));
    }

    phoneNumberValidateMsg = phoneNumber.isEmpty
        ? "PhoneNumberRequired".tr
        : isValidPhoneNumber
            ? "InvalidPhoneNumber".tr
            : !isValidPrefix
                ? "InvalidPhoneNumber".tr
                : "";
  }

  void onPhoneNumberFocus() {
    phoneNumberValidateMsg.isNotEmpty
        ? phoneNumberFocus.requestFocus()
        : phoneNumberFocus.unfocus();
  }

  void onFirstNameChanged(String value) {
    firstName = value;
    onFirstNameValidate();
  }

  void onFirstNameValidate() {
    bool isNameInvalid = !isValidName(firstName);
    if(firstName.isEmpty){
      firstNameValidateMsg = "FirstNameRequired".tr;
    }else if(isNameInvalid){
      firstNameValidateMsg = "NameInValid".tr;
    }else{
      firstNameValidateMsg = "";
    }
  }

  void onFirstNameFocus() {
    if (phoneNumberValidateMsg.isEmpty) {
      firstNameValidateMsg.isNotEmpty
          ? firstNameFocus.requestFocus()
          : firstNameFocus.unfocus();
    }
  }

  bool isInputAllValid() {
    return false;
  }

  void onLastNameChanged(String value) {
    lastName = value;
    onLastNameValidate();
  }

  void onLastNameValidate() {
    bool isNameInvalid = !isValidName(lastName);
    if(lastName.isEmpty){
      lastNameValidateMsg = "LastNameRequired".tr;
    }
    else if(isNameInvalid){
      lastNameValidateMsg = "NameInValid".tr;
    }else{
      lastNameValidateMsg = "";
    }
  }

  void onLastNameFocus() {
    if (firstNameValidateMsg.isEmpty) {
      lastNameValidateMsg.isNotEmpty
          ? lastNameFocus.requestFocus()
          : lastNameFocus.unfocus();
    }
  }

  void onNationalIdChanged(String value) {
    nationalId = value;
  }

  void onNationalIDValidate() {
    bool condition = nationalId.length < 9;
    if (nationalId.isNotEmpty) {
      nationalIdValidateMsg = condition ? "InvalidNationalID".tr : "";
    } else {
      nationalIdValidateMsg = "";
    }
  }

  void onLinkedOutletsValidate() {
    linkedOutletValidateMsg =
        selectedOutlets.isEmpty ? "LinkedOutletToCashierRequired".tr : "";
  }

  void onRefundChanged(bool value) {
    isRefund = value;
    return;
    // if (isRefund) {
    //   onSetRefundableLimit();
    // } else {
    //   refundAmount = "0";
    // }
  }

  void onSetRefundableLimit() {
    showCupertinoModalBottomSheet(
        context: Get.context!,
        isDismissible: false,
        builder: (_) {
          return BottomSheetRefundLimit(
            title: "SetRefundLimitPerTransaction".tr,
            onConfirmed: (String value) {
              refundAmount = value;
            },
            onCanceled: () {
              isRefund = false;
            },
          );
        });
  }

  bool onShowLinkedOutlets() {
    return outlets.length != selectedOutlets.length;
  }

  void showSelectedOutlets() {
    showCupertinoModalBottomSheet(
        topRadius: const Radius.circular(23),
        backgroundColor: AppColors.barrierBackground,
        isDismissible: false,
        context: Get.context!,
        builder: (_) {
          return BottomSheetCheckOption(
              title: "SelectOutlet".tr,
              items: outlets,
              onSelectedItem: (items) {
                selectedOutlets = items;
                onLinkedOutletsValidate();
              });
        });
  }

  void removeOutlets(ItemSelectOptionModel item) {
    outlets
        .where((element) => element.index == item.index)
        .toList()
        .map((e) => e.isSelected = false)
        .toList();
    selectedOutlets.remove(item);
  }

  void setupInitValue() {
    phoneNumber = cashierOldItem.phoneNumber;
    firstName = cashierOldItem.firstName;
    lastName = cashierOldItem.lastName;
    nationalId = cashierOldItem.nationalId;
  }

  void shareLink() async {

    var value = await createDynamicLink(inviteCode);

    String invitedLink = "CongratulationMessage"
        .tr
        .replaceAll("#1#", "$firstName $lastName")
        .replaceAll("#2#", inviteCode)
        .replaceAll("#3#", value);

    await Share.share(invitedLink, subject: invitedLink);
  }

  Future<String> createDynamicLink(String inviteCode) async {
    var parameters = DynamicLinkParameters(
      uriPrefix: 'https://canamerchantapp.page.link',
      link: Uri.parse('https://canamerchantapp.page.link/CNB?invitecode=$inviteCode'),
      androidParameters: const AndroidParameters(
        packageName: "com.canadiabank.canamerchantapp.android",
      ),
      iosParameters: const IOSParameters(
        bundleId: 'com.canadiabank.canamerchantapp.iphone',
        minimumVersion: '0',
      ),
    );
    FirebaseDynamicLinks dynamicLinks = FirebaseDynamicLinks.instance;
    final ShortDynamicLink shortLink = await dynamicLinks.buildShortLink(parameters);
    Uri url = shortLink.shortUrl;
    return url.toString();
  }

  bool isValidName(String name) {
    RegExp nameRegex = RegExp(r'^[\x00-\x7F\s_-]+$');
    return nameRegex.hasMatch(name);
  }
  //#endregion
}
